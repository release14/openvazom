package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Sar_msg_ref_num extends OptionalParameterBase {
	public Sar_msg_ref_num(){
		type = OptionalParameterType.SAR_MSG_REF_NUM;
	}
	public int value;

	public void init(byte[] data) {
		value = ((data[0] << 8) & 0xff) + (data[1] & 0xff);

	}

}
