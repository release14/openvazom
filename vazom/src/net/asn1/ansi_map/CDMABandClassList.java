package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMABandClassList extends SEQUENCE{

	public CDMABandClassInformationType new_child(){
		return new CDMABandClassInformationType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMABandClassInformationType getChild(int index){
		return (CDMABandClassInformationType)of_children.get(index);
	}
	public CDMABandClassList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
