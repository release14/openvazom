package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ProtocolVersion extends OCTET_STRING{

	public ProtocolVersion(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 26;
		asn_class = ASNTagClass.PRIVATE;
	}
}
