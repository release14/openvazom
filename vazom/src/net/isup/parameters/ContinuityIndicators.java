package net.isup.parameters;

import net.isup.ParameterType;

public class ContinuityIndicators extends ParameterBase {
	public boolean continuity_check_successful;
	
	public ContinuityIndicators(){
		type = ParameterType.CONTINUITY_INDICATORS;
	}

	public void init(byte[] data) {
		if(data != null){
			continuity_check_successful = (data[byte_position] & 0x01) == 0x01;
		}
		
	}

}
