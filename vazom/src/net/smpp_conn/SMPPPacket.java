package net.smpp_conn;

import net.smpp.pdu.PDUBase;

public class SMPPPacket {
	public String system_id;
	public String password;
	
	public PDUBase smpp_pdu;
	public byte[] smpp_data;
	public String client_ip;
	public int client_port;
	public String host_ip;
	public int host_port;
	
}
