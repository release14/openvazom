package net.gpu2;

public enum GPUDataItemType {
	S, 		// Kernel stats
	LD, 	// LD lists
	U, 		// Update list
	R,		// Remove item
	RST,	// reset
	MD5; 	// MD5 lists
}
