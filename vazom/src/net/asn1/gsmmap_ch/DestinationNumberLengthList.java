package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DestinationNumberLengthList extends SEQUENCE{

	public INTEGER new_child(){
		return new INTEGER();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public INTEGER getChild(int index){
		return (INTEGER)of_children.get(index);
	}
	public DestinationNumberLengthList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
