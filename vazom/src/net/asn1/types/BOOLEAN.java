package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class BOOLEAN extends ASNType {

	public static byte TRUE = (byte)0xff;
	public static byte FALSE = (byte)0x00;
	
	public BOOLEAN(){
		super();
		universalTag = ASNTag.BOOLEAN;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Primitive;
	}
}
