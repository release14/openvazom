package net.isup.parameters;

import net.isup.ParameterType;

public class PivotCapability extends ParameterBase {
	public PivotCapability(){
		type = ParameterType.PIVOT_CAPABILITY;
	}
}
