package net.spcap;

public class SLLHeader {
	public int packetType;
	public int addressType;
	public int addressLength;
	public byte[] source;
	public int protocol;
	public byte[] payload;
	
}
