package net.mtp3.messages.signalling;

import java.util.ArrayList;

import net.mtp3.messages.SignallingMessageGroupType;
import net.mtp3.messages.SignallingMessageType;
import net.utils.Utils;

public class TrafficRestartAllowed extends SignallingMessageBase {

	public TrafficRestartAllowed(){
		super();
		signallingGroupType = SignallingMessageGroupType.TRM;
		signallingType = SignallingMessageType.TRA;
	}
	
	
	public byte[] encode(){
		ArrayList<Byte> buff = new ArrayList<Byte>();
		byte[] res = null;
		int tmp = (signallingType.getId() << 4) + signallingGroupType.getId();
		buff.add((byte)tmp);
		res = Utils.list2array(buff);
		return res;
	
	}
	
	
	public void init(byte[] data) {
		super.init(data);
	}

}
