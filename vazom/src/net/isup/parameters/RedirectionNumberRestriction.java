package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectionNumberRestriction extends ParameterBase {
	public RedirectionNumberRestriction(){
		type = ParameterType.REDIRECTION_NUMBER_RESTRICTION;
	}
}
