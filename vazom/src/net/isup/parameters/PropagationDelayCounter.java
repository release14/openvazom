package net.isup.parameters;

import net.isup.ParameterType;

public class PropagationDelayCounter extends ParameterBase {
	public PropagationDelayCounter(){
		type = ParameterType.PROPAGATION_DELAY_COUNTER;
	}
}
