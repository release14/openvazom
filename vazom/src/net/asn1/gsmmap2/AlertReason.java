package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AlertReason extends ENUMERATED{

	public static final int _ms_Present = 0;
	public static final int _memoryAvailable = 1;
	public AlertReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
