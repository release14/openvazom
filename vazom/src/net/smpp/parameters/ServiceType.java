package net.smpp.parameters;

public enum ServiceType {
	DEFAULT,
	CMT,
	CPT,
	VMN,
	VMA,
	WAP,
	USSD;
}
