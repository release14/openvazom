package net.m3ua.messages;

import java.util.ArrayList;
import java.util.Arrays;

import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.ASPIdentifier;
import net.m3ua.parameters.InfoString;
import net.m3ua.parameters.RoutingContext;
import net.m3ua.parameters.Status;

public class NTFY extends M3UAMessage {
	public Status status;
	public ASPIdentifier aspIdentifier;
	public RoutingContext routingContext;
	public InfoString infoString;
	
	public NTFY(){
		type = M3UAMessageType.NTFY;
	}

	public void init(byte[] data) {
		int tag;
		int l;
		int m;
		M3UAParameterType pt;
		while(byte_pos < data.length){
			tag = (data[byte_pos] << 8) + (data[byte_pos + 1] & 0xFF);
			byte_pos += 2;
			// length(l) = two bytes for tag + two bytes for length + length of actual data
			l = (data[byte_pos] << 8) + (data[byte_pos + 1] & 0xFF);
			byte_pos += 2;
			
			pt = M3UAParameterType.get(tag);
			switch(pt){
				case STATUS:
					status = new Status();
					status.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				case ASP_IDENTIFIER:
					aspIdentifier = new ASPIdentifier();
					aspIdentifier.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				case ROUTING_CONTEXT:
					routingContext = new RoutingContext();
					routingContext.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				case INFO_STRING:
					infoString = new InfoString();
					infoString.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				
			}

			// parameter has to be a multiple of 4, if not, zero padding is added
			m = l % 4;
			byte_pos += l + (m > 0 ? 4 - m : 0) - 4;
			
		}
	}



	public int encode(ArrayList<Byte> buff) {
		int param_length = 0;
		// parameters
		param_length += processParameter(status, buff);
		param_length += processParameter(aspIdentifier, buff);
		param_length += processParameter(routingContext, buff);
		param_length += processParameter(infoString, buff);
		return param_length;
	}

}
