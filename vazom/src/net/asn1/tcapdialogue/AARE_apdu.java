package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARE_apdu extends SEQUENCE{

	public BIT_STRING get_protocol_version(){
		return (BIT_STRING)elements.get(0).data;
	}
	public BIT_STRING new_protocol_version(){
		return new BIT_STRING();
	}

	public AARE_apdu_application_context_name get_application_context_name(){
		return (AARE_apdu_application_context_name)elements.get(1).data;
	}
	public AARE_apdu_application_context_name new_application_context_name(){
		return new AARE_apdu_application_context_name();
	}

	public AARE_apdu_Associate_result get_result(){
		return (AARE_apdu_Associate_result)elements.get(2).data;
	}
	public AARE_apdu_Associate_result new_result(){
		return new AARE_apdu_Associate_result();
	}

	public AARE_apdu_result_source_diagnostic get_result_source_diagnostic(){
		return (AARE_apdu_result_source_diagnostic)elements.get(3).data;
	}
	public AARE_apdu_result_source_diagnostic new_result_source_diagnostic(){
		return new AARE_apdu_result_source_diagnostic();
	}

	public User_information get_user_information(){
		return (User_information)elements.get(4).data;
	}
	public User_information new_user_information(){
		return new User_information();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_protocol_version(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_application_context_name(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_result(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_result_source_diagnostic(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_user_information(); }});
	}
	public AARE_apdu(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 1;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.APPLICATION;
	}
}
