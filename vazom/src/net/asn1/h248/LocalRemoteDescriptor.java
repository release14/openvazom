package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocalRemoteDescriptor extends SEQUENCE{

	public PropertyGroupSequenceOf get_propGrps(){
		return (PropertyGroupSequenceOf)elements.get(0).data;
	}
	public PropertyGroupSequenceOf new_propGrps(){
		return new PropertyGroupSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_propGrps(); }});
	}
	public LocalRemoteDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
