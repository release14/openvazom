package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SearchRes extends SET{

	public ServiceDataAccessElementList get_serviceDataAccessElementList(){
		return (ServiceDataAccessElementList)elements.get(0).data;
	}
	public ServiceDataAccessElementList new_serviceDataAccessElementList(){
		return new ServiceDataAccessElementList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(271, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceDataAccessElementList(); }});
	}
	public SearchRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
