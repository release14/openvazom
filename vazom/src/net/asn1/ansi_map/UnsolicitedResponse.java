package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UnsolicitedResponse extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(2).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(3).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public Digits get_digits_Destination(){
		return (Digits)elements.get(4).data;
	}
	public Digits new_digits_Destination(){
		return new Digits();
	}

	public ExtendedMSCID get_extendedMSCID(){
		return (ExtendedMSCID)elements.get(5).data;
	}
	public ExtendedMSCID new_extendedMSCID(){
		return new ExtendedMSCID();
	}

	public ExtendedSystemMyTypeCode get_extendedSystemMyTypeCode(){
		return (ExtendedSystemMyTypeCode)elements.get(6).data;
	}
	public ExtendedSystemMyTypeCode new_extendedSystemMyTypeCode(){
		return new ExtendedSystemMyTypeCode();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(7).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public SystemAccessType get_systemAccessType(){
		return (SystemAccessType)elements.get(8).data;
	}
	public SystemAccessType new_systemAccessType(){
		return new SystemAccessType();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(9).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public MEID get_meid(){
		return (MEID)elements.get(10).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits_Destination(); }});
		elements.add(new ElementDescriptor(53, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedMSCID(); }});
		elements.add(new ElementDescriptor(54, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedSystemMyTypeCode(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(34, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemAccessType(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public UnsolicitedResponse(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
