package net.smstpdu.tpdcs.data_encoding;

import java.util.HashMap;



public enum MessageEncoding {
	DEFAULT(0x00),
	_8BIT(0x04);
	
	// BIT 2 = 00000X00
	private int id;
	private static final HashMap<Integer, MessageEncoding> lookup = new HashMap<Integer, MessageEncoding>();
	static{
		for(MessageEncoding td : MessageEncoding.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static MessageEncoding get(int id){ return lookup.get(id); }
	private MessageEncoding(int _id){ id = _id; }	
	
}
