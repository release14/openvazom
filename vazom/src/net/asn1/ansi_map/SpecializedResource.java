package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SpecializedResource extends OCTET_STRING{

	public SpecializedResource(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
