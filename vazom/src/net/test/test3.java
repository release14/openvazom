package net.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import net.m3ua.messages.DATA;
import net.sctp.SCTP;
import net.sctp.SCTPPacket;
import net.sctp.chunk.ChunkBase;
import net.smstpdu.GSMAlphabet;
import net.smstpdu.MessageDirection;
import net.smstpdu.tpdu.SmsSubmit;
import net.smstpdu.tpdu.TPDU;
import net.utils.Utils;

public class test3 {
	public static void main(String[] args) {
		byte[] data = Utils.strhex2bytes("c46fd09c2e3f87d9a07119247e83e665107dfd06c1c320b83c3caf83e66dd0da1e06d1d32078181406e9cb207b790e5a87dba0b71ea407adcbf2761d749697e7f430c8ee0685d92076180daa9fd3e270fb07");
		String msg = "";//GSMAlphabet.decode(GSMAlphabet.reposition(data, 1));
		//System.out.println(msg);
		//System.out.println(GSMAlphabet.decode(new byte[]{0x00}).length());
		msg = GSMAlphabet.decodePadded(data, 6);
		System.out.println(msg);
		
		data = Utils.strhex2bytes("cfb50b");
		msg = GSMAlphabet.decode(data);
		System.out.println(msg);
		
		TPDU tpdu =  TPDU.decode(Utils.strhex2bytes("51280b918336619046f40000ab8c050003750202ece536881c06b9d3f336c8bd7eb3416e71d9ddae83dc613939cd0e83dc63d09c1d169fc32c777ade06cdd961105c5e5ebf4174791d5e6683d26e905b5c769fc3a0f7b9ce3687d9e116081e06d1df20f718e40ed3cb207719c42ed3d3207218e40ed7e7a039bc4c07cdcb203b88fe06e9c3f0f4b80e"), MessageDirection.MS_SC);
		System.out.println(tpdu.type);
		SmsSubmit ss = (SmsSubmit)tpdu;
		System.out.println(ss.udh.findIERef().partNumber);
		data = Utils.strhex2bytes("0b5903e89ed7bd9cdbcd7718000300b87bcb921f000e33a70000000301000101000000a8020000080000000802100096000023010000148a030200070980030e190b12070011048346016011010b12060011048376000041016862664804498310156b1e281c060700118605010101a011600f80020780a1090607040000010003036c3ea13c0201000201043034800892730002401392f58107918346016011f18207918346515390f18807918376000001f1890743670136042a318f0206c090000000000301107bcb9220000b2e090000000301000101000001000200000800000008021000ef000023010000148b030200010980030e190b12080011048346012011010b1208001104837600700007c16581be4803024f1f4904970001cb6c81b0a181ad02010102012c3081a4800892730002404947f98407918376007000f704818e440b918346915905f70000012191310480408c050003750202ece536881c06b9d3f336c8bd7eb3416e71d9ddae83dc613939cd0e83dc63d09c1d169fc32c777ade06cdd961105c5e5ebf4174791d5e6683d26e905b5c769fc3a0f7b9ce3687d9e116081e06d1df20f718e40ed3cb207719c42ed3d3207218e40ed7e7a039bc4c07cdcb203b88fe06e9c3f0f4b80e00");
		SCTPPacket sp = SCTP.decode(data);
		//DATA chunk = (DATA)(sp.chunks.get(1;
		ChunkBase ch = sp.chunks.get(1);
		net.sctp.chunk.DATA dc = (net.sctp.chunk.DATA)ch;
		
		System.out.println(Utils.bytes2hexstr(dc.userData, " "));
		double aaa = 7.666;
		System.out.println(String.format("%.2f",aaa));
		//Date dt =
		Calendar c = Calendar.getInstance();
	
		c.set(Calendar.HOUR_OF_DAY, 0);
		System.out.println("HOD: " + c.get(Calendar.HOUR));
		c.set(2011, 5, 5, 0, 0);
		//c.set(Calendar.MILLISECOND, 0);
		System.out.println("MSEC: " + c.get(Calendar.MILLISECOND));
		HashMap<Calendar, Integer> t = new HashMap<Calendar, Integer>();
		t.put(c, 199);
		
		Calendar c2 = Calendar.getInstance();
		c2.clear();
		c2.set(2011, 5, 5, 0, 0);
		//c2.set(Calendar.MILLISECOND, 0);
		System.out.println("MSEC: " + c2.get(Calendar.MILLISECOND));
		System.out.println(c.get(Calendar.AM_PM));
		
		System.out.println(t.get(c2));
		
		System.out.println(Utils.date2str(Calendar.getInstance()));
		
		File f = new File("/tmp/test.dat");
		try{
			PrintWriter out = new PrintWriter(f);
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			out.println("AAAAA");
			out.println("BBBB");
			out.flush();
			in.mark(1);
			System.out.println("READ: " + in.readLine());
			System.out.println("READ: " + in.readLine());
			in.reset();
			System.out.println("READ: " + in.readLine());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String ref = "refilter start 2010-99-33 99:33:33, 2010-99-33 33:33:33";
		String[] sref = ref.split(" ", 3);
		System.out.println(sref[2]);
		
		Object ii = 3;
		Object ii2 = 6;
		
		System.out.println(ii.equals(ii2));
		
			
		
		
	}
			
}
