package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMACodeChannelInformation extends SEQUENCE{

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(0).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public CDMACodeChannel get_cdmaCodeChannel(){
		return (CDMACodeChannel)elements.get(1).data;
	}
	public CDMACodeChannel new_cdmaCodeChannel(){
		return new CDMACodeChannel();
	}

	public CDMAPilotPN get_cdmaPilotPN(){
		return (CDMAPilotPN)elements.get(2).data;
	}
	public CDMAPilotPN new_cdmaPilotPN(){
		return new CDMAPilotPN();
	}

	public CDMAPowerCombinedIndicator get_cdmaPowerCombinedIndicator(){
		return (CDMAPowerCombinedIndicator)elements.get(3).data;
	}
	public CDMAPowerCombinedIndicator new_cdmaPowerCombinedIndicator(){
		return new CDMAPowerCombinedIndicator();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(68, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaCodeChannel(); }});
		elements.add(new ElementDescriptor(173, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPilotPN(); }});
		elements.add(new ElementDescriptor(228, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPowerCombinedIndicator(); }});
	}
	public CDMACodeChannelInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
