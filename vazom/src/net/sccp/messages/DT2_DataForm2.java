package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.SeqSegm;

public class DT2_DataForm2 extends MessageBase {
	public Dlr destinationLocalReference;
	public SeqSegm sequencingSegmenting;
	public byte[] data;
	
	public DT2_DataForm2(){
		super();
		type = MessageType.DT2_DATA_FORM_2;
		pointers = new Pointer[1];
	}
	
	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;

		// Fixed mandatory
		sequencingSegmenting = new SeqSegm();
		sequencingSegmenting.init(new byte[]{_data[byte_pos++]});
		
		// One pointer, variable length
		pointers[0] = new Pointer((_data[byte_pos] & 0xFF), (_data[byte_pos] & 0xFF) + (byte_pos++));

		// First mandatory variable part
		data = Arrays.copyOfRange(_data, pointers[0].position + 1, pointers[0].position + 1 + (_data[pointers[0].position] & 0xFF));
		
		
	}

}
