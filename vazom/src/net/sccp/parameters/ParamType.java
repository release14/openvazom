package net.sccp.parameters;

import java.util.HashMap;


public enum ParamType {
	END_OF_OPTIONAL_PARAMETERS(0x00),
	DESTINATION_LOCAL_REFERENCE(0x01),
	SOURCE_LOCAL_REFERENCE(0x02),
	CALLED_PARTY_ADDRESS(0x03),
	CALLING_PARTY_ADDRESS(0x04),
	PROTOCOL_CLASS(0x05),
	SEGMENTING_REASSEMBLING(0x06),
	RECEIVE_SEQUENCE_NUMBER(0x07),
	SEQUENCING_SEGMENTING(0x08),
	CREDIT(0x09),
	RELEASE_CAUSE(0x0A),
	RETURN_CAUSE(0x0B),
	RESET_CAUSE(0x0C),
	ERROR_CAUSE(0x0D),
	REFUSAL_CAUSE(0x0E),
	DATA(0x0F),
	SEGMENTATION(0x10),
	HOP_COUNTER(0x11),
	IMPORTANCE(0x12),
	LONG_DATA(0x13);
	
	// 1 byte
	private int id;
	private static final HashMap<Integer, ParamType> lookup = new HashMap<Integer, ParamType>();
	static{
		for(ParamType td : ParamType.values()){
			lookup.put(td.getId(), td);
		}
	}
	private int getId(){ return id; }
	public static ParamType get(int id){ return lookup.get(id); }
	private ParamType(int _id){ id = _id; }
	
	
}
