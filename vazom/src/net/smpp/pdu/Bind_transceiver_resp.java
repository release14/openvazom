package net.smpp.pdu;

import java.util.ArrayList;
import java.util.Arrays;

import net.smpp.PDUType;
import net.utils.Utils;

public class Bind_transceiver_resp extends PDUBase {
	public String system_id;
	public byte[] sc_interface_version;

	public Bind_transceiver_resp(){
		type = PDUType.BIND_TRANSCEIVER_RESP;
	}

	public byte[] encode(){
		//command_length += system_id.length() + 1 + (sc_interface_version != null ? sc_interface_version.length : 0);
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		Utils.bytesToLst(buff, Utils.Cstr_encode(system_id));
		if(sc_interface_version != null) Utils.bytesToLst(buff, sc_interface_version);
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		
		return Utils.list2array(buff);

	}
	
	
	public void init(byte[] data) {
		// System ID
		system_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += system_id.length() + 1;
		sc_interface_version = Arrays.copyOfRange(data, byte_pos, data.length);

	}

}
