package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class GeographicPosition extends OCTET_STRING{

	public GeographicPosition(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
