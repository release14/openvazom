package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReturnErrorParameter extends ANY{

	public ReturnErrorParameter(){
		super();
		asn_pc = ASNTagComplexity.Unknown;
		tag = -2;
	}
}
