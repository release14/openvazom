package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PropertyIDSequenceOf extends SEQUENCE{

	public PropertyID new_child(){
		return new PropertyID();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public PropertyID getChild(int index){
		return (PropertyID)of_children.get(index);
	}
	public PropertyIDSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
