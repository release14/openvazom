package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class IMS_VoiceOverPS_SessionsInd extends ENUMERATED{

	public static final int _imsVoiceOverPS_SessionsNotSupported = 0;
	public static final int _imsVoiceOverPS_SessionsSupported = 1;
	public IMS_VoiceOverPS_SessionsInd(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
