package net.test;

public class test9 {
	public static Object mutex = new Object();
	public static class R1 implements Runnable{
		public void run() {
			System.out.println("R1:1");
			synchronized(mutex){
				try{
					mutex.wait();					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			System.out.println("R1:2");
			System.out.println("R1:3");
			
		}
		
		
	}
	public static class R2 implements Runnable{
		public void run() {
			System.out.println("R2:1");
			synchronized(mutex){
				try{
					mutex.notify();					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			System.out.println("R2:2");
			System.out.println("R2:3");
			
			
		}
		
		
	}
	public static void main(String[] args) {
		R1 r1 = new R1();
		R2 r2 = new R2();
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		t1.start();
		t2.start();
		
		
	}

}
