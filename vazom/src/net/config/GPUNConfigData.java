package net.config;

import java.lang.reflect.Field;
import java.util.ArrayList;

import net.gpu.GridDescriptor;

public class GPUNConfigData {
	public static String[] search(String ptrn){
		String[] res = null;
		ArrayList<String> tmp = new ArrayList<String>();
		Field[] flst = GPUNConfigData.class.getFields();
		for(int i = 0; i<flst.length; i++) if(flst[i].getName().toUpperCase().contains(ptrn.toUpperCase())){
			tmp.add(flst[i].getName().replaceAll("_", "."));
		}
		res = new String[tmp.size()];
		for(int i = 0; i<tmp.size(); i++) res[i] = tmp.get(i);
		return res;
	}
	
	public static void set(String item_name, String item_value){
		item_name = item_name.replaceAll("\\.", "_");
		try{
			Field fld = GPUNConfigData.class.getField(item_name);
			String fld_type = fld.getType().getSimpleName();

			if(fld_type.equalsIgnoreCase("String")){
				fld.set(GPUNConfigData.class, item_value);
				
			}else if(fld_type.equalsIgnoreCase("int")){
				fld.set(GPUNConfigData.class, Integer.parseInt(item_value));
				
			}else if(fld_type.equalsIgnoreCase("boolean")){
				if(item_value.equalsIgnoreCase("true")){
					fld.set(GPUNConfigData.class, true);
				}else{
					fld.set(GPUNConfigData.class, false);
					
				}
				
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
	}
	public static Object get(String item_name){
		item_name = item_name.replaceAll("\\.", "_");
		try{
			Field fld = GPUNConfigData.class.getField(item_name);
			return fld.get(GPUNConfigData.class);
		}catch(Exception e){
			//e.printStackTrace();
		}
		return null;
	}
	
	
	
	public static String id;
	public static int cuda_max_repetition_size;
	public static int cuda_max_spam_size;
	public static int cuda_max_quarantine_size;
	public static int cuda_max_md5_size;
	public static int cuda_max_i_size;
	public static int cuda_max_i_size_variation;
	public static int cuda_max_q_size;
	public static GridDescriptor repetition_grid;
	public static GridDescriptor spam_grid;
	public static GridDescriptor quarantine_grid;
	public static GridDescriptor md5_grid;
	public static int[] cuda_devices;
	public static int cli_port;
	public static int port;
	public static int ld_threshold;
	public static boolean debug;
	
}
