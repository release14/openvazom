package net.smsfs;

public enum FilterType {
	ISUP,
	MO,
	MT,
	SMPP_MO,
	SMPP_MT,
	HLR;
}
