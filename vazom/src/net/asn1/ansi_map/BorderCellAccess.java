package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class BorderCellAccess extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _border_Cell_Access =  1 ;
	public BorderCellAccess(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
