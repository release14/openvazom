package net.isup.parameters;

import java.util.ArrayList;

import net.isup.ParameterType;
import net.isup.parameters.csi.CSIOctet;
import net.isup.parameters.csi.CallProcessingStateType;
import net.isup.parameters.csi.HardwareBlockingStateType;
import net.isup.parameters.csi.MaintenanceBlockingStateType;

public class CircuitStateIndicator extends ParameterBase {
	public ArrayList<CSIOctet> csi_octets;
	
	public CircuitStateIndicator(){
		type = ParameterType.CIRCUIT_STATE_INDICATOR;
		csi_octets = new ArrayList<CSIOctet>();
	}

	public void init(byte[] data) {
		if(data != null){
			CSIOctet csi_o = null;
			for(int i = 0; i<data.length; i++){
				csi_o = new CSIOctet();
				if((data[i] & 0x0c) == 0x00){
					csi_o.maintenance_blocking_state = MaintenanceBlockingStateType.get(data[i] & 0x03);
				}else{
					csi_o.maintenance_blocking_state = MaintenanceBlockingStateType.get(data[i] & 0x03);
					csi_o.call_processing_state = CallProcessingStateType.get(data[i] & 0x0c);
					csi_o.hardware_blocking_state = HardwareBlockingStateType.get(data[i] & 0x30);
				}
				// add
				csi_octets.add(csi_o);
			}
		}
	}

}
