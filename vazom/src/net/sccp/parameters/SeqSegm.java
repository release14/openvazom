package net.sccp.parameters;

public class SeqSegm extends ParamBase {
	public int sendSeqNum;
	public int recvSeqNum;
	public boolean moreDataIndication;
	
	public SeqSegm(){
		super();
		type = ParamType.SEQUENCING_SEGMENTING;
	}

	public void init(byte[] data) {
		sendSeqNum = (data[0] & 0xFE) >> 1;
		recvSeqNum = (data[1] & 0xFE) >> 1;
		moreDataIndication = (data[1] & 0x01) == 0x01;
	}

}
