package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DP_AnalysedInfoCriteriaList extends SEQUENCE{

	public DP_AnalysedInfoCriterium new_child(){
		return new DP_AnalysedInfoCriterium();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public DP_AnalysedInfoCriterium getChild(int index){
		return (DP_AnalysedInfoCriterium)of_children.get(index);
	}
	public DP_AnalysedInfoCriteriaList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
