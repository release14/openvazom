package net.asn1.types;

import java.util.ArrayList;

import net.asn1.compiler.ASNTagClass;
import net.asn1.compiler.ASNType;
import net.asn1.compiler.ElementDescriptor;
//import net.asn1.compiler.TagDescriptor;

public class EXTERNAL_ENCODING extends CHOICE{
//	public ASNType selectedChoice;
/*
	@TagDescriptor(tagValue = 0, tagClass = ASNTagClass.CONTEXT_SPECIFIC)
	public ASNType single_asn1_type;

	@TagDescriptor(tagValue = 1, tagClass = ASNTagClass.CONTEXT_SPECIFIC)
	public OCTET_STRING octet_aligned;

	@TagDescriptor(tagValue = 2, tagClass = ASNTagClass.CONTEXT_SPECIFIC)
	public BIT_STRING arbitrary;
*/
	
	public ASNType get_single_asn1_type(){
		return (ASNType)elements.get(0).data;
	}
	public ASNType new_single_asn1_type(){
		return new ASNType();
	}
	
	public OCTET_STRING get_octet_aligned(){
		return (OCTET_STRING)elements.get(1).data;
	}
	public OCTET_STRING new_octet_aligned(){
		return new OCTET_STRING();
	}

	public BIT_STRING get_arbitrary(){
		return (BIT_STRING)elements.get(2).data;
	}
	public BIT_STRING new_arbitrary(){
		return new BIT_STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_single_asn1_type(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_octet_aligned(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_arbitrary(); }});
	}
	
	
	
	public EXTERNAL_ENCODING(){
		super();
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		
		
	}
}
