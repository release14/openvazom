package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSDeliveryPointToPoint extends SET{

	public SMS_BearerData get_sms_BearerData(){
		return (SMS_BearerData)elements.get(0).data;
	}
	public SMS_BearerData new_sms_BearerData(){
		return new SMS_BearerData();
	}

	public SMS_TeleserviceIdentifier get_sms_TeleserviceIdentifier(){
		return (SMS_TeleserviceIdentifier)elements.get(1).data;
	}
	public SMS_TeleserviceIdentifier new_sms_TeleserviceIdentifier(){
		return new SMS_TeleserviceIdentifier();
	}

	public ActionCode get_actionCode(){
		return (ActionCode)elements.get(2).data;
	}
	public ActionCode new_actionCode(){
		return new ActionCode();
	}

	public CDMAServingOneWayDelay2 get_cdmaServingOneWayDelay2(){
		return (CDMAServingOneWayDelay2)elements.get(3).data;
	}
	public CDMAServingOneWayDelay2 new_cdmaServingOneWayDelay2(){
		return new CDMAServingOneWayDelay2();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(4).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MEID get_meid(){
		return (MEID)elements.get(5).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public InterMessageTime get_interMessageTime(){
		return (InterMessageTime)elements.get(6).data;
	}
	public InterMessageTime new_interMessageTime(){
		return new InterMessageTime();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(7).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(8).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(9).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public NewlyAssignedIMSI get_newlyAssignedIMSI(){
		return (NewlyAssignedIMSI)elements.get(10).data;
	}
	public NewlyAssignedIMSI new_newlyAssignedIMSI(){
		return new NewlyAssignedIMSI();
	}

	public NewlyAssignedMIN get_newlyAssignedMIN(){
		return (NewlyAssignedMIN)elements.get(11).data;
	}
	public NewlyAssignedMIN new_newlyAssignedMIN(){
		return new NewlyAssignedMIN();
	}

	public NewMINExtension get_newMINExtension(){
		return (NewMINExtension)elements.get(12).data;
	}
	public NewMINExtension new_newMINExtension(){
		return new NewMINExtension();
	}

	public ServiceIndicator get_serviceIndicator(){
		return (ServiceIndicator)elements.get(13).data;
	}
	public ServiceIndicator new_serviceIndicator(){
		return new ServiceIndicator();
	}

	public SMS_ChargeIndicator get_sms_ChargeIndicator(){
		return (SMS_ChargeIndicator)elements.get(14).data;
	}
	public SMS_ChargeIndicator new_sms_ChargeIndicator(){
		return new SMS_ChargeIndicator();
	}

	public SMS_DestinationAddress get_sms_DestinationAddress(){
		return (SMS_DestinationAddress)elements.get(15).data;
	}
	public SMS_DestinationAddress new_sms_DestinationAddress(){
		return new SMS_DestinationAddress();
	}

	public SMS_MessageCount get_sms_MessageCount(){
		return (SMS_MessageCount)elements.get(16).data;
	}
	public SMS_MessageCount new_sms_MessageCount(){
		return new SMS_MessageCount();
	}

	public SMS_NotificationIndicator get_sms_NotificationIndicator(){
		return (SMS_NotificationIndicator)elements.get(17).data;
	}
	public SMS_NotificationIndicator new_sms_NotificationIndicator(){
		return new SMS_NotificationIndicator();
	}

	public SMS_OriginalDestinationAddress get_sms_OriginalDestinationAddress(){
		return (SMS_OriginalDestinationAddress)elements.get(18).data;
	}
	public SMS_OriginalDestinationAddress new_sms_OriginalDestinationAddress(){
		return new SMS_OriginalDestinationAddress();
	}

	public SMS_OriginalDestinationSubaddress get_sms_OriginalDestinationSubaddress(){
		return (SMS_OriginalDestinationSubaddress)elements.get(19).data;
	}
	public SMS_OriginalDestinationSubaddress new_sms_OriginalDestinationSubaddress(){
		return new SMS_OriginalDestinationSubaddress();
	}

	public SMS_OriginalOriginatingAddress get_sms_OriginalOriginatingAddress(){
		return (SMS_OriginalOriginatingAddress)elements.get(20).data;
	}
	public SMS_OriginalOriginatingAddress new_sms_OriginalOriginatingAddress(){
		return new SMS_OriginalOriginatingAddress();
	}

	public SMS_OriginalOriginatingSubaddress get_sms_OriginalOriginatingSubaddress(){
		return (SMS_OriginalOriginatingSubaddress)elements.get(21).data;
	}
	public SMS_OriginalOriginatingSubaddress new_sms_OriginalOriginatingSubaddress(){
		return new SMS_OriginalOriginatingSubaddress();
	}

	public SMS_OriginatingAddress get_sms_OriginatingAddress(){
		return (SMS_OriginatingAddress)elements.get(22).data;
	}
	public SMS_OriginatingAddress new_sms_OriginatingAddress(){
		return new SMS_OriginatingAddress();
	}

	public Teleservice_Priority get_teleservice_Priority(){
		return (Teleservice_Priority)elements.get(23).data;
	}
	public Teleservice_Priority new_teleservice_Priority(){
		return new Teleservice_Priority();
	}

	public TemporaryReferenceNumber get_temporaryReferenceNumber(){
		return (TemporaryReferenceNumber)elements.get(24).data;
	}
	public TemporaryReferenceNumber new_temporaryReferenceNumber(){
		return new TemporaryReferenceNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(105, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sms_BearerData(); }});
		elements.add(new ElementDescriptor(116, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sms_TeleserviceIdentifier(); }});
		elements.add(new ElementDescriptor(128, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_actionCode(); }});
		elements.add(new ElementDescriptor(347, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServingOneWayDelay2(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(325, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_interMessageTime(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(287, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_newlyAssignedIMSI(); }});
		elements.add(new ElementDescriptor(187, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_newlyAssignedMIN(); }});
		elements.add(new ElementDescriptor(328, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_newMINExtension(); }});
		elements.add(new ElementDescriptor(193, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceIndicator(); }});
		elements.add(new ElementDescriptor(106, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_ChargeIndicator(); }});
		elements.add(new ElementDescriptor(107, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_DestinationAddress(); }});
		elements.add(new ElementDescriptor(108, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_MessageCount(); }});
		elements.add(new ElementDescriptor(109, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_NotificationIndicator(); }});
		elements.add(new ElementDescriptor(110, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalDestinationAddress(); }});
		elements.add(new ElementDescriptor(111, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalDestinationSubaddress(); }});
		elements.add(new ElementDescriptor(112, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalOriginatingAddress(); }});
		elements.add(new ElementDescriptor(113, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalOriginatingSubaddress(); }});
		elements.add(new ElementDescriptor(114, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginatingAddress(); }});
		elements.add(new ElementDescriptor(290, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_teleservice_Priority(); }});
		elements.add(new ElementDescriptor(195, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_temporaryReferenceNumber(); }});
	}
	public SMSDeliveryPointToPoint(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
