package net.isup.parameters.csi;

import java.util.HashMap;

public enum CallProcessingStateType {
	CIRCUIT_INCOMING_BUSY(0x04),
	CIRCUIT_OUTGOING_BUSY(0x08),
	IDLE(0x0c);
	
	private int id;
	private static final HashMap<Integer, CallProcessingStateType> lookup = new HashMap<Integer, CallProcessingStateType>();
	static{
		for(CallProcessingStateType td :CallProcessingStateType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CallProcessingStateType get(int id){ return lookup.get(id); }
	private CallProcessingStateType(int _id){ id = _id; }			
}
