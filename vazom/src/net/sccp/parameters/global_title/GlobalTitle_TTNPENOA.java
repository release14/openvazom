package net.sccp.parameters.global_title;

import java.util.ArrayList;
import java.util.Arrays;

import net.sccp.NatureOfAddress;


public class GlobalTitle_TTNPENOA extends GlobalTitleBase {
	public int translationType;
	public NumberingPlan numberingPlan;
	public EncodingScheme encodingScheme;
	public NatureOfAddress natureOfAddress;
	public byte[] addressInformation;
	
	public GlobalTitle_TTNPENOA(){
		super();
		type = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
	}
	
	public byte[] encode(){
		byte[] res = null;
		int tmp;
		ArrayList<Byte> packet = new ArrayList<Byte>();
		packet.add((byte)translationType);
		
		tmp = numberingPlan.getId() + encodingScheme.getId();
		packet.add((byte)tmp);
		packet.add((byte)natureOfAddress.getId());
		for(int i = 0; i<addressInformation.length; i++) packet.add(addressInformation[i]);
		
		
		res = new byte[packet.size()];
		for(int i = 0; i<packet.size(); i++) res[i] = packet.get(i);
		return res;
	}
	
	
	
	public void init(byte[] data) {
		translationType = data[0] & 0xFF;
		numberingPlan = NumberingPlan.get(data[1] & 0xF0);
		encodingScheme = EncodingScheme.get(data[1] & 0x0F);
		natureOfAddress = NatureOfAddress.get(data[2] & 0xFF);
		addressInformation = Arrays.copyOfRange(data, 3, data.length);
		

	}

}
