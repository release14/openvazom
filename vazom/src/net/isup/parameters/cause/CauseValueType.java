package net.isup.parameters.cause;

import java.util.HashMap;


public enum CauseValueType {
	UNALLOCATED_NUMBER(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x01),
	NO_ROUTE_TO_SPECIFIED_TRANSIT_NETWORK(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x02),
	NO_ROUTE_TO_DESTINATION(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x03),
	SEND_SPECIAL_INFORMATION_TONE(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x04),
	MISDIALLED_TRUNK_PREFIX(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x05),
	CHANNEL_UNACCEPTABLE(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x06),
	CALL_AWARDED_BEING_DELIVERED(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x07),
	PREEMPTION(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x08),
	PREEMPTION_CIRCUIT_RESERVER(CauseValueClassType.NORMAL_EVENT_1.getId(), 0x09),
	
	NORMAL_CALL_CLEARING(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x00),
	USER_BUSY(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x01),
	NO_USER_RESPONDING(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x02),
	NO_ANSWER(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x03),
	SUBSCRIBER_ABSENT(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x04),
	CALL_REJECTED(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x05),
	NUMBER_CHANGED(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x06),
	REDIRECTION(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x07),
	EXCHANGE_ROUTING_ERROR(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x09),
	NON_SELECTED_USER_CLEARIG(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x0a),
	DESTINATION_OUT_OF_ORDER(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x0b),
	INVALID_NUMBER_FORMAT(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x0c),
	FACILITY_REJECTED(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x0d),
	RESPONSE_TO_STATUS_ENQUIRY(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x0e),
	NORMAL_UNSPECIFIED(CauseValueClassType.NORMAL_EVENT_2.getId(), 0x0f),
	
	CIRCUIT_OR_CHANNEL_UNAVAILABLE(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x02),
	NETWORK_OUT_OF_ORDER(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x06),
	PERMANENT_FRAME_MODE_CONNECTION_OUT_OF_SERVICE(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x07),
	PERMANENT_FRAME_MODE_CONNECTION_OPERATIONAL(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x08),
	TEMPORARY_FAILURE(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x09),
	SWITCHING_EQUIPMENT_CONGESTION(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x0a),
	ACCESS_INFORMATION_DISCARDED(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x0b),
	REQUESTED_CIRCUIT_OR_CHANNEL_UNAVAILABLE(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x0c),
	PRECEDENCE_CALL_BLOCKED(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x0e),
	RESOURCE_UNAVAILABLE_UNSPECIFIED(CauseValueClassType.RESOURCE_UNAVAILBLE.getId(), 0x0f),
	
	QUALITY_OF_SERVICE_UNAVAILABLE(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x01),
	EWQUESTED_FACILITY_NOT_SUBSCRIBED(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x02),
	OUTGOING_CALLS_BARRED(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x05),
	INCOMING_CALLS_BARRED(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x07),
	BEARER_CAPABILITY_NOT_AUTHORIZED(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x09),
	BEARER_CAPABILITY_UNAVAILABLE(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x0a),
	INCONSISTENCY_IN_DESIGNATED_OUTGOING_ACCESS_INFORMATION_AND_SUBSCRIBER_CLASS(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x0e),
	SERVICE_OR_OPTION_UNAVAILABLE(CauseValueClassType.SERVICE_OR_OPTION_UNAVAILABLE.getId(), 0x0f),
	
	BEARER_CAPABILITY_NOT_IMPLEMENTED(CauseValueClassType.SERVICE_OR_OPTION_NOT_IMPLEMENTED.getId(), 0x01),
	CHANNEL_TYPE_NOT_IMPLEMENTED(CauseValueClassType.SERVICE_OR_OPTION_NOT_IMPLEMENTED.getId(), 0x02),
	REQUESTED_FACILITY_NOT_IMPLEMENTED(CauseValueClassType.SERVICE_OR_OPTION_NOT_IMPLEMENTED.getId(), 0x05),
	RESTRICTED_DIGITAL_INFORMATION_BEARED_CAPABILITY(CauseValueClassType.SERVICE_OR_OPTION_NOT_IMPLEMENTED.getId(), 0x06),
	SERVICE_OR_OPTION_NOT_IMPLEMENTED(CauseValueClassType.SERVICE_OR_OPTION_NOT_IMPLEMENTED.getId(), 0x0f),
	
	INVALID_CALL_REFERENCE(CauseValueClassType.INVALID_MESSAGE.getId(), 0x01),
	CHANNEl_DOES_NOT_EXIST(CauseValueClassType.INVALID_MESSAGE.getId(), 0x02),
	CALL_IDENTITY_DOES_NOT_EXIST(CauseValueClassType.INVALID_MESSAGE.getId(), 0x03),
	CALL_IDENTITY_IN_USE(CauseValueClassType.INVALID_MESSAGE.getId(), 0x04),
	NO_CALL_SUSPENDED(CauseValueClassType.INVALID_MESSAGE.getId(), 0x05),
	CALL_CLEARED_FOR_REQUESTED_CALL_IDENTITY(CauseValueClassType.INVALID_MESSAGE.getId(), 0x06),
	USER_NOT_MEMBER_OF_CUG(CauseValueClassType.INVALID_MESSAGE.getId(), 0x07),
	IMCOMPATIBLE_DESTINATION(CauseValueClassType.INVALID_MESSAGE.getId(), 0x08),
	NON_EXISTENT_CUG(CauseValueClassType.INVALID_MESSAGE.getId(), 0x0a),
	INVALID_TRANSIT_NETWORK_SELECTION(CauseValueClassType.INVALID_MESSAGE.getId(), 0x0b),
	INVALID_MESSAGE_UNSPECIFIED(CauseValueClassType.INVALID_MESSAGE.getId(), 0x0f),
	
	MANDATORY_ELEMENT_MISSING(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x00),
	MESSAGE_TYPE_NON_EXISTENT(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x01),
	MESSAGE_NOT_COMPATIBLE(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x02),
	PARAMETER_NON_EXISTENT(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x03),
	INVALID_INFORMATION_ELEMENT_CONTENTS(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x04),
	MESSAGE_NOT_COMATIBLE_WITH_CALL_STATE(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x05),
	RECOVERY_ON_TIMER_EXPIRY(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x06),
	PARAMETER_NON_EXISTENT_PASSED_ON(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x07),
	MESSAGE_WITH_UNRECOGNIZED_PARAMETER_DISCARDED(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x0e),
	PROTOCOL_ERROR_UNSPECIFIED(CauseValueClassType.PROTOCOL_ERROR.getId(), 0x0f),
	
	INTERWORKING_UNSPECIFED(CauseValueClassType.INTERWORKING.getId(), 0x00);

	
	
	
	
	private int id;
	private int class_id;
	private static final HashMap<String, CauseValueType> lookup = new HashMap<String, CauseValueType>();
	static{
		for(CauseValueType td : CauseValueType.values()){
			lookup.put(td.class_id + ":" + td.id, td);
		}
	}
	public int getId(){ return id; }
	public int getClassId(){ return class_id; }
	public static CauseValueType get(int id, int _class_id){ return lookup.get(_class_id + ":" + id); }
	private CauseValueType(int _class_id, int _id){ id = _id; class_id = _class_id; }		
}
