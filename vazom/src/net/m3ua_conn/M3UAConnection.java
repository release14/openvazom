package net.m3ua_conn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.config.SGNConfigData;
import net.hlr.HLRManager;
import net.hlr.HLRPacket;
import net.hlr.HLR_REQ_CALLBACK;
import net.logging.LoggingManager;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.ASPACACK;
import net.m3ua.messages.ASPDNACK;
import net.m3ua.messages.ASPIAACK;
import net.m3ua.messages.ASPUPACK;
import net.m3ua.messages.BEAT;
import net.m3ua.messages.BEATACK;
import net.m3ua.messages.DATA;
import net.m3ua.messages.DAUD;
import net.m3ua.messages.DAVA;
import net.m3ua.messages.DUNA;
import net.m3ua.messages.ERR;
import net.m3ua.messages.NTFY;
import net.m3ua.messages.SCON;
import net.m3ua.parameters.AffetedPointCode;
import net.m3ua.parameters.ErrorCode;
import net.m3ua.parameters.HeartbeatData;
import net.m3ua.parameters.NetworkAppearance;
import net.m3ua.parameters.RoutingContext;
import net.m3ua.parameters.Status;
import net.m3ua.parameters.TrafficModeType;
import net.m3ua.parameters.errorcode.ErrorCodeType;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.m3ua.parameters.status.StatusInfoType;
import net.m3ua.parameters.status.StatusType;
import net.m3ua.parameters.tmt.TMTType;
import net.mtp3_conn.MTP3Payload;
import net.routing.CallbackDescriptor;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionDirectionType;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionWorkerMethod;
import net.routing.RoutingConnectionType;
import net.sccp.MessageType;
import net.sccp.NatureOfAddress;
import net.sccp.RoutingIndicator;
import net.sccp.SCCP;
import net.sccp.messages.UDT_UnitData;
import net.sccp.parameters.global_title.EncodingScheme;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.GlobalTitle_TTNPENOA;
import net.sccp.parameters.global_title.NumberingPlan;
import net.sccp.parameters.protocol_class.MessageHandling;
import net.sccp.parameters.protocol_class.ProtocolClassType;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.security.SecurityManager;
import net.smpp_conn.SMPPPayload;
import net.smstpdu.TBCD;
import net.smstpdu.tpdu.TPDU;
import net.utils.Utils;
import org.apache.log4j.Logger;

public class M3UAConnection extends RoutingConnection {
	public static Logger logger = Logger.getLogger(M3UAConnection.class);

	public int sctp_id;
	public boolean sctp_active;
	public int server_sctp_id = -1;
	public String label;
	public String local_ip;
	public int local_port;
	public String remote_ip;
	public int remote_port;
	public int dpc;
	public int opc;
	public int network_app;
	public int routing_ctx;
	public int stream_count;
	public int beat_interval;
	public int max_beat_miss_count;

	private Data_r datar_r;
	private Thread datar_t;
	private Data_w dataw_r;
	private Thread dataw_t;
	private Worker worker_r;
	private Thread worker_t;
	private Beat beat_r;
	private Thread beat_t;
	private CallbackTimeout callback_timeout_r;
	private DaudTimer daud_timer_r;
	private Thread daud_timer_t;
	private boolean daud_active;
	private int daud_sctp_sid;
	// private int daud_pc;
	// private int daud_rc;
	// private int daud_app;
	private SCON daud_scon;
	private Thread callback_timeout_t;

	// public boolean beat_sent;
	// public boolean beat_received;
	public byte[] beat_data;
	public int beat_counter = 0;

	private ConcurrentLinkedQueue<SSCTPDescriptor> in_queue;
	private ConcurrentLinkedQueue<RoutingConnectionPayload> out_queue;

	private ConcurrentHashMap<Long, CallbackDescriptor> callback_lst;

	private synchronized void setBeatCounter(int counter) {
		beat_counter = counter;
	}

	private synchronized int getBeatCounter() {
		return beat_counter;
	}

	public void start_server_listener() {

		ServerListener sl = new ServerListener();
		Thread th = new Thread(sl, "M3UA_SERVER_LISTENER");
		LoggingManager.debug(SGNConfigData.m3ua_debug, logger,
				"Starting M3UA SERVER connection listener!");
		th.start();
	}

	private class DaudTimer implements Runnable {

		public void run() {
			DAUD daud = null;
			M3UAPacket mp_new = null;
			SSCTPDescriptor sd = null;
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while (!stopping && daud_active) {
				try {
					Thread.sleep(SGNConfigData.m3ua_daud_interval);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (daud_active) {
					if (daud_scon != null) {
						mp_new = M3UA.prepareNew(M3UAMessageType.DAUD);
						daud = (DAUD) mp_new.message;
						if (daud_scon.affectedPointCode != null) daud.affectedPointCode = daud_scon.affectedPointCode;
						if (daud_scon.routingContext != null) daud.routingContext = daud_scon.routingContext;
						if (daud_scon.networkAppearance != null) daud.networkAppearance = daud_scon.networkAppearance;
						sd = new SSCTPDescriptor(daud_sctp_sid, mp_new.encode());
						SSctp.sendM3ua(sctp_id, sd.payload, daud_sctp_sid);
					}

				}
			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Endistartng...");

		}

	}

	private class ServerListener implements Runnable {
		public void run() {
			boolean ready = false;
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while (!ready && (server_sctp_id > -1)) {
				if (sctp_id <= 0) {
					sctp_id = SSctp.clientAccept(server_sctp_id);
					if (sctp_id > -1) {
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Accepting SCTP client id = [" + sctp_id + "]");
						// m3ua connect
						if (M3UAConnManager.init_m3ua_server(sctp_id, routing_ctx)) {
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA layer connected,  SCTP client id = [" + sctp_id + "]");
							// connection ready, stop listener, start connection
							// threads
							start();
							ready = true;

						} else
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Error while connecting M3UA layer, SCTP client id = [" + sctp_id + "]");
					}
				} else {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");

		}
	}

	private class CallbackTimeout implements Runnable {
		public void run() {
			CallbackDescriptor cd = null;
			long l;
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while (!stopping) {
				try {
					Thread.sleep(SGNConfigData.m3ua_smsfs_hlr_callback_check_interval);
				} catch (Exception e) {
					e.printStackTrace();
				}
				l = System.currentTimeMillis();
				for (Long i : callback_lst.keySet()) {
					cd = callback_lst.get(i);
					if (l - cd.ts > SGNConfigData.m3ua_smsfs_hlr_callback_timeout) {
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UAConnection callback timeout reached for CALLBACK id = [" + i + "]!");
						callback_lst.remove(i);
					}
				}
			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");

		}

	}

	private class Beat implements Runnable {
		public void run() {
			SSCTPDescriptor sd = null;
			M3UAPacket mp = null;
			BEAT beat = null;
			int res;
			// BEATACK beat_ack = null;
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while (!stopping) {
				try {
					Thread.sleep(beat_interval);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (!stopping) {
					mp = M3UA.prepareNew(M3UAMessageType.BEAT);
					beat = (BEAT) mp.message;
					beat.heartbeatData = new HeartbeatData();
					beat_data = Utils.md5("VAZOM M3UA HEARTBEAT");
					beat.heartbeatData.value = beat_data;
					sd = new SSCTPDescriptor(0, mp.encode());
					// send
					res = SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);
					if (res == -1) {
						// mark for reconnection
						stop();

					} else {
						setBeatCounter(getBeatCounter() + 1);
						if (getBeatCounter() > max_beat_miss_count) {
							LoggingManager.warn(logger, "M3UA HEARTBEAT: Missed [" + getBeatCounter() + "] beats, closing connection!");
							stop();
						}
					}

				}
			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");
		}
	}

	private class Worker implements Runnable {
		SSCTPDescriptor sd = null;

		public void run() {
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while (!stopping) {
				sd = in_queue.poll();
				if (sd != null) {
					if (sd.payload != null) {
						// custom method
						if (worker_method != null) worker_method.execute(sd);
					}
				} else {
					try {
						Thread.sleep(1);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");
		}
	}

	private class Data_r implements Runnable {
		SSCTPDescriptor sd = null;
		M3UAPacket mp = null;
		M3UAPacket mp_new = null;
		BEATACK beat_ack = null;
		BEAT beat = null;
		DUNA duna = null;
		DAVA dava = null;
		ASPDNACK aspdn_ack = null;
		ASPIAACK aspia_ack = null;
		ASPUPACK aspup_ack = null;
		ASPACACK aspac_ack = null;
		// SCON scon = null;
		ERR err = null;
		boolean aspup_received;
		NTFY ntfy = null;
		boolean callback_found;

		public void run() {
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while (!stopping) {
				sd = SSctp.receiveM3ua(sctp_id);
				if (sd != null) {
					if (sd.payload != null) {
						// statistics
						stats.IN++;
						stats.BYTES += sd.payload.length;

						mp = M3UA.decode(sd.payload);
						// SCON
						if (mp.messageType == M3UAMessageType.SCON) {
							daud_scon = (SCON) mp.message;
							if (daud_scon.congestionIndications != null) {
								// 1 - 3 are valid numbers
								if (daud_scon.congestionIndications.level > 0 && daud_scon.congestionIndications.level < 4) {
									if (!daud_active) {
										daud_active = true;
										daud_sctp_sid = sd.sid;
										active = false;
										daud_timer_r = new DaudTimer();
										daud_timer_t = new Thread(daud_timer_r, "DAUD_TIMER_" + id.toUpperCase());
										daud_timer_t.start();

									}
								} else {
									if (daud_scon.congestionIndications.level == 0) {
										if (daud_active) {
											daud_active = false;
											active = true;
										}
									} else {
										// ERR
										mp_new = M3UA.prepareNew(M3UAMessageType.ERR);
										err = (ERR) mp_new.message;
										err.errorCode = new ErrorCode();
										err.errorCode.setErrorCode(ErrorCodeType.INVALID_PARAMETER_VALUE.getId());
										err.affectedPointCode = new AffetedPointCode();
										err.affectedPointCode.setAffetedPointCode(opc);
										err.routingContext = new RoutingContext();
										err.routingContext.setRoutingContext(routing_ctx);
										sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
										SSctp.sendM3ua(sctp_id, sd.payload,	sd.sid);

									}
								}
							}

							// ASPAC
						} else if (mp.messageType == M3UAMessageType.ASPAC) {

							if (aspup_received) {
								// ASPAC ACK
								mp_new = M3UA.prepareNew(M3UAMessageType.ASPAC_ACK);
								aspac_ack = (ASPACACK) mp.message;
								aspac_ack.trafficModeType = new TrafficModeType(TMTType.LOADSHARE);
								aspac_ack.routingContext = new RoutingContext();
								aspac_ack.routingContext.setRoutingContext(routing_ctx);
								sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
								SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

								// shut down m3ua connection
								M3UAConnManager.noshut_rc(routing_ctx);

								// NTFY AS ACTIVE
								mp_new = M3UA.prepareNew(M3UAMessageType.NTFY);
								ntfy = (NTFY) mp_new.message;
								ntfy.status = new Status();
								ntfy.status.setStatus(StatusType.AS_STATE_CHANGE, StatusInfoType.AS_ACTIVE);
								sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
								SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

								aspup_received = false;
							} else {
								// ERR
								mp_new = M3UA.prepareNew(M3UAMessageType.ERR);
								err = (ERR) mp_new.message;
								err.errorCode = new ErrorCode();
								err.errorCode.setErrorCode(ErrorCodeType.UNEXPECTED_MESSAGE.getId());
								err.affectedPointCode = new AffetedPointCode();
								err.affectedPointCode.setAffetedPointCode(opc);
								err.routingContext = new RoutingContext();
								err.routingContext.setRoutingContext(routing_ctx);
								sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
								SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							}

							// ASPUP
						} else if (mp.messageType == M3UAMessageType.ASPUP) {
							aspup_received = true;

							// ASPUP ACK
							mp_new = M3UA.prepareNew(M3UAMessageType.ASPUP_ACK);
							aspup_ack = (ASPUPACK) mp_new.message;
							sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
							SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							// NTFY AS INACTIVE
							mp_new = M3UA.prepareNew(M3UAMessageType.NTFY);
							ntfy = (NTFY) mp_new.message;
							ntfy.status = new Status();
							ntfy.status.setStatus(StatusType.AS_STATE_CHANGE, StatusInfoType.AS_INACTIVE);
							sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
							SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							// ASPIA ACK
						} else if (mp.messageType == M3UAMessageType.ASPIA_ACK) {
							// shut down m3ua connection
							M3UAConnManager.shut_rc(routing_ctx);

							// ASPIA
						} else if (mp.messageType == M3UAMessageType.ASPIA) {
							// send ASPIA_ACK
							mp_new = M3UA.prepareNew(M3UAMessageType.ASPIA_ACK);
							aspia_ack = (ASPIAACK) mp_new.message;
							aspia_ack.routingContext = new RoutingContext();
							aspia_ack.routingContext.setRoutingContext(routing_ctx);
							sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
							SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							// shut down m3ua connection
							M3UAConnManager.shut_rc(routing_ctx);

							// send NTFY AS PENDING
							aspup_received = true;
							mp_new = M3UA.prepareNew(M3UAMessageType.NTFY);
							ntfy = (NTFY) mp_new.message;
							ntfy.status = new Status();
							ntfy.status.setStatus(StatusType.AS_STATE_CHANGE, StatusInfoType.AS_PENDING);
							sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
							SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							// ASPDN
						} else if (mp.messageType == M3UAMessageType.ASPDN) {
							// send ASPDN_ACK
							mp_new = M3UA.prepareNew(M3UAMessageType.ASPDN_ACK);
							aspdn_ack = (ASPDNACK) mp_new.message;
							sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
							SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							// shut down m3ua connection
							M3UAConnManager.shut_rc(routing_ctx);

							// ASPDN ACK
						} else if (mp.messageType == M3UAMessageType.ASPDN_ACK) {
							stop();

							// DAVA
						} else if (mp.message.type == M3UAMessageType.DAVA) {
							dava = (DAVA) mp.message;
							M3UAConnManager.process_DAVA((int)Utils.bytes2num(dava.affectedPointCode.value), (int)Utils.bytes2num(dava.routingContext.value));

							// DUNA
						} else if (mp.message.type == M3UAMessageType.DUNA) {
							duna = (DUNA) mp.message;
							M3UAConnManager.process_DUNA((int) Utils.bytes2num(duna.affectedPointCode.value), (int)Utils.bytes2num(duna.routingContext.value));
						}
						// HEARTBEAT
						else if (mp.message.type == M3UAMessageType.BEAT) {
							beat = (BEAT) mp.message;
							mp_new = M3UA.prepareNew(M3UAMessageType.BEAT_ACK);
							beat_ack = (BEATACK) mp_new.message;
							beat_ack.heartbeatData = new HeartbeatData();
							beat_ack.heartbeatData.value = beat.heartbeatData.value;
							sd = new SSCTPDescriptor(sd.sid, mp_new.encode());
							// send
							SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);

							// HEARTBEAT ACK
						} else if (mp.message.type == M3UAMessageType.BEAT_ACK) {
							beat_ack = (BEATACK) mp.message;
							if (Arrays.equals(beat_ack.heartbeatData.value, beat_data)) {
								setBeatCounter(0);
								// beat_received = true;
								// logger.debug("BEAT_ACK received!");
								// beat_sent = false;
								// beat_received = false;
							} else {
								LoggingManager.warn(logger, "Invalid HEARTBEAT data!");
								// mark for reconnection
								// stop();
							}
							// DATA
						} else if (mp.message.type == M3UAMessageType.DATA) {
							// check for SMSFS HLR REPLY
							HLRPacket hlrp = Utils.get_HRLP_REPLY(mp);
							// SMSFS HLR_REPLY
							if (hlrp != null) {
								CallbackDescriptor cd = callback_lst.get(hlrp.tcap_id);
								if (cd != null) {
									HLR_REQ_CALLBACK clb = (HLR_REQ_CALLBACK) cd.callback_method;
									if (clb != null) {
										//System.out.println("HLR CALLBACK!!");
										callback_found = true;
										// set callback params
										clb.setHLRP_REPLY(hlrp);
										// run callback
										clb.run();
										// remove callback
										callback_lst.remove(hlrp.tcap_id);
										// remove HLR correlation
										HLRManager.remove(SGNConfigData.hplmnr_hlr_gt_override + ":" +  SGNConfigData.hplmnr_smsrouter_gt + ":" + hlrp.tcap_id);
									} else
										logger.warn("Callback method not found, id = [" + hlrp.tcap_id + "]!");
								}
								// Other callback (SMPP->SS7)
							} else {
								long tmp_id = Utils.getTcap_did(Utils.get_TCAP(mp));
								if (tmp_id > -1) {
									CallbackDescriptor cd = callback_lst.get(tmp_id);
									if (cd != null) {
										callback_found = true;
										cd.callback_method.set_params(new Object[] { Utils.get_TCAP(mp) });
										cd.callback_method.run();
										// remove callback
										callback_lst.remove(cd.callback_method.get_callback_id());
									}
								}
							}
							// process if not callback
							if (!callback_found) {
								// if(FNManager.fn_connected){
								if (in_queue.size() < SGNConfigData.sgn_global_max_queue_size)
									in_queue.offer(sd);
								else
									IN_QUEUE_MAX++;
									//LoggingManager.warn(logger, "M3UAConnection.in_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
								// }
								// reset
							} else
								callback_found = false;

						}
					}
				} else {
					// mark for reconnection
					stop();
				}

			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");

		}

	}

	private class Data_w implements Runnable {
		SSCTPDescriptor sd = null;
		M3UAPayload mp = null;
		RoutingConnectionPayload rpl = null;
		M3UAConnection conn = null;
		M3UAPacket nm3uap = null;
		DATA nm3uad = null;
		int res;
		CallbackDescriptor cd = null;

		public void run() {
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger,
					"Starting...");
			while (!stopping) {
				// get payload, convert if necessary
				rpl = convert(out_queue.poll());
				// check if payload is valid
				if (rpl != null)
					mp = (M3UAPayload) rpl;
				else
					mp = null;
				// process if payload is valid
				if (mp != null) {
					// statistics
					stats.OUT++;
					stats.BYTES += mp.sd.payload.length;
					/*
					 * // callback set if(mp.callback != null){ cd = new
					 * CallbackDescriptor(); cd.ts = System.currentTimeMillis();
					 * cd.callback_method = mp.callback;
					 * callback_lst.put(mp.callback.get_callback_id(), cd); }
					 */
					// callback to be generated
					if (mp.callback_required) {
						cd = new CallbackDescriptor();
						cd.ts = System.currentTimeMillis();
						// used with SMPP->SS7
						if (rpl.extra_conversion_params != null) {
							mp.callback.set_callback_id((Long) rpl.extra_conversion_params.get(0));

						}
						cd.callback_method = mp.callback;
						callback_lst.put(cd.callback_method.get_callback_id(), cd);

					}
					// DAVA
					if (mp.m3ua_packet.message.type == M3UAMessageType.DAVA) {
						// send
						SSctp.sendM3ua(sctp_id, mp.sd.payload, mp.sd.sid);

						// DUNA
					} else if (mp.m3ua_packet.message.type == M3UAMessageType.DUNA) {
						// send
						SSctp.sendM3ua(sctp_id, mp.sd.payload, mp.sd.sid);
						// DATA
					} else if (mp.m3ua_packet.message.type == M3UAMessageType.DATA) {
						sd = mp.sd;

						// re-encode DPC/OPC, routing context, network app
						// M3UA
						if (mp.m3ua_packet != null) {
							nm3uap = M3UA.prepareNew(M3UAMessageType.DATA);
							// ndata.message
							nm3uad = (DATA) mp.m3ua_packet.message;
							nm3uap.message = nm3uad;
							nm3uad.protocolData.destinationPointCode = dpc;
							nm3uad.protocolData.originatingPointCode = opc;
							nm3uad.routingContext = new RoutingContext();
							nm3uad.routingContext
									.setRoutingContext(routing_ctx);
							nm3uad.networkAppearance = new NetworkAppearance();
							nm3uad.networkAppearance.setNetworkAppearance(network_app);
							sd.payload = nm3uap.encode();
						}

						// send
						res = SSctp.sendM3ua(sctp_id, sd.payload, sd.sid);
						// if failed
						if (res == -1) {
							// if using alternative connection
							if (mp.connections != null) {
								// nect connection in list
								mp.conn_counter++;
								// check if last one already used
								if (mp.conn_counter < mp.connections.size()) {
									// try connections, one by one
									for (int i = mp.conn_counter; i < mp.connections.size(); i++) {
										conn = M3UAConnManager.getConnection(mp.connections.get(i));
										if (conn != null) {
											if (conn.active) {
												conn.out_queue.offer(mp);
												mp.conn_counter = i;
												conn = null;
												break;
											} else
												LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection [" + mp.connections.get(mp.conn_counter) + "] is not active, trying next connection!");
										} else
											LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection ["+ mp.connections.get(mp.conn_counter) + "] is not available, trying next connection!");
									}
									if (conn == null)
										LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Cannot send packet, all connections failed!");
								} else
									LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Cannot send packet, all conections failed!");
							}
							// mark for reconnection
							stop();
						}
					}
				} else {
					try {
						Thread.sleep(1);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");

		}

	}

	public void attachWorkerMethod(RoutingConnectionWorkerMethod method) {
		worker_method = method;

	}

	public void detachWorkerMethod() {
		worker_method = null;
	}

	/*
	 * public void attachWorkerMethod(M3UAWorkerMethod method){ worker_method =
	 * method; }
	 */
	public void stop() {
		stopping = true;
		active = false;
		sctp_active = false;
		setBeatCounter(0);
		SSctp.shutdownClient(sctp_id);
		sctp_id = -1;
		if (direction == RoutingConnectionDirectionType.SERVER) {
			SSctp.shutdownServer(server_sctp_id);
			server_sctp_id = -1;
		}

		M3UAConnManager.notify_all_DUNA(label);

	}

	public void start() {
		if (!active || direction == RoutingConnectionDirectionType.SERVER) {
			in_queue.clear();
			out_queue.clear();

			active = true;
			sctp_active = true;
			stopping = false;
			//setBeatCounter(0);

			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Creating M3UA Connection Data Receiver thread: [ " + sctp_id + " ]!");
			datar_r = new Data_r();
			datar_t = new Thread(datar_r, "M3UA_DATA_RECEIVER_" + sctp_id);
			datar_t.start();

			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Creating M3UA Connection Data Sender thread: [ " + sctp_id + " ]!");
			dataw_r = new Data_w();
			dataw_t = new Thread(dataw_r, "M3UA_DATA_SENDER_" + sctp_id);
			dataw_t.start();

			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Creating M3UA Connection Data Worker thread: [ " + sctp_id + " ]!");
			worker_r = new Worker();
			worker_t = new Thread(worker_r, "M3UA_DATA_WORKER_" + sctp_id);
			worker_t.start();

			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Creating M3UA Connection HEARTBEAT thread: [ " + sctp_id + " ]!");
			beat_r = new Beat();
			beat_t = new Thread(beat_r, "M3UA_HEARTBEAT_" + sctp_id);
			beat_t.start();

			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Creating M3UA Connection CALLBACK TIMEOUT thread: [ " + sctp_id + " ]!");
			callback_timeout_r = new CallbackTimeout();
			callback_timeout_t = new Thread(callback_timeout_r, "M3UA_CALLBAC_TIMEOUT_" + sctp_id);
			callback_timeout_t.start();

			// stats
			stats.reset();
		}

	}

	public M3UAConnection(int _sctp_id) {
		type = RoutingConnectionType.M3UA;
		direction = RoutingConnectionDirectionType.CLIENT;
		sctp_id = _sctp_id;
		in_queue = new ConcurrentLinkedQueue<SSCTPDescriptor>();
		out_queue = new ConcurrentLinkedQueue<RoutingConnectionPayload>();
		callback_lst = new ConcurrentHashMap<Long, CallbackDescriptor>();

	}

	public M3UAConnection(int _sctp_id,
			RoutingConnectionDirectionType _direction) {
		type = RoutingConnectionType.M3UA;
		direction = _direction;
		sctp_id = _sctp_id;
		in_queue = new ConcurrentLinkedQueue<SSCTPDescriptor>();
		out_queue = new ConcurrentLinkedQueue<RoutingConnectionPayload>();
		callback_lst = new ConcurrentHashMap<Long, CallbackDescriptor>();

	}

	public void out_offer(RoutingConnectionPayload data) {
		if(out_queue.size() < SGNConfigData.sgn_global_max_queue_size) out_queue.offer(data);
		else{
			OUT_QUEUE_MAX++;
		}
	}

	public RoutingConnectionPayload convert(RoutingConnectionPayload input) {
		RoutingConnectionPayload res = null;
		MTP3Payload mtp3_pl = null;
		M3UAPacket m3ua_packet = null;
		DATA m3ua_data = null;
		SMPPPayload smpp_pl = null;
		TPDU tpdu = null;
		byte[] tmp = null;
		byte[] sccp_data = null;
		if (input != null) {
			switch (input.type) {
				case M3UA:
					res = input;
					break;
				case MTP3:
					// Feature check for FEATURE_PDU_CONVERT
					//if (SecurityManager.check_feature(SecurityManager.FEATURE_PDU_CONVERT)) {
						mtp3_pl = (MTP3Payload) input;
						m3ua_packet = Utils.convert_MTP3_M3UA(mtp3_pl.mtp3_packet);
						res = new M3UAPayload();
						res.setParams(new Object[] { new SSCTPDescriptor(0, m3ua_packet.encode()), m3ua_packet });

					//}
					break;
				case SMPP:
					// Feature check for FEATURE_PDU_CONVERT
					//if (SecurityManager.check_feature(SecurityManager.FEATURE_PDU_CONVERT)) {
						smpp_pl = (SMPPPayload) input;
						tpdu = Utils.convert_SMPP_TPDU_reverse(smpp_pl.smpp_packet);
						if (tpdu != null) {
							long callback_tcap_id = HLRManager.consume_TCAP_ID();
							//tmp = Utils.generate_MO_TCAP_MAP("0.4.0.0.1.0.21.3", callback_tcap_id, "1234", "5678", 1, tpdu.encode());
							tmp = Utils.generate_MT_TCAP_MAP("0.4.0.0.1.0.21.3", callback_tcap_id, 1, "1234", "5678", tpdu.encode());
							m3ua_packet = M3UA.prepareNew(M3UAMessageType.DATA);
							m3ua_data = (DATA) m3ua_packet.message;
							m3ua_data.protocolData.serviceIndicator = ServiceIndicatorType.SCCP;

							// encode SCCP
							UDT_UnitData ns = (UDT_UnitData) SCCP
									.prepareNew(MessageType.UDT_UNITDATA);
							ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
							ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
							// Called party
							ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
							ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
							ns.calledPartyAddress.SSNIndicator = true;
							ns.calledPartyAddress.pointCodeIndicator = false;
							ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
							ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
							// Called GT
							GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA) ns.calledPartyAddress.globalTitle;
							ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
							ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
							ngt.encodingScheme = (SGNConfigData.hplmnr_hlr_gt_override.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
							ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
							ngt.addressInformation = TBCD.encode(SGNConfigData.hplmnr_hlr_gt_override);
							// Calling party
							ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
							ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
							ns.callingPartyAddress.SSNIndicator = true;
							ns.callingPartyAddress.pointCodeIndicator = false;
							ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
							ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
							// Calling GT
							GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA) ns.callingPartyAddress.globalTitle;
							ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
							ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
							ngt2.encodingScheme = (SGNConfigData.hplmnr_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
							ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
							ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
							ns.data = tmp;
							sccp_data = ns.encode();
							// m3ua protocoldata user data
							m3ua_data.protocolData.userProtocolData = sccp_data;

							// payload
							res = new M3UAPayload();
							res.setParams(new Object[] {new SSCTPDescriptor(1, m3ua_packet.encode()), m3ua_packet });
							res.callback_required = input.callback_required;
							res.extra_params = input.extra_params;
							res.callback = input.callback;
							res.extra_conversion_params = new ArrayList<Object>();
							res.extra_conversion_params.add(callback_tcap_id);

						}
					//}
					break;
			}
			/*
			 * if(input.type == RoutingConnectionType.M3UA) res = input; else
			 * if(input.type == RoutingConnectionType.MTP3){ mtp3_pl =
			 * (MTP3Payload)input; m3ua_packet =
			 * Utils.convert_MTP3_M3UA(mtp3_pl.mtp3_packet); res = new
			 * M3UAPayload(); res.setParams(new Object[]{new SSCTPDescriptor(0,
			 * m3ua_packet.encode()), m3ua_packet}); }else if(input.type ==
			 * RoutingConnectionType.SMPP){ // TODO }
			 */

		}
		return res;
	}

}
