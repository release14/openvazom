package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Rejectproblem extends CHOICE{

	public GeneralProblem get_generalProble(){
		return (GeneralProblem)elements.get(0).data;
	}
	public GeneralProblem new_generalProble(){
		return new GeneralProblem();
	}

	public InvokeProblem get_invokeProblem(){
		return (InvokeProblem)elements.get(1).data;
	}
	public InvokeProblem new_invokeProblem(){
		return new InvokeProblem();
	}

	public ReturnResultProblem get_returnResultProblem(){
		return (ReturnResultProblem)elements.get(2).data;
	}
	public ReturnResultProblem new_returnResultProblem(){
		return new ReturnResultProblem();
	}

	public ReturnErrorProblem get_returnErrorProblem(){
		return (ReturnErrorProblem)elements.get(3).data;
	}
	public ReturnErrorProblem new_returnErrorProblem(){
		return new ReturnErrorProblem();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_generalProble(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_invokeProblem(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_returnResultProblem(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_returnErrorProblem(); }});
	}
	public Rejectproblem(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
