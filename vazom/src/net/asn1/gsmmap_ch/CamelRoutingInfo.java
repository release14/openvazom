package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CamelRoutingInfo extends SEQUENCE{

	public ForwardingData get_forwardingData(){
		return (ForwardingData)elements.get(0).data;
	}
	public ForwardingData new_forwardingData(){
		return new ForwardingData();
	}

	public GmscCamelSubscriptionInfo get_gmscCamelSubscriptionInfo(){
		return (GmscCamelSubscriptionInfo)elements.get(1).data;
	}
	public GmscCamelSubscriptionInfo new_gmscCamelSubscriptionInfo(){
		return new GmscCamelSubscriptionInfo();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_forwardingData(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_gmscCamelSubscriptionInfo(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public CamelRoutingInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
