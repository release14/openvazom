package net.smsfs;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.apache.log4j.Logger;

import antlr.CommonAST;




public class Main {
	private static Logger logger=Logger.getLogger(Main.class);
	

	public static void printTree(Tree t, int indent){
		if ( t != null ) {
			StringBuffer sb = new StringBuffer(indent);
			for ( int i = 0; i < indent; i++ )
				sb = sb.append("   ");
			for ( int i = 0; i < t.getChildCount(); i++ ) {
				System.out.println(sb.toString() + t.getChild(i).toString());
				printTree((CommonTree)t.getChild(i), indent+1);
			}
		}

		
	}
	
	public static void main(String[] args) {

        try {
            
        	SmsFilterScriptLexer lex = new SmsFilterScriptLexer(new ANTLRFileStream("/home/dfranusic/flood.smsfs", "UTF8"));
            CommonTokenStream tokens = new CommonTokenStream(lex);
            SmsFilterScriptParser g = new SmsFilterScriptParser(tokens);
            
            //  g.setTreeAdaptor(adaptor);
           
            SmsFilterScriptParser.input_return ft = g.input();
            g.input();
            //System.out.println(g.getNumberOfSyntaxErrors());
            Tree tree = (CommonTree)ft.getTree();
            
           // System.out.println(tree.getChild(0).toString());
            //System.out.println(((Tree)tree).toStringTree());
            //printTree(tree, 0);
            Tree mo = Smsfs.getRules(tree, FilterType.MO);
            //System.out.println(mo);
            //printTree(mo, 0);
            FilterDescriptor fd = Smsfs.processFilter(mo, FilterType.MO);
            //ModifierDescriptor md = (ModifierDescriptor)fd.rule_mod_lst.get(0);
            
            //System.out.println(md.extra_params.get("HLR_MSISDN"));
            //System.out.println(md.extra_params.get("HLR_SCA"));
            
            
            //System.out.println(Smsfs.executeRegex("^(...).*", "bla_ffff"));
            //System.out.println(fd.filterType);
            
          //  System.out.println(((Tree)ft.tree).toStringTree());
        // logger.debug("AAAASASsdsdsdldsjdskl");   
        } catch (Exception e) {
        	//logger.error(e.getStackTrace().toString());
            e.printStackTrace();
        }
		
	}

}
