package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAConnectionReferenceInformation extends SEQUENCE{

	public CDMAConnectionReference get_cdmaConnectionReference(){
		return (CDMAConnectionReference)elements.get(0).data;
	}
	public CDMAConnectionReference new_cdmaConnectionReference(){
		return new CDMAConnectionReference();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(1).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public CDMAState get_cdmaState(){
		return (CDMAState)elements.get(2).data;
	}
	public CDMAState new_cdmaState(){
		return new CDMAState();
	}

	public DataPrivacyParameters get_dataPrivacyParameters(){
		return (DataPrivacyParameters)elements.get(3).data;
	}
	public DataPrivacyParameters new_dataPrivacyParameters(){
		return new DataPrivacyParameters();
	}

	public CDMAServiceOptionConnectionIdentifier get_cdmaServiceOptionConnectionIdentifier(){
		return (CDMAServiceOptionConnectionIdentifier)elements.get(4).data;
	}
	public CDMAServiceOptionConnectionIdentifier new_cdmaServiceOptionConnectionIdentifier(){
		return new CDMAServiceOptionConnectionIdentifier();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(208, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaConnectionReference(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(213, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaState(); }});
		elements.add(new ElementDescriptor(216, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataPrivacyParameters(); }});
		elements.add(new ElementDescriptor(361, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionConnectionIdentifier(); }});
	}
	public CDMAConnectionReferenceInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
