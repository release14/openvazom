package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;
import net.smpp.parameters.MessageStateType;

public class Message_state extends OptionalParameterBase {

	public Message_state(){
		type = OptionalParameterType.MESSAGE_STATE;
	}
	public MessageStateType value;
	public void init(byte[] data) {
		value = MessageStateType.get(data[0] & 0xff);
	}

}
