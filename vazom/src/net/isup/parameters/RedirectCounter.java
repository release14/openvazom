package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectCounter extends ParameterBase {
	public RedirectCounter(){
		type = ParameterType.REDIRECT_COUNTER;
	}
}
