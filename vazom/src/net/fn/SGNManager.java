package net.fn;

import java.util.concurrent.ConcurrentHashMap;

import net.config.FNConfigData;
import net.ds.DataSourceType;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.smsfs.FilterResult;
import net.smsfs.FilterType;
import net.smsfs.ModeOfOperation;
import net.smsfs.SmsfsManager;
import net.smsfs.SmsfsPacket;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.HeaderDescriptor;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class SGNManager {
	public static Logger logger=Logger.getLogger(SGNManager.class);
	public static ConcurrentHashMap<String, SGDescriptor> sgn_map;
	public static boolean sgn_connected = false;
	private static SGReconnect sgr_r;
	private static Thread sgr_t;
	private static boolean stopping;
	
	private static class SGReconnect implements Runnable{
		public void run() {
			SGDescriptor sgd = null;
			logger.info("Starting...");
			while(!stopping){
				try{ Thread.sleep(FNConfigData.sg_reconnect_interval); }catch(Exception e){ e.printStackTrace(); }
				for(String s: sgn_map.keySet()){
					sgd = sgn_map.get(s);
					if(!sgd.active){
						logger.info("Reactivating signaling node [" + sgd.id + "], address = [" + sgd.address + "], port = [" + sgd.port + "]!");
						connectNode(sgd.id);
					}
				}
				

			}
			logger.info("Ending...");
				
		}
	}
	public static MessageDescriptor prepareFilter_result(FilterResult fres,  HeaderDescriptor vstp_hdr){
		MessageDescriptor res = null;
		String tmp = "";
		//if(fres != null){
		res = new MessageDescriptor();
		// set header
		res.header.destination = LocationType.SGN;
		res.header.ds = vstp_hdr.ds;
		res.header.msg_id = vstp_hdr.msg_id;
		res.header.msg_type = vstp_hdr.msg_type;
		res.header.source = LocationType.FN;
		// body
		// process modifies
		if(fres != null){
			if(fres.vstp_modifiers.size() > 0){
				for(int i = 0; i<fres.vstp_modifiers.size(); i++){
				//for(VSTPDataItemType s : fres.vstp_modifiers.keySet()){
					tmp += fres.vstp_modifiers.get(i).item.getId() + ":";
					res.values.put(fres.vstp_modifiers.get(i).item.getId(), fres.vstp_modifiers.get(i).value);
				}
				tmp = tmp.substring(0, tmp.length() - 1);
				res.values.put(VSTPDataItemType.PENDING_MODIFIERS.getId(), tmp);
				tmp = "";
			}
			// routing connection
			if(fres.routing_connections != null){
				if(fres.routing_connections.size() > 0){
					for(int i = 0; i<fres.routing_connections.size(); i++) tmp += fres.routing_connections.get(i) + ":";
					tmp = tmp.substring(0, tmp.length() - 1);
					res.values.put(VSTPDataItemType.ROUTING_CONNECTION.getId(), tmp);
				}
				
			}
			// result eval
			if(FilterResult.evaluate(fres, FNConfigData.max_total_score)){
				res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "1");
				
			}else{
				res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "0");
				
			}
			/*
			
			if(fres.result){
				if(fres.isUnconditional){
					res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "1");
				}else if(fres.total_score <= FNConfigData.max_total_score){
					res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "1");
				}else{
					res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "0");
				}
			}else{
				res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "0");
			}
			*/
			
		}else{
			// no filter
			res.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "1");
		}

		return res;
	}
	
	public static SmsfsPacket prepareFilter(MessageDescriptor md){
		SmsfsPacket fp = null;
		if(md != null){
			fp = new SmsfsPacket();
			// save md reference
			fp.extra_data.add(md);
			// filter type
			switch(md.header.msg_type){
				case HLR: 
					fp.filterType = FilterType.HLR;
					fp.filterDescriptor = SmsfsManager.currentFilterHLR;
					break;
				case SMS_MO: 
					fp.filterType = FilterType.MO; 
					fp.filterDescriptor = SmsfsManager.currentFilterMO;
					break;
				case SMS_MT: 
					fp.filterType = FilterType.MT; 
					fp.filterDescriptor = SmsfsManager.currentFilterMT;
					break;
				case SMPP_MO:
					fp.filterType = FilterType.SMPP_MO; 
					fp.filterDescriptor = SmsfsManager.currentFilterSMPP_MO;
					break;
				case SMPP_MT:
					fp.filterType = FilterType.SMPP_MT; 
					fp.filterDescriptor = SmsfsManager.currentFilterSMPP_MT;
					break;
			}
			// mode
			if(md.header.ds == DataSourceType.SPCAP_SCTP || md.header.ds == DataSourceType.SPCAP_SMPP) fp.packetMode = ModeOfOperation.NON_INTRUSIVE; else fp.packetMode = ModeOfOperation.INTRUSIVE;
			// data
			// HLR packet data
			if(md.header.msg_type == MessageType.HLR){
				fp.bindings.add(md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_TT.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_TT.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.HLR_IMSI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.HLR_MSISDN.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.HLR_NNN.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.HLR_ANNN.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.HLR_SCA.getId()));

			// SS7 SMS packet data
			}else if(md.header.msg_type == MessageType.SMS_MO || md.header.msg_type == MessageType.SMS_MT){
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.MAP_SCOA.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.MAP_SCDA.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.MAP_IMSI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.MAP_MSISDN.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING_ENC.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION_ENC.getId()));
				fp.bindings.add(new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.SMS_TPDU_UD.getId()))));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_TPDU_DCS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_MSG_TYPE.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_TT.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_TT.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId()));
			// SMPP SMS packet data
			}else if(md.header.msg_type == MessageType.SMPP_MO || md.header.msg_type == MessageType.SMPP_MT){
				fp.bindings.add(md.values.get(VSTPDataItemType.IP_SOURCE.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.IP_DESTINATION.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.TCP_SOURCE.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.TCP_DESTINATION.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_SYSTEM_ID.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_PASSWORD.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_SERVICE_TYPE.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_ORIGINATOR_TON.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_ORIGINATOR_NP.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_ORIGINATOR_ADDRESS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_RECIPIENT_TON.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_RECIPIENT_NP.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_RECIPIENT_ADDRESS.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_ESM_MESSAGE_MODE.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_ESM_MESSAGE_TYPE.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_ESM_GSM_FEATURES.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_PROTOCOL_ID.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_PRIORITY_FLAG.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_DELIVERY_TIME.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_VALIDITY_PERIOD.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_RD_SMSC_RECEIPT.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_RD_SME_ACK.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_RD_INTERMEDIATE_NOTIFICATION.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_REPLACE_IF_PRESENT.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_DATA_CODING.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_SM_DEFAULT_MSG_ID.getId()));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMPP_SM_LENGTH.getId()));
				fp.bindings.add(new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.SMPP_SM.getId()))));
				fp.bindings.add(md.values.get(VSTPDataItemType.SMS_MSG_TYPE.getId()));

			}
		}
		
		return fp;
		
	}	
	
	public static void start_SGN_monitor(){
		if(FNConfigData.sg_reconnect_status){
			sgr_r = new SGReconnect();
			sgr_t = new Thread(sgr_r, "SG_RECONNECT_MONITOR");
			sgr_t.start();
			
		}
	}
	public static void connectNode(String id){
		SGDescriptor sgd = sgn_map.get(id);
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		if(sgd != null){
			logger.info("Initializing SCTP Connection, client id = [" + id + "]!");
			sgd.sctp_id = SSctp.initClient(sgd.address, sgd.port, 16, null, 0);
			if(sgd.sctp_id > -1){
				logger.info("SCTP Connection, client id = [" + sgd.sctp_id + "], ready!");
				logger.info("Negotiating VSTP Connection, client id = [" + sgd.sctp_id + "]!");
				// negotiate VSTP connect
				res = SSctp.send(sgd.sctp_id, VSTP.generate_init(LocationType.FN, FNConfigData.fn_id).getBytes(), 0);
				if(res > -1){
					sd = SSctp.receive(sgd.sctp_id);
					if(sd != null){
						if(sd.payload != null){
							if(VSTP.check_accept(new String(sd.payload))){
								logger.info("VSTP Connection initialized, id = [" + id + "]!");
								vstp_ready = true;
								sgn_connected = true;
								// start
								sgd.start();
							}
						}
					}
					if(!vstp_ready){
						logger.warn("Error while negotiating VSTP Connection, removing SGN Connection [" + id + "]!");
						removeNode(id);
						
					}
				}
				
				
			}else logger.warn("Error while initializing SCTP Connection, client id = [" + id + "]!");

		}
	}
	public static void addNode(String id, SGDescriptor sgd){
		sgn_map.put(id, sgd);
		logger.info("Adding signaling node [" + id + "], address = [" + sgd.address + "], port = [" + sgd.port + "]!");
		connectNode(id);
	}
	public static SGDescriptor getNode(String id){
		return sgn_map.get(id);
	}
	public static void deactivateNode(String id){
		sgn_map.get(id).stop();
		sgn_map.get(id).active = false;
	}
	public static void removeNode(String id){
		sgn_map.remove(id);
		if(sgn_map.size() == 0){
			sgn_connected = false;
			// stats
			StatsManager.VSTP_STATS.VSTP_NO_SG_NODES++;
		}
	}
	public static void init(){
		logger.info("Starting Signaling Node Manager...");
		sgn_map = new ConcurrentHashMap<String, SGDescriptor>();
		logger.info("Starting SGN Connection Monitor...");
		start_SGN_monitor();
	}
}
