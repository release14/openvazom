package net.smstpdu.tpdu.udh;

public class IE_Concatenated_16bit extends IE_ConcatenatedReference {

	public IE_Concatenated_16bit(){
		super();
		type = InformationElementType.CONCATENATED_16bit;
		
	}
	public void init(byte[] data){
		messageIdentifier = (data[0] << 8) + (data[1] & 0xFF) & 0xFFFF;
		parts = data[2] & 0xFF;
		partNumber = data[3] & 0xFF;
	}

	public byte[] encode() {
		byte[] res = new byte[6];
		res[0] = (byte)type.getId();
		res[1] = 4;
		res[2] = (byte)(messageIdentifier >> 8);
		res[3] = (byte)messageIdentifier;
		res[4] = (byte)parts; 
		res[5] = (byte)partNumber;
		return res;
	}
	
}
