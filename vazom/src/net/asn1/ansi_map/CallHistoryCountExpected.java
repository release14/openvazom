package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallHistoryCountExpected extends INTEGER{

	public CallHistoryCountExpected(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
