package net.mtp3.parameters;

public class RoutingLabel {
	public int signallingLinkSelector;
	public int destinationPointCode;
	public int originatingPointCode;
}
