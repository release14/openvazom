
-- -----------------------------------------------------
-- Table `direction`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `direction` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL)
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `type` (
  `id` INT NOT NULL ,
  `name` VARCHAR(150) NOT NULL)
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `mode`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mode` (
  `id` INT NOT NULL  ,
  `name` VARCHAR(45) NOT NULL); 


-- -----------------------------------------------------
-- Table `filter_action`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `filter_action` (
  `id` INT NOT NULL  ,
  `name` VARCHAR(45) NOT NULL);


-- -----------------------------------------------------
-- Table `SmsEncoding`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SmsEncoding` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL);


-- -----------------------------------------------------
-- Table `TypeOfNumber`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TypeOfNumber` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL);


-- -----------------------------------------------------
-- Table `sms`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sms` (
  `id` INT NOT NULL ,
  `direction_id` INT NOT NULL ,
  `type_id` INT NOT NULL ,
  `mode_id` INT NOT NULL ,
  `filter_action_id` INT NULL ,
  `filter_total_score` INT NULL ,
  `gt_called` VARCHAR(150) NULL ,
  `gt_called_csum` BIGINT NULL,
  `gt_calling` VARCHAR(150) NULL ,
  `gt_calling_csum` BIGINT NULL,
  `scda` VARCHAR(150) NULL ,
  `scda_csum` BIGINT NULL,
  `scoa` VARCHAR(150) NULL ,
  `scoa_csum` BIGINT NULL,
  `imsi` VARCHAR(150) NULL ,
  `imsi_csum` BIGINT NULL,
  `msisdn` VARCHAR(150) NULL ,
  `msisdn_csum` BIGINT NULL,
  `sms_destination` VARCHAR(150) NULL ,
  `sms_destination_csum` BIGINT NULL,
  `sms_originating` VARCHAR(150) NULL ,
  `sms_originating_csum` BIGINT NULL,
  `sms_text` TEXT NULL ,
  `sms_text_csum` BIGINT NULL ,
  `timestamp` TIMESTAMP NULL ,
  `sms_text_enc_id` INT NULL ,
  `sms_destination_enc_id` INT NULL ,
  `sms_originating_enc_id` INT NULL ,
  `m3ua_data_dpc` INT NULL ,
  `m3ua_data_opc` INT NULL ,
  `tcap_sid` BIGINT NULL ,
  `tcap_did` BIGINT NULL) 
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sri`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sri` (
  `id` BIGINT NOT NULL ,
  `m3ua_data_opc` INT NULL ,
  `m3ua_data_dpc` INT NULL ,
  `gt_called` VARCHAR(150) NULL ,
  `gt_calling` VARCHAR(150) NULL ,
  `tcap_sid` BIGINT NULL ,
  `tcap_did` BIGINT NULL ,
  `app_ctx_oid` VARCHAR(300) NULL ,
  `imsi` VARCHAR(20) NULL ,
  `msisdn` VARCHAR(50) NULL ,
  `nnn` VARCHAR(50) NULL ,
  `annn` VARCHAR(50) NULL ,
  `sca` VARCHAR(50) NULL ,
  `timestamp` TIMESTAMP NULL ,
  `error_code` INT NULL ,
  `no_reply` TINYINT(1)  NULL ,
  `tcap_dialogue_error_code` INT NULL ,
  `tcap_abort_error_code` INT NULL);


-- -----------------------------------------------------
-- Table `GSMMAPLocalErrorcode`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `GSMMAPLocalErrorcode` (
  `code` INT NOT NULL ,
  `name` VARCHAR(45) NULL);


-- -----------------------------------------------------
-- Table `TCAPDialogueError`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TCAPDialogueError` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NULL);


-- -----------------------------------------------------
-- Table `TCAPP_AbortCause`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TCAPP_AbortCause` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NULL);

