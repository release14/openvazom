package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Dest_telematics_id extends OptionalParameterBase {

	public Dest_telematics_id(){
		type = OptionalParameterType.DEST_TELEMATICS_ID;
	}
	public byte[] value;
	
	public void init(byte[] data) {
		value = data.clone();

	}

}
