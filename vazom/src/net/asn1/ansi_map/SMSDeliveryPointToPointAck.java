package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSDeliveryPointToPointAck extends SET{

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(0).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(1).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MSID get_msid(){
		return (MSID)elements.get(2).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public SMS_BearerData get_sms_BearerData(){
		return (SMS_BearerData)elements.get(3).data;
	}
	public SMS_BearerData new_sms_BearerData(){
		return new SMS_BearerData();
	}

	public SMS_CauseCode get_sms_CauseCode(){
		return (SMS_CauseCode)elements.get(4).data;
	}
	public SMS_CauseCode new_sms_CauseCode(){
		return new SMS_CauseCode();
	}

	public SMS_TransactionID get_sms_TransactionID(){
		return (SMS_TransactionID)elements.get(5).data;
	}
	public SMS_TransactionID new_sms_TransactionID(){
		return new SMS_TransactionID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(105, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_BearerData(); }});
		elements.add(new ElementDescriptor(153, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_CauseCode(); }});
		elements.add(new ElementDescriptor(302, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_TransactionID(); }});
	}
	public SMSDeliveryPointToPointAck(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
