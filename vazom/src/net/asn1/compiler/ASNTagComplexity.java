package net.asn1.compiler;

public enum ASNTagComplexity {
	Primitive,
	Constructed,
	Unknown;
	
	public synchronized static ASNTagComplexity fromId(int id){
		ASNTagComplexity res = null;
		switch(id){
			case 0x00: res = Primitive; break;
			case 0x20: res = Constructed; break;
			case -1: res = Unknown; break;
		}
		return res;
	}
}
