package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ChangeFacilitiesRes extends SET{

	public ReasonList get_reasonList(){
		return (ReasonList)elements.get(0).data;
	}
	public ReasonList new_reasonList(){
		return new ReasonList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(218, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reasonList(); }});
	}
	public ChangeFacilitiesRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
