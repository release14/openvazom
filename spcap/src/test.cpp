#include <iostream>
#include <deque>
#include <vector>
#include <list>
#include <string>
#include <string.h>
#include <time.h>
#include <pcap.h>
#include <stdlib.h>
#include <pthread.h>
#include "SPcap.h"
using namespace std;
struct thread_args{
	SPcap *spcap;
	pcap_t *handle;
	u_char id;
	deque<string> *dq;
};
bool dq_locked = false;
pthread_mutex_t mt = PTHREAD_MUTEX_INITIALIZER;
pthread_spinlock_t sl;


void *thread_routine(void *args){
	stream_descriptor *sd = (stream_descriptor *)args;
	packet_descriptor *pd;

	//pcap_stat *ps = new pcap_stat;
	//cout << "thread_routine " << (int)sd->id << endl;
	//u_char *tmp = new u_char[1500];
	timespec st;
	timespec rmtp;
	st.tv_sec = 0;
	st.tv_nsec = 1000000; // 1ms
	//st.tv_sec
	//nanosleep(&st, &rmtp);
	while(true){
		//pthread_spin_lock(&sl);
		pd = SPcap::popPacket(sd->id);
		//pthread_mutex_lock(&sd->q_lock);

		//memcpy(tmp, sd->fq[0].packet, 1500);
		//pthread_mutex_unlock(&sd->q_lock);

		if(pd != NULL){
			//cout << "Popped: " << dec << (u_int)pd->caplen << endl;
			delete[] pd->packet;
			pd->packet = NULL;
			delete pd;
			pd = NULL;
			//pcap_stats(sd->handle, ps);
			//cout << "Stats: recv=" << ps->ps_recv << ", drop=" << ps->ps_drop << ", ifdrop=" << ps->ps_ifdrop <<  endl;


		}else {
			//cout << "SLeeping" << endl;
			//sleep(1);
			nanosleep(&st, &rmtp);
		}

	}

	//aa->spcap->startCapture(aa->id);
	return NULL;
}
void *thread_routine2(void *args){
	stream_descriptor *sd = (stream_descriptor *)args;
	//pcap_stat *ps = NULL;
	pfring_stat *ps = NULL;
	while(true){
		ps = SPcap::getStats(sd->id);
		//pcap_stats(sd->handle, ps);
		pthread_mutex_lock(&sd->q_lock);
		cout << "Stats: recv=" << ps->recv << ", drop=" << ps->drop <<  ":" << sd->q->size() <<  ":" << sd->tmp_q->size() << endl;
		pthread_mutex_unlock(&sd->q_lock);
		delete ps;
		sleep(5);

	}
/*
	while(true){
		pthread_mutex_lock(&mt);
		//pthread_spin_lock(&sl);

		(*aa).dq->push_back("routine2");
		pthread_mutex_unlock(&mt);
		//pthread_spin_unlock(&sl);
	}
	cout << "ROUTINE2 DONE" << endl;
	*/
	//aa->spcap->startCapture(aa->id);
}

int main() {
	//SPcap spcap;
	string *dev = new string[100];
	int l = SPcap::getDevices(dev);
	//cout << l << endl;
	/*
	for(int i = 0; i<l; i++){
		cout << dev[i] << endl;

	}
*/
/*
	stream_descriptor sd;
	sd.id = 99;
	u_char tst = 99;
	u_char *tstp = &tst;
	*tstp = 99;
	SPcap::stream_lst.push_back(sd);
	cout << (int)SPcap::getStream(tstp)->id << endl;

	tstp = NULL;
	delete tstp;
	return 0;

	deque<string> *dq = new deque<string>;
	string test = "ABC";
	dq->push_back(test);

	cout << "1:" << dq->front() << endl;
	//dq->pop_front();
	delete dq;
	cout << "2:" << dq->front() << endl;
	return 1;
	//stream_descriptor sd;
	//stream_descriptor *sdp = &sd;
	//cout << &sd << endl;
	//cout << sdp << endl;

	return 1;
*/
	/*
	timeval tv;
	timespec tspec;
	gettimeofday(&tv, 0);
	long int usec = tv.tv_usec;
	bool done = false;
	clock_gettime(CLOCK_REALTIME, &tspec);
	usec =  tspec.tv_sec * 1000000000 + tspec.tv_nsec;
	cout << usec << endl;
	long  int usec2;
	while(!done){
		clock_gettime(CLOCK_REALTIME, &tspec);
		usec2 =  tspec.tv_sec * 1000000000 + tspec.tv_nsec;
		if(usec2 - usec >= 100000000){
			cout << (usec2 - usec) / 1000000 << " msec" << endl;
			done = true;
		}
	}
	return 1;
*/
	SPcap::init();
	stream_descriptor *sd1 = SPcap::initCapture("dna:eth0", 1500, 100000);

	cout << sd1->handle << endl;

	//SPcap::setBPF(sd1->handle, "tcp port 80");
	cout << "ID " << (int)sd1->id << endl;
	SPcap::startCapture(sd1->id);

	//cout << "AAAAAAAAAAAAAAAA" << endl;

	//return 1;
	//stream_descriptor *sd2 = spcap.initCapture("eth1", 64*1024);
	//sd1->queue = NULL;
	//delete sd2->queue;
	//spcap.setBPF(handle2, "udp port 1194");

	thread_args args;
	thread_args args2;

	//args.handle = handle;
	//args.spcap = &spcap;
	args.id = 1;

	//args2.handle = handle2;
	//args2.spcap = &spcap;
	args2.id = 2;

	pthread_t thread1;
	pthread_t thread2;
	//pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
/*
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setschedpolicy(&attr, SCHED_RR);
	*/
	//pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	//cout << sched_get_priority_min(SCHED_OTHER) << endl;
	//sched_param sp;
	//sp.__sched_priority = 99;
	//pthread_attr_setschedparam(&attr, &sp);
	//cpu_set_t cs;
	//CPU_ZERO(&cs);
	//CPU_SET(0, &cs);
	//pthread_setaffinity_np(thread2, sizeof(cs), &cs);

	pthread_create(&thread2, NULL, thread_routine2, (void *)sd1);
	pthread_create(&thread1, NULL, thread_routine, (void *)sd1);

	//SPcap::stopCapture((u_char)1);
	//pthread_join(thread1, NULL);
	//pthread_join(sd1->thread, NULL);

	pthread_join(thread2, NULL);
	//spcap.startCapture(handle2, 200);
	/*
	while(true){
		packet = pcap_next(handle, &hdr);
		cout << "Packet Length: " << dec << hdr.len << endl;

		for(u_int i = 0; i<hdr.len; i++){
			cout << hex << (int)packet[i] << ":";
		}
		cout << endl;

	}
*/
	delete[] dev;
}

