package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReturnResultResult extends SEQUENCE{

	public MAP_OPERATION get_opCode(){
		return (MAP_OPERATION)elements.get(0).data;
	}
	public MAP_OPERATION new_opCode(){
		return new MAP_OPERATION();
	}

	public ReturnResultParameter get_returnparameter(){
		return (ReturnResultParameter)elements.get(1).data;
	}
	public ReturnResultParameter new_returnparameter(){
		return new ReturnResultParameter();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_opCode(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_returnparameter(); }});
	}
	public ReturnResultResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
