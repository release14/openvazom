package net.vstp;

import java.util.ArrayList;
import java.util.HashMap;

import net.utils.Utils;

public class MessageDescriptor {
	public HeaderDescriptor header;
	public HashMap<String, String> values;
	// available only on reply message
	public FilterResultType filter_result;
	// extra
	public ArrayList<Object> extra_data;
	
	public Object mutex;
	public VSTPCallback callback;
	
	public boolean exists(String id){
		return values.get(id) != null;
	}

	
	public MessageDescriptor(){
		values = new HashMap<String, String>();
		header = new HeaderDescriptor();
	}
	public byte[] encode(){
		byte[] res = null;
		byte[] hdr = null;
		try{
			ArrayList<Byte> tmp = new ArrayList<Byte>();
			// header
			hdr = header.encode();
			for(int i = 0; i<hdr.length; i++) tmp.add(hdr[i]);
			// body
			for(String s : values.keySet()){
				Utils.bytesToLst(tmp, s.getBytes());
				Utils.bytesToLst(tmp, new byte[]{'='});
				Utils.bytesToLst(tmp, (values.get(s) != null ? values.get(s).getBytes() : null));
				Utils.bytesToLst(tmp, new byte[]{'\n'});
			}
			// end
			Utils.bytesToLst(tmp, new byte[]{'E', 'N', 'D', '\n'});
			
			// result
			res = new byte[tmp.size()];
			for(int i = 0; i<tmp.size(); i++) res[i] = tmp.get(i);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}
}
