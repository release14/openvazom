package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Abort extends SEQUENCE{

	public TransactionID get_identifier(){
		return (TransactionID)elements.get(0).data;
	}
	public TransactionID new_identifier(){
		return new TransactionID();
	}

	public DialoguePortion get_dialogPortion(){
		return (DialoguePortion)elements.get(1).data;
	}
	public DialoguePortion new_dialogPortion(){
		return new DialoguePortion();
	}

	public AbortCauseInformation get_causeInformation(){
		return (AbortCauseInformation)elements.get(2).data;
	}
	public AbortCauseInformation new_causeInformation(){
		return new AbortCauseInformation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(7, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_identifier(); }});
		elements.add(new ElementDescriptor(25, ASNTagClass.PRIVATE, true, false){public void set(){ data = new_dialogPortion(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_causeInformation(); }});
	}
	public Abort(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
