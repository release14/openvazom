package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.RangeAndStatus;

public class CircuitGroupQuery extends MessageBase {
	public RangeAndStatus range_and_status;
	public CircuitGroupQuery(){
		super();
		type = MessageType.CIRCUIT_GROUP_QUERY;
		pointers = new Pointer[1];
	}
	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			range_and_status = new RangeAndStatus();
			range_and_status.init(getPointerData(data, 0));
			
			
		}

	}

}
