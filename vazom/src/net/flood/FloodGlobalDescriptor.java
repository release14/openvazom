package net.flood;

import java.util.ArrayList;

public class FloodGlobalDescriptor {
	// global mo
	public long TMP_MO;
	public long CURRENT_MO_MINUTE;
	public long CURRENT_MO_HOUR;
	public long CURRENT_MO_DAY;
	public long MAX_MO_MINUTE = 9223372036854775807L;
	public long MAX_MO_HOUR = 9223372036854775807L;
	public long MAX_MO_DAY = 9223372036854775807L;
	// global mt
	public long TMP_MT;
	public long CURRENT_MT_MINUTE;
	public long CURRENT_MT_HOUR;
	public long CURRENT_MT_DAY;
	public long MAX_MT_MINUTE = 9223372036854775807L;
	public long MAX_MT_HOUR = 9223372036854775807L;
	public long MAX_MT_DAY = 9223372036854775807L;
	// global all dests
	public long MAX_ALL_MO_MINUTE = 9223372036854775807L;
	public long MAX_ALL_MO_HOUR = 9223372036854775807L;
	public long MAX_ALL_MO_DAY = 9223372036854775807L;
	public long MAX_ALL_MT_MINUTE = 9223372036854775807L;
	public long MAX_ALL_MT_HOUR = 9223372036854775807L;
	public long MAX_ALL_MT_DAY = 9223372036854775807L;
	
	// tmp arrays for calculations
	public ArrayList<Long> tmp_mo_minute;
	public ArrayList<Long> tmp_mo_hour;
	
	public ArrayList<Long> tmp_mt_minute;
	public ArrayList<Long> tmp_mt_hour;
	
	
	public FloodGlobalDescriptor(){
		tmp_mo_minute = new ArrayList<Long>();
		tmp_mo_hour = new ArrayList<Long>();
		
		tmp_mt_minute = new ArrayList<Long>();
		tmp_mt_hour = new ArrayList<Long>();
		
	}
	

}
