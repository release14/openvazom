package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudPackagesDescriptor extends SEQUENCE{

	public Name get_packageName(){
		return (Name)elements.get(0).data;
	}
	public Name new_packageName(){
		return new Name();
	}

	public INTEGER get_packageVersion(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_packageVersion(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_packageName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_packageVersion(); }});
	}
	public IndAudPackagesDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
