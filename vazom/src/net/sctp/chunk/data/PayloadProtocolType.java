package net.sctp.chunk.data;

import java.util.HashMap;



public enum PayloadProtocolType {
	M3UA(0x03);
	
	
	private int id;
	private static final HashMap<Integer, PayloadProtocolType> lookup = new HashMap<Integer, PayloadProtocolType>();
	static{
		for(PayloadProtocolType td : PayloadProtocolType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static PayloadProtocolType get(int id){ return lookup.get(id); }
	private PayloadProtocolType(int _id){ id = _id; }		
}
