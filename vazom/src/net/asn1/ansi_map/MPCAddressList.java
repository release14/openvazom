package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MPCAddressList extends SET{

	public MPCAddress get_mpcAddress(){
		return (MPCAddress)elements.get(0).data;
	}
	public MPCAddress new_mpcAddress(){
		return new MPCAddress();
	}

	public MPCAddress get_mpcAddress2(){
		return (MPCAddress)elements.get(1).data;
	}
	public MPCAddress new_mpcAddress2(){
		return new MPCAddress();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(370, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mpcAddress(); }});
		elements.add(new ElementDescriptor(370, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mpcAddress2(); }});
	}
	public MPCAddressList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
