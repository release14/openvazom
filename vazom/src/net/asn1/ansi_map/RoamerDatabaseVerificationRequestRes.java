package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RoamerDatabaseVerificationRequestRes extends SET{

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(0).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
	}
	public RoamerDatabaseVerificationRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
