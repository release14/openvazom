if exists("b:current_syntax")
  finish
endif


syn keyword smsfsKeywords MODIFY RULE
syn keyword smsfsFields MAP SCOA SCCP GT_CALLED GT_CALLING WL SCDA M3UA OPC DPC SMS_TPDU UD ENC ORIGINATING DESTINATION MD5 QUARANTINE SPAM DCS MSG_TYPE TON UNKNOWN INTERNATIONAL NATIONAL NETWORK_SPECIFIC SUBSCRIBER_NUMBER ALPHANUMERIC ABBREIATED DEFAULT 8BIT UCS2 UPDATE_LST REMOVE RP DICT MO MT HLR NNN ANNN SCA TT NAI NP ISDN_TELEPHONE GENERIC DATA_X121 TELEX MARITIME LAND_MOBILE ISDN_MOBILE PRIVATE SUBSCRIBER_NUMBER RESERVED_FOR_NATIONAL_USE NAI_NATIONAL_SIGNIFICANT_NUMBER NAI_INTERNATIONAL NONE NAI_ONLY TT_ONLY TTNPE TTNPENOA IMSI ADDRESS REQUEST MTP3 RESULT SMPP_MO SMPP_MT FLOOD MAX HOUR MINUTE DAY GLOBAL ALL SMPP IP SOURCE TCP SYSTEM_ID PASSWORD SERVICE ORIGINATOR RECIPIENT ESM MODE MESSAGE PROTOCOL_ID PRIORITY FLAG DELIVERY_TIME VALIDITY_PERIOD RD SMSC_RECEIPT SME_ACK INTERMEDIATE_NOTIFICATION REPLACE_IF_PRESENT DATA_CODING SM_DEFAULT_MSG_ID SM_LENGTH SM MM DATAGRAM FORWARD STORE_FORWARD DELIVERY_ACK MANUAL_USER_ACK MT GF NO YES UDHI_INDICATOR SET_REPLY_PATH SET_BOTH ERMES INTERNET_IP WAP_CLIENT_ID RD SMSCDR SUCCESS_FAILURE FAILURE SMEOA ACK MANUAL_USER_ACK BOTH IN DC IA5_ASCII 8BIT_BINARY_1 ISO_8859_1 8BIT_BINARY_2 JIS ISO_8859_5 ISO_8859_8 PICTOGRAM ISO_2011_JP EXTENDED_KANJI KS_C_5601 LIST
syn keyword smsfsOnOff ON OFF
syn keyword smsfsActions CONTINUE CONTINUE_QUARANTINE ALLOW ALLOW_QUARANTINE ALLOW_UNCONDITIONAL ALLOW_UNCONDITIONAL_QUARANTINE DENY DENY_QUARANTINE GOTO
syn match smsfsOp1 "=="
syn match smsfsOp2 "!="
syn match smsfsOp3 "<="
syn match smsfsOp4 ">="
syn match smsfsOp5 "<"
syn match smsfsOp6 ">"
syn match smsfsOp7 "IN"
syn match smsfsOp8 "NIN"
syn match smsfsExpr ":"
syn match smsfsEvalTrue "+"
syn match smsfsEvalFalse "-"
syn match smsfsStmt ";"
syn match smsfsAttr "@"
syn match smsfsColon1 "("
syn match smsfsColon2 ")"
syn match smsfComment2 "//.*$"
syn match smsfString '".*\"'
let b:current_syntax = "smsfs"

hi def link smsfComment2 Comment
hi def link smsfsKeywords Constant
hi def link smsfString Constant
hi def link smsfsOnOff Type
hi def link smsfsFields Statement
hi def link smsfsActions Constant
hi def link smsfsOp1 PreProc
hi def link smsfsOp2 PreProc
hi def link smsfsOp3 PreProc
hi def link smsfsOp4 PreProc
hi def link smsfsOp5 PreProc
hi def link smsfsOp6 PreProc
hi def link smsfsOp7 PreProc
hi def link smsfsOp8 PreProc

hi def link smsfsExpr Type
hi def link smsfsEvalTrue Type
hi def link smsfsEvalFalse Type

hi def link smsfsAttr Comment
hi def link smsfsStmt Type
hi def link smsfsColon1 Type
hi def link smsfsColon2 Type


