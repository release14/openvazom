package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PkgdName extends OCTET_STRING{

	public PkgdName(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
