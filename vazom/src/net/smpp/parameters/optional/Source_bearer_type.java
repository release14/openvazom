package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Source_bearer_type extends OptionalParameterBase {
	public enum ValueType{
		UNKNOWN(0x00),
		SMS(0x01),
		CSD(0x02),
		PACKET_DATA(0x03),
		USSD(0x04),
		CDPD(0x05),
		DATA_TAC(0x06),
		FLEX_REFLEX(0x07),
		CELL_BROADCAST(0x08);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public ValueType value;
	public Source_bearer_type(){
		type = OptionalParameterType.SOURCE_NETWORK_TYPE;
	}

	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);

	}

}
