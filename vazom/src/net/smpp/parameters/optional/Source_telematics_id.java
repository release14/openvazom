package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Source_telematics_id extends OptionalParameterBase {
	public byte[] value;

	public Source_telematics_id(){
		type = OptionalParameterType.SOURCE_TELEMATICS_ID;
	}
	public void init(byte[] data) {
		value = data.clone();
	}

}
