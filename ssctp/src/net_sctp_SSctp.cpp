#include <jni.h>
#include <iostream>
#include <arpa/inet.h>
#include <netinet/sctp.h>
#include "sctp_functions.h"
#include "net_sctp_SSctp.h"
#include <string.h>

using namespace std;

JNIEXPORT jint JNICALL Java_net_sctp_SSctp_initClient(JNIEnv *env, jclass cls, jstring jaddr, jint port, jint sc, jstring local_ip, jint local_port){
	const char *addr = env->GetStringUTFChars(jaddr, 0);
	unsigned long remote_address = inet_addr(addr);
	unsigned long local_address;
	int socketId = -1;
	env->ReleaseStringUTFChars(jaddr, addr);
	//cout << remote_address << endl;
	if(local_ip != NULL){
		addr = env->GetStringUTFChars(local_ip, 0);
		local_address = inet_addr(addr);
		env->ReleaseStringUTFChars(local_ip, addr);
		//int init_sctp_client_bind(unsigned long addr, unsigned long local_addr, int local_port, int remote_port, int stream_count)
		socketId = init_sctp_client_bind(remote_address, local_address, local_port, port, sc);
	}else socketId = init_sctp_client(remote_address, port, sc);
	//cout << "init_sctp_client, ID: " << socketId << endl;
	return (jint)socketId;
}

JNIEXPORT jint JNICALL Java_net_sctp_SSctp_initServer(JNIEnv *env, jclass cls, jstring jaddr, jint port){
	const char *addr = env->GetStringUTFChars(jaddr, 0);
	unsigned long local_address = inet_addr(addr);
	env->ReleaseStringUTFChars(jaddr, addr);
	//cout << remote_address << endl;
	//int socketId = init_sctp_client(remote_address, port, sc);
	int socketId = init_sctp_server(local_address, port);
	//cout << "init_sctp_server, ID: " << socketId << endl;
	return (jint)socketId;
}

JNIEXPORT jint JNICALL Java_net_sctp_SSctp_clientAccept(JNIEnv *env, jclass cls, jint server_socket_id){
	return get_client(server_socket_id);
}

JNIEXPORT jint JNICALL Java_net_sctp_SSctp_send(JNIEnv *env, jclass cls, jint socketId, jbyteArray jdata, jint sid){
	if(jdata != NULL){
		unsigned long m3ua = ntohl(66);//be32toh(3);
		jbyte *data = env->GetByteArrayElements(jdata, 0);
		int data_length = env->GetArrayLength(jdata);
		size_t dl = data_length;

		int res = send_sctp(socketId, data, &dl, &m3ua, sid);
		env->ReleaseByteArrayElements(jdata, data, 0);
		//cout << "send_sctp, L: " << data_length << endl;
		return res;

	}else cout << "JNI Java_net_sctp_SSctp_sendM3ua: data is NULL!" << endl;
	return -1;
}

JNIEXPORT jint JNICALL Java_net_sctp_SSctp_sendM3ua(JNIEnv *env, jclass cls, jint socketId, jbyteArray jdata, jint sid){
	if(jdata != NULL){
		unsigned long m3ua = ntohl(3);//be32toh(3);
		jbyte *data = env->GetByteArrayElements(jdata, 0);
		int data_length = env->GetArrayLength(jdata);
		//for(int i = 0; i<data_length; i++) cout << i << ":" << hex << (int)data[i] << endl;
		size_t dl = data_length;

		int res = send_sctp(socketId, data, &dl, &m3ua, sid);
		env->ReleaseByteArrayElements(jdata, data, 0);
		//cout << "send_sctp m3ua, L: " << data_length << endl;
		//env->DeleteLocalRef(data);
		return res;
	}else cout << "JNI Java_net_sctp_SSctp_sendM3ua: data is NULL!" << endl;
	return -1;
}

JNIEXPORT jobject JNICALL Java_net_sctp_SSctp_receive(JNIEnv *env, jclass, jint socketId){
	unsigned char *data = new unsigned char[8192];
	int flags = 0;
	int *flagsp = &flags;
	sctp_sndrcvinfo prcvinfo;
	jbyteArray payload = NULL;
	jobject obj = NULL;
	jclass c;
	jvalue args[2];

	jmethodID constructor;

	//bzero(&prcvinfo, sizeof(sctp_sndrcvinfo));

	int dl = rcv_sctp(socketId, data, flagsp, &prcvinfo);
	//cout << "prcvinfo.sinfo_ppid = " << ntohl(prcvinfo.sinfo_ppid) << endl;

	if(dl > 0 && ntohl(prcvinfo.sinfo_ppid) == 66){
		//cout << "rcv_sctp, L: " << dec << dl << endl;
		payload = env->NewByteArray(dl);
		env->SetByteArrayRegion(payload, 0, dl, (jbyte *)data);
		c = env->FindClass("net/sctp/SSCTPDescriptor");
		if(c != NULL){
			constructor = env->GetMethodID(c, "<init>", "(I[B)V");
			if(constructor != NULL){
				args[0].i = prcvinfo.sinfo_stream;
				args[1].l = payload;
				obj = env->NewObjectA(c, constructor, args);
				// remove reference
				args[1].l = NULL;
			}
		}

	}
	delete[] data;

	return obj;



}

JNIEXPORT jobject JNICALL Java_net_sctp_SSctp_receiveM3ua(JNIEnv *env, jclass cls, jint socketId){
	unsigned char *data = new unsigned char[8192];
	int flags = 0;
	int *flagsp = &flags;
	sctp_sndrcvinfo prcvinfo;
	jbyteArray payload = NULL;
	jobject obj = NULL;
	jclass c;
	jvalue args[2];

	jmethodID constructor;

	int dl = rcv_sctp(socketId, data, flagsp, &prcvinfo);
	//cout << prcvinfo.sinfo_stream << endl;
	if(dl > 0 && ntohl(prcvinfo.sinfo_ppid) == 3){
		//cout << "rcv_sctp m3ua, L: " << dec << dl << endl;
		payload = env->NewByteArray(dl);
		env->SetByteArrayRegion(payload, 0, dl, (jbyte *)data);
		c = env->FindClass("net/sctp/SSCTPDescriptor");
		if(c != NULL){
			constructor = env->GetMethodID(c, "<init>", "(I[B)V");
			if(constructor != NULL){
				args[0].i = prcvinfo.sinfo_stream;
				args[1].l = payload;
				obj = env->NewObjectA(c, constructor, args);
				// remove reference
				args[1].l = NULL;
			}
		}

	}
	delete[] data;

	return obj;



}

JNIEXPORT void JNICALL Java_net_sctp_SSctp_shutdownClient(JNIEnv *env, jclass, jint socketId){
	shutdown_sctp_client(&socketId);
	//cout << "shutdown_sctp_client" << endl;
}

JNIEXPORT void JNICALL Java_net_sctp_SSctp_shutdownServer(JNIEnv *env, jclass, jint socketId){
	shutdown_sctp_server(socketId);
	//cout << "shutdown_sctp_server" << endl;
}
