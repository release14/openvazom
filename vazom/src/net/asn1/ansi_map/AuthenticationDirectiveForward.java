package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationDirectiveForward extends SET{

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(0).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(1).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public AuthenticationResponseUniqueChallenge get_authenticationResponseUniqueChallenge(){
		return (AuthenticationResponseUniqueChallenge)elements.get(2).data;
	}
	public AuthenticationResponseUniqueChallenge new_authenticationResponseUniqueChallenge(){
		return new AuthenticationResponseUniqueChallenge();
	}

	public RandomVariableUniqueChallenge get_randomVariableUniqueChallenge(){
		return (RandomVariableUniqueChallenge)elements.get(3).data;
	}
	public RandomVariableUniqueChallenge new_randomVariableUniqueChallenge(){
		return new RandomVariableUniqueChallenge();
	}

	public UpdateCount get_updateCount(){
		return (UpdateCount)elements.get(4).data;
	}
	public UpdateCount new_updateCount(){
		return new UpdateCount();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(37, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponseUniqueChallenge(); }});
		elements.add(new ElementDescriptor(43, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableUniqueChallenge(); }});
		elements.add(new ElementDescriptor(51, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_updateCount(); }});
	}
	public AuthenticationDirectiveForward(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
