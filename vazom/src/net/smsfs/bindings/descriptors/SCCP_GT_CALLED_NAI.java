package net.smsfs.bindings.descriptors;

import net.hplmnr.HPLMNRPacket;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.EncodingScheme;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitle_NOA;
import net.sccp.parameters.global_title.GlobalTitle_TT;
import net.sccp.parameters.global_title.GlobalTitle_TTNPE;
import net.sccp.parameters.global_title.GlobalTitle_TTNPENOA;
import net.smsfs.FilterMode;
import net.smsfs.SmsfsPacket;
import net.smsfs.bindings.BindingDescriptor;
import net.smstpdu.TBCD;

public class SCCP_GT_CALLED_NAI extends BindingDescriptor {
	public SCCP_GT_CALLED_NAI(){
		super(17);
	}
	public SCCP_GT_CALLED_NAI(int index){
		super(index);
	}

	public void modify(SmsfsPacket fp, String new_value) {
		byte[] sccp_packet;
		byte[] m3ua_packet;
		HPLMNRPacket sp;
		GlobalTitleBase gt;
		// not allowed in REFILTER mode
		if(fp.filterMode == FilterMode.NORMAL){
			// get SRIPacket reference
			sp = (HPLMNRPacket)fp.extra_data.get(1);
			gt = sp.sccp_udt.calledPartyAddress.globalTitle;
			// encode SCCP
			switch(sp.sccp_udt.calledPartyAddress.globalTitle.type){
				case NATURE_OF_ADDRESS_INDICATOR_ONLY:
					((GlobalTitle_NOA)gt).natureOfAddress = NatureOfAddress.get(Integer.parseInt(new_value));
					break;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					((GlobalTitle_TTNPENOA)gt).natureOfAddress = NatureOfAddress.get(Integer.parseInt(new_value));
					break;
			}
			// extra data index 2 = tcap packet
			sccp_packet = sp.sccp_udt.encode();
			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			//ndata.message
			DATA ndd = (DATA)sp.m3ua_packet.message;
			ndata.message = ndd;
			ndd.protocolData.setUserProtocolData(sccp_packet);
			m3ua_packet = ndata.encode();

			// final combined result(m3ua + sccp + tcap)
			fp.packet = m3ua_packet;
			
			
		}


	}

}
