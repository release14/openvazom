package net.security;

public class FeatureDescriptor {
	public int fid;
	public boolean active;
	public boolean critical;
	public int err_count;
	
	
	public FeatureDescriptor(int _fid, boolean _active){
		fid = _fid;
		active = _active;
	}
}
