package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RejectInvokeIDRej extends CHOICE{

	public InvokeIdType get_derivable(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_derivable(){
		return new InvokeIdType();
	}

	public NULL get_not_derivable(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_not_derivable(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_derivable(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_not_derivable(); }});
	}
	public RejectInvokeIDRej(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
