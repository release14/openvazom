package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class GeneralProblem extends INTEGER{

	public static final int _unrecognizedComponent = 0;
	public static final int _mistypedComponent = 1;
	public static final int _badlyStructuredComponent = 2;
	public GeneralProblem(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
