package net.hlr;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;
import net.asn1.gsmmap2.AddressString;
import net.asn1.gsmmap2.GSMMAPOperationLocalvalue;
import net.asn1.gsmmap2.ISDN_AddressString;
import net.asn1.gsmmap2.MAP_OPERATION;
import net.asn1.gsmmap2.OperationLocalvalue;
import net.asn1.gsmmap2.RoutingInfoForSM_Arg;
import net.asn1.tcap2.Begin;
import net.asn1.tcap2.Component;
import net.asn1.tcap2.ComponentPortion;
import net.asn1.tcap2.DialoguePortion;
import net.asn1.tcap2.Invoke;
import net.asn1.tcap2.InvokeIdType;
import net.asn1.tcap2.OrigTransactionID;
import net.asn1.tcap2.TCMessage;
import net.asn1.tcapdialogue.AARQ_apdu;
import net.asn1.tcapdialogue.DialoguePDU;
import net.asn1.types.BIT_STRING;
import net.asn1.types.BOOLEAN;
import net.asn1.types.EXTERNAL;
import net.asn1.types.EXTERNAL_ENCODING;
import net.asn1.types.OBJECT_IDENTIFIER;
import net.ber.BerTag;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.m3ua.parameters.NetworkAppearance;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.m3ua_conn.M3UAConnection;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionType;
import net.sccp.MessageType;
import net.sccp.NatureOfAddress;
import net.sccp.RoutingIndicator;
import net.sccp.SCCP;
import net.sccp.messages.UDT_UnitData;
import net.sccp.parameters.global_title.EncodingScheme;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.GlobalTitle_TTNPENOA;
import net.sccp.parameters.global_title.NumberingPlan;
import net.sccp.parameters.protocol_class.MessageHandling;
import net.sccp.parameters.protocol_class.ProtocolClassType;
import net.smstpdu.TBCD;
import net.smstpdu.TypeOfNumber;
import net.utils.Utils;

public class HLRManager {
	public static Logger logger=Logger.getLogger(HLRManager.class);

	private static ConcurrentHashMap<String, HLRPacket> map;
	private static BerTranscoderv2 ber = new BerTranscoderv2();
	private static long NEXT_TCAP_ID = 0;
	private static Timeout timeout_r;
	private static Thread timeout_t;
	public static boolean stopping;
	private static int timeout_interval;
	private static int timeout;
	
	private static class Timeout implements Runnable{
		HLRPacket hlrp = null;
		long ts;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				try{ Thread.sleep(timeout_interval); }catch(Exception e){ e.printStackTrace(); }
				ts = System.currentTimeMillis();
				for(String s : map.keySet()){
					hlrp = map.get(s);
					if(ts - hlrp.ts > timeout){
						logger.info("HLRManager Timeout reached for HLRP id = [" + s + "]!");
						// if mutex exists, release
						if(hlrp.mutex != null){
							synchronized(hlrp.mutex){
								hlrp.mutex.notify();
							}													
						}
						// remove
						map.remove(s);
					}
				}
			}
			logger.info("Ending...");
		}
		
	}
	
	public static synchronized long consume_TCAP_ID(){
		NEXT_TCAP_ID++;
		int tbits = (int)Math.ceil(Math.log10(NEXT_TCAP_ID + 1) / Math.log10(2));
		int tbytes = (int)Math.ceil((double)tbits / 8);
		if(tbytes > 4) NEXT_TCAP_ID = 1;
		return NEXT_TCAP_ID;
		
	}

	public static void addMutex(String id, Object mutex){
		HLRPacket hlrp = new HLRPacket();
		hlrp.mutex = mutex;
		hlrp.ts = System.currentTimeMillis();
		map.put(id, hlrp);
	}
	public static Object getMutex(String id){
		HLRPacket hlrp = map.get(id);
		if(hlrp != null) return hlrp.mutex;
		return null;
	}
	public static HLRPacket getHLRP(String id){
		return map.get(id);
	}
	public static void removeMutex(String id){
		map.remove(id);
	}
	public static void remove(String id){
		map.remove(id);
	}
	
	public static HLRPacket generateRequest(String _msisdn, String _sca, RoutingConnection r_conn){
		byte[] tcap_packet = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		M3UAConnection m3ua_conn;
		
		if(_msisdn != null && _sca != null & r_conn != null){
			long tcap_id = consume_TCAP_ID();
			net.smstpdu.AddressString as;
			HLRPacket hlrp = new HLRPacket();
			// TCM
			TCMessage tcm = new TCMessage();
			BerTag b_root = BerTag.createNew(tcm, null, null);
			
				// BEGIN
				Begin begin = new Begin();
				BerTag b_begin = BerTag.createNew(begin, b_root, tcm.elements.get(1));

				
					// Source Transaction ID
					OrigTransactionID sid = new OrigTransactionID();
					BerTag b_sid = BerTag.createNew(sid, b_begin, null);
					sid.value = Utils.num2bytes(tcap_id); 

					
					//DialoguePortion
					DialoguePortion dportion = new DialoguePortion();
					BerTag b_dportion = BerTag.createNew(dportion, b_begin, null);
					
						// EXTERNAL
						EXTERNAL external = new EXTERNAL();
						BerTag b_external = BerTag.createNew(external, b_dportion, null);
							
							// OID
							OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
							BerTag b_oid = BerTag.createNew(oid, b_external, null);
							oid.value = Utils.encodeOID("0.0.17.773.1.1.1");
							
							// External Encoding
							EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
							BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
							
								// single asn1 type
								ASNType single_type = new ASNType();
								single_type.asn_pc = ASNTagComplexity.Constructed;
								BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
								
									// DialoguePDU
									DialoguePDU dpdu = new DialoguePDU();
									BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
									
										// DialogueRequest
										AARQ_apdu drq = new AARQ_apdu();
										BerTag b_drq = BerTag.createNew(drq, b_dpdu, dpdu.elements.get(0));
										
										
											// Protocol Version
											BIT_STRING pversion = new BIT_STRING();
											BerTag b_pversion = BerTag.createNew(pversion, b_drq, drq.elements.get(0));
											pversion.value = new byte[]{0x07, (byte)0x80};
											
											
											// Application Context Name EXPLICIT
											ASNType appctx_ex = new ASNType();
											appctx_ex.asn_pc = ASNTagComplexity.Constructed;
											BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_drq, drq.elements.get(1));
											
												// Application Context Name
												OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
												BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
												appctx.value = Utils.encodeOID("0.4.0.0.1.0.20.2");
														
					
					
					// Components
					ComponentPortion components = new ComponentPortion();
					BerTag b_components = BerTag.createNew(components, b_begin, null);
		
						// Compoent
						Component component = new Component();
						BerTag b_component = BerTag.createNew(component, b_components, null);
		
							// Invoke
							Invoke invoke = new Invoke();
							BerTag b_invoke = BerTag.createNew(invoke, b_component, component.elements.get(0));
		
								// Invoke ID
								InvokeIdType invoke_id = new InvokeIdType();
								BerTag b_invoke_id = BerTag.createNew(invoke_id, b_invoke, null);
								invoke_id.value = new byte[]{1};
		
								// opCode
								MAP_OPERATION opCode = new MAP_OPERATION();
								BerTag b_opCode = BerTag.createNew(opCode, b_invoke, null);
								
		
									// localValue
									OperationLocalvalue localValue = new OperationLocalvalue();
									BerTag b_localValue = BerTag.createNew(localValue, b_opCode, null);
									localValue.value = new byte[]{GSMMAPOperationLocalvalue._sendRoutingInfoForSM};
		
								// RoutingInfoForSM_Arg
								RoutingInfoForSM_Arg sm_arg = new RoutingInfoForSM_Arg();
								BerTag b_sm_arg = BerTag.createNew(sm_arg, b_invoke, null);
		
									// MSISDN
									ISDN_AddressString msisdn = new ISDN_AddressString();
									BerTag b_msisdn = BerTag.createNew(msisdn, b_sm_arg, sm_arg.elements.get(0));
									as = new net.smstpdu.AddressString();
									as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
									as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
									as.digits = TBCD.encode(_msisdn, 0x0f);
									msisdn.value = net.smstpdu.AddressString.encode(as);
									
									// SM-RP-PRI
									BOOLEAN sm_rp_pri = new BOOLEAN();
									BerTag b_sm_rp_pri = BerTag.createNew(sm_rp_pri, b_sm_arg, sm_arg.elements.get(1));
									sm_rp_pri.value = new byte[]{BOOLEAN.TRUE};
									
									// Service Centre Address
									AddressString sca = new AddressString();
									BerTag b_sca = BerTag.createNew(sca, b_sm_arg, sm_arg.elements.get(2));
									as = new net.smstpdu.AddressString();
									as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
									as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
									as.digits = TBCD.encode(_sca, 0x0f);
									sca.value = net.smstpdu.AddressString.encode(as);
			
			
			
			
			// encode TCAP
			ber.clearLengths(b_root);
			ber.prepareLenghts(b_root);
			tcap_packet = ber.encode(b_root);		
			
			
			
			// encode SCCP
			UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
			ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
			ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
			// Called party
			ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.calledPartyAddress.SSNIndicator = true;
			ns.calledPartyAddress.pointCodeIndicator = false;
			ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
			ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Called GT
			GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
			ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
			ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt.encodingScheme =  (SGNConfigData.hplmnr_hlr_gt_override.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(SGNConfigData.hplmnr_hlr_gt_override);
			// Calling party
			ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.callingPartyAddress.SSNIndicator = true;
			ns.callingPartyAddress.pointCodeIndicator = false;
			ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
			ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Calling GT
			GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
			ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
			ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt2.encodingScheme = (SGNConfigData.hplmnr_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
			ns.data = tcap_packet;
			sccp_packet = ns.encode();
			
			if(r_conn.type == RoutingConnectionType.M3UA){
				m3ua_conn = (M3UAConnection)r_conn;
				// M3UA
				M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
				DATA ndd = (DATA)ndata.message;
				ndd.protocolData.setOPC(m3ua_conn.opc);
				ndd.protocolData.setDPC(m3ua_conn.dpc);
				ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
				ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
				ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
				ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
				ndd.protocolData.setUserProtocolData(sccp_packet);
				ndd.networkAppearance = new NetworkAppearance();
				ndd.networkAppearance.setNetworkAppearance(m3ua_conn.network_app);
				//ndd.routingContext = new RoutingContext();
				//ndd.routingContext.setRoutingContext(m3ua_conn.routing_ctx);
				m3ua_packet = ndata.encode();

				// data
				hlrp.gt_called = SGNConfigData.hplmnr_hlr_gt_override;
				hlrp.gt_calling = SGNConfigData.hplmnr_smsrouter_gt;
				hlrp.tcap_id = tcap_id;
				hlrp.msisdn = _msisdn;
				hlrp.sca = _sca;
				hlrp.packet = m3ua_packet;
				hlrp.m3ua_packet = ndata;
				// add to map
				map.put(SGNConfigData.hplmnr_hlr_gt_override + ":" +  SGNConfigData.hplmnr_smsrouter_gt + ":" + tcap_id, hlrp);
				return hlrp;
				
			}else if(r_conn.type == RoutingConnectionType.MTP3){
				//TODO
			}
			

		}
						
		return null;
	}
	
	public static HLRPacket requestExists(long dest_tcap_id, String gt_calling, String gt_called){
		HLRPacket hlrp = map.get(gt_calling + ":" + gt_called + ":" + dest_tcap_id);
		if(hlrp != null){
			map.remove(gt_calling + ":" + gt_called + ":" + dest_tcap_id);
			return hlrp;
		}else logger.warn("Request [" + gt_calling + ":" + gt_called + ":" + dest_tcap_id + "] does not exist!");
		return null;
	}
	/*
	private static void initConnection(){
		M3UAConnManager.addConnection("_HLR_CONN", 
									"_HLR_CONN", 
									"localhost", 
									0, 
									SGNConfigData.sri_remote_ip,
									SGNConfigData.sri_remote_port,
									SGNConfigData.sri_hlr_dpc_m3ua_data_dpc, 
									SGNConfigData.sri_m3ua_data_opc,
									SGNConfigData.sri_m3ua_data_nap, 
									-1, 
									16);
		
	}
	*/
	public static void init(int _timeout_interval, int _timeout){
		logger.info("Starting HLR Manager...");
		map = new ConcurrentHashMap<String, HLRPacket>();
		
		timeout_interval = _timeout_interval;
		timeout = _timeout;
		
		
		logger.info("Starting HLR Timeout monitor...");
		timeout_r = new Timeout();
		timeout_t = new Thread(timeout_r, "HLR_TIMEOUT");
		timeout_t.start();
	}

}
