package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class O_BcsmCamelTDPCriteriaList extends SEQUENCE{

	public O_BcsmCamelTDP_Criteria new_child(){
		return new O_BcsmCamelTDP_Criteria();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public O_BcsmCamelTDP_Criteria getChild(int index){
		return (O_BcsmCamelTDP_Criteria)of_children.get(index);
	}
	public O_BcsmCamelTDPCriteriaList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
