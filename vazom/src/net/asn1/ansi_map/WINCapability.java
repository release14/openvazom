package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class WINCapability extends SET{

	public TriggerCapability get_triggerCapability(){
		return (TriggerCapability)elements.get(0).data;
	}
	public TriggerCapability new_triggerCapability(){
		return new TriggerCapability();
	}

	public WINOperationsCapability get_wINOperationsCapability(){
		return (WINOperationsCapability)elements.get(1).data;
	}
	public WINOperationsCapability new_wINOperationsCapability(){
		return new WINOperationsCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(277, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerCapability(); }});
		elements.add(new ElementDescriptor(281, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_wINOperationsCapability(); }});
	}
	public WINCapability(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
