package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PQOS_VerticalPosition extends OCTET_STRING{

	public PQOS_VerticalPosition(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
