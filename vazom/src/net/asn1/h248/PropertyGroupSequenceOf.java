package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PropertyGroupSequenceOf extends SEQUENCE{

	public PropertyGroup new_child(){
		return new PropertyGroup();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public PropertyGroup getChild(int index){
		return (PropertyGroup)of_children.get(index);
	}
	public PropertyGroupSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
