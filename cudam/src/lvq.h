// (L)ength (V)alue (Q)ueue
// - circular FIFO queue
// - stores actual data internally, no pointers are stored
#include <pthread.h>

class LVQ{
private:
	int max_q_size;
	int max_i_size;
	int current_size;
	char *queue;
	// mutex for multi threaded environment
	pthread_mutex_t q_lock;
	void init();

public:
	LVQ(int max_q_size, int max_i_size);	
	~LVQ();
	// push data at the end of queue
	// data is copied to internal queue, no pointers used
	void push(char *, int length);
	// put data in buffer, return length of buffer, pop from queue
	int pop(char *buffer);
	// create a snapshot of current queue, return current length
	int snapshot(char *buffer);

};
