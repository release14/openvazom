package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Number_of_messages extends OptionalParameterBase {

	public Number_of_messages(){
		type = OptionalParameterType.NUMBER_OF_MESSAGES;
	}
	int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
