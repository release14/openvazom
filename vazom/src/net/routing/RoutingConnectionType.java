package net.routing;

public enum RoutingConnectionType {
	M3UA,
	MTP3,
	SMPP;
}
