package net.smstpdu.tpdu.udh;

public abstract class InformationElement {
	public InformationElementType type;
	public int length;
	
	
	public abstract byte[] encode();
	
	public abstract void init(byte[] data);
	

}
