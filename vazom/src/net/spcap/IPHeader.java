package net.spcap;

public class IPHeader {
	public int vesion;
	public int headerLength;
	public int totalLength;
	public IPProtocolType protocol;
	public int identification;
	public int fragment_offset;
	public boolean do_not_fragment;
	public boolean more_fragments;
	public String source;
	public String destination;
	public byte[] payload;
	
}
