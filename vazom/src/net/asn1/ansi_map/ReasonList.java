package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReasonList extends ENUMERATED{

	public static final int _unknown = 0;
	public static final int _unable_to_configure_ISLP = 1;
	public static final int _iSLP_failure = 2;
	public static final int _service_allowed_but_facilities_not_available = 3;
	public static final int _service_not_allowed = 4;
	public static final int _no_Response_to_TMSI_assignment = 5;
	public static final int _required_parameters_unavailable = 6;
	public ReasonList(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
