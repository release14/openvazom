package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.tmt.TMTType;

public class TrafficModeType extends M3UAParameter {
	public TMTType trafficModeType;
	public TrafficModeType(){
		type = M3UAParameterType.TRAFFIC_MODE_TYPE;
	}
	public TrafficModeType(TMTType tmt){
		type = M3UAParameterType.TRAFFIC_MODE_TYPE;
		value = new byte[]{(byte)(tmt.getId() << 24), (byte)(tmt.getId() << 16), (byte)(tmt.getId() << 8), (byte)(tmt.getId() & 0xFF)};
		
	}

	public void setTMT(TMTType tmt){
		value = new byte[]{(byte)(tmt.getId() << 24), (byte)(tmt.getId() << 16), (byte)(tmt.getId() << 8), (byte)(tmt.getId() & 0xFF)};
	}
	public void init(byte[] data) {
		value = data;
		trafficModeType = TMTType.get((data[0] << 24) + (data[1] << 16) + (data[2] << 8) + (data[3] & 0xFF));
	}



}
