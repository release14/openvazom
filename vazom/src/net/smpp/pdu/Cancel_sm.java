package net.smpp.pdu;

import java.util.ArrayList;

import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.smpp.parameters.ServiceType;
import net.utils.Utils;

public class Cancel_sm extends PDUBase {
	public ServiceType service_type;
	public String message_id;
	public TypeOfNumber source_addr_ton;
	public NumberingPlan source_addr_npi;
	public String source_addr;
	public TypeOfNumber dest_addr_ton;
	public NumberingPlan dest_addr_npi;
	public String destination_addr;

	public Cancel_sm(){
		type = PDUType.CANCEL_SM;
	}

	public byte[] encode(){
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		// submit sm
		if(service_type == null || service_type == ServiceType.DEFAULT){
			Utils.bytesToLst(buff, Utils.Cstr_encode(""));
		}else{
			Utils.bytesToLst(buff, Utils.Cstr_encode(service_type.toString()));
		}
		Utils.bytesToLst(buff, Utils.Cstr_encode(message_id));
		buff.add((byte)source_addr_ton.getId());
		buff.add((byte)source_addr_npi.getId());
		Utils.bytesToLst(buff, Utils.Cstr_encode(source_addr));
		buff.add((byte)dest_addr_ton.getId());
		buff.add((byte)dest_addr_npi.getId());
		Utils.bytesToLst(buff, Utils.Cstr_encode(destination_addr));
		// common
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		return Utils.list2array(buff);
	
	}
	
	public void init(byte[] data) {
		// Service type
		if(Utils.Cstr_decode(data, byte_pos).equals("")){
			service_type = ServiceType.DEFAULT;
			byte_pos++;
		}
		else{
			service_type = ServiceType.valueOf(Utils.Cstr_decode(data, byte_pos));
			byte_pos += service_type.toString().length() + 1;
		}
		// message id
		message_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += message_id.length() + 1;
		// Source addr ton
		source_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Source addr npi
		source_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Source addr
		source_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += source_addr.length() + 1;
		// Dest addr ton
		dest_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Dest addr npi
		dest_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Dest addr
		destination_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += destination_addr.length() + 1;

	}

}
