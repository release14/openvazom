package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudMediaDescriptor extends SEQUENCE{

	public IndAudTerminationStateDescriptor get_termStateDescr(){
		return (IndAudTerminationStateDescriptor)elements.get(0).data;
	}
	public IndAudTerminationStateDescriptor new_termStateDescr(){
		return new IndAudTerminationStateDescriptor();
	}

	public IndAudMediaDescriptorStreamsChoiceWrapper get_streams(){
		return (IndAudMediaDescriptorStreamsChoiceWrapper)elements.get(1).data;
	}
	public IndAudMediaDescriptorStreamsChoiceWrapper new_streams(){
		return new IndAudMediaDescriptorStreamsChoiceWrapper();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_termStateDescr(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streams(); }});
	}
	public IndAudMediaDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
