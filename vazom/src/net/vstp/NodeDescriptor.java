package net.vstp;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;

public class NodeDescriptor {
	public String ip;
	public int port;
	private int sctp_id;
	private Reader reader;
	private Writer writer;
	private Thread reader_t;
	private Thread writer_t;
	private boolean stopping;
	public ConcurrentLinkedQueue<MessageDescriptor> out_queue;
	public ConcurrentLinkedQueue<MessageDescriptor> in_queue;

	private class Reader implements Runnable{
		MessageDescriptor md = null;
		SSCTPDescriptor sd = null;
		public void run() {
			while(!stopping){
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						md = VSTP.decode(sd.payload);
						in_queue.offer(md);
						
					}
					
				}
			}
		}
		
	}
	
	private class Writer implements Runnable{
		MessageDescriptor md = null;
		public void run() {
			while(!stopping){
				md = out_queue.poll();
				if(md != null) SSctp.send(sctp_id, md.encode(), 0);
				else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
			}
			
		}
		
	}	
	public NodeDescriptor(String _ip, int _port){
		ip = _ip;
		port = _port;
		// init queues
		in_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		// init sctp connection
		sctp_id = SSctp.initClient(_ip, _port, 16, null, 0);
		// init reader thread
		reader = new Reader();
		reader_t = new Thread(reader, "SG_READER");
		reader_t.start();
		// init writer thread
		writer = new Writer();
		writer_t = new Thread(writer, "SG_WRITER");
		writer_t.start();
		
		
	}
}
