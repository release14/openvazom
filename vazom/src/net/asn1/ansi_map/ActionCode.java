package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ActionCode extends OCTET_STRING{

	public ActionCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
