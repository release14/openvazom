package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SigParameterExtraInfoChoiceWrapper extends SEQUENCE{

	public SigParameterExtraInfoChoice get_sigParameterExtraInfoChoice(){
		return (SigParameterExtraInfoChoice)elements.get(0).data;
	}
	public SigParameterExtraInfoChoice new_sigParameterExtraInfoChoice(){
		return new SigParameterExtraInfoChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sigParameterExtraInfoChoice(); }});
	}
	public SigParameterExtraInfoChoiceWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
