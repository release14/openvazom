version=$1
dist=$2
cd $dist/sgn/lib
ln -s ../../lib/utils-${version}.jar 
ln -s ../../lib/asn1-${version}.jar
ln -s ../../lib/config-${version}.jar
ln -s ../../lib/log4j-1.2.16.jar
ln -s ../../lib/log4jconfig-${version}.jar
ln -s ../../lib/sgn-${version}.jar
ln -s ../../lib/vstp-${version}.jar
ln -s ../../lib/hplmnr-${version}.jar
ln -s ../../lib/m3ua-conn-${version}.jar
ln -s ../../lib/m3ua-${version}.jar
ln -s ../../lib/sccp-${version}.jar
ln -s ../../lib/sctp-${version}.jar
ln -s ../../lib/stats-${version}.jar
ln -s ../../lib/ds-${version}.jar
ln -s ../../lib/spcap-${version}.jar
ln -s ../../lib/ber-${version}.jar
ln -s ../../lib/smstpdu-${version}.jar
ln -s ../../lib/sgc-${version}.jar
ln -s ../../lib/hlr-${version}.jar
ln -s ../../lib/smpp-${version}.jar
ln -s ../../lib/smpp-conn-${version}.jar
ln -s ../../lib/routing-${version}.jar
ln -s ../../lib/mtp3-conn-${version}.jar
ln -s ../../lib/mtp3-${version}.jar
ln -s ../../lib/mtp2-${version}.jar
ln -s ../../lib/cli-${version}.jar
ln -s ../../lib/smsfs-${version}.jar
ln -s ../../lib/security-${version}.jar
ln -s ../../lib/hasp-srm-api.jar
ln -s ../../lib/logging-${version}.jar
ln -s ../../lib/isup-${version}.jar
ln -s ../../lib/smpp2ss7-${version}.jar
