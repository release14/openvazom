package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ABRT_apdu extends SEQUENCE{

	public ABRT_source get_abort_source(){
		return (ABRT_source)elements.get(0).data;
	}
	public ABRT_source new_abort_source(){
		return new ABRT_source();
	}

	public User_information get_user_information(){
		return (User_information)elements.get(1).data;
	}
	public User_information new_user_information(){
		return new User_information();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_abort_source(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_user_information(); }});
	}
	public ABRT_apdu(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 4;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.APPLICATION;
	}
}
