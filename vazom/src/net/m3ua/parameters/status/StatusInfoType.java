package net.m3ua.parameters.status;

import java.util.HashMap;

public enum StatusInfoType {
	AS_INACTIVE(StatusType.AS_STATE_CHANGE.getId(), 0x02),
	AS_ACTIVE(StatusType.AS_STATE_CHANGE.getId(), 0x03),
	AS_PENDING(StatusType.AS_STATE_CHANGE.getId(), 0x04),

	INSUFFICIENT_ASP_RESOURCES_ACTIVE_IN_AS(StatusType.OTHER.getId(), 0x01),
	ALTERNATE_ASP_ACTIVE(StatusType.OTHER.getId(), 0x02),
	ASP_FAILURE(StatusType.OTHER.getId(), 0x03);
	
	private int id;
	private int type_id;
	
	private static final HashMap<String, StatusInfoType> lookup = new HashMap<String, StatusInfoType>();
	static{
		for(StatusInfoType td : StatusInfoType.values()){
			lookup.put(td.type_id + ":" + td.id, td);
		}
	}
	public int getId(){ return id; }
	public static StatusInfoType get(int id){ return lookup.get(id); }
	private StatusInfoType(int _type_id, int _id){ id = _id; type_id = _type_id; }		
}
