package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMATargetMeasurementInformationType extends CDMATargetMeasurementInformation{

	public CDMATargetMeasurementInformationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 133;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
