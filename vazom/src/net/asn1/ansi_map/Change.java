package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Change extends ENUMERATED{

	public static final int _setDataItemToDefaultValue = 1;
	public static final int _addDataItem = 2;
	public static final int _deleteDataItem = 3;
	public static final int _replaceDataItemWithAssociatedDataValue = 4;
	public Change(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
