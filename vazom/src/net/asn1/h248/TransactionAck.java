package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionAck extends SEQUENCE{

	public TransactionId get_firstAck(){
		return (TransactionId)elements.get(0).data;
	}
	public TransactionId new_firstAck(){
		return new TransactionId();
	}

	public TransactionId get_lastAck(){
		return (TransactionId)elements.get(1).data;
	}
	public TransactionId new_lastAck(){
		return new TransactionId();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_firstAck(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lastAck(); }});
	}
	public TransactionAck(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
