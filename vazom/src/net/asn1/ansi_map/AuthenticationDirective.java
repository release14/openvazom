package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationDirective extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public AuthenticationAlgorithmVersion get_authenticationAlgorithmVersion(){
		return (AuthenticationAlgorithmVersion)elements.get(2).data;
	}
	public AuthenticationAlgorithmVersion new_authenticationAlgorithmVersion(){
		return new AuthenticationAlgorithmVersion();
	}

	public AuthenticationResponseReauthentication get_authenticationResponseReauthentication(){
		return (AuthenticationResponseReauthentication)elements.get(3).data;
	}
	public AuthenticationResponseReauthentication new_authenticationResponseReauthentication(){
		return new AuthenticationResponseReauthentication();
	}

	public AuthenticationResponseUniqueChallenge get_authenticationResponseUniqueChallenge(){
		return (AuthenticationResponseUniqueChallenge)elements.get(4).data;
	}
	public AuthenticationResponseUniqueChallenge new_authenticationResponseUniqueChallenge(){
		return new AuthenticationResponseUniqueChallenge();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(5).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(6).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(7).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public CaveKey get_caveKey(){
		return (CaveKey)elements.get(8).data;
	}
	public CaveKey new_caveKey(){
		return new CaveKey();
	}

	public DenyAccess get_denyAccess(){
		return (DenyAccess)elements.get(9).data;
	}
	public DenyAccess new_denyAccess(){
		return new DenyAccess();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(10).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(11).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public RandomVariableReauthentication get_randomVariableReauthentication(){
		return (RandomVariableReauthentication)elements.get(12).data;
	}
	public RandomVariableReauthentication new_randomVariableReauthentication(){
		return new RandomVariableReauthentication();
	}

	public MEID get_meid(){
		return (MEID)elements.get(13).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MobileStationMIN get_mobileStationMIN(){
		return (MobileStationMIN)elements.get(14).data;
	}
	public MobileStationMIN new_mobileStationMIN(){
		return new MobileStationMIN();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(15).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public RandomVariableSSD get_randomVariableSSD(){
		return (RandomVariableSSD)elements.get(16).data;
	}
	public RandomVariableSSD new_randomVariableSSD(){
		return new RandomVariableSSD();
	}

	public RandomVariableUniqueChallenge get_randomVariableUniqueChallenge(){
		return (RandomVariableUniqueChallenge)elements.get(17).data;
	}
	public RandomVariableUniqueChallenge new_randomVariableUniqueChallenge(){
		return new RandomVariableUniqueChallenge();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(18).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(19).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SharedSecretData get_sharedSecretData(){
		return (SharedSecretData)elements.get(20).data;
	}
	public SharedSecretData new_sharedSecretData(){
		return new SharedSecretData();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(21).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public SSDNotShared get_ssdnotShared(){
		return (SSDNotShared)elements.get(22).data;
	}
	public SSDNotShared new_ssdnotShared(){
		return new SSDNotShared();
	}

	public UpdateCount get_updateCount(){
		return (UpdateCount)elements.get(23).data;
	}
	public UpdateCount new_updateCount(){
		return new UpdateCount();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(77, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationAlgorithmVersion(); }});
		elements.add(new ElementDescriptor(182, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponseReauthentication(); }});
		elements.add(new ElementDescriptor(37, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponseUniqueChallenge(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(316, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_caveKey(); }});
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_denyAccess(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(191, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableReauthentication(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(184, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileStationMIN(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(42, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableSSD(); }});
		elements.add(new ElementDescriptor(43, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableUniqueChallenge(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(46, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sharedSecretData(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(52, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ssdnotShared(); }});
		elements.add(new ElementDescriptor(51, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_updateCount(); }});
	}
	public AuthenticationDirective(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
