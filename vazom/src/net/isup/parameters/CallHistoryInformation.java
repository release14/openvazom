package net.isup.parameters;

import net.isup.ParameterType;

public class CallHistoryInformation extends ParameterBase {
	public byte[] value;
	
	public CallHistoryInformation(){
		type = ParameterType.CALL_HISTORY_INFORMATION;
	}
	public void init(byte[] data) {
		value = data;
	}

}
