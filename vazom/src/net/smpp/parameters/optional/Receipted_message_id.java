package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;
import net.utils.Utils;

public class Receipted_message_id extends OptionalParameterBase {
	public String value;
	public Receipted_message_id(){
		type = OptionalParameterType.RECEIPTED_MESSAGE_ID;
	}
	
	public void init(byte[] data) {
		value = Utils.Cstr_decode(data, 0);

	}

}
