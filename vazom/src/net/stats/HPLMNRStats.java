package net.stats;

public class HPLMNRStats {
	public long SCTP_PACKET_COUNT;
	public long HPLMNR_NOT_ROUTABLE;
	
	public long HPLMNR_HLR_IN_REQUEST;
	public long HPLMNR_HLR_OUT_REQUEST;
	public long HPLMNR_HLR_OUT_ACK;
	public long HPLMNR_HLR_OUT_ERR;
	public long HPLMNR_HLR_IN_REPLY_ACK;
	public long HPLMNR_HLR_IN_REPLY_ERR;
	public long HPLMNR_HLR_IN_REPLY_ABORT;
	public long HPLMNR_MT_IN_BEGIN;
	public long HPLMNR_MT_IN_CONTINUE;
	public long HPLMNR_MT_IN_END;
	public long HPLMNR_MT_TCAP_IN_BEGIN;
	public long HPLMNR_MT_TCAP_IN_CONTINUE;
	public long HPLMNR_MT_TCAP_IN_END;
	public long HPLMNR_MT_OUT_BEGIN;
	public long HPLMNR_MT_OUT_CONTINUE;
	public long HPLMNR_MT_OUT_END;
	public long HPLMNR_MT_TCAP_OUT_BEGIN;
	public long HPLMNR_MT_TCAP_OUT_CONTINUE;
	public long HPLMNR_MT_TCAP_OUT_END;
	public long HPLMNR_MT_OUT_CONTINUE_ACK;
	public long HPLMNR_MT_OUT_CONTINUE_ERR;
	public long HPLMNR_MT_OUT_CONTINUE_TCAP_REPLY;
	public long HPLMNR_MT_OUT_END_ACK;
	public long HPLMNR_MT_OUT_END_ERR;
	public long HPLMNR_MT_OUT_END_TCAP_REPLY;
	public long HPLMNR_MT_OUT_ABORT_REPLY;
	public long HPLMNR_MT_IN_ACK;
	public long HPLMNR_MT_IN_ERR;
	public long HPLMNR_MT_IN_TCAP_REPLY;
	public long HPLMNR_MO_IN;
	public long HPLMNR_MO_OUT;
	public long HPLMNR_MO_OUT_REPLY;
	public long HPLMNR_MO_IN_REPLY;
	
}
