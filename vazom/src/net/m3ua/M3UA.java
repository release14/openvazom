package net.m3ua;

import java.util.Arrays;
import net.m3ua.messages.ASPAC;
import net.m3ua.messages.ASPACACK;
import net.m3ua.messages.ASPDN;
import net.m3ua.messages.ASPDNACK;
import net.m3ua.messages.ASPIA;
import net.m3ua.messages.ASPIAACK;
import net.m3ua.messages.ASPUP;
import net.m3ua.messages.ASPUPACK;
import net.m3ua.messages.BEAT;
import net.m3ua.messages.BEATACK;
import net.m3ua.messages.DATA;
import net.m3ua.messages.DAUD;
import net.m3ua.messages.DAVA;
import net.m3ua.messages.DEREGREQ;
import net.m3ua.messages.DEREGRSP;
import net.m3ua.messages.DRST;
import net.m3ua.messages.DUNA;
import net.m3ua.messages.DUPU;
import net.m3ua.messages.ERR;
import net.m3ua.messages.NTFY;
import net.m3ua.messages.REGREQ;
import net.m3ua.messages.REGRSP;
import net.m3ua.messages.SCON;

public class M3UA {

	
	public static M3UAPacket prepareNew(M3UAMessageType type){
		M3UAPacket packet = new M3UAPacket();
		packet.version = 1;
		packet.messageType = type;
		packet.messageClass = M3UAMessageClass.get(type.getClassId());
		switch(type){
			case ASPAC: packet.message = new ASPAC(); break;
			case ASPAC_ACK: packet.message = new ASPACACK(); break;
			case ASPDN: packet.message = new ASPDN(); break;
			case ASPDN_ACK: packet.message = new ASPDNACK(); break;
			case ASPIA: packet.message = new ASPIA(); break;
			case ASPIA_ACK: packet.message = new ASPIAACK(); break;
			case ASPUP: packet.message = new ASPUP(); break;
			case ASPUP_ACK: packet.message = new ASPUPACK(); break;
			case BEAT: packet.message = new BEAT(); break;
			case BEAT_ACK: packet.message = new BEATACK(); break;
			case DATA: packet.message = new DATA(); break;
			case DAUD: packet.message = new DAUD(); break;
			case DAVA: packet.message = new DAVA(); break;
			case DEREG_REQ: packet.message = new DEREGREQ(); break;
			case DEREG_RSP: packet.message = new DEREGRSP(); break;
			case DRST: packet.message = new DRST(); break;
			case DUNA: packet.message = new DUNA(); break;
			case DUPU: packet.message = new DUPU(); break;
			case ERR: packet.message = new ERR(); break;
			case NTFY: packet.message = new NTFY(); break;
			case REG_REQ: packet.message = new REGREQ(); break;
			case REG_RSP: packet.message = new REGRSP(); break;
			case SCON: packet.message = new SCON(); break;
		}
		if(packet != null) packet.message.initNew();
		return packet;
		
	}
	
	
	public static M3UAPacket decode(byte[] data){
		M3UAPacket res = new M3UAPacket();
		// min hedaer length
		if(data.length >=8){
			res.version = data[0] & 0xFF;
			// only version 1.0 supported
			if(res.version == 1){
				res.messageClass = M3UAMessageClass.get(data[2] & 0xFF);
				if(res.messageClass != null){
					res.messageType = M3UAMessageType.get(data[3] & 0xFF, res.messageClass.getId());
					if(res.messageType != null){
						res.messageLength = (data[4] << 24) + (data[5] << 16) + (data[6] << 8) + (data[7] & 0xFF);
						if(res.messageLength > 0){
							switch(res.messageType){
								case ASPAC: res.message = new ASPAC(); break;
								case ASPAC_ACK: res.message = new ASPACACK(); break;
								case ASPDN: res.message = new ASPDN(); break;
								case ASPDN_ACK: res.message = new ASPDNACK(); break;
								case ASPIA: res.message = new ASPIA(); break;
								case ASPIA_ACK: res.message = new ASPIAACK(); break;
								case ASPUP: res.message = new ASPUP(); break;
								case ASPUP_ACK: res.message = new ASPUPACK(); break;
								case BEAT: res.message = new BEAT(); break;
								case BEAT_ACK: res.message = new BEATACK(); break;
								case DATA: res.message = new DATA(); break;
								case DAUD: res.message = new DAUD(); break;
								case DAVA: res.message = new DAVA(); break;
								case DEREG_REQ: res.message = new DEREGREQ(); break;
								case DEREG_RSP: res.message = new DEREGRSP(); break;
								case DRST: res.message = new DRST(); break;
								case DUNA: res.message = new DUNA(); break;
								case DUPU: res.message = new DUPU(); break;
								case ERR: res.message = new ERR(); break;
								case NTFY: res.message = new NTFY(); break;
								case REG_REQ: res.message = new REGREQ(); break;
								case REG_RSP: res.message = new REGRSP(); break;
								case SCON: res.message = new SCON(); break;
							}				
							if(res.message != null) res.message.init(Arrays.copyOfRange(data, 8, data.length));							
						}
					}
				}
			}		
		}
		

		return res;
	}
}