package net.m3ua.parameters.apc;

public class ApcITU14bit extends Apc {

	public int zone;
	public int region;
	public int sp;
	
	public void init(byte[] data) {
		mask = data[0] & 0xFF;
		zone = (data[2] & 0x38 >> 3);
		region = ((data[2] & 0x07) << 5) + ((data[3] & 0xF8) >> 3);
		sp = data[3] & 0x07;
	}

}
