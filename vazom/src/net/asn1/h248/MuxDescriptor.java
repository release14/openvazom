package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MuxDescriptor extends SEQUENCE{

	public MuxType get_muxType(){
		return (MuxType)elements.get(0).data;
	}
	public MuxType new_muxType(){
		return new MuxType();
	}

	public TerminationIDSequenceOf get_termList(){
		return (TerminationIDSequenceOf)elements.get(1).data;
	}
	public TerminationIDSequenceOf new_termList(){
		return new TerminationIDSequenceOf();
	}

	public NonStandardData get_nonStandardData(){
		return (NonStandardData)elements.get(2).data;
	}
	public NonStandardData new_nonStandardData(){
		return new NonStandardData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_muxType(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_termList(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nonStandardData(); }});
	}
	public MuxDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
