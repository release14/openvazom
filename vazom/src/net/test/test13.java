package net.test;

import net.db.DBRecordSMS;
import net.utils.Utils;
import net.vstp.MessageDescriptor;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;


public class test13 {
	public static String ansi_box(String data){
		String res = "";
		int l = data.length();
		res += "+";
		for(int i = 0; i<l; i++) res += "-";
		res += "+\n";
		res += "|";
		res += data;
		res += "|\n";
		res += "+";
		for(int i = 0; i<l; i++) res += "-";
		res += "+";
		return res;
		
	}
	
	public static void main(String[] args) {
		long l = 1000000000;
		double d = 10000129.44;
		System.out.println(String.format("%,d", l));
		System.out.println(String.format("%,.2f", d));
		System.out.println(String.format("%20s", "aa"));
		System.out.println(ansi_box("Connection1"));
		System.exit(0);
		//StatsManager.init();
		//CMDIListener.init(20000);
		//net.sgn.cmdi.CMDIListener.init(20000);
		String data = "NA:-:NA:NA:-:CMDI:1\n" +
						"VSTP.CMDI.CMD=UPTIME\n" +
						"END\n";
		
		MessageDescriptor md = VSTP.decode(data.getBytes());
		//System.out.println(new String(md.encode()));
		byte[] hdata = Utils.strhex2bytes("555054494d453a2030206461792873292c2030303a30303a3132");
		System.out.println(new String(hdata));
		
	}

}
