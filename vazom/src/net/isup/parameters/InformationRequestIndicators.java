package net.isup.parameters;

import net.isup.ParameterType;

public class InformationRequestIndicators extends ParameterBase {

	public InformationRequestIndicators(){
		type = ParameterType.INFORMATION_REQUEST_INDICATORS;
	}
}
