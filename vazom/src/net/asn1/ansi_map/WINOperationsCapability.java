package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class WINOperationsCapability extends OCTET_STRING{

	public WINOperationsCapability(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
