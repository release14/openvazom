package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemPage2 extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(2).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(3).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(4).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(5).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CDMABandClass get_cdmaBandClass(){
		return (CDMABandClass)elements.get(6).data;
	}
	public CDMABandClass new_cdmaBandClass(){
		return new CDMABandClass();
	}

	public CDMAMobileProtocolRevision get_cdmaMobileProtocolRevision(){
		return (CDMAMobileProtocolRevision)elements.get(7).data;
	}
	public CDMAMobileProtocolRevision new_cdmaMobileProtocolRevision(){
		return new CDMAMobileProtocolRevision();
	}

	public ControlChannelMode get_controlChannelMode(){
		return (ControlChannelMode)elements.get(8).data;
	}
	public ControlChannelMode new_controlChannelMode(){
		return new ControlChannelMode();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(9).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(10).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public CDMASlotCycleIndex get_cdmaSlotCycleIndex(){
		return (CDMASlotCycleIndex)elements.get(11).data;
	}
	public CDMASlotCycleIndex new_cdmaSlotCycleIndex(){
		return new CDMASlotCycleIndex();
	}

	public CDMAStationClassMark get_cdmaStationClassMark(){
		return (CDMAStationClassMark)elements.get(12).data;
	}
	public CDMAStationClassMark new_cdmaStationClassMark(){
		return new CDMAStationClassMark();
	}

	public CDMAStationClassMark2 get_cdmaStationClassMark2(){
		return (CDMAStationClassMark2)elements.get(13).data;
	}
	public CDMAStationClassMark2 new_cdmaStationClassMark2(){
		return new CDMAStationClassMark2();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(14).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DisplayText2 get_displayText2(){
		return (DisplayText2)elements.get(15).data;
	}
	public DisplayText2 new_displayText2(){
		return new DisplayText2();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(16).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(17).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(18).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(19).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MSIDUsage get_mSIDUsage(){
		return (MSIDUsage)elements.get(20).data;
	}
	public MSIDUsage new_mSIDUsage(){
		return new MSIDUsage();
	}

	public NetworkTMSI get_networkTMSI(){
		return (NetworkTMSI)elements.get(21).data;
	}
	public NetworkTMSI new_networkTMSI(){
		return new NetworkTMSI();
	}

	public NonPublicData get_nonPublicData(){
		return (NonPublicData)elements.get(22).data;
	}
	public NonPublicData new_nonPublicData(){
		return new NonPublicData();
	}

	public PageCount get_pageCount(){
		return (PageCount)elements.get(23).data;
	}
	public PageCount new_pageCount(){
		return new PageCount();
	}

	public PageIndicator get_pageIndicator(){
		return (PageIndicator)elements.get(24).data;
	}
	public PageIndicator new_pageIndicator(){
		return new PageIndicator();
	}

	public PagingFrameClass get_pagingFrameClass(){
		return (PagingFrameClass)elements.get(25).data;
	}
	public PagingFrameClass new_pagingFrameClass(){
		return new PagingFrameClass();
	}

	public PageResponseTime get_pageResponseTime(){
		return (PageResponseTime)elements.get(26).data;
	}
	public PageResponseTime new_pageResponseTime(){
		return new PageResponseTime();
	}

	public PSID_RSIDList get_pSID_RSIDList(){
		return (PSID_RSIDList)elements.get(27).data;
	}
	public PSID_RSIDList new_pSID_RSIDList(){
		return new PSID_RSIDList();
	}

	public RedirectingNumberString get_redirectingNumberString(){
		return (RedirectingNumberString)elements.get(28).data;
	}
	public RedirectingNumberString new_redirectingNumberString(){
		return new RedirectingNumberString();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(29).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public TDMADataFeaturesIndicator get_tdmaDataFeaturesIndicator(){
		return (TDMADataFeaturesIndicator)elements.get(30).data;
	}
	public TDMADataFeaturesIndicator new_tdmaDataFeaturesIndicator(){
		return new TDMADataFeaturesIndicator();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(31).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(32).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public UserZoneData get_userZoneData(){
		return (UserZoneData)elements.get(33).data;
	}
	public UserZoneData new_userZoneData(){
		return new UserZoneData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(170, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaBandClass(); }});
		elements.add(new ElementDescriptor(66, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileProtocolRevision(); }});
		elements.add(new ElementDescriptor(199, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelMode(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(166, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaSlotCycleIndex(); }});
		elements.add(new ElementDescriptor(59, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark(); }});
		elements.add(new ElementDescriptor(177, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark2(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(299, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText2(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(327, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSIDUsage(); }});
		elements.add(new ElementDescriptor(233, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkTMSI(); }});
		elements.add(new ElementDescriptor(200, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nonPublicData(); }});
		elements.add(new ElementDescriptor(300, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pageCount(); }});
		elements.add(new ElementDescriptor(71, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pageIndicator(); }});
		elements.add(new ElementDescriptor(210, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pagingFrameClass(); }});
		elements.add(new ElementDescriptor(301, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pageResponseTime(); }});
		elements.add(new ElementDescriptor(203, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pSID_RSIDList(); }});
		elements.add(new ElementDescriptor(101, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberString(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(221, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaDataFeaturesIndicator(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(209, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_userZoneData(); }});
	}
	public InterSystemPage2(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
