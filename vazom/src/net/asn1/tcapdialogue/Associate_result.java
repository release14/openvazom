package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Associate_result extends INTEGER{

	public static final int _accepted = 0;
	public static final int _reject_permanent = 1;
	public Associate_result(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
