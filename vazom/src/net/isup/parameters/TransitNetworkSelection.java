package net.isup.parameters;

import net.isup.ParameterType;

public class TransitNetworkSelection extends ParameterBase {
	public TransitNetworkSelection(){
		type = ParameterType.TRANSIT_NETWORK_SELECTION;
	}
}
