package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IST_AlertRes extends SEQUENCE{

	public IST_AlertTimerValue get_istAlertTimer(){
		return (IST_AlertTimerValue)elements.get(0).data;
	}
	public IST_AlertTimerValue new_istAlertTimer(){
		return new IST_AlertTimerValue();
	}

	public NULL get_istInformationWithdraw(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_istInformationWithdraw(){
		return new NULL();
	}

	public CallTerminationIndicator get_callTerminationIndicator(){
		return (CallTerminationIndicator)elements.get(2).data;
	}
	public CallTerminationIndicator new_callTerminationIndicator(){
		return new CallTerminationIndicator();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_istAlertTimer(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_istInformationWithdraw(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callTerminationIndicator(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public IST_AlertRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
