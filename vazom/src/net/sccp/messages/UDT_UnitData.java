package net.sccp.messages;

import java.util.ArrayList;
import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.CalledPA;
import net.sccp.parameters.CallingPA;
import net.sccp.parameters.ProtocolClass;

public class UDT_UnitData extends MessageBase {
	public ProtocolClass protocolClass;
	public CalledPA calledPartyAddress;
	public CallingPA callingPartyAddress;
	public byte[] data;

	
	public UDT_UnitData(){
		super();
		type = MessageType.UDT_UNITDATA;
		pointers = new Pointer[3];
	}
	public void initNew(){
		protocolClass = new ProtocolClass();
		calledPartyAddress = new CalledPA();
		callingPartyAddress = new CallingPA();
		pointers = new Pointer[3];
		
		
		
	}
	
	public byte[] encode(){
		byte[] res = null;
		byte[] tmp_cpa = null;
		byte[] tmp_clpa = null;
		int tmp;
		ArrayList<Byte> packet = new ArrayList<Byte>();
		packet.add((byte)type.getId());
		tmp = protocolClass.protocolClass.getId() + protocolClass.messageHandling.getId(); 
		packet.add((byte)tmp);
		// pointers, calculated later
		packet.add((byte)0x00);
		packet.add((byte)0x00);
		packet.add((byte)0x00);
		// Called Party
		tmp_cpa = calledPartyAddress.encode();
		for(int i = 0; i<tmp_cpa.length; i++) packet.add(tmp_cpa[i]);
		// Calling Party
		tmp_clpa = callingPartyAddress.encode();
		for(int i = 0; i<tmp_clpa.length; i++) packet.add(tmp_clpa[i]);
		
		// set pointers
		packet.set(2, (byte)0x03);
		packet.set(3, (byte)(tmp_cpa.length + 2));
		packet.set(4, (byte)(tmp_cpa.length + tmp_clpa.length + 1));
		
		// data
		if(data != null){
			packet.add((byte)data.length);
			for(int i = 0; i<data.length; i++) packet.add(data[i]);
			
		}
		
		
		
		res = new byte[packet.size()];
		for(int i = 0; i<packet.size(); i++) res[i] = packet.get(i);
		return res;
		
	}
	
	public void init(byte[] _data) {
		byte_pos++;
		// Fixed mandatory
		protocolClass = new ProtocolClass();
		protocolClass.init(new byte[]{_data[byte_pos++]});
		
		// pointers to mandatory variable parts
		pointers[0] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		pointers[1] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		pointers[2] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		
		// First mandatory variable part
		calledPartyAddress = new CalledPA(pointers[0].position + 1);
		calledPartyAddress.init(Arrays.copyOfRange(_data, pointers[0].position + 1, pointers[0].position + 1 + (_data[pointers[0].position] & 0xFF)));

		// Second mandatory variable part
		callingPartyAddress = new CallingPA();
		callingPartyAddress.init(Arrays.copyOfRange(_data, pointers[1].position + 1, pointers[1].position + 1 + (_data[pointers[1].position] & 0xFF)));
		
		// Third mandatory variable part
		data = Arrays.copyOfRange(_data, pointers[2].position + 1, (pointers[2].position + 1) + (_data[pointers[2].position] & 0xFF));
	}
}
