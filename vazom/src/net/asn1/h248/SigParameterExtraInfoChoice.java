package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SigParameterExtraInfoChoice extends CHOICE{

	public Relation get_relation(){
		return (Relation)elements.get(0).data;
	}
	public Relation new_relation(){
		return new Relation();
	}

	public BOOLEAN get_range(){
		return (BOOLEAN)elements.get(1).data;
	}
	public BOOLEAN new_range(){
		return new BOOLEAN();
	}

	public BOOLEAN get_sublist(){
		return (BOOLEAN)elements.get(2).data;
	}
	public BOOLEAN new_sublist(){
		return new BOOLEAN();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_relation(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_range(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sublist(); }});
	}
	public SigParameterExtraInfoChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
