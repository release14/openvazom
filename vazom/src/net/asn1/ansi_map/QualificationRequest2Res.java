package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class QualificationRequest2Res extends SET{

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(0).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public AnalogRedirectRecord get_analogRedirectRecord(){
		return (AnalogRedirectRecord)elements.get(1).data;
	}
	public AnalogRedirectRecord new_analogRedirectRecord(){
		return new AnalogRedirectRecord();
	}

	public AuthorizationDenied get_authorizationDenied(){
		return (AuthorizationDenied)elements.get(2).data;
	}
	public AuthorizationDenied new_authorizationDenied(){
		return new AuthorizationDenied();
	}

	public AuthorizationPeriod get_authorizationPeriod(){
		return (AuthorizationPeriod)elements.get(3).data;
	}
	public AuthorizationPeriod new_authorizationPeriod(){
		return new AuthorizationPeriod();
	}

	public CDMARedirectRecord get_cdmaRedirectRecord(){
		return (CDMARedirectRecord)elements.get(4).data;
	}
	public CDMARedirectRecord new_cdmaRedirectRecord(){
		return new CDMARedirectRecord();
	}

	public ControlChannelMode get_controlChannelMode(){
		return (ControlChannelMode)elements.get(5).data;
	}
	public ControlChannelMode new_controlChannelMode(){
		return new ControlChannelMode();
	}

	public DeniedAuthorizationPeriod get_deniedAuthorizationPeriod(){
		return (DeniedAuthorizationPeriod)elements.get(6).data;
	}
	public DeniedAuthorizationPeriod new_deniedAuthorizationPeriod(){
		return new DeniedAuthorizationPeriod();
	}

	public Digits get_digits(){
		return (Digits)elements.get(7).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(8).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(9).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(10).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(11).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public AuthenticationCapability get_authenticationCapability(){
		return (AuthenticationCapability)elements.get(12).data;
	}
	public AuthenticationCapability new_authenticationCapability(){
		return new AuthenticationCapability();
	}

	public CallingFeaturesIndicator get_callingFeaturesIndicator(){
		return (CallingFeaturesIndicator)elements.get(13).data;
	}
	public CallingFeaturesIndicator new_callingFeaturesIndicator(){
		return new CallingFeaturesIndicator();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(14).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(15).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public ControlNetworkID get_controlNetworkID(){
		return (ControlNetworkID)elements.get(16).data;
	}
	public ControlNetworkID new_controlNetworkID(){
		return new ControlNetworkID();
	}

	public DMH_AccountCodeDigits get_dmh_AccountCodeDigits(){
		return (DMH_AccountCodeDigits)elements.get(17).data;
	}
	public DMH_AccountCodeDigits new_dmh_AccountCodeDigits(){
		return new DMH_AccountCodeDigits();
	}

	public DMH_AlternateBillingDigits get_dmh_AlternateBillingDigits(){
		return (DMH_AlternateBillingDigits)elements.get(18).data;
	}
	public DMH_AlternateBillingDigits new_dmh_AlternateBillingDigits(){
		return new DMH_AlternateBillingDigits();
	}

	public DMH_BillingDigits get_dmh_BillingDigits(){
		return (DMH_BillingDigits)elements.get(19).data;
	}
	public DMH_BillingDigits new_dmh_BillingDigits(){
		return new DMH_BillingDigits();
	}

	public GeographicAuthorization get_geographicAuthorization(){
		return (GeographicAuthorization)elements.get(20).data;
	}
	public GeographicAuthorization new_geographicAuthorization(){
		return new GeographicAuthorization();
	}

	public MEIDValidated get_meidValidated(){
		return (MEIDValidated)elements.get(21).data;
	}
	public MEIDValidated new_meidValidated(){
		return new MEIDValidated();
	}

	public MessageWaitingNotificationCount get_messageWaitingNotificationCount(){
		return (MessageWaitingNotificationCount)elements.get(22).data;
	}
	public MessageWaitingNotificationCount new_messageWaitingNotificationCount(){
		return new MessageWaitingNotificationCount();
	}

	public MessageWaitingNotificationType get_messageWaitingNotificationType(){
		return (MessageWaitingNotificationType)elements.get(23).data;
	}
	public MessageWaitingNotificationType new_messageWaitingNotificationType(){
		return new MessageWaitingNotificationType();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(24).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MobilePositionCapability get_mobilePositionCapability(){
		return (MobilePositionCapability)elements.get(25).data;
	}
	public MobilePositionCapability new_mobilePositionCapability(){
		return new MobilePositionCapability();
	}

	public OriginationIndicator get_originationIndicator(){
		return (OriginationIndicator)elements.get(26).data;
	}
	public OriginationIndicator new_originationIndicator(){
		return new OriginationIndicator();
	}

	public OriginationTriggers get_originationTriggers(){
		return (OriginationTriggers)elements.get(27).data;
	}
	public OriginationTriggers new_originationTriggers(){
		return new OriginationTriggers();
	}

	public PACAIndicator get_pACAIndicator(){
		return (PACAIndicator)elements.get(28).data;
	}
	public PACAIndicator new_pACAIndicator(){
		return new PACAIndicator();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(29).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public QoSPriority get_qosPriority(){
		return (QoSPriority)elements.get(30).data;
	}
	public QoSPriority new_qosPriority(){
		return new QoSPriority();
	}

	public RestrictionDigits get_restrictionDigits(){
		return (RestrictionDigits)elements.get(31).data;
	}
	public RestrictionDigits new_restrictionDigits(){
		return new RestrictionDigits();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(32).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public PSID_RSIDList get_pSID_RSIDList(){
		return (PSID_RSIDList)elements.get(33).data;
	}
	public PSID_RSIDList new_pSID_RSIDList(){
		return new PSID_RSIDList();
	}

	public SMS_OriginationRestrictions get_sms_OriginationRestrictions(){
		return (SMS_OriginationRestrictions)elements.get(34).data;
	}
	public SMS_OriginationRestrictions new_sms_OriginationRestrictions(){
		return new SMS_OriginationRestrictions();
	}

	public SMS_TerminationRestrictions get_sms_TerminationRestrictions(){
		return (SMS_TerminationRestrictions)elements.get(35).data;
	}
	public SMS_TerminationRestrictions new_sms_TerminationRestrictions(){
		return new SMS_TerminationRestrictions();
	}

	public SPINIPIN get_spinipin(){
		return (SPINIPIN)elements.get(36).data;
	}
	public SPINIPIN new_spinipin(){
		return new SPINIPIN();
	}

	public SPINITriggers get_spiniTriggers(){
		return (SPINITriggers)elements.get(37).data;
	}
	public SPINITriggers new_spiniTriggers(){
		return new SPINITriggers();
	}

	public TDMADataFeaturesIndicator get_tdmaDataFeaturesIndicator(){
		return (TDMADataFeaturesIndicator)elements.get(38).data;
	}
	public TDMADataFeaturesIndicator new_tdmaDataFeaturesIndicator(){
		return new TDMADataFeaturesIndicator();
	}

	public TerminationRestrictionCode get_terminationRestrictionCode(){
		return (TerminationRestrictionCode)elements.get(39).data;
	}
	public TerminationRestrictionCode new_terminationRestrictionCode(){
		return new TerminationRestrictionCode();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(40).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public TriggerAddressList get_triggerAddressList(){
		return (TriggerAddressList)elements.get(41).data;
	}
	public TriggerAddressList new_triggerAddressList(){
		return new TriggerAddressList();
	}

	public UserGroup get_userGroup(){
		return (UserGroup)elements.get(42).data;
	}
	public UserGroup new_userGroup(){
		return new UserGroup();
	}

	public NonPublicData get_nonPublicData(){
		return (NonPublicData)elements.get(43).data;
	}
	public NonPublicData new_nonPublicData(){
		return new NonPublicData();
	}

	public UserZoneData get_userZoneData(){
		return (UserZoneData)elements.get(44).data;
	}
	public UserZoneData new_userZoneData(){
		return new UserZoneData();
	}

	public CallingPartyCategory get_callingPartyCategory(){
		return (CallingPartyCategory)elements.get(45).data;
	}
	public CallingPartyCategory new_callingPartyCategory(){
		return new CallingPartyCategory();
	}

	public LIRMode get_lirMode(){
		return (LIRMode)elements.get(46).data;
	}
	public LIRMode new_lirMode(){
		return new LIRMode();
	}

	public RoamingIndication get_roamingIndication(){
		return (RoamingIndication)elements.get(47).data;
	}
	public RoamingIndication new_roamingIndication(){
		return new RoamingIndication();
	}

	public ServiceRedirectionInfo get_serviceRedirectionInfo(){
		return (ServiceRedirectionInfo)elements.get(48).data;
	}
	public ServiceRedirectionInfo new_serviceRedirectionInfo(){
		return new ServiceRedirectionInfo();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(225, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_analogRedirectRecord(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authorizationDenied(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authorizationPeriod(); }});
		elements.add(new ElementDescriptor(229, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaRedirectRecord(); }});
		elements.add(new ElementDescriptor(199, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelMode(); }});
		elements.add(new ElementDescriptor(167, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_deniedAuthorizationPeriod(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(78, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationCapability(); }});
		elements.add(new ElementDescriptor(25, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingFeaturesIndicator(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(307, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlNetworkID(); }});
		elements.add(new ElementDescriptor(140, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AccountCodeDigits(); }});
		elements.add(new ElementDescriptor(141, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AlternateBillingDigits(); }});
		elements.add(new ElementDescriptor(142, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_BillingDigits(); }});
		elements.add(new ElementDescriptor(143, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_geographicAuthorization(); }});
		elements.add(new ElementDescriptor(401, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meidValidated(); }});
		elements.add(new ElementDescriptor(92, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_messageWaitingNotificationCount(); }});
		elements.add(new ElementDescriptor(145, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_messageWaitingNotificationType(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(335, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobilePositionCapability(); }});
		elements.add(new ElementDescriptor(23, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_originationIndicator(); }});
		elements.add(new ElementDescriptor(98, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_originationTriggers(); }});
		elements.add(new ElementDescriptor(146, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pACAIndicator(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(348, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_qosPriority(); }});
		elements.add(new ElementDescriptor(227, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_restrictionDigits(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(203, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pSID_RSIDList(); }});
		elements.add(new ElementDescriptor(115, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginationRestrictions(); }});
		elements.add(new ElementDescriptor(117, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_TerminationRestrictions(); }});
		elements.add(new ElementDescriptor(154, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_spinipin(); }});
		elements.add(new ElementDescriptor(155, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_spiniTriggers(); }});
		elements.add(new ElementDescriptor(221, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaDataFeaturesIndicator(); }});
		elements.add(new ElementDescriptor(24, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationRestrictionCode(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(276, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerAddressList(); }});
		elements.add(new ElementDescriptor(208, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_userGroup(); }});
		elements.add(new ElementDescriptor(200, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nonPublicData(); }});
		elements.add(new ElementDescriptor(209, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_userZoneData(); }});
		elements.add(new ElementDescriptor(355, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyCategory(); }});
		elements.add(new ElementDescriptor(369, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lirMode(); }});
		elements.add(new ElementDescriptor(239, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_roamingIndication(); }});
		elements.add(new ElementDescriptor(238, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceRedirectionInfo(); }});
	}
	public QualificationRequest2Res(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
