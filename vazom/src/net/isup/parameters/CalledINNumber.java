package net.isup.parameters;

import net.isup.ParameterType;

public class CalledINNumber extends ParameterBase {
	public CalledINNumber(){
		type = ParameterType.CALLED_IN_NUMBER;
	}
}
