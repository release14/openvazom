package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class VIDEOTEXSTRING extends ASNType {
	public VIDEOTEXSTRING(){
		super();
		universalTag = ASNTag.VIDEOTEXSTRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
