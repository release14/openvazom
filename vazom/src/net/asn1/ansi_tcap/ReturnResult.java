package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReturnResult extends SEQUENCE{

	public OCTET_STRING get_componentID(){
		return (OCTET_STRING)elements.get(0).data;
	}
	public OCTET_STRING new_componentID(){
		return new OCTET_STRING();
	}

	public PARAMETER get_parameter(){
		return (PARAMETER)elements.get(1).data;
	}
	public PARAMETER new_parameter(){
		return new PARAMETER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(15, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_componentID(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_parameter(); }});
	}
	public ReturnResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
