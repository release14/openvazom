package net.smstpdu;

import java.util.Arrays;

public class AddressString {
	public TypeOfNumber typeOfNumber;
	public NumberingPlan numberingPlan;
	public byte[] digits;
	
	
	public static byte[] encode(AddressString as){
		byte[] res = new byte[as.digits.length + 1];
		res[0] = (byte)(as.typeOfNumber.getId() + as.numberingPlan.getId());
		// no extension
		res[0] += 0x80;
		// digits
		for(int i = 0; i<as.digits.length; i++) res[i + 1] = as.digits[i];
		return res;
		
	}
	public static AddressString decode(byte[] data){
		AddressString res = new AddressString();
		if(data != null){
			if(data.length > 0){
				res.typeOfNumber = TypeOfNumber.get(data[0] & 0x70);
				res.numberingPlan = NumberingPlan.get(data[0] & 0x0F);
				res.digits = Arrays.copyOfRange(data, 1, data.length);
				
			}
			
		}
		return res;
		
	}

}
