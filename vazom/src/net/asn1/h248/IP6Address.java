package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IP6Address extends SEQUENCE{

	public OCTET_STRING get_address(){
		return (OCTET_STRING)elements.get(0).data;
	}
	public OCTET_STRING new_address(){
		return new OCTET_STRING();
	}

	public INTEGER get_portNumber(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_portNumber(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_address(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_portNumber(); }});
	}
	public IP6Address(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
