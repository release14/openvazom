package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TMSIDirective extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(1).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public NetworkTMSIExpirationTime get_networkTMSIExpirationTime(){
		return (NetworkTMSIExpirationTime)elements.get(2).data;
	}
	public NetworkTMSIExpirationTime new_networkTMSIExpirationTime(){
		return new NetworkTMSIExpirationTime();
	}

	public NewNetworkTMSI get_newNetworkTMSI(){
		return (NewNetworkTMSI)elements.get(3).data;
	}
	public NewNetworkTMSI new_newNetworkTMSI(){
		return new NewNetworkTMSI();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(4).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public NetworkTMSI get_networkTMSI(){
		return (NetworkTMSI)elements.get(5).data;
	}
	public NetworkTMSI new_networkTMSI(){
		return new NetworkTMSI();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(234, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_networkTMSIExpirationTime(); }});
		elements.add(new ElementDescriptor(235, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_newNetworkTMSI(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(233, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkTMSI(); }});
	}
	public TMSIDirective(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
