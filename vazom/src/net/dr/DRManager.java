package net.dr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.config.FNConfigData;
import net.db.DBManager;
import net.db.DBRecordBase;
import net.db.DBRecordISUP;
import net.db.DBRecordSMS;
import net.db.DBRecordSRI;
import net.db.DBRecordType;
import net.ds.DataSourceType;
import net.smsfs.SmsfsManager;
import net.smsfs.SmsfsPacket;
import net.smsfs.FilterType;
import net.smsfs.ModeOfOperation;
import net.smstpdu.MultiPartDescriptor;
import net.smstpdu.SmsDirection;
import net.smstpdu.SmsPduType;
import net.smstpdu.SmsType;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.MessageDescriptor;

import org.apache.log4j.Logger;

public class DRManager {
	private static Logger logger=Logger.getLogger(DRManager.class);
	public static ConcurrentLinkedQueue<DRPacket> queue;
	private static ArrayList<DRWorker_vstp> workerLst;
	private static ConcurrentHashMap<String, DBRecordBase> error_correlation_map;
	private static ErrorTimeout_r err_timeout_r;
	private static Thread err_timeout_t;
	public static boolean stopping;

	private static void filterDBR(DBRecordISUP dbr){
		SmsfsPacket fp = null;
		
		if(dbr != null){
			fp = new SmsfsPacket();
			fp.packetMode = (dbr.dataSource == DataSourceType.SCTP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE);
			fp.filterType = FilterType.ISUP;
			fp.filterDescriptor = SmsfsManager.currentFilterISUP;
			// add fields
			// TODO
			
			// save DBR
			fp.extra_data.add(dbr);
			
			// Smsfs queue
			SmsfsManager.queue.offer(fp);
			
		}
	}
	
	
	private static void filterDBR(DBRecordSRI dbr){
		//FilterResult fres = null;
		SmsfsPacket fp = null;
		
		if(dbr != null){
			fp = new SmsfsPacket();
			fp.packetMode = (dbr.dataSource == DataSourceType.SCTP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE);
			fp.filterType = FilterType.HLR;
			fp.filterDescriptor = SmsfsManager.currentFilterHLR;
			fp.bindings.add(dbr.m3ua_data_dpc);
			fp.bindings.add(dbr.m3ua_data_opc);
			fp.bindings.add(dbr.gt_called);
			fp.bindings.add(dbr.gt_calling);
			fp.bindings.add(dbr.gt_called_tt);
			fp.bindings.add(dbr.gt_calling_tt);
			fp.bindings.add((dbr.gt_called_nai != null ? dbr.gt_called_nai.getId() : null));
			fp.bindings.add((dbr.gt_calling_nai != null ? dbr.gt_calling_nai.getId() : null));
			fp.bindings.add((dbr.gt_called_np != null ? dbr.gt_called_np.getId() : null));
			fp.bindings.add((dbr.gt_calling_np != null ? dbr.gt_calling_np.getId() : null));
			fp.bindings.add((dbr.gt_called_gti != null ? dbr.gt_called_gti.getId() : null));
			fp.bindings.add((dbr.gt_calling_gti != null ? dbr.gt_calling_gti.getId() : null));
			fp.bindings.add(dbr.imsi);
			fp.bindings.add(dbr.msisdn);
			fp.bindings.add(dbr.nnn);
			fp.bindings.add(dbr.annn);
			fp.bindings.add(dbr.sca);
			// save DBR
			fp.extra_data.add(dbr);
			
			// Smsfs queue
			SmsfsManager.queue.offer(fp);
			/*
			// regular SMSFS
			if(fres != null){
				dbr.filter_total_score = fres.total_score;
				dbr.filter_action_id = (fres.result && fres.total_score <= ConfigData.max_total_score ? 1 : (fres.result && fres.isUnconditional ? 1 : 2));
				if(fres.result) StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED++;
				else StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED++;

			}
			
			// Repetition
			if(ConfigData.smsfs_rm_status){
				// INTRUSIVE
				if(dbr.dataSource == DataSourceType.SCTP){
					if(ConfigData.smsfs_rm_intrusive_status){
						//TODO
					}
					
				// NON INTRUSIVE
				}else{
					if(ConfigData.smsfs_rm_non_intrusive_status){
						RepetitionManager.non_intrusive_queue.offer(dbr);
						return null;
					}
					
				}
			}
			*/
		}
		
		//return dbr;
	}	
	
	private static void filterDBR_SS7(DBRecordSMS dbr){
		//FilterResult fres = null;
		SmsfsPacket fp = null;
		
		if(dbr != null){
			fp = new SmsfsPacket();
			fp.packetMode = (dbr.dataSource == DataSourceType.SCTP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE);
			fp.filterType = (dbr.direction == SmsDirection.MT ? FilterType.MT : FilterType.MO);
			fp.bindings.add(dbr.gt_called);
			fp.bindings.add(dbr.gt_calling);
			fp.bindings.add(dbr.scoa);
			fp.bindings.add(dbr.scda);
			fp.bindings.add(dbr.imsi);
			fp.bindings.add(dbr.msisdn);
			fp.bindings.add(Integer.toString(dbr.m3ua_data_dpc));
			fp.bindings.add(Integer.toString(dbr.m3ua_data_opc));
			fp.bindings.add(dbr.sms_originating);
			fp.bindings.add(Integer.toString(dbr.sms_originating_enc));
			fp.bindings.add(dbr.sms_destination);
			fp.bindings.add(Integer.toString(dbr.sms_destination_enc));
			fp.bindings.add(dbr.sms_text);
			fp.bindings.add(Integer.toString(dbr.sms_text_enc));
			fp.bindings.add(Integer.toString(dbr.type.getId()));
			fp.bindings.add(Integer.toString(dbr.gt_called_tt));
			fp.bindings.add(Integer.toString(dbr.gt_calling_tt));
			fp.bindings.add((dbr.gt_called_nai != null ? Integer.toString(dbr.gt_called_nai.getId()) : null));
			fp.bindings.add((dbr.gt_calling_nai != null ? Integer.toString(dbr.gt_calling_nai.getId()) : null));
			fp.bindings.add((dbr.gt_called_np != null ? Integer.toString(dbr.gt_called_np.getId()) : null));
			fp.bindings.add((dbr.gt_calling_np != null ? Integer.toString(dbr.gt_calling_np.getId()) : null));
			fp.bindings.add((dbr.gt_called_gti != null ? Integer.toString(dbr.gt_called_gti.getId()) : null));
			fp.bindings.add((dbr.gt_calling_gti != null ? Integer.toString(dbr.gt_calling_gti.getId()) : null));
			

			switch(dbr.direction){
				case MO: fp.filterDescriptor = SmsfsManager.currentFilterMO; break; // fres = Smsfs.executeFilter(SmsfsManager.currentFilterMO, fp); break;
				case MT: fp.filterDescriptor = SmsfsManager.currentFilterMT; break; //fres = Smsfs.executeFilter(SmsfsManager.currentFilterMT, fp); break;
			}
			// save DBR
			fp.extra_data.add(dbr);
			
			// Smsfs queue
			SmsfsManager.queue.offer(fp);

		}		
	}
	private static void filterDBR_SMPP(DBRecordSMS dbr){
		//FilterResult fres = null;
		SmsfsPacket fp = null;
		
		if(dbr != null){
			fp = new SmsfsPacket();
			fp.packetMode = (dbr.dataSource == DataSourceType.SMPP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE);
			fp.filterType = (dbr.direction == SmsDirection.MT ? FilterType.SMPP_MT : FilterType.SMPP_MO);
			fp.bindings.add(dbr.ip_source);
			fp.bindings.add(dbr.ip_destination);
			fp.bindings.add(dbr.tcp_source);
			fp.bindings.add(dbr.tcp_destination);
			fp.bindings.add(dbr.system_id);
			fp.bindings.add(dbr.password); // password
			fp.bindings.add(Integer.toString(dbr.service_type)); // service type
			fp.bindings.add(Integer.toString(dbr.sms_originating_enc));
			fp.bindings.add(Integer.toString(dbr.originator_np)); // originator np
			fp.bindings.add(dbr.sms_originating);
			fp.bindings.add(Integer.toString(dbr.sms_destination_enc));
			fp.bindings.add(Integer.toString(dbr.recipient_np)); // destination np
			fp.bindings.add(dbr.sms_destination);
			fp.bindings.add(Integer.toString(dbr.esm_message_mode)); 
			fp.bindings.add(Integer.toString(dbr.esm_message_type)); 
			fp.bindings.add(Integer.toString(dbr.esm_gsm_features)); 
			fp.bindings.add(Integer.toString(dbr.protocol_id)); 
			fp.bindings.add(Integer.toString(dbr.priority_flag)); 
			fp.bindings.add(dbr.delivery_time);
			fp.bindings.add(dbr.validity_period);
			fp.bindings.add(Integer.toString(dbr.rd_smsc_receipt)); 
			fp.bindings.add(Integer.toString(dbr.rd_sme_ack)); 
			fp.bindings.add(Integer.toString(dbr.rd_intermediate_notification)); 
			fp.bindings.add(Integer.toString(dbr.replace_if_present)); 
			fp.bindings.add(Integer.toString(dbr.sms_text_enc)); 
			fp.bindings.add(Integer.toString(dbr.sm_default_msg_id)); 
			fp.bindings.add(Integer.toString(dbr.sm_msg_length)); 
			fp.bindings.add(dbr.sms_text);
			
			switch(dbr.direction){
				case MO: fp.filterDescriptor = SmsfsManager.currentFilterSMPP_MO; break;
				case MT: fp.filterDescriptor = SmsfsManager.currentFilterSMPP_MT; break;
			}
			// save DBR
			fp.extra_data.add(dbr);
			
			// Smsfs queue
			SmsfsManager.queue.offer(fp);

		}		
	}
	
	private static void filterDBR(DBRecordSMS dbr){
		if(dbr.pdu_type == SmsPduType.SMS_TPDU) filterDBR_SS7(dbr);
		else if(dbr.pdu_type == SmsPduType.SMPP_PDU) filterDBR_SMPP(dbr);

	}
	public static void processDBR(DBRecordBase dbr, ModeOfOperation mode){
		if(dbr.recordType == DBRecordType.SRI) processDBR((DBRecordSRI)dbr, mode);
		else if(dbr.recordType == DBRecordType.SMS) processDBR((DBRecordSMS)dbr, mode);
		else if(dbr.recordType == DBRecordType.ISUP) processDBR_ISUP((DBRecordISUP)dbr, mode);
		
	}


	public static void processDBR_ISUP(DBRecordISUP dbr, ModeOfOperation mode){
		if(dbr != null){
			// DB queue
			switch(mode){
				case NON_INTRUSIVE:
					// no filter
					if(!FNConfigData.smsfs_status){
						if(FNConfigData.dr_non_intrusive_isup_status) DBManager.offer(dbr);
					// filter
					}else{
						if(!FNConfigData.smsfs_isup_non_intrusive_status){
							if(FNConfigData.dr_non_intrusive_isup_status) DBManager.offer(dbr);
							
						}else{
							filterDBR(dbr);
						}
					}
					break;
				case INTRUSIVE:
					if(!FNConfigData.smsfs_status){
						if(FNConfigData.dr_intrusive_isup_status) DBManager.offer(dbr);
					}else{
						if(!FNConfigData.smsfs_isup_intrusive_status){
							if(FNConfigData.dr_intrusive_isup_status) DBManager.offer(dbr);
							
						}else{
							filterDBR(dbr);
						}
					}
					break;
			}			
		}		
	}
	
	public static void processDBR(DBRecordSMS dbr, ModeOfOperation mode){
		if(dbr != null){
			// DB queue
			switch(mode){
				case NON_INTRUSIVE:
					// no filter
					if(!FNConfigData.smsfs_status){
						if(FNConfigData.dr_non_intrusive_sms_status) DBManager.offer(dbr);
					// filter
					}else{
						if(!FNConfigData.smsfs_sms_non_intrusive_status){
							if(FNConfigData.dr_non_intrusive_sms_status) DBManager.offer(dbr);
							
						}else{
							//DBManager.offer(filterDBR(dbr));
							filterDBR(dbr);
						}
					}
					break;
				case INTRUSIVE:
					if(!FNConfigData.smsfs_status){
						if(FNConfigData.dr_intrusive_sms_status) DBManager.offer(dbr);
					}else{
						if(!FNConfigData.smsfs_sms_intrusive_status){
							if(FNConfigData.dr_intrusive_sms_status) DBManager.offer(dbr);
							
						}else{
							//DBManager.offer(filterDBR(dbr));
							filterDBR(dbr);
						}
					}
					break;
			}			
		}
	}

	public static void processDBR(DBRecordSRI dbr, ModeOfOperation mode){
		// DB queue
		switch(mode){
			case NON_INTRUSIVE:
				// no filter
				if(!FNConfigData.smsfs_status){
					if(FNConfigData.dr_non_intrusive_hlr_status) DBManager.offer(dbr);
				// filter
				}else{
					if(!FNConfigData.smsfs_hlr_non_intrusive_status){
						if(FNConfigData.dr_non_intrusive_hlr_status) DBManager.offer(dbr);
						
					}else{
						//DBManager.offer(filterDBR(dbr));
						filterDBR(dbr);
					}
				}
				break;
			case INTRUSIVE:
				if(!FNConfigData.smsfs_status){
					if(FNConfigData.dr_intrusive_hlr_status) DBManager.offer(dbr);
				}else{
					if(!FNConfigData.smsfs_hlr_intrusive_status){
						if(FNConfigData.dr_intrusive_hlr_status) DBManager.offer(dbr);
						
					}else{
						//DBManager.offer(filterDBR(dbr));
						filterDBR(dbr);
					}
				}
				break;
		}
		
	}
	
	public static void offerDRP(DataSourceType ds, DBRecordType type, byte[] data){
		DRPacket drp = null;
		if(data != null){
			switch(ds){
				// INTRUSIVE
				case SCTP: 
					if(FNConfigData.dr_status){
						if(type == DBRecordType.SMS){
							if(FNConfigData.dr_intrusive_sms_status) drp = new DRPacket(ds, data); 
						}else if(type == DBRecordType.SRI){
							if(FNConfigData.dr_intrusive_hlr_status) drp = new DRPacket(ds, data); 
							
						}
					}
					break;
				// NON INTRUSIVE
				default: 
					if(FNConfigData.dr_status){
						if(type == DBRecordType.SMS){
							if(FNConfigData.dr_non_intrusive_sms_status) drp = new DRPacket(ds, data); 
						}else if(type == DBRecordType.SRI){
							if(FNConfigData.dr_non_intrusive_hlr_status) drp = new DRPacket(ds, data); 
							
						}
					}
					break;
			}
			if(drp != null) DRManager.queue.offer(drp);
		}
	}
	public static void offerDRP(DataSourceType ds, DBRecordType type, MessageDescriptor data){
		DRPacket drp = null;
		if(data != null){
			switch(ds){
				// INTRUSIVE
				case SCTP: 
				case SMPP:
				case E1:
					if(FNConfigData.dr_status){
						if(type == DBRecordType.SMS){
							if(FNConfigData.dr_intrusive_sms_status) drp = new DRPacket(ds, data); 
						}else if(type == DBRecordType.SRI){
							if(FNConfigData.dr_intrusive_hlr_status) drp = new DRPacket(ds, data); 
						}else if(type == DBRecordType.ACK_ERR){
							drp = new DRPacket(ds, data);
						}else if(type == DBRecordType.ISUP){
							drp = new DRPacket(ds, data);
						}
					}
					break;
				// NON INTRUSIVE
				default: 
					if(FNConfigData.dr_status){
						if(type == DBRecordType.SMS){
							if(FNConfigData.dr_non_intrusive_sms_status) drp = new DRPacket(ds, data); 
						}else if(type == DBRecordType.SRI){
							if(FNConfigData.dr_non_intrusive_hlr_status) drp = new DRPacket(ds, data); 
						}else if(type == DBRecordType.ACK_ERR){
							drp = new DRPacket(ds, data);
						}else if(type == DBRecordType.ISUP){
							drp = new DRPacket(ds, data);
						}
					}
					break;
			}
			if(drp != null) DRManager.queue.offer(drp);
		}
	}
	
	// Error correlation
	// ISUP
	public static boolean isup_correlation_put(int cic, int dpc, int opc, DBRecordBase dbr){
		if(error_correlation_map.get(cic + ":" + dpc + ":" + opc) == null && error_correlation_map.get(cic + ":" + opc + ":" + dpc) == null){
			error_correlation_map.put(cic + ":" + dpc + ":" + opc, dbr);
			return true;
		}else return false;
	}
	public static DBRecordBase isup_correlation_get(int cic, int dpc, int opc){
		DBRecordBase res = error_correlation_map.get(cic + ":" + dpc + ":" + opc);
		if(res != null) return res;
		return error_correlation_map.get(cic + ":" + opc + ":" + dpc);
	}
	
	public static void isup_correlation_remove(int cic, int dpc, int opc){
		error_correlation_map.remove(cic + ":" + dpc + ":" + opc);
		error_correlation_map.remove(cic + ":" + opc + ":" + dpc);
	}

	
	// Other
	public static boolean errorCorrelationPut(Long id, int invoke_id, DBRecordBase dbr){
		if(error_correlation_map.get(id + ":" + invoke_id) == null){
			error_correlation_map.put(id + ":" + invoke_id, dbr);
			return true;
		}else return false;
	}
	
	public static boolean errorCorrelationPut(String key, DBRecordBase dbr){
		if(error_correlation_map.get(key) == null){
			error_correlation_map.put(key, dbr);
			return true;
			
		}else return false;
	}

	public static DBRecordBase errorCorrelationGet(Long id, int invoke_id){
		return error_correlation_map.get(id + ":" + invoke_id);
	}
	public static DBRecordBase errorCorrelationGet(String key){
		return error_correlation_map.get(key);
	}

	public static DBRecordBase errorCorrelationGetById(Long id){
		String tmp;
		for(String s : error_correlation_map.keySet()){
			tmp = s.split(":")[0];
			if(tmp.equals(id.toString())) return error_correlation_map.get(s);
		}
		return null;
	}
	public static DBRecordBase errorCorrelationGetById(Long id, DBRecordType type){
		String tmp;
		DBRecordBase tmp_dbr;
		for(String s : error_correlation_map.keySet()){
			tmp_dbr = error_correlation_map.get(s);
			if(tmp_dbr.recordType == type){
				tmp = s.split(":")[0];
				if(tmp.equals(id.toString())) return error_correlation_map.get(s);
				
			}
		}
		return null;
	}

	public static void errorCorrelationRemoveById(Long id){
		String tmp;
		for(String s : error_correlation_map.keySet()){
			tmp = s.split(":")[0];
			if(tmp.equals(id.toString())){
				error_correlation_map.remove(s);
				break;
			}
		}
	}

	
	public static boolean errorCorrelationGetRemove(Long id, int invoke_id){
		if(error_correlation_map.get(id + ":" + invoke_id) != null){
			error_correlation_map.remove(id + ":" + invoke_id);
			return true;
		}else return false;
	}
	public static void errorCorrelationRemove(Long id, int invoke_id){
		error_correlation_map.remove(id + ":" + invoke_id);
	}
	public static void errorCorrelationRemove(String key){
		error_correlation_map.remove(key);
	}
	
	
	public static DBRecordSMS reassembleMultiPart(MultiPartDescriptor mpd, SmsDirection direction){
		String tmp = "";
		DBRecordSMS dbr = null;
		HashMap<Integer, String> tmp_map;
		tmp_map = new HashMap<Integer, String>();

		// SMS Text reassembly
		for(int i = 0; i<mpd.messageParts.size(); i++) tmp_map.put(mpd.messageParts.get(i).partNumber, mpd.messageParts.get(i).text);
		for(int i = 1; i<=mpd.totalParts; i++) if(tmp_map.get(i) != null) tmp += tmp_map.get(i); else tmp+= "<SMS_PART_" + i + "_MISSING>";

		// DB packet
		dbr = new DBRecordSMS();
		dbr.direction = direction;
		dbr.type = SmsType.CONCATENATED;
		dbr.gt_called = mpd.GT_called;
		dbr.gt_calling = mpd.GT_calling;
		dbr.scoa = mpd.SCOA;
		dbr.scda = mpd.SCDA;
		dbr.imsi = mpd.IMSI;
		dbr.msisdn = mpd.MSISDN;
		dbr.sms_originating_enc = mpd.originating_encoding.getId();
		dbr.sms_originating = mpd.originating;
		dbr.sms_destination_enc = mpd.destination_encoding.getId();
		dbr.sms_destination = mpd.destination;
		dbr.sms_text_enc = mpd.encoding.getId();
		dbr.sms_text = tmp;
		dbr.m3ua_data_dpc = mpd.m3ua_data_dpc;
		dbr.m3ua_data_opc = mpd.m3ua_data_opc;
		dbr.tcap_sid = mpd.tcap_sid;
		dbr.tcap_did = mpd.tcap_did;
		dbr.timestamp = System.currentTimeMillis();

		mpd.messageParts.clear();
		
		return dbr;
		
	}
	private static class ErrorTimeout_r implements Runnable{
		long l = 0;
		DBRecordBase tmp;
		public void run() {
			logger.info("STARTING!");
			while(!stopping){
				// Sleep
				try{
					Thread.sleep(FNConfigData.dr_errors_check_interval);
				}catch(Exception e){
					e.printStackTrace();
				}
				//logger.info("Checking for TCAP Requests without Replies...");
				l = System.currentTimeMillis();
				for(String key : error_correlation_map.keySet()){
					tmp = error_correlation_map.get(key);
					if(tmp != null){
						if(tmp.recordType == DBRecordType.ISUP){
							if(l - tmp.timestamp > FNConfigData.dr_isup_timeout){
								//logger.debug("Removing TCAP correlation: [" + key + "]");
								((DBRecordISUP)tmp).release_cause = 9999;
								((DBRecordISUP)tmp).release_cause_class = 9999;
								DRManager.processDBR(tmp, Utils.convert_DS2MOO(tmp.dataSource));

								error_correlation_map.remove(key);
								//StatsManager.DECODER_STATS.SRI_NO_REPLY++;
							}							
						}else{
							if(l - tmp.timestamp > FNConfigData.dr_errors_timeout){
								//logger.debug("Removing TCAP correlation: [" + key + "]");
								tmp.no_reply = true;
								if(tmp.recordType == DBRecordType.SMS){
									DRManager.processDBR((DBRecordSMS)tmp, Utils.convert_DS2MOO(tmp.dataSource));
								}else if(tmp.recordType == DBRecordType.SRI){
									DRManager.processDBR((DBRecordSRI)tmp, Utils.convert_DS2MOO(tmp.dataSource));
									
								}

								error_correlation_map.remove(key);
								//StatsManager.DECODER_STATS.SRI_NO_REPLY++;
							}							
						}

					}
				}
				
				
			}
			logger.info("ENDING!");
			
		}
	
	}

	public static void init(){
		DRWorker_vstp dw;
		queue = new ConcurrentLinkedQueue<DRPacket>();
		workerLst = new ArrayList<DRWorker_vstp>();
		error_correlation_map = new ConcurrentHashMap<String, DBRecordBase>();
		
		// If SRI DR is ON and SRI Error Correlation is ON
		//if((FNConfigData.dr_intrusive_hlr_status || FNConfigData.dr_non_intrusive_hlr_status) && FNConfigData.dr_sri_errors_status){
		err_timeout_r = new ErrorTimeout_r();
		err_timeout_t = new Thread(err_timeout_r, "ERROR_TIMEOUT_THREAD");
		err_timeout_t.start();
			
		//}
		
		logger.info("Creating DecoderWorker threads: [" + FNConfigData.dr_workers + "]");
		for(int i = 0; i<FNConfigData.dr_workers; i++){
			dw = new DRWorker_vstp(i);
			workerLst.add(dw);
		}
		
		
		
	}

	public static void stopWorker(int id){
		logger.info("Stopping DecoderWorker thread: [" + id + "]");
		workerLst.get(id).stop();
		
	}
}
