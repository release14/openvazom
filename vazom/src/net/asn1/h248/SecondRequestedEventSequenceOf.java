package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SecondRequestedEventSequenceOf extends SEQUENCE{

	public SecondRequestedEvent new_child(){
		return new SecondRequestedEvent();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SecondRequestedEvent getChild(int index){
		return (SecondRequestedEvent)of_children.get(index);
	}
	public SecondRequestedEventSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
