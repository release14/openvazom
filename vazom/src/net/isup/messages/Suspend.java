package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.SuspendResumeIndicators;

public class Suspend extends MessageBase {
	public SuspendResumeIndicators suspend_resume_indicators;
	public Suspend(){
		super();
		type = MessageType.SUSPEND;
		pointers = new Pointer[1];
		
	}
	public void init(byte[] data) {
		if(data != null){
			suspend_resume_indicators = new SuspendResumeIndicators();
			suspend_resume_indicators.init(new byte[]{data[byte_pos++]});
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			processOptional(data, 0);
			
		}

	}

}
