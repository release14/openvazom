package net.mtp3;

import java.util.ArrayList;
import java.util.Arrays;

import net.mtp3.messages.MessageBase;
import net.mtp3.parameters.RoutingLabel;
import net.mtp3.parameters.ServiceInformation;
import net.mtp3.types.NetworkIndicatorType;
import net.mtp3.types.ServiceIndicatorType;
import net.utils.Utils;


public class MTP3Packet {
	public ServiceInformation serviceInformation;
	public RoutingLabel routingLabel;
	public MessageBase message;
	public int byte_pos;
	
	public MTP3Packet(){
		serviceInformation = new ServiceInformation();
		routingLabel = new RoutingLabel();
		
	}
	
	public byte[] encode(){
		ArrayList<Byte> buff = new ArrayList<Byte>();
		int tmp;
		byte[] res = null;
		byte[] tmp_res = null;
		// network indicator, serevice indicator
		tmp = serviceInformation.networkIndicator.getId() + serviceInformation.serviceIndicator.getId();
		buff.add((byte)tmp);
		// routing label
		boolean[] sls = Utils.int2bits(routingLabel.signallingLinkSelector, 4);
		boolean[] opc = Utils.int2bits(routingLabel.originatingPointCode, 14);
		boolean[] dpc = Utils.int2bits(routingLabel.destinationPointCode, 14);
		boolean[] res_bits = Utils.bitsCombine(Utils.bitsCombine(sls, opc), dpc);
		// reverse to network order
		tmp_res = Utils.reverse(Utils.bits2bytes(res_bits));
		// add to buff
		for(int i = 0; i<tmp_res.length; i++) buff.add(tmp_res[i]);
		// message
		if(message != null){
			tmp_res = message.encode();
			if(tmp_res != null){
				// add to buff
				for(int i = 0; i<tmp_res.length; i++) buff.add(tmp_res[i]);
			}
		}
		
		
		// prepare result
		res = Utils.list2array(buff);
		return res;
		
	}
	public void init(byte[] data) {
		boolean[] tmp_bits = null;
		// network indicator
		serviceInformation.networkIndicator = NetworkIndicatorType.get(data[byte_pos] & 0xc0);
		// service indicator
		serviceInformation.serviceIndicator = ServiceIndicatorType.get(data[byte_pos] & 0x0f);
		byte_pos++;
		
		tmp_bits = Utils.bytes2bits(Utils.reverse(Arrays.copyOfRange(data, 1, 5)));
		// routingLabel
		// SLS
		routingLabel.signallingLinkSelector = Utils.bits2int(tmp_bits, 0, 4);
		// OPC
		routingLabel.originatingPointCode = Utils.bits2int(tmp_bits, 4, 14);
		// DPC
		routingLabel.destinationPointCode = Utils.bits2int(tmp_bits, 18, 14);
		
		byte_pos += 4;
		
		
	}

}
