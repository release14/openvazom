package net.routing;

import java.util.ArrayList;


public abstract class RoutingConnection {
	public String id;
	public RoutingConnectionType type;
	public ArrayList<Object> params;
	public RoutingConnectionWorkerMethod worker_method = null;
	public RoutingStats stats;
	public RoutingConnectionDirectionType direction;
	public boolean stopping;
	public boolean active;
	public boolean down;
	public long IN_QUEUE_MAX;
	public long OUT_QUEUE_MAX;
	
	public RoutingConnection(){
		params = new ArrayList<Object>();
		stats = new RoutingStats();
	}

	public abstract void attachWorkerMethod(RoutingConnectionWorkerMethod method);
	public abstract void detachWorkerMethod();
	
	public abstract void out_offer(RoutingConnectionPayload data);
	public abstract RoutingConnectionPayload convert(RoutingConnectionPayload input);
	
	public abstract void stop();
	public abstract void start_server_listener();
}
