package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SignalQuality extends INTEGER{

	public static final int _not_a_usable_signal = 0;
	public static final int _treat_as_Not_a_usable_signal1 = 1;
	public static final int _treat_as_Not_a_usable_signal2 = 2;
	public static final int _treat_as_Not_a_usable_signal3 = 3;
	public static final int _treat_as_Not_a_usable_signal4 = 4;
	public static final int _treat_as_Not_a_usable_signal5 = 5;
	public static final int _treat_as_Not_a_usable_signal6 = 6;
	public static final int _treat_as_Not_a_usable_signal7 = 7;
	public static final int _treat_as_Not_a_usable_signal8 = 8;
	public static final int _usable_signal_range1 = 9;
	public static final int _usable_signal_range2 = 245;
	public static final int _treat_the_same_as_interference1 = 246;
	public static final int _treat_the_same_as_interference2 = 247;
	public static final int _treat_the_same_as_interference3 = 248;
	public static final int _treat_the_same_as_interference4 = 249;
	public static final int _treat_the_same_as_interference5 = 250;
	public static final int _treat_the_same_as_interference6 = 251;
	public static final int _treat_the_same_as_interference7 = 252;
	public static final int _treat_the_same_as_interference8 = 253;
	public static final int _treat_the_same_as_interference9 = 254;
	public static final int _interference = 255;
	public SignalQuality(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
