package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AuditTokenType extends BIT_STRING{

	public static final int _muxToken = 0;
	public static final int _modemToken = 1;
	public static final int _mediaToken = 2;
	public static final int _eventsToken = 3;
	public static final int _signalsToken = 4;
	public static final int _digitMapToken = 5;
	public static final int _statsToken = 6;
	public static final int _observedEventsToken = 7;
	public static final int _packagesToken = 8;
	public static final int _eventBufferToken = 9;
	public AuditTokenType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
