package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendRoutingInfoArgV2 extends SEQUENCE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public CUG_CheckInfo get_cug_CheckInfo(){
		return (CUG_CheckInfo)elements.get(1).data;
	}
	public CUG_CheckInfo new_cug_CheckInfo(){
		return new CUG_CheckInfo();
	}

	public NumberOfForwarding get_numberOfForwarding(){
		return (NumberOfForwarding)elements.get(2).data;
	}
	public NumberOfForwarding new_numberOfForwarding(){
		return new NumberOfForwarding();
	}

	public ExternalSignalInfo get_networkSignalInfo(){
		return (ExternalSignalInfo)elements.get(3).data;
	}
	public ExternalSignalInfo new_networkSignalInfo(){
		return new ExternalSignalInfo();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cug_CheckInfo(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_numberOfForwarding(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkSignalInfo(); }});
	}
	public SendRoutingInfoArgV2(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
