package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ParameterRequest extends SET{

	public RequiredParametersMask get_requiredParametersMask(){
		return (RequiredParametersMask)elements.get(0).data;
	}
	public RequiredParametersMask new_requiredParametersMask(){
		return new RequiredParametersMask();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(2).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(3).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public NetworkTMSI get_networkTMSI(){
		return (NetworkTMSI)elements.get(4).data;
	}
	public NetworkTMSI new_networkTMSI(){
		return new NetworkTMSI();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(5).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(6).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(7).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(236, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_requiredParametersMask(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(233, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkTMSI(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemMyTypeCode(); }});
	}
	public ParameterRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
