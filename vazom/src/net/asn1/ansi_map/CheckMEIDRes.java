package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CheckMEIDRes extends SET{

	public MEIDStatus get_meidStatus(){
		return (MEIDStatus)elements.get(0).data;
	}
	public MEIDStatus new_meidStatus(){
		return new MEIDStatus();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(391, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meidStatus(); }});
	}
	public CheckMEIDRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
