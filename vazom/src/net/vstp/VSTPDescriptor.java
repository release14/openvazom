package net.vstp;

import net.asn1.tcap2.TCMessage;
import net.m3ua.messages.DATA;
import net.sccp.messages.UDT_UnitData;

public class VSTPDescriptor {
	public String id;
	public TCMessage tcm;
	public DATA m3ua_data;
	public UDT_UnitData sccp_udt;
}
