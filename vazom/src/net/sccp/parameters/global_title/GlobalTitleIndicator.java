package net.sccp.parameters.global_title;

import java.util.HashMap;

public enum GlobalTitleIndicator {
	NO_TITLE(0x00),
	NATURE_OF_ADDRESS_INDICATOR_ONLY(0x04),
	TRANSLATION_TYPE_ONLY(0x08),
	TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING(0x0C),
	TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS(0x10);
	
	
	
	
	// bits 5..2
	private int id;
	private static final HashMap<Integer, GlobalTitleIndicator> lookup = new HashMap<Integer, GlobalTitleIndicator>();
	static{
		for(GlobalTitleIndicator td : GlobalTitleIndicator.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static GlobalTitleIndicator get(int id){ return lookup.get(id); }
	private GlobalTitleIndicator(int _id){ id = _id; }
	

}
