package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public abstract class OptionalParameterBase {
	public OptionalParameterType type;
	
	public abstract void init(byte[] data);
	
}
