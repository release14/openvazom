package net.smstpdu;


public class TBCD {

	public static String convert(int val){
		String res = "";
		switch(val){
			case 10: res = "*"; break;
			case 11: res = "#"; break;
			case 12: res = "a"; break;
			case 13: res = "b"; break;
			case 14: res = "c"; break;
			case 15: res = ""; break;
			default: res = Integer.toString(val);
		}
		return res;
		
	}
	public static byte[] encode(String data){
		byte[] res = new byte[(int)Math.ceil((double)data.length() / 2)];
		byte tmp = 0x00;
		byte[] digits = data.getBytes();
		for(int i = 0; i<digits.length; i++){
			if(i % 2 == 0) tmp += Integer.parseInt(String.valueOf((char)digits[i]));
			else{
				tmp += Integer.parseInt(String.valueOf((char)digits[i])) << 4;
				res[i / 2] = tmp;
				tmp = 0x00;
			}
		}

		if(digits.length % 2 != 0) res[digits.length / 2] = tmp;
		return res;
	}
	public static byte[] encode(String data, int filler){
		byte[] res = new byte[(int)Math.ceil((double)data.length() / 2)];
		byte tmp = 0x00;
		byte[] digits = data.getBytes();
		for(int i = 0; i<digits.length; i++){
			if(i % 2 == 0) tmp += Integer.parseInt(String.valueOf((char)digits[i]));
			else{
				tmp += Integer.parseInt(String.valueOf((char)digits[i])) << 4;
				res[i / 2] = tmp;
				tmp = 0x00;
			}
		}

		if(digits.length % 2 != 0){
			res[digits.length / 2] = tmp;
			res[digits.length / 2] += (filler << 4);
			
		}
		return res;
	}
	
	public static String decode(byte[] data, boolean odd){
		if(odd) return decodeOdd(data); else return decode(data);
	}

	public static String decode(byte[] data){
		String res = "";
		if(data != null){
			for(int i = 0; i<data.length; i++){
				res += convert(data[i] & 0x0F) + convert((data[i] & 0xF0) >> 4);
				
			}
			
		}
		return res;
	}
	public static String decodeOdd(byte[] data){
		String res = "";
		if(data != null){
			for(int i = 0; i<data.length; i++){
				res += convert(data[i] & 0x0F) + (i < data.length - 1 ? convert((data[i] & 0xF0) >> 4) : "");
				
			}
		}
		return res;
	}

}
