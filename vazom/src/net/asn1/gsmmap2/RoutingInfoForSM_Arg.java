package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RoutingInfoForSM_Arg extends SEQUENCE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public BOOLEAN get_sm_RP_PRI(){
		return (BOOLEAN)elements.get(1).data;
	}
	public BOOLEAN new_sm_RP_PRI(){
		return new BOOLEAN();
	}

	public AddressString get_serviceCentreAddress(){
		return (AddressString)elements.get(2).data;
	}
	public AddressString new_serviceCentreAddress(){
		return new AddressString();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NULL get_gprsSupportIndicator(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_gprsSupportIndicator(){
		return new NULL();
	}

	public SM_RP_MTI get_sm_RP_MTI(){
		return (SM_RP_MTI)elements.get(5).data;
	}
	public SM_RP_MTI new_sm_RP_MTI(){
		return new SM_RP_MTI();
	}

	public SM_RP_SMEA get_sm_RP_SMEA(){
		return (SM_RP_SMEA)elements.get(6).data;
	}
	public SM_RP_SMEA new_sm_RP_SMEA(){
		return new SM_RP_SMEA();
	}

	public SM_DeliveryNotIntended get_sm_deliveryNotIntended(){
		return (SM_DeliveryNotIntended)elements.get(7).data;
	}
	public SM_DeliveryNotIntended new_sm_deliveryNotIntended(){
		return new SM_DeliveryNotIntended();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sm_RP_PRI(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceCentreAddress(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_gprsSupportIndicator(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_sm_RP_MTI(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_sm_RP_SMEA(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_sm_deliveryNotIntended(); }});
	}
	public RoutingInfoForSM_Arg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
