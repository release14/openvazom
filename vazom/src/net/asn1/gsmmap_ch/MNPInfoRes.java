package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MNPInfoRes extends SEQUENCE{

	public RouteingNumber get_routeingNumber(){
		return (RouteingNumber)elements.get(0).data;
	}
	public RouteingNumber new_routeingNumber(){
		return new RouteingNumber();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(1).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(2).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public NumberPortabilityStatus get_numberPortabilityStatus(){
		return (NumberPortabilityStatus)elements.get(3).data;
	}
	public NumberPortabilityStatus new_numberPortabilityStatus(){
		return new NumberPortabilityStatus();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(4).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routeingNumber(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_numberPortabilityStatus(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public MNPInfoRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
