package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FacilitiesDirective2Res extends SET{

	public BSMCStatus get_bsmcstatus(){
		return (BSMCStatus)elements.get(0).data;
	}
	public BSMCStatus new_bsmcstatus(){
		return new BSMCStatus();
	}

	public CDMA2000HandoffResponseIOSData get_cdma2000HandoffResponseIOSData(){
		return (CDMA2000HandoffResponseIOSData)elements.get(1).data;
	}
	public CDMA2000HandoffResponseIOSData new_cdma2000HandoffResponseIOSData(){
		return new CDMA2000HandoffResponseIOSData();
	}

	public CDMAChannelData get_cdmaChannelData(){
		return (CDMAChannelData)elements.get(2).data;
	}
	public CDMAChannelData new_cdmaChannelData(){
		return new CDMAChannelData();
	}

	public CDMACodeChannelList get_cdmaCodeChannelList(){
		return (CDMACodeChannelList)elements.get(3).data;
	}
	public CDMACodeChannelList new_cdmaCodeChannelList(){
		return new CDMACodeChannelList();
	}

	public CDMAConnectionReferenceList get_cdmaConnectionReferenceList(){
		return (CDMAConnectionReferenceList)elements.get(4).data;
	}
	public CDMAConnectionReferenceList new_cdmaConnectionReferenceList(){
		return new CDMAConnectionReferenceList();
	}

	public CDMASearchParameters get_cdmaSearchParameters(){
		return (CDMASearchParameters)elements.get(5).data;
	}
	public CDMASearchParameters new_cdmaSearchParameters(){
		return new CDMASearchParameters();
	}

	public CDMASearchWindow get_cdmaSearchWindow(){
		return (CDMASearchWindow)elements.get(6).data;
	}
	public CDMASearchWindow new_cdmaSearchWindow(){
		return new CDMASearchWindow();
	}

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(7).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(8).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(9).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public ISLPInformation get_ilspInformation(){
		return (ISLPInformation)elements.get(10).data;
	}
	public ISLPInformation new_ilspInformation(){
		return new ISLPInformation();
	}

	public NAMPSChannelData get_nampsChannelData(){
		return (NAMPSChannelData)elements.get(11).data;
	}
	public NAMPSChannelData new_nampsChannelData(){
		return new NAMPSChannelData();
	}

	public SOCStatus get_sOCStatus(){
		return (SOCStatus)elements.get(12).data;
	}
	public SOCStatus new_sOCStatus(){
		return new SOCStatus();
	}

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(13).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public TDMABurstIndicator get_tdmaBurstIndicator(){
		return (TDMABurstIndicator)elements.get(14).data;
	}
	public TDMABurstIndicator new_tdmaBurstIndicator(){
		return new TDMABurstIndicator();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(15).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TDMAVoiceCoder get_tdmaVoiceCoder(){
		return (TDMAVoiceCoder)elements.get(16).data;
	}
	public TDMAVoiceCoder new_tdmaVoiceCoder(){
		return new TDMAVoiceCoder();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(198, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_bsmcstatus(); }});
		elements.add(new ElementDescriptor(357, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdma2000HandoffResponseIOSData(); }});
		elements.add(new ElementDescriptor(63, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaChannelData(); }});
		elements.add(new ElementDescriptor(132, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaCodeChannelList(); }});
		elements.add(new ElementDescriptor(212, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceList(); }});
		elements.add(new ElementDescriptor(230, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaSearchParameters(); }});
		elements.add(new ElementDescriptor(69, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaSearchWindow(); }});
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(217, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ilspInformation(); }});
		elements.add(new ElementDescriptor(74, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsChannelData(); }});
		elements.add(new ElementDescriptor(205, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sOCStatus(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(31, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBurstIndicator(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(180, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaVoiceCoder(); }});
	}
	public FacilitiesDirective2Res(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
