package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CUG_Interlock extends OCTET_STRING{

	public CUG_Interlock(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
