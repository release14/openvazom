package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ErrorCode extends CHOICE{

	public INTEGER get_local(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_local(){
		return new INTEGER();
	}

	public OBJECT_IDENTIFIER get_global(){
		return (OBJECT_IDENTIFIER)elements.get(1).data;
	}
	public OBJECT_IDENTIFIER new_global(){
		return new OBJECT_IDENTIFIER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_local(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_global(); }});
	}
	public ErrorCode(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
