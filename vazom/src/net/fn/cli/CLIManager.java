package net.fn.cli;
import net.cli.AuthManager;
import net.cli.CLIBase;
import net.cli.GroupDescriptor;
import net.cli.MethodDescriptor;
import net.cli.ParamDescriptor;
import net.config.ConfigManagerV2;
import net.config.FNConfigData;
import net.db.DBManager;
import net.db.DBRecordSMS;
import net.flood.FloodItem;
import net.flood.FloodManager;
import net.flood.GlobalMaxType;
import net.fn.FNode;
import net.fn.SGDescriptor;
import net.fn.SGNManager;
import net.gpu2.GPUBalancer;
import net.gpu2.NodeDescriptor;
import net.smsfs.SmsfsManager;
import net.smsfs.list.ListBase;
import net.smsfs.list.ListManagerV2;
import net.smsfs.list.ListType;
import net.smsfs.list.lists.SmsfsList;
import net.stats.StatsManager;
import net.utils.Utils;

public class CLIManager extends CLIBase{
	// AUTH 
    public class GRP_AUTH extends GroupDescriptor {

        public GRP_AUTH() {
        	super("AUTH", "CLI Authentication management ", "", "");
        }

        public String execute(Object[] params) {
        	return null;
        }

    }
	public GRP_AUTH _grp_AUTH = new GRP_AUTH();
	
	
	public class CLI_user_list extends MethodDescriptor {
	
		public CLI_user_list() {
			super("USRLST", "List users ", "AUTH", "");
		}
		
		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.auth_user_group == 0){
					for(String s : AuthManager.auth_map.keySet()){
						res += "[WHITE]Username = [[GREEN]" + s + "[WHITE]], credentials = [[GREEN]" + (AuthManager.checkGroup(s) == 0 ? "admin" : "user") + "[WHITE]]\n";
					}
				}else{
					return "[RED]Insufficient credentials!";
				}

			}
			return res;
			
		}
	
	}
	public CLI_user_list _cli_user_list = new CLI_user_list();
	
	public class CLI_grp_list extends MethodDescriptor {
	
		public CLI_grp_list() {
	        super("GRPLST", "List groups ", "AUTH", "");
		}
		
		public String execute(Object[] params) {
			return "[WHITE] Groups = [[RED]0 [WHITE]= [GREEN]admin[WHITE], [RED]1[WHITE] = [GREEN]user[WHITE]]!";
		}
	
	}
	public CLI_grp_list _cli_grp_list = new CLI_grp_list();
	
	public class CLI_user_add extends MethodDescriptor {
	
	    public CLI_user_add() {
            super("USRADD", "Add user ", "AUTH", "group_id:username:password:password_repeat");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				ParamDescriptor pd4 = (ParamDescriptor)params[3];
				CLIConnection conn = (CLIConnection)params[4];
				if(conn.auth_user_group == 0){
					if(pd1.value != null && pd2.value != null && pd3.value != null && pd4.value != null){
						if(!pd3.value.equals(pd4.value)) return "[RED]Passwords do not match!";
						if(!Utils.isInt(pd1.value)) return "[RED]Unknown group id = [" + pd1.value +"]!";
						if(Integer.parseInt(pd1.value) != 0 && Integer.parseInt(pd1.value) != 1) return "[RED]Unknown group id = [" + pd1.value +"]!";
						// add user
						if(!AuthManager.userExists(pd2.value)){
							AuthManager.set(pd2.value, pd3.value, Integer.parseInt(pd1.value));
							return "[WHITE]Adding user [[GREEN]" + pd2.value + "[WHITE]], credentials = [[GREEN]" + AuthManager.grpId2Name(Integer.parseInt(pd1.value)) + "[WHITE]]";
							
						}else{
							return "[RED]User [[GREEN]" + pd2.value +"[RED]] already exists!";
							
						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }
	
	}
	public CLI_user_add _cli_user_add = new CLI_user_add();
	
    public class CLI_user_set extends MethodDescriptor {

	    public CLI_user_set() {
            super("USRSET", "Set user information ", "AUTH", "group_id:username:password:password_repeat");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				ParamDescriptor pd4 = (ParamDescriptor)params[3];
				CLIConnection conn = (CLIConnection)params[4];
				if(conn.auth_user_group == 0){
					if(pd1.value != null && pd2.value != null && pd3.value != null && pd4.value != null){
						if(!pd3.value.equals(pd4.value)) return "[RED]Passwords do not match!";
						if(!Utils.isInt(pd1.value)) return "[RED]Unknown group id = [" + pd1.value +"]!";
						if(Integer.parseInt(pd1.value) != 0 && Integer.parseInt(pd1.value) != 1) return "[RED]Unknown group id = [" + pd1.value +"]!";
						// set user
						if(AuthManager.userExists(pd2.value)){
							AuthManager.set(pd2.value, pd3.value, Integer.parseInt(pd1.value));
							return "[WHITE]Setting user [[GREEN]" + pd2.value + "[WHITE]], credentials = [[GREEN]" + AuthManager.grpId2Name(Integer.parseInt(pd1.value)) + "[WHITE]]";
							
						}else{
							return "[RED]User [[GREEN]" + pd2.value +"[RED]] does not exist!";

						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }

	}
	public CLI_user_set _cli_user_set = new CLI_user_set();
	
	public class CLI_user_rm extends MethodDescriptor {
	
	    public CLI_user_rm() {
            super("USRRM", "Remove user ", "AUTH", "username");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				CLIConnection conn = (CLIConnection)params[1];
				if(conn.auth_user_group == 0){
					if(pd1.value != null){
						// set user
						if(AuthManager.userExists(pd1.value)){
							AuthManager.remove(pd1.value);
							return "[WHITE]Removing user [[GREEN]" + pd1.value + "[WHITE]]!";
							
						}else{
							return "[RED]User [[GREEN]" + pd1.value +"[RED]] does not exist!";

						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }
	
	}
	public CLI_user_rm _cli_user_rm = new CLI_user_rm();
	
	public class CLI_users_passwd extends MethodDescriptor {
	
        public CLI_users_passwd() {
            super("PASSWD", "Change password ", "AUTH", "password:password_repeat");
        }

        public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				CLIConnection conn = (CLIConnection)params[2];
				if(pd1.value != null && pd2.value != null){
					if(!pd1.value.equals(pd2.value)) return "[RED]Passwords do not match!";
					
					AuthManager.set(conn.auth_user, pd1.value, AuthManager.checkGroup(conn.auth_user));
					return "[WHITE]Changing password for user [[GREEN]" + conn.auth_user + "[WHITE]]!";
					
				}else{
					return "[RED]Missing parameters!";
					
				}

	
			}
			return res;

        }
	
	}
	public CLI_users_passwd _cli_users_passwd = new CLI_users_passwd();
	
	
	// AUTH end
	
	
	public class GRP_SHOW extends GroupDescriptor {

		public GRP_SHOW() {
			super("SHOW", "Statistics and memory usage ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_SHOW _grp_SHOW = new GRP_SHOW();

	public class GRP_FLOOD extends GroupDescriptor {

		public GRP_FLOOD() {
			super("FLOOD", "Flood detection management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_FLOOD _grp_FLOOD = new GRP_FLOOD();	
	
	

	public class CLI_show_memory extends MethodDescriptor {

		public CLI_show_memory() {
			super("MEMORY", "Memory usage ", "SHOW", "");
		}

		public String execute(Object[] params) {
			double used_mem =  (double)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024/1024;
			double total_mem =  (double)Runtime.getRuntime().totalMemory() / 1024/1024;
			return String.format("[ [GREEN]%7.2fMb[RESET] / [RED]%7.2fMb[RESET] ]", used_mem, total_mem);
		}

	}
	public CLI_show_memory _cli_show_memory = new CLI_show_memory();

	public class GRP_STATS extends GroupDescriptor {

		public GRP_STATS() {
			super("STATS", "Various statistics ", "SHOW", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_STATS _grp_STATS = new GRP_STATS();


	public class CLI_show_stats_system extends MethodDescriptor{
		public CLI_show_stats_system(){
			super("SYSTEM", "Vazom Various System statistics ", "STATS,SHOW", "");
			
		}
		
		public String execute(Object[] params) {
			String res = "";
			res += "[WHITE]FGN_IN_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.FGN_IN_QUEUE_MAX + "\n";
			res += "[WHITE]FGN_OUT_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.FGN_OUT_QUEUE_MAX + "\n";
			res += "[WHITE]SGC_IN_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.SGC_IN_QUEUE_MAX + "\n";
			res += "[WHITE]SGC_OUT_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.SGC_OUT_QUEUE_MAX + "\n";
			return Utils.str_align(res, ":");
		}
		
	}
	public CLI_show_stats_system _cli_show_stats_system = new CLI_show_stats_system();
	
	
	public class CLI_show_stats_db extends MethodDescriptor {

		public CLI_show_stats_db() {
			super("DB", "Database statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			res += "[WHITE]DB_CONNECTION_LOST COUNT: [RED]" + StatsManager.DB_STATS.DB_CONNECTION_LOST + "\n";
			res += "[WHITE]DB_QUEUE_FULL_COUNT: [RED]" + StatsManager.DB_STATS.DB_QUEUE_FULL_COUNT + "\n";
			res += "[WHITE]DB_SMS_BATCH_COUNT COUNT: [GREEN]" + StatsManager.DB_STATS.DB_SMS_BATCH_COUNT + "\n";
			res += "[WHITE]DB_SRI_BATCH_COUNT COUNT: [GREEN]" + StatsManager.DB_STATS.DB_SRI_BATCH_COUNT + "\n";
			res += "[WHITE]DB_ISUP_BATCH_COUNT COUNT: [GREEN]" + StatsManager.DB_STATS.DB_ISUP_BATCH_COUNT + "\n";
			res += "[WHITE]LAST_SMS_BATCH_ELAPSED TIME(msec): [GREEN]" + StatsManager.DB_STATS.LAST_SMS_BATCH_ELAPSED + "\n";
			res += "[WHITE]LAST_SRI_BATCH_ELAPSED TIME(msec): [GREEN]" + StatsManager.DB_STATS.LAST_SRI_BATCH_ELAPSED + "\n";
			res += "[WHITE]LAST_ISUP_BATCH_ELAPSED TIME(msec): [GREEN]" + StatsManager.DB_STATS.LAST_ISUP_BATCH_ELAPSED + "\n";
			res += "[WHITE]MAX_SMS_BATCH_ELAPSED TIME(msec): [YELLOW]" + StatsManager.DB_STATS.MAX_SMS_BATCH_ELAPSED + "\n";
			res += "[WHITE]MAX_SRI_BATCH_ELAPSED TIME(msec): [YELLOW]" + StatsManager.DB_STATS.MAX_SRI_BATCH_ELAPSED + "\n";
			res += "[WHITE]MAX_ISUP_BATCH_ELAPSED TIME(msec): [YELLOW]" + StatsManager.DB_STATS.MAX_ISUP_BATCH_ELAPSED + "\n";

			return Utils.str_align(res, ":");
		}

	}
	public CLI_show_stats_db _cli_show_stats_db = new CLI_show_stats_db();

	public class CLI_show_stats_vstp extends MethodDescriptor {

		public CLI_show_stats_vstp() {
			super("VSTP", "Vazom Signaling Transfer Protocol statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			res += "[WHITE]VSTP_SG_CONNECTION_LOST COUNT: [RED]" + StatsManager.VSTP_STATS.VSTP_SG_CONNECTION_LOST + "\n";
			res += "[WHITE]VSTP_NO_SG_NODES COUNT: [RED]" + StatsManager.VSTP_STATS.VSTP_NO_SG_NODES + "\n";
			return Utils.str_align(res, ":");
		}

	}
	public CLI_show_stats_vstp _cli_show_stats_vstp = new CLI_show_stats_vstp();

	public class CLI_show_stats_smsfs extends MethodDescriptor {

		public CLI_show_stats_smsfs() {
			super("SMSFS", "Sms Filter Script statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(FNConfigData.smsfs_status){
				res += "[WHITE]SMSFS queue size: [GREEN]"  + SmsfsManager.queue.size() + "\n";
				res += "[WHITE]INTRUSIVE ACCEPTED COUNT: [GREEN]"  + Long.toString(StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED) + "\n";
				res += "[WHITE]INTRUSIVE REJECTED COUNT: [RED]"  + Long.toString(StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED) + "\n";
				res += "[WHITE]NON INTRUSIVE ACCEPTED COUNT: [GREEN]"  + Long.toString(StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED) + "\n";
				res += "[WHITE]NON INTRUSIVE REJECTED COUNT: [RED]"  + Long.toString(StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED) + "\n";
				res = Utils.str_align(res, ":");
			}else res = "[RED]SMSFS if OFF!";

			return res;
		}

	}
	public CLI_show_stats_smsfs _cli_show_stats_smsfs = new CLI_show_stats_smsfs();

	public class CLI_show_stats_gpub extends MethodDescriptor {

		public CLI_show_stats_gpub() {
			super("GPUB", "GPU Balancer statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(FNConfigData.gpub_status){
				// LD stats
				res += "[WHITE]LD_REQUEST COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.LD_REQUEST_COUNT) + "\n";
				res += "[WHITE]LD_REPLY_OK COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.LD_REPLY_OK_COUNT) + "\n";
				res += "[WHITE]LD_REPLY_ERROR COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.LD_REPLY_ERROR_COUNT) + "\n";
				res += "[WHITE]LD_MAX_REPLY TIME: [YELLOW]"  + Long.toString(StatsManager.GPUB_STATS.LD_MAX_REPLY_TIME) + "msec\n";
				res += "[WHITE]LD_LAST_REPLY TIME: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME) + "msec\n";
				res += "[WHITE]LD_CONNECTION_DOWN COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.LD_CONNECTION_DOWN_COUNT) + "\n";
				res += "[WHITE]LD_NODE_BUSY COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT) + "\n";
				// UPDATE stats
				res += "[WHITE]UPDATE_REQUEST COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REQUEST_COUNT) + "\n";
				res += "[WHITE]UPDATE_REPLY_OK COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REPLY_OK_COUNT) + "\n";
				res += "[WHITE]UPDATE_REPLY_ERROR COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REPLY_ERROR_COUNT) + "\n";
				res += "[WHITE]UPDATE_MAX_REPLY TIME: [YELLOW]"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_MAX_REPLY_TIME) + "msec\n";
				res += "[WHITE]UPDATE_LAST_REPLY TIME: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME) + "msec\n";
				res += "[WHITE]UPDATE_CONNECTION_DOWN COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_CONNECTION_DOWN_COUNT) + "\n";

				// MD5 stats
				res += "[WHITE]MD5_REQUEST COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.MD5_REQUEST_COUNT) + "\n";
				res += "[WHITE]MD5_REPLY_OK COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.MD5_REPLY_OK_COUNT) + "\n";
				res += "[WHITE]MD5_REPLY_ERROR COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.MD5_REPLY_ERROR_COUNT) + "\n";
				res += "[WHITE]MD5_MAX_REPLY TIME: [YELLOW]"  + Long.toString(StatsManager.GPUB_STATS.MD5_MAX_REPLY_TIME) + "msec\n";
				res += "[WHITE]MD5_LAST_REPLY TIME: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME) + "msec\n";
				res += "[WHITE]MD5_CONNECTION_DOWN COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.MD5_CONNECTION_DOWN_COUNT) + "\n";
				res += "[WHITE]MD5_NODE_BUSY COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.MD5_NODE_BUSY_COUNT) + "\n";
				// MD5 UPDATE stats
				res += "[WHITE]MD5_UPDATE_REQUEST COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REQUEST_COUNT) + "\n";
				res += "[WHITE]MD5_UPDATE_REPLY_OK COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_OK_COUNT) + "\n";
				res += "[WHITE]MD5_UPDATE_REPLY_ERROR COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_ERROR_COUNT) + "\n";
				res += "[WHITE]MD5_UPDATE_MAX_REPLY TIME: [YELLOW]"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_MAX_REPLY_TIME) + "msec\n";
				res += "[WHITE]MD5_UPDATE_LAST_REPLY TIME: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME) + "msec\n";
				res += "[WHITE]MD5_UPDATE_CONNECTION_DOWN COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_CONNECTION_DOWN_COUNT) + "\n";
				// LST REMOVE stats
				res += "[WHITE]LST_REMOVE COUNT: [GREEN]"  + Long.toString(StatsManager.GPUB_STATS.LST_REMOVE_COUNT) + "\n";
				res += "[WHITE]LST_REMOVE_ERROR COUNT: [RED]"  + Long.toString(StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT) + "\n";
				
				res = Utils.str_align(res, ":");
			}else res = "[RED]GPUB module if OFF";
			return res;
		}

	}
	public CLI_show_stats_gpub _cli_show_stats_gpub = new CLI_show_stats_gpub();

	public class CLI_show_stats_gpu extends MethodDescriptor {

		public CLI_show_stats_gpu() {
			super("GPU", "GPU Node statistics ", "STATS,SHOW", "node_id:gpu_id");
		}

		public String execute(Object[] params) {
			String res = "[RED]Not available...";
			/*
			if(params != null){
				if(FNConfigData.gpub_status){
					ParamDescriptor pd1 = (ParamDescriptor)params[0];
					ParamDescriptor pd2 = (ParamDescriptor)params[1];
					if(pd1.value != null && pd2.value != null){
						try{
							String rep_node = pd1.value;
							String rep_gpu = pd2.value;
							String gpu_stats_line = null;
							String[] gpu_result = null;
							
							
							Socket gpu_socket = new Socket(GPUBalancer.getNodeDescriptor(rep_node).host, GPUBalancer.getNodeDescriptor(rep_node).port);
							gpu_socket.setSoTimeout(10000);
							BufferedReader gpu_in = new BufferedReader(new InputStreamReader(gpu_socket.getInputStream()));
							PrintWriter gpu_out = new PrintWriter(gpu_socket.getOutputStream());
							gpu_out.println("S." + rep_gpu);
							gpu_out.flush();
							gpu_stats_line = gpu_in.readLine();
							gpu_result = gpu_stats_line.split(":");
							res += "[WHITE]KERNEL EXECUTION COUNT: [GREEN]" + gpu_result[0] + "\n";
							res += "[WHITE]LAST KERNEL EXECUTION TIME: [GREEN]" + gpu_result[1] + "msec\n";
							res += "[WHITE]MAX KERNEL EXECUTION TIME: [YELLOW]" + gpu_result[2] + "msec\n";
							res += "[WHITE]CURRENT UPDATE QUEUE SIZE: [YELLOW]" + gpu_result[3] + "\n";
							res += "[WHITE]CURRENT LD QUEUE SIZE: [YELLOW]" + gpu_result[4] + "\n";
							res += "[WHITE]CURRENT MD5 QUEUE SIZE: [YELLOW]" + gpu_result[5] + "\n";
							
							gpu_socket.close();
						}catch(Exception e){
							res = "[RED]Node connection not available...";
							e.printStackTrace();
						}

					}
				
				}
			}
			*/
			return res;
		}

	}
	public CLI_show_stats_gpu _cli_show_stats_gpu = new CLI_show_stats_gpu();

	public class GRP_GPUB extends GroupDescriptor {

		public GRP_GPUB() {
			super("GPUB", "GPU Balancer statistics", "", "");
		}

		public String execute(Object[] params) {
			
			return (params == null ?"NO PARAMS" : "PARAM COUNTL : " + params.length);
		}

	}
	
	public class CLI_show_stats_flood extends MethodDescriptor {

		public CLI_show_stats_flood() {
			super("FLOOD", "Flood detection statistics ", "STATS,SHOW", "show_details:search_ptrn");
		}

		public String execute(Object[] params) {
			String res = "";
			
			res += "[BLUE]Global statistics MO \n";
			res += "[WHITE] CURRENT MO/min: [GREEN]" + FloodManager.get_global_current(GlobalMaxType.GLOBAL_MO_MINUTE) + "\n";
			res += "[WHITE]CURRENT MO/hour: [YELLOW]" + FloodManager.get_global_current(GlobalMaxType.GLOBAL_MO_HOUR) + "\n";
			res += "[WHITE] CURRENT MO/day: [RED]" + FloodManager.get_global_current(GlobalMaxType.GLOBAL_MO_DAY) + "\n";
			res += "[BLUE]Global statistics MT \n";
			res += "[WHITE] CURRENT MT/min: [GREEN]" + FloodManager.get_global_current(GlobalMaxType.GLOBAL_MT_MINUTE) + "\n";
			res += "[WHITE]CURRENT MT/hour: [YELLOW]" + FloodManager.get_global_current(GlobalMaxType.GLOBAL_MT_HOUR) + "\n";
			res += "[WHITE] CURRENT MT/day: [RED]" + FloodManager.get_global_current(GlobalMaxType.GLOBAL_MT_DAY) + "\n";
			res += "[BLUE]Destination statistics: \n";
			res += "[WHITE]Item count: [[GREEN]" + FloodManager.flood_map.size()  + "[WHITE]]" + "\n";
			FloodItem fi = null;
			ParamDescriptor pd1 = (ParamDescriptor)params[0];
			ParamDescriptor pd2 = (ParamDescriptor)params[1];
			
			if(pd1.value != null){
				if(pd1.value.equalsIgnoreCase("1")){
					res += "[BLUE]Item search results: \n";
					for(String s : FloodManager.flood_map.keySet()){
						fi = FloodManager.flood_map.get(s);
						if(fi != null){
							if(pd2.value != null){
								if(s.contains(pd2.value)){
									res += "[WHITE]" + s + " = " + "[GREEN]" +  fi.minute + "[WHITE]/[YELLOW]" + fi.hour + "[WHITE]/[RED]" + fi.day + "\n"; 
								}
							}else{
								res += "[WHITE]" + s + " = " + "[GREEN]" +  fi.minute + "[WHITE]/[YELLOW]" + fi.hour + "[WHITE]/[RED]" + fi.day + "\n"; 
								
							}
						}
					}
					
				}
			}
			return res;
		}

	}
	public CLI_show_stats_flood _cli_show_stats_flood = new CLI_show_stats_flood();	
	
	public class CLI_flood_save extends MethodDescriptor {

		public CLI_flood_save() {
			super("SAVE", "Save current snapshot ", "FLOOD", "");
		}

		public String execute(Object[] params) {
			String tmp_name = "conf/tmp.flood." + System.currentTimeMillis() + ".properties";
			FloodManager.save("conf/tmp.flood." + System.currentTimeMillis() + ".properties");
			return "[WHITE]Flood manager snapshot saved to file [[GREEN]" + tmp_name + "[WHITE]]!";
		}

	}
	public CLI_flood_save _cli_flood_save = new CLI_flood_save();	
	
	
	public GRP_GPUB _grp_GPUB = new GRP_GPUB();


	public class CLI_gpub_start extends MethodDescriptor {

		public CLI_gpub_start() {
			super("START", "Start GPU Balancer ", "GPUB", "");
		}

		public String execute(Object[] params) {
			GPUBalancer.init();
			FNConfigData.gpub_status = true;
			return "[WHITE]Starting GPU Balancer...\n";
		}

	}
	public CLI_gpub_start _cli_gpub_start = new CLI_gpub_start();

	public class CLI_gpub_stop extends MethodDescriptor {

		public CLI_gpub_stop() {
			super("STOP", "Stop GPU Balancer ", "GPUB", "");
		}

		public String execute(Object[] params) {
			GPUBalancer.stop();
			FNConfigData.gpub_status = false;
			return "[WHITE]Stopping GPU Balancer...\n";
		}

	}
	public CLI_gpub_stop _cli_gpub_stop = new CLI_gpub_stop();

	public class CLI_gpub_status extends MethodDescriptor {

		public CLI_gpub_status() {
			super("STATUS", "Show GPU Balancer status ", "GPUB", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]GPU Balancer status: [" + (FNConfigData.gpub_status ? "[GREEN]ACTIVE" : "[RED]INACTIVE") + "[WHITE]]!";
		}

	}
	public CLI_gpub_status _cli_gpub_status = new CLI_gpub_status();

	public class CLI_gpub_info extends MethodDescriptor {

		public CLI_gpub_info() {
			super("INFO", "Show GPU Node list ", "GPUB", "");
		}

		public String execute(Object[] params) {
			NodeDescriptor nd = null;
			String res = "";
			if(FNConfigData.gpub_status){
				res += "[WHITE]GPU Nodes:\n";
				for(int i = 0; i<GPUBalancer.node_lst.size(); i++){
					nd = GPUBalancer.node_lst.get(i);
					res += "[WHITE]Node ID: [[GREEN]" + nd.id + "[WHITE]], host: [[GREEN]" + nd.host + "[WHITE]], port: [[GREEN]" + 
							nd.port + "[WHITE]], gpu count: [[GREEN]" + (nd.connections.size()-1) + "[WHITE]]!\n";  
				}
				
			}
			return res;
		}

	}
	public CLI_gpub_info _cli_gpub_info = new CLI_gpub_info();

	public class GRP_SGN extends GroupDescriptor {

		public GRP_SGN() {
			super("SGN", "Signaling Nodes ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_SGN _grp_SGN = new GRP_SGN();


	public class CLI_sg_info extends MethodDescriptor {

		public CLI_sg_info() {
			super("INFO", "Show Signaling Node List ", "SGN", "");
		}

		public String execute(Object[] params) {
			SGDescriptor sgd = null;
			String res = "";
			res += "[BLUE]Signaling Gateway Nodes:\n";
			for(String k : SGNManager.sgn_map.keySet()){
				sgd = SGNManager.sgn_map.get(k);
				res += "[WHITE]Node ID: [[GREEN]" + sgd.id + "[WHITE]], host: [[GREEN]" + sgd.address + "[WHITE]], port: [[GREEN]" + 
						sgd.port + "[WHITE]], active: [[GREEN]" + sgd.active + "[WHITE]]!\n";  
				
			}
			return res;
		}

	}
	public CLI_sg_info _cli_sg_info = new CLI_sg_info();

	public class GRP_DB extends GroupDescriptor {

		public GRP_DB() {
			super("DB", "Database statistics ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_DB _grp_DB = new GRP_DB();


	public class CLI_db_info extends MethodDescriptor {

		public CLI_db_info() {
			super("INFO", "Database information ", "DB", "");
		}

		public String execute(Object[] params) {
			String res = "";
			res += "[BLUE]DB Info\n";
			res += "[WHITE]Name: [[GREEN]" + FNConfigData.db_name + "[WHITE]], Host: [[GREEN]" + FNConfigData.db_host + "[WHITE]], Port: [[GREEN]" + 
					FNConfigData.db_port + "[WHITE]], Username: [[GREEN]" + FNConfigData.db_username + "[WHITE]], Password: [[GREEN]" + FNConfigData.db_password + "[WHITE]]!\n";
			res += "[WHITE]  DB queue size: [[GREEN]" + DBManager.get_queue_size() + "[WHITE]]!\n";
			res += "[WHITE]SMS buffer size: [[GREEN]" + DBManager.dbq_sms.size() + "[WHITE]]!\n";
			res += "[WHITE]SRI buffer size: [[GREEN]" + DBManager.dbq_sri.size() + "[WHITE]]!\n";
			res += "[WHITE]ISUP buffer size: [[GREEN]" + DBManager.dbq_isup.size() + "[WHITE]]!\n";
			return res;
		}

	}
	public CLI_db_info _cli_db_info = new CLI_db_info();

	public class CLI_db_flush extends MethodDescriptor {

		public CLI_db_flush() {
			super("FLUSH", "Flush database buffers ", "DB", "");
		}

		public String execute(Object[] params) {
			String res = "";
			res += "[WHITE]Flushing DB buffer...\n";
			res += "[WHITE]SMS buffer size: [[GREEN]" + DBManager.dbq_sms.size() + "[WHITE]]!\n";
			res += "[WHITE]SRI buffer size: [[GREEN]" + DBManager.dbq_sri.size() + "[WHITE]]!\n";
			res += "[WHITE]ISUP buffer size: [[GREEN]" + DBManager.dbq_isup.size() + "[WHITE]]!\n";
			DBManager.buffer_flush();
			return res;
		}

	}
	public CLI_db_flush _cli_db_flush = new CLI_db_flush();

	public class GRP_CONF extends GroupDescriptor {

		public GRP_CONF() {
			super("CONF", "Configuration management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_CONF _grp_CONF = new GRP_CONF();


	public class CLI_conf_get extends MethodDescriptor {

		public CLI_conf_get() {
			super("GET", "Show configuration item ", "CONF", "conf_item");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				if(pd.value != null){
					String val = ConfigManagerV2.get(pd.value);
					if(val != null) return "[WHITE]CONF Item: [[GREEN]" + pd.value + "[WHITE]], value: [[GREEN]" + val + "[WHITE]]!";
				
				}				
			}
			return "";
		}

	}
	public CLI_conf_get _cli_conf_get = new CLI_conf_get();

	public class CLI_conf_set extends MethodDescriptor {

		public CLI_conf_set() {
			super("SET", "Set configuration item ", "CONF", "conf_item:value");
		}

		public String execute(Object[] params) {
			if(params != null){
				if(params.length >= 2){
					ParamDescriptor pd1 = (ParamDescriptor)params[0];
					ParamDescriptor pd2 = (ParamDescriptor)params[1];
					if(pd1.value != null && pd2.value != null){
						ConfigManagerV2.set(pd1.value, pd2.value, null);
						FNConfigData.set(pd1.value, pd2.value);
						return "[WHITE]CONF Item: [[GREEN]" + pd1.value + "[WHITE]], value: [[GREEN]" + FNConfigData.get(pd1.value) + "[WHITE]] set!";
					
					}				
				}

			}

			return "";
		}

	}
	public CLI_conf_set _cli_conf_set = new CLI_conf_set();

	public class CLI_conf_search extends MethodDescriptor {

		public CLI_conf_search() {
			super("SEARCH", "Search configuration ", "CONF", "conf_item");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				int max_l = 0;
				if(pd.value != null){
					String[] res_lst = FNConfigData.search(pd.value);
					// max conf item length
					for(int i = 0; i<res_lst.length; i++) if(res_lst[i].length() > max_l) max_l = res_lst[i].length();
					String res = "";
					// result
					for(int i = 0; i<res_lst.length; i++) res += "[WHITE]" + String.format("%" + max_l + "s", res_lst[i]) + " = [[GREEN]" + FNConfigData.get(res_lst[i]) + "[WHITE]]\n";
					return res;
					
				}
				
			}
			return "";
		}

	}
	public CLI_conf_search _cli_conf_search = new CLI_conf_search();

	public class CLI_uptime_get extends MethodDescriptor {

		public CLI_uptime_get() {
			super("UPTIME", "Show running time ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]UPTIME: [GREEN]" + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.SYSTEM_STATS.STARTUP_TS);
		}

	}
	public CLI_uptime_get _cli_uptime_get = new CLI_uptime_get();

	public class CLI_sms_get extends MethodDescriptor {

		public CLI_sms_get() {
			super("SMS", "Get sms from database", "", "sms_id");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				if(pd.value != null){
					DBRecordSMS dbr = DBManager.getSMSByID(Long.parseLong(pd.value), null);
					String res = "";
					if(dbr != null){
						res += "[WHITE]Id: [GREEN]" + dbr.id + "\n";
						res += "[WHITE]Direction: [GREEN]" + dbr.direction + "\n";
						res += "[WHITE]Type: [GREEN]" + dbr.type + "\n";
						res += "[WHITE]GT Called: [GREEN]" + dbr.gt_called + "\n";
						res += "[WHITE]GT Calling: [GREEN]" + dbr.gt_calling + "\n";
						res += "[WHITE]Imsi: [GREEN]" + dbr.imsi + "\n";
						res += "[WHITE]Msisdn: [GREEN]" + dbr.msisdn + "\n";
						res += "[WHITE]Scda: [GREEN]" + dbr.scda + "\n";
						res += "[WHITE]Scoa: [GREEN]" + dbr.scoa + "\n";
						res += "[WHITE]Sms destination: [GREEN]" + dbr.sms_destination + "\n";
						res += "[WHITE]Sms destination enc: [GREEN]" + dbr.sms_destination_enc + "\n";
						res += "[WHITE]Sms originating: [GREEN]" + dbr.sms_originating + "\n";
						res += "[WHITE]Sms originating enc: [GREEN]" + dbr.sms_originating_enc + "\n";
						//res += "[WHITE]Sms text: [GREEN]" + dbr.sms_text + "\n";
						res += "[WHITE]Sms text enc: [GREEN]" + dbr.sms_text_enc + "\n";
						res += "[WHITE]Tcap sid: [GREEN]" + dbr.tcap_sid + "\n";
						res += "[WHITE]Tcap did: [GREEN]" + dbr.tcap_did + "\n";
						
						res = Utils.str_align(res, ":");
						res += "\n[WHITE]Sms text:\n" + dbr.sms_text;
						
						return res;
						
					}else res = "SMS not found!";
				}
			}
			return "";
		}

	}
	public CLI_sms_get _cli_sms_get = new CLI_sms_get();

	public class GRP_FILTER extends GroupDescriptor {

		public GRP_FILTER() {
			super("FILTER", "Filter management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_FILTER _grp_FILTER = new GRP_FILTER();


	public class CLI_filter_start extends MethodDescriptor {

		public CLI_filter_start() {
			super("START", "Start filter upload ", "FILTER", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				
				CLIConnection conn = (CLIConnection)params[0];
				conn.tmp_smsfs_filter = "";
				conn.filterMode = true;
				res += "[GREEN]Start sending SMSFS Data!\n";

			}
			
			return res;
		}

	}
	public CLI_filter_start _cli_filter_start = new CLI_filter_start();

	public class CLI_filter_end extends MethodDescriptor {

		public CLI_filter_end() {
			super("END", "Stop filter upload ", "FILTER", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]Filter mode not started!";
		}

	}
	public CLI_filter_end _cli_filter_end = new CLI_filter_end();

	public class CLI_filter_activate extends MethodDescriptor {

		public CLI_filter_activate() {
			super("ACTIVATE", "Activate uploaded filter ", "FILTER", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.tmp_smsfs_filter != ""){
					SmsfsManager.filterActivate_str(conn.tmp_smsfs_filter);
					res = "[GREEN]Filter activated!\n";
					
				}else{
					res = "[RED]Filter not available!\n";
				}
			}
			return res;
		}

	}
	public CLI_filter_activate _cli_filter_activate = new CLI_filter_activate();

	public class CLI_filter_save extends MethodDescriptor {

		public CLI_filter_save() {
			super("SAVE", "Save uploaded filter ", "FILTER", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.tmp_smsfs_filter != ""){
					String fname = SmsfsManager.filterSave(conn.tmp_smsfs_filter);
					if(fname != null){
						res += "[WHITE]Filter [ [GREEN]" + fname + "[WHITE] ] saved!\n";
						
					}else res += "[RED]Error while saving filter!\n";
					
				}else{
					res += "[RED]Filter not available!\n";
				}

			}
			return res;
		}

	}
	public CLI_filter_save _cli_filter_save = new CLI_filter_save();

	public class CLI_filter_get extends MethodDescriptor {

		public CLI_filter_get() {
			super("GET", "Show current filter (display_mode 0 = NORMAL, display_mode 1 = XML)", "FILTER", "display_mode");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				if(pd.value != null){
					if(pd.value.equals("1")) return SmsfsManager.currentFilter2xml();
					else if(pd.value.equals("0")) return SmsfsManager.current_filter_data;
				}
			}
			return "";
		}

	}
	public CLI_filter_get _cli_filter_get = new CLI_filter_get();

	public class GRP_REFILTER extends GroupDescriptor {

		public GRP_REFILTER() {
			super("REFILTER", "Refilter management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_REFILTER _grp_REFILTER = new GRP_REFILTER();


	public class CLI_refilter_status extends MethodDescriptor {

		public CLI_refilter_status() {
			super("STATUS", "Show refilter status ", "REFILTER", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]Refiltering status: [ " + (DBManager.refiltering_active ? "[GREEN]ACTIVE" : "[RED]INACTIVE") + "[WHITE] ]!";
		}

	}
	public CLI_refilter_status _cli_refilter_status = new CLI_refilter_status();

	public class CLI_refilter_start extends MethodDescriptor {

		public CLI_refilter_start() {
			super("START", "Start refiltering process", "REFILTER", "time_from:time_to");
		}

		public String execute(Object[] params) {
			String[] tmp;
			String[] tmp2;
			if(!DBManager.refiltering_active){
				if(params != null){
					ParamDescriptor pd1 = (ParamDescriptor)params[0];
					ParamDescriptor pd2 = (ParamDescriptor)params[1];
					if(pd1.value != null && pd2.value != null){
						tmp = pd1.value.split("-");
						tmp2 = pd2.value.split("-");
						if(tmp.length == 4 && tmp2.length == 4){
							pd1.value = tmp[0] + "-" + tmp[1] + "-" + tmp[2] + " " + tmp[3];
							pd2.value = tmp2[0] + "-" + tmp2[1] + "-" + tmp2[2] + " " + tmp2[3];
							if(Utils.check_mysql_ts(pd1.value) && Utils.check_mysql_ts(pd2.value)){
								DBManager.refilterRetainedSMS(pd1.value, pd2.value);
								return "[WHITE]Refiltering timespan: [ [GREEN]" + pd1.value + "[WHITE] - [GREEN]" + pd2.value + "[WHITE] ]!" + "\n";
								
							}else return "[RED]Invalid timestamp, format should be [[GREEN]YYYY-MM-DD-HH:MM:SS[RED]]!";
						}else return "[RED]Invalid timestamp, format should be [[GREEN]YYYY-MM-DD-HH:MM:SS[RED]]!";
						
					}else DBManager.refilterRetainedSMS(null, null);
					return "[WHITE]Starting Refiltering process...";
					
				}else return "";
			}else return "[YELLOW]Refiltering process already started!";
		}

	}
	public CLI_refilter_start _cli_refilter_start = new CLI_refilter_start();

	public class CLI_refilter_stop extends MethodDescriptor {

		public CLI_refilter_stop() {
			super("STOP", "Stop refiltering process ", "REFILTER", "");
		}

		public String execute(Object[] params) {
			DBManager.stopRefiltering();
			return "[WHITE]Stopping Refiltering process...";
		}

	}
	public CLI_refilter_stop _cli_refilter_stop = new CLI_refilter_stop();

	public class CLI_refilter_pos extends MethodDescriptor {

		public CLI_refilter_pos() {
			super("POS", "Show refiltering position ", "REFILTER", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]Refiltering position (CURRENT ID / MAX ID): [ [GREEN]" + StatsManager.REFILTER_STATS.POSITION + "[WHITE] / [RED]" + StatsManager.REFILTER_STATS.MAX_ID +  "[WHITE] ]!";
		}

	}
	public CLI_refilter_pos _cli_refilter_pos = new CLI_refilter_pos();

	public class CLI_refilter_time extends MethodDescriptor {

		public CLI_refilter_time() {
			super("TIME", "Show elapsed refiltering time ", "REFILTER", "");
		}

		public String execute(Object[] params) {
			if(DBManager.refiltering_active){
				return "[WHITE]Current Refiltering elapsed time, still working...: [ [GREEN]" + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.REFILTER_STATS.START_TIME) +  "[WHITE] ]!";
				
			}else{
				return "[WHITE]Last Refiltering elapsed time: [ [GREEN]" + Utils.tsElapsed2String(StatsManager.REFILTER_STATS.ELAPSED_TIME) +  "[WHITE] ]!";
				
			}
		}

	}
	public CLI_refilter_time _cli_refilter_time = new CLI_refilter_time();

	public class CLI_version_get extends MethodDescriptor {

		public CLI_version_get() {
			super("VERSION", "Show system version ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]VAZOM VERSION: [[GREEN]" + FNode.class.getPackage().getSpecificationVersion() + ".[YELLOW]" + FNode.class.getPackage().getImplementationVersion() + "[WHITE]]";
		}

	}
	public CLI_version_get _cli_version_get = new CLI_version_get();

	public class CLI_bye_execute extends MethodDescriptor {

		public CLI_bye_execute() {
			super("BYE", "Disconnect ", "", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				conn.stopping = true;
			}
			return "";
		}

	}
	public CLI_bye_execute _cli_bye_execute = new CLI_bye_execute();

	public class CLI_help_execute extends MethodDescriptor {

		public CLI_help_execute() {
			super("HELP", "Show help ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]Help pending...";
		}

	}
	public CLI_help_execute _cli_help_execute = new CLI_help_execute();

	public class CLI_halt_execute extends MethodDescriptor {

		public CLI_halt_execute() {
			super("HALT", "Stop current node ", "", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.halt_counter >= 2){
					new Thread(){
						public void run(){
							try{ Thread.sleep(2000); }catch(Exception e){ e.printStackTrace(); }
							System.exit(0);
						}
					}.start();
					return "[RED]Node shutting down...";
				}else{
					conn.halt_counter++;
					return "[RED]Node shutdown initiated, please confirm by executing [[GREEN]HALT[RED]] for [[GREEN]" + (3 - conn.halt_counter) + "[RED]] more times!";
				}
			}			
			return "";
		}

	}
	public CLI_halt_execute _cli_halt_execute = new CLI_halt_execute();

	
	
	
	public class GRP_SMSFS extends GroupDescriptor {

		public GRP_SMSFS() {
			super("SMSFS", "SMSFS management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_SMSFS _grp_SMSFS = new GRP_SMSFS();


	public class GRP_LIST extends GroupDescriptor {

		public GRP_LIST() {
			super("LIST", "List management ", "SMSFS", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_LIST _grp_LIST = new GRP_LIST();


	public class CLI_smsfs_list_info extends MethodDescriptor {

		public CLI_smsfs_list_info() {
			super("INFO", "List information ", "LIST,SMSFS", "");
		}

		public String execute(Object[] params) {
			String res = "[BLUE]SMSFS List information:\n";
			/*
			for(String s : ListManager.map.keySet()){
				res += "[WHITE]List ID: [[GREEN]" + s + "[WHITE]], size: [[GREEN]" + ListManager.getList(s).size() + "[WHITE]]\n"; 
			}
			*/
			for(String s : ListManagerV2.list_map.keySet()){
				res += "[WHITE]List ID: [[GREEN]" + s + "[WHITE]], size: [[GREEN]" + ListManagerV2.list_map.get(s).get_list(false).size() + "[WHITE]]\n"; 
			}
			return res;
		}

	}
	public CLI_smsfs_list_info _cli_smsfs_list_info = new CLI_smsfs_list_info();

	public class CLI_smsfs_list_addlst extends MethodDescriptor {

		public CLI_smsfs_list_addlst() {
			super("ADDLST", "Create new list ", "LIST,SMSFS", "list_id");
		}

		public String execute(Object[] params) {
			ParamDescriptor pd1 = (ParamDescriptor)params[0];
			if(pd1.value != null){
				//ListManager.initList(pd1.value.toUpperCase());
				ListManagerV2.add_list(new SmsfsList(pd1.value.toUpperCase(), FNConfigData.smsfs_lists_max));
				return "[WHITE]Initializing new SMSFS List, ID = [[GREEN]" + pd1.value + "[WHITE]]!"; 
			}
			return null;
		}

	}
	public CLI_smsfs_list_addlst _cli_smsfs_list_addlst = new CLI_smsfs_list_addlst();

	public class CLI_smsfs_list_dellst extends MethodDescriptor {

		public CLI_smsfs_list_dellst() {
			super("RMLST", "Remove list ", "LIST,SMSFS", "list_id");
		}

		public String execute(Object[] params) {
			ParamDescriptor pd1 = (ParamDescriptor)params[0];
			if(pd1.value != null){
				//ListManager.removeList(pd1.value.toUpperCase());
				ListManagerV2.del_list(ListType.SMSFS_LIST, pd1.value.toUpperCase());
				return "[WHITE]Removing SMSFS List, ID = [[GREEN]" + pd1.value + "[WHITE]]!"; 
			}
			return null;
		}

	}
	public CLI_smsfs_list_dellst _cli_smsfs_list_dellst = new CLI_smsfs_list_dellst();

	public class CLI_smsfs_lists_add_val extends MethodDescriptor {

		public CLI_smsfs_lists_add_val() {
			super("ADDVAL", "Add new value to the list ", "LIST,SMSFS", "list_id:value");
		}

		public String execute(Object[] params) {
			ParamDescriptor pd1 = (ParamDescriptor)params[0];
			ParamDescriptor pd2 = (ParamDescriptor)params[1];
			if(pd1.value != null && pd2.value != null){
				//ListManager.add(pd1.value.toUpperCase(), pd2.value);
				ListBase lb = ListManagerV2.get_list(ListType.SMSFS_LIST, pd1.value.toUpperCase());
				if(lb != null){
					lb.add(pd2.value.getBytes(), false, true);
					return "[WHITE]Adding value [[GREEN]" + pd2.value + "[WHITE]] to SMSFS List, ID = [[GREEN]" + pd1.value + "[WHITE]]!"; 
				}else return "[RED]SMSFS List id = [" + pd1.value + "] does not exist!";
			}
			return null;
		}

	}
	public CLI_smsfs_lists_add_val _cli_smsfs_lists_add_val = new CLI_smsfs_lists_add_val();

	public class CLI_smsfs_list_rm_val extends MethodDescriptor {

		public CLI_smsfs_list_rm_val() {
			super("RMVAL", "Remove value from the list ", "LIST,SMSFS", "list_id:value");
		}

		public String execute(Object[] params) {
			ParamDescriptor pd1 = (ParamDescriptor)params[0];
			ParamDescriptor pd2 = (ParamDescriptor)params[1];
			if(pd1.value != null && pd2.value != null){
				//ListManager.removeFromListByValue(pd1.value.toUpperCase(), pd2.value);
				ListBase lb = ListManagerV2.get_list(ListType.SMSFS_LIST, pd1.value.toUpperCase());
				if(lb != null){
					lb.remove(pd2.value.getBytes(), false);
					return "[WHITE]Removing value [[GREEN]" + pd2.value + "[WHITE]] from SMSFS List, ID = [[GREEN]" + pd1.value + "[WHITE]]!"; 
				}else return "[RED]SMSFS List id = [" + pd1.value + "] does not exist!";
			}
			return null;
		}

	}
	public CLI_smsfs_list_rm_val _cli_smsfs_list_rm_val = new CLI_smsfs_list_rm_val();
	

    
	public CLIManager(){
		super();
		// methods
		method_lst.add(_cli_show_memory);
		method_lst.add(_cli_show_stats_system);
		method_lst.add(_cli_show_stats_db);
		method_lst.add(_cli_show_stats_vstp);
		method_lst.add(_cli_show_stats_smsfs);
		method_lst.add(_cli_show_stats_gpub);
		method_lst.add(_cli_show_stats_gpu);
		method_lst.add(_cli_show_stats_flood);
		method_lst.add(_cli_flood_save);
		method_lst.add(_cli_gpub_start);
		method_lst.add(_cli_gpub_stop);
		method_lst.add(_cli_gpub_status);
		method_lst.add(_cli_gpub_info);
		method_lst.add(_cli_sg_info);
		method_lst.add(_cli_db_info);
		method_lst.add(_cli_db_flush);
		method_lst.add(_cli_conf_get);
		method_lst.add(_cli_conf_set);
		method_lst.add(_cli_conf_search);
		method_lst.add(_cli_uptime_get);
		method_lst.add(_cli_sms_get);
		method_lst.add(_cli_filter_start);
		method_lst.add(_cli_filter_end);
		method_lst.add(_cli_filter_activate);
		method_lst.add(_cli_filter_save);
		method_lst.add(_cli_filter_get);
		method_lst.add(_cli_refilter_status);
		method_lst.add(_cli_refilter_start);
		method_lst.add(_cli_refilter_stop);
		method_lst.add(_cli_refilter_pos);
		method_lst.add(_cli_refilter_time);
		method_lst.add(_cli_version_get);
		method_lst.add(_cli_bye_execute);
		method_lst.add(_cli_help_execute);
		method_lst.add(_cli_halt_execute);
		method_lst.add(_cli_smsfs_list_info);
		method_lst.add(_cli_smsfs_list_addlst);
		method_lst.add(_cli_smsfs_list_dellst);
		method_lst.add(_cli_smsfs_lists_add_val);
		method_lst.add(_cli_smsfs_list_rm_val);
	
		// auth
        method_lst.add(_cli_user_list);
        method_lst.add(_cli_grp_list);
        method_lst.add(_cli_user_add);
        method_lst.add(_cli_user_set);
        method_lst.add(_cli_user_rm);
        method_lst.add(_cli_users_passwd);

		

		// groups
		group_lst.add(_grp_STATS);
		group_lst.add(_grp_SHOW);
		group_lst.add(_grp_FLOOD);
		group_lst.add(_grp_GPUB);
		group_lst.add(_grp_SGN);
		group_lst.add(_grp_DB);
		group_lst.add(_grp_CONF);
		group_lst.add(_grp_FILTER);
		group_lst.add(_grp_REFILTER);
		group_lst.add(_grp_LIST);
		group_lst.add(_grp_SMSFS);
		group_lst.add(_grp_AUTH);
	}


	
}
