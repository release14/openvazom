package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SequenceNum extends OCTET_STRING{

	public SequenceNum(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
