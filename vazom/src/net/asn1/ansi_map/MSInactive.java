package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MSInactive extends SET{

	public ElectronicSerialNumber get_lectronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_lectronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(1).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(2).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public DeregistrationType get_deregistrationType(){
		return (DeregistrationType)elements.get(3).data;
	}
	public DeregistrationType new_deregistrationType(){
		return new DeregistrationType();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(4).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(5).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(6).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public ServicesResult get_servicesResult(){
		return (ServicesResult)elements.get(7).data;
	}
	public ServicesResult new_servicesResult(){
		return new ServicesResult();
	}

	public SMS_MessageWaitingIndicator get_sms_MessageWaitingIndicator(){
		return (SMS_MessageWaitingIndicator)elements.get(8).data;
	}
	public SMS_MessageWaitingIndicator new_sms_MessageWaitingIndicator(){
		return new SMS_MessageWaitingIndicator();
	}

	public MEID get_meid(){
		return (MEID)elements.get(9).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_lectronicSerialNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(73, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_deregistrationType(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(204, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servicesResult(); }});
		elements.add(new ElementDescriptor(118, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_MessageWaitingIndicator(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public MSInactive(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
