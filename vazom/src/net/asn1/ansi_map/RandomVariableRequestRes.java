package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RandomVariableRequestRes extends SET{

	public RandomVariable get_randomVariable(){
		return (RandomVariable)elements.get(0).data;
	}
	public RandomVariable new_randomVariable(){
		return new RandomVariable();
	}

	public RANDValidTime get_randValidTime(){
		return (RANDValidTime)elements.get(1).data;
	}
	public RANDValidTime new_randValidTime(){
		return new RANDValidTime();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(40, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariable(); }});
		elements.add(new ElementDescriptor(148, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_randValidTime(); }});
	}
	public RandomVariableRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
