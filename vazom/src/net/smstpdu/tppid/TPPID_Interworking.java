package net.smstpdu.tppid;

import java.util.HashMap;

public enum TPPID_Interworking {
	NO_INTERWORKING_BUT_SME_TO_SME(0x00),
	TELEMATIC_INTERWORKING(0x20);
	
	// BIT 5 = 00X00000
	
	private int id;
	private static final HashMap<Integer, TPPID_Interworking> lookup = new HashMap<Integer, TPPID_Interworking>();
	static{
		for(TPPID_Interworking td : TPPID_Interworking.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static TPPID_Interworking get(int id){ return lookup.get(id); }
	private TPPID_Interworking(int _id){ id = _id; }
	
}
