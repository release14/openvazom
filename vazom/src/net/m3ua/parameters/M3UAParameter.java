package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public abstract class M3UAParameter {
	protected M3UAParameterType type;
	protected int length;
	protected int byte_pos = 0;
	public byte[] value;
	
	public abstract void init(byte[] data);
	public int hasExtraLength(){ return 0; }
	
	public byte[] encode(){
		byte[] res = new byte[4 + value.length];
		// parameter type = two bytes
		// parameter length = two bytes
		// data
		res[0] = (byte)(type.getId() >> 8);
		res[1] = (byte)(type.getId());
		res[2] = (byte)((value.length + 4) >> 8);
		res[3] = (byte)(value.length + 4);
		
		// get actual data
		for(int i = 0; i<value.length; i++) res[i + 4] = value[i];
		return res;
		
	}
}
