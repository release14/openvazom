package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MT_ForwardSM_VGCS_Arg extends SEQUENCE{

	public ASCI_CallReference get_asciCallReference(){
		return (ASCI_CallReference)elements.get(0).data;
	}
	public ASCI_CallReference new_asciCallReference(){
		return new ASCI_CallReference();
	}

	public SM_RP_OA get_sm_RP_OA(){
		return (SM_RP_OA)elements.get(1).data;
	}
	public SM_RP_OA new_sm_RP_OA(){
		return new SM_RP_OA();
	}

	public SignalInfo get_sm_RP_UI(){
		return (SignalInfo)elements.get(2).data;
	}
	public SignalInfo new_sm_RP_UI(){
		return new SignalInfo();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_asciCallReference(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sm_RP_OA(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sm_RP_UI(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public MT_ForwardSM_VGCS_Arg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
