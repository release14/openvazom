package net.hplmnr;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import net.config.SGNConfigData;
import net.ds.DataSourceType;
import net.logging.LoggingManager;
import net.m3ua.messages.DATA;
import net.m3ua_conn.M3UAConnManager;
import net.m3ua_conn.M3UAConnection;
import net.m3ua_conn.M3UAPayload;
import net.mtp3_conn.MTP3ConnManager;
import net.mtp3_conn.MTP3Connection;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionType;
import net.routing.RoutingConnectionWorkerMethod;
import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.NumberingPlan;
import net.sctp.SSCTPDescriptor;
import net.sgc.SGCManager;
import net.sgn.FNManager;
import net.sgn.SGNode;
import net.smstpdu.MessageDirection;
import net.smstpdu.SmsType;
import net.smstpdu.TBCD;
import net.smstpdu.tpdu.SmsDeliver;
import net.smstpdu.tpdu.SmsSubmit;
import net.smstpdu.tpdu.TPDU;
import net.smstpdu.tpdu.udh.IE_ConcatenatedReference;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.HeaderDescriptor;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;
import net.vstp.VSTP_SGF_CorrelationPacket;
import net.vstp.VSTP_SGF_CorrelationManager;

public class HPLMNRManager {
	public static Logger logger=Logger.getLogger(HPLMNRManager.class);
	private static ArrayList<HPLMNRDecoderWorker> decoder_workerLst;
	private static ArrayList<HPLMNREncoderWorker> encoder_workerLst;
	public static int ssctp_client_id;
	public static boolean ssctp_ready = false;
	public static ConcurrentLinkedQueue<SSCTPDescriptor> in_queue;
	public static ConcurrentLinkedQueue<HPLMNRPacket> sri_queue;
	private static long NEXT_TCAP_ID = 1;
	private static long NEXT_IMSI_CORRELATION_ID = 100000000L;
	private static ConcurrentHashMap<Long, HPLMNRCorrelationPacket> sri_correlation_map;
	private static ConcurrentHashMap<Long, MTCorrelationPacket> mt_correlation_map;
	private static ConcurrentHashMap<String, TCAPCorrelationPacket> tcap_correlation_map;
	private static ConcurrentHashMap<String, RoutingConnectionWorkerMethod> routing_conn_cnv_map;
	
	private static Timeout timeout_r;
	private static Thread timeout_t;
	public static boolean stopping;
	
	private static class Timeout implements Runnable{
		HPLMNRCorrelationPacket sri_p;
		MTCorrelationPacket mt_p;
		TCAPCorrelationPacket tcap_p;
		long ts;
		public void run() {
			LoggingManager.info(logger, "Starting...");
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.hplmnr_timeout_check_interval); }catch(Exception e){ e.printStackTrace(); }
				ts = System.currentTimeMillis();
				// sri_correlation_map
				for(Long i : sri_correlation_map.keySet()){
					sri_p = sri_correlation_map.get(i);
					if(ts - sri_p.ts > SGNConfigData.hplmnr_timeout){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "HPLMNRManager TIMEOUT: Removing HPLMNRCorrelationPacket id = [" + i + "]!");
						sri_correlation_map.remove(i);
					}
				}
				// mt_correlation_map
				for(Long i : mt_correlation_map.keySet()){
					mt_p = mt_correlation_map.get(i);
					if(ts - mt_p.ts > SGNConfigData.hplmnr_timeout){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "HPLMNRManager TIMEOUT: Removing MTCorrelationPacket id = [" + i + "]!");
						mt_correlation_map.remove(i);
					}
				}
				// tcap_correlation_map
				for(String i : tcap_correlation_map.keySet()){
					tcap_p = tcap_correlation_map.get(i);
					if(ts - tcap_p.ts > SGNConfigData.hplmnr_timeout){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "HPLMNRManager TIMEOUT: Removing TCAPCorrelationPacket id = [" + i + "]!");
						tcap_correlation_map.remove(i);
					}
				}
				

			}
			LoggingManager.info(logger, "Ending...");
		}
		
	}

	
	/*
	public static void outOffer(SSCTPDescriptor sd){
		out_queue.offer(sd);
	}
	public static SSCTPDescriptor outPoll(){
		return out_queue.poll();
	}
	*/
	
	private static class MTP3Converter extends RoutingConnectionWorkerMethod{
		public MTP3Converter(MTP3Connection conn){
			super(conn);
		}
		public void execute(Object o) {
			// TODO
		}
		
	}
	
	private static class M3UAConverter extends RoutingConnectionWorkerMethod{
		
		public M3UAConverter(M3UAConnection conn){
			super(conn);
		}

		public void execute(Object o) {
			SSCTPDescriptor _sd = (SSCTPDescriptor)o;
			// add m3ua connection as extra parameter to SSCTP
			_sd.extra_params = new ArrayList<Object>();
			_sd.extra_params.add(conn);
			in_queue.offer(_sd);
			
		}
		
	}
	
	
	private static void processFilter_sms(SSCTPDescriptor sd, HPLMNRPacket sp){
		// send to filter node
		MessageDescriptor md = new MessageDescriptor();
		NumberingPlan tmp_np = null;
		NatureOfAddress tmp_nai = null;
		TPDU tpdu = null;
		SmsSubmit sms_s = null;
		SmsDeliver sms_d = null;
		IE_ConcatenatedReference ie_ref = null;
		VSTP_SGF_CorrelationPacket vsp = null;
		try{
			// VSTP
			// header
			md.header = new HeaderDescriptor();
			md.header.destination = LocationType.FN;
			// DS
			switch(sp.routingType){
				case M3UA: md.header.ds = DataSourceType.SCTP; break;
				case MTP3: md.header.ds = DataSourceType.E1; break;
				case SMPP: md.header.ds = DataSourceType.SMPP; break;
			}
			
			md.header.msg_id = sp.calledPartyGT + "." + sp.callingPartyGT + "." + Utils.bytes2num(sp.tcap_sid) + "." + Utils.bytes2num(sp.tcap_did);
			md.header.source = LocationType.SGN;
			// vstp-sri correlation
			vsp = new VSTP_SGF_CorrelationPacket();
			//vsp.sctp_sid = sd.sid;]
			vsp.sd = sd;
			vsp.sri_packet = sp;
			vsp.type = ((RoutingConnection)sp.extra_params.get(0)).type;
			VSTP_SGF_CorrelationManager.add(md.header.msg_id, vsp);
			// distribute
			SGCManager.distribute_vstp_crl_add(md.header.msg_id, vsp);
			// stats
			StatsManager.VSTP_STATS.VSTP_CRL_ADD++;
			
			// body
			// sccp part
			md.values.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), sp.calledPartyGT);
			md.values.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), sp.callingPartyGT);
			
			md.values.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(sp.sccp_udt.calledPartyAddress.globalTitleIndicator.getId()));
			md.values.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(sp.sccp_udt.callingPartyAddress.globalTitleIndicator.getId()));
			
			md.values.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(Utils.get_called_gt_tt(sp.sccp_udt)));
			md.values.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(Utils.get_calling_gt_tt(sp.sccp_udt)));
			
			tmp_np = Utils.get_called_gt_np(sp.sccp_udt);
			if(tmp_np != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), Integer.toString(tmp_np.getId()));
			tmp_np = Utils.get_calling_gt_np(sp.sccp_udt);
			if(tmp_np != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), Integer.toString(tmp_np.getId()));
			
			tmp_nai = Utils.get_called_gt_nai(sp.sccp_udt);
			if(tmp_nai != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), Integer.toString(tmp_nai.getId()));
			tmp_nai = Utils.get_calling_gt_nai(sp.sccp_udt);
			if(tmp_nai != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), Integer.toString(tmp_nai.getId()));
			
			// m3ua 
			md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(((DATA)sp.m3ua_packet.message).protocolData.destinationPointCode));
			md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(((DATA)sp.m3ua_packet.message).protocolData.originatingPointCode));
	
			// tcap
			// TCAP SID & DID
			if(sp.tcap_sid != null) md.values.put(VSTPDataItemType.TCAP_SID.getId(), Long.toString(Utils.bytes2num(sp.tcap_sid)));
			if(sp.tcap_did != null) md.values.put(VSTPDataItemType.TCAP_DID.getId(), Long.toString(Utils.bytes2num(sp.tcap_did)));
			
			// MO
			if(sp.mo_arg != null){
				md.values.put(VSTPDataItemType.MAP_SCOA.getId(), Utils.mo_getSCOA(sp.mo_arg));
				md.values.put(VSTPDataItemType.MAP_SCDA.getId(), Utils.mo_getSCDA(sp.mo_arg));
				md.values.put(VSTPDataItemType.MAP_IMSI.getId(), Utils.mo_getIMSI(sp.mo_arg));
				//System.out.println("MO DEBUG IMSI: " + Utils.mo_getIMSI(sp.mo_arg));
				md.values.put(VSTPDataItemType.MAP_MSISDN.getId(), Utils.mo_getMSISDN(sp.mo_arg));
				
				// decode SMS TPDU
				if(sp.mo_arg.get_sm_RP_DA().get_serviceCentreAddressDA() != null)
					tpdu = TPDU.decode(sp.mo_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.MS_SC);
				else
					tpdu = TPDU.decode(sp.mo_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.SC_MS);
	
			// MT	
			}else if(sp.mt_arg != null){
				md.values.put(VSTPDataItemType.MAP_SCOA.getId(), Utils.mt_getSCOA(sp.mt_arg));
				md.values.put(VSTPDataItemType.MAP_SCDA.getId(), Utils.mt_getSCDA(sp.mt_arg));
				md.values.put(VSTPDataItemType.MAP_IMSI.getId(), Utils.mt_getIMSI(sp.mt_arg));
				//System.out.println("MT DEBUG IMSI: " + Utils.mt_getIMSI(sp.mt_arg));
				md.values.put(VSTPDataItemType.MAP_MSISDN.getId(), Utils.mt_getMSISDN(sp.mt_arg));
				
				// decode SMS TPDU
				if(sp.mt_arg.get_sm_RP_DA().get_serviceCentreAddressDA() != null)
					tpdu = TPDU.decode(sp.mt_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.MS_SC);
				else
					tpdu = TPDU.decode(sp.mt_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.SC_MS);
				
	
			}

			// SMS TPDU
			if(tpdu != null){
				switch(tpdu.type){
					case SMS_SUBMIT: 
						sms_s = (SmsSubmit)tpdu;
						md.values.put(VSTPDataItemType.SMS_TPDU_DESTINATION.getId(), Utils.SMS_SUBMIT_get_TP_DA(sms_s));
						md.values.put(VSTPDataItemType.SMS_TPDU_DESTINATION_ENC.getId(), Integer.toString(sms_s.TP_DA.typeOfNumber.getId()));
						// concatenated sms fields
						if(sms_s.udh != null){
							ie_ref = sms_s.udh.findIERef();
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
							md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(ie_ref.messageIdentifier));
							md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(ie_ref.partNumber));
							md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(ie_ref.parts));
							md.values.put(VSTPDataItemType.SMS_TPDU_UD.getId(), Utils.bytes2hexstr(Utils.SMS_TPDU_decodeUD_conc(sms_s.TP_DCS, sms_s.TP_UD, sms_s.udh.length + 1).getBytes(), ""));
						// single sms fields
						}else{
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
							md.values.put(VSTPDataItemType.SMS_TPDU_UD.getId(), Utils.bytes2hexstr(Utils.SMS_TPDU_decodeUD(sms_s.TP_DCS, sms_s.TP_UD, sms_s.TP_UDL).getBytes(), ""));
							
						}
						// common fields
						md.header.msg_type = MessageType.SMS_MO;
						md.values.put(VSTPDataItemType.SMS_TPDU_DCS.getId(), Integer.toString(sms_s.TP_DCS.encoding.getId()));
						break;
						
					case SMS_DELIVER: 
						sms_d = (SmsDeliver)tpdu; 
						md.values.put(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId(), Utils.SMS_DELIVER_get_TP_OA(sms_d));
						md.values.put(VSTPDataItemType.SMS_TPDU_ORIGINATING_ENC.getId(), Integer.toString(sms_d.TP_OA.typeOfNumber.getId()));
						// concatenated sms fields
						if(sms_d.udh != null){
							ie_ref = sms_d.udh.findIERef();
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
							md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(ie_ref.messageIdentifier));
							md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(ie_ref.partNumber));
							md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(ie_ref.parts));
							md.values.put(VSTPDataItemType.SMS_TPDU_UD.getId(), Utils.bytes2hexstr(Utils.SMS_TPDU_decodeUD_conc(sms_d.TP_DCS, sms_d.TP_UD, sms_d.udh.length + 1).getBytes(), ""));
						// single sms fields
						}else{
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
							md.values.put(VSTPDataItemType.SMS_TPDU_UD.getId(), Utils.bytes2hexstr(Utils.SMS_TPDU_decodeUD(sms_d.TP_DCS, sms_d.TP_UD, sms_d.TP_UDL).getBytes(), ""));
							
						}
						// common fields
						md.header.msg_type = MessageType.SMS_MT;
						md.values.put(VSTPDataItemType.SMS_TPDU_DCS.getId(), Integer.toString(sms_d.TP_DCS.encoding.getId()));
						break;
				
				}				
				// send to queue
				SGNode.out_offer(md);
				LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "VSTP id = [" + md.header.msg_id + "], type = [" + md.header.msg_type + "] sent to filter node!");
			}


			
			
		}catch(Exception e){
			e.printStackTrace();
		}
			
	}
	/*
	private static void processFilter_sms(SSCTPDescriptor sd, SRIPacket sp){
		SmsfsPacket fp = null;
		TPDU tpdu = null;
		SmsSubmit sms_s = null;
		SmsDeliver sms_d = null;
		//FilterResult fres = null;
		
		// smsfs = off
		if(!SGNConfigData.smsfs_status){
			logger.info("Filter bypassed for packet type: [" + sp.replyType + "]");
			out_queue.offer(sd);
			
		// smsfs = on, sri.smsfs = on
		}else if(SGNConfigData.sri_mo_mt_smsfs_status){
			fp = new SmsfsPacket();
			fp.filterType = FilterType.MT;
			fp.packetMode = ModeOfOperation.INTRUSIVE;
			fp.bindings.add(sp.calledPartyGT);
			fp.bindings.add(sp.callingPartyGT);
			
			// MO
			if(sp.mo_arg != null){
				fp.bindings.add(Utils.mo_getSCOA(sp.mo_arg));
				fp.bindings.add(Utils.mo_getSCDA(sp.mo_arg));
				fp.bindings.add(Utils.mo_getIMSI(sp.mo_arg));
				fp.bindings.add(Utils.mo_getMSISDN(sp.mo_arg));
				
				// decode SMS TPDU
				if(sp.mo_arg.get_sm_RP_DA().get_serviceCentreAddressDA() != null)
					tpdu = TPDU.decode(sp.mo_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.MS_SC);
				else
					tpdu = TPDU.decode(sp.mo_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.SC_MS);

			// MT	
			}else if(sp.mt_arg != null){
				fp.bindings.add(Utils.mt_getSCOA(sp.mt_arg));
				fp.bindings.add(Utils.mt_getSCDA(sp.mt_arg));
				fp.bindings.add(Utils.mt_getIMSI(sp.mt_arg));
				fp.bindings.add(Utils.mt_getMSISDN(sp.mt_arg));
				
				// decode SMS TPDU
				if(sp.mt_arg.get_sm_RP_DA().get_serviceCentreAddressDA() != null)
					tpdu = TPDU.decode(sp.mt_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.MS_SC);
				else
					tpdu = TPDU.decode(sp.mt_arg.get_sm_RP_UI().getValueBytes(), MessageDirection.SC_MS);
				
				
				
			}
			// M3UA
			fp.bindings.add(sp.m3ua_data.protocolData.destinationPointCode);
			fp.bindings.add(sp.m3ua_data.protocolData.originatingPointCode);

			// SMS TPDU 
			switch(tpdu.type){
				case SMS_SUBMIT: 
					sms_s = (SmsSubmit)tpdu;
					fp.bindings.add(null);
					fp.bindings.add(null);
					fp.bindings.add(Utils.SMS_SUBMIT_get_TP_DA(sms_s));
					fp.bindings.add(sms_s.TP_DA.typeOfNumber.getId());
					fp.bindings.add(Utils.SMS_TPDU_decodeUD(sms_s.TP_DCS, sms_s.TP_UD));
					fp.bindings.add(sms_s.TP_DCS.encoding.getId());
					fp.bindings.add((sms_s.udh == null ? SmsType.SINGLE.getId() : SmsType.CONCATENATED.getId()));
					break;
				case SMS_DELIVER: 
					sms_d = (SmsDeliver)tpdu; 
					fp.bindings.add(Utils.SMS_DELIVER_get_TP_OA(sms_d));
					fp.bindings.add(sms_d.TP_OA.typeOfNumber.getId());
					fp.bindings.add(null);
					fp.bindings.add(null);
					fp.bindings.add(Utils.SMS_TPDU_decodeUD(sms_d.TP_DCS, sms_d.TP_UD));
					fp.bindings.add(sms_d.TP_DCS.encoding.getId());
					fp.bindings.add((sms_d.udh == null ? SmsType.SINGLE.getId() : SmsType.CONCATENATED.getId()));
					break;
			}
			
			// save SSCTPDescriptor and SRIPacket
			fp.extra_data.add(sd);
			fp.extra_data.add(sp);

			// Filter type 
			fp.filterDescriptor = SmsfsManager.currentFilterMT;
			
			// Smsfs queue
			SmsfsManager.queue.offer(fp);

		}else{
			out_queue.offer(sd);

		}
		
		
	}
	*/
	private static void processFilter_hlr(SSCTPDescriptor sd, HPLMNRPacket sp){
		// send to filter node
		MessageDescriptor md = new MessageDescriptor();
		NumberingPlan tmp_np = null;
		NatureOfAddress tmp_nai = null;
		VSTP_SGF_CorrelationPacket vsp = null;
		int error_code;
		try{
			// VSTP
			// header
			md.header = new HeaderDescriptor();
			md.header.destination = LocationType.FN;
			md.header.ds = DataSourceType.SCTP;
			md.header.msg_type = MessageType.HLR;
			md.header.msg_id = sp.calledPartyGT + "." + sp.callingPartyGT + "." + Utils.bytes2num(sp.tcap_sid) + "." + Utils.bytes2num(sp.tcap_did);
			md.header.source = LocationType.SGN;
			// vstp-sri correlation
			vsp = new VSTP_SGF_CorrelationPacket();
			//vsp.sctp_sid = sd.sid;]
			vsp.sd = sd;
			vsp.sri_packet = sp;
			vsp.type = ((RoutingConnection)sp.extra_params.get(0)).type;
			VSTP_SGF_CorrelationManager.add(md.header.msg_id, vsp);
			// distribute
			SGCManager.distribute_vstp_crl_add(md.header.msg_id, vsp);
			// stats
			StatsManager.VSTP_STATS.VSTP_CRL_ADD++;
			
			// body
			// sccp part
			md.values.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), sp.calledPartyGT);
			md.values.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), sp.callingPartyGT);
			
			md.values.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(sp.sccp_udt.calledPartyAddress.globalTitleIndicator.getId()));
			md.values.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(sp.sccp_udt.callingPartyAddress.globalTitleIndicator.getId()));
			
			md.values.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(Utils.get_called_gt_tt(sp.sccp_udt)));
			md.values.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(Utils.get_calling_gt_tt(sp.sccp_udt)));
			
			tmp_np = Utils.get_called_gt_np(sp.sccp_udt);
			if(tmp_np != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), Integer.toString(tmp_np.getId()));
			tmp_np = Utils.get_calling_gt_np(sp.sccp_udt);
			if(tmp_np != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), Integer.toString(tmp_np.getId()));
			
			tmp_nai = Utils.get_called_gt_nai(sp.sccp_udt);
			if(tmp_nai != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), Integer.toString(tmp_nai.getId()));
			tmp_nai = Utils.get_calling_gt_nai(sp.sccp_udt);
			if(tmp_nai != null) md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), Integer.toString(tmp_nai.getId()));
			
			// m3ua 
			md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(((DATA)sp.m3ua_packet.message).protocolData.destinationPointCode));
			md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(((DATA)sp.m3ua_packet.message).protocolData.originatingPointCode));
			
			// tcap
			// MSISDN
			// TCAP SID & DID
			if(sp.tcap_sid != null) md.values.put(VSTPDataItemType.TCAP_SID.getId(), Long.toString(Utils.bytes2num(sp.tcap_sid)));
			if(sp.tcap_did != null) md.values.put(VSTPDataItemType.TCAP_DID.getId(), Long.toString(Utils.bytes2num(sp.tcap_did)));
			if(sp.invoke_id != null) md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), Long.toString(Utils.bytes2num(sp.invoke_id)));
		
			
			if(sp.msisdn != null) md.values.put(VSTPDataItemType.HLR_MSISDN.getId(), TBCD.decode(sp.msisdn.digits));
			// SCA
			if(sp.smsc != null) md.values.put(VSTPDataItemType.HLR_SCA.getId(), TBCD.decode(sp.smsc.digits));
			// IMSI
			if(sp.imsi != null) md.values.put(VSTPDataItemType.HLR_IMSI.getId(), sp.imsi);
			// NNN
			if(sp.networkNodeNumber != null) md.values.put(VSTPDataItemType.HLR_NNN.getId(), TBCD.decode(sp.networkNodeNumber.digits));
			// ANNN
			if(sp.additonalNumber != null) md.values.put(VSTPDataItemType.HLR_ANNN.getId(), TBCD.decode(sp.additonalNumber.digits));
			
			// GSM MAP ERRORS
			if(sp.sri_gsmmap_err != null){
				if(sp.sri_gsmmap_err.get_errorCode() != null){
					if(sp.sri_gsmmap_err.get_errorCode().get_localValue() != null){
						md.values.put(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId(), Long.toString(Utils.bytes2num(sp.sri_gsmmap_err.get_errorCode().get_localValue().value)));
					// Unknown ErrorCode
					}else{
						md.values.put(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId(), "9999");
					}
				}
			}
			// TCAP errors
			if((error_code = Utils.getTcapAbortErrorCode(sp.sri_tcm)) > -1){
				md.values.put(VSTPDataItemType.TCAP_ERROR_CODE.getId(), Integer.toString(error_code));

			}else if((error_code = Utils.getTcapAbortDialogueError(sp.sri_tcm)) > -1){
				md.values.put(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId(), Integer.toString(error_code));
			}
			
			
			// send to queue
			SGNode.out_offer(md);
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "VSTP id = [" + md.header.msg_id + "], type = [" + md.header.msg_type + "] sent to filter node!");
			
			
		}catch(Exception e){
			e.printStackTrace();
		}		
		
		/*
	
		M3UAConnection m3ua_conn = null;
		M3UAPayload m3ua_pl = null;
		m3ua_conn = (M3UAConnection)sd.extra_params.get(0);
		m3ua_pl = new M3UAPayload();
		m3ua_pl.sd = sd;
		m3ua_conn.out_queue.offer(m3ua_pl);	
		*/

	}
	/*
	private static void processFilter_hlr(SSCTPDescriptor sd, SRIPacket sp){
		SmsfsPacket fp = null;
		
		// smsfs = off
		if(!SGNConfigData.smsfs_status){
			logger.info("Filter bypassed for packet type: [" + sp.replyType + "]");
			out_queue.offer(sd);
			
		// smsfs = on, sri.smsfs = on
		}else if(SGNConfigData.sri_hlr_smsfs_status){
			fp = new SmsfsPacket();
			fp.filterType = FilterType.HLR;
			fp.packetMode = ModeOfOperation.INTRUSIVE;
			// M3UA
			fp.bindings.add(sp.m3ua_data.protocolData.destinationPointCode);
			fp.bindings.add(sp.m3ua_data.protocolData.originatingPointCode);
			// SCCP
			fp.bindings.add(sp.calledPartyGT);
			fp.bindings.add(sp.callingPartyGT);
			fp.bindings.add(Utils.get_called_gt_tt(sp.sccp_udt));
			fp.bindings.add(Utils.get_called_gt_nai(sp.sccp_udt).getId());
			fp.bindings.add(Utils.get_calling_gt_nai(sp.sccp_udt).getId());
			fp.bindings.add(Utils.get_called_gt_np(sp.sccp_udt).getId());
			fp.bindings.add(Utils.get_calling_gt_np(sp.sccp_udt).getId());
			fp.bindings.add(sp.sccp_udt.calledPartyAddress.globalTitle.type.getId());
			fp.bindings.add(sp.sccp_udt.callingPartyAddress.globalTitle.type.getId());
			// MAP
			fp.bindings.add(sp.imsi);
			fp.bindings.add(sp.msisdn);
			fp.bindings.add(TBCD.decode(sp.networkNodeNumber.digits));
			fp.bindings.add(TBCD.decode(sp.smsc.digits));

			// save SSCTPDescriptor and SRIPacket
			fp.extra_data.add(sd);
			fp.extra_data.add(sp);

			// Filter type 
			fp.filterDescriptor = SmsfsManager.currentFilterHLR;
			
			// Smsfs queue
			SmsfsManager.queue.offer(fp);

			

		}else{
			out_queue.offer(sd);

		}
	}
	
	*/
	public static void processFilter(SSCTPDescriptor sd, HPLMNRPacket sp){
		M3UAConnection m3ua_conn = null;
		M3UAPayload m3ua_pl = null;
		switch(sp.replyType){
			// SMS
			case MT_PHASE_1_BEGIN:
			case MT_PHASE_1_CONTINUE:
			case MT_PHASE_1_END:
			case MO_PHASE_1:
				if(FNManager.fn_connected) processFilter_sms(sd, sp);
				else{
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "No available F Nodes, bypassing filters, message sent to default M3UA Connection!");
					m3ua_conn = (M3UAConnection)sd.extra_params.get(0);
					m3ua_pl = new M3UAPayload();
					m3ua_pl.sd = sd;
					m3ua_pl.m3ua_packet = sp.m3ua_packet;
					m3ua_conn.out_offer(m3ua_pl);	
				}
				break;
			// HLR
			case SRI_PHASE_1:
			case SRI_PHASE_2_ACK:
				if(FNManager.fn_connected) processFilter_hlr(sd, sp);
				else{
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "No available F Nodes, bypassing filters, message sent to default M3UA Connection!");
					m3ua_conn = (M3UAConnection)sd.extra_params.get(0);
					m3ua_pl = new M3UAPayload();
					m3ua_pl.sd = sd;
					m3ua_pl.m3ua_packet = sp.m3ua_packet;
					m3ua_conn.out_offer(m3ua_pl);	
				}
				break;
			default:
				LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Filter bypassed for packet type: [" + sp.replyType + "]");
				m3ua_conn = (M3UAConnection)sd.extra_params.get(0);
				m3ua_pl = new M3UAPayload();
				m3ua_pl.sd = sd;
				m3ua_pl.m3ua_packet = sp.m3ua_packet;
				m3ua_conn.out_offer(m3ua_pl);	
				break;
		}
		
	}
		
	
	
	
	public static long getNext_TCAP_ID(){
		return NEXT_TCAP_ID;
	}
	public static long getNext_IMSI_CORRELATION_ID(){
		return NEXT_IMSI_CORRELATION_ID;
	}
	
	public static void consume_TCAP_ID(){
		NEXT_TCAP_ID++;
		int tbits = (int)Math.ceil(Math.log10(NEXT_TCAP_ID + 1) / Math.log10(2));
		int tbytes = (int)Math.ceil((double)tbits / 8);
		if(tbytes > 4) NEXT_TCAP_ID = 1;
		
	}
	// TCAP correlation
	public static void TCAPCorrelationPut(Long sid, Long did, TCAPCorrelationPacket p){
		p.ts = System.currentTimeMillis();
		tcap_correlation_map.put((sid == null ? "" : Long.toString(sid)) + ":" + (did == null ? "" : Long.toString(did)), p);
	}
	public static TCAPCorrelationPacket TCAPCheckCorrelation(Long sid, Long did){
		// C-E
		// ---
		// S D
		// 1 2
		//   1
		
		// B-C
		// ---
		// S D
		// 1
		// 2 1

		// B-E
		// ---
		// S D
		// 1
		//   1
		
		// C-C
		// ---
		// S D
		// 1 2
		// 2 1
		
		TCAPCorrelationPacket res = null;
		boolean sid_eq = false;
		boolean did_eq = false;
		for(String key: tcap_correlation_map.keySet()){
			res = tcap_correlation_map.get(key);
			// BEGIN/CONTINUE - CONTINUE
			if(sid != null && did != null){
				if(res.tcap_sid != null) sid_eq = (res.tcap_sid.compareTo(did) == 0); else sid_eq = true;
				if(res.tcap_did != null) did_eq = (res.tcap_did.compareTo(sid) == 0); else did_eq = true;
				if(sid_eq && did_eq) return res;
			// BEGIN/CONTINUE - END
			}else if(sid == null && did != null){
				if(res.tcap_sid != null) did_eq = (res.tcap_sid.compareTo(did) == 0);
				if(did_eq) return res;
			}
			sid_eq = false;
			did_eq = false;
		}
		return null;
	}

	
	public static TCAPCorrelationPacket TCAPCorrelationGet(Integer sid, Integer did){
		TCAPCorrelationPacket res = tcap_correlation_map.get((sid == null ? "" : Integer.toString(sid)) + ":" + (did == null ? "" : Integer.toString(did)));
		if(res == null){
			res = tcap_correlation_map.get(":" + (did == null ? "" : Integer.toString(did)));
		}
		if(res == null){
			res = tcap_correlation_map.get((sid == null ? "" : Integer.toString(sid)) + ":");
		}
		return res;
	}
	public static TCAPCorrelationPacket TCAPCorrelationGetPut(Integer sid, Integer did, TCAPCorrelationPacket packet){
		TCAPCorrelationPacket p = tcap_correlation_map.get((sid == null ? "" : Integer.toString(sid)) + ":" + (did == null ? "" : Integer.toString(did)));
		if(p == null){
			tcap_correlation_map.put((sid == null ? "" : Integer.toString(sid)) + ":" + (did == null ? "" : Integer.toString(did)), packet);
			p = packet;
		}
		return p;
	}

	public static void TCAPCorrelationRemove(Long sid, Long did){
		tcap_correlation_map.remove((sid == null ? "" : Long.toString(sid)) + ":" + (did == null ? "" : Long.toString(did)));
	}
	
	// MT TCAP only Correlation
	/*
	public static void TCAPOnlyCorrelationPut(int id, TCAPOnlyCorrelationPacket p){
		tcap_only_correlation_map.put(id, p);
	}
	public static TCAPOnlyCorrelationPacket TCAPOnlyCorrelationGet(int id){
		return tcap_only_correlation_map.get(id);
	}
	public static void TCAPOnlyCorrelationRemove(int id){
		tcap_only_correlation_map.remove(id);
	}
	*/
	// MT status Correlation
	/*
	public static void MTStatusCorrelationPut(String id, MTStatusCorrelationPacket p){
		mt_status_correlation_map.put(id, p);
	}
	public static MTStatusCorrelationPacket MTStatusCorrelationGet(String id){
		return mt_status_correlation_map.get(id);
	}
	public static void MTStatusCorrelationRemove(String id){
		mt_status_correlation_map.remove(id);
	}
	*/
	// SRI Correlation
	public static void SRICorrelationPut(long tcapId, HPLMNRCorrelationPacket sp){
		sp.ts = System.currentTimeMillis();
		sri_correlation_map.put(tcapId, sp);
	}
	public static HPLMNRCorrelationPacket SRICorrelationGet(long tcapId){
		return sri_correlation_map.get(tcapId);
	}
	public static void SRICorrelationRemove(long tcapId){
		sri_correlation_map.remove(tcapId);
	}
	// MT Correlation
	public static void MTCorrelationPut(long imsi, MTCorrelationPacket mp){
		mp.ts = System.currentTimeMillis();
		mt_correlation_map.put(imsi, mp);
	}
	public static MTCorrelationPacket MTCorrelationGet(long imsi){
		return mt_correlation_map.get(imsi);
	}
	public static void MTCorrelationRemove(long imsi){
		mt_correlation_map.remove(imsi);
	}
	
	
	public static void consume_IMSI_CORRELATION_ID(){
		NEXT_IMSI_CORRELATION_ID++;
		if(NEXT_IMSI_CORRELATION_ID > 999999999L) NEXT_IMSI_CORRELATION_ID = 100000000L;
	}
	
	
	
	
	public static void addConverter(String conn_id, RoutingConnectionType type){
		switch(type){
			case M3UA:
				M3UAConnection conn = M3UAConnManager.getConnection(conn_id);
				M3UAConverter cnv = null;
				if(conn != null){
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Initializing M3UA Connection converter: [" + conn_id + "]!");
					cnv = new M3UAConverter(conn);
					routing_conn_cnv_map.put(conn_id, cnv);
					conn.attachWorkerMethod(cnv);

				}else{
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Error while initializing M3UA Connection converter for connection: [" + conn_id + "]!");
				}
				break;
			case MTP3:
				// TODO
				break;
		}
		
	}
	
	public static void removeConverter(String conn_id){
		RoutingConnectionWorkerMethod rwm = routing_conn_cnv_map.get(conn_id);
		if(rwm != null){
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Detaching Routing Connection converter for connection: [" + conn_id + "]!");
			// remove worker method
			rwm.conn.detachWorkerMethod();
			// remove converter
			routing_conn_cnv_map.remove(conn_id);
		}else{
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Error while removing Routing Connection converter for connection: [" + conn_id + "]!");
			
		}
	}
	
	
	public static void init(){
		HPLMNRDecoderWorker decoder_w;
		//SRIDataWorker data_w;
		HPLMNREncoderWorker encoder_w;
		
		if(SGNConfigData.hplmnr_status){
			in_queue = new ConcurrentLinkedQueue<SSCTPDescriptor>();
			//out_queue = new ConcurrentLinkedQueue<SSCTPDescriptor>();
			sri_queue = new ConcurrentLinkedQueue<HPLMNRPacket>();
			encoder_workerLst = new ArrayList<HPLMNREncoderWorker>();
			//data_workerLst = new ArrayList<SRIDataWorker>();
			decoder_workerLst = new ArrayList<HPLMNRDecoderWorker>();
			sri_correlation_map = new ConcurrentHashMap<Long, HPLMNRCorrelationPacket>();
			mt_correlation_map = new ConcurrentHashMap<Long, MTCorrelationPacket>();
			//mt_status_correlation_map = new HashMap<String, MTStatusCorrelationPacket>();
			//tcap_only_correlation_map = new HashMap<Integer, TCAPOnlyCorrelationPacket>();
			tcap_correlation_map = new ConcurrentHashMap<String, TCAPCorrelationPacket>();
			routing_conn_cnv_map = new ConcurrentHashMap<String, RoutingConnectionWorkerMethod>();
			LoggingManager.info(logger, "Starting HPLMNR module...");
			LoggingManager.info(logger, "Initializing M3UA Connection converters...");
			
			// m3ua connection converters
			for(String s : M3UAConnManager.conn_map.keySet()) addConverter(s, RoutingConnectionType.M3UA);

			// mtp3 connection converters
			//TODO
			
			
			LoggingManager.info(logger,"Creating HPLMNR Decoder Workers...");
			decoder_w = new HPLMNRDecoderWorker(0);
			decoder_workerLst.add(decoder_w);
			
			LoggingManager.info(logger,"Creating HPLMNR Encoder Workers...");
			encoder_w = new HPLMNREncoderWorker(0);
			encoder_workerLst.add(encoder_w);

			LoggingManager.info(logger,"Creating HPLMNR Timeout monitor...");
			timeout_r = new Timeout();
			timeout_t = new Thread(timeout_r, "HPLMNR_TIMEOUT");
			timeout_t.start();
			
		}
		
	}
}
