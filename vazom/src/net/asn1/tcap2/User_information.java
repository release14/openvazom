package net.asn1.tcap2;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
public class User_information extends SEQUENCE{

	public EXTERNAL new_child(){
		return new EXTERNAL();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public EXTERNAL getChild(int index){
		return (EXTERNAL)of_children.get(index);
	}
	public User_information(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 30;
		of_children = new ArrayList<ASNType>();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
