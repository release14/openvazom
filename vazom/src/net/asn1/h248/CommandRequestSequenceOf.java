package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CommandRequestSequenceOf extends SEQUENCE{

	public CommandRequest new_child(){
		return new CommandRequest();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CommandRequest getChild(int index){
		return (CommandRequest)of_children.get(index);
	}
	public CommandRequestSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
