package net.sccp.parameters;

import net.sccp.parameters.error_cause.ErrorCauseType;

public class ErrorCause extends ParamBase {
	public ErrorCauseType errorCause;
	public ErrorCause(){
		super();
		type = ParamType.ERROR_CAUSE;
	}
	
	
	public void init(byte[] data) {
		errorCause = ErrorCauseType.get(data[0] & 0xFF);
	}

}
