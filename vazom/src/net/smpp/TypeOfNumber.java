package net.smpp;

import java.util.HashMap;


public enum TypeOfNumber {
	UNKNOWN(0),
	INTERNATIONAL(1),
	NATIONAL(2),
	NETWORK_SPECIFIC(3),
	SUBSCRIBER_NUMBER(4),
	ALPHANUMERIC(5),
	ABBREVIATED(6);
	
	private int id;
	private static final HashMap<Integer, TypeOfNumber> lookup = new HashMap<Integer, TypeOfNumber>();
	static{
		for(TypeOfNumber td : TypeOfNumber.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static TypeOfNumber get(int id){
		TypeOfNumber res = lookup.get(id);
		if(res == null) res = UNKNOWN;
		return res;
		
	}
	private TypeOfNumber(int _id){ id = _id; }

}
