package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Message extends SEQUENCE{

	public INTEGER get_version(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_version(){
		return new INTEGER();
	}

	public MIdType get_mId(){
		return (MIdType)elements.get(1).data;
	}
	public MIdType new_mId(){
		return new MIdType();
	}

	public MessageBodyType get_messageBody(){
		return (MessageBodyType)elements.get(2).data;
	}
	public MessageBodyType new_messageBody(){
		return new MessageBodyType();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_version(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mId(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_messageBody(); }});
	}
	public Message(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
