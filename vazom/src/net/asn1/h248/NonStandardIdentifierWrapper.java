package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NonStandardIdentifierWrapper extends SEQUENCE{

	public NonStandardIdentifier get_nonStandardIdentifier(){
		return (NonStandardIdentifier)elements.get(0).data;
	}
	public NonStandardIdentifier new_nonStandardIdentifier(){
		return new NonStandardIdentifier();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_nonStandardIdentifier(); }});
	}
	public NonStandardIdentifierWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
