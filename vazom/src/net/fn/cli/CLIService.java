package net.fn.cli;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import net.cli.AuthManager;
import net.cli.CliConnectionBase;

import org.apache.log4j.Logger;


public class CLIService {
	public static Logger logger=Logger.getLogger(CLIService.class);
	
	private static ServerSocket tcp;
	private static int listener_port;
	private static ConcurrentHashMap<String, CliConnectionBase> connection_lst;
	private static Listener_r listener_r;
	private static Thread listener_t;
	private static boolean stopping;
	
	
	private static class Listener_r implements Runnable{
		private Socket connection;
		private CLIConnection con;
		
		public void run() {
			while(!stopping){
				try{
					connection = tcp.accept();
					con = new CLIConnection(connection, connection_lst);
					connection_lst.put(connection.getInetAddress().toString() + ":" + connection.getPort(), con);
					
					//logger.info("NEW CONNECTION TO CLI! [ " + connection.getInetAddress().toString() + ":" + connection.getPort() + " ]");
					
				}catch(Exception e){
					logger.error("ERROR WHILE ACCEPTING NEW CONNECTION TO CLI! [ " + listener_port + " ]");
					e.printStackTrace();
					stopping = true;
				}
			}			
		}
		
		
	}
	
	
	
	private static void init_listener_thread(){
		listener_r = new Listener_r();
		listener_t = new Thread(listener_r, "CLI_LISTENER");
		listener_t.start();
		
	}
	
	private static void init_server(int port){
		try{
			tcp = new ServerSocket(port);
			listener_port = port;
			logger.info("CLI INITIALIZED ! [ " + port + " ]");
			init_listener_thread();
			
		}catch(Exception e){
			logger.error("ERROR WHILE INITIALIZING CLI! [ " + port + " ]");
			e.printStackTrace();
			
		}
		
		
	}
	
	public static void init(int _port){
		connection_lst = new ConcurrentHashMap<String, CliConnectionBase>();
		AuthManager.init("conf/cli.auth");
		init_server(_port);
		
		
		
	}
}
