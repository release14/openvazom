package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.CalledPA;
import net.sccp.parameters.CallingPA;
import net.sccp.parameters.Credit;
import net.sccp.parameters.Eoop;
import net.sccp.parameters.HopCounter;
import net.sccp.parameters.Importance;
import net.sccp.parameters.ParamType;
import net.sccp.parameters.ProtocolClass;
import net.sccp.parameters.Slr;

public class CR_ConnectionRequest extends MessageBase {
	public Slr sourceLocalReference;
	public ProtocolClass protocolClass;
	public CalledPA calledPartyAddress;
	// Optional
	public Credit credit;
	public CallingPA callingPartyAddress;
	public byte[] data;
	public HopCounter hopCounter;
	public Importance importance;
	public Eoop eoop;
	
	
	public CR_ConnectionRequest(){
		super();
		type = MessageType.CR_CONNECTION_REQUEST;
		pointers = new Pointer[2];
	}
	
	public void init(byte[] _data) {
		boolean opt_done = false;
		ParamType pt = null;
		int l = 0;
		byte_pos++;
		// Fixed mandatory
		sourceLocalReference = new Slr();
		sourceLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		protocolClass = new ProtocolClass();
		protocolClass.init(new byte[]{_data[byte_pos++]});
		
		// Two pointers, one variable length and optional part
		pointers[0] = new Pointer((_data[byte_pos] & 0xFF), (_data[byte_pos] & 0xFF) + (byte_pos++));
		pointers[1] = new Pointer((_data[byte_pos] & 0xFF), (_data[byte_pos] & 0xFF) + (byte_pos++));
		
		// Variable mandatory
		calledPartyAddress = new CalledPA();
		calledPartyAddress.init(Arrays.copyOfRange(_data, pointers[0].position + 1, pointers[0].position + 1 + (_data[pointers[0].position] & 0xFF)));
		
		// Optional part
		// if equals 0(zero) no optional parameters present
		if(pointers[1].value != 0x00){
			// position = first optional parameter
			byte_pos = pointers[1].position;
			while(!opt_done){
				pt = ParamType.get(_data[byte_pos++] & 0xFF);
				// End of optional parameters has no length, only one byte which is 0x00
				if(pt != ParamType.END_OF_OPTIONAL_PARAMETERS) l = _data[byte_pos++] & 0xFF;
				else l = 0;
				switch(pt){
					case CREDIT:
						credit = new Credit();
						credit.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case CALLING_PARTY_ADDRESS:
						callingPartyAddress = new CallingPA();
						callingPartyAddress.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case DATA:
						data = Arrays.copyOfRange(_data, byte_pos, l + byte_pos);
						break;
					case HOP_COUNTER:
						hopCounter = new HopCounter();
						hopCounter.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case IMPORTANCE:
						importance = new Importance();
						importance.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case END_OF_OPTIONAL_PARAMETERS:
						eoop = new Eoop();
						opt_done = true;
						break;
				}
				byte_pos += l;
			}
		}		
		
	}

}
