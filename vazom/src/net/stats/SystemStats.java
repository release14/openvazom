package net.stats;

public class SystemStats {
	public long STARTUP_TS;
	public long FGN_IN_QUEUE_MAX;
	public long FGN_OUT_QUEUE_MAX;
	public long SGN_IN_QUEUE_MAX;
	public long SGN_OUT_QUEUE_MAX;
	public long SGC_IN_QUEUE_MAX;
	public long SGC_OUT_QUEUE_MAX;

}
