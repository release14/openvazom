package net.mtp3.types;

import java.util.HashMap;

public enum UserPartIdentityType {
	SCCP(0x03),
	TUP(0x04),
	ISUP(0x05),
	DUP(0x06),
	MTP_TESTING(0x08),
	BROADBAND_ISDN_USER_PART(0x09),
	SATELLITE_ISDN_USER_PART(0x0a);
	
	private int id;
	private static final HashMap<Integer, UserPartIdentityType> lookup = new HashMap<Integer, UserPartIdentityType>();
	static{
		for(UserPartIdentityType td : UserPartIdentityType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static UserPartIdentityType get(int id){ return lookup.get(id); }
	private UserPartIdentityType(int _id){ id = _id; }	
}
