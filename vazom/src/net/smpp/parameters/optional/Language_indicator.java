package net.smpp.parameters.optional;

import java.util.HashMap;


public class Language_indicator extends OptionalParameterBase {
	public enum ValueType{
		UNSPECiFIED(0x00),
		ENGLISH(0x01),
		FRENCH(0x02),
		SPANISH(0x03),
		GERMAN(0x04),
		PORTUGUESE(0x05);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public ValueType value;
	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
