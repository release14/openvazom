package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TerminationID extends SEQUENCE{

	public WildcardFieldSequenceOf get_wildcard(){
		return (WildcardFieldSequenceOf)elements.get(0).data;
	}
	public WildcardFieldSequenceOf new_wildcard(){
		return new WildcardFieldSequenceOf();
	}

	public OCTET_STRING get_id(){
		return (OCTET_STRING)elements.get(1).data;
	}
	public OCTET_STRING new_id(){
		return new OCTET_STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_wildcard(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_id(); }});
	}
	public TerminationID(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
