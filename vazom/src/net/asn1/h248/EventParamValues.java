package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventParamValues extends SEQUENCE{

	public EventParamValue new_child(){
		return new EventParamValue();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public EventParamValue getChild(int index){
		return (EventParamValue)of_children.get(index);
	}
	public EventParamValues(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
