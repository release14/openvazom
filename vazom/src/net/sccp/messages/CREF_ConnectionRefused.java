package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.CalledPA;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.Eoop;
import net.sccp.parameters.Importance;
import net.sccp.parameters.ParamType;
import net.sccp.parameters.RefusalCause;


public class CREF_ConnectionRefused extends MessageBase {
	public Dlr destinationLocalReference;
	public RefusalCause refusalCause;
	// optional
	public CalledPA calledPartyAddress;
	public byte[] data;
	public Importance importance;
	public Eoop eoop;
	
	public CREF_ConnectionRefused(){
		super();
		type = MessageType.CREF_CONNECTION_REFUSED;
		pointers = new Pointer[1];
	}
	
	public void init(byte[] _data) {
		boolean opt_done = false;
		ParamType pt = null;
		int l = 0;

		byte_pos++;
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		refusalCause = new RefusalCause();
		refusalCause.init(new byte[]{_data[byte_pos++]});

		// One pointer, optional part
		pointers[0] = new Pointer((_data[byte_pos] & 0xFF), (_data[byte_pos] & 0xFF) + (byte_pos++));
		
		// Optional part
		// if equals 0(zero) no optional parameters present
		if(pointers[0].value != 0x00){
			// position = first optional parameter
			byte_pos = pointers[1].position;
			while(!opt_done){
				pt = ParamType.get(_data[byte_pos++] & 0xFF);
				// End of optional parameters has no length, only one byte which is 0x00
				if(pt != ParamType.END_OF_OPTIONAL_PARAMETERS) l = _data[byte_pos++] & 0xFF;
				else l = 0;
				switch(pt){
					case CALLED_PARTY_ADDRESS:
						calledPartyAddress = new CalledPA();
						calledPartyAddress.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case DATA:
						data = Arrays.copyOfRange(_data, byte_pos, l + byte_pos);
						break;
					case IMPORTANCE:
						importance = new Importance();
						importance.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case END_OF_OPTIONAL_PARAMETERS:
						eoop = new Eoop();
						opt_done = true;
						break;
				}
				byte_pos += l;
			}
		}
		
		
		
	}

}
