package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MonitoringMode extends ENUMERATED{

	public static final int _a_side = 0;
	public static final int _b_side = 1;
	public MonitoringMode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
