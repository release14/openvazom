package net.smpp.parameters;

import java.util.HashMap;

public enum IntermediateNotificationType {
	NO(0x00),
	YES(0x10);

	
	private int id;
	private static final HashMap<Integer, IntermediateNotificationType> lookup = new HashMap<Integer, IntermediateNotificationType>();
	static{
		for(IntermediateNotificationType td : IntermediateNotificationType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static IntermediateNotificationType get(int id){ return lookup.get(id); }
	private IntermediateNotificationType(int _id){ id = _id; }	
}
