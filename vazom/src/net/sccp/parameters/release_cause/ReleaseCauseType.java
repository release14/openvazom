package net.sccp.parameters.release_cause;

import java.util.HashMap;


public enum ReleaseCauseType {
	END_USER_ORIGINATED(0x00),
	END_USER_CONGESTION(0x01),
	END_USER_FAILURE(0x02),
	SCCP_USER_ORIGINATED(0x03),
	REMOTE_PROCEDUER_ERROR(0x04),
	INCONSISTENT_CONNECTION_DATA(0x05),
	ACCESS_FAILURE(0x06),
	ACCESS_CONGESTION(0x07),
	SUBSYSTEM_FAILUER(0x08),
	SUBSYSTEM_CONGESTION(0x09),
	MTP_FAILURE(0x0A),
	NETWORK_CONGESTION(0x0B),
	EXPIRATION_OF_RESET_TIMER(0x0C),
	EXPIRATION_OF_RECEIVE_INACTIVITY_TIMER(0x0D),
	RESERVED(0x0E),
	UNQUALIFIED(0x0F),
	SCCP_FAILURE(0x10);
	
	
	// 1 byte
	private int id;
	private static final HashMap<Integer, ReleaseCauseType> lookup = new HashMap<Integer, ReleaseCauseType>();
	static{
		for(ReleaseCauseType td : ReleaseCauseType.values()){
			lookup.put(td.getId(), td);
		}
	}
	private int getId(){ return id; }
	public static ReleaseCauseType get(int id){ return lookup.get(id); }
	private ReleaseCauseType(int _id){ id = _id; }
	
}
