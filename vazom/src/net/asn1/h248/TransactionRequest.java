package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionRequest extends SEQUENCE{

	public TransactionId get_transactionId(){
		return (TransactionId)elements.get(0).data;
	}
	public TransactionId new_transactionId(){
		return new TransactionId();
	}

	public ActionsSequenceOf get_actions(){
		return (ActionsSequenceOf)elements.get(1).data;
	}
	public ActionsSequenceOf new_actions(){
		return new ActionsSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_actions(); }});
	}
	public TransactionRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
