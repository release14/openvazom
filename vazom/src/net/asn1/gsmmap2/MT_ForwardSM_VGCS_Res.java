package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MT_ForwardSM_VGCS_Res extends SEQUENCE{

	public SignalInfo get_sm_RP_UI(){
		return (SignalInfo)elements.get(0).data;
	}
	public SignalInfo new_sm_RP_UI(){
		return new SignalInfo();
	}

	public DispatcherList get_dispatcherList(){
		return (DispatcherList)elements.get(1).data;
	}
	public DispatcherList new_dispatcherList(){
		return new DispatcherList();
	}

	public NULL get_ongoingCall(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_ongoingCall(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sm_RP_UI(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dispatcherList(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_ongoingCall(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public MT_ForwardSM_VGCS_Res(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
