package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class End extends SEQUENCE{

	public DestTransactionID get_dtid(){
		return (DestTransactionID)elements.get(0).data;
	}
	public DestTransactionID new_dtid(){
		return new DestTransactionID();
	}

	public DialoguePortion get_dialoguePortion(){
		return (DialoguePortion)elements.get(1).data;
	}
	public DialoguePortion new_dialoguePortion(){
		return new DialoguePortion();
	}

	public ComponentPortion get_components(){
		return (ComponentPortion)elements.get(2).data;
	}
	public ComponentPortion new_components(){
		return new ComponentPortion();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_dtid(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.APPLICATION, true, false){public void set(){ data = new_dialoguePortion(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.APPLICATION, true, false){public void set(){ data = new_components(); }});
	}
	public End(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
