package net.smpp.pdu;

import java.util.ArrayList;

import net.smpp.ErrorCodeType;
import net.smpp.PDUType;
import net.utils.Utils;

public class Unbind extends PDUBase {

	public Unbind(){
		type = PDUType.UNBIND;
		command_status = ErrorCodeType.ESME_ROK;
	}

	public byte[] encode(){
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		//System.out.println(buff.size());
		return Utils.list2array(buff);
	}
	
	
	public void init(byte[] data) {
		// no data
	}

}
