package net.isup.messages;

import net.isup.MessageType;

public class LoopBackAck extends MessageBase {
	public LoopBackAck(){
		super();
		type = MessageType.LOOP_BACK_ACKNOWLEDGEMENT;
	}
	public void init(byte[] data) {
		// no data
	}

}
