package net.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ConfigManagerV2 {

	private static File props_file;
	private static ArrayList<ConfigItem> map;
	private static FileInputStream props_in;
	private static FileOutputStream props_out;
	private static BufferedReader props_reader;
	private static PrintWriter props_writer;
	
	public static synchronized String get(String key){
		for(int i = 0; i<map.size(); i++) if(map.get(i).name != null) if(map.get(i).name.equalsIgnoreCase(key)) return map.get(i).value;
		return null;
		
	}
	public static synchronized void add_after(String key, String value, String comment, String after_item){
		int item_index = -1;
		for(int i = 0; i<map.size(); i++){
			if(map.get(i).name != null){
				if(map.get(i).name.equalsIgnoreCase(after_item)){
					item_index = i;
					break;
				}
			}
		}
		
		ConfigItem ci = new ConfigItem();
		ci.name = key;
		ci.value = value;
		ci.comment = comment;
		if(item_index > -1) map.add(item_index + 1, ci); else map.add(ci);
		save();
	
	}
	
	public static synchronized void set(String key, String value, String comment){
		boolean done = false;
		for(int i = 0; i<map.size(); i++) if(map.get(i).name != null) if(map.get(i).name.equalsIgnoreCase(key)){
			map.get(i).value = value;
			if(comment != null) map.get(i).comment = comment;
			done = true;
			break;
		}
		if(!done){
			ConfigItem ci = new ConfigItem();
			ci.name = key;
			ci.value = value;
			ci.comment = comment;
			map.add(ci);
		}
		save();
		
	}
	private static void save(){
		ConfigItem ci = null;
		try{
			props_out = new FileOutputStream(props_file);
			props_writer = new PrintWriter(props_out);
			for(int i = 0; i<map.size(); i++){
				ci = map.get(i);
				if(ci.isComment) props_writer.println(ci.comment);
				else{
					if(ci.comment != null) props_writer.println("#" + ci.comment);
					if(ci.name != null) props_writer.println(ci.name + " = " + ci.value);
				}
				props_writer.flush();
			}
			props_out.close();
			props_writer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	public static void getList(String fname, List<String> lst){
		if(fname != null){
			File f = new File(fname);
			String line;
			if(f.exists()){
				try{
					lst.clear();
					BufferedReader br = new BufferedReader(new FileReader(f));
					while((line = br.readLine()) != null) lst.add(line);
					br.close();
				}catch(Exception e ){
					//e.printStackTrace();
				}
			}
		}
	}
	public static void init(String fname){
		props_file = new File(fname);
		map = new ArrayList<ConfigItem>();
		String line;
		String[] tmp;
		ConfigItem ci = null;
		try{
			if(props_file.exists()){
				props_in = new FileInputStream(props_file);
				props_reader = new BufferedReader(new InputStreamReader(props_in));
				while((line = props_reader.readLine()) != null){
					ci = new ConfigItem();
					// comment
					if(line.startsWith("#")){
						ci.isComment = true;
						ci.comment = line;
					}else if(line.contains("=")){
						tmp = line.split("=");
						ci.name = tmp[0].trim();
						ci.value = (tmp.length > 1 ?tmp[1].trim() : "");
						//System.out.println(tmp[0].trim() + "=" + tmp[1].trim());
						
					}
					map.add(ci);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
}
