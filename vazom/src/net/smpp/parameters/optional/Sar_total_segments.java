package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Sar_total_segments extends OptionalParameterBase {

	public Sar_total_segments(){
		type = OptionalParameterType.SAR_TOTAL_SEGMENTS;
	}
	public int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
