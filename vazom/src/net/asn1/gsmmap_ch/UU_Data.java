package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UU_Data extends SEQUENCE{

	public UUIndicator get_uuIndicator(){
		return (UUIndicator)elements.get(0).data;
	}
	public UUIndicator new_uuIndicator(){
		return new UUIndicator();
	}

	public UUI get_uui(){
		return (UUI)elements.get(1).data;
	}
	public UUI new_uui(){
		return new UUI();
	}

	public NULL get_uusCFInteraction(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_uusCFInteraction(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uuIndicator(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uui(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uusCFInteraction(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public UU_Data(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
