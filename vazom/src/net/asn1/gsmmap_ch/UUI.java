package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class UUI extends OCTET_STRING{

	public UUI(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
