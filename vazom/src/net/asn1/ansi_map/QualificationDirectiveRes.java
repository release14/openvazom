package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class QualificationDirectiveRes extends SET{

	public OCTET_STRING get_zeroOctets(){
		return (OCTET_STRING)elements.get(0).data;
	}
	public OCTET_STRING new_zeroOctets(){
		return new OCTET_STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_zeroOctets(); }});
	}
	public QualificationDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
