package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SSDNotShared extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _discard_SSD =  1 ;
	public SSDNotShared(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
