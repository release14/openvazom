package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.CauseIndicators;

public class Confusion extends MessageBase {
	public CauseIndicators cause_indicators;
	
	public Confusion(){
		super();
		type = MessageType.CONFUSION;
		pointers = new Pointer[2];
	}

	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			pointers[1] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
	
			cause_indicators = new CauseIndicators();
			cause_indicators.init(getPointerData(data, 0));
			
			// optional
			processOptional(data, 1);
			
		}
	}

}
