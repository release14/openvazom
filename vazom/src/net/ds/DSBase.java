package net.ds;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

public abstract class DSBase {
	protected static Logger logger=Logger.getLogger(DSBase.class);
	public DataSourceType type;
	public ConcurrentLinkedQueue<DSPacket> queue;
	protected Reader reader_r;
	protected Thread reader_t;
	protected boolean stopping;
	protected Object[] reader_method_params;
	public DSDescriptor dsd;
	public long QUEUE_FULL;
	
	private class Reader implements Runnable{
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				reader_method(reader_method_params);
			}
			logger.info("Ending...");
			
		}
	}	
	protected abstract void reader_method(Object[] params);
	public boolean isReady(){ return true; }
	public DSBase(DSDescriptor _dsd){
		queue = new ConcurrentLinkedQueue<DSPacket>();
		reader_r = new Reader();
		reader_t = new Thread(reader_r, "DS_READER");
		dsd = _dsd;
		
	}
	
	
}
