#include <jni.h>
#include <iostream>
#include <net_gpu_cuda_CudaManager.h>
#include <jcallback.h>
using namespace std;

JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_init(JNIEnv *env, jclass c,
														jint _max_rep_lst_size,
														jint _max_spam_lst_size,
														jint _max_md5_quarantine_lst_size,
														jint _max_md5_normal_lst_size,
														jint _max_i_size,
														jint _max_i_variation,
														jint _max_q_size){
	CUDAM::init(_max_rep_lst_size,
				_max_spam_lst_size,
				_max_md5_quarantine_lst_size,
				_max_md5_normal_lst_size,
				_max_i_size,
				_max_i_variation,
				_max_q_size);
	// save gloval JVM reference
	env->GetJavaVM(&_jvm);

}

JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_initGrid(JNIEnv *env, jclass c, jint lst_id, jint blocks_w, jint blocks_h, jint threads){
	CUDAM::initGrid(lst_id, blocks_w, blocks_h, threads);
}

JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_initDevice(JNIEnv *env, jclass c, jint device){
	CUDAM::init_device(device);
}
JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_initAllDevices(JNIEnv *env, jclass c){
	CUDAM::initAllDevices();
}
JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_shutdown(JNIEnv *env, jclass c){
	CUDAM::shutdown();
}
JNIEXPORT jint JNICALL Java_net_gpu_cuda_CudaManager_getDeviceCount(JNIEnv *env, jclass c){
	return CUDAM::getDeviceCount();
}
JNIEXPORT jint JNICALL Java_net_gpu_cuda_CudaManager_getFreeDevice(JNIEnv *env, jclass c){
	return CUDAM::getFreeDevice();
}
JNIEXPORT jobject JNICALL Java_net_gpu_cuda_CudaManager_getStats(JNIEnv *env, jclass cls, jint device){
	KernelStats *kstats = new KernelStats;
	jmethodID constructor;
	jclass c;
	jobject obj = NULL;
	jvalue args[6];

	// init values
	kstats->execution_count = 0;
	kstats->last_execution_time = 0;
	kstats->max_execution_time = 0;
	kstats->update_q_size = 0;
	kstats->ld_q_size = 0;
	kstats->md5_q_size = 0;

	CUDAM::getKernelStats(device, kstats);
	c = env->FindClass("net/gpu/cuda/KernelStats");
	if(c != NULL){
		constructor = env->GetMethodID(c, "<init>", "(FFJJJJ)V");
		if(constructor != NULL){
			args[0].f = kstats->last_execution_time;
			args[1].f = kstats->max_execution_time;
			args[2].j = kstats->execution_count;
			args[3].j = kstats->update_q_size;
			args[4].j = kstats->ld_q_size;
			args[5].j = kstats->md5_q_size;
			obj = env->NewObjectA(c, constructor, args);
		}
	}
	delete kstats;
	return obj;


}
JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_reset(JNIEnv *env, jclass c, jint device, jint lst_type){
	DataPacket *dp = NULL;
	dp = new DataPacket;
	dp->action = ACT_RESET;
	dp->lst_type = lst_type;
	dp->data = NULL;
	// push to queue
	CUDAM::push_update_host(device, dp);
	// DataPacket memory will be unallocated by CUDAM

}


JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_pushToMD5Queue(JNIEnv *env, jclass c, jint device, jint lst_type, jbyteArray hash, jobject j_callback){
	int l = env->GetArrayLength(hash);
	MD5Packet *md5p = NULL;
	jcallback_args *tc = NULL;
	if(l == 16 && (lst_type == MD5_NORMAL_LST || lst_type == MD5_QUARANTINE_LST)){
		// cretate MD5Packet for CUDAM
		md5p = new MD5Packet;
		md5p->action = ACT_COMPARE;
		md5p->lst_type = lst_type;
		md5p->hash = new char[16];
		md5p->result = new char[(lst_type == MD5_NORMAL_LST ? CUDAM::max_md5_normal_lst_size : CUDAM::max_md5_quarantine_lst_size)];
		md5p->callback_args = NULL;
		md5p->callback_method = NULL;
		if(j_callback != NULL){
			// fill callback sruct
			tc = new jcallback_args;
			// save JVM reference
			//env->GetJavaVM(&tc->vm);
			tc->vm = _jvm;
			// set callback type
			tc->cb_type = CB_MD5;
			// set callback function pointer
			// create global reference(used later)
			tc->obj = env->NewGlobalRef(j_callback);
			// save LDPacket reference
			tc->md5p = md5p;
			// set LDPacket
			md5p->callback_method = &java_callback_method;
			md5p->callback_args = tc;
		}
		// copy to buffer
		env->GetByteArrayRegion(hash, 0, l, (jbyte *)md5p->hash);
		// push to queue
		CUDAM::push_md5_host(device, md5p);
		// MD5Packet memory will be unallocated by CUDAM
	}else cout << "JNI pushToMD5Queue error: device [" << device << "], lst id [" <<  lst_type << "]!" << endl;
}


JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_pushToMD5UpdateQueue(JNIEnv *env, jclass c, jint device, jint lst_type, jbyteArray hash){
	int l = env->GetArrayLength(hash);
	DataPacket *dp = NULL;
	if(l == 16){
		dp = new DataPacket;
		dp->action = ACT_NEW;
		dp->length = l;
		dp->data = new char[l];
		dp->lst_type = lst_type;
		// copy to buffer
		env->GetByteArrayRegion(hash, 0, l, (jbyte *)dp->data);
		// push to queue
		CUDAM::push_update_host(device, dp);
		// DataPacket memory will be unallocated by CUDAM
	}else cout << "JNI pushToMD5UpdateQueue [" << device << "]: hash length mismatch!" << endl;
}

JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_remove(JNIEnv *env, jclass c, jint device, jint lst_type, jint item_id, jbyteArray msg_bytes){
	DataPacket *dp = NULL;
	int l;// = env->GetArrayLength(msg_bytes);
	if(lst_type != LD_REPETITION){
		dp = new DataPacket;
		dp->action = ACT_DELETE;
		dp->action_param = item_id;
		dp->lst_type = lst_type;
		if(msg_bytes != NULL){
			l = env->GetArrayLength(msg_bytes);
			dp->data = new char[l];
			dp->length = l;
			// init buffer to zero
			for(int i = 0; i<l; i++) dp->data[i] = 0;
			// copy to buffer
			env->GetByteArrayRegion(msg_bytes, 0, l, (jbyte *)dp->data);

		}
		CUDAM::push_update_host(device, dp);
	}else cout << "JNI Java_net_gpu_cuda_CudaManager_remove: ACT_DELETE not supported on lst id [" << lst_type << "]!" << endl;


}

JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_enqueueRemove(JNIEnv *env, jclass c, jint device, jint lst_type, jint item_id){
	MD5Packet *md5p = NULL;
	DataPacket *dp = NULL;
	// MD5 list
	/*
	if(lst_type == MD5_QUARANTINE_LST || lst_type == MD5_NORMAL_LST){
		md5p = new MD5Packet;
		md5p->action = ACT_DELETE;
		md5p->action_param = item_id;
		md5p->hash = NULL;
		md5p->lst_type = lst_type;
		md5p->callback_args = NULL;
		md5p->callback_method = NULL;
		CUDAM::push_md5_host(device, md5p);
	// SPAM lst
	}else
	*/
	if(lst_type != LD_REPETITION){
		dp = new DataPacket;
		dp->action = ACT_DELETE;
		dp->action_param = item_id;
		dp->lst_type = lst_type;
		dp->data = NULL;
		CUDAM::push_update_host(device, dp);
	}else cout << "JNI enqueueRemove: ACT_DELETE not supported on lst id [" << lst_type << "]!" << endl;

}



JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_pushToUPDATEQueue(JNIEnv *env, jclass c, jint device, jint lst_type, jbyteArray msg_bytes){
	int l = env->GetArrayLength(msg_bytes);
	DataPacket *dp = NULL;
	if(l <= CUDAM::max_i_size){
		// create DataPacket for CUDAM
		dp = new DataPacket;
		dp->data = new char[l];
		dp->length = l;
		dp->action = ACT_NEW;
		dp->lst_type = lst_type;
		// init buffer to zero
		for(int i = 0; i<l; i++) dp->data[i] = 0;
		// copy to buffer
		env->GetByteArrayRegion(msg_bytes, 0, l, (jbyte *)dp->data);
		// push to queue
		CUDAM::push_update_host(device, dp);
		// DataPacket memory will be unallocated by CUDAM
	}else cout << "JNI pushToUPDATEQueue [" << device << "]: max message length [" << CUDAM::max_i_size << "] exceeded [" << l << "]" << endl;
}


JNIEXPORT void JNICALL Java_net_gpu_cuda_CudaManager_pushToLDQueue(JNIEnv *env, jclass c, jint device, jint lst_type, jbyteArray msg_bytes, jobject j_callback){
	jcallback_args *tc = NULL;
	LDPacket *ldp = NULL;
	int max_size = -1;
	int l = env->GetArrayLength(msg_bytes);
	if(l <= CUDAM::max_i_size){
		switch(lst_type){
			case LD_REPETITION: max_size = CUDAM::max_rep_lst_size; break;
			case LD_SPAM: max_size =  CUDAM::max_spam_lst_size; break;
		}
		if(max_size != -1){
			// init memory
			ldp = new LDPacket;
			ldp->data = new char[l];
			ldp->result = new char[max_size];
			ldp->length = l;
			ldp->lst_type = lst_type;
			ldp->callback_args = NULL;
			ldp->callback_method = NULL;
			// check for callback
			if(j_callback != NULL){
				// fill callback sruct
				tc = new jcallback_args;
				// save JVM reference
				//env->GetJavaVM(&tc->vm);
				tc->vm = _jvm;
				// set callback type
				tc->cb_type = CB_LD;
				// set callback function pointer
				// create global reference(used later)
				tc->obj = env->NewGlobalRef(j_callback);
				// save LDPacket reference
				tc->ldp = ldp;
				// set LDPacket
				ldp->callback_method = &java_callback_method;
				ldp->callback_args = tc;
			}
			// set buffer
			env->GetByteArrayRegion(msg_bytes, 0, l, (jbyte *)ldp->data);
			// send to queue
			CUDAM::push_ld_host(device, ldp);
		}else cout << "JNI pushToLDQueue [" << device << "]: unknown lst id: [" << lst_type << "]!" << endl;
	}else cout << "JNI pushToLDQueue [" << device << "]: max message length [" << CUDAM::max_i_size << "] exceeded [" << l << "]" << endl;

}




