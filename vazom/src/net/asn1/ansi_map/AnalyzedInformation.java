package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AnalyzedInformation extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public Digits get_digits(){
		return (Digits)elements.get(1).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(3).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public TriggerType get_triggerType(){
		return (TriggerType)elements.get(4).data;
	}
	public TriggerType new_triggerType(){
		return new TriggerType();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(5).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public ACGEncountered get_acgencountered(){
		return (ACGEncountered)elements.get(6).data;
	}
	public ACGEncountered new_acgencountered(){
		return new ACGEncountered();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(7).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(8).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(9).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(10).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(11).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public ConferenceCallingIndicator get_conferenceCallingIndicator(){
		return (ConferenceCallingIndicator)elements.get(12).data;
	}
	public ConferenceCallingIndicator new_conferenceCallingIndicator(){
		return new ConferenceCallingIndicator();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(13).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public DMH_BillingIndicator get_dmd_BillingIndicator(){
		return (DMH_BillingIndicator)elements.get(14).data;
	}
	public DMH_BillingIndicator new_dmd_BillingIndicator(){
		return new DMH_BillingIndicator();
	}

	public DMH_ChargeInformation get_dmh_ChargeInformation(){
		return (DMH_ChargeInformation)elements.get(15).data;
	}
	public DMH_ChargeInformation new_dmh_ChargeInformation(){
		return new DMH_ChargeInformation();
	}

	public DMH_RedirectionIndicator get_dmh_RedirectionIndicator(){
		return (DMH_RedirectionIndicator)elements.get(16).data;
	}
	public DMH_RedirectionIndicator new_dmh_RedirectionIndicator(){
		return new DMH_RedirectionIndicator();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(17).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public FeatureIndicator get_featureIndicator(){
		return (FeatureIndicator)elements.get(18).data;
	}
	public FeatureIndicator new_featureIndicator(){
		return new FeatureIndicator();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(19).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(20).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(21).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(22).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(23).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(24).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(25).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(26).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingPartyName get_redirectingPartyName(){
		return (RedirectingPartyName)elements.get(27).data;
	}
	public RedirectingPartyName new_redirectingPartyName(){
		return new RedirectingPartyName();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(28).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(29).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(30).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(31).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public TerminationAccessType get_terminationAccessType(){
		return (TerminationAccessType)elements.get(32).data;
	}
	public TerminationAccessType new_terminationAccessType(){
		return new TerminationAccessType();
	}

	public TimeDateOffset get_timeDateOffset(){
		return (TimeDateOffset)elements.get(33).data;
	}
	public TimeDateOffset new_timeDateOffset(){
		return new TimeDateOffset();
	}

	public TimeOfDay get_timeOfDay(){
		return (TimeOfDay)elements.get(34).data;
	}
	public TimeOfDay new_timeOfDay(){
		return new TimeOfDay();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(279, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_triggerType(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(340, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_acgencountered(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(137, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_conferenceCallingIndicator(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(312, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmd_BillingIndicator(); }});
		elements.add(new ElementDescriptor(311, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_ChargeInformation(); }});
		elements.add(new ElementDescriptor(88, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_RedirectionIndicator(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(306, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_featureIndicator(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(245, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingPartyName(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(119, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationAccessType(); }});
		elements.add(new ElementDescriptor(275, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeDateOffset(); }});
		elements.add(new ElementDescriptor(309, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeOfDay(); }});
	}
	public AnalyzedInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
