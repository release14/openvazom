package net.sccp.parameters;

import net.sccp.parameters.reset_cause.ResetCauseType;

public class ResetCause extends ParamBase {
	public ResetCauseType resetCause;
	
	public ResetCause(){
		super();
		type = ParamType.RESET_CAUSE;
	}
	
	public void init(byte[] data) {
		resetCause = ResetCauseType.get(data[0] & 0xFF);

	}

}
