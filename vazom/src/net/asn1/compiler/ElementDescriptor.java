package net.asn1.compiler;

public class ElementDescriptor {
	public int tagValue;
	public ASNTagClass tagClass;
	public boolean optional;
	public boolean extAddition;
	public ASNType data;
	
	public void set(){}
	
	
	public ElementDescriptor(int _tv, ASNTagClass _tc, boolean _o, boolean _ea){
		tagValue = _tv;
		tagClass = _tc;
		optional = _o;
		extAddition = _ea;
	}
	
}
