package net.ds;

import java.util.ArrayList;

public class DSPacket {
	// IP header
	public String ip_source;
	public String ip_destination;
	// TCP header
	public int tcp_source;
	public int tcp_destination;
	
	public DataSourceType dataSource;
	public byte[] data;
	public ArrayList<Object> extra_data;
	
	public DSPacket(DataSourceType ds, byte[] _data){
		dataSource = ds;
		data = _data;
	}

}
