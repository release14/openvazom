package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReportType extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _unspecified_security_violation =  1 ;
	public static final int _mSID_ESN_mismatch =  2 ;
	public static final int _rANDC_mismatch =  3 ;
	public static final int _reserved =  4 ;
	public static final int _sSD_update_failed =  5 ;
	public static final int _reserved2 =  6 ;
	public static final int _cOUNT_mismatch =  7 ;
	public static final int _reserved3 =  8 ;
	public static final int _unique_Challenge_failed =  9 ;
	public static final int _unsolicited_Base_Station_Challenge =  10 ;
	public static final int _sSD_Update_no_response =  11 ;
	public static final int _cOUNT_Update_no_response =  12 ;
	public static final int _unique_Challenge_no_response =  13 ;
	public static final int _aUTHR_mismatch =  14 ;
	public static final int _tERMTYP_mismatch =  15 ;
	public static final int _missing_authentication_parameters =  16 ;
	public ReportType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
