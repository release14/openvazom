package net.smsfs.list.lists;

import net.smsfs.list.ListBase;
import net.smsfs.list.ListType;

public class SmsfsList extends ListBase {

	public SmsfsList(String id, int max_size) {
		super(false, max_size);
		list_type = ListType.SMSFS_LIST;
		list_type_id = id;
	}

}
