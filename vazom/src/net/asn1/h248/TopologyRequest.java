package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TopologyRequest extends SEQUENCE{

	public TerminationID get_terminationFrom(){
		return (TerminationID)elements.get(0).data;
	}
	public TerminationID new_terminationFrom(){
		return new TerminationID();
	}

	public TerminationID get_terminationTo(){
		return (TerminationID)elements.get(1).data;
	}
	public TerminationID new_terminationTo(){
		return new TerminationID();
	}

	public TopologyDirectionEnum get_topologyDirection(){
		return (TopologyDirectionEnum)elements.get(2).data;
	}
	public TopologyDirectionEnum new_topologyDirection(){
		return new TopologyDirectionEnum();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(3).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public TopologyDirectionExtensionEnum get_topologyDirectionExtension(){
		return (TopologyDirectionExtensionEnum)elements.get(4).data;
	}
	public TopologyDirectionExtensionEnum new_topologyDirectionExtension(){
		return new TopologyDirectionExtensionEnum();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationFrom(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationTo(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_topologyDirection(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_topologyDirectionExtension(); }});
	}
	public TopologyRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
