package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARQ_apdu extends SEQUENCE{

	public BIT_STRING get_protocol_version(){
		return (BIT_STRING)elements.get(0).data;
	}
	public BIT_STRING new_protocol_version(){
		return new BIT_STRING();
	}

	public AARQ_apdu_application_context_name get_application_context_name(){
		return (AARQ_apdu_application_context_name)elements.get(1).data;
	}
	public AARQ_apdu_application_context_name new_application_context_name(){
		return new AARQ_apdu_application_context_name();
	}

	public User_information get_user_information(){
		return (User_information)elements.get(2).data;
	}
	public User_information new_user_information(){
		return new User_information();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_protocol_version(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_application_context_name(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_user_information(); }});
	}
	public AARQ_apdu(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 0;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.APPLICATION;
	}
}
