package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SecurityHeader extends SEQUENCE{

	public SecurityParametersIndex get_securityParametersIndex(){
		return (SecurityParametersIndex)elements.get(0).data;
	}
	public SecurityParametersIndex new_securityParametersIndex(){
		return new SecurityParametersIndex();
	}

	public OriginalComponentIdentifier get_originalComponentIdentifier(){
		return (OriginalComponentIdentifier)elements.get(1).data;
	}
	public OriginalComponentIdentifier new_originalComponentIdentifier(){
		return new OriginalComponentIdentifier();
	}

	public InitialisationVector get_initialisationVector(){
		return (InitialisationVector)elements.get(2).data;
	}
	public InitialisationVector new_initialisationVector(){
		return new InitialisationVector();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_securityParametersIndex(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_originalComponentIdentifier(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_initialisationVector(); }});
	}
	public SecurityHeader(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
