package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReturnResultProblem extends INTEGER{

	public static final int _unrecognizedInvokeID = 0;
	public static final int _returnResultUnexpected = 1;
	public static final int _mistypedParameter = 2;
	public ReturnResultProblem(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
