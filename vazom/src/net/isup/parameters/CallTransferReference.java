package net.isup.parameters;

import net.isup.ParameterType;

public class CallTransferReference extends ParameterBase {
	public CallTransferReference(){
		type = ParameterType.CALL_TRANSFER_REFERENCE;
	}
}
