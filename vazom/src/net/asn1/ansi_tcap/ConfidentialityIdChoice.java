package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ConfidentialityIdChoice extends CHOICE{

	public INTEGER get_integerConfidentialityId(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_integerConfidentialityId(){
		return new INTEGER();
	}

	public OBJECT_IDENTIFIER get_objectConfidentialityId(){
		return (OBJECT_IDENTIFIER)elements.get(1).data;
	}
	public OBJECT_IDENTIFIER new_objectConfidentialityId(){
		return new OBJECT_IDENTIFIER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_integerConfidentialityId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_objectConfidentialityId(); }});
	}
	public ConfidentialityIdChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
