package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSNotificationRes extends SET{

	public SMS_MessageCount get_sms_MessageCount(){
		return (SMS_MessageCount)elements.get(0).data;
	}
	public SMS_MessageCount new_sms_MessageCount(){
		return new SMS_MessageCount();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(108, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_MessageCount(); }});
	}
	public SMSNotificationRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
