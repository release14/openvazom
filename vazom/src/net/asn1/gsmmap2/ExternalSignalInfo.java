package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ExternalSignalInfo extends SEQUENCE{

	public ProtocolId get_protocolId(){
		return (ProtocolId)elements.get(0).data;
	}
	public ProtocolId new_protocolId(){
		return new ProtocolId();
	}

	public SignalInfo get_signalInfo(){
		return (SignalInfo)elements.get(1).data;
	}
	public SignalInfo new_signalInfo(){
		return new SignalInfo();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_protocolId(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_signalInfo(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public ExternalSignalInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
