package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MediaDescriptorStreamsChoiceWrapper extends SEQUENCE{

	public MediaDescriptorStreamsChoice get_mediaDescriptorStreamsChoice(){
		return (MediaDescriptorStreamsChoice)elements.get(0).data;
	}
	public MediaDescriptorStreamsChoice new_mediaDescriptorStreamsChoice(){
		return new MediaDescriptorStreamsChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_mediaDescriptorStreamsChoice(); }});
	}
	public MediaDescriptorStreamsChoiceWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
