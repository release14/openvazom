package net.spcap.vlan;

import net.spcap.EtherType;

public class VLANHeader {
	public EtherType ether_type;
	public VLANPriority priority;
	public boolean cfi_indicator;
	public int vlan_identifier;
	public byte[] payload;
}
