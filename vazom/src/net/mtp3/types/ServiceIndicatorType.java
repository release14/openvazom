package net.mtp3.types;

import java.util.HashMap;

// bits ....xxxx (mask 0x0f)
public enum ServiceIndicatorType {
	SIGNALLING_NETWORK_MANAGEMENT_MESSAGES(0x00),
	SIGNALLING_NETWORK_TESTING_AND_MAINTENANCE_MESSAGES(0x01),
	SCCP(0x03),
	TELEPHONE_USER_PART(0x04),
	ISDN_USER_PART(0x05),
	DATA_USER_PART_1(0x06),
	DATA_USER_PART_2(0x07),
	RESERVED_FOR_MTP_TESTING_USER_PART(0x08),
	BROADBAND_ISDN_USER_PART(0x09),
	SATELLITE_ISDN_USER_PART(0x0a);
	
	
	
	private int id;
	private static final HashMap<Integer, ServiceIndicatorType> lookup = new HashMap<Integer, ServiceIndicatorType>();
	static{
		for(ServiceIndicatorType td : ServiceIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ServiceIndicatorType get(int id){ return lookup.get(id); }
	private ServiceIndicatorType(int _id){ id = _id; }	
}
