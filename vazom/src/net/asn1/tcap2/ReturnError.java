package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReturnError extends SEQUENCE{

	public InvokeIdType get_invokeID(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_invokeID(){
		return new InvokeIdType();
	}

	public ErrorCode get_errorCode(){
		return (ErrorCode)elements.get(1).data;
	}
	public ErrorCode new_errorCode(){
		return new ErrorCode();
	}

	public Parameter get_parameter(){
		return (Parameter)elements.get(2).data;
	}
	public Parameter new_parameter(){
		return new Parameter();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeID(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_errorCode(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_parameter(); }});
	}
	public ReturnError(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
