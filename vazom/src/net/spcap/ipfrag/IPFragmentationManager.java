package net.spcap.ipfrag;

import java.util.concurrent.ConcurrentHashMap;
import net.spcap.IPHeader;
import net.stats.StatsManager;

import org.apache.log4j.Logger;

public class IPFragmentationManager {
	private static Logger logger=Logger.getLogger(IPFragmentationManager.class);
	private static ConcurrentHashMap<String, IPFragPacket> packet_lst;
	private static int ip_timeout;
	private static Timeout timeout_r;
	private static Thread timeout_t;
	private static boolean stopping;
	private static class Timeout implements Runnable{

		public void run() {
			IPFragPacket ipf = null;
			logger.info("Starting...");
			while(!stopping){
				try{ Thread.sleep(ip_timeout); }catch(Exception e){ e.printStackTrace(); }
				for(String id : packet_lst.keySet()){
					ipf = packet_lst.get(id);
					if(System.currentTimeMillis() - ipf.ts > ip_timeout){
						packet_lst.remove(id);
						StatsManager.IP_FRAG_STATS.IP_FRAG_MISSING++;
					}
				}
				
			}
			logger.info("Ending...");
			
		}
		
	}
	
	
	
	
	public static void init(int _timeout){
		logger.info("Starting IP Fragmentation Manager...");
		packet_lst = new ConcurrentHashMap<String, IPFragPacket>();
		ip_timeout = _timeout;
		
		timeout_r = new Timeout();
		timeout_t = new Thread(timeout_r, "IP_FRAG_TIMEOUT");
		timeout_t.start();
	}
	
	private static IPHeader reassemble(String id){
		IPFragPacket ipf = null;
		int ttl = 0;
		int fc = 0;
		try{
			ipf = packet_lst.get(id);
			IPHeader iph = null;
			byte[] buff = null;
			int tmp_l;
			if(ipf != null){
				ttl = ipf.get_total_length();
				if(ttl > 0){
					buff = new byte[ttl];
					for(Integer fid : ipf.ip_fragment_map.keySet()){
						iph = ipf.ip_fragment_map.get(fid);
						tmp_l = iph.totalLength - iph.headerLength;
						for(int j = iph.fragment_offset; j<tmp_l + iph.fragment_offset; j++){
							fc++;
							buff[j] = iph.payload[j - iph.fragment_offset];
						}
						
					}
					// remove from list
					packet_lst.remove(id);
					// result
					// check if full
					if(fc == buff.length){
						iph.payload = buff;
						iph.fragment_offset = 0;
						iph.identification = 0;
						iph.more_fragments = false;
						iph.payload = buff;
						//logger.debug("Reassembled Fragmented packet, size: " + buff.length + ", fragments: " + ipf.ip_fragment_lst.size());
						StatsManager.IP_FRAG_STATS.IP_FRAG_ASSEMBLED++;
						StatsManager.IP_FRAG_STATS.setMaxFragCount(ipf.ip_fragment_map.size());
						StatsManager.IP_FRAG_STATS.setMinFragCount(ipf.ip_fragment_map.size());
						return iph;
						
					}else{
						StatsManager.IP_FRAG_STATS.IP_FRAG_MISSING++;
						//logger.warn("IPFragmentationManager: Missing fragments: " + fc + "/" + buff.length + "/" + ipf.ip_fragment_map.size());
					}
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static IPHeader process(IPHeader iph){
		IPFragPacket ipf = null;
		if(iph != null){
			if(iph.more_fragments || iph.fragment_offset != 0){
				ipf = packet_lst.get(iph.source + ":" + iph.destination + ":" + iph.identification);
				if(ipf == null){
					ipf = new IPFragPacket();
					packet_lst.put(iph.source + ":" + iph.destination + ":" + iph.identification, ipf);
				}
				// first fragment
				ipf.new_fragment(iph);
				ipf.ts = System.currentTimeMillis();
				// last fragment
				if(!iph.more_fragments && ipf.ip_fragment_map.size() > 1){
					return reassemble(iph.source + ":" + iph.destination + ":" + iph.identification);
				}
			}else{
				return iph;
			}			
		}
		return null;
	}
}
