package net.isup.parameters;

import net.isup.ParameterType;

public class CollectCallRequest extends ParameterBase {
	public CollectCallRequest(){
		type = ParameterType.COLLECT_CALL_REQUEST;
	}
}
