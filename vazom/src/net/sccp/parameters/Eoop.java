package net.sccp.parameters;

// one octet value, has to be 0(zero)
public class Eoop extends ParamBase {
	private int value;
	
	public Eoop() {
		super();
		type = ParamType.END_OF_OPTIONAL_PARAMETERS;
		value = 0;
	}
	
	public int getValue(){ return value; }
	public void setValue(int val){ value = val; }

	//public int encodeValue(){ return value; }

	public void init(byte[] data) {}
}
