package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class HeartbeatData extends M3UAParameter {
	
	public HeartbeatData(){
		type = M3UAParameterType.HEARTBEAT;
	}

	public void init(byte[] data) {
		value = data;
	}



}
