package net.cli;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import net.utils.Utils;
import org.apache.log4j.Logger;

public abstract class CliConnectionBase {
	public Logger logger=Logger.getLogger(CliConnectionBase.class);
	protected Socket tcp;
	protected ConcurrentHashMap<String, CliConnectionBase> conn_list;
	protected Reader_r reader_r;
	protected Thread reader_t;
	public boolean stopping;
	protected PrintWriter out;
	protected OutputStream outb;
	protected InputStream inb;
	protected String con_ip;
	protected int con_port;
	public String auth_user;
	public int auth_user_group;
	public CLIBase cli = null;
	public String node_id;
	public String node_type;
	public boolean history_found;
	public CliConnectionBase current_conn;
	
	protected String cmd = "";
	protected int last_cmd_index = 0;
	protected ArrayList<String> last_cmd_lst = new ArrayList<String>();
	protected String res;
	protected ArrayList<CLIHistoryItem> his_lst = new ArrayList<CLIHistoryItem>();
	protected CLIHistoryItem his = null;
	protected String tmp_parent = "";
	protected String[] tokens = null;
	protected ArrayList<DescriptorBase> tmp_lst = null;
	protected ArrayList<String> tmp_str = null;

	protected void init_threads(){
		reader_r = new Reader_r(this);
		reader_t = new Thread(reader_r, "CLI_CONN_R_" + con_ip + ":" + con_port);
		reader_t.start();
	}
	
	
	public abstract void cli_execute();
	
	public CliConnectionBase(CLIBase climanager, String _node_id, String _node_type, Socket socket, ConcurrentHashMap<String, CliConnectionBase> _conn_list){
		tcp = socket;
		cli = climanager;
		node_id = _node_id;
		node_type = _node_type;
		try{
			tcp.setSoTimeout(300000);
			//in = new BufferedReader(new InputStreamReader(tcp.getInputStream()));
			inb = tcp.getInputStream();
			out = new PrintWriter(tcp.getOutputStream());
			outb = tcp.getOutputStream();
			con_ip = tcp.getInetAddress().toString();
			conn_list = _conn_list;
			con_port = tcp.getPort();
			init_threads();
		}catch(Exception e){
			logger.error("ERROR WHILE INITIALIZING INPUT/OUTPUT STREAMS FOR CLI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
			e.printStackTrace();
		}
		
		
	}
	
	protected ArrayList<CLIHistoryItem> rebuildCLI(String cmd){
		return cli.processLine(cmd);
	}

	
	private class Reader_r implements Runnable{
		
		
		
		public Reader_r(CliConnectionBase conn){
			current_conn = conn;
		}

		public void login(){
			boolean auth = false;
			int bc;
			byte[] buff = new byte[512];
			int c = 0;
			String username = null;
			String password = null;
			int attempts = 0;

			out.print("\u001B[1;33m");
			out.print("Username: ");
			out.print("\u001B[0m");
			out.flush();
			
			while(!auth){
				try{
					bc = inb.read(buff);
					if(bc == -1) stopping = true;
					else{
						for(int i = 0; i<bc; i++){
							c = buff[i];
							if(c == 13){
								if(username == null){
									username = cmd;
									cmd = "";
									out.print("\u001B[1;33m");
									out.print("\n\rPassword: ");
									out.print("\u001B[0m");
									out.flush();

								}else if(password == null){
									password = cmd;
									if(AuthManager.check(username, Utils.bytes2hexstr(Utils.md5(password), ""))){
										auth = true;
										auth_user = username;
										
										out.print("\n\r");
										out.print(Utils.ansi_paint("[WHITE]User [[GREEN]" + username + "[WHITE]] logged in!\n\r"));
										out.flush();
										cmd = "";
									}else{
										//auth = true;
										out.print(Utils.ansi_paint("\n\r[RED]Invalid credentials!\n\r"));
										attempts++;
										if(attempts >=3){
											auth = true;
											stopping = true;
										}else{
											username = null;
											password = null;
											out.print("\n\r");
											out.print("\u001B[1;33m");
											out.print("Username: ");
											out.print("\u001B[0m");
											out.flush();
											cmd = "";
											
										}
										
									}
								}
								
								
							}else{
								// clear
								if(c == 12){
									out.print("\u001B[2J");
									out.print("\u001B[0;0H");
									out.print("\u001B[1;33m");
									if(username == null){
										out.print("Username: ");
										out.print("\u001B[0m");
										out.print(cmd);
										
									}else{
										out.print("Password: ");
										out.print("\u001B[0m");
										out.print(Utils.convert_str2pwd(cmd));
										
									}
									out.flush();
								
								// backspace
								}else if(c == 127){
									if(cmd.length() > 0){
										cmd = cmd.substring(0, cmd.length() - 1);
										out.print("\u001B[1D");
										out.print("\u001B[K");
										out.flush();
									
									}

								
								// normal key
								}else if(c != 0){
									cmd += (char)c;
									// echo
									if(username != null){
										out.print("*");
										out.flush();
										
									}else{
										out.print((char)c);
										out.flush();
										
									}
		
								}
							}
						}						
					}

				}catch(SocketTimeoutException e){
					logger.error("CONNECTION TIMEOUT! [ " + con_ip + ":" + con_port + " ]");
					//e.printStackTrace();
					stopping = true;
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}

		}		
		
		
		public void run() {
			byte[] buff = new byte[512];
			byte[] buff_special = new byte[2];
			boolean special = false;
			int c = 0;
			int bc;
			//logger.info("VAZOM CLI READER STARTED: [ " + con_ip + ":" + con_port + " ]");
			try{
				// IAC WILL ECHO IAC WILL SUPPRESS_GO_AHEAD IAC WONT LINEMODE
				outb.write(new byte[]{(byte)255, (byte)251, 1, (byte)255, (byte)251, 3, (byte)255, (byte)252, 34});
				out.flush();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				
			}catch(Exception e){}
			//out.flush();
			out.println();
			out.println();
			/*
			String vl = "^[[0;37m     ^[[0;1;37;47m  ^[[0;37m    ^[[0;5;37;47mS^[[0;37m ^[[0;1;37;47m;^[[0;5;37;47m8888%%^[[0;37m      ^[[0;1;37;47m%^[[0;37m  ^[[0;5;37;47mXX888@8^[[0;37m ^[[0;5;37;47m%888S8@8^[[0;37m     ^[[0m \n" + 
			"^[[0;37m     ^[[0;1;37;47m; ^[[0;37m   ^[[0;5;37;47m@^[[0;1;37;47m ^[[0;37m     ^[[0;5;37;47m%8:^[[0;37m    ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m   ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m   ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m ^[[0;5;37;47m ^[[0;37m   ^[[0;5;37;47m ^[[0;37m  ^[[0;5;37;47m8^[[0;37m     ^[[0m \n" +
			"^[[0;37m     ^[[0;1;37;47m;^[[0;5;37;47m8^[[0;37m  ^[[0;5;37;47m%^[[0;1;37;47m ^[[0;37m    ^[[0;5;37;47m88^[[0;37m  ^[[0;5;37;47m ^[[0;37m   ^[[0;5;37;47m%^[[0;1;37;47m ^[[0;37m     ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m  ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m    ^[[0;5;37;47mt^[[0;37m   ^[[0;5;37;47m8^[[0;37m     ^[[0m \n" + 
			"^[[0;37m     ^[[0;1;37;47m;^[[0;5;37;47m88^[[0;1;37;47m ^[[0;37m     ^[[0;5;37;47m8^[[0;37m    ^[[0;5;37;47m ^[[0;37m ^[[0;1;37;47m ^[[0;5;37;47m;888^[[0;1;37;47m. ^[[0;37m    ^[[0;1;37;47m ^[[0;5;37;47m8X8^[[0;37m ^[[0;5;37;47m88^[[0;1;37;47m ^[[0;37m    ^[[0;5;37;47m8^[[0;37m     ^[[0m \n";
			*/
			String vl = 
			"[WHITE]" +
			"@@@  @@@   @@@@@@   @@@@@@@@   @@@@@@   @@@@@@@@@@\n" +   
			"@@@  @@@  @@@@@@@@  @@@@@@@@  @@@@@@@@  @@@@@@@@@@@\n" +  
			"@@!  @@@  @@!  @@@       @@!  @@!  @@@  @@! @@! @@!\n" +  
			"!@!  @!@  !@!  @!@      !@!   !@!  @!@  !@! !@! !@!\n" +  
			"@!@  !@!  @!@!@!@!     @!!    @!@  !@!  @!! !!@ @!@\n" +  
			"!@!  !!!  !!!@!!!!    !!!     !@!  !!!  !@!   ! !@!\n" +  
			":!:  !!:  !!:  !!!   !!:      !!:  !!!  !!:     !!:\n" +  
			" ::!!:!   :!:  !:!  :!:       :!:  !:!  :!:     :!:\n" +  
			"  ::::    ::   :::   :: ::::  ::::: ::  :::     ::\n" +   
			"   :       :   : :  : :: : :   : :  :    :      :\n"; 
				   
			vl = vl.replaceAll("\\^\\[", "\u001b");
			vl = vl.replaceAll("\n", "\n" + (char)13);
			out.print(Utils.ansi_paint(vl));

			out.println();
			out.println();
			out.print("\u001B[1;34m");
			out.println("Welcome to VAZOM CLI!" + (char)13);
			login();
			if(!stopping){
				out.print("\u001B[0m");
				//out.println("\u001B[1;37mType '\u001B[1;32mHELP\u001B[1;37m' for help!" + (char)13);
				out.print("\u001B[1;33m");
				out.print("\u001B[1;33mVAZOM-" + node_type + "[\u001B[1;34m" + node_id + "\u001B[1;33m]> ");
				out.print("\u001B[0m");
				out.print("\u001B7"); // save cursor pos
				out.flush();
				
			}
			
			// abstract
			while(!stopping){
				try{
					bc = inb.read(buff);
					if(bc == -1) stopping = true;
					else{
						for(int i = 0; i<bc; i++){
							c = buff[i];
							if(c == 13){
								// abstract
								cli_execute();
								
							}else{
								// start of special key
								if(c == 0x1b){
									special = true;
									
								// tab
								}else if(c == 9){
									tokens = cmd.split("[ \t]+");
									res = "\n";
									his_lst = cli.processLine(cmd);
									if(his_lst.size() > 0){
										cmd = "";
										for(int j = 0; j<his_lst.size(); j++){
											his = his_lst.get(j);
											cmd += his.cmd + " ";
											if(his.params != null){
												for(int k = 0; k<his.params.size(); k++){
													if(his.params.get(k).value != null){
														if(his.params.get(k).group){
															cmd += his.params.get(k).name + " ";
															cmd += "\"" + his.params.get(k).value +  "\" ";
															
														}else{
															cmd += his.params.get(k).name + " ";
															cmd += his.params.get(k).value + " ";
															
														}
													}
												}
											}
											
										}
										//cmd = cmd.toLowerCase();
										
										
										// param check
										ParamDescriptor pd = null;
										for(int j = 0; j<his_lst.size(); j++) if(his_lst.get(j).incomplete){
											his = his_lst.get(j);
											if(his.params != null){
												for(int k = 0; k<his.params.size(); k++){
													pd = his.params.get(k);
													if(pd.value == null) res += Utils.ansi_paint("[RED] " + his.cmd + ": parameter [ [WHITE]" + pd.name + "[RED] ] missing!\n");
												}
											}
										}
										
										
										if(tokens.length != his_lst.size()){

											// param hints
											tmp_lst = cli.getLst(his_lst.get(his_lst.size() - 1).cmd, his_lst.get(his_lst.size() - 1).parent);
											tmp_str = cli.getParamLst(tmp_lst.get(tmp_lst.size() - 1), tokens[tokens.length - 1]);
											if(tmp_str != null){
												int j = 0;
												int k = 0;
												if(tmp_str.size() > 0){
													//cmd += tmp_str.get(0) + " ";
													his = his_lst.get(his_lst.size() - 1);
													while(j < tmp_str.size()){
														k = 0;
														while(k < his.params.size()){
															if(his.params.get(k).name.equalsIgnoreCase(tmp_str.get(j)) && his.params.get(k).value == null){
																cmd += tmp_str.get(j) + " ";
																k = his.params.size();
																j = tmp_str.size();
															}
															k++;
														}
														j++;
													}
												}
											}else{
												// group/method hint
												for(int j = 0; j<his_lst.size(); j++) tmp_parent = his_lst.get(j).cmd + "," + tmp_parent;
												tmp_parent = tmp_parent.substring(0, tmp_parent.length() - 1);
												//System.out.println("PARENT: " + tmp_parent);
												tmp_lst = cli.getLst(tokens[tokens.length - 1], tmp_parent);
												cmd += tokens[tokens.length - 1];
												res += cli.generateLst(tmp_lst);
												if(tmp_lst.size() == 0){
													//res += "Unknown method [" + tokens[tokens.length - 1] + "]!\n";
													tmp_lst = cli.getLst("", tmp_parent);
													res += cli.generateLst(tmp_lst);
													
												}
												
											}
											
											
											
										}else{								

											if(his_lst.size() > 1)
												res += cli.generateLst(cli.getLst("", his_lst.get(his_lst.size() - 1).cmd + "," + his_lst.get(his_lst.size() - 1).parent));
											else
												res += cli.generateLst(cli.getLst("", his_lst.get(his_lst.size() - 1).cmd));
										}
									
									}else{
										tmp_lst = cli.getLst(tokens[0], "");
										if(tmp_lst.size() == 1){
											//cmd = tmp_lst.get(0).name;
										}
										res += cli.generateLst(tmp_lst);
									}
									tmp_parent = "";
									out.println(res.replaceAll("\n", "\n" + (char)13) + (char)13);
									
									out.print("\u001B[1;33m");
									out.print("\u001B[1;33mVAZOM-" + node_type + "[\u001B[1;34m" + node_id + "\u001B[1;33m]> \u001B[0m" + cmd);
									out.print("\u001B[0m");
									out.print("\u001B7"); // save cursor pos
									out.flush();
																	
									// bye
								}else if(c == 4){
									stopping = true;
								// backspace
								}else if(c == 127){
									if(cmd.length() > 0){
										//System.out.println("AAA");
										cmd = cmd.substring(0, cmd.length() - 1);
										out.print("\u001B[1D");
										out.print("\u001B[K");
										out.flush();
									}
								// clear
								}else if(c == 12){
									out.print("\u001B[2J");
									out.print("\u001B[0;0H");
									out.print("\u001B[1;33m");
									out.print("\u001B[1;33mVAZOM-" + node_type + "[\u001B[1;34m" + node_id + "\u001B[1;33m]> ");
									out.print("\u001B[0m");
									out.print(cmd);
									out.print("\u001B7"); // save cursor pos
									out.print("\u001B[0m");
									out.flush();
								// normal key
								}else if(c != 0){
									// special started
									if(special){
										if(buff[i] == 0x5b) buff_special[0] = 0x5b;
										else if(buff[i] == 0x41) buff_special[1] = 0x41;
										else if(buff[i] == 0x42) buff_special[1] = 0x42;
										else special = false;
										
										
										// up key
										if(buff_special[0] == 0x5b && buff_special[1] == 0x41 && last_cmd_lst.size() > 0){
											if(last_cmd_index < 0 || last_cmd_index >= last_cmd_lst.size()) last_cmd_index = last_cmd_lst.size() - 1;
											//System.out.println("LAST: " + last_cmd_lst.get(last_cmd_index));
											
											out.print("\u001B8"); // get cursor pos
											out.print("\u001B[K"); //clear line from cursor right
											
											out.print(last_cmd_lst.get(last_cmd_index));
											out.flush();
											//System.out.println("AAA");
											cmd = last_cmd_lst.get(last_cmd_index--);
											c = 0;
											special = false;
											buff_special[0] = -1;
											buff_special[1] = -1;
												
										// down key
										}else if(buff_special[0] == 0x5b && buff_special[1] == 0x42 && last_cmd_lst.size() > 0){
											if(last_cmd_index >= last_cmd_lst.size() || last_cmd_index < 0) last_cmd_index = 0;
											//System.out.println(last_cmd_index);
											
											out.print("\u001B8"); // get cursor pos
											out.print("\u001B[K"); //clear line from cursor right
											
											out.print(last_cmd_lst.get(last_cmd_index));
											out.flush();
											//System.out.println("AAA");
											cmd = last_cmd_lst.get(last_cmd_index++);
											c = 0;
											special = false;
											buff_special[0] = -1;
											buff_special[1] = -1;
		
										}
									// normal keuy
									}else{
										special = false;
										cmd += (char)c;
										// echo
										out.print((char)c);
										out.flush();
										
									}
								}
							}
						}						
					}

				}catch(SocketTimeoutException e){
					logger.error("CONNECTION TIMEOUT! [ " + con_ip + ":" + con_port + " ]");
					//e.printStackTrace();
					stopping = true;
					
				}catch(Exception e){
					logger.error("ERROR WHILE READING INPUT STREAM FOR CLI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
					e.printStackTrace();
					stopping = true;
					
				}
			}
			//logger.info("CLI READER STOPPED: [ " + con_ip + ":" + con_port + " ]");
			//logger.info("CLOSING CLI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
			conn_list.remove(con_ip + ":" + con_port); 
			try{ tcp.close(); }catch(Exception e){}
			
		}
	
	}	
}
