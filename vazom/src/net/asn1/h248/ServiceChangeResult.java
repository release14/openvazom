package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceChangeResult extends CHOICE{

	public ErrorDescriptor get_errorDescriptor(){
		return (ErrorDescriptor)elements.get(0).data;
	}
	public ErrorDescriptor new_errorDescriptor(){
		return new ErrorDescriptor();
	}

	public ServiceChangeResParm get_serviceChangeResParms(){
		return (ServiceChangeResParm)elements.get(1).data;
	}
	public ServiceChangeResParm new_serviceChangeResParms(){
		return new ServiceChangeResParm();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_errorDescriptor(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceChangeResParms(); }});
	}
	public ServiceChangeResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
