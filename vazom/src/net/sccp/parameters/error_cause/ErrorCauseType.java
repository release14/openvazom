package net.sccp.parameters.error_cause;

import java.util.HashMap;


public enum ErrorCauseType {
	UNASSIGNED_DESTINATION_LRN(0x00),
	INCONSISTENT_SOURCE_LRN(0x01),
	POINT_CODE_MISMATCH(0x02),
	SERVICE_CLASS_MISMATCH(0x03),
	UNQUALIFIED(0x04);
	
	
	private int id;
	private static final HashMap<Integer, ErrorCauseType> lookup = new HashMap<Integer, ErrorCauseType>();
	static{
		for(ErrorCauseType td : ErrorCauseType.values()){
			lookup.put(td.getId(), td);
		}
	}
	private int getId(){ return id; }
	public static ErrorCauseType get(int id){ return lookup.get(id); }
	private ErrorCauseType(int _id){ id = _id; }
	
}
