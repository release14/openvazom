package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CommandRequest extends SEQUENCE{

	public CommandWrapper get_command(){
		return (CommandWrapper)elements.get(0).data;
	}
	public CommandWrapper new_command(){
		return new CommandWrapper();
	}

	public NULL get_optional(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_optional(){
		return new NULL();
	}

	public NULL get_wildcardReturn(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_wildcardReturn(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_command(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_optional(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_wildcardReturn(); }});
	}
	public CommandRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
