package net.m3ua;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.HashMap;

import net.asn1.compiler.ASNType;
import net.asn1.gsmmap.GSMMAPOperationLocalvalue;
import net.asn1.gsmmap.Invoke;
import net.asn1.gsmmap.MO_ForwardSM_Arg;
import net.asn1.gsmmap.MT_ForwardSM_Arg;
import net.asn1.tcap.TCMessage;
import net.ber.BerTranscoder;
import net.sccp.MessageType;
import net.sccp.SCCP;
import net.sccp.messages.MessageBase;
import net.sccp.messages.UDT_UnitData;
import net.sccp.parameters.global_title.EncodingScheme;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitle_NOA;
import net.sccp.parameters.global_title.GlobalTitle_TT;
import net.sccp.parameters.global_title.GlobalTitle_TTNPE;
import net.sccp.parameters.global_title.GlobalTitle_TTNPENOA;
import net.sms.AddressString;
import net.sms.GSMAlphabet;
import net.sms.MessageDirection;
import net.sms.MessagePart;
import net.sms.MultiPartDescriptor;
import net.sms.TBCD;
import net.sms.db.DBManager;
import net.sms.db.DBRecord;
import net.sms.db.SmsDirection;
import net.sms.db.SmsType;
import net.sms.smsfs.ConfigManager;
import net.sms.smsfs.FilterManager;
import net.sms.smsfs.FilterPacket;
import net.sms.tpdu.SmsDeliver;
import net.sms.tpdu.SmsSubmit;
import net.sms.tpdu.TPDU;
import net.sms.tpdu.udh.IE_Concatenated;
import net.sms.tpdu.udh.InformationElementType;

import org.apache.log4j.Logger;

public class DecoderWorker {
	private static Logger logger=Logger.getLogger(DecoderWorker.class);
	private Dw_r dw_r;
	private Thread dw_t;
	private String worker_name;
	private boolean stopping;

	
	public boolean isTCAP(byte[] data){
		if((data[0] == 0x61) || (data[0] == 0x62) || (data[0] == 0x64) || (data[0] == 0x65) || (data[0] == 0x67)) return true;
		return false;
	}
	
	public boolean isMO(TCMessage tcm){
		boolean res = false;
		// Begin
		if(isBegin(tcm)){
			if(tcm._begin._components != null){
				if(tcm._begin._components.children.size() > 0){
					if(tcm._begin._components.children.get(0)._invoke != null){
						if((tcm._begin._components.children.get(0)._invoke._opCode._localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue.mo_forwardSM)
							res = true;
					}
				}
			}
		// Continue
		}else if(isContinue(tcm)){
			if(tcm._continue._components != null){
				if(tcm._continue._components.children.size() > 0){
					if(tcm._continue._components.children.get(0)._invoke != null){
						if((tcm._continue._components.children.get(0)._invoke._opCode._localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue.mo_forwardSM)
							res = true;
					}
				}
			}
			
		}
		return res;
	}
	public boolean isContinue(TCMessage tcm){
		return tcm._continue != null;
	}
	
	public boolean isBegin(TCMessage tcm){
		return tcm._begin != null;
	}
	
	public String getGT(GlobalTitleBase _gt){
		String res = "";
		if(_gt instanceof GlobalTitle_TTNPENOA){
			GlobalTitle_TTNPENOA gt = (GlobalTitle_TTNPENOA)_gt;
			if(gt != null){
				if(gt.encodingScheme == EncodingScheme.BCD_EVEN){
					res = TBCD.decode(gt.addressInformation);
				} else if(gt.encodingScheme == EncodingScheme.BCD_ODD){
					res = TBCD.decodeOdd(gt.addressInformation);
				}
				
			}
			
		}else if(_gt instanceof GlobalTitle_NOA){
			GlobalTitle_NOA gt = (GlobalTitle_NOA)_gt;
			if(gt != null){
				if(gt.oddNumOfAddrSignals) res = TBCD.decodeOdd(gt.addressSignals);
				else res = TBCD.decode(gt.addressSignals);
			}
		}else if(_gt instanceof GlobalTitle_TT){
			GlobalTitle_TT gt = (GlobalTitle_TT)_gt;
			if(gt != null){
				res = TBCD.decode(gt.addressInformation);
			}
		}else if(_gt instanceof GlobalTitle_TTNPE){
			GlobalTitle_TTNPE gt = (GlobalTitle_TTNPE)_gt;
			if(gt != null){
				if(gt.encodingScheme == EncodingScheme.BCD_EVEN){
					res = TBCD.decode(gt.addressInformation);
				} else if(gt.encodingScheme == EncodingScheme.BCD_ODD){
					res = TBCD.decodeOdd(gt.addressInformation);
				}
			}
		}
		
		return res;
		
	}
	public boolean isMT(TCMessage tcm){
		boolean res = false;
		// Begin
		if(isBegin(tcm)){
			if(tcm._begin._components != null){
				if(tcm._begin._components.children.size() > 0){
					if(tcm._begin._components.children.get(0)._invoke != null){
						if((tcm._begin._components.children.get(0)._invoke._opCode._localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue.mt_forwardSM)
							res = true;
					}
				}
			}
		// Continue
		}else if(isContinue(tcm)){
			if(tcm._continue._components != null){
				if(tcm._continue._components.children.size() > 0){
					if(tcm._continue._components.children.get(0)._invoke != null){
						if((tcm._continue._components.children.get(0)._invoke._opCode._localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue.mt_forwardSM)
							res = true;
					}
				}
			}
		}
		return res;
	}

	public boolean isUDT(byte[] data){
		return (data[2] == 0x03);
	}
	public String getPrintable(byte[] data){
		String res = "";
		for(int i = 0; i<data.length; i++){
			if(data[i] >= 32 && data[i] < 127) res += (char)data[i];
			else res += ".";
		}
		return res;
		
	}
	public void MT_multiMsgDone(MultiPartDescriptor mpd){
		String tmp = "";
		DBRecord dbr;
		HashMap<Integer, String> tmp_map;
		if(mpd.totalParts == mpd.messageParts.size()){
			
			System.out.println("TYPE: CONCATENATED [" + mpd.totalParts + "] mt-forwardSM/SMS_DELIVER");							
			System.out.println("GT CALLED: " + mpd.GT_called);							
			System.out.println("GT CALLING: " + mpd.GT_calling);			
			System.out.println("SCOA: " + mpd.SCOA);			
			System.out.println("IMSI: " + mpd.IMSI);			
			System.out.println("MSISDN: " + mpd.MSISDN);			
			System.out.println("SMS ENCODING: " + mpd.encoding);			
			System.out.println("ORIGINATING ENCODING: " + mpd.originating_encoding);	
			System.out.println("SMS ORIGINATING: " + mpd.originating);	

			// SMS Text reassembling
			tmp_map = new HashMap<Integer, String>();
			for(int i = 0; i<mpd.messageParts.size(); i++) tmp_map.put(mpd.messageParts.get(i).partNumber, mpd.messageParts.get(i).text);
			for(int i = 1; i<=mpd.totalParts; i++) if(tmp_map.get(i) != null) tmp += tmp_map.get(i);
			System.out.println("SMS TEXT: " + tmp);
			System.out.println("----------------------------");
			

			// DB packet
			dbr = new DBRecord();
			dbr.direction = SmsDirection.MT;
			dbr.type = SmsType.CONCATENATED;
			dbr.gt_called = mpd.GT_called;
			dbr.gt_calling = mpd.GT_calling;
			dbr.scoa = mpd.SCOA;
			dbr.imsi = mpd.IMSI;
			dbr.msisdn = mpd.MSISDN;
			dbr.sms_text_enc = mpd.encoding.toString();
			dbr.sms_originating_enc = mpd.originating_encoding.toString();
			dbr.sms_originating = mpd.originating;
			dbr.sms_text = tmp;
			DBManager.queue.offer(dbr);
			
			
			mpd.messageParts.clear();
			DecoderManager.MT_multiMessageLst.remove(mpd.messageIdentifier);
		}
		
		
	
	}
	public void MO_multiMsgDone(MultiPartDescriptor mpd){
		String tmp = "";
		DBRecord dbr;
		HashMap<Integer, String> tmp_map;
		if(mpd.totalParts == mpd.messageParts.size()){
			System.out.println("TYPE: CONCATENATED [" + mpd.totalParts + "] mo-forwardSM/SMS_SUBMIT");							
			System.out.println("GT CALLED: " + mpd.GT_called);							
			System.out.println("GT CALLING: " + mpd.GT_calling);			
			System.out.println("SCDA: " + mpd.SCDA);			
			System.out.println("IMSI: " + mpd.IMSI);			
			System.out.println("MSISDN: " + mpd.MSISDN);			
			System.out.println("SMS ENCODING: " + mpd.encoding);			
			System.out.println("DESTINATION ENCODING: " + mpd.destination_encoding);	
			System.out.println("SMS DESTINATION: " + mpd.destination);	
			
			// SMS Text reassembling
			tmp_map = new HashMap<Integer, String>();
			for(int i = 0; i<mpd.messageParts.size(); i++) tmp_map.put(mpd.messageParts.get(i).partNumber, mpd.messageParts.get(i).text);
			for(int i = 1; i<=mpd.totalParts; i++) if(tmp_map.get(i) != null) tmp += tmp_map.get(i);
			System.out.println("SMS TEXT: " + tmp);
			System.out.println("----------------------------");
			
			// DB packet
			dbr = new DBRecord();
			dbr.direction = SmsDirection.MO;
			dbr.type = SmsType.CONCATENATED;
			dbr.gt_called = mpd.GT_called;
			dbr.gt_calling = mpd.GT_calling;
			dbr.scda = mpd.SCDA;
			dbr.imsi = mpd.IMSI;
			dbr.msisdn = mpd.MSISDN;
			dbr.sms_text_enc = mpd.encoding.toString();
			dbr.sms_destination_enc = mpd.destination_encoding.toString();
			dbr.sms_destination = mpd.destination;
			dbr.sms_text = tmp;
			
			DBManager.queue.offer(dbr);
			
			
			mpd.messageParts.clear();
			DecoderManager.MO_multiMessageLst.remove(mpd.messageIdentifier);
		}
		
		
	}
	
	private class Dw_r implements Runnable{
		private byte[] packet;
		FilterPacket fp;
		UDT_UnitData udt;
		MessageBase sccp_packet;
		BerTranscoder ber = new BerTranscoder();
		byte[] mo_data;
		byte[] mt_data;
		MO_ForwardSM_Arg mo_f_sm;
		MT_ForwardSM_Arg mt_f_sm;
		SmsSubmit sms_s;
		SmsDeliver sms_d;
		String gt_called_s;
		String gt_calling_s;
		AddressString as;
		String sms_text_decoded;
		ASNType invoke_param = null;
		DBRecord dbr;
		
		public void run() {
			int counter = 0;
			logger.info(worker_name + " STARTING!");
			while(!stopping){
				packet = DecoderManager.queue.poll();
				if(packet != null){
					try{
						//System.out.println("PACKET NUM: " + (counter++));
						counter++;
						if(SCCP.decodeType(packet) == MessageType.UDT_UNITDATA){
							// verify UDT packet, valid UDT must have its 3rd byte set to 0x03
							if(isUDT(packet)){
								sccp_packet = SCCP.decode(packet);
								DecoderManager.STATS.UDT_COUNT++;
								
								udt = (UDT_UnitData)sccp_packet;
								
								// decode GT info
								gt_called_s = getGT(udt.calledPartyAddress.globalTitle);
								gt_calling_s = getGT(udt.callingPartyAddress.globalTitle);
								
								
								
								// check if TCAP exists
								if(isTCAP(udt.data)){
									//System.out.println("DECODING TCAP");
									DecoderManager.STATS.TCAP_COUNT++;
									TCMessage tcm = (TCMessage)ber.decode("net.asn1.tcap.TCMessage", udt.data);
									//logger.info("PACKET DECODED...");
									
									// MO
									if(isMO(tcm)){
										DecoderManager.STATS.MO_COUNT++;
										// Begin or Continue
										if(isBegin(tcm)) invoke_param = tcm._begin._components.children.get(0)._invoke._parameter;
										else if(isContinue(tcm)) invoke_param = tcm._continue._components.children.get(0)._invoke._parameter;
										
										mo_data = Arrays.copyOfRange(udt.data,invoke_param.byte_position - invoke_param.berTag.lengthSize - 1, 
												(invoke_param.byte_position - invoke_param.berTag.lengthSize - 1) + (invoke_param.length + invoke_param.berTag.lengthSize + 1));
										
										mo_f_sm = (MO_ForwardSM_Arg)ber.decode("net.asn1.gsmmap.MO_ForwardSM_Arg", mo_data);
										if(mo_f_sm._sm_RP_DA != null){
											if(mo_f_sm._sm_RP_DA._serviceCentreAddressDA != null){
												// SMS TPDU
												if(mo_f_sm._sm_RP_UI != null){
													TPDU tpdu = TPDU.decode(mo_f_sm._sm_RP_UI.getValueBytes(), MessageDirection.MS_SC);
													if(tpdu instanceof SmsSubmit){
														DecoderManager.STATS.SMS_SUBMIT_COUNT++;
														sms_s = (SmsSubmit)tpdu;

														// Multi Part message
														if(sms_s.udh != null){
															int ie_index = sms_s.udh.findIE(InformationElementType.CONCATENATED);
															if(ie_index > -1){
																DecoderManager.STATS.MO_CONCATENATED_COUNT++;
																IE_Concatenated iec = (IE_Concatenated)sms_s.udh.informationElements.get(ie_index);
																
																// new multipart message
																if(DecoderManager.MO_multiMessageLst.get(iec.messageIdentifier) == null){
																	MultiPartDescriptor mpd = new MultiPartDescriptor();
																	mpd.lastUpdate = System.currentTimeMillis();
																	mpd.totalParts = iec.parts;
																	// add to list
																	DecoderManager.MO_multiMessageLst.put(iec.messageIdentifier, mpd);
																	mpd.messageIdentifier = iec.messageIdentifier;
																	
																	MessagePart mp = new MessagePart();
																	mp.partNumber = iec.partNumber;
																	
																	mpd.type = GSMMAPOperationLocalvalue.mo_forwardSM;
																	mpd.GT_called = gt_called_s;
																	mpd.GT_calling = gt_calling_s;
																	as = AddressString.decode(mo_f_sm._sm_RP_DA._serviceCentreAddressDA.getValueBytes());
																	mpd.SCDA = TBCD.decode(as.digits);
																	if(mo_f_sm._imsi != null) mpd.IMSI = TBCD.decode(mo_f_sm._imsi.getValueBytes());
																	if(mo_f_sm._sm_RP_OA != null){
																		if(mo_f_sm._sm_RP_OA._msisdn != null){
																			as = AddressString.decode(mo_f_sm._sm_RP_OA._msisdn.getValueBytes());
																			mpd.MSISDN = TBCD.decode(as.digits);
																			
																		}
																	}
																	if(sms_s != null){
																		if(sms_s.TP_UD != null){
																			mpd.encoding = sms_s.TP_DCS.encoding;
																			switch(sms_s.TP_DCS.encoding){
																				case DEFAULT: 
																					sms_text_decoded = GSMAlphabet.decode(GSMAlphabet.reposition(sms_s.TP_UD, 1)); 
																					break;
																				case _8BIT:
																					sms_text_decoded = getPrintable(sms_s.TP_UD);
																					break;
																				case UCS2:
																					sms_text_decoded = new String(sms_s.TP_UD, "UTF-16");
																					break;
																					
																			}
																			mp.text = sms_text_decoded;		
																			sms_text_decoded = "";
																		}
																		// Mandatory field
																		mpd.destination_encoding = sms_s.TP_DA.typeOfNumber;
																		switch(sms_s.TP_DA.typeOfNumber){
																			case ALPHANUMERIC:
																				mpd.destination = GSMAlphabet.decode(sms_s.TP_DA.digits);							
																				break;
																			default:
																				mpd.destination = TBCD.decode(sms_s.TP_DA.digits);							
																				break;
																		}
																	}
																	// add to list
																	mpd.messageParts.add(mp);
																
																// next part of multipart message
																}else{
																	MultiPartDescriptor mpd = DecoderManager.MO_multiMessageLst.get(iec.messageIdentifier);
																	mpd.lastUpdate = System.currentTimeMillis();
																	MessagePart mp = new MessagePart();
																	mp.partNumber = iec.partNumber;
																	sms_s = (SmsSubmit)tpdu;
																	if(sms_s != null){
																		if(sms_s.TP_UD != null){
																			switch(mpd.encoding){
																				case DEFAULT: 
																					sms_text_decoded = GSMAlphabet.decode(GSMAlphabet.reposition(sms_s.TP_UD, 1)); 
																					break;
																				case _8BIT:
																					sms_text_decoded = getPrintable(sms_s.TP_UD);
																					break;
																				case UCS2:
																					sms_text_decoded = new String(sms_s.TP_UD, "UTF-16");
																					break;
																					
																			}
																			mp.text = sms_text_decoded;		
																			sms_text_decoded = "";
																		}
																	}																	
																	mpd.messageParts.add(mp);
																	
																	// check if multi message is finished
																	MO_multiMsgDone(mpd);
																}
															}
														// Single message
														}else if(isBegin(tcm)){
															// DB packet
															dbr = new DBRecord();
															dbr.direction = SmsDirection.MO;
															dbr.type = SmsType.SINGLE;
															dbr.gt_called = gt_called_s;
															dbr.gt_calling = gt_calling_s;
															
															System.out.println("TYPE: mo-forwardSM/SMS_SUBMIT");							
															System.out.println("GT CALLED: " + gt_called_s);							
															System.out.println("GT CALLING: " + gt_calling_s);			
															as = AddressString.decode(mo_f_sm._sm_RP_DA._serviceCentreAddressDA.getValueBytes());
															System.out.println("SCDA: " + TBCD.decode(as.digits));
															dbr.scda = TBCD.decode(as.digits);
															if(mo_f_sm._imsi != null){
																System.out.println("IMSI: " + TBCD.decode(mo_f_sm._imsi.getValueBytes()));
																dbr.imsi = TBCD.decode(mo_f_sm._imsi.getValueBytes());
																
															}
															if(mo_f_sm._sm_RP_OA != null){
																if(mo_f_sm._sm_RP_OA._msisdn != null){
																	as = AddressString.decode(mo_f_sm._sm_RP_OA._msisdn.getValueBytes());
																	System.out.println("MSISDN: " + TBCD.decode(as.digits));
																	dbr.msisdn = TBCD.decode(as.digits);
																	
																}
															}
															if(sms_s != null){
																if(sms_s.TP_UD != null){
																	System.out.println("SMS ENCODING: " + sms_s.TP_DCS.encoding);
																	dbr.sms_text_enc = sms_s.TP_DCS.encoding.toString();
																	switch(sms_s.TP_DCS.encoding){
																		case DEFAULT: 
																			sms_text_decoded = GSMAlphabet.decode(sms_s.TP_UD); 
																			break;
																		case _8BIT:
																			sms_text_decoded = getPrintable(sms_s.TP_UD);
																			break;
																		case UCS2:
																			sms_text_decoded = new String(sms_s.TP_UD, "UTF-16");
																			break;
																			
																	}
																	System.out.println("SMS TEXT: " + sms_text_decoded);	
																	dbr.sms_text = sms_text_decoded;
																	sms_text_decoded = "";
																}
																// Mandatory field
																System.out.println("DESTINATION ENCODING: " + sms_s.TP_DA.typeOfNumber);
																dbr.sms_destination_enc = sms_s.TP_DA.typeOfNumber.toString();
																switch(sms_s.TP_DA.typeOfNumber){
																	case ALPHANUMERIC:
																		System.out.println("SMS DESTINATION [Alphanumeric]: " + GSMAlphabet.decode(sms_s.TP_DA.digits));							
																		dbr.sms_destination = GSMAlphabet.decode(sms_s.TP_DA.digits);
																		break;
																	default:
																		System.out.println("SMS DESTINATION: " + TBCD.decode(sms_s.TP_DA.digits));
																		dbr.sms_destination = TBCD.decode(sms_s.TP_DA.digits);
																		break;
																}
															}
															System.out.println("----------------------------");
															// DB queue
															DBManager.queue.offer(dbr);
															
														}
													}
												}
											}
										}
									// MT
									}else if(isMT(tcm)){
										DecoderManager.STATS.MT_COUNT++;
										
										// Begin or Continue
										if(isBegin(tcm)) invoke_param = tcm._begin._components.children.get(0)._invoke._parameter;
										else if(isContinue(tcm)) invoke_param = tcm._continue._components.children.get(0)._invoke._parameter;

										mt_data = Arrays.copyOfRange(udt.data,invoke_param.byte_position - invoke_param.berTag.lengthSize - 1, 
												(invoke_param.byte_position - invoke_param.berTag.lengthSize - 1) + (invoke_param.length + invoke_param.berTag.lengthSize + 1));
										mt_f_sm = (MT_ForwardSM_Arg)ber.decode("net.asn1.gsmmap.MT_ForwardSM_Arg", mt_data);
										if(mt_f_sm._sm_RP_OA != null){
											if(mt_f_sm._sm_RP_OA._serviceCentreAddressOA != null){
												if(mt_f_sm._sm_RP_UI != null){
													TPDU tpdu = TPDU.decode(mt_f_sm._sm_RP_UI.getValueBytes(), MessageDirection.SC_MS);
													if(tpdu instanceof SmsDeliver){
														DecoderManager.STATS.SMS_DELIVER_COUNT++;
														sms_d = (SmsDeliver)tpdu;
														// Multi Part message
														if(sms_d.udh != null){
															int ie_index = sms_d.udh.findIE(InformationElementType.CONCATENATED);
															if(ie_index > -1){
																DecoderManager.STATS.MT_CONCATENATED_COUNT++;
																IE_Concatenated iec = (IE_Concatenated)sms_d.udh.informationElements.get(ie_index);
																
																// new multipart message
																if(DecoderManager.MT_multiMessageLst.get(iec.messageIdentifier) == null){
																	MultiPartDescriptor mpd = new MultiPartDescriptor();
																	mpd.lastUpdate = System.currentTimeMillis();
																	mpd.totalParts = iec.parts;
																	// add to list
																	DecoderManager.MT_multiMessageLst.put(iec.messageIdentifier, mpd);
																	mpd.messageIdentifier = iec.messageIdentifier;
																	
																	MessagePart mp = new MessagePart();
																	mp.partNumber = iec.partNumber;
																	
																	mpd.type = GSMMAPOperationLocalvalue.mt_forwardSM;
																	mpd.GT_called = gt_called_s;
																	mpd.GT_calling = gt_calling_s;
																	as = AddressString.decode(mt_f_sm._sm_RP_OA._serviceCentreAddressOA.getValueBytes());
																	mpd.SCDA = TBCD.decode(as.digits);
																	if(mt_f_sm._sm_RP_DA._imsi != null) mpd.IMSI = TBCD.decode(mt_f_sm._sm_RP_DA._imsi.getValueBytes());
																	if(mt_f_sm._sm_RP_OA._msisdn != null){
																		as = AddressString.decode(mt_f_sm._sm_RP_OA._msisdn.getValueBytes());
																		mpd.MSISDN = TBCD.decode(as.digits);
																	}
																	if(sms_d != null){
																		if(sms_d.TP_UD != null){
																			mpd.encoding = sms_d.TP_DCS.encoding;
																			switch(sms_d.TP_DCS.encoding){
																				case DEFAULT: 
																					sms_text_decoded = GSMAlphabet.decode(GSMAlphabet.reposition(sms_d.TP_UD, 1)); 
																					break;
																				case _8BIT:
																					sms_text_decoded = getPrintable(sms_d.TP_UD);
																					break;
																				case UCS2:
																					sms_text_decoded = new String(sms_d.TP_UD, "UTF-16");
																					break;
																					
																			}
																			mp.text = sms_text_decoded;		
																			sms_text_decoded = "";
																		}
																		// Mandatory field
																		mpd.originating_encoding = sms_d.TP_OA.typeOfNumber;
																		switch(sms_d.TP_OA.typeOfNumber){
																			case ALPHANUMERIC:
																				mpd.originating = GSMAlphabet.decode(sms_d.TP_OA.digits);							
																				break;
																			default:
																				mpd.originating = TBCD.decode(sms_d.TP_OA.digits);							
																				break;
																		}
																	}
																	// add to list
																	mpd.messageParts.add(mp);
																
																// next part of multipart message
																}else{
																	MultiPartDescriptor mpd = DecoderManager.MT_multiMessageLst.get(iec.messageIdentifier);
																	mpd.lastUpdate = System.currentTimeMillis();
																	MessagePart mp = new MessagePart();
																	mp.partNumber = iec.partNumber;
																	sms_d = (SmsDeliver)tpdu;
																	if(sms_d != null){
																		if(sms_d.TP_UD != null){
																			switch(mpd.encoding){
																				case DEFAULT: 
																					sms_text_decoded = GSMAlphabet.decode(GSMAlphabet.reposition(sms_d.TP_UD, 1)); 
																					break;
																				case _8BIT:
																					sms_text_decoded = getPrintable(sms_d.TP_UD);
																					break;
																				case UCS2:
																					sms_text_decoded = new String(sms_d.TP_UD, "UTF-16");
																					break;
																					
																			}
																			mp.text = sms_text_decoded;		
																			sms_text_decoded = "";
																		}
																	}																	
																	mpd.messageParts.add(mp);
																	
																	// check if multi message is finished
																	MT_multiMsgDone(mpd);
																	
																}
															}
														// Single message
														}else if(isBegin(tcm)){
															// DB packet
															dbr = new DBRecord();
															dbr.direction = SmsDirection.MT;
															dbr.type = SmsType.SINGLE;
															dbr.gt_called = gt_called_s;
															dbr.gt_calling = gt_calling_s;
															
															System.out.println("TYPE: mt-forwardSM/SMS_DELIVER");							
															System.out.println("GT CALLED: " + gt_called_s);							
															System.out.println("GT CALLING: " + gt_calling_s);
															AddressString as = AddressString.decode(mt_f_sm._sm_RP_OA._serviceCentreAddressOA.getValueBytes());
															System.out.println("SCOA: " + TBCD.decode(as.digits));
															dbr.scoa = TBCD.decode(as.digits);
															if(mt_f_sm._sm_RP_DA != null){
																if(mt_f_sm._sm_RP_DA._imsi != null){
																	System.out.println("IMSI: " + TBCD.decode(mt_f_sm._sm_RP_DA._imsi.getValueBytes()));
																	dbr.imsi = TBCD.decode(mt_f_sm._sm_RP_DA._imsi.getValueBytes());
																}
															}
															if(mt_f_sm._sm_RP_OA._msisdn != null){
																as = AddressString.decode(mt_f_sm._sm_RP_OA._msisdn.getValueBytes());
																System.out.println("MSISDN: " + TBCD.decode(as.digits));
																dbr.msisdn = TBCD.decode(as.digits);
															}
															
															if(sms_d != null){
																if(sms_d.TP_UD != null){
																	System.out.println("SMS ENCODING: " + sms_d.TP_DCS.encoding);
																	dbr.sms_text_enc = sms_d.TP_DCS.encoding.toString();
																	switch(sms_d.TP_DCS.encoding){
																		case DEFAULT: 
																			sms_text_decoded = GSMAlphabet.decode(sms_d.TP_UD); 
																			break;
																		case _8BIT:
																			sms_text_decoded = getPrintable(sms_d.TP_UD);
																			break;
																		case UCS2:
																			sms_text_decoded = new String(sms_d.TP_UD, "UTF-16");
																			break;
																	}
																	System.out.println("SMS TEXT: " + sms_text_decoded);		
																	dbr.sms_text = sms_text_decoded;
																	sms_text_decoded = "";

																	// Mandatory field
																	System.out.println("ORIGINATING ENCODING: " + sms_d.TP_OA.typeOfNumber);
																	dbr.sms_originating_enc = sms_d.TP_OA.typeOfNumber.toString();
																	switch(sms_d.TP_OA.typeOfNumber){
																		case ALPHANUMERIC:
																			System.out.println("SMS ORIGINATING [Alphanumeric]: " + GSMAlphabet.decode(sms_d.TP_OA.digits));							
																			dbr.sms_originating = GSMAlphabet.decode(sms_d.TP_OA.digits);
																			break;
																		default:
																			System.out.println("SMS ORIGINATING: " + TBCD.decode(sms_d.TP_OA.digits));
																			dbr.sms_originating = TBCD.decode(sms_d.TP_OA.digits);
																			break;
																	}
																}
															}
															System.out.println("----------------------------");
															// DB queue
															DBManager.queue.offer(dbr);
														}												
													}
												}
											}
										}
									}
								}							
							}else DecoderManager.STATS.SKIPPED_COUNT++;
							
						}else DecoderManager.STATS.SKIPPED_COUNT++;
						
					//	counter++;
					}catch(Exception e){
						long ts = System.currentTimeMillis();
						logger.error("Error while decoding Sigtran stack, possible invalid packet!");
						System.out.println("PACKET["+ts+"]: " +counter);
						e.printStackTrace();
						File f = new File("/tmp/" + ts + ".raw");
						try{
							FileOutputStream fo = new FileOutputStream(f);
							for(int i = 0; i<packet.length; i++) fo.write(packet[i]);
							fo.close();
						}catch(Exception e2){
							e2.printStackTrace();
						}
						
						
					}
					
				}else{
					//logger.info(worker_name + " sleeping for 1000 msec!");
					try{ Thread.sleep(1000); }catch(Exception e){ e.printStackTrace(); }
				}
				
			}
			logger.info(worker_name + " ENDING!");
			
		}
	
	
	}
	public void stop(){
		stopping = true;
		
	}
	
	public DecoderWorker(int id){
		dw_r = new Dw_r();
		worker_name = "DECODER_WORKER_" + id;
		dw_t = new Thread(dw_r, worker_name);
		dw_t.start();
		
		
	}
}
