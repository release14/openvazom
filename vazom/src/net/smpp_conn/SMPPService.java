package net.smpp_conn;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import net.config.SGNConfigData;
import net.stats.StatsManager;

import org.apache.log4j.Logger;

public class SMPPService {
	public static Logger logger=Logger.getLogger(SMPPService.class);
	private static ConcurrentHashMap<String, SMPPClientConnection> connection_lst;
	private static ServerSocket tcp;
	private static int listener_port;
	private static Listener_r listener_r;
	private static Thread listener_t;
	private static boolean stopping;

	private static class Listener_r implements Runnable{
		private Socket connection;
		private SMPPClientConnection con;
		
		public void run() {
			while(!stopping){
				try{
					connection = tcp.accept();
					con = new SMPPClientConnection(connection, connection_lst, true);
					connection_lst.put(connection.getInetAddress().toString() + ":" + connection.getPort(), con);
					// stats
					StatsManager.SMPP_STATS.CLIENT_CONNECTION_COUNT++;
					//logger.info("New connection to SMPP Service! [ " + connection.getInetAddress().toString() + ":" + connection.getPort() + " ]");
				}catch(Exception e){
					logger.error("Error while accepting new connection to SMPP Service! [ " + listener_port + " ]");
					e.printStackTrace();
					stopping = true;
				}
			}			
		}
	}	
	
	private static void init_listener_thread(){
		listener_r = new Listener_r();
		listener_t = new Thread(listener_r, "SMPP_CLIENT_LISTENER");
		listener_t.start();
		
	}

	private static void init_server(int port){
		try{
			logger.info("SMPP Service: end point connection = [" + SGNConfigData.smpp_proxy_end_point + "]!");
			logger.info("SMPP Service: client port = [" + SGNConfigData.smpp_proxy_client_port + "]!");
			tcp = new ServerSocket(port);
			listener_port = port;
			logger.info("SMPP Service initialized!");
			init_listener_thread();
			
		}catch(Exception e){
			logger.error("Error while initializing SMPP Service, port = [ " + port + " ]");
			e.printStackTrace();
			
		}
		
		
	}
	
	
	public static void init(int _port){
		logger.info("Starting SMPP Service...");
		connection_lst = new ConcurrentHashMap<String, SMPPClientConnection>();
		init_server(_port);
		
		
	}
	
}
