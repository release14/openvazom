package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SM_RP_MTI extends INTEGER{

	public SM_RP_MTI(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
