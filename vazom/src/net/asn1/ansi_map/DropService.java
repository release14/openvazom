package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DropService extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public CDMAConnectionReferenceList get_cdmaConnectionReferenceList(){
		return (CDMAConnectionReferenceList)elements.get(1).data;
	}
	public CDMAConnectionReferenceList new_cdmaConnectionReferenceList(){
		return new CDMAConnectionReferenceList();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(2).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(3).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(4).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public ReleaseReason get_releaseReason(){
		return (ReleaseReason)elements.get(5).data;
	}
	public ReleaseReason new_releaseReason(){
		return new ReleaseReason();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(212, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceList(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_releaseReason(); }});
	}
	public DropService(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
