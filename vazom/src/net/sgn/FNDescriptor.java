package net.sgn;

import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;
import net.config.SGNConfigData;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingManager;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.sgc.SGCManager;
import net.stats.StatsManager;
import net.vstp.MessageDescriptor;
import net.vstp.VSTP;
import net.vstp.VSTP_SGF_CorrelationPacket;
import net.vstp.VSTP_SGF_CorrelationManager;

public class FNDescriptor {
	public static Logger logger=Logger.getLogger(FNDescriptor.class);
	public String id;
	private boolean stopping;
	public int sctp_id = -1;
	public ConcurrentLinkedQueue<MessageDescriptor> private_out_queue;
	private Reader reader;
	private Writer writer;
	private Thread reader_t;
	private Thread writer_t;
	
	
	private class Reader implements Runnable{
		MessageDescriptor md = null;
		SSCTPDescriptor sd = null;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						//System.out.println(new String(sd.payload));
						md = VSTP.decode(sd.payload);
						SGNode.in_offer(md);
						
					}
				}else{
					stopping = true;
					logger.info("Removing FN Connection [" + id + "]!");
					FNManager.removeNode(sctp_id);
				}
			}
			logger.info("Ending...");
			
		}
	}	
	private class Writer implements Runnable{
		MessageDescriptor md = null;
		MessageDescriptor md_p = null;
		//M3UAConnection m3ua_conn = null;
		RoutingConnection r_conn = null;
		//M3UAPayload mp = null;
		RoutingConnectionPayload rpl = null;
		VSTP_SGF_CorrelationPacket vsd = null;
		int res;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				md = SGNode.out_poll();
				if(md != null){
					// check if filter connections exist
					if(FNManager.fn_map.size() > 0){
						// set destination id
						md.header.destination_id = id;
						md.header.source_id = SGNConfigData.sgn_id;
						// encode and send
						res = SSctp.send(sctp_id, md.encode(), 0);
						if(res == -1){
							// send back to queue
							SGNode.out_offer(md);
							// remove connection
							stopping = true;
							logger.info("Removing FN Connection [" + id + "]!");
							FNManager.removeNode(sctp_id);
							// stats
							StatsManager.VSTP_STATS.VSTP_F_CONNECTION_LOST++;
						}
					// no filter node connectins, accept message without filtering
					}else{
						r_conn = RoutingManager.getDefault(vsd.type, vsd);
						if(r_conn != null){
							rpl = RoutingManager.createPayload(vsd.type);
							if(rpl != null){
								vsd = VSTP_SGF_CorrelationManager.get(md.header.msg_id);
								rpl.setParamsFromVSTP_SGF(vsd);
								// send to default routing connection
								r_conn.out_offer(rpl);
								logger.debug("No available F Nodes, bypassing filters, message sent to default M3UA Connection!");
							}else{
								logger.warn("RoutingManager: unknown payload type [" + vsd.type + "]!");
							}

						}else logger.warn("RoutingManager: default connection for type [" + vsd.type + "] does not exist!");
						// remove correlation
						VSTP_SGF_CorrelationManager.remove(md.header.msg_id);
						// distribute remove
						SGCManager.distribute_vstp_crl_remove(md.header.msg_id, md.header.ds);
						// stats
						StatsManager.VSTP_STATS.VSTP_CRL_REMOVE++;
						StatsManager.VSTP_STATS.VSTP_NO_F_NODES++;
						
					}
					
				}
				// private queue
				md_p = private_out_queue.poll();
				if(md_p != null){
					// set destination id
					md_p.header.destination_id = id;
					md_p.header.source_id = SGNConfigData.sgn_id;
					// encode and send
					SSctp.send(sctp_id, md_p.encode(), 0);

				}

				if(md == null && md_p == null){
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
			}
			logger.info("Ending...");
			
		}
	}	
	public void start(){
		logger.info("Starting FN Connection id = [" + sctp_id + "]!");
		// threads
		reader = new Reader();
		reader_t = new Thread(reader, "FN_READER_" + sctp_id);
		reader_t.start();
		
		writer = new Writer();
		writer_t = new Thread(writer, "FN_WRITER_" + sctp_id);
		writer_t.start();
	}
	
	public FNDescriptor(int _sctp_id){
		//in_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		//out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		private_out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		sctp_id = _sctp_id;
		//id = _id;
		//address = _address;
		//port = _port;
	}

}
