package net.stats;

public class StatsManager {
	public static CMDIStats CMDI_STATS;
	public static SystemStats SYSTEM_STATS;
	public static DSStats DS_STATS;
	public static DecoderStats DECODER_STATS;
	public static HPLMNRStats HPLMNR_STATS;
	public static RefilterStats REFILTER_STATS;
	public static SMSFSStats SMSFS_STATS;
	public static GPUBStats GPUB_STATS;
	public static VSTPStats VSTP_STATS;
	public static DBStats DB_STATS;
	public static SMPPStats SMPP_STATS;
	public static IPFragStats IP_FRAG_STATS;
	
	
	
	public static void init(){
		CMDI_STATS = new CMDIStats();
		SYSTEM_STATS = new SystemStats();
		DS_STATS = new DSStats();
		DECODER_STATS = new DecoderStats();
		HPLMNR_STATS = new HPLMNRStats();
		REFILTER_STATS = new RefilterStats();
		SMSFS_STATS = new SMSFSStats();
		GPUB_STATS = new GPUBStats();
		VSTP_STATS = new VSTPStats();
		DB_STATS = new DBStats();
		SMPP_STATS = new SMPPStats();
		IP_FRAG_STATS = new IPFragStats();
		SYSTEM_STATS.STARTUP_TS = System.currentTimeMillis();
		
	}

}
