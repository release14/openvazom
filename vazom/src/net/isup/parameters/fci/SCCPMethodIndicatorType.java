package net.isup.parameters.fci;

import java.util.HashMap;



public enum SCCPMethodIndicatorType {
	NO(0x00),
	CONNETIONLESS(0x02),
	CONNECTION(0x04),
	CONNECTIONLESS_AND_CONNECTION(0x06);
	
	
	private int id;
	private static final HashMap<Integer, SCCPMethodIndicatorType> lookup = new HashMap<Integer, SCCPMethodIndicatorType>();
	static{
		for(SCCPMethodIndicatorType td : SCCPMethodIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static SCCPMethodIndicatorType get(int id){ return lookup.get(id); }
	private SCCPMethodIndicatorType(int _id){ id = _id; }	
}
