package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TerminationStateDescriptor extends SEQUENCE{

	public PropertyParmSequenceOf get_propertyParms(){
		return (PropertyParmSequenceOf)elements.get(0).data;
	}
	public PropertyParmSequenceOf new_propertyParms(){
		return new PropertyParmSequenceOf();
	}

	public EventBufferControl get_eventBufferControl(){
		return (EventBufferControl)elements.get(1).data;
	}
	public EventBufferControl new_eventBufferControl(){
		return new EventBufferControl();
	}

	public ServiceState get_serviceState(){
		return (ServiceState)elements.get(2).data;
	}
	public ServiceState new_serviceState(){
		return new ServiceState();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_propertyParms(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_eventBufferControl(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceState(); }});
	}
	public TerminationStateDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
