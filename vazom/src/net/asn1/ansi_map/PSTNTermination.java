package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PSTNTermination extends SEQUENCE{

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(0).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(1).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(2).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(3).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(4).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(5).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(6).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(7).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
	}
	public PSTNTermination(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
