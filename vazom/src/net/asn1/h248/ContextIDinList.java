package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ContextIDinList extends INTEGER{

	public ContextIDinList(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
