package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.CauseIndicators;
import net.isup.parameters.FacilityIndicator;

public class FacilityReject extends MessageBase {
	public FacilityIndicator facility_indicator;
	public CauseIndicators cause_indicators;
	
	public FacilityReject(){
		super();
		type = MessageType.FACILITY_REJECT;
		pointers = new Pointer[2];
	}

	public void init(byte[] data) {
		if(data != null){
			facility_indicator = new FacilityIndicator();
			facility_indicator.init(new byte[]{data[byte_pos++]});
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			pointers[1] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			cause_indicators = new CauseIndicators();
			cause_indicators.init(getPointerData(data, 0));
			
			// optional
			processOptional(data, 1);
			
		}
	}

}
