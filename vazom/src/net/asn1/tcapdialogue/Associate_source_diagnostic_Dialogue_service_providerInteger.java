package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Associate_source_diagnostic_Dialogue_service_providerInteger extends SEQUENCE{

	public Dialogue_service_providerInteger get_dialogue_service_provider(){
		return (Dialogue_service_providerInteger)elements.get(0).data;
	}
	public Dialogue_service_providerInteger new_dialogue_service_provider(){
		return new Dialogue_service_providerInteger();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_dialogue_service_provider(); }});
	}
	public Associate_source_diagnostic_Dialogue_service_providerInteger(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 2;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
