package net.config;

import java.lang.reflect.Field;
import java.util.ArrayList;

import net.ds.DataSourceType;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.sccp.SubsystemNumber;

public class SGNConfigData {
	
	public static String[] search(String ptrn){
		String[] res = null;
		ArrayList<String> tmp = new ArrayList<String>();
		Field[] flst = SGNConfigData.class.getFields();
		for(int i = 0; i<flst.length; i++) if(flst[i].getName().toUpperCase().contains(ptrn.toUpperCase())){
			tmp.add(flst[i].getName().replaceAll("_", "."));
		}
		res = new String[tmp.size()];
		for(int i = 0; i<tmp.size(); i++) res[i] = tmp.get(i);
		return res;
	}	
	
	
	public static void set(String item_name, String item_value){
		item_name = item_name.replaceAll("\\.", "_");
		try{
			Field fld = SGNConfigData.class.getField(item_name);
			String fld_type = fld.getType().getSimpleName();

			if(fld_type.equalsIgnoreCase("String")){
				fld.set(SGNConfigData.class, item_value);
				
			}else if(fld_type.equalsIgnoreCase("int")){
				fld.set(SGNConfigData.class, Integer.parseInt(item_value));
				
			}else if(fld_type.equalsIgnoreCase("ServiceIndicatorType")){
				fld.set(SGNConfigData.class, ServiceIndicatorType.valueOf(item_value));
				
			}else if(fld_type.equalsIgnoreCase("SubsystemNumber")){
				fld.set(SGNConfigData.class, SubsystemNumber.valueOf(item_value));
				
			}else if(fld_type.equalsIgnoreCase("boolean")){
				if(item_value.equalsIgnoreCase("true")){
					fld.set(SGNConfigData.class, true);
				}else{
					fld.set(SGNConfigData.class, false);
					
				}
				
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
	}
	public static Object get(String item_name){
		item_name = item_name.replaceAll("\\.", "_");
		try{
			Field fld = SGNConfigData.class.getField(item_name);
			return fld.get(SGNConfigData.class);
		}catch(Exception e){
			//e.printStackTrace();
		}
		return null;
	}
	
	// h248
	public static boolean h248_status;
	
	//isup
	public static boolean isup_status;
	
	
	// node
	public static String sgn_id;
	public static boolean sgn_debug;
	public static String sgn_bind;
	public static int sgn_port;
	public static int sgn_global_max_queue_size;
	public static int f_workers;
	
	// spcap
	public static int spcap_snaplen;
	public static int spcap_queue_max;
	
	// smpp -> ss7
	public static boolean smpp2ss7_status;
	public static int smpp2ss7_error_code_sync_interval;
	
	// smpp
	public static boolean smpp_debug;
	public static int smpp_check_inerval;
	public static int smpp_timeout;
	public static boolean smpp_proxy_status;
	public static int smpp_proxy_client_port;
	public static String smpp_proxy_end_point;
	public static boolean smpp_users_sync_status;
	public static int smpp_users_sync_interval;

	//m3ua
	public static boolean m3ua_debug;
	public static int m3ua_reconnect_interval;
	public static boolean m3ua_reconnect_status;
	public static int m3ua_daud_interval;
	
	public static int m3ua_smsfs_hlr_callback_check_interval;
	public static int m3ua_smsfs_hlr_callback_timeout;
	
	
	// hlr
	public static int hlr_timeout_check_interval;
	public static int hlr_timeout;
	

	
	//sgc
	public static String sgc_bind;
	public static int sgc_port;
	public static boolean sgc_reconnect_status;
	public static int sgc_reconnect_interval;
	// vstp
	public static int vstp_sgf_crl_check_interval;
	public static int vstp_sgf_crl_timeout;
	
	// PDS
	//public static DataSourceType pds_type;
	public static boolean pds_status;
	public static int pds_ip_frag_timeout;
	//public static String pds_spcap_if;
	//public static String pds_spcap_bpf;
	//public static int pds_workers;
	//public static int[] pds_spcap_id;
	// CMDI
	public static int cmdi_port;
	public static int vstp_cmdi_port;
	
	// SRI
	public static int hplmnr_timeout_check_interval;
	public static int hplmnr_timeout;
	public static boolean hplmnr_status;
	public static boolean hplmnr_debug;
	//public static boolean sri_mo_mt_smsfs_status;
	//public static boolean sri_hlr_smsfs_status;
	public static boolean hplmnr_hlr_gt_override_status;
	public static String hplmnr_hlr_gt_override;
	public static String hplmnr_nnn_gt;
	public static String hplmnr_nnn_routable_gt;
	public static String hplmnr_msc_gt;
	public static String hplmnr_sgsn_gt;
	public static String hplmnr_smsrouter_gt;
	//public static int sri_m3ua_data_routingcontext;
	//public static int hplmnr_m3ua_data_opc;
	//public static int hplmnr_mss_dpc_m3ua_data_dpc;
	//public static int hplmnr_hlr_dpc_m3ua_data_dpc;
	//public static ServiceIndicatorType hplmnr_m3ua_data_si;
	public static int hplmnr_m3ua_data_ni;
	public static int hplmnr_m3ua_data_mp;
	public static int hplmnr_m3ua_data_sls;
	public static int hplmnr_m3ua_data_nap;
	public static int hplmnr_sccp_called_gt_translationtype;
	public static int hplmnr_sccp_calling_gt_translationtype;
	public static SubsystemNumber hplmnr_sccp_called_ssn;
	public static SubsystemNumber hplmnr_sccp_calling_ssn;
	public static SubsystemNumber hplmnr_mo_sccp_called_ssn;
	public static SubsystemNumber hplmnr_mo_sccp_calling_ssn;
	public static String hplmnr_mo_smsc;
	public static String hplmnr_mo_smsrouter_gt;
	public static int hplmnr_imsi_correlation_mcc;
	public static int hplmnr_imsi_correlation_mnc;
	//public static String hplmnr_tcap_dialogue_appctx_oid;
	public static boolean dr_sri_status;
	public static boolean dr_sri_errors_status;
	public static int dr_sri_errors_check_interval;
	public static int dr_sri_errors_timeout;
	public static String hplmnr_sri_gt;
	public static String hplmnr_mo_gt;
	public static String hplmnr_mt_gt;

}
