package net.smsfs;

public enum FilterDescriptorType {
	RULE,
	MODIFIER;
}
