package net.smpp.parameters;

import java.util.HashMap;

public enum SMSCDeliveryReceiptType {
	NO_SMSC_DELIVERY(0x00),
	SUCCESS_FAILURE(0x01),
	FAILURE(0x02);
	
	private int id;
	private static final HashMap<Integer, SMSCDeliveryReceiptType> lookup = new HashMap<Integer, SMSCDeliveryReceiptType>();
	static{
		for(SMSCDeliveryReceiptType td : SMSCDeliveryReceiptType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static SMSCDeliveryReceiptType get(int id){ return lookup.get(id); }
	private SMSCDeliveryReceiptType(int _id){ id = _id; }	
}
