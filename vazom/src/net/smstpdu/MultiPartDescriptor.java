package net.smstpdu;

import java.util.Vector;

import net.ds.DataSourceType;
import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.NumberingPlan;
import net.smstpdu.tpdcs.general.Alphabet;

public class MultiPartDescriptor {
	public int totalParts;
	public int messageIdentifier;
	public Vector<MessagePart> messageParts;
	public long lastUpdate;
	// impportant message info
	public int type;
	
	public SmsPduType pdu_type;
	// -------- SMPP ------- 
	public String ip_source;
	public String ip_destination;
	public int tcp_source;
	public int tcp_destination;
	public String system_id;
	// ------------------
	
	public String GT_called;
	public String GT_calling;
	public int gt_called_tt;
	public int gt_calling_tt;
	public NatureOfAddress gt_called_nai;
	public NatureOfAddress gt_calling_nai;
	public NumberingPlan gt_called_np;
	public NumberingPlan gt_calling_np;
	public GlobalTitleIndicator gt_called_gti;
	public GlobalTitleIndicator gt_calling_gti;
	
	public String SCDA;
	public String SCOA;
	public String MSISDN;
	public String IMSI;
	public Alphabet encoding;
	public TypeOfNumber destination_encoding;
	public TypeOfNumber originating_encoding;
	public String destination;
	public String originating;
	public int m3ua_data_dpc;
	public int m3ua_data_opc;
	public Long tcap_sid;
	public Long tcap_did;
	public DataSourceType dataSource;
	
	public int sms_status;
	
	public MultiPartDescriptor(){
		messageParts = new Vector<MessagePart>();
	}
	public boolean partExists(int partNum){
		for(int i = 0; i<messageParts.size(); i++) if(messageParts.get(i).partNumber == partNum) return true;
		return false;
	}
}
