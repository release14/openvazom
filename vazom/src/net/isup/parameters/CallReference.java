package net.isup.parameters;

import java.util.Arrays;

import net.isup.ParameterType;

public class CallReference extends ParameterBase {
	public byte[] call_identity;
	public int signalling_point_code;
	
	public CallReference(){
		type = ParameterType.CALL_REFERENCE;
	}

	public void init(byte[] data) {
		if(data != null){
			call_identity = Arrays.copyOfRange(data, byte_position, byte_position + 3);
			byte_position += 3;
			signalling_point_code = (data[byte_position] & 0x0c) >> 2;
		}
	}

}
