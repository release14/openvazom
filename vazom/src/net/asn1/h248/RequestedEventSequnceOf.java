package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RequestedEventSequnceOf extends SEQUENCE{

	public RequestedEvent new_child(){
		return new RequestedEvent();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public RequestedEvent getChild(int index){
		return (RequestedEvent)of_children.get(index);
	}
	public RequestedEventSequnceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
