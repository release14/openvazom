package net.isup.parameters.cldpn;

import java.util.HashMap;

public enum NumberingPlanIndicatorType {
	ISDN(0x10),
	DATA(0x30),
	TELEX(0x40);
	
	
	private int id;
	private static final HashMap<Integer, NumberingPlanIndicatorType> lookup = new HashMap<Integer, NumberingPlanIndicatorType>();
	static{
		for(NumberingPlanIndicatorType td :NumberingPlanIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NumberingPlanIndicatorType get(int id){ return lookup.get(id); }
	private NumberingPlanIndicatorType(int _id){ id = _id; }		
}
