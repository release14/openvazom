package net.spcap;

public class ETHHeader {
	public byte[] destination;
	public byte[] source;
	public byte[] payload;
	public EtherType ether_type; 
}
