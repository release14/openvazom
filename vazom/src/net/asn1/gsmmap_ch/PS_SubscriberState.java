package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PS_SubscriberState extends CHOICE{

	public NULL get_notProvidedFromSGSNorMME(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_notProvidedFromSGSNorMME(){
		return new NULL();
	}

	public NULL get_ps_Detached(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_ps_Detached(){
		return new NULL();
	}

	public NULL get_ps_AttachedNotReachableForPaging(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_ps_AttachedNotReachableForPaging(){
		return new NULL();
	}

	public NULL get_ps_AttachedReachableForPaging(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_ps_AttachedReachableForPaging(){
		return new NULL();
	}

	public PDP_ContextInfoList get_ps_PDP_ActiveNotReachableForPaging(){
		return (PDP_ContextInfoList)elements.get(4).data;
	}
	public PDP_ContextInfoList new_ps_PDP_ActiveNotReachableForPaging(){
		return new PDP_ContextInfoList();
	}

	public PDP_ContextInfoList get_ps_PDP_ActiveReachableForPaging(){
		return (PDP_ContextInfoList)elements.get(5).data;
	}
	public PDP_ContextInfoList new_ps_PDP_ActiveReachableForPaging(){
		return new PDP_ContextInfoList();
	}

	public NotReachableReason get_netDetNotReachable(){
		return (NotReachableReason)elements.get(6).data;
	}
	public NotReachableReason new_netDetNotReachable(){
		return new NotReachableReason();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_notProvidedFromSGSNorMME(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ps_Detached(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ps_AttachedNotReachableForPaging(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ps_AttachedReachableForPaging(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ps_PDP_ActiveNotReachableForPaging(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ps_PDP_ActiveReachableForPaging(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_netDetNotReachable(); }});
	}
	public PS_SubscriberState(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
