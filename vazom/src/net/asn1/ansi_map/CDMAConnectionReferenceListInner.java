package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAConnectionReferenceListInner extends SEQUENCE{

	public CDMAConnectionReferenceInformation get_cdmaConnectionReferenceInformation(){
		return (CDMAConnectionReferenceInformation)elements.get(0).data;
	}
	public CDMAConnectionReferenceInformation new_cdmaConnectionReferenceInformation(){
		return new CDMAConnectionReferenceInformation();
	}

	public CDMAConnectionReferenceInformation get_cdmaConnectionReferenceInformation2(){
		return (CDMAConnectionReferenceInformation)elements.get(1).data;
	}
	public CDMAConnectionReferenceInformation new_cdmaConnectionReferenceInformation2(){
		return new CDMAConnectionReferenceInformation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(211, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaConnectionReferenceInformation(); }});
		elements.add(new ElementDescriptor(211, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceInformation2(); }});
	}
	public CDMAConnectionReferenceListInner(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
