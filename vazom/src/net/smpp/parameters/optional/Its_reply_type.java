package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Its_reply_type extends OptionalParameterBase {
	public enum ValueType{
		DIGIT(0),
		NUMBER(1),
		TELEPHONE_NO(2),
		PASSWORD(3),
		CHARACTER_LINE(4),
		MENU(5),
		DATE(6),
		TIME(7),
		CONTINUE(8);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}	
	public ValueType value;
	public Its_reply_type(){
		type = OptionalParameterType.ITS_REPLY_TYPE;
	}

	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
