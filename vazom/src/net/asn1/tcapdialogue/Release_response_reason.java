package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Release_response_reason extends INTEGER{

	public static final int _normal = 0;
	public static final int _not_finished = 1;
	public static final int _user_defined = 30;
	public Release_response_reason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
