package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FacilitiesDirectiveRes extends SET{

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(0).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(1).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public TDMABurstIndicator get_tdmaBurstIndicator(){
		return (TDMABurstIndicator)elements.get(2).data;
	}
	public TDMABurstIndicator new_tdmaBurstIndicator(){
		return new TDMABurstIndicator();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(3).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(31, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBurstIndicator(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
	}
	public FacilitiesDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
