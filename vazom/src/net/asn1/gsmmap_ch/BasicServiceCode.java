package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class BasicServiceCode extends CHOICE{

	public BearerServiceCode get_bearerService(){
		return (BearerServiceCode)elements.get(0).data;
	}
	public BearerServiceCode new_bearerService(){
		return new BearerServiceCode();
	}

	public TeleserviceCode get_teleservice(){
		return (TeleserviceCode)elements.get(1).data;
	}
	public TeleserviceCode new_teleservice(){
		return new TeleserviceCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_bearerService(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_teleservice(); }});
	}
	public BasicServiceCode(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
