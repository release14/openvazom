package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class BearerServiceCode extends OCTET_STRING{

	public static final String allBearerServices = "'00000000'B";
	public static final String allDataCDA_Services = "'00010000'B";
	public static final String dataCDA_300bps = "'00010001'B";
	public static final String dataCDA_1200bps = "'00010010'B";
	public static final String dataCDA_1200_75bps = "'00010011'B";
	public static final String dataCDA_2400bps = "'00010100'B";
	public static final String dataCDA_4800bps = "'00010101'B";
	public static final String dataCDA_9600bps = "'00010110'B";
	public static final String general_dataCDA = "'00010111'B";
	public static final String allDataCDS_Services = "'00011000'B";
	public static final String dataCDS_1200bps = "'00011010'B";
	public static final String dataCDS_2400bps = "'00011100'B";
	public static final String dataCDS_4800bps = "'00011101'B";
	public static final String dataCDS_9600bps = "'00011110'B";
	public static final String general_dataCDS = "'00011111'B";
	public static final String allPadAccessCA_Services = "'00100000'B";
	public static final String padAccessCA_300bps = "'00100001'B";
	public static final String padAccessCA_1200bps = "'00100010'B";
	public static final String padAccessCA_1200_75bps = "'00100011'B";
	public static final String padAccessCA_2400bps = "'00100100'B";
	public static final String padAccessCA_4800bps = "'00100101'B";
	public static final String padAccessCA_9600bps = "'00100110'B";
	public static final String general_padAccessCA = "'00100111'B";
	public static final String allDataPDS_Services = "'00101000'B";
	public static final String dataPDS_2400bps = "'00101100'B";
	public static final String dataPDS_4800bps = "'00101101'B";
	public static final String dataPDS_9600bps = "'00101110'B";
	public static final String general_dataPDS = "'00101111'B";
	public static final String llAlternateSpeech_DataCDA = "'00110000'B";
	public static final String allAlternateSpeech_DataCDS = "'00111000'B";
	public static final String allSpeechFollowedByDataCDA = "'01000000'B";
	public static final String allSpeechFollowedByDataCDS = "'01001000'B";
	public static final String allDataCircuitAsynchronous = "'01010000'B";
	public static final String allAsynchronousServices = "'01100000'B";
	public static final String allDataCircuitSynchronous = "'01011000'B";
	public static final String allSynchronousServices = "'01101000'B";
	public static final String allPLMN_specificBS = "'11010000'B";
	public static final String plmn_specificBS_1 = "'11010001'B";
	public static final String plmn_specificBS_2 = "'11010010'B";
	public static final String plmn_specificBS_3 = "'11010011'B";
	public static final String plmn_specificBS_4 = "'11010100'B";
	public static final String plmn_specificBS_5 = "'11010101'B";
	public static final String plmn_specificBS_6 = "'11010110'B";
	public static final String plmn_specificBS_7 = "'11010111'B";
	public static final String plmn_specificBS_8 = "'11011000'B";
	public static final String plmn_specificBS_9 = "'11011001'B";
	public static final String plmn_specificBS_A = "'11011010'B";
	public static final String plmn_specificBS_B = "'11011011'B";
	public static final String plmn_specificBS_C = "'11011100'B";
	public static final String plmn_specificBS_D = "'11011101'B";
	public static final String plmn_specificBS_E = "'11011110'B";
	public static final String plmn_specificBS_F = "'11011111'B";
	public BearerServiceCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
