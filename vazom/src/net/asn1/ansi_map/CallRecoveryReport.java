package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CallRecoveryReport extends SET{

	public CallRecoveryIDList get_callRecoveryIDList(){
		return (CallRecoveryIDList)elements.get(0).data;
	}
	public CallRecoveryIDList new_callRecoveryIDList(){
		return new CallRecoveryIDList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(304, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_callRecoveryIDList(); }});
	}
	public CallRecoveryReport(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
