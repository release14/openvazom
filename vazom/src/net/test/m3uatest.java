package net.test;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.ASPAC;
import net.m3ua.messages.ASPUP;
import net.m3ua.parameters.RoutingContext;
import net.m3ua.parameters.TrafficModeType;
import net.m3ua.parameters.tmt.TMTType;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;


public class m3uatest {

	public static void main(String[] args) {
		byte[] data = null;
		boolean done = false;
		M3UAPacket mp;
		ASPUP aspup;
		ASPAC aspac;
		mp = M3UA.prepareNew(M3UAMessageType.ASPUP);
		aspup = (ASPUP)mp.message;
		
		data = mp.encode();
		
		
		
		
		
		int id = SSctp.initClient("10.4.6.19", 7100, 16, null, 0);
		
		System.out.println("INIT");
		SSctp.sendM3ua(id, data, 3);
		System.out.println("Sent : " + mp.message.type);
		
		SSCTPDescriptor sd;
		while(!done){
			sd = SSctp.receiveM3ua(id);
			if(sd.payload != null){
				mp = M3UA.decode(sd.payload);
				System.out.println("Received : " + mp.message.type);
				if(mp.message.type == M3UAMessageType.ASPUP_ACK){
					mp = M3UA.prepareNew(M3UAMessageType.ASPAC);
					aspac = (ASPAC)mp.message;
					aspac.trafficModeType = new TrafficModeType(TMTType.LOADSHARE);
					aspac.routingContext = new RoutingContext();
					aspac.routingContext.value = new byte[]{0, 0, 0x03, (byte)0xe8};
					data = mp.encode();
					SSctp.sendM3ua(id, data, sd.sid);
					System.out.println("Sent : " + mp.message.type);

					
					
					
				}else if(mp.message.type == M3UAMessageType.ASPAC_ACK){
					//mp = M3UA.decode(data);
					//try{ Thread.sleep(43200000); }catch(Exception e){ e.printStackTrace(); }
					done = true;
					
				}
				
			}
			
			
		}
		
		
		SSctp.shutdownClient(id);
		System.out.println("SHUTDOWN");
	}

}
