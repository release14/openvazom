package net.mtp3.messages.signalling;

import java.util.Arrays;

import net.utils.Utils;

public class SignallingDataLinkConnectionOrder extends SignallingMessageBase {
	public int dataLinkIdentity;
	
	public SignallingDataLinkConnectionOrder(){
		super();
	}
	public void init(byte[] data) {
		super.init(data);
		boolean[] tmp_bits = Utils.bytes2bits(Utils.reverse(Arrays.copyOfRange(data, byte_pos, byte_pos + 1)));;
		dataLinkIdentity = Utils.bits2int(tmp_bits, 0, 12);
	}

}
