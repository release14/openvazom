package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModificationRequest extends SEQUENCE{

	public ServiceDataAccessElementList get_serviceDataAccessElementList(){
		return (ServiceDataAccessElementList)elements.get(0).data;
	}
	public ServiceDataAccessElementList new_serviceDataAccessElementList(){
		return new ServiceDataAccessElementList();
	}

	public AllOrNone get_allOrNone(){
		return (AllOrNone)elements.get(1).data;
	}
	public AllOrNone new_allOrNone(){
		return new AllOrNone();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(271, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceDataAccessElementList(); }});
		elements.add(new ElementDescriptor(247, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_allOrNone(); }});
	}
	public ModificationRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
