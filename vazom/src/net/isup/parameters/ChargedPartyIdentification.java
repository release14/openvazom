package net.isup.parameters;

import net.isup.ParameterType;

public class ChargedPartyIdentification extends ParameterBase {
	public ChargedPartyIdentification(){
		type = ParameterType.CHARGED_PARTY_IDENTIFICATION;
	}
}
