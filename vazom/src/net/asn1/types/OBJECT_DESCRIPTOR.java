package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class OBJECT_DESCRIPTOR extends ASNType {

	public OBJECT_DESCRIPTOR(){
		super();
		universalTag = ASNTag.OBJECT_DESCRIPTOR;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Primitive;
		
	}
}
