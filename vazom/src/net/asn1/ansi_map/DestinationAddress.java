package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DestinationAddress extends CHOICE{

	public GlobalTitle get_globalTitle(){
		return (GlobalTitle)elements.get(0).data;
	}
	public GlobalTitle new_globalTitle(){
		return new GlobalTitle();
	}

	public PC_SSN get_pC_SSN(){
		return (PC_SSN)elements.get(1).data;
	}
	public PC_SSN new_pC_SSN(){
		return new PC_SSN();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(261, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_globalTitle(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_pC_SSN(); }});
	}
	public DestinationAddress(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
