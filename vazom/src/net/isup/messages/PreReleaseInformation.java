package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;

public class PreReleaseInformation extends MessageBase {
	public PreReleaseInformation(){
		super();
		type = MessageType.PRE_RELEASE_INFORMATION;
		pointers = new Pointer[1];
	}
	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			processOptional(data, 0);
			
		}

	}

}
