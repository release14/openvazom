package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.CircuitGroupSupervision;
import net.isup.parameters.RangeAndStatus;

public class CircuitGroupBlocking extends MessageBase {
	public CircuitGroupSupervision circuit_group_supervision;
	public RangeAndStatus range_and_status;
	public CircuitGroupBlocking(){
		super();
		type = MessageType.CIRCUIT_GROUP_BLOCKING;
		pointers = new Pointer[1];
	}

	public void init(byte[] data) {
		if(data != null){
			circuit_group_supervision = new CircuitGroupSupervision();
			circuit_group_supervision.init(new byte[]{data[byte_pos++]});
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			range_and_status = new RangeAndStatus();
			range_and_status.init(getPointerData(data, 0));
			
			
		}
	}

}
