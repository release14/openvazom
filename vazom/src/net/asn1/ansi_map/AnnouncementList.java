package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AnnouncementList extends SEQUENCE{

	public AnnouncementCode get_announcementCode1(){
		return (AnnouncementCode)elements.get(0).data;
	}
	public AnnouncementCode new_announcementCode1(){
		return new AnnouncementCode();
	}

	public AnnouncementCode get_announcementCode2(){
		return (AnnouncementCode)elements.get(1).data;
	}
	public AnnouncementCode new_announcementCode2(){
		return new AnnouncementCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(76, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_announcementCode1(); }});
		elements.add(new ElementDescriptor(76, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_announcementCode2(); }});
	}
	public AnnouncementList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
