package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TerminationAudit extends SEQUENCE{

	public AuditReturnParameter new_child(){
		return new AuditReturnParameter();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public AuditReturnParameter getChild(int index){
		return (AuditReturnParameter)of_children.get(index);
	}
	public TerminationAudit(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
