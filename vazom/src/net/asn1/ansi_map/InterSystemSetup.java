package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemSetup extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(2).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(3).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(4).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(5).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(6).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(7).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(8).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(9).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public ChangeServiceAttributes get_changeServiceAttributes(){
		return (ChangeServiceAttributes)elements.get(10).data;
	}
	public ChangeServiceAttributes new_changeServiceAttributes(){
		return new ChangeServiceAttributes();
	}

	public DataKey get_dataKey(){
		return (DataKey)elements.get(11).data;
	}
	public DataKey new_dataKey(){
		return new DataKey();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(12).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DisplayText2 get_displayText2(){
		return (DisplayText2)elements.get(13).data;
	}
	public DisplayText2 new_displayText2(){
		return new DisplayText2();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(14).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public ISLPInformation get_ilspInformation(){
		return (ISLPInformation)elements.get(15).data;
	}
	public ISLPInformation new_ilspInformation(){
		return new ISLPInformation();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(16).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public RedirectingNumberString get_redirectingNumberString(){
		return (RedirectingNumberString)elements.get(17).data;
	}
	public RedirectingNumberString new_redirectingNumberString(){
		return new RedirectingNumberString();
	}

	public RedirectingSubaddress get_edirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(18).data;
	}
	public RedirectingSubaddress new_edirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(19).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(20).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public MEID get_meid(){
		return (MEID)elements.get(21).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(214, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_changeServiceAttributes(); }});
		elements.add(new ElementDescriptor(215, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataKey(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(299, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText2(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(217, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ilspInformation(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(101, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberString(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_edirectingSubaddress(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public InterSystemSetup(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
