package net.smsfs.list.lists;

import net.config.FNConfigData;
import net.gpu.cuda.CudaManager;
import net.gpu2.GPUBalancer;
import net.smsfs.list.ListBase;
import net.smsfs.list.ListId;
import net.smsfs.list.ListType;

public class MD5QuarantineList extends ListBase {

	public MD5QuarantineList(int max_size) {
		super(true, max_size);
		list_type = ListType.SMSFS_LIST;
		list_type_id = ListId.MD5_QUARANTINE.toString();
	}

	public void on_add(byte[] data){
		if(data != null){
			if(FNConfigData.gpub_status){
				if(data.length == 16){
					GPUBalancer.update_md5(data, CudaManager.MD5_QUARANTINE_LST);
					
				}
			}
			
			
		}
	}
	public void on_delete(byte[] data){
		if(data != null){
			if(FNConfigData.gpub_status){
				if(data.length == 16){
					GPUBalancer.remove(CudaManager.MD5_QUARANTINE_LST, data);
					
				}
			}
			
		}
	}

}
