package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARE_apdu_application_context_name extends SEQUENCE{

	public OBJECT_IDENTIFIER get_application_context_name(){
		return (OBJECT_IDENTIFIER)elements.get(0).data;
	}
	public OBJECT_IDENTIFIER new_application_context_name(){
		return new OBJECT_IDENTIFIER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_application_context_name(); }});
	}
	public AARE_apdu_application_context_name(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 1;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
