package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SigParameterSequenceOf extends SEQUENCE{

	public SigParameter new_child(){
		return new SigParameter();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SigParameter getChild(int index){
		return (SigParameter)of_children.get(index);
	}
	public SigParameterSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
