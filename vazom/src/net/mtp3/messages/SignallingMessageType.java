package net.mtp3.messages;

import java.util.HashMap;


public enum SignallingMessageType {
	// CHM
	COO(SignallingMessageGroupType.CHM.getId(), 0x01),// COO Changeover-order signal
	COA(SignallingMessageGroupType.CHM.getId(), 0x02),// COA Changeover-acknowledgement signal
	CBD(SignallingMessageGroupType.CHM.getId(), 0x05),// CBD Changeback-declaration signal
	CBA(SignallingMessageGroupType.CHM.getId(), 0x06),// CBA Changeback-acknowledgement signal
	// ECM
	ECO(SignallingMessageGroupType.ECM.getId(), 0x01),// ECO Emergency-changeover-order signal
	ECA(SignallingMessageGroupType.ECM.getId(), 0x02),// ECA Emergency-changeover-acknowledgement signal
	// FCM
	RCT(SignallingMessageGroupType.FCM.getId(), 0x01),// RCT Signalling-route-set-congestion-test signal
	TFC(SignallingMessageGroupType.FCM.getId(), 0x02),// TFC Transfer-controlled signal
	// TFM
	TFP(SignallingMessageGroupType.TFM.getId(), 0x01),// TFP Transfer-prohibited signal
	TFR(SignallingMessageGroupType.TFM.getId(), 0x03),// TFR Transfer-restricted signal (national option)
	TFA(SignallingMessageGroupType.TFM.getId(), 0x05),// TFA Transfer-allowed signal
	// RSM
	RST(SignallingMessageGroupType.RSM.getId(), 0x01),// RST Signalling-route-set-test signal for prohibited destination
	RSR(SignallingMessageGroupType.RSM.getId(), 0x02),// RSR Signalling-route-set-test signal for restricted destination (national option)
	// MIM
	LIN(SignallingMessageGroupType.MIM.getId(), 0x01),// LIN Link inhibit signal
	LUN(SignallingMessageGroupType.MIM.getId(), 0x02),// LUN Link uninhibit signal
	LIA(SignallingMessageGroupType.MIM.getId(), 0x03),// LIA Link inhibit acknowledgement signal
	LUA(SignallingMessageGroupType.MIM.getId(), 0x04),// LUA Link uninhibit acknowledgement signal
	LID(SignallingMessageGroupType.MIM.getId(), 0x05),// LID Link inhibit denied signal
	LFU(SignallingMessageGroupType.MIM.getId(), 0x06),// LFU Link forced uninhibit signal
	LLT(SignallingMessageGroupType.MIM.getId(), 0x07),// LLT Link local inhibit test signal
	LRT(SignallingMessageGroupType.MIM.getId(), 0x08),// LRT Link remote inhibit test signal
	// TRM
	TRA(SignallingMessageGroupType.TRM.getId(), 0x01),// TRA Traffic-restart-allowed signal
	// DLM
	DLC(SignallingMessageGroupType.DLM.getId(), 0x01),// DLC Signalling-data-link-connection-order signal
	CSS(SignallingMessageGroupType.DLM.getId(), 0x02),// CSS Connection-successful signal
	CNS(SignallingMessageGroupType.DLM.getId(), 0x03),// CNS Connection-not-successful signal
	CNP(SignallingMessageGroupType.DLM.getId(), 0x04),// CNP Connection-not-possible signal
	// UFC
	UPU(SignallingMessageGroupType.UFC.getId(), 0x01);// UPU User part unavailable signal
	
	
	private int id;
	private int class_id;
	private static final HashMap<String, SignallingMessageType> lookup = new HashMap<String, SignallingMessageType>();
	static{
		for(SignallingMessageType td : SignallingMessageType.values()){
			lookup.put(td.class_id + ":" + td.id, td);
		}
	}
	public int getId(){ return id; }
	public int getClassId(){ return class_id; }
	public static SignallingMessageType get(int id, int _class_id){ return lookup.get(_class_id + ":" + id); }
	private SignallingMessageType(int _class_id, int _id){ id = _id; class_id = _class_id; }		
}
