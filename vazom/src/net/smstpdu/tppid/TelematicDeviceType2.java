package net.smstpdu.tppid;

import java.util.HashMap;

public enum TelematicDeviceType2 {
	SHORT_MESSAGE_TYPE0(0x00),
	REPLACE_SHORT_MESSAGE_TYPE1(0x01),
	REPLACE_SHORT_MESSAGE_TYPE2(0x02),
	REPLACE_SHORT_MESSAGE_TYPE3(0x03),
	REPLACE_SHORT_MESSAGE_TYPE4(0x04),
	REPLACE_SHORT_MESSAGE_TYPE5(0x05),
	REPLACE_SHORT_MESSAGE_TYPE6(0x06),
	REPLACE_SHORT_MESSAGE_TYPE7(0x07),
	ENHANCED_MESSAGE_SERVICE(0x1E),
	RETURN_CALL_MESSAGE(0x1F),
	ANSI_136_R_DATA(0x3C),
	ME_DATA_DOWNLOAD(0x3D),
	ME_DEPERSONALIZATION_SHORT_MESSAGE(0x3E),
	U_SIM_DATA_DOWNLOAD(0x3F);
	
	// BITS 5 to 0 = 00XXXXXX
	private int id;
	private static final HashMap<Integer, TelematicDeviceType2> lookup = new HashMap<Integer, TelematicDeviceType2>();

	static{
		for(TelematicDeviceType2 td : TelematicDeviceType2.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static TelematicDeviceType2 get(int id){ return lookup.get(id); }
	private TelematicDeviceType2(int _id){ id = _id; }

	
	
}
