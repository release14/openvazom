package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.ei.EventInformationType;
import net.isup.parameters.ei.EventPresentationRestrictedIndicatorType;

public class EventInformation extends ParameterBase {
	public EventInformationType event_information_indicator;
	public EventPresentationRestrictedIndicatorType event_presentation_restricted_indicator;
	
	public EventInformation(){
		type = ParameterType.EVENT_INFORMATION;
		
	}

	public void init(byte[] data) {
		if(data != null){
			event_information_indicator = EventInformationType.get(data[byte_position] & 0x7f);
			event_presentation_restricted_indicator = EventPresentationRestrictedIndicatorType.get(data[byte_position] & 0x80);
		}
	}

}
