package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ABRT_source extends INTEGER{

	public static final int _dialogue_service_user = 0;
	public static final int _dialogue_service_provider = 1;
	public ABRT_source(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
