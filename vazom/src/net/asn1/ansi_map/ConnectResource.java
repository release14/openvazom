package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ConnectResource extends SET{

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(0).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(1).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public RoutingDigits get_outingDigits(){
		return (RoutingDigits)elements.get(2).data;
	}
	public RoutingDigits new_outingDigits(){
		return new RoutingDigits();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_outingDigits(); }});
	}
	public ConnectResource(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
