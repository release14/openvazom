package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class FeatureIndicator extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _user_selective_call_forwarding = 38;
	public FeatureIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
