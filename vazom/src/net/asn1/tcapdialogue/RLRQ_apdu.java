package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RLRQ_apdu extends SEQUENCE{

	public Release_request_reason get_reason(){
		return (Release_request_reason)elements.get(0).data;
	}
	public Release_request_reason new_reason(){
		return new Release_request_reason();
	}

	public User_information get_user_information(){
		return (User_information)elements.get(1).data;
	}
	public User_information new_user_information(){
		return new User_information();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reason(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_user_information(); }});
	}
	public RLRQ_apdu(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 2;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.APPLICATION;
	}
}
