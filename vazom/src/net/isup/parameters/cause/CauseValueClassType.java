package net.isup.parameters.cause;

import java.util.HashMap;

public enum CauseValueClassType {
	NORMAL_EVENT_1(0x00),
	NORMAL_EVENT_2(0x10),
	RESOURCE_UNAVAILBLE(0x20),
	SERVICE_OR_OPTION_UNAVAILABLE(0x30),
	SERVICE_OR_OPTION_NOT_IMPLEMENTED(0x40),
	INVALID_MESSAGE(0x50),
	PROTOCOL_ERROR(0x60),
	INTERWORKING(0x70);
	
	private int id;
	private static final HashMap<Integer, CauseValueClassType> lookup = new HashMap<Integer, CauseValueClassType>();
	static{
		for(CauseValueClassType td :CauseValueClassType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CauseValueClassType get(int id){ return lookup.get(id); }
	private CauseValueClassType(int _id){ id = _id; }	
}
