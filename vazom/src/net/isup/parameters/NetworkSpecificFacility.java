package net.isup.parameters;

import net.isup.ParameterType;

public class NetworkSpecificFacility extends ParameterBase {
	public NetworkSpecificFacility(){
		type = ParameterType.NETWORK_SPECIFIC_FACILITIES;
	}
}
