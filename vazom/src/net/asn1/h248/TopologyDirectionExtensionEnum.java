package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TopologyDirectionExtensionEnum extends ENUMERATED{

	public static final int _onewayexternal = 0;
	public static final int _onewayboth = 1;
	public TopologyDirectionExtensionEnum(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
