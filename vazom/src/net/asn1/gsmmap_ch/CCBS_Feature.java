package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CCBS_Feature extends SEQUENCE{

	public CCBS_Index get_ccbs_Index(){
		return (CCBS_Index)elements.get(0).data;
	}
	public CCBS_Index new_ccbs_Index(){
		return new CCBS_Index();
	}

	public ISDN_AddressString get_b_subscriberNumber(){
		return (ISDN_AddressString)elements.get(1).data;
	}
	public ISDN_AddressString new_b_subscriberNumber(){
		return new ISDN_AddressString();
	}

	public ISDN_SubaddressString get_b_subscriberSubaddress(){
		return (ISDN_SubaddressString)elements.get(2).data;
	}
	public ISDN_SubaddressString new_b_subscriberSubaddress(){
		return new ISDN_SubaddressString();
	}

	public BasicServiceCode get_basicServiceGroup(){
		return (BasicServiceCode)elements.get(3).data;
	}
	public BasicServiceCode new_basicServiceGroup(){
		return new BasicServiceCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ccbs_Index(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_b_subscriberNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_b_subscriberSubaddress(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_basicServiceGroup(); }});
	}
	public CCBS_Feature(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
