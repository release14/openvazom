package net.isup.parameters;

import net.isup.ParameterType;

public class MessageCompatibilityInformation extends ParameterBase {
	public MessageCompatibilityInformation(){
		type = ParameterType.MESSAGE_COMPATIBILITY_INFORMATION;
	}
}
