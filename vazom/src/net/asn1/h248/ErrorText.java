package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ErrorText extends IA5STRING{

	public ErrorText(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 22;
	}
}
