package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class FailureType extends ENUMERATED{

	public static final int _callAbandoned = 1;
	public static final int _resourceDisconnect = 2;
	public static final int _failureAtMSC = 3;
	public static final int _sSFTExpiration = 4;
	public FailureType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
