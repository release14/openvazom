package net.smpp_conn;

import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionType;
import net.smpp.pdu.PDUBase;
import net.vstp.VSTP_SGF_CorrelationPacket;

public class SMPPPayload extends RoutingConnectionPayload {
	public PDUBase smpp_packet;
	public byte[] payload_data;

	public SMPPPayload(){
		type = RoutingConnectionType.SMPP;
	}
	

	public void setParams(Object[] params) {
		try{
			smpp_packet = (PDUBase)params[0];
			payload_data = (byte[])params[1];
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void setParamsFromVSTP_SGF(VSTP_SGF_CorrelationPacket vstp_sgf) {
		smpp_packet = vstp_sgf.smpp_packet.smpp_pdu;
		payload_data = vstp_sgf.smpp_packet_data;
		callback_required = vstp_sgf.callback_required;
		callback = vstp_sgf.callback;
		extra_params = vstp_sgf.extra_params;

	}

}
