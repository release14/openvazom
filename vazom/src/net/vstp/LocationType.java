package net.vstp;

public enum LocationType {
	SGN, // Signaling Node
	FN, // Filter node
	GPUN, // GPU node
	NA; // Not available
}
