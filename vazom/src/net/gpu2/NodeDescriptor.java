package net.gpu2;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.UUID;

import net.config.FNConfigData;
import net.ds.DataSourceType;
import net.gpu.cuda.CudaManager;
import net.logging.LoggingManager;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class NodeDescriptor {
	private static Logger logger=Logger.getLogger(NodeDescriptor.class);
	public String id;
	public String host;
	public int port;
	public int conn_count;
	public ArrayList<NodeType> type_lst;
	public ArrayList<ConnectionDescriptor> connections;

	public synchronized void closeConnections(){
		for(int i = 0; i<connections.size(); i++){
			SSctp.shutdownClient(connections.get(i).sctp_id);
		}
	}
	public int checkMD5(byte[] hash, ConnectionDescriptor cd, int lst_id){
		int res = 0;
		String reply = null;
		long ts;
		MessageDescriptor md = null;
		UUID uuid = UUID.randomUUID();
		int sctp_res;
		SSCTPDescriptor sd = null;
		try{
			StatsManager.GPUB_STATS.MD5_REQUEST_COUNT++;
			ts = System.currentTimeMillis();
			md = new MessageDescriptor();
			md.header.destination = LocationType.GPUN;
			md.header.destination_id = id;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = uuid.toString();
			md.header.msg_type = MessageType.SGC;
			md.header.source = LocationType.FN;
			md.header.source_id = FNConfigData.fn_id;
			md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.MD5.toString());
			md.values.put(VSTPDataItemType.GPU_PARAM.getId(), lst_id + ":" + Utils.bytes2hexstr(hash, ""));
			// send
			sctp_res = SSctp.send(cd.sctp_id, md.encode(), 0);
			if(sctp_res < 0) throw new ConnectException();
			// receive
			sd = SSctp.receive(cd.sctp_id);
			if(sd != null){
				if(sd.payload != null){
					md = VSTP.decode(sd.payload);
					// check vstp type
					if(md.header.source == LocationType.GPUN && md.header.msg_type == MessageType.SGC){
						// check if msg id is valid
						if(md.header.msg_id.equals(uuid.toString())){
							// vstp valid
							reply = md.values.get(VSTPDataItemType.GPU_RESULT.getId());
							if(reply != null){
								if(!reply.equalsIgnoreCase("ERROR") && !reply.equalsIgnoreCase("CERR") && !reply.equalsIgnoreCase("ERR110")){
									StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
									StatsManager.GPUB_STATS.md5_setMaxTS(StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME);
									
									res = Integer.parseInt(reply);
									StatsManager.GPUB_STATS.MD5_REPLY_OK_COUNT++;
									return res;
								// check for errors
								}else{
									StatsManager.GPUB_STATS.MD5_REPLY_ERROR_COUNT++;
									if(reply.equalsIgnoreCase("CERR")){
										LoggingManager.error(logger, "MD5 Node [" + id + "] CUDA Error!");
									}else if(reply.equalsIgnoreCase("ERR110")){
										LoggingManager.error(logger, "MD5 Node [" + id + "] Unknown List ID Error!");
										
									}else{
										LoggingManager.error(logger, "MD5 Node [" + id + "] Unknown Error!");
										
									}
								}								
							}
						}else{
							LoggingManager.warn(logger, "Unknown VSTP message, msg type = [" + md.header.msg_type + "], source = [" + md.header.source_id + "], msg id = [" + md.header.msg_id + "]!");
						}
					}else{
						LoggingManager.warn(logger, "Unsupported VSTP message, msg type = [" + md.header.msg_type + "], source = [" + md.header.source_id + "], msg id = [" + md.header.msg_id + "]!");
						
					}
				}else throw new ConnectException();
			}else throw new ConnectException();
	
		}catch(ConnectException e){
			StatsManager.GPUB_STATS.MD5_CONNECTION_DOWN_COUNT++;
			LoggingManager.error(logger, "Connection error while invoking MD5, lst id = [" + lst_id + "]!");
			cd.available = false;
			reconnect(cd);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return 0;
		
	}

	
	public int checkLD(int lst_id, String data, ConnectionDescriptor cd){
		int res = 0;
		String reply = null;
		long ts;
		MessageDescriptor md = null;
		UUID uuid = UUID.randomUUID();
		int sctp_res;
		SSCTPDescriptor sd = null;

		try{
			StatsManager.GPUB_STATS.LD_REQUEST_COUNT++;
			ts = System.currentTimeMillis();
			md = new MessageDescriptor();
			md.header.destination = LocationType.GPUN;
			md.header.destination_id = id;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = uuid.toString();
			md.header.msg_type = MessageType.SGC;
			md.header.source = LocationType.FN;
			md.header.source_id = FNConfigData.fn_id;
			md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.LD.toString());
			md.values.put(VSTPDataItemType.GPU_PARAM.getId(), lst_id + ":" + Utils.bytes2hexstr(data.getBytes(), ""));
			// send
			sctp_res = SSctp.send(cd.sctp_id, md.encode(), 0);
			if(sctp_res < 0) throw new ConnectException();
			// receive
			sd = SSctp.receive(cd.sctp_id);
			if(sd != null){
				if(sd.payload != null){
					md = VSTP.decode(sd.payload);
					// check vstp type
					if(md.header.source == LocationType.GPUN && md.header.msg_type == MessageType.SGC){
						// check if msg id is valid
						if(md.header.msg_id.equals(uuid.toString())){
							// vstp valid
							reply = md.values.get(VSTPDataItemType.GPU_RESULT.getId());
							if(reply != null){
								if(!reply.equalsIgnoreCase("ERROR") && !reply.equalsIgnoreCase("CERR") && !reply.equalsIgnoreCase("ERR110")){
									StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
									StatsManager.GPUB_STATS.ld_setMaxTS(StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME);
									
									res = Integer.parseInt(reply);
									StatsManager.GPUB_STATS.LD_REPLY_OK_COUNT++;
									return res;
								// check for errors
								}else{
									StatsManager.GPUB_STATS.LD_REPLY_ERROR_COUNT++;
									if(reply.equalsIgnoreCase("CERR")){
										LoggingManager.error(logger, "GPU Node [" + id + "] CUDA Error!");
									}else if(reply.equalsIgnoreCase("ERR110")){
										LoggingManager.error(logger, "GPU Node [" + id + "] Unknown List ID Error!");
										
									}else{
										LoggingManager.error(logger, "GPU Node [" + id + "] Unknown Error!");
										
									}
								}								
							}
						}
					}
					
				}else throw new ConnectException();
			}else throw new ConnectException();
			

		}catch(ConnectException e){
			StatsManager.GPUB_STATS.LD_CONNECTION_DOWN_COUNT++;
			LoggingManager.error(logger, "Connection error while invoking LD, lst id = [" + lst_id + "]!");
			cd.available = false;
			reconnect(cd);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
		
	}
	public void reconnect(ConnectionDescriptor cd){
		try{
			LoggingManager.info(logger, "GPU Node [" + id + "], [" + cd.type + "] connection down, trying to reconnect!");
			SSctp.shutdownClient(cd.sctp_id);
			cd.sctp_id = SSctp.initClient(host, port, 16, null, 0);
			if(cd.sctp_id > 0){
				if(connectNode(cd.sctp_id)){
					cd.available = true;
					LoggingManager.info(logger, "GPU Node [" + id + "], connection ready!");
				}
			}else LoggingManager.error(logger, "GPU Node [" + id + "], connection down, error while starting SCTP connection!");
		}catch(Exception e){
			LoggingManager.error(logger, "GPU Node [" + id + "], connection down, error while reconnecting!");
		}
		
	}
	private void update_ld(int lst_id, String data){
		String reply = null;
		long ts;
		MessageDescriptor md = null;
		UUID uuid = UUID.randomUUID();
		int sctp_res;
		SSCTPDescriptor sd = null;

		try{
			StatsManager.GPUB_STATS.UPDATE_REQUEST_COUNT++;
			ts = System.currentTimeMillis();
			md = new MessageDescriptor();
			md.header.destination = LocationType.GPUN;
			md.header.destination_id = id;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = uuid.toString();
			md.header.msg_type = MessageType.SGC;
			md.header.source = LocationType.FN;
			md.header.source_id = FNConfigData.fn_id;
			md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.U.toString());
			md.values.put(VSTPDataItemType.GPU_PARAM.getId(), lst_id + ":" + Utils.bytes2hexstr(data.getBytes(), ""));
			// send
			sctp_res = SSctp.send(connections.get(0).sctp_id, md.encode(), 0);
			if(sctp_res < 0) throw new ConnectException();
			// receive
			sd = SSctp.receive(connections.get(0).sctp_id);
			if(sd != null){
				if(sd.payload != null){
					md = VSTP.decode(sd.payload);
					// check vstp type
					if(md.header.source == LocationType.GPUN && md.header.msg_type == MessageType.SGC){
						// check if msg id is valid
						if(md.header.msg_id.equals(uuid.toString())){
							// vstp valid
							reply = md.values.get(VSTPDataItemType.GPU_RESULT.getId());
							if(reply != null){
								StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
								StatsManager.GPUB_STATS.update_setMaxTS(StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME);
								StatsManager.GPUB_STATS.UPDATE_REPLY_OK_COUNT++;
								
							}
						}
					}
				}else throw new ConnectException();
			}else throw new ConnectException();
		}catch(ConnectException e){
			StatsManager.GPUB_STATS.UPDATE_CONNECTION_DOWN_COUNT++;
			LoggingManager.error(logger, "Connection error while updating GPU Node [" + id + "]!");
			connections.get(0).available = false;
			reconnect(connections.get(0));
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
	}
	private void update_md5(byte[] hash, int lst_id){
		String reply = null;
		long ts;
		MessageDescriptor md = null;
		UUID uuid = UUID.randomUUID();
		int sctp_res;
		SSCTPDescriptor sd = null;
		try{
			StatsManager.GPUB_STATS.MD5_UPDATE_REQUEST_COUNT++;
			ts = System.currentTimeMillis();
			md = new MessageDescriptor();
			md.header.destination = LocationType.GPUN;
			md.header.destination_id = id;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = uuid.toString();
			md.header.msg_type = MessageType.SGC;
			md.header.source = LocationType.FN;
			md.header.source_id = FNConfigData.fn_id;
			md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.U.toString());
			md.values.put(VSTPDataItemType.GPU_PARAM.getId(), lst_id + ":" + Utils.bytes2hexstr(hash, ""));
			// send
			sctp_res = SSctp.send(connections.get(0).sctp_id, md.encode(), 0);
			if(sctp_res < 0) throw new ConnectException();
			// receive
			sd = SSctp.receive(connections.get(0).sctp_id);
			if(sd != null){
				if(sd.payload != null){
					md = VSTP.decode(sd.payload);
					// check vstp type
					if(md.header.source == LocationType.GPUN && md.header.msg_type == MessageType.SGC){
						// check if msg id is valid
						if(md.header.msg_id.equals(uuid.toString())){
							// vstp valid
							reply = md.values.get(VSTPDataItemType.GPU_RESULT.getId());
							if(reply != null){
								StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
								StatsManager.GPUB_STATS.md5_update_setMaxTS(StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME);
								StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_OK_COUNT++;
							}
						}
					}
				}else throw new ConnectException();
			}else throw new ConnectException();

		}catch(ConnectException e){
			StatsManager.GPUB_STATS.MD5_UPDATE_CONNECTION_DOWN_COUNT++;
			LoggingManager.error(logger, "Connection error while updating GPU Node [" + id + "]!");
			connections.get(0).available = false;
			reconnect(connections.get(0));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void reset(int lst_id){
		String reply = null;
		MessageDescriptor md = null;
		UUID uuid = UUID.randomUUID();
		int sctp_res;
		SSCTPDescriptor sd = null;

		try{
			md = new MessageDescriptor();
			md.header.destination = LocationType.GPUN;
			md.header.destination_id = id;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = uuid.toString();
			md.header.msg_type = MessageType.SGC;
			md.header.source = LocationType.FN;
			md.header.source_id = FNConfigData.fn_id;
			md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.RST.toString());
			md.values.put(VSTPDataItemType.GPU_PARAM.getId(), Integer.toString(lst_id));
			// send
			sctp_res = SSctp.send(connections.get(0).sctp_id, md.encode(), 0);
			if(sctp_res < 0) throw new ConnectException();
			// receive
			sd = SSctp.receive(connections.get(0).sctp_id);
			if(sd != null){
				if(sd.payload != null){
					md = VSTP.decode(sd.payload);
					// check vstp type
					if(md.header.source == LocationType.GPUN && md.header.msg_type == MessageType.SGC){
						// check if msg id is valid
						if(md.header.msg_id.equals(uuid.toString())){
							// vstp valid
							reply = md.values.get(VSTPDataItemType.GPU_RESULT.getId());
						}
					}
				}else throw new ConnectException();
			}else throw new ConnectException();

		}catch(ConnectException e){
			//StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
			LoggingManager.error(logger, "Error while invoking RESET...");
			connections.get(0).available = false;
			reconnect(connections.get(0));
		}catch(Exception e){
			e.printStackTrace();
		}

	}	
	
	public void remove(int lst_id, byte[] data){
		String reply = null;
		MessageDescriptor md = null;
		UUID uuid = UUID.randomUUID();
		int sctp_res;
		SSCTPDescriptor sd = null;

		try{
			md = new MessageDescriptor();
			md.header.destination = LocationType.GPUN;
			md.header.destination_id = id;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = uuid.toString();
			md.header.msg_type = MessageType.SGC;
			md.header.source = LocationType.FN;
			md.header.source_id = FNConfigData.fn_id;
			md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.R.toString());
			md.values.put(VSTPDataItemType.GPU_PARAM.getId(), lst_id + ":"  + Utils.bytes2hexstr(data, ""));
			// send
			sctp_res = SSctp.send(connections.get(0).sctp_id, md.encode(), 0);
			if(sctp_res < 0) throw new ConnectException();
			// receive
			sd = SSctp.receive(connections.get(0).sctp_id);
			if(sd != null){
				if(sd.payload != null){
					md = VSTP.decode(sd.payload);
					// check vstp type
					if(md.header.source == LocationType.GPUN && md.header.msg_type == MessageType.SGC){
						// check if msg id is valid
						if(md.header.msg_id.equals(uuid.toString())){
							// vstp valid
							reply = md.values.get(VSTPDataItemType.GPU_RESULT.getId());
							if(reply != null){
								if(!reply.equalsIgnoreCase("ERROR") && !reply.equalsIgnoreCase("CERR") && !reply.equalsIgnoreCase("ERR110")){
									StatsManager.GPUB_STATS.LST_REMOVE_COUNT++;
								// check for errors
								}else{
									StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
									if(reply.equalsIgnoreCase("CERR")){
										LoggingManager.error(logger, "Node [" + id + "] CUDA Error!");
									}else if(reply.equalsIgnoreCase("ERR110")){
										LoggingManager.error(logger, "Node [" + id + "] Unknown List ID Error!");
										
									}else{
										LoggingManager.error(logger, "Node [" + id + "] Unknown Error!");
										
									}
								}								
							}
						}
					}
				}else throw new ConnectException();
			}else throw new ConnectException();

		}catch(ConnectException e){
			StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
			LoggingManager.error(logger, "Error while invoking REMOVE...");
			connections.get(0).available = false;
			reconnect(connections.get(0));
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	public boolean typeExists(NodeType type){
		for(int i = 0; i<type_lst.size(); i++) if(type_lst.get(i) == type) return true;
		return false;
	}
	public void update(int lst_id, GPUDataPacket data){
		if(lst_id == CudaManager.MD5_QUARANTINE_LST || lst_id == CudaManager.MD5_NORMAL_LST) update_md5(data.hash_data, lst_id);
		else update_ld(lst_id, data.data);
	}

	public void addType(NodeType type){
		type_lst.add(type);
	}
	
	
	public static boolean connectNode(int sctp_id){
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		if(sctp_id > -1){
			LoggingManager.info(logger, "Negotiating VSTP Client Connection, client id = [" + sctp_id + "]!");
			// negotiate VSTP connect
			res = SSctp.send(sctp_id, VSTP.generate_init(LocationType.GPUN, FNConfigData.fn_id).getBytes(), 0);
			if(res > -1){
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						if(VSTP.check_accept(new String(sd.payload))){
							LoggingManager.info(logger, "VSTP Client Connection initialized, sctp id = [" + sctp_id + "]!");
							vstp_ready = true;
						}
					}
				}
				if(!vstp_ready){
					LoggingManager.warn(logger, "Error while negotiating VSTP Client Connection, sctp id =  [" + sctp_id + "]!");
					SSctp.shutdownClient(sctp_id);
					return false;
					
				}else return true;
			}else{
				LoggingManager.warn(logger, "Error while negotiating VSTP Client Connection, sctp id = [" + sctp_id + "]!");
				SSctp.shutdownClient(sctp_id);
				return false;
			}
		}else{
			LoggingManager.warn(logger, "Error while initializing SCTP Connection, sctp id = [" + sctp_id + "]!");
			SSctp.shutdownClient(sctp_id);
			return false;
		}
	}	
	
	public NodeDescriptor(String _id, String _host, int _port, int _conn_count){
		ConnectionDescriptor cd = null;
		try{
			type_lst = new ArrayList<NodeType>();
			id = _id;
			host = _host;
			port = _port;
			conn_count = _conn_count;
			connections = new ArrayList<ConnectionDescriptor>();
			LoggingManager.info(logger, "GPU Node [" + id + "], connecting!");
			// UPDATER connection
			cd = new ConnectionDescriptor(ConnectionType.UPDATER);
			cd.sctp_id = SSctp.initClient(host, port, 16, null, 0);
			if(connectNode(cd.sctp_id)) LoggingManager.info(logger, "GPU Node [" + id + "], [" + cd.type + "] connection ready!");
			// add to list
			connections.add(cd);

			// KERNEL connections
			for(int i = 0; i<conn_count; i++){
				cd = new ConnectionDescriptor(ConnectionType.KERNEL);
				cd.sctp_id = SSctp.initClient(host, port, 16, null, 0);
				if(connectNode(cd.sctp_id)) LoggingManager.info(logger, "GPU Node [" + id + "], [" + cd.type + "] connection [" + i + "] ready!");
				// add to list
				cd.node = this;
				connections.add(cd);
			
			}
			LoggingManager.info(logger, "GPU Node [" + id + "], ready!");
		}catch(Exception e){
			LoggingManager.error(logger, "GPU Node [" + id + "] not available!");
		}
		
	}
}
