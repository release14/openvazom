package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Ms_validity extends OptionalParameterBase {
	public enum ValueType{
		STORE_INDEFINITELY(0x00),
		POWER_DOWN(0x01),
		SID_BASED_REGISTRATION_AREA(0x02),
		DISPLAY_ONLY(0x03);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public Ms_validity(){
		type = OptionalParameterType.MS_VALIDITY;
	}
	public ValueType value;
	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);

	}

}
