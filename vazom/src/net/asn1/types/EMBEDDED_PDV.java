package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class EMBEDDED_PDV extends ASNType {

	public EMBEDDED_PDV(){
		super();
		universalTag = ASNTag.EMBEDDED_PDV;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Constructed;
	}
}
