package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuditDescriptor extends SEQUENCE{

	public AuditTokenType get_auditToken(){
		return (AuditTokenType)elements.get(0).data;
	}
	public AuditTokenType new_auditToken(){
		return new AuditTokenType();
	}

	public IndAuditParameterSequenceOf get_auditPropertyToken(){
		return (IndAuditParameterSequenceOf)elements.get(1).data;
	}
	public IndAuditParameterSequenceOf new_auditPropertyToken(){
		return new IndAuditParameterSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_auditToken(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_auditPropertyToken(); }});
	}
	public AuditDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
