package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Vertical_Velocity extends OCTET_STRING{

	public Vertical_Velocity(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
