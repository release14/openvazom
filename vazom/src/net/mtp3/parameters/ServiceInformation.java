package net.mtp3.parameters;

import net.mtp3.types.NetworkIndicatorType;
import net.mtp3.types.ServiceIndicatorType;

public class ServiceInformation {
	public NetworkIndicatorType networkIndicator;
	public ServiceIndicatorType serviceIndicator;
}
