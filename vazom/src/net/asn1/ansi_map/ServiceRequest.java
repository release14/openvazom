package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceRequest extends SET{

	public ServiceID get_serviceID(){
		return (ServiceID)elements.get(0).data;
	}
	public ServiceID new_serviceID(){
		return new ServiceID();
	}

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(1).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public ACGEncountered get_acgencountered(){
		return (ACGEncountered)elements.get(2).data;
	}
	public ACGEncountered new_acgencountered(){
		return new ACGEncountered();
	}

	public AvailabilityType get_availabilityType(){
		return (AvailabilityType)elements.get(3).data;
	}
	public AvailabilityType new_availabilityType(){
		return new AvailabilityType();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(4).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(5).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(6).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(7).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(8).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(9).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public ConditionallyDeniedReason get_conditionallyDeniedReason(){
		return (ConditionallyDeniedReason)elements.get(10).data;
	}
	public ConditionallyDeniedReason new_conditionallyDeniedReason(){
		return new ConditionallyDeniedReason();
	}

	public DataAccessElementList get_dataAccessElementList(){
		return (DataAccessElementList)elements.get(11).data;
	}
	public DataAccessElementList new_dataAccessElementList(){
		return new DataAccessElementList();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(12).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public Digits get_digits(){
		return (Digits)elements.get(13).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public DMH_RedirectionIndicator get_dmh_RedirectionIndicator(){
		return (DMH_RedirectionIndicator)elements.get(14).data;
	}
	public DMH_RedirectionIndicator new_dmh_RedirectionIndicator(){
		return new DMH_RedirectionIndicator();
	}

	public DMH_ServiceID get_dmh_ServiceID(){
		return (DMH_ServiceID)elements.get(15).data;
	}
	public DMH_ServiceID new_dmh_ServiceID(){
		return new DMH_ServiceID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(16).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public ExtendedMSCID get_extendedMSCID(){
		return (ExtendedMSCID)elements.get(17).data;
	}
	public ExtendedMSCID new_extendedMSCID(){
		return new ExtendedMSCID();
	}

	public FeatureIndicator get_featureIndicator(){
		return (FeatureIndicator)elements.get(18).data;
	}
	public FeatureIndicator new_featureIndicator(){
		return new FeatureIndicator();
	}

	public GroupInformation get_groupInformation(){
		return (GroupInformation)elements.get(19).data;
	}
	public GroupInformation new_groupInformation(){
		return new GroupInformation();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(20).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(21).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(22).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(23).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(24).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(25).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(26).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public PilotBillingID get_pilotBillingID(){
		return (PilotBillingID)elements.get(27).data;
	}
	public PilotBillingID new_pilotBillingID(){
		return new PilotBillingID();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(28).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(29).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingPartyName get_redirectingPartyName(){
		return (RedirectingPartyName)elements.get(30).data;
	}
	public RedirectingPartyName new_redirectingPartyName(){
		return new RedirectingPartyName();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(31).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(32).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public RedirectionReason get_redirectionReason(){
		return (RedirectionReason)elements.get(33).data;
	}
	public RedirectionReason new_redirectionReason(){
		return new RedirectionReason();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(34).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(35).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(36).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(37).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public TerminationAccessType get_terminationAccessType(){
		return (TerminationAccessType)elements.get(38).data;
	}
	public TerminationAccessType new_terminationAccessType(){
		return new TerminationAccessType();
	}

	public TimeDateOffset get_timeDateOffset(){
		return (TimeDateOffset)elements.get(39).data;
	}
	public TimeDateOffset new_timeDateOffset(){
		return new TimeDateOffset();
	}

	public TimeOfDay get_timeOfDay(){
		return (TimeOfDay)elements.get(40).data;
	}
	public TimeOfDay new_timeOfDay(){
		return new TimeOfDay();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(41).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public TriggerType get_triggerType(){
		return (TriggerType)elements.get(42).data;
	}
	public TriggerType new_triggerType(){
		return new TriggerType();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(43).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(246, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceID(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(340, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_acgencountered(); }});
		elements.add(new ElementDescriptor(90, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_availabilityType(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(162, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_conditionallyDeniedReason(); }});
		elements.add(new ElementDescriptor(250, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataAccessElementList(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(88, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_RedirectionIndicator(); }});
		elements.add(new ElementDescriptor(305, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_ServiceID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(53, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedMSCID(); }});
		elements.add(new ElementDescriptor(306, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_featureIndicator(); }});
		elements.add(new ElementDescriptor(163, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_groupInformation(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(169, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotBillingID(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(245, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingPartyName(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(19, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectionReason(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(119, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationAccessType(); }});
		elements.add(new ElementDescriptor(275, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeDateOffset(); }});
		elements.add(new ElementDescriptor(309, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeOfDay(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(279, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerType(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_winCapability(); }});
	}
	public ServiceRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
