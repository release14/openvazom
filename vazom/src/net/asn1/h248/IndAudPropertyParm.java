package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudPropertyParm extends SEQUENCE{

	public PkgdName get_name(){
		return (PkgdName)elements.get(0).data;
	}
	public PkgdName new_name(){
		return new PkgdName();
	}

	public PropertyParm get_propertyParms(){
		return (PropertyParm)elements.get(1).data;
	}
	public PropertyParm new_propertyParms(){
		return new PropertyParm();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_name(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_propertyParms(); }});
	}
	public IndAudPropertyParm(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
