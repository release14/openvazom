package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DefaultCallHandling extends ENUMERATED{

	public static final int _continueCall = 0;
	public static final int _releaseCall = 1;
	public DefaultCallHandling(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
