package net.isup.parameters;

import net.isup.ParameterType;

public class QueryOnReleaseCapability extends ParameterBase {
	public QueryOnReleaseCapability(){
		type = ParameterType.QUERY_ON_RELEASE_CAPABILITY;
	}
}
