package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudSignal extends SEQUENCE{

	public PkgdName get_signalName(){
		return (PkgdName)elements.get(0).data;
	}
	public PkgdName new_signalName(){
		return new PkgdName();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(1).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public RequestID get_signalRequestID(){
		return (RequestID)elements.get(2).data;
	}
	public RequestID new_signalRequestID(){
		return new RequestID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_signalRequestID(); }});
	}
	public IndAudSignal(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
