package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceDataAccessElementList extends SEQUENCE{

	public ServiceDataAccessElementType new_child(){
		return new ServiceDataAccessElementType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ServiceDataAccessElementType getChild(int index){
		return (ServiceDataAccessElementType)of_children.get(index);
	}
	public ServiceDataAccessElementList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
