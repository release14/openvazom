package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ObjectIDApplicationContext extends OBJECT_IDENTIFIER{

	public ObjectIDApplicationContext(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 28;
		asn_class = ASNTagClass.PRIVATE;
	}
}
