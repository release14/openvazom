package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DigitMapValue extends SEQUENCE{

	public INTEGER get_startTimer(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_startTimer(){
		return new INTEGER();
	}

	public INTEGER get_shortTimer(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_shortTimer(){
		return new INTEGER();
	}

	public INTEGER get_longTimer(){
		return (INTEGER)elements.get(2).data;
	}
	public INTEGER new_longTimer(){
		return new INTEGER();
	}

	public IA5STRING get_digitMapBody(){
		return (IA5STRING)elements.get(3).data;
	}
	public IA5STRING new_digitMapBody(){
		return new IA5STRING();
	}

	public INTEGER get_durationTimer(){
		return (INTEGER)elements.get(4).data;
	}
	public INTEGER new_durationTimer(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_startTimer(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_shortTimer(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_longTimer(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digitMapBody(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_durationTimer(); }});
	}
	public DigitMapValue(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
