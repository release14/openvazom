package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DialoguePDU extends CHOICE{

	public AARQ_apdu get_dialogueRequest(){
		return (AARQ_apdu)elements.get(0).data;
	}
	public AARQ_apdu new_dialogueRequest(){
		return new AARQ_apdu();
	}

	public AARE_apdu get_dialogueResponse(){
		return (AARE_apdu)elements.get(1).data;
	}
	public AARE_apdu new_dialogueResponse(){
		return new AARE_apdu();
	}

	public ABRT_apdu get_dialogueAbort(){
		return (ABRT_apdu)elements.get(2).data;
	}
	public ABRT_apdu new_dialogueAbort(){
		return new ABRT_apdu();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_dialogueRequest(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_dialogueResponse(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_dialogueAbort(); }});
	}
	public DialoguePDU(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
