package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARE_apdu_result_source_diagnostic extends SEQUENCE{

	public Associate_source_diagnostic get_result_source_diagnostic(){
		return (Associate_source_diagnostic)elements.get(0).data;
	}
	public Associate_source_diagnostic new_result_source_diagnostic(){
		return new Associate_source_diagnostic();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_result_source_diagnostic(); }});
	}
	public AARE_apdu_result_source_diagnostic(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
