package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SystemMyTypeCode extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _eDS =  1 ;
	public static final int _astronet =  2 ;
	public static final int _lucent_Technologies =  3 ;
	public static final int _ericsson =  4 ;
	public static final int _gTE =  5 ;
	public static final int _motorola =  6 ;
	public static final int _nEC =  7 ;
	public static final int _nORTEL =  8 ;
	public static final int _novAtel =  9 ;
	public static final int _plexsys =  10 ;
	public static final int _digital_Equipment_Corp =  11 ;
	public static final int _iNET =  12 ;
	public static final int _bellcore =  13 ;
	public static final int _alcatel_SEL =  14 ;
	public static final int _compaq =  15 ;
	public static final int _qUALCOMM =  16 ;
	public static final int _aldiscon =  17 ;
	public static final int _celcore =  18 ;
	public static final int _tELOS =  19 ;
	public static final int _aDI_Limited =  20 ;
	public static final int _coral_Systems =  21 ;
	public static final int _synacom_Technology =  22 ;
	public static final int _dSC =  23 ;
	public static final int _mCI =  24 ;
	public static final int _newNet =  25 ;
	public static final int _sema_Group_Telecoms =  26 ;
	public static final int _lG_Information_and_Communications =  27 ;
	public static final int _cBIS =  28 ;
	public static final int _siemens =  29 ;
	public static final int _samsung_Electronics =  30 ;
	public static final int _readyCom_Inc =  31 ;
	public static final int _aG_Communication_Systems =  32 ;
	public static final int _hughes_Network_Systems =  33 ;
	public static final int _phoenix_Wireless_Group =  34 ;
	public SystemMyTypeCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
