package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InformServiceCentreArg extends SEQUENCE{

	public ISDN_AddressString get_storedMSISDN(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_storedMSISDN(){
		return new ISDN_AddressString();
	}

	public MW_Status get_mw_Status(){
		return (MW_Status)elements.get(1).data;
	}
	public MW_Status new_mw_Status(){
		return new MW_Status();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public AbsentSubscriberDiagnosticSM get_absentSubscriberDiagnosticSM(){
		return (AbsentSubscriberDiagnosticSM)elements.get(3).data;
	}
	public AbsentSubscriberDiagnosticSM new_absentSubscriberDiagnosticSM(){
		return new AbsentSubscriberDiagnosticSM();
	}

	public AbsentSubscriberDiagnosticSM get_additionalAbsentSubscriberDiagnosticSM(){
		return (AbsentSubscriberDiagnosticSM)elements.get(4).data;
	}
	public AbsentSubscriberDiagnosticSM new_additionalAbsentSubscriberDiagnosticSM(){
		return new AbsentSubscriberDiagnosticSM();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_storedMSISDN(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_mw_Status(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, true, true){public void set(){ data = new_absentSubscriberDiagnosticSM(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additionalAbsentSubscriberDiagnosticSM(); }});
	}
	public InformServiceCentreArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
