package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSDeliveryBackward extends SET{

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(0).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(1).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public SMS_BearerData get_sms_BearerData(){
		return (SMS_BearerData)elements.get(2).data;
	}
	public SMS_BearerData new_sms_BearerData(){
		return new SMS_BearerData();
	}

	public SMS_TeleserviceIdentifier get_sms_TeleserviceIdentifier(){
		return (SMS_TeleserviceIdentifier)elements.get(3).data;
	}
	public SMS_TeleserviceIdentifier new_sms_TeleserviceIdentifier(){
		return new SMS_TeleserviceIdentifier();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(4).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public SMS_ChargeIndicator get_sms_ChargeIndicator(){
		return (SMS_ChargeIndicator)elements.get(5).data;
	}
	public SMS_ChargeIndicator new_sms_ChargeIndicator(){
		return new SMS_ChargeIndicator();
	}

	public SMS_DestinationAddress get_sms_DestinationAddress(){
		return (SMS_DestinationAddress)elements.get(6).data;
	}
	public SMS_DestinationAddress new_sms_DestinationAddress(){
		return new SMS_DestinationAddress();
	}

	public SMS_OriginalDestinationAddress get_sms_OriginalDestinationAddress(){
		return (SMS_OriginalDestinationAddress)elements.get(7).data;
	}
	public SMS_OriginalDestinationAddress new_sms_OriginalDestinationAddress(){
		return new SMS_OriginalDestinationAddress();
	}

	public SMS_OriginalDestinationSubaddress get_sms_OriginalDestinationSubaddress(){
		return (SMS_OriginalDestinationSubaddress)elements.get(8).data;
	}
	public SMS_OriginalDestinationSubaddress new_sms_OriginalDestinationSubaddress(){
		return new SMS_OriginalDestinationSubaddress();
	}

	public SMS_OriginalOriginatingAddress get_sms_OriginalOriginatingAddress(){
		return (SMS_OriginalOriginatingAddress)elements.get(9).data;
	}
	public SMS_OriginalOriginatingAddress new_sms_OriginalOriginatingAddress(){
		return new SMS_OriginalOriginatingAddress();
	}

	public SMS_OriginalOriginatingSubaddress get_sms_OriginalOriginatingSubaddress(){
		return (SMS_OriginalOriginatingSubaddress)elements.get(10).data;
	}
	public SMS_OriginalOriginatingSubaddress new_sms_OriginalOriginatingSubaddress(){
		return new SMS_OriginalOriginatingSubaddress();
	}

	public SMS_OriginatingAddress get_sms_OriginatingAddress(){
		return (SMS_OriginatingAddress)elements.get(11).data;
	}
	public SMS_OriginatingAddress new_sms_OriginatingAddress(){
		return new SMS_OriginatingAddress();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(105, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sms_BearerData(); }});
		elements.add(new ElementDescriptor(116, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sms_TeleserviceIdentifier(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(106, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_ChargeIndicator(); }});
		elements.add(new ElementDescriptor(107, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_DestinationAddress(); }});
		elements.add(new ElementDescriptor(110, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalDestinationAddress(); }});
		elements.add(new ElementDescriptor(111, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalDestinationSubaddress(); }});
		elements.add(new ElementDescriptor(112, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalOriginatingAddress(); }});
		elements.add(new ElementDescriptor(113, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalOriginatingSubaddress(); }});
		elements.add(new ElementDescriptor(114, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginatingAddress(); }});
	}
	public SMSDeliveryBackward(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
