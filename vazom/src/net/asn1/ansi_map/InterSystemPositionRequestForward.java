package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemPositionRequestForward extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(1).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public PositionRequestType get_positionRequestType(){
		return (PositionRequestType)elements.get(2).data;
	}
	public PositionRequestType new_positionRequestType(){
		return new PositionRequestType();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(3).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LCSBillingID get_lcsBillingID(){
		return (LCSBillingID)elements.get(4).data;
	}
	public LCSBillingID new_lcsBillingID(){
		return new LCSBillingID();
	}

	public LCS_Client_ID get_lcs_Client_ID(){
		return (LCS_Client_ID)elements.get(5).data;
	}
	public LCS_Client_ID new_lcs_Client_ID(){
		return new LCS_Client_ID();
	}

	public MEID get_meid(){
		return (MEID)elements.get(6).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(7).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MobilePositionCapability get_mobilePositionCapability(){
		return (MobilePositionCapability)elements.get(8).data;
	}
	public MobilePositionCapability new_mobilePositionCapability(){
		return new MobilePositionCapability();
	}

	public MPCID get_mpcid(){
		return (MPCID)elements.get(9).data;
	}
	public MPCID new_mpcid(){
		return new MPCID();
	}

	public PQOS_HorizontalPosition get_pqos_HorizontalPosition(){
		return (PQOS_HorizontalPosition)elements.get(10).data;
	}
	public PQOS_HorizontalPosition new_pqos_HorizontalPosition(){
		return new PQOS_HorizontalPosition();
	}

	public PQOS_HorizontalVelocity get_pqos_HorizontalVelocity(){
		return (PQOS_HorizontalVelocity)elements.get(11).data;
	}
	public PQOS_HorizontalVelocity new_pqos_HorizontalVelocity(){
		return new PQOS_HorizontalVelocity();
	}

	public PQOS_MaximumPositionAge get_pqos_MaximumPositionAge(){
		return (PQOS_MaximumPositionAge)elements.get(12).data;
	}
	public PQOS_MaximumPositionAge new_pqos_MaximumPositionAge(){
		return new PQOS_MaximumPositionAge();
	}

	public PQOS_PositionPriority get_pqos_PositionPriority(){
		return (PQOS_PositionPriority)elements.get(13).data;
	}
	public PQOS_PositionPriority new_pqos_PositionPriority(){
		return new PQOS_PositionPriority();
	}

	public PQOS_ResponseTime get_pqos_ResponseTime(){
		return (PQOS_ResponseTime)elements.get(14).data;
	}
	public PQOS_ResponseTime new_pqos_ResponseTime(){
		return new PQOS_ResponseTime();
	}

	public PQOS_VerticalPosition get_pqos_VerticalPosition(){
		return (PQOS_VerticalPosition)elements.get(15).data;
	}
	public PQOS_VerticalPosition new_pqos_VerticalPosition(){
		return new PQOS_VerticalPosition();
	}

	public PQOS_VerticalVelocity get_pqos_VerticalVelocity(){
		return (PQOS_VerticalVelocity)elements.get(16).data;
	}
	public PQOS_VerticalVelocity new_pqos_VerticalVelocity(){
		return new PQOS_VerticalVelocity();
	}

	public TDMA_MAHORequest get_tdma_MAHORequest(){
		return (TDMA_MAHORequest)elements.get(17).data;
	}
	public TDMA_MAHORequest new_tdma_MAHORequest(){
		return new TDMA_MAHORequest();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(337, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_positionRequestType(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(367, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcsBillingID(); }});
		elements.add(new ElementDescriptor(358, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcs_Client_ID(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(335, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobilePositionCapability(); }});
		elements.add(new ElementDescriptor(371, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mpcid(); }});
		elements.add(new ElementDescriptor(372, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_HorizontalPosition(); }});
		elements.add(new ElementDescriptor(373, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_HorizontalVelocity(); }});
		elements.add(new ElementDescriptor(374, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_MaximumPositionAge(); }});
		elements.add(new ElementDescriptor(375, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_PositionPriority(); }});
		elements.add(new ElementDescriptor(376, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_ResponseTime(); }});
		elements.add(new ElementDescriptor(377, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_VerticalPosition(); }});
		elements.add(new ElementDescriptor(378, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_VerticalVelocity(); }});
		elements.add(new ElementDescriptor(364, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_MAHORequest(); }});
	}
	public InterSystemPositionRequestForward(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
