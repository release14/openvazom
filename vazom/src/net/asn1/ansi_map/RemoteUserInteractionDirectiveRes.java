package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RemoteUserInteractionDirectiveRes extends SET{

	public Digits get_digits(){
		return (Digits)elements.get(0).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
	}
	public RemoteUserInteractionDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
