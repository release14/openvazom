package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CSG_Id extends BIT_STRING{

	public CSG_Id(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
