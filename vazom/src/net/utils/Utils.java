package net.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;
import net.asn1.gsmmap2.GSMMAPLocalErrorcode;
import net.asn1.gsmmap2.GSMMAPOperationLocalvalue;
import net.asn1.gsmmap2.IMSI;
import net.asn1.gsmmap2.ISDN_AddressString;
import net.asn1.gsmmap2.InvokeIdType;
import net.asn1.gsmmap2.LocalErrorcode;
import net.asn1.gsmmap2.LocationInfoWithLMSI;
import net.asn1.gsmmap2.MAP_ERROR;
import net.asn1.gsmmap2.MAP_OPERATION;
import net.asn1.gsmmap2.MO_ForwardSM_Arg;
import net.asn1.gsmmap2.MT_ForwardSM_Arg;
import net.asn1.gsmmap2.OperationLocalvalue;
import net.asn1.gsmmap2.RoutingInfoForSM_Arg;
import net.asn1.gsmmap2.RoutingInfoForSM_Res;
import net.asn1.gsmmap2.SM_RP_DA;
import net.asn1.gsmmap2.SM_RP_OA;
import net.asn1.gsmmap2.SignalInfo;
import net.asn1.tcap2.Abort;
import net.asn1.tcap2.Begin;
import net.asn1.tcap2.Component;
import net.asn1.tcap2.ComponentPortion;
import net.asn1.tcap2.DestTransactionID;
import net.asn1.tcap2.DialoguePortion;
import net.asn1.tcap2.End;
import net.asn1.tcap2.Invoke;
import net.asn1.tcap2.OrigTransactionID;
import net.asn1.tcap2.Reason;
import net.asn1.tcap2.ResultRetRes;
import net.asn1.tcap2.ReturnError;
import net.asn1.tcap2.TCMessage;
import net.asn1.tcapdialogue.AARE_apdu;
import net.asn1.tcapdialogue.AARQ_apdu;
import net.asn1.tcapdialogue.Associate_result;
import net.asn1.tcapdialogue.Associate_source_diagnostic;
import net.asn1.tcapdialogue.DialoguePDU;
import net.asn1.tcapdialogue.Dialogue_service_userInteger;
import net.asn1.types.BIT_STRING;
import net.asn1.types.BOOLEAN;
import net.asn1.types.EXTERNAL;
import net.asn1.types.EXTERNAL_ENCODING;
import net.asn1.types.OBJECT_IDENTIFIER;
import net.ber.BerTag;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
import net.db.DBRecordSMS;
import net.ds.DataSourceType;
import net.hlr.HLRPacket;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.m3ua.parameters.NetworkAppearance;
import net.m3ua.parameters.RoutingContext;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.mtp3.MTP3Packet;
import net.mtp3.messages.normal.SCCPDataMessage;
import net.mtp3.types.NetworkIndicatorType;
import net.sccp.MessageType;
import net.sccp.NatureOfAddress;
import net.sccp.RoutingIndicator;
import net.sccp.SCCP;
import net.sccp.messages.MessageBase;
import net.sccp.messages.UDT_UnitData;
import net.sccp.parameters.global_title.EncodingScheme;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.GlobalTitle_NOA;
import net.sccp.parameters.global_title.GlobalTitle_TT;
import net.sccp.parameters.global_title.GlobalTitle_TTNPE;
import net.sccp.parameters.global_title.GlobalTitle_TTNPENOA;
import net.sccp.parameters.global_title.NumberingPlan;
import net.sccp.parameters.protocol_class.MessageHandling;
import net.sccp.parameters.protocol_class.ProtocolClassType;
import net.smpp.ErrorCodeType;
import net.smpp.OptionalParameterType;
import net.smpp.PDUType;
import net.smpp.parameters.optional.Sar_msg_ref_num;
import net.smpp.parameters.optional.Sar_segment_seqnum;
import net.smpp.parameters.optional.Sar_total_segments;
import net.smpp.pdu.Cancel_sm;
import net.smpp.pdu.Deliver_sm;
import net.smpp.pdu.Deliver_sm_resp;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_multi_resp;
import net.smpp.pdu.Submit_sm;
import net.smpp.pdu.Submit_sm_resp;
import net.smsfs.ModeOfOperation;
import net.smsfs.SmsfsPacket;
import net.smstpdu.AddressString;
import net.smstpdu.GSMAlphabet;
import net.smstpdu.SmsDirection;
import net.smstpdu.SmsPduType;
import net.smstpdu.SmsType;
import net.smstpdu.TBCD;
import net.smstpdu.TPDA_OA_DA;
import net.smstpdu.TPValidityPeriodFormat;
import net.smstpdu.TypeOfNumber;
import net.smstpdu.tpdcs.TPDCS;
import net.smstpdu.tpdcs.TPDCS_General;
import net.smstpdu.tpdcs.general.Alphabet;
import net.smstpdu.tpdu.SmsDeliver;
import net.smstpdu.tpdu.SmsSubmit;
import net.smstpdu.tpdu.TPDU;
import net.smstpdu.tpdu.deliver.TPSCTS;
import net.smstpdu.tpdu.udh.IE_Concatenated_8bit;
import net.smstpdu.tpdu.udh.UDH;
import net.smstpdu.tppid.TPPID;
import net.smstpdu.tppid.TPPID_Format;
import net.smstpdu.tppid.TPPID_Interworking;
import net.vstp.MessageDescriptor;
import net.vstp.VSTPDataItemType;

public class Utils {
	

	public static void file_save_contents(String file, byte[] data){
		try{
			FileOutputStream fo = new FileOutputStream(new File(file));
			fo.write(data);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static byte[] file_get_contents(String file){
		File f = new File(file);
		byte[] data = new byte[(int)f.length()];
		int i = 0;

		try{
			FileInputStream fi = new FileInputStream(f);
			int a;
			while( (a = fi.read()) != -1){
				data[i++] = (byte)a;
			}
			return data;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String str_align(String data, String column_delimiter){
		if(data != null && column_delimiter != null){
			String[] lst = data.split("\n");
			String[] data_lst = new String[lst.length];
			String[] tmp_lst = null;
			String res = "";
			int max_l = 0;
			
			for(int i = 0; i<lst.length; i++){
				tmp_lst = lst[i].split(column_delimiter, 2);
				if(tmp_lst.length >= 2){
					data_lst[i] = tmp_lst[1];
					if(tmp_lst[0].length() > max_l) max_l = tmp_lst[0].length();
					lst[i] = tmp_lst[0];
					
				}
			}

			for(int i = 0; i<lst.length; i++){
				if(data_lst[i] != null){
					res += String.format("%" + max_l + "s", lst[i]) + column_delimiter + data_lst[i] + "\n";
				}else{
					res += lst[i] + "\n";
				}
			}
			return res;
			
			
		}
		return null;
	
	}
	
	
	public static boolean isIntrusive(DataSourceType ds){
		switch(ds){
			case E1: return true;
			case SCTP: return true;
			case SMPP: return true;
		}
		return false;
		
	}
	
	public static ModeOfOperation convert_DS2MOO(DataSourceType ds){
		switch(ds){
			case E1: return ModeOfOperation.INTRUSIVE;
			case SCTP: return ModeOfOperation.INTRUSIVE;
			case SMPP: return ModeOfOperation.INTRUSIVE;
			case SPCAP_E1: return ModeOfOperation.NON_INTRUSIVE;
			case SPCAP_SCTP: return ModeOfOperation.NON_INTRUSIVE;
			case SPCAP_SMPP: return ModeOfOperation.NON_INTRUSIVE;
		}
		return ModeOfOperation.NON_INTRUSIVE;
		
	}


	
	public static int safeStr2int(String str){
		int res;
		try{
			res = Integer.parseInt(str);
			return res;
			
		}catch(Exception e){
			return 0;
		}
	}
	public static long safeLong2int(String str){
		long res;
		try{
			res = Long.parseLong(str);
			return res;
			
		}catch(Exception e){
			return 0;
		}
	}
	
	public static boolean isInt(String str){
		try{
			Long.parseLong(str.trim());
			
		}catch(Exception e){
			return false;
		}
		return true;
		
	}
	public static UDT_UnitData generate_default_SCCP(){
		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_mo_sccp_called_ssn;// SubsystemNumber.MSC;//ConfigData.sri_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt.encodingScheme = EncodingScheme.BCD_ODD;
		ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt.addressInformation = TBCD.encode("123456789");

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_mo_sccp_calling_ssn;//SubsystemNumber.MSC;// ConfigData.sri_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_mo_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_mo_smsrouter_gt);
		//ns.data = sri.tcap_packet;
		//sccp_packet = ns.encode();
		//sri.sccp_udt = ns;
		return ns;
		
	
	}	
	
	public static M3UAPacket generate_default_M3UA(){
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		ndd.routingContext = new RoutingContext();
		ndd.routingContext.setRoutingContext(0);

		return ndata;
		
	}
	public static DBRecordSMS convert_VSTP_SMPP2DBR(MessageDescriptor md){
		try{
			DBRecordSMS dbr = new DBRecordSMS();
			
			dbr.pdu_type = SmsPduType.SMPP_PDU;
			dbr.dataSource = md.header.ds;
			dbr.timestamp = System.currentTimeMillis();

			switch(md.header.msg_type){
				case SMPP_MO: dbr.direction = SmsDirection.MO; break;
				case SMPP_MT: dbr.direction = SmsDirection.MT; break;
			}
			
			if(md.values.get(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId()) != null){
				dbr.type = SmsType.CONCATENATED;
				dbr.sms_conc_msgid = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId()));
				dbr.sms_conc_partnum = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId()));
				dbr.sms_conc_parts = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_CONC_PARTS.getId()));
			}else dbr.type = SmsType.SINGLE;
			
			
			dbr.sequence_number = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_SEQ_NUM.getId()));
			dbr.ip_destination = md.values.get(VSTPDataItemType.IP_DESTINATION.getId());
			dbr.ip_source = md.values.get(VSTPDataItemType.IP_SOURCE.getId());
			dbr.tcp_destination = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.TCP_DESTINATION.getId()), "0"));
			dbr.tcp_source = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.TCP_SOURCE.getId()), "0"));
			dbr.system_id = md.values.get(VSTPDataItemType.SMPP_SYSTEM_ID.getId());
			dbr.service_type = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_SERVICE_TYPE.getId()));
			dbr.password = md.values.get(VSTPDataItemType.SMPP_PASSWORD.getId());
			dbr.originator_np = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_ORIGINATOR_NP.getId()));
			dbr.recipient_np = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_RECIPIENT_NP.getId()));
			dbr.esm_message_mode = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_ESM_MESSAGE_MODE.getId()));
			dbr.esm_message_type = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_ESM_MESSAGE_TYPE.getId()));
			dbr.esm_gsm_features = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_ESM_GSM_FEATURES.getId()));
			dbr.protocol_id = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_PROTOCOL_ID.getId()));
			dbr.priority_flag = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_PRIORITY_FLAG.getId()));
			dbr.delivery_time = md.values.get(VSTPDataItemType.SMPP_DELIVERY_TIME.getId());
			dbr.validity_period = md.values.get(VSTPDataItemType.SMPP_VALIDITY_PERIOD.getId());
			dbr.rd_smsc_receipt = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_RD_SMSC_RECEIPT.getId()));
			dbr.rd_sme_ack = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_RD_SME_ACK.getId()));
			dbr.rd_intermediate_notification = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_RD_INTERMEDIATE_NOTIFICATION.getId()));
			dbr.replace_if_present = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_REPLACE_IF_PRESENT.getId()));
			dbr.sm_default_msg_id = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_SM_DEFAULT_MSG_ID.getId()));
			dbr.sm_msg_length = Utils.safeStr2int(md.values.get(VSTPDataItemType.SMPP_SM_LENGTH.getId()));
			dbr.sms_status = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.SMS_STATUS.getId()), "1"));
			dbr.sms_text_enc = 1000 + Integer.parseInt(md.values.get(VSTPDataItemType.SMPP_DATA_CODING.getId()));
			if(md.values.get(VSTPDataItemType.SMPP_SM.getId()) != null) dbr.sms_text = new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.SMPP_SM.getId())));
			dbr.sms_destination_enc = 1000 + Integer.parseInt(md.values.get(VSTPDataItemType.SMPP_RECIPIENT_TON.getId()));
			dbr.sms_destination = md.values.get(VSTPDataItemType.SMPP_RECIPIENT_ADDRESS.getId());
			dbr.sms_originating_enc = 1000 + Integer.parseInt(md.values.get(VSTPDataItemType.SMPP_ORIGINATOR_TON.getId()));
			dbr.sms_originating = md.values.get(VSTPDataItemType.SMPP_ORIGINATOR_ADDRESS.getId());
			
			return dbr;		
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public static DBRecordSMS convert_VSTP_TPDU2DBR(MessageDescriptor md){
		try{
			DBRecordSMS dbr = new DBRecordSMS();
			
			dbr.pdu_type = SmsPduType.SMS_TPDU;
			dbr.dataSource = md.header.ds;
			dbr.timestamp = System.currentTimeMillis();

			switch(md.header.msg_type){
				case SMS_MO: dbr.direction = SmsDirection.MO; break;
				case SMS_MT: dbr.direction = SmsDirection.MT; break;
			}
			
			if(md.values.get(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId()) != null){
				dbr.type = SmsType.CONCATENATED;
				dbr.sms_conc_msgid = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId()));
				dbr.sms_conc_partnum = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId()));
				dbr.sms_conc_parts = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_CONC_PARTS.getId()));
			}else dbr.type = SmsType.SINGLE;

			dbr.gt_called = md.values.get(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId());
			dbr.gt_calling = md.values.get(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId());
			dbr.gt_called_gti = GlobalTitleIndicator.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId())));
			dbr.gt_calling_gti = GlobalTitleIndicator.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId())));
			dbr.gt_called_tt = Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_TT.getId()));
			dbr.gt_calling_tt = Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_TT.getId()));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId()) != null) dbr.gt_called_np = NumberingPlan.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId()) != null) dbr.gt_calling_np = NumberingPlan.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId()) != null) dbr.gt_called_nai = NatureOfAddress.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId()) != null) dbr.gt_calling_nai = NatureOfAddress.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId())));
			
			dbr.scda = md.values.get(VSTPDataItemType.MAP_SCDA.getId());
			dbr.scoa = md.values.get(VSTPDataItemType.MAP_SCOA.getId());
			dbr.imsi =  md.values.get(VSTPDataItemType.MAP_IMSI.getId());
			dbr.msisdn = md.values.get(VSTPDataItemType.MAP_MSISDN.getId());
			dbr.m3ua_data_dpc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
			dbr.m3ua_data_opc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_SID.getId()) != null) dbr.tcap_sid = Long.parseLong(md.values.get(VSTPDataItemType.TCAP_SID.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_DID.getId()) != null) dbr.tcap_did = Long.parseLong(md.values.get(VSTPDataItemType.TCAP_DID.getId()));
			dbr.sms_text_enc = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_TPDU_DCS.getId()));
			dbr.timestamp = System.currentTimeMillis();
			dbr.dataSource = md.header.ds;
			if(md.values.get(VSTPDataItemType.SMS_TPDU_UD.getId()) != null) dbr.sms_text = new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.SMS_TPDU_UD.getId())));
			if(md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING_ENC.getId()) != null) dbr.sms_originating_enc = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING_ENC.getId()));
			if(md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId()) != null) dbr.sms_originating = md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId());
			if(md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION_ENC.getId()) != null) dbr.sms_destination_enc = Integer.parseInt(md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION_ENC.getId()));
			if(md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION.getId()) != null) dbr.sms_destination = md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION.getId());
			
			
			return dbr;
		}catch(Exception e){
			e.printStackTrace();
		}		
		return null;
		
	}
	
	
	public static String convert_obj2str(Object o){
		String res = null;
		if(o instanceof Integer) res = Integer.toString((Integer)o);
		else if(o instanceof Long) res = Long.toString((Long)o);
		else if(o instanceof String) res = (String)o;
		return res;
		
	}
	
	
	public static boolean check_mysql_ts(String ts){
		//2010-01-01 00:00:00
		Pattern pt = Pattern.compile("^\\d+-\\d+-\\d+ \\d+:\\d+:\\d+", Pattern.CASE_INSENSITIVE);
		Matcher mt = pt.matcher(ts);
		return mt.matches();
	}
	
	
	// ANSI
	public static String ansi_box(String data, String grid_color, String text_color){
		String res = grid_color;
		int l = data.length();
		res += "+";
		for(int i = 0; i<l; i++) res += "-";
		res += "+\n";
		res += "|";
		res += text_color + data;
		res += grid_color + "|\n";
		res += "+";
		for(int i = 0; i<l; i++) res += "-";
		res += "+";
		return res;
	}	
	public static String convert_str2pwd(String data){
		String res = "";
		for(int i = 0; i<data.length(); i++) res += "*";
		return res;
	}	
	
	public static String ansi_paint(String data){
		/*
		\u001B[1;37m white
		\u001B[1;31m red
		\u001B[1;32m green
		\u001B[1;33m yellow
		\u001B[1;34m blue
	 */
		if(data != null){
			data = data.replaceAll("\\[WHITE\\]", "\u001B[1;37m");
			data = data.replaceAll("\\[BLUE\\]", "\u001B[1;34m");
			data = data.replaceAll("\\[GREEN\\]", "\u001B[1;32m");
			data = data.replaceAll("\\[RED\\]", "\u001B[1;31m");
			data = data.replaceAll("\\[YELLOW\\]", "\u001B[1;33m");
			data = data.replaceAll("\\[BLACK\\]", "\u001B[1;30m");
			data = data.replaceAll("\\[MAGENTA\\]", "\u001B[1;35m");
			data = data.replaceAll("\\[CYAN\\]", "\u001B[1;36m");
			data = data.replaceAll("\\[RESET\\]", "\u001B[0m");
			
		}

		return data;
		
	}
	public static String ansi_clear(String data){
		if(data != null){
			data = data.replaceAll("\\[WHITE\\]", "");
			data = data.replaceAll("\\[BLUE\\]", "");
			data = data.replaceAll("\\[GREEN\\]", "");
			data = data.replaceAll("\\[RED\\]", "");
			data = data.replaceAll("\\[YELLOW\\]", "");
			data = data.replaceAll("\\[BLACK\\]", "");
			data = data.replaceAll("\\[MAGENTA\\]", "");
			data = data.replaceAll("\\[CYAN\\]", "");
			data = data.replaceAll("\\[RESET\\]", "");
			
		}

		return data;
		
	}
	
	public static M3UAPacket convert_MTP3_M3UA(MTP3Packet mtp3){
		M3UAPacket res = null;
		DATA m3ua_data;
		SCCPDataMessage sccp_data = null;
		if(mtp3 != null){
			try{
				if(mtp3.serviceInformation.serviceIndicator == net.mtp3.types.ServiceIndicatorType.SCCP){
					sccp_data = (SCCPDataMessage)mtp3.message;
					res = M3UA.prepareNew(M3UAMessageType.DATA);
					m3ua_data = (DATA)res.message;
					m3ua_data.protocolData.setOPC(mtp3.routingLabel.originatingPointCode);
					m3ua_data.protocolData.setDPC(mtp3.routingLabel.destinationPointCode);
					m3ua_data.protocolData.setSI(ServiceIndicatorType.SCCP);
					m3ua_data.protocolData.setNI(mtp3.serviceInformation.networkIndicator.getId() >> 6);
					m3ua_data.protocolData.setMP(0);
					m3ua_data.protocolData.setSLS(mtp3.routingLabel.signallingLinkSelector);
					m3ua_data.protocolData.setUserProtocolData(sccp_data.data);
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return res;
	}
	public static MTP3Packet convert_M3UA_MTP3(M3UAPacket m3ua){
		MTP3Packet res = null;
		SCCPDataMessage sccp_data = null;
		DATA m3ua_data;
		if(m3ua != null){
			try{
				if(m3ua.message.type == M3UAMessageType.DATA){
					m3ua_data = (DATA)m3ua.message;
					// SCCP
					if(m3ua_data.protocolData.signallingLinkSelectionCode == 3){
						res = new MTP3Packet();
						if(NetworkIndicatorType.get(m3ua_data.protocolData.networkIndicator << 6) != null){
							res.serviceInformation.networkIndicator = NetworkIndicatorType.get(m3ua_data.protocolData.networkIndicator << 6);
						}else res.serviceInformation.networkIndicator = NetworkIndicatorType.NATIONAL_NETWORK;
						
						res.serviceInformation.serviceIndicator = net.mtp3.types.ServiceIndicatorType.SCCP;
						res.routingLabel.destinationPointCode = m3ua_data.protocolData.destinationPointCode;
						res.routingLabel.originatingPointCode = m3ua_data.protocolData.originatingPointCode;
						res.routingLabel.signallingLinkSelector = m3ua_data.protocolData.signallingLinkSelectionCode;
						sccp_data = new SCCPDataMessage();
						sccp_data.data = m3ua_data.protocolData.userProtocolData;
						res.message = sccp_data;
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return res;
	}


	public static TPDU convert_SMPP_TPDU_reverse(PDUBase smpp){
		SmsSubmit s = null;
		SmsDeliver d = null;
		Submit_sm ssm = null;
		//Deliver_sm dsm = null;
		Calendar c = null;
		try{
			switch(smpp.type){
				case SUBMT_SM:
					ssm = (Submit_sm)smpp;
					d = new SmsDeliver();
					// first byte
					d.TP_RP = false;
					d.TP_SRI = false;
					d.TP_MMS = true;
					// TP-OA
					d.TP_OA = new TPDA_OA_DA();
					d.TP_OA.typeOfNumber = TypeOfNumber.valueOf(ssm.source_addr_ton.toString());
					d.TP_OA.numberingPlan = net.smstpdu.NumberingPlan.valueOf(ssm.source_addr_npi.toString());
					if(d.TP_OA.typeOfNumber == TypeOfNumber.ALPHANUMERIC) d.TP_OA.digits = GSMAlphabet.encode(ssm.source_addr, 0);
					else d.TP_OA.digits = TBCD.encode(ssm.source_addr);
					// TP-PID
					d.TP_PID = new TPPID();
					d.TP_PID.format = TPPID_Format.DEFAULT1;
					d.TP_PID.interworking = TPPID_Interworking.NO_INTERWORKING_BUT_SME_TO_SME;
					// TP-DCS
					d.TP_DCS = new TPDCS_General();
					((TPDCS_General)d.TP_DCS).isCompressed = false;
					((TPDCS_General)d.TP_DCS).hasMessageClass = false;
					switch(ssm.data_coding){
						case DEFAULT:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet.DEFAULT;
							break;
						case _8BIT_BINARY_1:
						case _8BIT_BINARY_2:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet._8BIT;
							break;
						default:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet.UCS2;
					}
					((TPDCS_General)d.TP_DCS).encoding = ((TPDCS_General)d.TP_DCS).alphabet;
					
					// TP-SCTS
					d.TP_SCTS = new TPSCTS();
					c = Calendar.getInstance();
					d.TP_SCTS.year = Integer.parseInt(Integer.toString(c.get(Calendar.YEAR)).substring(2));
					d.TP_SCTS.month = c.get(Calendar.MONTH) + 1;
					d.TP_SCTS.day = c.get(Calendar.DAY_OF_MONTH);
					d.TP_SCTS.hour = c.get(Calendar.HOUR_OF_DAY);
					d.TP_SCTS.minute = c.get(Calendar.MINUTE);
					d.TP_SCTS.second = c.get(Calendar.SECOND);
					d.TP_SCTS.time_zone = 0;
					// TP-UD
					// CONCATENATED
					if(ssm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM) != null){
						d.TP_UDHI = true;
						// TP-UDL
						d.TP_UDL = ssm.sm_length;
						d.udh = new UDH();
						// create udh IE
						IE_Concatenated_8bit iec = new IE_Concatenated_8bit();
						// get CONC elements
						Sar_msg_ref_num ref_num = (Sar_msg_ref_num)ssm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						Sar_segment_seqnum partnum = (Sar_segment_seqnum)ssm.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
						Sar_total_segments parts = (Sar_total_segments)ssm.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
						// set IE values
						iec.messageIdentifier = ref_num.value;
						iec.partNumber = partnum.value;
						iec.parts = parts.value;
						// add IE to list
						d.udh.informationElements.add(iec);
						// encode message, get pre padding bits based on UDH length of 6 bytes
						if(d.TP_DCS.encoding == Alphabet.DEFAULT) d.TP_UD = GSMAlphabet.encode(new String(ssm.short_message), GSMAlphabet.getPrePaddingBits(6));
						else d.TP_UD = ssm.short_message;
					// SINGLE
					}else{
						d.TP_UDHI = false;
						// TP-UDL
						d.TP_UDL = ssm.sm_length;
						if(d.TP_DCS.encoding == Alphabet.DEFAULT) d.TP_UD = GSMAlphabet.encode(new String(ssm.short_message), 0);
						else d.TP_UD = ssm.short_message;
					}				
					return d;
/*
				case DELIVER_SM:
					dsm = (Deliver_sm)smpp;
					d = new SmsDeliver();
					// first byte
					d.TP_RP = true;
					d.TP_SRI = true;
					d.TP_MMS = true;
					// TP-OA
					d.TP_OA = new TPDA_OA_DA();
					d.TP_OA.typeOfNumber = TypeOfNumber.valueOf(dsm.source_addr_ton.toString());
					d.TP_OA.numberingPlan = net.smstpdu.NumberingPlan.valueOf(dsm.source_addr_npi.toString());
					if(d.TP_OA.typeOfNumber == TypeOfNumber.ALPHANUMERIC) d.TP_OA.digits = GSMAlphabet.encode(dsm.source_addr, 0);
					else d.TP_OA.digits = TBCD.encode(dsm.source_addr);
					// TP-PID
					d.TP_PID = new TPPID();
					d.TP_PID.format = TPPID_Format.DEFAULT1;
					d.TP_PID.interworking = TPPID_Interworking.NO_INTERWORKING_BUT_SME_TO_SME;
					// TP-DCS
					d.TP_DCS = new TPDCS_General();
					((TPDCS_General)d.TP_DCS).isCompressed = false;
					((TPDCS_General)d.TP_DCS).hasMessageClass = false;
					switch(dsm.data_coding){
						case DEFAULT:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet.DEFAULT;
							break;
						case UCS2:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet.UCS2;
							break;
						default:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet._8BIT;
					}
					// TP-SCTS
					d.TP_SCTS = new TPSCTS();
					c = Calendar.getInstance();
					d.TP_SCTS.year = Integer.parseInt(Integer.toString(c.get(Calendar.YEAR)).substring(2));
					d.TP_SCTS.month = c.get(Calendar.MONTH) + 1;
					d.TP_SCTS.day = c.get(Calendar.DAY_OF_MONTH);
					d.TP_SCTS.hour = c.get(Calendar.HOUR_OF_DAY);
					d.TP_SCTS.minute = c.get(Calendar.MINUTE);
					d.TP_SCTS.second = c.get(Calendar.SECOND);
					d.TP_SCTS.time_zone = 0;
					// TP-UD
					// CONCATENATED
					if(dsm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM) != null){
						d.TP_UDHI = true;
						d.udh = new UDH();
						// create udh IE
						IE_Concatenated_8bit iec = new IE_Concatenated_8bit();
						// get CONC elements
						Sar_msg_ref_num ref_num = (Sar_msg_ref_num)dsm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						Sar_segment_seqnum partnum = (Sar_segment_seqnum)dsm.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
						Sar_total_segments parts = (Sar_total_segments)dsm.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
						// set IE values
						iec.messageIdentifier = ref_num.value;
						iec.partNumber = partnum.value;
						iec.parts = parts.value;
						// add IE to list
						d.udh.informationElements.add(iec);
						// encode message, get pre padding bits based on UDH length of 6 bytes
						if(d.TP_DCS.encoding == Alphabet.DEFAULT) d.TP_UD = GSMAlphabet.encode(new String(dsm.short_message), GSMAlphabet.getPrePaddingBits(6));
						else d.TP_UD = dsm.short_message;
					// SINGLE
					}else{
						d.TP_UDHI = false;
						if(d.TP_DCS.encoding == Alphabet.DEFAULT) d.TP_UD = GSMAlphabet.encode(new String(dsm.short_message), 0);
						else d.TP_UD = dsm.short_message;
					}				
					return d;
				*/	
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return null;	
	}
	
	public static TPDU convert_SMPP_TPDU(PDUBase smpp){
		SmsSubmit s = null;
		SmsDeliver d = null;
		Submit_sm ssm = null;
		Deliver_sm dsm = null;
		try{
			switch(smpp.type){
				case SUBMT_SM:
					ssm = (Submit_sm)smpp;
					s = new SmsSubmit();
					// first byte
					s.TP_RP = true;
					s.TP_SRR = true;
					s.TP_VPF = TPValidityPeriodFormat.TP_VP_Relative;
					s.TP_RD = true;
					s.TP_MR = (int)ssm.sequence_number;
					// TP-DA
					s.TP_DA = new TPDA_OA_DA();
					s.TP_DA.typeOfNumber = TypeOfNumber.valueOf(ssm.dest_addr_ton.toString());
					s.TP_DA.numberingPlan = net.smstpdu.NumberingPlan.valueOf(ssm.dest_addr_npi.toString());
					if(s.TP_DA.typeOfNumber == TypeOfNumber.ALPHANUMERIC) s.TP_DA.digits = GSMAlphabet.encode(ssm.destination_addr, 0);
					else s.TP_DA.digits = TBCD.encode(ssm.destination_addr);
					// TP-PID
					s.TP_PID = new TPPID();
					s.TP_PID.format = TPPID_Format.DEFAULT1;
					s.TP_PID.interworking = TPPID_Interworking.NO_INTERWORKING_BUT_SME_TO_SME;
					// TP-DCS
					s.TP_DCS = new TPDCS_General();
					((TPDCS_General)s.TP_DCS).isCompressed = false;
					((TPDCS_General)s.TP_DCS).hasMessageClass = false;
					switch(ssm.data_coding){
						case DEFAULT:
							((TPDCS_General)s.TP_DCS).alphabet = Alphabet.DEFAULT;
							break;
						case UCS2:
							((TPDCS_General)s.TP_DCS).alphabet = Alphabet.UCS2;
							break;
						default:
							((TPDCS_General)s.TP_DCS).alphabet = Alphabet._8BIT;
					}
					((TPDCS_General)s.TP_DCS).encoding = ((TPDCS_General)s.TP_DCS).alphabet;
					// TP-VP
					s.TP_VP = 0xff;
					
					// TP-UD
					// CONCATENATED
					if(ssm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM) != null){
						s.TP_UDHI = true;
						// TP-UDL
						s.TP_UDL = ssm.sm_length;
						s.udh = new UDH();
						// create udh IE
						IE_Concatenated_8bit iec = new IE_Concatenated_8bit();
						// get CONC elements
						Sar_msg_ref_num ref_num = (Sar_msg_ref_num)ssm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						Sar_segment_seqnum partnum = (Sar_segment_seqnum)ssm.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
						Sar_total_segments parts = (Sar_total_segments)ssm.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
						// set IE values
						iec.messageIdentifier = ref_num.value;
						iec.partNumber = partnum.value;
						iec.parts = parts.value;
						// add IE to list
						s.udh.informationElements.add(iec);
						// encode message, get pre padding bits based on UDH length of 6 bytes
						if(s.TP_DCS.encoding == Alphabet.DEFAULT) s.TP_UD = GSMAlphabet.encode(new String(ssm.short_message), GSMAlphabet.getPrePaddingBits(6));
						else s.TP_UD = ssm.short_message;
					// SINGLE
					}else{
						s.TP_UDHI = false;
						// TP-UDL
						s.TP_UDL = ssm.sm_length;
						// TP-UD
						if(s.TP_DCS.encoding == Alphabet.DEFAULT) s.TP_UD = GSMAlphabet.encode(new String(ssm.short_message), 0);
						else s.TP_UD = ssm.short_message;
						
					}
					return s;

				case DELIVER_SM:
					dsm = (Deliver_sm)smpp;
					d = new SmsDeliver();
					// first byte
					d.TP_RP = true;
					d.TP_SRI = true;
					d.TP_MMS = true;
					// TP-OA
					d.TP_OA = new TPDA_OA_DA();
					d.TP_OA.typeOfNumber = TypeOfNumber.valueOf(dsm.source_addr_ton.toString());
					d.TP_OA.numberingPlan = net.smstpdu.NumberingPlan.valueOf(dsm.source_addr_npi.toString());
					if(d.TP_OA.typeOfNumber == TypeOfNumber.ALPHANUMERIC) d.TP_OA.digits = GSMAlphabet.encode(dsm.source_addr, 0);
					else d.TP_OA.digits = TBCD.encode(dsm.source_addr);
					// TP-PID
					d.TP_PID = new TPPID();
					d.TP_PID.format = TPPID_Format.DEFAULT1;
					d.TP_PID.interworking = TPPID_Interworking.NO_INTERWORKING_BUT_SME_TO_SME;
					// TP-DCS
					d.TP_DCS = new TPDCS_General();
					((TPDCS_General)d.TP_DCS).isCompressed = false;
					((TPDCS_General)d.TP_DCS).hasMessageClass = false;
					switch(dsm.data_coding){
						case DEFAULT:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet.DEFAULT;
							break;
						case UCS2:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet.UCS2;
							break;
						default:
							((TPDCS_General)d.TP_DCS).alphabet = Alphabet._8BIT;
					}
					// TP-SCTS
					d.TP_SCTS = new TPSCTS();
					Calendar c = Calendar.getInstance();
					d.TP_SCTS.year = Integer.parseInt(Integer.toString(c.get(Calendar.YEAR)).substring(2));
					d.TP_SCTS.month = c.get(Calendar.MONTH) + 1;
					d.TP_SCTS.day = c.get(Calendar.DAY_OF_MONTH);
					d.TP_SCTS.hour = c.get(Calendar.HOUR_OF_DAY);
					d.TP_SCTS.minute = c.get(Calendar.MINUTE);
					d.TP_SCTS.second = c.get(Calendar.SECOND);
					d.TP_SCTS.time_zone = 0;
					// TP-UD
					// CONCATENATED
					if(dsm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM) != null){
						d.TP_UDHI = true;
						// TP-UDL
						s.TP_UDL = ssm.sm_length;
						d.udh = new UDH();
						// create udh IE
						IE_Concatenated_8bit iec = new IE_Concatenated_8bit();
						// get CONC elements
						Sar_msg_ref_num ref_num = (Sar_msg_ref_num)dsm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						Sar_segment_seqnum partnum = (Sar_segment_seqnum)dsm.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
						Sar_total_segments parts = (Sar_total_segments)dsm.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
						// set IE values
						iec.messageIdentifier = ref_num.value;
						iec.partNumber = partnum.value;
						iec.parts = parts.value;
						// add IE to list
						d.udh.informationElements.add(iec);
						// encode message, get pre padding bits based on UDH length of 6 bytes
						if(d.TP_DCS.encoding == Alphabet.DEFAULT) d.TP_UD = GSMAlphabet.encode(new String(dsm.short_message), GSMAlphabet.getPrePaddingBits(6));
						else d.TP_UD = dsm.short_message;
					// SINGLE
					}else{
						d.TP_UDHI = false;
						// TP-UDL
						s.TP_UDL = ssm.sm_length;
						if(d.TP_DCS.encoding == Alphabet.DEFAULT) d.TP_UD = GSMAlphabet.encode(new String(dsm.short_message), 0);
						else d.TP_UD = dsm.short_message;
					}				
					return d;
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static PDUBase convert_TPDU_SMPP(TPDU tpdu){
		SmsSubmit s = null;
		SmsDeliver d = null;
		Submit_sm ssm = null;
		Deliver_sm dsm = null;
		switch(tpdu.type){
			case SMS_SUBMIT:
				break;
			case SMS_DELIVER:
				break;
		}
		return null;
	}
	
	public static String bytes2IPv4(byte[] data){
		if(data != null){
			return Integer.toString(data[0] & 0xff) + "." + Integer.toString(data[1] & 0xff) + "." + Integer.toString(data[2] & 0xff) + "." + Integer.toString(data[3] & 0xff);
		}
		return null;
	}
	public static byte[] reverse(byte[] data){
		byte[] res = new byte[data.length];
		for(int i = data.length-1; i>=0; i--) res[data.length - i - 1] = data[i];
		return res;
	}
	public static boolean[] reverse(boolean[] data){
		boolean[] res = new boolean[data.length];
		for(int i = data.length-1; i>=0; i--) res[data.length - i - 1] = data[i];
		return res;
	}

	public static Invoke get_INVOKE(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_begin() != null){
					if(tcm.get_begin().get_components() != null){
						for(int i = 0; i<tcm.get_begin().get_components().of_children.size(); i++){
							if(tcm.get_begin().get_components().getChild(i).get_invoke() != null){
								return tcm.get_begin().get_components().getChild(i).get_invoke();
								
							}
						}
					}
				}else if(tcm.get_continue() != null){
					if(tcm.get_continue().get_components() != null){
						for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
							if(tcm.get_continue().get_components().getChild(i).get_invoke() != null){
								return tcm.get_continue().get_components().getChild(i).get_invoke();
								
							}
						}
					}
					
				}else if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_invoke() != null){
								return tcm.get_end().get_components().getChild(i).get_invoke();
								
							}
						}
					}
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}	
	public static ReturnError get_RETURN_ERROR(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_returnError() != null){
								return tcm.get_end().get_components().getChild(i).get_returnError();
								
							}
						}
					}
				}else if(tcm.get_continue() != null){
					if(tcm.get_continue().get_components() != null){
						for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
							if(tcm.get_continue().get_components().getChild(i).get_returnError() != null){
								return tcm.get_continue().get_components().getChild(i).get_returnError();
								
							}
						}
					}
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}	

	
	public static ASNType get_RETURN_RESULT_LAST(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_returnResultLast() != null){
								return tcm.get_end().get_components().getChild(i).get_returnResultLast();
								
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}	
	public static ResultRetRes get_RETURN_RESULT_LAST_RETRES(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_returnResultLast() != null){
								return tcm.get_end().get_components().getChild(i).get_returnResultLast().get_resultretres();
								
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}	
	public static ASNType get_RETURN_RESULT_LAST_RETRES_PARAMETER(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_returnResultLast() != null){
								if(tcm.get_end().get_components().getChild(i).get_returnResultLast().get_resultretres() != null){
									return tcm.get_end().get_components().getChild(i).get_returnResultLast().get_resultretres().get_parameter();
								}
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}	

	
	public static ASNType get_INVOKE_PARAMETER(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_begin() != null){
					if(tcm.get_begin().get_components() != null){
						for(int i = 0; i<tcm.get_begin().get_components().of_children.size(); i++){
							if(tcm.get_begin().get_components().getChild(i).get_invoke() != null){
								return tcm.get_begin().get_components().getChild(i).get_invoke().get_parameter();
								
							}
						}
					}
				}else if(tcm.get_continue() != null){
					if(tcm.get_continue().get_components() != null){
						for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
							if(tcm.get_continue().get_components().getChild(i).get_invoke() != null){
								return tcm.get_continue().get_components().getChild(i).get_invoke().get_parameter();
								
							}
						}
					}
					
				}else if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_invoke() != null){
								return tcm.get_end().get_components().getChild(i).get_invoke().get_parameter();
								
							}
						}
					}
					
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public static byte[] get_INVOKE_ID(TCMessage tcm){
		try{
			if(tcm != null){
				if(tcm.get_begin() != null){
					if(tcm.get_begin().get_components() != null){
						for(int i = 0; i<tcm.get_begin().get_components().of_children.size(); i++){
							if(tcm.get_begin().get_components().getChild(i).get_invoke() != null){
								return tcm.get_begin().get_components().getChild(i).get_invoke().get_invokeID().value;
							}
						}
					}
				}else if(tcm.get_continue() != null){
					if(tcm.get_continue().get_components() != null){
						for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
							if(tcm.get_continue().get_components().getChild(i).get_invoke() != null){
								return tcm.get_continue().get_components().getChild(i).get_invoke().get_invokeID().value;
							}
						}
					}
				}else if(tcm.get_end() != null){
					if(tcm.get_end().get_components() != null){
						for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
							if(tcm.get_end().get_components().getChild(i).get_invoke() != null){
								return tcm.get_end().get_components().getChild(i).get_invoke().get_invokeID().value;
							}else if(tcm.get_end().get_components().getChild(i).get_returnResultLast() != null){
								return tcm.get_end().get_components().getChild(i).get_returnResultLast().get_invokeID().value;						
							}else if(tcm.get_end().get_components().getChild(i).get_returnError() != null){
								return tcm.get_end().get_components().getChild(i).get_returnError().get_invokeID().value;						
								
							}
						
						}					
						
					}
				}				
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return null; 
		
	}
	public static void bytes2file(byte[] data, String file){
		File f = new File(file);
		try{
			FileOutputStream fo = new FileOutputStream(f);
			for(int i = 0; i<data.length; i++) fo.write(data[i]);
			fo.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static TCMessage get_TCAP(M3UAPacket m3ua_packet){
		DATA data = null;
		UDT_UnitData udt = null;
		MessageBase mb = null;
		BerTranscoderv2 ber = new BerTranscoderv2();
		try{
			if(m3ua_packet.message.type == M3UAMessageType.DATA){
				data = (DATA)m3ua_packet.message;
				if(data.protocolData.serviceIndicator == ServiceIndicatorType.SCCP){
					mb = SCCP.decode(data.protocolData.userProtocolData);
					if(mb.type == MessageType.UDT_UNITDATA){
						udt = (UDT_UnitData)mb;
						return (TCMessage)ber.decode(new TCMessage(), udt.data);
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static HLRPacket get_HRLP_REPLY(M3UAPacket m3ua_packet){
		try{
			HLRPacket res = new HLRPacket();
			DATA data = null;
			UDT_UnitData udt = null;
			MessageBase mb = null;
			BerTranscoderv2 ber = new BerTranscoderv2();
			TCMessage tcm = null;
			if(m3ua_packet.message.type == M3UAMessageType.DATA){
				data = (DATA)m3ua_packet.message;
				if(data.protocolData.serviceIndicator == ServiceIndicatorType.SCCP){
					mb = SCCP.decode(data.protocolData.userProtocolData);
					if(mb.type == MessageType.UDT_UNITDATA){
						udt = (UDT_UnitData)mb;
						tcm = (TCMessage)ber.decode(new TCMessage(), udt.data);
						// SRI-for-SM localvalue = 45
						if(tcm.get_end() != null){
							if(tcm.get_end().get_components() != null){
								if(tcm.get_end().get_components().of_children.size() > 0){
									if(tcm.get_end().get_components().getChild(0).get_returnResultLast() != null){
										if(tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres() != null){
											if(bytes2num(tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_opCode().get_localValue().value) == 45){
												res.tcap_id = bytes2num(tcm.get_end().get_dtid().value);
												ASNType returnResultLast = tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_parameter();
												if(returnResultLast != null){
													RoutingInfoForSM_Res sm_res = (RoutingInfoForSM_Res)ber.decode(new RoutingInfoForSM_Res(), returnResultLast.getDataBytes());
													if(sm_res != null){
														// IMSI
														res.imsi = TBCD.decode(sm_res.get_imsi().value);
														LocationInfoWithLMSI li_wlmsi = sm_res.get_locationInfoWithLMSI();
														// Network Node Number
														res.nnn = TBCD.decode(AddressString.decode(li_wlmsi.get_networkNode_Number().value).digits);
														// Additional Number
														if(li_wlmsi.get_additional_Number() != null){
															if(li_wlmsi.get_additional_Number().get_msc_Number() != null){
																res.annn = TBCD.decode(AddressString.decode(li_wlmsi.get_additional_Number().get_msc_Number().value).digits);
															}else if(li_wlmsi.get_additional_Number().get_sgsn_Number() != null){
																res.annn = TBCD.decode(AddressString.decode(li_wlmsi.get_additional_Number().get_sgsn_Number().value).digits);
															}
														}
														return res;
													}
												}
											 }
										}
									}
								}
							}
						}
					 }
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static PDUBase genereate_SMPP_Spam_reply(PDUBase smpp_pdu){
		switch(smpp_pdu.type){
			case SUBMT_SM:
				Submit_sm_resp submit_resp = new Submit_sm_resp();
				submit_resp.command_status = ErrorCodeType.ESME_RINVDSTADR;
				submit_resp.message_id = "VAZOM SPAM";
				submit_resp.sequence_number = smpp_pdu.sequence_number;
				return submit_resp;
			case SUBMIT_MULTI:
				Submit_multi_resp submit_multi_resp = new Submit_multi_resp();
				submit_multi_resp.command_status = ErrorCodeType.ESME_RINVDSTADR;
				submit_multi_resp.message_id = "VAZOM SPAM";
				submit_multi_resp.sequence_number = smpp_pdu.sequence_number;
				return submit_multi_resp;
			case DELIVER_SM:
				Deliver_sm_resp deliver_resp = new Deliver_sm_resp();
				deliver_resp.command_status = ErrorCodeType.ESME_RINVDSTADR;
				deliver_resp.message_id = "VAZOM SPAM";
				deliver_resp.sequence_number = smpp_pdu.sequence_number;
				return deliver_resp;
		}
		
		return null;
	}
	public static Cancel_sm generate_SMPP_Cancel_from_SUBMIT(PDUBase smpp_pdu){
		Submit_sm submit_sm = null;
		if(smpp_pdu.type == PDUType.SUBMT_SM){
			submit_sm = (Submit_sm)smpp_pdu;
			Cancel_sm cancel_sm = new Cancel_sm();
			cancel_sm.dest_addr_npi = submit_sm.dest_addr_npi;
			cancel_sm.dest_addr_ton = submit_sm.dest_addr_ton;
			cancel_sm.destination_addr = submit_sm.destination_addr;
			cancel_sm.message_id = "VAZOM SPAM";
			cancel_sm.sequence_number = submit_sm.sequence_number;
			cancel_sm.service_type = submit_sm.service_type;
			cancel_sm.source_addr = submit_sm.source_addr;
			cancel_sm.source_addr_npi = submit_sm.source_addr_npi;
			cancel_sm.source_addr_ton = submit_sm.source_addr_ton;
			return cancel_sm;
		}
		return null;
	}
	
	public static byte[] generate_MT_TCAP_MAP(String app_ctx_oid, long tcap_sid, long _invoke_id, String _imsi, String _scoa_val, byte[] sms_tpdu){
		AddressString as = null;
		BerTranscoderv2 ber = null;
		// TCM
		TCMessage tcm = new TCMessage();
		BerTag b_root = BerTag.createNew(tcm, null, null);
		
			// BEGIN
			Begin begin = new Begin();
			BerTag b_begin = BerTag.createNew(begin, b_root, tcm.elements.get(1));

			
				// Source Transaction ID
				OrigTransactionID sid = new OrigTransactionID();
				BerTag b_sid = BerTag.createNew(sid, b_begin, null);
				sid.value = Utils.num2bytes(tcap_sid); 

				
				//DialoguePortion
				DialoguePortion dportion = new DialoguePortion();
				BerTag b_dportion = BerTag.createNew(dportion, b_begin, null);
				
					// EXTERNAL
					EXTERNAL external = new EXTERNAL();
					BerTag b_external = BerTag.createNew(external, b_dportion, null);
						
						// OID
						OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
						BerTag b_oid = BerTag.createNew(oid, b_external, null);
						oid.value = Utils.encodeOID("0.0.17.773.1.1.1");
						
						// External Encoding
						EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
						BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
						
							// single asn1 type
							ASNType single_type = new ASNType();
							single_type.asn_pc = ASNTagComplexity.Constructed;
							BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
							
								// DialoguePDU
								DialoguePDU dpdu = new DialoguePDU();
								BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
								
									// DialogueRequest
									AARQ_apdu drq = new AARQ_apdu();
									BerTag b_drq = BerTag.createNew(drq, b_dpdu, dpdu.elements.get(0));
									
									
										// Protocol Version
										BIT_STRING pversion = new BIT_STRING();
										BerTag b_pversion = BerTag.createNew(pversion, b_drq, drq.elements.get(0));
										pversion.value = new byte[]{0x07, (byte)0x80};
										
										
										// Application Context Name EXPLICIT
										ASNType appctx_ex = new ASNType();
										appctx_ex.asn_pc = ASNTagComplexity.Constructed;
										BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_drq, drq.elements.get(1));
										
											// Application Context Name
											OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
											BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
											appctx.value = Utils.encodeOID(app_ctx_oid);
													
				
				
				// Components
				ComponentPortion components = new ComponentPortion();
				BerTag b_components = BerTag.createNew(components, b_begin, null);
	
					// Compoent
					Component component = new Component();
					BerTag b_component = BerTag.createNew(component, b_components, null);
	
						// Invoke
						Invoke invoke = new Invoke();
						BerTag b_invoke = BerTag.createNew(invoke, b_component, component.elements.get(0));
	
							// Invoke ID
							InvokeIdType invoke_id = new InvokeIdType();
							BerTag b_invoke_id = BerTag.createNew(invoke_id, b_invoke, null);
							invoke_id.value = Utils.num2bytes(_invoke_id);
	
							// opCode
							MAP_OPERATION opCode = new MAP_OPERATION();
							BerTag b_opCode = BerTag.createNew(opCode, b_invoke, null);
							
	
								// localValue
								OperationLocalvalue localValue = new OperationLocalvalue();
								BerTag b_localValue = BerTag.createNew(localValue, b_opCode, null);
								localValue.value = new byte[]{GSMMAPOperationLocalvalue._mt_forwardSM};
	
								
							// MT_ForwardSM_Arg
							MT_ForwardSM_Arg mt_arg = new MT_ForwardSM_Arg();
							BerTag b_mt_arg = BerTag.createNew(mt_arg, b_invoke, null);
							
							
								// sm-RP-DA
								SM_RP_DA sm_rp_da = new SM_RP_DA();
								BerTag b_sm_rp_da = BerTag.createNew(sm_rp_da, b_mt_arg, null);
								
									// IMSI
									IMSI imsi = new IMSI();
									BerTag b_imsi = BerTag.createNew(imsi, b_sm_rp_da, sm_rp_da.elements.get(0));
									imsi.value = TBCD.encode((_imsi == null ? "0" : _imsi), 0x0f);
									
									
								// sm-RP-OA
								SM_RP_OA sm_rp_oa = new SM_RP_OA();
								BerTag b_sm_rp_oa = BerTag.createNew(sm_rp_oa, b_mt_arg, null);
								
									// serviceCentreAddressOA
									net.asn1.gsmmap2.AddressString scaoa = new net.asn1.gsmmap2.AddressString();
									BerTag b_scaoa = BerTag.createNew(scaoa, b_sm_rp_oa, sm_rp_oa.elements.get(1));
									as = new AddressString();
									as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
									as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
									as.digits = TBCD.encode((_scoa_val == null ? "0" : _scoa_val), 0x0f);
									scaoa.value = AddressString.encode(as);
								
									
								// sm-RP-UI
								SignalInfo sm_rp_ui = new SignalInfo();
								BerTag b_sm_rp_ui = BerTag.createNew(sm_rp_ui, b_mt_arg,  null);
								sm_rp_ui.value = sms_tpdu;
									
								
								

		// encode TCAP
		ber = new BerTranscoderv2();
		ber.clearLengths(b_root);
		ber.prepareLenghts(b_root);
		return ber.encode(b_root);		
	}		
	
	
	public static byte[] generate_MO_TCAP_MAP(String app_ctx_oid, long tcap_sid, String scda_val, String msisdn_val, long _invoke_id, byte[] sms_tpdu){
		AddressString as = null;
		BerTranscoderv2 ber = null;
		// TCM
		TCMessage tcm = new TCMessage();
		BerTag b_root = BerTag.createNew(tcm, null, null);
		
			// BEGIN
			Begin begin = new Begin();
			BerTag b_begin = BerTag.createNew(begin, b_root, tcm.elements.get(1));

			
				// Source Transaction ID
				OrigTransactionID sid = new OrigTransactionID();
				BerTag b_sid = BerTag.createNew(sid, b_begin, null);
				sid.value = Utils.num2bytes(tcap_sid); 

				
				//DialoguePortion
				DialoguePortion dportion = new DialoguePortion();
				BerTag b_dportion = BerTag.createNew(dportion, b_begin, null);
				
					// EXTERNAL
					EXTERNAL external = new EXTERNAL();
					BerTag b_external = BerTag.createNew(external, b_dportion, null);
						
						// OID
						OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
						BerTag b_oid = BerTag.createNew(oid, b_external, null);
						oid.value = Utils.encodeOID("0.0.17.773.1.1.1");
						
						// External Encoding
						EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
						BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
						
							// single asn1 type
							ASNType single_type = new ASNType();
							single_type.asn_pc = ASNTagComplexity.Constructed;
							BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
							
								// DialoguePDU
								DialoguePDU dpdu = new DialoguePDU();
								BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
								
									// DialogueRequest
									AARQ_apdu drq = new AARQ_apdu();
									BerTag b_drq = BerTag.createNew(drq, b_dpdu, dpdu.elements.get(0));
									
									
										// Protocol Version
										BIT_STRING pversion = new BIT_STRING();
										BerTag b_pversion = BerTag.createNew(pversion, b_drq, drq.elements.get(0));
										pversion.value = new byte[]{0x07, (byte)0x80};
										
										
										// Application Context Name EXPLICIT
										ASNType appctx_ex = new ASNType();
										appctx_ex.asn_pc = ASNTagComplexity.Constructed;
										BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_drq, drq.elements.get(1));
										
											// Application Context Name
											OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
											BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
											appctx.value = Utils.encodeOID(app_ctx_oid);
													
				
				
				// Components
				ComponentPortion components = new ComponentPortion();
				BerTag b_components = BerTag.createNew(components, b_begin, null);
	
					// Compoent
					Component component = new Component();
					BerTag b_component = BerTag.createNew(component, b_components, null);
	
						// Invoke
						Invoke invoke = new Invoke();
						BerTag b_invoke = BerTag.createNew(invoke, b_component, component.elements.get(0));
	
							// Invoke ID
							InvokeIdType invoke_id = new InvokeIdType();
							BerTag b_invoke_id = BerTag.createNew(invoke_id, b_invoke, null);
							invoke_id.value = Utils.num2bytes(_invoke_id);
	
							// opCode
							MAP_OPERATION opCode = new MAP_OPERATION();
							BerTag b_opCode = BerTag.createNew(opCode, b_invoke, null);
							
	
								// localValue
								OperationLocalvalue localValue = new OperationLocalvalue();
								BerTag b_localValue = BerTag.createNew(localValue, b_opCode, null);
								localValue.value = new byte[]{GSMMAPOperationLocalvalue._mo_forwardSM};
	
								
							// MO_ForwardSM_Arg
							MO_ForwardSM_Arg mo_arg = new MO_ForwardSM_Arg();
							BerTag b_mo_arg = BerTag.createNew(mo_arg, b_invoke, null);
							
							
								// sm-RP-DA
								SM_RP_DA sm_rp_da = new SM_RP_DA();
								BerTag b_sm_rp_da = BerTag.createNew(sm_rp_da, b_mo_arg, null);
								
									// serviceCentreAddressDA
									net.asn1.gsmmap2.AddressString scada = new net.asn1.gsmmap2.AddressString();
									BerTag b_scada = BerTag.createNew(scada, b_sm_rp_da, sm_rp_da.elements.get(2));
									as = new AddressString();
									as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
									as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
									as.digits = TBCD.encode(scda_val, 0x0f);
									scada.value = AddressString.encode(as);
									
									
								// sm-RP-OA
								SM_RP_OA sm_rp_oa = new SM_RP_OA();
								BerTag b_sm_rp_oa = BerTag.createNew(sm_rp_oa, b_mo_arg, null);
								
									// MSISDN
									ISDN_AddressString msisdn = new ISDN_AddressString();
									BerTag b_msisdn = BerTag.createNew(msisdn, b_sm_rp_oa, sm_rp_oa.elements.get(0));
									as = new AddressString();
									as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
									as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
									as.digits = TBCD.encode(msisdn_val, 0x0f);
									msisdn.value = AddressString.encode(as);
								
									
								// sm-RP-UI
								SignalInfo sm_rp_ui = new SignalInfo();
								BerTag b_sm_rp_ui = BerTag.createNew(sm_rp_ui, b_mo_arg,  null);
								sm_rp_ui.value = sms_tpdu;
									
								
								

		// encode TCAP
		ber = new BerTranscoderv2();
		ber.clearLengths(b_root);
		ber.prepareLenghts(b_root);
		return ber.encode(b_root);		
	}	
	
	public static byte[] generate_SMS_Abort(byte[] app_ctx_oid, long tcap_did, long invoke_id){
		byte[] res = null;
		try{
			// TCM
			TCMessage tcm = new TCMessage();
			BerTag b_root = BerTag.createNew(tcm, null, null);
			
				// END
				End end = new End();
				BerTag b_end = BerTag.createNew(end, b_root, tcm.elements.get(2));

					// Dest Transaction ID
					DestTransactionID did = new DestTransactionID();
					BerTag b_did = BerTag.createNew(did, b_end, null);
					did.value = Utils.num2bytes(tcap_did);
					
					// DialoguePortion
					DialoguePortion dportion = new DialoguePortion();
					BerTag b_dportion = BerTag.createNew(dportion, b_end, end.elements.get(1));
					
						// EXTERNAL
						EXTERNAL external = new EXTERNAL();
						BerTag b_external = BerTag.createNew(external, b_dportion, null);
		
							// OID
							OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
							BerTag b_oid = BerTag.createNew(oid, b_external, null);
							oid.value = new byte[]{0x00, 0x11, (byte)0x86, 0x05, 0x01, 0x01, 0x01};
		
				
							// External Encoding
							EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
							BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
				

								// single asn1 type
								ASNType single_type = new ASNType();
								single_type.asn_pc = ASNTagComplexity.Constructed;
								BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
					
									// DialoguePDU
									DialoguePDU dpdu = new DialoguePDU();
									BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
					
										// DialogueResponse
										AARE_apdu dresponse = new AARE_apdu(); 
										BerTag b_dresponse = BerTag.createNew(dresponse, b_dpdu, dpdu.elements.get(1));
										
											// Protocol Version
											BIT_STRING pversion = new BIT_STRING();
											BerTag b_pversion = BerTag.createNew(pversion, b_dresponse, dresponse.elements.get(0));
											pversion.value = new byte[]{0x07, (byte)0x80};
											
											// Application Context Name EXPLICIT
											ASNType appctx_ex = new ASNType();
											appctx_ex.asn_pc = ASNTagComplexity.Constructed;
											BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_dresponse, dresponse.elements.get(1));
											
												// Application Context Name
												OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
												BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
												appctx.value = app_ctx_oid;
											
											
											// Result EXPLICIT
											ASNType asres_ex = new ASNType();
											asres_ex.asn_pc = ASNTagComplexity.Constructed;
											BerTag b_asres_ex = BerTag.createNew(asres_ex, b_dresponse, dresponse.elements.get(2));
											
											
												// Result
												Associate_result asres = new Associate_result();
												BerTag b_asres = BerTag.createNew(asres, b_asres_ex, null);
												asres.value = Utils.num2bytes(Associate_result._accepted);
			
												
											// Source Diagnostic EXPLICIT
											ASNType asd_ex = new ASNType();
											asd_ex.asn_pc = ASNTagComplexity.Constructed;
											BerTag b_asd_ex = BerTag.createNew(asd_ex, b_dresponse, dresponse.elements.get(3));
												
												
												// Source Diagnostic
												Associate_source_diagnostic asd = new Associate_source_diagnostic();
												BerTag b_asd = BerTag.createNew(asd, b_asd_ex, null);
											
													// Dialogue service user EXLICIT
													ASNType dsui_ex = new ASNType();
													dsui_ex.asn_pc = ASNTagComplexity.Constructed;
													BerTag b_dsui_ex = BerTag.createNew(dsui_ex, b_asd, asd.elements.get(0));
													
												
														// Dialogue service user
														Dialogue_service_userInteger dsui = new Dialogue_service_userInteger();
														BerTag b_dsui = BerTag.createNew(dsui, b_dsui_ex, null);
														dsui.value = Utils.num2bytes(Dialogue_service_userInteger._null);
														
														
														
					// Components
					ComponentPortion components = new ComponentPortion();
					BerTag b_components = BerTag.createNew(components, b_end, end.elements.get(2));
						
						// Compoent
						Component component = new Component();
						BerTag b_component = BerTag.createNew(component, b_components, null);

							// ReturnError
							net.asn1.gsmmap2.ReturnError return_error = new net.asn1.gsmmap2.ReturnError();
							BerTag b_return_error = BerTag.createNew(return_error, b_components, component.elements.get(2));
							
								// Invoke ID
								InvokeIdType inv_id = new InvokeIdType();
								BerTag b_inv_id = BerTag.createNew(inv_id, b_return_error, null);
								inv_id.value = num2bytes(invoke_id);
							
								// MAP-ERROR
								MAP_ERROR map_err = new MAP_ERROR();
								BerTag b_map_err = BerTag.createNew(map_err, b_return_error, null);
								
									// Local Error
									LocalErrorcode local_error = new LocalErrorcode();
									BerTag b_local_error = BerTag.createNew(local_error, b_map_err, map_err.elements.get(0));
									local_error.value = num2bytes(GSMMAPLocalErrorcode._illegalSubscriber);
									/*
								// Call Baring cause
								ASNType call_baring_cause = new ASNType();
								ElementDescriptor esd = new ElementDescriptor(10, ASNTagClass.UNIVERSAL, true, false);
								BerTag b_call_baring_cause = BerTag.createNew(call_baring_cause, b_return_error, esd);
								call_baring_cause.value = num2bytes(0);
										*/
										
									
								
								
							
							
								
							
									

			
			// encode TCAP
			BerTranscoderv2 ber = new BerTranscoderv2();
			ber.clearLengths(b_root);
			ber.prepareLenghts(b_root);
			res = ber.encode(b_root);
									
			return res;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] generate_TCAP_Abort(byte[] app_ctx_oid, long tcap_did){
		byte[] res = null;
		try{
			// TCM
			TCMessage tcm = new TCMessage();
			BerTag b_root = BerTag.createNew(tcm, null, null);
			
				// ABORT
				Abort abort = new Abort();
				BerTag b_abort = BerTag.createNew(abort, b_root, tcm.elements.get(4));

					// Dest Transaction ID
					DestTransactionID did = new DestTransactionID();
					BerTag b_did = BerTag.createNew(did, b_abort, null);
					did.value = Utils.num2bytes(tcap_did);

					// Reason
					Reason reason = new Reason();
					BerTag b_reason = BerTag.createNew(reason, b_abort, abort.elements.get(1));
					
						// DialoguePortion
						DialoguePortion dportion = new DialoguePortion();
						BerTag b_dportion = BerTag.createNew(dportion, b_reason, reason.elements.get(1));
						
							// EXTERNAL
							EXTERNAL external = new EXTERNAL();
							BerTag b_external = BerTag.createNew(external, b_dportion, null);
			
								// OID
								OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
								BerTag b_oid = BerTag.createNew(oid, b_external, null);
								oid.value = new byte[]{0x00, 0x11, (byte)0x86, 0x05, 0x01, 0x01, 0x01};
			
					
								// External Encoding
								EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
								BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
					

									// single asn1 type
									ASNType single_type = new ASNType();
									single_type.asn_pc = ASNTagComplexity.Constructed;
									BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
						
										// DialoguePDU
										DialoguePDU dpdu = new DialoguePDU();
										BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
						
											// DialogueResponse
											AARE_apdu dresponse = new AARE_apdu(); 
											BerTag b_dresponse = BerTag.createNew(dresponse, b_dpdu, dpdu.elements.get(1));
											
												
												// Application Context Name EXPLICIT
												ASNType appctx_ex = new ASNType();
												appctx_ex.asn_pc = ASNTagComplexity.Constructed;
												BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_dresponse, dresponse.elements.get(1));
												
													// Application Context Name
													OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
													BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
													appctx.value = app_ctx_oid;
												
												
												// Result EXPLICIT
												ASNType asres_ex = new ASNType();
												asres_ex.asn_pc = ASNTagComplexity.Constructed;
												BerTag b_asres_ex = BerTag.createNew(asres_ex, b_dresponse, dresponse.elements.get(2));
												
												
													// Result
													Associate_result asres = new Associate_result();
													BerTag b_asres = BerTag.createNew(asres, b_asres_ex, null);
													asres.value = Utils.num2bytes(Associate_result._reject_permanent);
				
													
												// Source Diagnostic EXPLICIT
												ASNType asd_ex = new ASNType();
												asd_ex.asn_pc = ASNTagComplexity.Constructed;
												BerTag b_asd_ex = BerTag.createNew(asd_ex, b_dresponse, dresponse.elements.get(3));
													
													
													// Source Diagnostic
													Associate_source_diagnostic asd = new Associate_source_diagnostic();
													BerTag b_asd = BerTag.createNew(asd, b_asd_ex, null);
												
														// Dialogue service user EXLICIT
														ASNType dsui_ex = new ASNType();
														dsui_ex.asn_pc = ASNTagComplexity.Constructed;
														BerTag b_dsui_ex = BerTag.createNew(dsui_ex, b_asd, asd.elements.get(0));
														
													
															// Dialogue service user
															Dialogue_service_userInteger dsui = new Dialogue_service_userInteger();
															BerTag b_dsui = BerTag.createNew(dsui, b_dsui_ex, null);
															dsui.value = Utils.num2bytes(Dialogue_service_userInteger._no_reason_given);

			
			// encode TCAP
			BerTranscoderv2 ber = new BerTranscoderv2();
			ber.clearLengths(b_root);
			ber.prepareLenghts(b_root);
			res = ber.encode(b_root);
									
			return res;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static Object isNull(Object source, String nullVal){
		return (source == null ? nullVal : source);
		
	}
	public static int bytesToLst(ArrayList<Byte> buff, byte[] data){
		if(data != null){
			for(int i = 0; i<data.length; i++) buff.add(data[i]);
			return data.length;
		}
		return 0;
	}

	public static NumberingPlan get_called_gt_np(UDT_UnitData udt){
		if(udt.calledPartyAddress.globalTitle != null){
			GlobalTitleBase gt = udt.calledPartyAddress.globalTitle;;
			switch(udt.calledPartyAddress.globalTitle.type){
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING:
					return ((GlobalTitle_TTNPE)gt).numberingPlan;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					return ((GlobalTitle_TTNPENOA)gt).numberingPlan;
			}
		}
		return null;
	
	}
	public static NumberingPlan get_calling_gt_np(UDT_UnitData udt){
		if(udt.callingPartyAddress.globalTitle != null){
			GlobalTitleBase gt = udt.callingPartyAddress.globalTitle;;
			switch(udt.callingPartyAddress.globalTitle.type){
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING:
					return ((GlobalTitle_TTNPE)gt).numberingPlan;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					return ((GlobalTitle_TTNPENOA)gt).numberingPlan;
			}
			
		}
		return null;
	
	}
	
	public static NatureOfAddress get_called_gt_nai(UDT_UnitData udt){
		if(udt.calledPartyAddress.globalTitle != null){
			GlobalTitleBase gt = udt.calledPartyAddress.globalTitle;;
			switch(udt.calledPartyAddress.globalTitle.type){
				case NATURE_OF_ADDRESS_INDICATOR_ONLY:
					return ((GlobalTitle_NOA)gt).natureOfAddress;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					return ((GlobalTitle_TTNPENOA)gt).natureOfAddress;
			}
			
		}
		return null;
	}
	public static NatureOfAddress get_calling_gt_nai(UDT_UnitData udt){
		if(udt.callingPartyAddress.globalTitle != null){
			GlobalTitleBase gt = udt.callingPartyAddress.globalTitle;;
			switch(udt.callingPartyAddress.globalTitle.type){
				case NATURE_OF_ADDRESS_INDICATOR_ONLY:
					return ((GlobalTitle_NOA)gt).natureOfAddress;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					return ((GlobalTitle_TTNPENOA)gt).natureOfAddress;
			}
			
		}
		return null;
	}
	
	public static int get_called_gt_tt(UDT_UnitData udt){
		if(udt.calledPartyAddress.globalTitle != null){
			GlobalTitleBase gt = udt.calledPartyAddress.globalTitle;;
			switch(udt.calledPartyAddress.globalTitle.type){
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING:
					return ((GlobalTitle_TTNPE)gt).translationType;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					return ((GlobalTitle_TTNPENOA)gt).translationType;
				case TRANSLATION_TYPE_ONLY:
					return ((GlobalTitle_TT)gt).translationType;
			}
			
		}
		return -1;
	}
	public static int get_calling_gt_tt(UDT_UnitData udt){
		if(udt.callingPartyAddress.globalTitle != null){
			GlobalTitleBase gt = udt.callingPartyAddress.globalTitle;;
			switch(udt.callingPartyAddress.globalTitle.type){
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING:
					return ((GlobalTitle_TTNPE)gt).translationType;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					return ((GlobalTitle_TTNPENOA)gt).translationType;
				case TRANSLATION_TYPE_ONLY:
					return ((GlobalTitle_TT)gt).translationType;
			}
			
		}
		return -1;
	}
	
	
	public static String Cstr_decode(byte[] data, int pos){
		//String res = "";
		StringBuilder res = new StringBuilder();
		for(int i = pos; i<data.length; i++) if(data[i] != 0) res.append((char)data[i]); else break;
		return res.toString();
	}	
	
	public static byte[] Cstr_encode(String data){
		byte[] res = new byte[data.length() + 1];
		byte[] str_data = data.getBytes();
		for(int i = 0; i<str_data.length; i++) res[i] = str_data[i];
		res[str_data.length] = 0x00;
		return res;
	}	
	
	
	public static byte[] md5(String data){
		try{
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(data.getBytes());
			return digest.digest();		
		}catch(Exception e){
			//e.printStackTrace();
			//return null;
		}
		return null;
	}
	public static byte[] sms_md5(SmsfsPacket fp){
		//String tmp = "";
		StringBuilder tmp = new StringBuilder();
		try{
			switch(fp.filterType){
				case MO:
				case MT:
					for(int i = 0; i<6; i++) tmp.append((fp.bindings.get(i) == null ? "" : String.valueOf(fp.bindings.get(i))));
					for(int i = 8; i<14; i++) tmp.append((fp.bindings.get(i) == null ? "" : String.valueOf(fp.bindings.get(i))));
					break;
				case SMPP_MO:
				case SMPP_MT:
					tmp.append((String)fp.bindings.get(27));
					break;
			}
		}catch(Exception e){
			return null;
		}
		
		return md5(tmp.toString());
	}
	
	public static String mo_getSCDA(MO_ForwardSM_Arg mo){
		String res = null;
		AddressString as = null;
		// serviceCentreAddressDA
		if(mo.get_sm_RP_DA().get_serviceCentreAddressDA() != null){
			as = AddressString.decode(mo.get_sm_RP_DA().get_serviceCentreAddressDA().value);
			res = TBCD.decode(as.digits);
			
		}		
		return res;
	}
	
	public static String mo_getSCOA(MO_ForwardSM_Arg mo){
		String res = null;
		AddressString as = null;
		// serviceCentreAddressOA
		if(mo.get_sm_RP_OA().get_serviceCentreAddressOA() != null){
			as = AddressString.decode(mo.get_sm_RP_OA().get_serviceCentreAddressOA().value);
			res = TBCD.decode(as.digits);
			
		}		
		return res;
	}
	public static String mo_getMSISDN(MO_ForwardSM_Arg mo){
		String res = null;
		AddressString as = null;
		// MSISDN
		if(mo.get_sm_RP_OA().get_msisdn() != null){
			as = AddressString.decode(mo.get_sm_RP_OA().get_msisdn().value);
			res = TBCD.decode(as.digits);
			
		}
		return res;
	}

	public static String mo_getIMSI(MO_ForwardSM_Arg mo){
		String res = null;
		// IMSI
		if(mo.get_imsi() != null)
			res = TBCD.decode(mo.get_imsi().value);
		else if(mo.get_sm_RP_DA().get_imsi() != null)
			res = TBCD.decode(mo.get_sm_RP_DA().get_imsi().value);
		return res;
	}

	public static String mt_getSCDA(MT_ForwardSM_Arg mt){
		String res = null;
		AddressString as = null;
		// serviceCentreAddressDA
		if(mt.get_sm_RP_DA().get_serviceCentreAddressDA() != null){
			as = AddressString.decode(mt.get_sm_RP_DA().get_serviceCentreAddressDA().value);
			res = TBCD.decode(as.digits);
			
		}		
		return res;
	}
	public static String mt_getSCOA(MT_ForwardSM_Arg mt){
		String res = null;
		AddressString as = null;
		// serviceCentreAddressOA
		if(mt.get_sm_RP_OA().get_serviceCentreAddressOA() != null){
			as = AddressString.decode(mt.get_sm_RP_OA().get_serviceCentreAddressOA().value);
			res = TBCD.decode(as.digits);
			
		}		
		return res;
	}
	public static String mt_getMSISDN(MT_ForwardSM_Arg mt){
		String res = null;
		AddressString as = null;
		// MSISDN
		if(mt.get_sm_RP_OA().get_msisdn() != null){
			as = AddressString.decode(mt.get_sm_RP_OA().get_msisdn().value);
			res = TBCD.decode(as.digits);
			
		}
		return res;
	}
	
	public static String mt_getIMSI(MT_ForwardSM_Arg mt){
		String res = null;
		// IMSI
		if(mt.get_sm_RP_DA().get_imsi() != null) res = TBCD.decode(mt.get_sm_RP_DA().get_imsi().value);
		return res;
	}
	
	public static String SMS_DELIVER_get_TP_OA(SmsDeliver sms){
		String res = null;
		switch(sms.TP_OA.typeOfNumber){
			case ALPHANUMERIC:
				res = GSMAlphabet.decode(sms.TP_OA.digits);							
				break;
			default:
				res = TBCD.decode(sms.TP_OA.digits);							
				break;
		}
		return res;
		
	}

	public static String SMS_SUBMIT_get_TP_DA(SmsSubmit sms){
		String res = null;
		switch(sms.TP_DA.typeOfNumber){
			case ALPHANUMERIC:
				res = GSMAlphabet.decode(sms.TP_DA.digits);
				break;
			default:
				res = TBCD.decode(sms.TP_DA.digits);
				break;
		}
		return res;
		
	}
	public static String SMS_TPDU_decodeUD(TPDCS dcs, byte[] data, int udl){
		String res = null;
		switch(dcs.encoding){
			case DEFAULT: 
				res = GSMAlphabet.decode(data, udl); 
				break;
			case _8BIT:
				//res = Utils.getPrintable(data);
				res = Utils.bytes2hexstr(data, "");
				break;
			case UCS2:
				try{ res = new String(data, "UTF-16"); }catch(Exception e){ res = "<UNKNOWN ENCODING>"; }
				break;
				
		}
		
		return res;
	}
	
	public static String SMS_TPDU_decodeUD_conc(TPDCS dcs, byte[] data, int udh_length){
		String res = null;
		switch(dcs.encoding){
			case DEFAULT: 
				res = GSMAlphabet.decodePadded(data, udh_length); 
				break;
			case _8BIT:
				res = Utils.getPrintable(data);
				break;
			case UCS2:
				try{ res = new String(data, "UTF-16"); }catch(Exception e){ res = "<UNKNOWN ENCODING>"; }
				break;
				
		}
		
		return res;
	}
	
	
	public static String date2str(Calendar c){
		String res = c.get(Calendar.YEAR) + "-" + String.format("%02d", (c.get(Calendar.MONTH) + 1)) + "-" + String.format("%02d", c.get(Calendar.DAY_OF_MONTH));
		return res;
		
	}
	public static boolean is_ascii(byte[] data){
	    CharsetDecoder d = Charset.forName("US-ASCII").newDecoder();
	    try{
		    d.decode(ByteBuffer.wrap(data));
	    }catch(CharacterCodingException e){
	    	return false;
	    }
	    return true;
	}

	public static String getPrintable(byte[] data){
		//String res = "";
		StringBuilder res = new StringBuilder();
		for(int i = 0; i<data.length; i++){
			if(data[i] >= 32 && data[i] < 127) res.append((char)data[i]);
			else res.append(".");
		}
		return res.toString();
		
	}
	public static String tsElapsed2String(long ts){
		String res = "";
		int d, h, m, s;
		ts /= 1000; // seconds
		d = (int)(ts / 86400);
		h = (int)(ts % 86400 / 3600);
		m = (int)(ts % 86400 % 3600 / 60);
		s = (int)(ts % 86400 % 3600 % 60 );
		res = d + " day(s), " + String.format("%02d", h) + ":" + String.format("%02d", m) + ":" + String.format("%02d", s);
		return res;
	}


	
	public static boolean isTCAP(byte[] data){
		if(data != null){
			if(data.length > 0){
				if(		((data[0] & 0xff) == 0x61) || 
						((data[0] & 0xff) == 0x62) || 
						((data[0] & 0xff) == 0x64) || 
						((data[0] & 0xff) == 0x65) || 
						((data[0] & 0xff) == 0x67)) return true;
				
			}
		}
		return false;
	}
	
	public static boolean isContinue(TCMessage tcm){
		return tcm.get_continue() != null;
	}
	
	
	public static boolean isBegin(TCMessage tcm){
		return tcm.get_begin() != null;
	}
	public static boolean isEnd(TCMessage tcm){
		return tcm.get_end() != null;
	}
	public static boolean isAbort(TCMessage tcm){
		return tcm.get_abort() != null;
	}

	public static boolean isReportSM_DeliveryStatus(TCMessage tcm){
		ASNType localValue = null;
		if(tcm.get_begin() != null){
			if(tcm.get_begin().get_components() != null){
				for(int i = 0; i<tcm.get_begin().get_components().of_children.size(); i++){
					if(tcm.get_begin().get_components().getChild(i).get_invoke() != null){
						if(tcm.get_begin().get_components().getChild(i).get_invoke().get_opCode().get_localValue() != null){
							localValue = tcm.get_begin().get_components().getChild(i).get_invoke().get_opCode().get_localValue();
							if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._reportSM_DeliveryStatus) return true;
						}
					}
					
				}
			}else if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_invoke() != null){
						if(tcm.get_continue().get_components().getChild(i).get_invoke().get_opCode().get_localValue() != null){
							localValue = tcm.get_continue().get_components().getChild(i).get_invoke().get_opCode().get_localValue();
							if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._reportSM_DeliveryStatus) return true;
						}
					}
					
				}
				
			}else if(tcm.get_end().get_components() != null){
				for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
					if(tcm.get_end().get_components().getChild(i).get_invoke() != null){
						if(tcm.get_end().get_components().getChild(i).get_invoke().get_opCode().get_localValue() != null){
							localValue = tcm.get_end().get_components().getChild(i).get_invoke().get_opCode().get_localValue();
							if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._reportSM_DeliveryStatus) return true;
						}
					}
					
				}
				
				
			}
		}
	
	
		return false;
	}
	public static boolean isEnd_CH_SRI(TCMessage tcm){
		ASNType localValue = null;
		ResultRetRes retres = Utils.get_RETURN_RESULT_LAST_RETRES(tcm);
		if(retres != null && tcm.get_end() != null){
			if(retres.get_opCode().get_localValue() != null){
				localValue = retres.get_opCode().get_localValue();
				if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfo) return true;
			}
		}
		return false;
	}
	
	public static boolean isBegin_CH_SRI(TCMessage tcm){
		if(tcm != null){
			ASNType localValue = null;
			Invoke invoke = Utils.get_INVOKE(tcm);
			if(invoke != null && tcm.get_begin() != null){
				if(invoke.get_opCode() != null){
					if(invoke.get_opCode().get_localValue() != null){
						localValue = invoke.get_opCode().get_localValue();
						if(localValue.value.length > 0){
							if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfo) return true;
						}
					}
				}
			}			
		}
		return false;

	}	
	
	
	public static boolean isBeginSRI(TCMessage tcm){
		if(tcm != null){
			ASNType localValue = null;
			Invoke invoke = Utils.get_INVOKE(tcm);
			if(invoke != null && tcm.get_begin() != null){
				if(invoke.get_opCode() != null){
					if(invoke.get_opCode().get_localValue() != null){
						localValue = invoke.get_opCode().get_localValue();
						if(localValue.value.length > 0){
							if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfoForSM) return true;
						}
					}
				}
			}			
		}

		/*
		if(tcm.get_begin() != null){
			if(tcm.get_begin().get_components() != null){
				if(tcm.get_begin().get_components().getChild(0).get_invoke() != null){
					if(tcm.get_begin().get_components().getChild(0).get_invoke().get_opCode().get_localValue() != null){
						localValue = tcm.get_begin().get_components().getChild(0).get_invoke().get_opCode().get_localValue();
						if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfoForSM) return true;
					}
				}
			}
		}
		*/
		return false;
		
	}
	public static boolean isEndSRI(TCMessage tcm){
		ASNType localValue = null;
		ResultRetRes retres = Utils.get_RETURN_RESULT_LAST_RETRES(tcm);
		if(retres != null && tcm.get_end() != null){
			if(retres.get_opCode().get_localValue() != null){
				localValue = retres.get_opCode().get_localValue();
				if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfoForSM) return true;
			}
		}		
		/*
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() != null){
				if(tcm.get_end().get_components().getChild(0).get_returnResultLast() != null){
					if(tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres() != null){
						if(tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_opCode().get_localValue() != null){
							localValue = tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_opCode().get_localValue();
							if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfoForSM) return true;
						}
					}
				}
			}
		}
		*/
		return false;
		
	}
	public static boolean isContinueRequestSRI(TCMessage tcm){
		ASNType localValue = null;
		Invoke invoke = Utils.get_INVOKE(tcm);
		if(invoke != null && tcm.get_continue() != null){
			if(invoke.get_opCode().get_localValue() != null){
				localValue = invoke.get_opCode().get_localValue();
				if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfoForSM) return true;
			}
		
		}		
		/*
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				if(tcm.get_continue().get_components().getChild(0).get_invoke() != null){
					if(tcm.get_continue().get_components().getChild(0).get_invoke().get_opCode().get_localValue() != null){
						localValue = tcm.get_continue().get_components().getChild(0).get_invoke().get_opCode().get_localValue();
						if((localValue.value[0] & 0xFF) == GSMMAPOperationLocalvalue._sendRoutingInfoForSM) return true;
					}
				}
			}
		}
		*/
		return false;
		
		
	}
	public static boolean isTcapAbort(TCMessage tcm){
		return tcm.get_abort() != null;
		
	}

	public static boolean isEndERRSRI(TCMessage tcm){
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() != null){
				for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
					if(tcm.get_end().get_components().getChild(i).get_returnError() != null) return true;
					
				}
			}
		}
		return false;
		
	}
	public static boolean isContinueERRSRI(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_returnError() != null) return true;
				
				}
			}
		}
		return false;
		
	}
	public static boolean isBeginMO(TCMessage tcm){
		if(tcm.get_begin() != null){
			if(tcm.get_begin().get_components() != null){
				for(int i = 0; i<tcm.get_begin().get_components().of_children.size(); i++){
					if(tcm.get_begin().get_components().getChild(i).get_invoke() != null) return true;
				
				}
			}
		}
		return false;
	}
	public static boolean isContinueMO(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_invoke() != null) return true;
				
				}				
			
			}			
		}
		return false;
	}
	public static boolean isEndMO(TCMessage tcm){
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() != null){
				for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
					if(tcm.get_end().get_components().getChild(i).get_invoke() != null) return true;
				
				}
			
			}			
		}
		return false;
	}

	
	
	public static boolean isBeginMT(TCMessage tcm){
		if(tcm.get_begin() != null){
			if(tcm.get_begin().get_components() != null){
				for(int i = 0; i<tcm.get_begin().get_components().of_children.size(); i++){
					if(tcm.get_begin().get_components().getChild(i).get_invoke() != null) return true;
				
				}
				
			}
		}
		return false;
	}
	public static boolean isContinueMT(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_invoke() != null) return true;
				
				}
			
			}			
		}
		return false;
	}
	public static boolean isEndMT(TCMessage tcm){
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() != null){
				for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
					if(tcm.get_end().get_components().getChild(i).get_invoke() != null) return true;
				
				}
			
			}			
		}
		return false;
	}

	public static boolean isTCAPOnlyBegin(TCMessage tcm){
		if(tcm.get_begin() != null){
			if(tcm.get_begin().get_components() == null) return true;
		}
		return false;
	}

	public static boolean isTCAPOnlyContinue(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() == null) return true;
		}
		return false;
	}

	public static boolean isTCAPOnlyEnd(TCMessage tcm){
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() == null) return true;
		}
		return false;
	}

	
	
	public static boolean isContinueReturnResultLast(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_returnResultLast() != null) return true;
				
				}				
			}
		}
		return false;
	
	}	
	public static boolean isContinueReturnResultNotLast(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_returnResultNotLast() != null) return true;
				
				}
			}
		}
		return false;
	
	}	
	
	public static boolean isContinueReturnError(TCMessage tcm){
		if(tcm.get_continue() != null){
			if(tcm.get_continue().get_components() != null){
				for(int i = 0; i<tcm.get_continue().get_components().of_children.size(); i++){
					if(tcm.get_continue().get_components().getChild(i).get_returnError()!= null) return true;
				
				}
			}
		}
		return false;
	
	}	
	
	public static boolean isReturnResultLast(TCMessage tcm){
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() != null){
				for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
					if(tcm.get_end().get_components().getChild(i).get_returnResultLast() != null) return true;
				
				}
			}
		}
		return false;
	}

	public static boolean isReturnError(TCMessage tcm){
		if(tcm.get_end() != null){
			if(tcm.get_end().get_components() != null){
				for(int i = 0; i<tcm.get_end().get_components().of_children.size(); i++){
					if(tcm.get_end().get_components().getChild(i).get_returnError() != null) return true;
				
				}
			}			
		}
		return false;
	}

	public static int semiOctetDecode(byte octet){
		return ((octet & 0x0f) * 10) + ((octet & 0xf0) >> 4);
	}
	public static byte semiOctetEncode(int value){
		byte[] tmp = Integer.toString(value).getBytes();
		int v1;
		int v2;
		if(tmp.length == 1){
			v2 = Integer.parseInt(String.valueOf((char)tmp[0]));
			v1 = 0;
			
		}else{
			v1 = Integer.parseInt(String.valueOf((char)tmp[0]));
			v2 = Integer.parseInt(String.valueOf((char)tmp[1]));
		}
		return (byte)((v2 << 4) + v1);
	}
	public static String getHexStr(byte[] data){
		//String res = "";
		StringBuilder res = new StringBuilder();
		for(int i = 0; i<data.length; i++){
			res.append(String.format("%02x", data[i]));
			res.append(":");
		}
		return res.toString();
	}
	
	public static boolean[] bytes2bits(byte[] input){
		boolean[] out = new boolean[input.length * 8]; //byte = 8 bits 
		byte tmp;
		boolean[] bits;
		for(int i = 0; i<input.length; i++){
			tmp = input[i];  // for example 00100101
			bits = byte2bits(tmp);
			for(int j = 0; j<bits.length; j++) out[i*8 + j] = bits[j];
			
		}
		
		return out;
		
	}
	public static byte[] bits2bytes(boolean[] input){
		byte[] out = new byte[(int)Math.ceil((double)input.length/8)];
		int tmp;
		int rc = 0;
		for(int i = 0; i<input.length; i+=8){
			tmp = bits2int(input, i, 8);
			out[rc] = (byte)tmp;
			rc++;
			
		}
		
		return out;
		
	}
	public static byte[] bits2bytes_reverse(boolean[] input){
		byte[] out = new byte[(int)Math.ceil((double)input.length/8)];
		int tmp;
		int rc = 0;
		for(int i = 0; i<input.length; i+=8){
			tmp = bits2int(input, input.length - i - 8, 8);
			out[rc] = (byte)tmp;
			rc++;
			
		}
		
		return out;
		
	}



	public static int bits2int(boolean[] source, int position, int length){
		int res = 0;
		int pos;
		//System.out.println("L :" + source.length);
		//System.out.println("P :" + position);
		if(position < 0) position = 0;
		for(int i = 0; i<length; i++){
			//power of 2
			pos = (int)Math.pow(2, length - i - 1);
			// bitwise OR for bit 1, 0 | 1 = 1
			// bitwise AND with inverted bits "~" for bit 0, 0110 & ~1001 =  0110 & 0110 = 0110 
			if(position + i < source.length){
				if(source[position + i]) res = res | pos; else res = res & ~pos;
			}
			
		}			
		return res;
		
		
	}
	public static boolean[] bitsCombine(boolean[] src1, boolean[] src2){
		int src1_l = (src1 != null ? src1.length : 0);
		int src2_l = (src2 != null ? src2.length : 0);
		boolean[] res = new boolean[src1_l + src2_l];
		if(src1 != null){
			for(int i = 0; i<src1_l; i++) res[i] = src1[i];
		}
		if(src2 != null){
			for(int i = 0; i<src2_l; i++) res[i + src1_l] = src2[i];
			
		}
		return res;
		
	}

	public static boolean[] byte2bits(byte input){
		boolean[] out = new boolean[8];
		out[0] = (input & 0x80) > 0;
		out[1] = (input & 0x40) > 0;
		out[2] = (input & 0x20) > 0;
		out[3] = (input & 0x10) > 0;
		out[4] = (input & 0x08) > 0;
		out[5] = (input & 0x04) > 0;
		out[6] = (input & 0x02) > 0;
		out[7] = (input & 0x01) > 0;
		return out;
		
	}
	
	public static void bits_print(boolean[] input){
		for(int i = 0; i<input.length; i++) System.out.print(input[i] ? "1" : "0");
		System.out.println();
	}
	public static int int_required_bits(int input){
		return (int)Math.ceil(Math.log10(input + 1) / Math.log10(2));
	}
	
	public static boolean[] int2bits(int input, int max_bit_count){
		boolean[] out = new boolean[max_bit_count];
		// num of octets
		int bits = (int)Math.ceil(Math.log10(input + 1) / Math.log10(2));
		int bc = bits;
		int pow;
		for(int i = max_bit_count - bits; i<max_bit_count; i++ ){
			pow = (int)Math.pow(2, bc - 1);
			out[i] = (input & pow) == pow;
			bc--;
			
		}
		return out;
		
	}
	
	public static byte[] list2array(ArrayList<Byte> input){
		if(input != null){
			byte[] res = new byte[input.size()];
			for(int i = 0; i<input.size(); i++) res[i] = input.get(i);
			return res;
		}
		return null;
	}
	
	
	
	public static byte[] encodeOID(String data){
		byte[] res = null;
		byte[] tmp_b = null;
		boolean[] bits = null;
		boolean[] bits_oid = new boolean[8];
		int c = 7;
		int tmp;
		int tmp_p = 0;
		ArrayList<Byte> tmp_data = new ArrayList<Byte>();
		String[] ssplit = data.split("\\.");

		// first two
		tmp = Integer.parseInt(ssplit[0]);
		tmp_data.add((byte)((tmp * 40) + Integer.parseInt(ssplit[1])));
		
		// others
		for(int i = 2; i<ssplit.length; i++){
			tmp = Integer.parseInt(ssplit[i]);
			tmp_b = Utils.num2bytes(tmp);
			bits = bytes2bits(tmp_b);
			c = 7;
			tmp_p = tmp_data.size();
			for(int j = bits.length-1; j>=0; j--){
				if(c > 0){
					bits_oid[c] = bits[j];
					c--; 
				}else{
					bits_oid[c] = (j < bits.length - 8 ? true : false);
					if(tmp_b.length > 1){
						tmp_data.add(tmp_p, (byte)bits2int(bits_oid, 0, 8));
						bits_oid[7] = bits[j];
						c = 6;
					}else tmp_data.add((byte)bits2int(bits_oid, 0, 8));
				}
			}
		}
		res = new byte[tmp_data.size()];
		for(int i = 0; i<tmp_data.size(); i++) res[i] = tmp_data.get(i);
		//System.out.println(Utils.getHexStr(res));
		return res;
	}

	public static String decodeOID(byte[] data){
		String res = "";
		int tmp;
		int moreOctets = 0;
		ArrayList<Byte> tmp_data = new ArrayList<Byte>();
		// first two
		tmp = data[0] & 0xff;
		res += (tmp / 40) + ".";
		res += (tmp % 40) + ".";
		// others
		for(int i = 1; i<data.length; i++){
			moreOctets = data[i] & 0x80;
			tmp = data[i] & 0x7f;
			tmp_data.add((byte)tmp);
			if(moreOctets == 0){
				tmp = 0x00;
				for(int j = 0; j<tmp_data.size(); j++) tmp += tmp_data.get(j) << 7*(tmp_data.size() - j -1);
				res += tmp + ".";
				tmp_data.clear();
			}
		}
		// remove last dot
		res = res.substring(0, res.length() - 1);
		//System.out.println(res);
		return res;
		
	}
	public static int getTcapAbortErrorCode(TCMessage tcm){
		int res = -1;
		if(tcm.get_abort() != null){
			if(tcm.get_abort().get_reason() != null){
				if(tcm.get_abort().get_reason().get_p_abortCause() != null){
					res = (int)Utils.bytes2num(tcm.get_abort().get_reason().get_p_abortCause().value);
				}
			}
		}
		
		return res;
	}

	public static int get_GSMMAP_ERROR_CODE(TCMessage tcm){
		ReturnError re = get_RETURN_ERROR(tcm);
		BerTranscoderv2 ber = new BerTranscoderv2();
		if(re != null){
			net.asn1.gsmmap2.ReturnError returnErrorGSMMAP = (net.asn1.gsmmap2.ReturnError)ber.decode(new net.asn1.gsmmap2.ReturnError(), re.getDataBytes());
			if(returnErrorGSMMAP != null){
				if(returnErrorGSMMAP.get_errorCode() != null){
					// known error
					if(returnErrorGSMMAP.get_errorCode().get_localValue() != null){
						return (int)Utils.bytes2num(returnErrorGSMMAP.get_errorCode().get_localValue().value);
					// Error, but unknown error code	
					}else{
						return 9999;
					}
				}
				
			}
			
		}
		return -1;
	}
	
	public static int getTcapAbortDialogueError(TCMessage tcm){
		int res = -1;
		DialoguePDU dpdu = null;
		BerTranscoderv2 ber = new BerTranscoderv2();
		DialoguePortion dportion = null;
		if(tcm != null){
			if(tcm.get_abort() != null){
				if(tcm.get_abort().get_reason() != null){
					if((dportion = tcm.get_abort().get_reason().get_u_abortCause()) != null){
						if(dportion != null){
							// EXTERNAL
							EXTERNAL ext = dportion.get_DialoguePortion();
							if(ext != null){
								// EXTERNAL ENCODING
								EXTERNAL_ENCODING ext_enc = ext.get_encoding();
								if(ext_enc != null){
									// SINGLE ASN.1 TYPE
									ASNType ext_enc_single = ext_enc.get_single_asn1_type();
									if(ext_enc_single != null){
										// Dialogue PDU
										byte[] ext_enc_single_data = ext_enc_single.value;
										if(ext_enc_single_data != null){
											dpdu = (DialoguePDU)ber.decode(new DialoguePDU(), ext_enc_single_data);
											if(dpdu.get_dialogueResponse() != null){
												if(dpdu.get_dialogueResponse().get_result_source_diagnostic().get_result_source_diagnostic().get_dialogue_service_user() != null){
													res = (int)Utils.bytes2num(dpdu.get_dialogueResponse().get_result_source_diagnostic().get_result_source_diagnostic().get_dialogue_service_user().get_dialogue_service_user().value);
												}else if(dpdu.get_dialogueResponse().get_result_source_diagnostic().get_result_source_diagnostic().get_dialogue_service_provider() != null){
													res = (int)Utils.bytes2num(dpdu.get_dialogueResponse().get_result_source_diagnostic().get_result_source_diagnostic().get_dialogue_service_provider().value);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}			
		}

		return res;
	}
	public static long getTcap_sid(TCMessage tcm){
		if(tcm.get_begin() != null){
			return bytes2num(tcm.get_begin().get_otid().value);
		}else if(tcm.get_continue() != null){
			return bytes2num(tcm.get_begin().get_otid().value);
		}		
		return -1;
	}
	
	public static long getTcap_did(TCMessage tcm){
		if(tcm != null){
			if(tcm.get_continue() != null){
				return bytes2num(tcm.get_continue().get_dtid().value);
			}else if(tcm.get_end() != null){
				return bytes2num(tcm.get_end().get_dtid().value);
			}else if(tcm.get_abort() != null){
				return bytes2num(tcm.get_abort().get_dtid().value);
			}
			
		}
		return -1;
	}
	
	public static byte[] getTcapDialogueAppCtx(TCMessage tcm){
		if(tcm != null){
			if(tcm.get_begin() != null){
				if(tcm.get_begin().get_dialoguePortion() != null) return getTcapDialogueAppCtx(tcm.get_begin().get_dialoguePortion());
			}else if(tcm.get_continue() != null){
				if(tcm.get_continue().get_dialoguePortion() != null) return getTcapDialogueAppCtx(tcm.get_continue().get_dialoguePortion());
			}else if(tcm.get_end() != null){
				if(tcm.get_end().get_dialoguePortion() != null) return getTcapDialogueAppCtx(tcm.get_end().get_dialoguePortion());
				
			}
		}
		return null;
	}

	
	
	public static byte[] getTcapDialogueAppCtx(DialoguePortion dportion){
		byte[] res = null;
		if(dportion != null){
			DialoguePDU dpdu = null;
			BerTranscoderv2 ber = new BerTranscoderv2();
			
			// EXTERNAL
			EXTERNAL ext = dportion.get_DialoguePortion();
			// EXTERNAL ENCODING
			EXTERNAL_ENCODING ext_enc = ext.get_encoding();
			// SINGLE ASN.1 TYPE
			ASNType ext_enc_single = ext_enc.get_single_asn1_type();
			if(ext_enc != null){
				// Dialogue PDU
				byte[] ext_enc_single_data = ext_enc_single.value;
				if(ext_enc_single_data != null){
					dpdu = (DialoguePDU)ber.decode(new DialoguePDU(), ext_enc_single_data);
					// decode OID 
					if(dpdu.get_dialogueRequest() != null){
						res = dpdu.get_dialogueRequest().get_application_context_name().get_application_context_name().value;
					}else if(dpdu.get_dialogueResponse() != null){
						res = dpdu.get_dialogueResponse().get_application_context_name().get_application_context_name().value;
					}
					
				}
				
			}
			return res;
		}
		return null;
	}
	
	
	public static long generateImsiCorrelation(){
		long res = 0;
		
		return res;
	}
	public static byte[] strhex2bytes(String hex){
		byte[] res = new byte[hex.length() / 2];
		for(int i = 0; i<res.length; i++) res[i] = (byte)Integer.parseInt(hex.substring(i*2, (i*2) + 2), 16);
		
		return res;
		
	}

	public static String bytes2hexstr(byte[] data, String separator){
		//String res = "";
		StringBuilder res = new StringBuilder();
		for(int i = 0; i<data.length - 1; i++){
			res.append(String.format("%02x", data[i]));
			res.append(separator);
		}
		res.append(String.format("%02x", data[data.length - 1]));
//		if(separator.length() > 0) return res.substring(0, res.length() - separator.length());
		return res.toString();
		
	}

	
	public static byte[] num2bytes(long l){
		int ttb = (int)Math.ceil(Math.log10((l > 0 ? l : 1) + 1) / Math.log10(2));
		int tl = (int)Math.ceil((double)ttb / 8);
		byte[] res = new byte[tl];
		
		for(int i = 0; i<tl; i++){
			res[i] = (byte)(l >> (8*(tl - i - 1)));
		}
		return res;
		
	}
	
	public static long bytes2num(byte[] data){
		long res = 0;
		if(data != null){
			int l = data.length;
			for(int i = 0; i<data.length; i++) res += (long)(data[i] & 0xFF )<< ((l - i - 1) * 8);
		}
		return res;
		
	}
	
	public static boolean isMO(TCMessage tcm){
		//boolean res = false;
		Invoke invoke = Utils.get_INVOKE(tcm);

		if(invoke != null){
			if(invoke.get_opCode() != null){
				if(invoke.get_opCode().get_localValue() != null){
					if((invoke.get_opCode().get_localValue().value.length > 0)){
						if((invoke.get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mo_forwardSM) return true;
						
					}
				
				}				
			}
		}
		/*
		// Begin
		if(isBegin(tcm)){
			if(tcm.get_begin().get_components() != null){
				if(tcm.get_begin().get_components().of_children.size() > 0){
					if(tcm.get_begin().get_components().getChild(0).get_invoke() != null){
						if((tcm.get_begin().get_components().getChild(0).get_invoke().get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mo_forwardSM)
							res = true;
					}
				}
			}
		// Continue
		}else if(isContinue(tcm)){
			if(tcm.get_continue().get_components() != null){
				if(tcm.get_continue().get_components().of_children.size() > 0){
					if(tcm.get_continue().get_components().getChild(0).get_invoke() != null){
						if((tcm.get_continue().get_components().getChild(0).get_invoke().get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mo_forwardSM)
							res = true;
					}
				}
			}
			
		}else if(isEnd(tcm)){
			if(tcm.get_end().get_components() != null){
				if(tcm.get_end().get_components().of_children.size() > 0){
					if(tcm.get_end().get_components().getChild(0).get_invoke() != null){
						if((tcm.get_end().get_components().getChild(0).get_invoke().get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mo_forwardSM)
							res = true;
					}
				}
			}
			
		}
		return res;
		*/
		return false;
	}
	
	
	public static boolean isMT(TCMessage tcm){
		//boolean res = false;
		if(tcm != null){
			Invoke invoke = Utils.get_INVOKE(tcm);

			if(invoke != null){
				if(invoke.get_opCode() != null){
					if(invoke.get_opCode().get_localValue() != null){
						if((invoke.get_opCode().get_localValue().value.length > 0)){
							if((invoke.get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mt_forwardSM) return true;
						}
					}
				}
			}
		
		}
		
		/*
		// Begin
		if(isBegin(tcm)){
			if(tcm.get_begin().get_components() != null){
				if(tcm.get_begin().get_components().of_children.size() > 0){
					if(tcm.get_begin().get_components().getChild(0).get_invoke() != null){
						if((tcm.get_begin().get_components().getChild(0).get_invoke().get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mt_forwardSM)
							res = true;
					}
				}
			}
		// Continue
		}else if(isContinue(tcm)){
			if(tcm.get_continue().get_components() != null){
				if(tcm.get_continue().get_components().of_children.size() > 0){
					if(tcm.get_continue().get_components().getChild(0).get_invoke() != null){
						if((tcm.get_continue().get_components().getChild(0).get_invoke().get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mt_forwardSM)
							res = true;
					}
				}
			}
		}else if(isEnd(tcm)){
			if(tcm.get_end().get_components() != null){
				if(tcm.get_end().get_components().of_children.size() > 0){
					if(tcm.get_end().get_components().getChild(0).get_invoke() != null){
						if((tcm.get_end().get_components().getChild(0).get_invoke().get_opCode().get_localValue().value[0] & 0xFF) == GSMMAPOperationLocalvalue._mt_forwardSM)
							res = true;
					}
				}
			}
			
		}
		return res;
		*/
		return false;
	}

	
	
	public static String getGT(GlobalTitleBase _gt){
		String res = "";
		if(_gt instanceof GlobalTitle_TTNPENOA){
			GlobalTitle_TTNPENOA gt = (GlobalTitle_TTNPENOA)_gt;
			if(gt != null){
				if(gt.encodingScheme == EncodingScheme.BCD_EVEN){
					res = TBCD.decode(gt.addressInformation);
				} else if(gt.encodingScheme == EncodingScheme.BCD_ODD){
					res = TBCD.decodeOdd(gt.addressInformation);
				}else res = "<UNKNOWN_GT_CODING_[" + gt.encodingScheme + "]>";
			
			}
			
		}else if(_gt instanceof GlobalTitle_NOA){
			GlobalTitle_NOA gt = (GlobalTitle_NOA)_gt;
			if(gt != null){
				if(gt.oddNumOfAddrSignals) res = TBCD.decodeOdd(gt.addressSignals);
				else res = TBCD.decode(gt.addressSignals);
			}
		}else if(_gt instanceof GlobalTitle_TT){
			GlobalTitle_TT gt = (GlobalTitle_TT)_gt;
			if(gt != null){
				res = "<UNKNOWN_GT_CODING_[" + gt.translationType + "]>";//TBCD.decode(gt.addressInformation);
			}
		}else if(_gt instanceof GlobalTitle_TTNPE){
			GlobalTitle_TTNPE gt = (GlobalTitle_TTNPE)_gt;
			if(gt != null){
				if(gt.encodingScheme == EncodingScheme.BCD_EVEN){
					res = TBCD.decode(gt.addressInformation);
				} else if(gt.encodingScheme == EncodingScheme.BCD_ODD){
					res = TBCD.decodeOdd(gt.addressInformation);
				}else res = "<UNKNOWN_GT_CODING_[" + gt.encodingScheme + "]>";
			}
		}
		
		return res;
		
	}

}
