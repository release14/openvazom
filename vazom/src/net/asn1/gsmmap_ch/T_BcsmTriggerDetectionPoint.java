package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class T_BcsmTriggerDetectionPoint extends ENUMERATED{

	public static final int _termAttemptAuthorized = 12;
	public static final int _tBusy = 13;
	public static final int _tNoAnswer = 14;
	public T_BcsmTriggerDetectionPoint(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
