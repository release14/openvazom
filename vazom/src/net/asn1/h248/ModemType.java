package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ModemType extends ENUMERATED{

	public static final int _v18 = 0;
	public static final int _v22 = 1;
	public static final int _v22bis = 2;
	public static final int _v32 = 3;
	public static final int _v32bis = 4;
	public static final int _v34 = 5;
	public static final int _v90 = 6;
	public static final int _v91 = 7;
	public static final int _synchISDN = 8;
	public ModemType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
