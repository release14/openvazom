package net.isup.parameters.gn;

import java.util.HashMap;



public enum ScreeningIndicatorType {
	USER_PROVIDED_NOT_VERIFIED(0x00),
	USER_PROVIDED_VERIFIED_PASSED(0x01),
	USER_PROVIDED_VERIFIED_FAILED(0x02),
	NETWORK_PROVIDED(0x03);
	
	
	
	private int id;
	private static final HashMap<Integer, ScreeningIndicatorType> lookup = new HashMap<Integer, ScreeningIndicatorType>();
	static{
		for(ScreeningIndicatorType td :ScreeningIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ScreeningIndicatorType get(int id){ return lookup.get(id); }
	private ScreeningIndicatorType(int _id){ id = _id; }	
}
