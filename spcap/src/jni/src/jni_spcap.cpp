#include <jni.h>
#include "net_spcap_SPcap.h"
#include "../../SPcap.h"


JNIEXPORT void JNICALL Java_net_spcap_SPcap_init(JNIEnv *, jclass){
	SPcap::init();
	cout << "SPCAP init" << endl;
}

JNIEXPORT jobjectArray JNICALL Java_net_spcap_SPcap_getDevices(JNIEnv *env, jclass){

	string *dev = new string[100];
	jstring device;
	jclass ret_cls = env->FindClass("Ljava/lang/String;");
	jobjectArray jobjarr = NULL;

	int l = SPcap::getDevices(dev);
	if(l > 0){
		jobjarr = env->NewObjectArray(l, ret_cls, NULL);

		for(int i = 0; i<l; i++){
			device = env->NewStringUTF(dev[i].c_str());
			env->SetObjectArrayElement(jobjarr, i, device);
		}
		delete[] dev;
	}

	return jobjarr;
}
JNIEXPORT jobject JNICALL Java_net_spcap_SPcap_getStats(JNIEnv *env, jclass cls, jint id){
	#ifdef __PFRING__
	pfring_stat *stats = SPcap::getStats((int)id);
	#else
	pcap_stat *stats = SPcap::getStats((int)id);
	#endif
	jmethodID constructor;
	jvalue args[4];
	jobject obj = NULL;
	jclass c;
	stream_descriptor *sd = SPcap::getStream((int)id);

	if(stats != NULL && sd != NULL){
		c = env->FindClass("net/spcap/CapStats");
		if(c != NULL){
			constructor = env->GetMethodID(c, "<init>", "(JJJJ)V");
			if(constructor != NULL){
				pthread_mutex_lock(&sd->q_lock);
				#ifdef __PFRING__
				args[0].j = stats->recv;
				args[1].j = stats->drop;
				args[2].j = 0;
				args[3].j = sd->MAX_Q_LIMIT_REACHED;
				#else
				args[0].j = stats->ps_recv;
				args[1].j = stats->ps_drop;
				args[2].j = stats->ps_ifdrop;
				args[3].j = sd->MAX_Q_LIMIT_REACHED;
				#endif
				pthread_mutex_unlock(&sd->q_lock);

				obj = env->NewObjectA(c, constructor, args);

			}
		}
		delete stats;

	}
	return obj;

}


JNIEXPORT jint JNICALL Java_net_spcap_SPcap_initCapture(JNIEnv *env, jclass, jstring device, jint snaplen, jint max_q_size){
	jboolean iscopy;
	stream_descriptor *sd = NULL;
	string dev(env->GetStringUTFChars(device, &iscopy));
	sd = SPcap::initCapture(dev, (int)snaplen, (int)max_q_size);
	if(sd != NULL){
		cout << "SPCAP initCapture: " << dev << ", ID: " << sd->id << endl;
		return (jint)sd->id;
	}
	return -1;
}

JNIEXPORT void JNICALL Java_net_spcap_SPcap_startCapture(JNIEnv *env, jclass, jint id){
	//cout << "startCapture: " << id << endl;
	cout << "SPCAP startCapture: " << id << endl;
	SPcap::startCapture((int)id);
}
JNIEXPORT void JNICALL Java_net_spcap_SPcap_stopCapture(JNIEnv *env, jclass, jint id){
	//cout << "stopCapture: " << id << endl;
	cout << "SPCAP stopCapture: " << id << endl;
	SPcap::stopCapture((int)id);
}

JNIEXPORT void JNICALL Java_net_spcap_SPcap_setBPF(JNIEnv *env, jclass, jstring jfilter, jint id){
	jboolean iscopy;
	string filter(env->GetStringUTFChars(jfilter, &iscopy));
	stream_descriptor *sd = SPcap::getStream((int)id);
	if(sd != NULL){
		//cout << "setting filter: " << filter << endl;
		cout << "SPCAP setting filter: " << filter << endl;
		SPcap::setBPF(sd->handle, filter);

	}

}

JNIEXPORT jbyteArray JNICALL Java_net_spcap_SPcap_getPacket(JNIEnv *env, jclass, jint id){
	packet_descriptor *pd = NULL;
	jbyteArray res = NULL;

	pd = SPcap::popPacket((int)id);
	if(pd != NULL){
		res = env->NewByteArray(pd->caplen);
		env->SetByteArrayRegion(res, 0, pd->caplen, (jbyte *)pd->packet);
		//cout << id << ":" << "c++ getPacket: " << pd->caplen << endl;

		delete[] pd->packet;
		pd->packet = NULL;
		delete pd;
		pd = NULL;

	}

	return res;
}
