package net.isup.parameters.clgpn;

import java.util.HashMap;

public enum ScreeningIndicatorType {
	USER_PROVIDED_VERIFIED_PASSED(0x01),
	NETWORK_PROVIDED(0x03);
	
	
	
	private int id;
	private static final HashMap<Integer, ScreeningIndicatorType> lookup = new HashMap<Integer, ScreeningIndicatorType>();
	static{
		for(ScreeningIndicatorType td :ScreeningIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ScreeningIndicatorType get(int id){ return lookup.get(id); }
	private ScreeningIndicatorType(int _id){ id = _id; }		
}
