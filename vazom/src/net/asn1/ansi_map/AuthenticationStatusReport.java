package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationStatusReport extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(2).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public CountUpdateReport get_countUpdateReport(){
		return (CountUpdateReport)elements.get(3).data;
	}
	public CountUpdateReport new_countUpdateReport(){
		return new CountUpdateReport();
	}

	public MEID get_meid(){
		return (MEID)elements.get(4).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(5).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public ReauthenticationReport get_reauthenticationReport(){
		return (ReauthenticationReport)elements.get(6).data;
	}
	public ReauthenticationReport new_reauthenticationReport(){
		return new ReauthenticationReport();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(7).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public ServiceIndicator get_serviceIndicator(){
		return (ServiceIndicator)elements.get(8).data;
	}
	public ServiceIndicator new_serviceIndicator(){
		return new ServiceIndicator();
	}

	public SignalingMessageEncryptionReport get_signalingMessageEncryptionReport(){
		return (SignalingMessageEncryptionReport)elements.get(9).data;
	}
	public SignalingMessageEncryptionReport new_signalingMessageEncryptionReport(){
		return new SignalingMessageEncryptionReport();
	}

	public SSDUpdateReport get_ssdUpdateReport(){
		return (SSDUpdateReport)elements.get(10).data;
	}
	public SSDUpdateReport new_ssdUpdateReport(){
		return new SSDUpdateReport();
	}

	public UniqueChallengeReport get_uniqueChallengeReport(){
		return (UniqueChallengeReport)elements.get(11).data;
	}
	public UniqueChallengeReport new_uniqueChallengeReport(){
		return new UniqueChallengeReport();
	}

	public VoicePrivacyReport get_voicePrivacyReport(){
		return (VoicePrivacyReport)elements.get(12).data;
	}
	public VoicePrivacyReport new_voicePrivacyReport(){
		return new VoicePrivacyReport();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemCapabilities(); }});
		elements.add(new ElementDescriptor(138, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_countUpdateReport(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(192, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reauthenticationReport(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(193, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceIndicator(); }});
		elements.add(new ElementDescriptor(194, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionReport(); }});
		elements.add(new ElementDescriptor(156, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ssdUpdateReport(); }});
		elements.add(new ElementDescriptor(124, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uniqueChallengeReport(); }});
		elements.add(new ElementDescriptor(196, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyReport(); }});
	}
	public AuthenticationStatusReport(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
