package net.sccp;

public class SignallingPointCode {
	public int LSB;
	public int MSB;
	
	
	public SignallingPointCode(byte[] data){
		init(data);
		
	}
	// 2 octets
	public void init(byte[] data){
		LSB = data[0] & 0xFF;
		MSB = data[1] & 0x3F;
	}
	
	
}
