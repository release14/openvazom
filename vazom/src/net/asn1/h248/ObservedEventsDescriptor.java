package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ObservedEventsDescriptor extends SEQUENCE{

	public RequestID get_requestId(){
		return (RequestID)elements.get(0).data;
	}
	public RequestID new_requestId(){
		return new RequestID();
	}

	public ObservedEventSequenceOf get_observedEventLst(){
		return (ObservedEventSequenceOf)elements.get(1).data;
	}
	public ObservedEventSequenceOf new_observedEventLst(){
		return new ObservedEventSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_requestId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_observedEventLst(); }});
	}
	public ObservedEventsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
