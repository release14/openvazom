package net.isup.parameters.cdi;

import java.util.HashMap;

public enum RedirectingReasonType {
	UNKNOWN(0x00),
	USER_BUSY(0x08),
	NO_REPLY(0x10),
	UNCONDITIONAL(0x18),
	DEFLECTION_DURING_ALERTING(0x20),
	DEFLECTION_IMMEDIATE_RESPONSE(0x28),
	MOBILE_SUBSCRIBER_NOT_REACHABLE(0x30);
	
	
	private int id;
	private static final HashMap<Integer, RedirectingReasonType> lookup = new HashMap<Integer, RedirectingReasonType>();
	static{
		for(RedirectingReasonType td : RedirectingReasonType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static RedirectingReasonType get(int id){ return lookup.get(id); }
	private RedirectingReasonType(int _id){ id = _id; }	
}
