package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Component extends CHOICE{

	public Invoke get_invoke(){
		return (Invoke)elements.get(0).data;
	}
	public Invoke new_invoke(){
		return new Invoke();
	}

	public ReturnResult get_returnResultLast(){
		return (ReturnResult)elements.get(1).data;
	}
	public ReturnResult new_returnResultLast(){
		return new ReturnResult();
	}

	public ReturnError get_returnError(){
		return (ReturnError)elements.get(2).data;
	}
	public ReturnError new_returnError(){
		return new ReturnError();
	}

	public Reject get_reject(){
		return (Reject)elements.get(3).data;
	}
	public Reject new_reject(){
		return new Reject();
	}

	public ReturnResult get_returnResultNotLast(){
		return (ReturnResult)elements.get(4).data;
	}
	public ReturnResult new_returnResultNotLast(){
		return new ReturnResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_invoke(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_returnResultLast(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_returnError(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_reject(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_returnResultNotLast(); }});
	}
	public Component(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
