package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DialoguePortion extends ASNType{

	public EXTERNAL get_DialoguePortion(){
		return (EXTERNAL)elements.get(0).data;
	}
	public EXTERNAL new_DialoguePortion(){
		return new EXTERNAL();
	}
	public void initElements(){
//		elements.add(new ElementDescriptor(11, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_DialoguePortion(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_DialoguePortion(); }});
	}
	public DialoguePortion(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 11;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		explicit = true;
		asn_class = ASNTagClass.APPLICATION;
	}
}
