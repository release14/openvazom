package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TopologyRequestSequenceOf extends SEQUENCE{

	public TopologyRequest new_child(){
		return new TopologyRequest();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public TopologyRequest getChild(int index){
		return (TopologyRequest)of_children.get(index);
	}
	public TopologyRequestSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
