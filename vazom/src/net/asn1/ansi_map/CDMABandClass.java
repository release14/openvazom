package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMABandClass extends OCTET_STRING{

	public CDMABandClass(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
