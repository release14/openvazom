package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TriggerList extends SET{

	public DestinationAddress get_destinationAddress(){
		return (DestinationAddress)elements.get(0).data;
	}
	public DestinationAddress new_destinationAddress(){
		return new DestinationAddress();
	}

	public WIN_TriggerList get_wIN_TriggerList(){
		return (WIN_TriggerList)elements.get(1).data;
	}
	public WIN_TriggerList new_wIN_TriggerList(){
		return new WIN_TriggerList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_destinationAddress(); }});
		elements.add(new ElementDescriptor(283, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_wIN_TriggerList(); }});
	}
	public TriggerList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
