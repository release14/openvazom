package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceChangeResultWrapper extends SEQUENCE{

	public ServiceChangeResult get_serviceChangeResult(){
		return (ServiceChangeResult)elements.get(0).data;
	}
	public ServiceChangeResult new_serviceChangeResult(){
		return new ServiceChangeResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_serviceChangeResult(); }});
	}
	public ServiceChangeResultWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
