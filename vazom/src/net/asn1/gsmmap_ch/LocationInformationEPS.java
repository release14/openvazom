package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocationInformationEPS extends SEQUENCE{

	public OCTET_STRING get_e_utranCellGlobalIdentity(){
		return (OCTET_STRING)elements.get(0).data;
	}
	public OCTET_STRING new_e_utranCellGlobalIdentity(){
		return new OCTET_STRING();
	}

	public OCTET_STRING get_trackingAreaIdentity(){
		return (OCTET_STRING)elements.get(1).data;
	}
	public OCTET_STRING new_trackingAreaIdentity(){
		return new OCTET_STRING();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public GeographicalInformation get_geographicalInformation(){
		return (GeographicalInformation)elements.get(3).data;
	}
	public GeographicalInformation new_geographicalInformation(){
		return new GeographicalInformation();
	}

	public GeodeticInformation get_geodeticInformation(){
		return (GeodeticInformation)elements.get(4).data;
	}
	public GeodeticInformation new_geodeticInformation(){
		return new GeodeticInformation();
	}

	public NULL get_currentLocationRetrieved(){
		return (NULL)elements.get(5).data;
	}
	public NULL new_currentLocationRetrieved(){
		return new NULL();
	}

	public AgeOfLocationInformation get_ageOfLocationInformation(){
		return (AgeOfLocationInformation)elements.get(6).data;
	}
	public AgeOfLocationInformation new_ageOfLocationInformation(){
		return new AgeOfLocationInformation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_e_utranCellGlobalIdentity(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_trackingAreaIdentity(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_geographicalInformation(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_geodeticInformation(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_currentLocationRetrieved(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ageOfLocationInformation(); }});
	}
	public LocationInformationEPS(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
