package net.smpp2ss7;


import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import net.asn1.tcap2.TCMessage;
import net.config.SGNConfigData;
import net.ds.DataSourceType;
import net.logging.LoggingManager;
import net.sgn.SGNode;
import net.smpp.ErrorCodeType;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_sm_resp;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class SMPP2SS7Manager {
	public static Logger logger=Logger.getLogger(SMPP2SS7Manager.class);
	private static ConcurrentHashMap<Integer, Integer> error_map;
	private static boolean stopping;
	private static Db_sync db_sync_r;
	private static Thread db_sync_t;
	private static ConcurrentHashMap<String, MessageDescriptor> error_correlation_map;
	
	
	private static class Db_sync implements Runnable{
		MessageDescriptor md = null;
		UUID uid = null;
		
		 
		public void run() {
			LoggingManager.info(logger, "Starting...");
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.smpp2ss7_error_code_sync_interval); }catch(Exception e){ e.printStackTrace();	}
				// clear previouse correlations
				error_correlation_map.clear();
				
				uid = UUID.randomUUID();
				md = new MessageDescriptor();
				md.header.destination = LocationType.FN;
				md.header.ds = DataSourceType.NA;
				md.header.msg_id = uid.toString();
				md.header.msg_type = MessageType.DB_REQUEST;
				md.header.source = LocationType.SGN;
				md.callback = new SMPP_SS7_EC_Callback(md.header.msg_id);
				// body
				md.values.put(VSTPDataItemType.DB_REQUEST_TYPE.getId(), VSTPDataItemType.DB_SS7_SMPP_EC_CONVERSION.getId());
				
				// save correlation
				error_correlation_map.put(md.header.msg_id, md);
				
				// send
				SGNode.out_offer(md);
			}
			LoggingManager.info(logger, "Ending...");
			
		}
		
	}
	
	public static void remove_error_correlation(String id){
		error_correlation_map.remove(id);
	}
	public static void add_error_correlation(MessageDescriptor md){
		error_correlation_map.put(md.header.msg_id, md);
	}
	
	public static void process_sync_reply(MessageDescriptor md){
		MessageDescriptor md_cor =  error_correlation_map.get(md.header.msg_id);
		if(md_cor != null){
			if(md.callback != null){
				((SMPP_SS7_EC_Callback)md.callback).set_error_code_reply(md);
				md.callback.run();
			}
		}
		
	}
	
	public static PDUBase convert(TCMessage tcm, long sequence_number){
		Submit_sm_resp submit_resp = null;
		int tmp;
		try{
			if(tcm != null){
				// TCAP Abort
				if(Utils.isTcapAbort(tcm)){
					submit_resp = new Submit_sm_resp();
					
					tmp = Utils.getTcapAbortErrorCode(tcm);
					if(tmp == -1) tmp = Utils.getTcapAbortDialogueError(tcm);

					submit_resp.command_status = ErrorCodeType.get(error_code_convert(tmp));
					submit_resp.message_id = Long.toString(Utils.getTcap_did(tcm));
					submit_resp.sequence_number = sequence_number;
					return submit_resp;

				// Return Result
				}else if(Utils.isReturnResultLast(tcm)){
					submit_resp = new Submit_sm_resp();
					submit_resp.command_status = ErrorCodeType.ESME_ROK;
					submit_resp.message_id = Long.toString(Utils.getTcap_did(tcm));
					submit_resp.sequence_number = sequence_number;
					return submit_resp;

				// Return Error
				}else if(Utils.isReturnError(tcm)){
					submit_resp = new Submit_sm_resp();
					submit_resp.command_status = ErrorCodeType.get(error_code_convert(Utils.get_GSMMAP_ERROR_CODE(tcm)));
					submit_resp.message_id = Long.toString(Utils.getTcap_did(tcm));
					submit_resp.sequence_number = sequence_number;
					return submit_resp;
					
				}
			
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static void add_conversion(int ss7_ec, int smpp_ec){
		error_map.put(ss7_ec, smpp_ec);
	}
	
	public static Integer error_code_convert(int ss7_error_code){
		Integer smpp_error_code = error_map.get(ss7_error_code);
		if(smpp_error_code != null){
			return smpp_error_code;
		}
		return ErrorCodeType.ESME_RUNKNOWNERR.getId();
	}
	
	public static void init_ec_sync_thread(){
		db_sync_r = new Db_sync();
		db_sync_t = new Thread(db_sync_r, "SMPP2SS7_ERROR_CODE_SYNC");
		db_sync_t.start();
		
	}
	
	public static void init(){
		logger.info("Initializing SMPP->SS7 Manager...");
		error_map = new ConcurrentHashMap<Integer, Integer>();
		error_correlation_map = new ConcurrentHashMap<String, MessageDescriptor>();

		
		
	}
}
