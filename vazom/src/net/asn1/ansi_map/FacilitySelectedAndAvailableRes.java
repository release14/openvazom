package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FacilitySelectedAndAvailableRes extends SET{

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(0).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public ActionCode get_actionCode(){
		return (ActionCode)elements.get(1).data;
	}
	public ActionCode new_actionCode(){
		return new ActionCode();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(2).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(3).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DMH_AccountCodeDigits get_dmh_AccountCodeDigits(){
		return (DMH_AccountCodeDigits)elements.get(4).data;
	}
	public DMH_AccountCodeDigits new_dmh_AccountCodeDigits(){
		return new DMH_AccountCodeDigits();
	}

	public DMH_AlternateBillingDigits get_dmh_AlternateBillingDigits(){
		return (DMH_AlternateBillingDigits)elements.get(5).data;
	}
	public DMH_AlternateBillingDigits new_dmh_AlternateBillingDigits(){
		return new DMH_AlternateBillingDigits();
	}

	public DMH_BillingDigits get_dmh_BillingDigits(){
		return (DMH_BillingDigits)elements.get(6).data;
	}
	public DMH_BillingDigits new_dmh_BillingDigits(){
		return new DMH_BillingDigits();
	}

	public DMH_ChargeInformation get_dmh_ChargeInformation(){
		return (DMH_ChargeInformation)elements.get(7).data;
	}
	public DMH_ChargeInformation new_dmh_ChargeInformation(){
		return new DMH_ChargeInformation();
	}

	public DMH_RedirectionIndicator get_dmh_RedirectionIndicator(){
		return (DMH_RedirectionIndicator)elements.get(8).data;
	}
	public DMH_RedirectionIndicator new_dmh_RedirectionIndicator(){
		return new DMH_RedirectionIndicator();
	}

	public DMH_ServiceID get_dmh_ServiceID(){
		return (DMH_ServiceID)elements.get(9).data;
	}
	public DMH_ServiceID new_dmh_ServiceID(){
		return new DMH_ServiceID();
	}

	public NoAnswerTime get_noAnswerTime(){
		return (NoAnswerTime)elements.get(10).data;
	}
	public NoAnswerTime new_noAnswerTime(){
		return new NoAnswerTime();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(11).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public ResumePIC get_resumePIC(){
		return (ResumePIC)elements.get(12).data;
	}
	public ResumePIC new_resumePIC(){
		return new ResumePIC();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(13).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public TriggerAddressList get_triggerAddressList(){
		return (TriggerAddressList)elements.get(14).data;
	}
	public TriggerAddressList new_triggerAddressList(){
		return new TriggerAddressList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(128, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_actionCode(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(140, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AccountCodeDigits(); }});
		elements.add(new ElementDescriptor(141, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AlternateBillingDigits(); }});
		elements.add(new ElementDescriptor(142, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_BillingDigits(); }});
		elements.add(new ElementDescriptor(311, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_ChargeInformation(); }});
		elements.add(new ElementDescriptor(88, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_RedirectionIndicator(); }});
		elements.add(new ElementDescriptor(305, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_ServiceID(); }});
		elements.add(new ElementDescriptor(96, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_noAnswerTime(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(266, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_resumePIC(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(276, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerAddressList(); }});
	}
	public FacilitySelectedAndAvailableRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
