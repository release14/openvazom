package net.isup.parameters.clgpc;

import java.util.HashMap;


public enum CallingPartyCategoryType {
	UNKNOWN(0x00),
	OPERATOR_LANGUAGE_FRENCH(0x01),
	OPERATOR_LANGUAGE_ENGLISH(0x02),
	OPERATOR_LANGUAGE_GERMAN(0x03),
	OPERATOR_LANGUAGE_RUSSIAN(0x04),
	OPERATOR_LANGUAGE_SPANISH(0x05),
	ORDINARY_CALLING_SUBSCRIBER(0x0a),
	CALLING_SUBSCRIBER_WITH_PRIORITY(0x0b),
	DATA_CALL(0x0c),
	TEST_CALL(0x0d),
	PAYPHONE(0x0f);
	
	private int id;
	private static final HashMap<Integer, CallingPartyCategoryType> lookup = new HashMap<Integer, CallingPartyCategoryType>();
	static{
		for(CallingPartyCategoryType td : CallingPartyCategoryType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CallingPartyCategoryType get(int id){ return lookup.get(id); }
	private CallingPartyCategoryType(int _id){ id = _id; }	
}
