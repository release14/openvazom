package net.smstpdu;

import java.util.HashMap;

public enum SmsPduType {
	SMS_TPDU(1),
	SMPP_PDU(2);
	
	private int id;
	private static final HashMap<Integer, SmsPduType> lookup = new HashMap<Integer, SmsPduType>();
	static{
		for(SmsPduType td : SmsPduType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static SmsPduType get(int id){ return lookup.get(id); }
	private SmsPduType(int _id){ id = _id; }
}
