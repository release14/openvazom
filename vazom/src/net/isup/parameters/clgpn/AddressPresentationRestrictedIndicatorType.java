package net.isup.parameters.clgpn;

import java.util.HashMap;


public enum AddressPresentationRestrictedIndicatorType {
	ALLOWED(0x00),
	RESTRICTED(0x04),
	NOT_AVAILABLE(0x08),
	RESTRICTION_BY_NETWORK(0x0c);
	
	
	private int id;
	private static final HashMap<Integer, AddressPresentationRestrictedIndicatorType> lookup = new HashMap<Integer, AddressPresentationRestrictedIndicatorType>();
	static{
		for(AddressPresentationRestrictedIndicatorType td :AddressPresentationRestrictedIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static AddressPresentationRestrictedIndicatorType get(int id){ return lookup.get(id); }
	private AddressPresentationRestrictedIndicatorType(int _id){ id = _id; }	
}
