package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Category extends OCTET_STRING{

	public Category(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
