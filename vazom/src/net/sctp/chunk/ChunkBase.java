package net.sctp.chunk;

import net.sctp.ChunkType;

public class ChunkBase {
	public ChunkType type;
	public byte flags;
	public int length;
	protected int byte_pos = 0;
	
	public int getLength(byte[] data, int byte_pos){
		return (data[byte_pos + 2] << 8) + (data[byte_pos + 3] & 0xFF) & 0xFFFF;
		
	}
	public void decode(byte[] data){
		byte_pos++;
		flags = data[byte_pos++];
		length = (data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF) & 0xFFFF;
		//for(int j = 0; j<data.length; j++) System.out.print(String.format("%02x", data[j]) + ":");
		//System.out.println();
		
	}
	
}
