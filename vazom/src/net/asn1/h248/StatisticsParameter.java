package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StatisticsParameter extends SEQUENCE{

	public PkgdName get_statName(){
		return (PkgdName)elements.get(0).data;
	}
	public PkgdName new_statName(){
		return new PkgdName();
	}

	public Value get_statValue(){
		return (Value)elements.get(1).data;
	}
	public Value new_statValue(){
		return new Value();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_statName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_statValue(); }});
	}
	public StatisticsParameter(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
