package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudTerminationStateDescriptor extends SEQUENCE{

	public IndAudPropertyParmSequenceOf get_propertyParms(){
		return (IndAudPropertyParmSequenceOf)elements.get(0).data;
	}
	public IndAudPropertyParmSequenceOf new_propertyParms(){
		return new IndAudPropertyParmSequenceOf();
	}

	public NULL get_eventBufferControl(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_eventBufferControl(){
		return new NULL();
	}

	public NULL get_serviceState(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_serviceState(){
		return new NULL();
	}

	public ServiceState get_serviceStateSel(){
		return (ServiceState)elements.get(3).data;
	}
	public ServiceState new_serviceStateSel(){
		return new ServiceState();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_propertyParms(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_eventBufferControl(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceState(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_serviceStateSel(); }});
	}
	public IndAudTerminationStateDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
