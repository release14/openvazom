package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class PRINTABLESTRING extends ASNType {

	public PRINTABLESTRING(){
		super();
		universalTag = ASNTag.PRINTABLESTRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
