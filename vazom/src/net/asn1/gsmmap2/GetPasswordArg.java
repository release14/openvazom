package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class GetPasswordArg extends ENUMERATED{

	public static final int _enterPW = 0;
	public static final int _enterNewPW = 1;
	public static final int _enterNewPW_Again = 2;
	public GetPasswordArg(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
