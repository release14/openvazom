package net.mtp3.messages.signalling;

import java.util.ArrayList;
import java.util.Arrays;

import net.mtp3.messages.SignallingMessageGroupType;
import net.mtp3.messages.SignallingMessageType;
import net.utils.Utils;

public class TransferProhibited extends SignallingMessageBase {
	public int destination;
	
	public TransferProhibited(){
		super();
		signallingGroupType = SignallingMessageGroupType.TFM;
		signallingType = SignallingMessageType.TFP;
	}
	
	public byte[] encode(){
		ArrayList<Byte> buff = new ArrayList<Byte>();
		boolean[] dest = Utils.int2bits(destination, 14);
		byte[] tmp_res = null;
		byte[] res = null;
		// HO & H1
		int tmp = (signallingType.getId() << 4) + signallingGroupType.getId();
		buff.add((byte)tmp);
		// reverse to network order
		tmp_res = Utils.reverse(Utils.bits2bytes(dest));
		// add to buff
		for(int i = 0; i<tmp_res.length; i++) buff.add(tmp_res[i]);
		// result
		res = Utils.list2array(buff);
		return res;
	}
	
	
	
	public void init(byte[] data) {
		super.init(data);
		boolean[] tmp_bits = Utils.bytes2bits(Utils.reverse(Arrays.copyOfRange(data, byte_pos, byte_pos + 1)));;
		destination = Utils.bits2int(tmp_bits, 0, 14);
	
	}
}
