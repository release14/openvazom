package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SeqSigList extends SEQUENCE{

	public INTEGER get_id(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_id(){
		return new INTEGER();
	}

	public SignalSequenceOf get_signalList(){
		return (SignalSequenceOf)elements.get(1).data;
	}
	public SignalSequenceOf new_signalList(){
		return new SignalSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_id(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalList(); }});
	}
	public SeqSigList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
