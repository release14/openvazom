package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ForwardingData extends SEQUENCE{

	public ISDN_AddressString get_forwardedToNumber(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_forwardedToNumber(){
		return new ISDN_AddressString();
	}

	public ISDN_SubaddressString get_forwardedToSubaddress(){
		return (ISDN_SubaddressString)elements.get(1).data;
	}
	public ISDN_SubaddressString new_forwardedToSubaddress(){
		return new ISDN_SubaddressString();
	}

	public ForwardingOptions get_forwardingOptions(){
		return (ForwardingOptions)elements.get(2).data;
	}
	public ForwardingOptions new_forwardingOptions(){
		return new ForwardingOptions();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public FTN_AddressString get_longForwardedToNumber(){
		return (FTN_AddressString)elements.get(4).data;
	}
	public FTN_AddressString new_longForwardedToNumber(){
		return new FTN_AddressString();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_forwardedToNumber(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_forwardedToSubaddress(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_forwardingOptions(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_longForwardedToNumber(); }});
	}
	public ForwardingData(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
