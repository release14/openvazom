package net.smsfs.cli;

import net.cli.CLIBase;
import net.cli.GroupDescriptor;
import net.cli.MethodDescriptor;
import net.cli.ParamDescriptor;
import net.smsfs.FilterDescriptor;
import net.smsfs.FilterDescriptorType;
import net.smsfs.FilterType;
import net.smsfs.RuleDescriptor;

public class CLIManager extends CLIBase{
	private String simpleValue(Object[] params, String smsfs_val){
		if(params != null){
			ParamDescriptor pd1 = (ParamDescriptor)params[0];
			ParamDescriptor pd2 = (ParamDescriptor)params[1];
			ParamDescriptor pd3 = (ParamDescriptor)params[2];
			CLIConnection conn = (CLIConnection)params[3];
			
			if(pd1.value != null && pd2.value != null && pd3.value != null){
				FilterType ft = null;
				if(pd1.value.equalsIgnoreCase("MO")) ft = FilterType.MO;
				else if(pd1.value.equalsIgnoreCase("MT")) ft = FilterType.MT;
				else if(pd1.value.equalsIgnoreCase("HLT")) ft = FilterType.HLR;

				if(ft != null){
					FilterDescriptor fd = null;
					for(int i = 0; i<conn.current_filter.size(); i++){
						if(conn.current_filter.get(i).filterType == ft){
							fd = conn.current_filter.get(i);
							break;
						}
					}
					if(fd != null){
						
						try{
							Integer.parseInt(pd2.value);
						}catch(Exception e){
							return "[RED]Invalid rule ID = [[GREEN]" + pd2.value + "[RED]]!";
						}
						
						if(Integer.parseInt(pd2.value) < fd.rule_mod_lst.size()){
							if(fd.rule_mod_lst.get(Integer.parseInt(pd2.value)).type == FilterDescriptorType.RULE){
								RuleDescriptor rd = (RuleDescriptor)fd.rule_mod_lst.get(Integer.parseInt(pd2.value));
								if(pd3.value.equalsIgnoreCase("0")){
									rd.evalItem1 = smsfs_val;
									
								}else{
									rd.evalItem2 = smsfs_val;
									
								}
								
								return "[WHITE]Adding operand [[GREEN]" + smsfs_val + "[WHITE]] to rule ID = [[GREEN]" + pd2.value + "[WHITE]], block = [[GREEN]" +
									pd1.value + "[WHITE]]!";
								
							}else return "[RED]Unknown rule with ID = [[GREEN]" + pd2.value + "[RED]]!";
						}else return "[RED]Unknown rule with ID = [[GREEN]" + pd2.value + "[RED]]!";
						
						
						
					}else return "[RED]Block type [[GREEN]" + pd1.value + "[RED]] does not exist!";
					
					
				}else return "[RED]Unknown block type [[GREEN]" + pd1.value + "[RED]]!";
				
				
			}else return "[RED]Incomplete syntax!";
		}else return "[RED]Incomplete syntax!";
	}

	public class GRP_FILTER extends GroupDescriptor {

		public GRP_FILTER() {
			super("FILTER", "Filter management ", "", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_FILTER _grp_FILTER = new GRP_FILTER();


	public class CLI_filter_show extends MethodDescriptor {

		public CLI_filter_show() {
			super("SHOW", "Show current SMSFS filter ", "FILTER", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				if(params.length == 1){
					CLIConnection conn = (CLIConnection)params[0];
					FilterDescriptor fd = null;
					RuleDescriptor rd = null;
					///System.out.println("SIZE: " + conn.current_filter.size());
					for(int i = 0; i<conn.current_filter.size(); i++){
						fd = conn.current_filter.get(i);
						res += "[YELLOW]" +  fd.filterType.toString() + "[WHITE] {\n";
						for(int j = 0; j<fd.rule_mod_lst.size(); j++){
							if(fd.rule_mod_lst.get(j).type == FilterDescriptorType.RULE){
								rd = (RuleDescriptor)fd.rule_mod_lst.get(j);
								
								res += "  [BLUE]// ID = " + j + "\n";
								res += "  " + (rd.label != null ? "[BLUE]" + rd.label + "[WHITE]:" : "") + "[WHITE][[BLUE]@" + (rd.active ? "[GREEN]ON" : "[GREEN]OFF") + "]:[RED]RULE" +
									"[WHITE][" + rd.points + "] {\n";
								
								res += "    [GREEN]: ([YELLOW]" + (rd.evalItem1.length() == 0 ? "[RED]<OPERAND_1 NOT DEFINED>" : rd.evalItem1) + "[GREEN] " + 
									(rd.evalOperator == null ? "[RED]<OPERATOR NOT DEFINED>" : rd.evalOperator) + " " + 
									"[YELLOW]" + (rd.evalItem2.length() == 0 ? "[RED]<OPERAND_2 NOT DEFINED>" : rd.evalItem2) 
									+ "[GREEN]);\n";
								
								res += "    [GREEN]+ " + (rd.ruleEvalTrue == null ? "[RED]<ACTION NOT DEFINED>": "") + "[GREEN];\n";
								res += "    [GREEN]- " + (rd.ruleEvalTrue == null ? "[RED]<ACTION NOT DEFINED>": "") + "[GREEN];\n";
								
								
								res += "[WHITE]  }\n";
								
							}
							
						}
						res += "[WHITE]}\n";
					}
					
				}
			}
			return res;
		}
	}
	public CLI_filter_show _cli_filter_show = new CLI_filter_show();

	public class GRP_BLOCK extends GroupDescriptor {

		public GRP_BLOCK() {
			super("BLOCK", "Block management ", "", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_BLOCK _grp_BLOCK = new GRP_BLOCK();


	public class CLI_block_add extends MethodDescriptor {

		public CLI_block_add() {
			super("ADD", "Add new block ", "BLOCK", "block_type");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				CLIConnection conn = (CLIConnection)params[1];
				FilterType ft = null;
				if(pd1.value.equalsIgnoreCase("MT")) ft = FilterType.MT;
				else if(pd1.value.equalsIgnoreCase("MO")) ft = FilterType.MO;
				else if(pd1.value.equalsIgnoreCase("HLR")) ft = FilterType.HLR;
				else if(pd1.value.equalsIgnoreCase("SMPP_MO")) ft = FilterType.SMPP_MO;
				else if(pd1.value.equalsIgnoreCase("SMPP_MT")) ft = FilterType.SMPP_MT;
				
				if(ft != null){
					FilterDescriptor fd = new FilterDescriptor();
					fd.filterType = ft;
					conn.current_filter.add(fd);
					return "[WHITE]Adding new block type [[GREEN]" + ft + "[WHITE]]!";					
				}else return "[RED]Unknown block type [[GREEN]" + pd1.value + "[RED]]!";
			}
			return "";
		}

	}
	public CLI_block_add _cli_block_add = new CLI_block_add();

	public class CLI_block_remove extends MethodDescriptor {

		public CLI_block_remove() {
			super("REMOVE", "Remove block ", "BLOCK", "block_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_block_remove _cli_block_remove = new CLI_block_remove();

	public class GRP_RULE extends GroupDescriptor {

		public GRP_RULE() {
			super("RULE", "Rule management ", "", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_RULE _grp_RULE = new GRP_RULE();


	public class CLI_rule_set extends MethodDescriptor {

		public CLI_rule_set() {
			super("SET", "Set rule parameters ", "RULE", "block_type:rule_id:rule_status:rule_score:rule_label");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_rule_set _cli_rule_set = new CLI_rule_set();

	public class CLI_rule_add extends MethodDescriptor {

		public CLI_rule_add() {
			super("ADD", "Add new rule ", "RULE", "block_type:rule_status:rule_score:rule_label");
		}

		public String execute(Object[] params) {
			if(params != null){
				if(params.length == 5){
					ParamDescriptor pd1 = (ParamDescriptor)params[0];
					ParamDescriptor pd2 = (ParamDescriptor)params[1];
					ParamDescriptor pd3 = (ParamDescriptor)params[2];
					ParamDescriptor pd4 = (ParamDescriptor)params[3];
					CLIConnection pd5 = (CLIConnection)params[4];
					
					if(pd1.value != null && pd2.value != null && pd3.value != null){
						FilterType ft = null;
						if(pd1.value.equalsIgnoreCase("MO")) ft = FilterType.MO;
						else if(pd1.value.equalsIgnoreCase("MT")) ft = FilterType.MT;
						else if(pd1.value.equalsIgnoreCase("HLT")) ft = FilterType.HLR;
						
						if(ft != null){
							FilterDescriptor fd = null;
							for(int i = 0; i<pd5.current_filter.size(); i++){
								if(pd5.current_filter.get(i).filterType == ft){
									fd = pd5.current_filter.get(i);
									break;
								}
							}
							if(fd != null){
								RuleDescriptor rd = new RuleDescriptor();
								rd.active = (pd2.value.equalsIgnoreCase("1") ? true : false);
								rd.label = pd4.value;
								rd.points = Integer.parseInt(pd3.value);
								fd.rule_mod_lst.add(rd);
								
								return "[WHITE]Adding new rule, rule index = [[GREEN]" + (fd.rule_mod_lst.size() - 1) + 
								"[WHITE]], rule label = [[GREEN]" + pd4.value + "[WHITE]]!";
								
							}else return "[RED]Block type [[GREEN]" + pd1.value + "[RED]] does not exist!";
							
						}else return "[RED]Unknown block type [[GREEN]" + pd1.value + "[RED]]!";
						
					}else return "[RED]Incomplete syntax!";
					
					
					
				}else return "[RED]Incomplete syntax!";
			}else return "[RED]Incomplete syntax!";
		}
	}
	public CLI_rule_add _cli_rule_add = new CLI_rule_add();

	public class CLI_rule_remove extends MethodDescriptor {

		public CLI_rule_remove() {
			super("REMOVE", "Remove rule ", "RULE", "block_type:rule_id");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_rule_remove _cli_rule_remove = new CLI_rule_remove();

	public class GRP_ACTION extends GroupDescriptor {

		public GRP_ACTION() {
			super("ACTION", "Action management ", "RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_ACTION _grp_ACTION = new GRP_ACTION();


	public class GRP_SET extends GroupDescriptor {

		public GRP_SET() {
			super("SET", "Set rule action ", "ACTION,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SET _grp_SET = new GRP_SET();


	public class CLI_action_set_continue extends MethodDescriptor {

		public CLI_action_set_continue() {
			super("CONTINUE", "Continue to next rule or modifier ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_continue _cli_action_set_continue = new CLI_action_set_continue();

	public class CLI_action_set_continue_q extends MethodDescriptor {

		public CLI_action_set_continue_q() {
			super("CONTINUE_QUARANTINE", "Continue to next rule or modifier and mark as quarantined ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_continue_q _cli_action_set_continue_q = new CLI_action_set_continue_q();

	public class CLI_action_set_allow extends MethodDescriptor {

		public CLI_action_set_allow() {
			super("ALLOW", "Allow message ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_allow _cli_action_set_allow = new CLI_action_set_allow();

	public class CLI_action_set_allow_q extends MethodDescriptor {

		public CLI_action_set_allow_q() {
			super("ALLOW_QUARANTINE", "Allow message and mark as quarantined ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_allow_q _cli_action_set_allow_q = new CLI_action_set_allow_q();

	public class CLI_action_set_allow_u extends MethodDescriptor {

		public CLI_action_set_allow_u() {
			super("ALLOW_UNCONDITIONAL", "Allow message without checking the total score ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_allow_u _cli_action_set_allow_u = new CLI_action_set_allow_u();

	public class CLI_action_set_allow_u_q extends MethodDescriptor {

		public CLI_action_set_allow_u_q() {
			super("ALLOW_UNCONDITIONAL_QUARANTINE", "Allow message without checking the total score and mark as quarantined ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_allow_u_q _cli_action_set_allow_u_q = new CLI_action_set_allow_u_q();

	public class CLI_action_set_deny extends MethodDescriptor {

		public CLI_action_set_deny() {
			super("DENY", "Deny message ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_deny _cli_action_set_deny = new CLI_action_set_deny();

	public class CLI_action_set_deny_q extends MethodDescriptor {

		public CLI_action_set_deny_q() {
			super("DENY_QUARANTINE", "Deny message and mark as quarantined ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_deny_q _cli_action_set_deny_q = new CLI_action_set_deny_q();

	public class CLI_action_set_goto extends MethodDescriptor {

		public CLI_action_set_goto() {
			super("GOTO", "Skip to other rule or modifier ", "SET,ACTION,RULE", "block_type:rule_id:evaluation_type:goto_label");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_action_set_goto _cli_action_set_goto = new CLI_action_set_goto();

	public class GRP_OPERAND extends GroupDescriptor {

		public GRP_OPERAND() {
			super("OPERAND", "Rule operand management ", "RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_OPERAND _grp_OPERAND = new GRP_OPERAND();


	public class GRP_GENERAL extends GroupDescriptor {

		public GRP_GENERAL() {
			super("GENERAL", "String and number operand ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GENERAL _grp_GENERAL = new GRP_GENERAL();


	public class CLI_general_operand_set extends MethodDescriptor {

		public CLI_general_operand_set() {
			super("SET", "Set general operand value ", "GENERAL,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_general_operand_set _cli_general_operand_set = new CLI_general_operand_set();

	public class CLI_general_operand_regex extends MethodDescriptor {

		public CLI_general_operand_regex() {
			super("REGEX", "Set as regex rule operand ", "GENERAL,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_general_operand_regex _cli_general_operand_regex = new CLI_general_operand_regex();

	public class CLI_general_operand_info extends MethodDescriptor {

		public CLI_general_operand_info() {
			super("INFO", "Operand description ", "GENERAL,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_general_operand_info _cli_general_operand_info = new CLI_general_operand_info();

	public class GRP_SMS_TPDU extends GroupDescriptor {

		public GRP_SMS_TPDU() {
			super("SMS_TPDU", "SMS TPDU related operands ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU _grp_SMS_TPDU = new GRP_SMS_TPDU();


	public class GRP_TON extends GroupDescriptor {

		public GRP_TON() {
			super("TON", "Type of number ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON _grp_TON = new GRP_TON();


	public class GRP_TON_UNKNOWN extends GroupDescriptor {

		public GRP_TON_UNKNOWN() {
			super("TON_UNKNOWN", "Unknown ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_UNKNOWN _grp_TON_UNKNOWN = new GRP_TON_UNKNOWN();


	public class CLI_sms_tpdu_ton_unknown_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_unknown_set() {
			super("SET", "Set operand value ", "TON_UNKNOWN,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_unknown_set _cli_sms_tpdu_ton_unknown_set = new CLI_sms_tpdu_ton_unknown_set();

	public class GRP_TON_INTERNATIONAL extends GroupDescriptor {

		public GRP_TON_INTERNATIONAL() {
			super("TON_INTERNATIONAL", "International ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_INTERNATIONAL _grp_TON_INTERNATIONAL = new GRP_TON_INTERNATIONAL();


	public class CLI_sms_tpdu_ton_international_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_international_set() {
			super("SET", "Set operand value ", "TON_INTERNATIONAL,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_international_set _cli_sms_tpdu_ton_international_set = new CLI_sms_tpdu_ton_international_set();

	public class GRP_TON_NATIONAL extends GroupDescriptor {

		public GRP_TON_NATIONAL() {
			super("TON_NATIONAL", "National ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_NATIONAL _grp_TON_NATIONAL = new GRP_TON_NATIONAL();


	public class CLI_sms_tpdu_ton_national_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_national_set() {
			super("SET", "Set operand value ", "TON_NATIONAL,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_national_set _cli_sms_tpdu_ton_national_set = new CLI_sms_tpdu_ton_national_set();

	public class GRP_TON_NETWORK_SPECIFIC extends GroupDescriptor {

		public GRP_TON_NETWORK_SPECIFIC() {
			super("TON_NETWORK_SPECIFIC", "Network specific ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_NETWORK_SPECIFIC _grp_TON_NETWORK_SPECIFIC = new GRP_TON_NETWORK_SPECIFIC();


	public class CLI_sms_tpdu_ton_network_specific_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_network_specific_set() {
			super("SET", "Set operand value ", "TON_NETWORK_SPECIFIC,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_network_specific_set _cli_sms_tpdu_ton_network_specific_set = new CLI_sms_tpdu_ton_network_specific_set();

	public class GRP_TON_SUBSCRIBER_NUMBER extends GroupDescriptor {

		public GRP_TON_SUBSCRIBER_NUMBER() {
			super("TON_SUBSCRIBER_NUMBER", "Subscriber number ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_SUBSCRIBER_NUMBER _grp_TON_SUBSCRIBER_NUMBER = new GRP_TON_SUBSCRIBER_NUMBER();


	public class CLI_sms_tpdu_ton_subscriber_number_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_subscriber_number_set() {
			super("SET", "Set operand value ", "TON_SUBSCRIBER_NUMBER,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_subscriber_number_set _cli_sms_tpdu_ton_subscriber_number_set = new CLI_sms_tpdu_ton_subscriber_number_set();

	public class GRP_TON_ALPHANUMERIC extends GroupDescriptor {

		public GRP_TON_ALPHANUMERIC() {
			super("TON_ALPHANUMERIC", "Alphanumeric ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_ALPHANUMERIC _grp_TON_ALPHANUMERIC = new GRP_TON_ALPHANUMERIC();


	public class CLI_sms_tpdu_ton_alphanumeric_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_alphanumeric_set() {
			super("SET", "Set operand value ", "TON_ALPHANUMERIC,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_alphanumeric_set _cli_sms_tpdu_ton_alphanumeric_set = new CLI_sms_tpdu_ton_alphanumeric_set();

	public class GRP_TON_ABBREVIATED extends GroupDescriptor {

		public GRP_TON_ABBREVIATED() {
			super("TON_ABBREVIATED", "Abbreviated ", "TON,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_TON_ABBREVIATED _grp_TON_ABBREVIATED = new GRP_TON_ABBREVIATED();


	public class CLI_sms_tpdu_ton_abbreviated_set extends MethodDescriptor {

		public CLI_sms_tpdu_ton_abbreviated_set() {
			super("SET", "Set operand value ", "TON_ABBREVIATED,TON,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ton_abbreviated_set _cli_sms_tpdu_ton_abbreviated_set = new CLI_sms_tpdu_ton_abbreviated_set();

	public class GRP_DCS extends GroupDescriptor {

		public GRP_DCS() {
			super("DCS", "Data coding scheme ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_DCS _grp_DCS = new GRP_DCS();


	public class GRP_DCS_DEFAULT extends GroupDescriptor {

		public GRP_DCS_DEFAULT() {
			super("DCS_DEFAULT", "Default 7bit GSM Alphabet ", "DCS,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_DCS_DEFAULT _grp_DCS_DEFAULT = new GRP_DCS_DEFAULT();


	public class CLI_sms_tpdu_dcs_default_set extends MethodDescriptor {

		public CLI_sms_tpdu_dcs_default_set() {
			super("SET", "Set operand value ", "DCS_DEFAULT,DCS,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dcs_default_set _cli_sms_tpdu_dcs_default_set = new CLI_sms_tpdu_dcs_default_set();

	public class GRP_DCS_8BIT extends GroupDescriptor {

		public GRP_DCS_8BIT() {
			super("DCS_8BIT", "8bit encoding ", "DCS,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_DCS_8BIT _grp_DCS_8BIT = new GRP_DCS_8BIT();


	public class CLI_sms_tpdu_dcs_8bit_set extends MethodDescriptor {

		public CLI_sms_tpdu_dcs_8bit_set() {
			super("SET", "Set operand value ", "DCS_8BIT,DCS,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dcs_8bit_set _cli_sms_tpdu_dcs_8bit_set = new CLI_sms_tpdu_dcs_8bit_set();

	public class GRP_DCS_UCS2 extends GroupDescriptor {

		public GRP_DCS_UCS2() {
			super("DCS_UCS2", "16bit encoding ", "DCS,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_DCS_UCS2 _grp_DCS_UCS2 = new GRP_DCS_UCS2();


	public class CLI_sms_tpdu_dcs_16bit_set extends MethodDescriptor {

		public CLI_sms_tpdu_dcs_16bit_set() {
			super("SET", "Set operand value ", "DCS_UCS2,DCS,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dcs_16bit_set _cli_sms_tpdu_dcs_16bit_set = new CLI_sms_tpdu_dcs_16bit_set();

	public class GRP_MSG_TYPE extends GroupDescriptor {

		public GRP_MSG_TYPE() {
			super("MSG_TYPE", "SINGLE or CONCATENATED message ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MSG_TYPE _grp_MSG_TYPE = new GRP_MSG_TYPE();


	public class GRP_SINGLE extends GroupDescriptor {

		public GRP_SINGLE() {
			super("SINGLE", "", "MSG_TYPE,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SINGLE _grp_SINGLE = new GRP_SINGLE();


	public class CLI_sms_tpdu_msg_type_single_set extends MethodDescriptor {

		public CLI_sms_tpdu_msg_type_single_set() {
			super("SET", "Set operand value ", "SINGLE,MSG_TYPE,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_msg_type_single_set _cli_sms_tpdu_msg_type_single_set = new CLI_sms_tpdu_msg_type_single_set();

	public class GRP_CONCATENATED extends GroupDescriptor {

		public GRP_CONCATENATED() {
			super("CONCATENATED", "", "MSG_TYPE,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_CONCATENATED _grp_CONCATENATED = new GRP_CONCATENATED();


	public class CLI_sms_tpdu_msg_type_concatenated_set extends MethodDescriptor {

		public CLI_sms_tpdu_msg_type_concatenated_set() {
			super("SET", "Set operand value ", "CONCATENATED,MSG_TYPE,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_msg_type_concatenated_set _cli_sms_tpdu_msg_type_concatenated_set = new CLI_sms_tpdu_msg_type_concatenated_set();

	public class GRP_SMS_TPDU_ORIGINATING extends GroupDescriptor {

		public GRP_SMS_TPDU_ORIGINATING() {
			super("SMS_TPDU_ORIGINATING", "SMS Originating address ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU_ORIGINATING _grp_SMS_TPDU_ORIGINATING = new GRP_SMS_TPDU_ORIGINATING();


	public class CLI_sms_tpdu_originating_set extends MethodDescriptor {

		public CLI_sms_tpdu_originating_set() {
			super("SET", "Set as rule operand ", "SMS_TPDU_ORIGINATING,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			return simpleValue(params, "SMS_TPDU.ORIGINATING");
		}

	}
	public CLI_sms_tpdu_originating_set _cli_sms_tpdu_originating_set = new CLI_sms_tpdu_originating_set();

	public class CLI_sms_tpdu_originating_regex extends MethodDescriptor {

		public CLI_sms_tpdu_originating_regex() {
			super("REGEX", "Set as regex rule operand ", "SMS_TPDU_ORIGINATING,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_originating_regex _cli_sms_tpdu_originating_regex = new CLI_sms_tpdu_originating_regex();

	public class CLI_sms_tpdu_originating_info extends MethodDescriptor {

		public CLI_sms_tpdu_originating_info() {
			super("INFO", "Operand description ", "SMS_TPDU_ORIGINATING,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_originating_info _cli_sms_tpdu_originating_info = new CLI_sms_tpdu_originating_info();

	public class GRP_SMS_TPDU_ORIGINATING_ENC extends GroupDescriptor {

		public GRP_SMS_TPDU_ORIGINATING_ENC() {
			super("SMS_TPDU_ORIGINATING_ENC", "SMS Originating address encoding ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU_ORIGINATING_ENC _grp_SMS_TPDU_ORIGINATING_ENC = new GRP_SMS_TPDU_ORIGINATING_ENC();


	public class CLI_sms_tpdu_originating_enc_set extends MethodDescriptor {

		public CLI_sms_tpdu_originating_enc_set() {
			super("SET", "Set as rule operand ", "SMS_TPDU_ORIGINATING_ENC,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_originating_enc_set _cli_sms_tpdu_originating_enc_set = new CLI_sms_tpdu_originating_enc_set();

	public class CLI_sms_tpdu_originating_enc_info extends MethodDescriptor {

		public CLI_sms_tpdu_originating_enc_info() {
			super("INFO", "Operand description ", "SMS_TPDU_ORIGINATING_ENC,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_originating_enc_info _cli_sms_tpdu_originating_enc_info = new CLI_sms_tpdu_originating_enc_info();

	public class GRP_SMS_TPDU_DESTINATION extends GroupDescriptor {

		public GRP_SMS_TPDU_DESTINATION() {
			super("SMS_TPDU_DESTINATION", "SMS Destination address ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU_DESTINATION _grp_SMS_TPDU_DESTINATION = new GRP_SMS_TPDU_DESTINATION();


	public class CLI_sms_tpdu_destination_set extends MethodDescriptor {

		public CLI_sms_tpdu_destination_set() {
			super("SET", "Set as rule operand ", "SMS_TPDU_DESTINATION,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_destination_set _cli_sms_tpdu_destination_set = new CLI_sms_tpdu_destination_set();

	public class CLI_sms_tpdu_destination_regex extends MethodDescriptor {

		public CLI_sms_tpdu_destination_regex() {
			super("REGEX", "Set as regex rule operand ", "SMS_TPDU_DESTINATION,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_destination_regex _cli_sms_tpdu_destination_regex = new CLI_sms_tpdu_destination_regex();

	public class CLI_sms_tpdu_destination_info extends MethodDescriptor {

		public CLI_sms_tpdu_destination_info() {
			super("INFO", "Operand description ", "SMS_TPDU_DESTINATION,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_destination_info _cli_sms_tpdu_destination_info = new CLI_sms_tpdu_destination_info();

	public class GRP_SMS_TPDU_DESTINATION_ENC extends GroupDescriptor {

		public GRP_SMS_TPDU_DESTINATION_ENC() {
			super("SMS_TPDU_DESTINATION_ENC", "SMS Destination address encoding ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU_DESTINATION_ENC _grp_SMS_TPDU_DESTINATION_ENC = new GRP_SMS_TPDU_DESTINATION_ENC();


	public class CLI_sms_tpdu_destination_enc_set extends MethodDescriptor {

		public CLI_sms_tpdu_destination_enc_set() {
			super("SET", "Set as rule operand ", "SMS_TPDU_DESTINATION_ENC,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_destination_enc_set _cli_sms_tpdu_destination_enc_set = new CLI_sms_tpdu_destination_enc_set();

	public class CLI_sms_tpdu_destination_enc_info extends MethodDescriptor {

		public CLI_sms_tpdu_destination_enc_info() {
			super("INFO", "Operand description ", "SMS_TPDU_DESTINATION_ENC,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_destination_enc_info _cli_sms_tpdu_destination_enc_info = new CLI_sms_tpdu_destination_enc_info();

	public class GRP_SMS_TPDU_UD extends GroupDescriptor {

		public GRP_SMS_TPDU_UD() {
			super("SMS_TPDU_UD", "SMS Text ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU_UD _grp_SMS_TPDU_UD = new GRP_SMS_TPDU_UD();


	public class CLI_sms_tpdu_ud_set extends MethodDescriptor {

		public CLI_sms_tpdu_ud_set() {
			super("SET", "Set as rule operand ", "SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ud_set _cli_sms_tpdu_ud_set = new CLI_sms_tpdu_ud_set();

	public class CLI_sms_tpdu_ud_regex extends MethodDescriptor {

		public CLI_sms_tpdu_ud_regex() {
			super("REGEX", "Set as regex rule operand ", "SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ud_regex _cli_sms_tpdu_ud_regex = new CLI_sms_tpdu_ud_regex();

	public class CLI_sms_tpdu_ud_info extends MethodDescriptor {

		public CLI_sms_tpdu_ud_info() {
			super("INFO", "Operand description ", "SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_ud_info _cli_sms_tpdu_ud_info = new CLI_sms_tpdu_ud_info();

	public class GRP_DICT_SMS_TPDU_UD extends GroupDescriptor {

		public GRP_DICT_SMS_TPDU_UD() {
			super("DICT_SMS_TPDU_UD", "SMS Text dictionary check ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_DICT_SMS_TPDU_UD _grp_DICT_SMS_TPDU_UD = new GRP_DICT_SMS_TPDU_UD();


	public class CLI_sms_tpdu_dict_set extends MethodDescriptor {

		public CLI_sms_tpdu_dict_set() {
			super("SET", "Set as rule operand ", "DICT_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dict_set _cli_sms_tpdu_dict_set = new CLI_sms_tpdu_dict_set();

	public class CLI_sms_tpdu_dict_info extends MethodDescriptor {

		public CLI_sms_tpdu_dict_info() {
			super("INFO", "Operand description ", "DICT_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dict_info _cli_sms_tpdu_dict_info = new CLI_sms_tpdu_dict_info();

	public class GRP_SPAM_SMS_TPDU_UD extends GroupDescriptor {

		public GRP_SPAM_SMS_TPDU_UD() {
			super("SPAM_SMS_TPDU_UD", "SMS Text known spam check ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SPAM_SMS_TPDU_UD _grp_SPAM_SMS_TPDU_UD = new GRP_SPAM_SMS_TPDU_UD();


	public class CLI_sms_tpdu_spam_check_set extends MethodDescriptor {

		public CLI_sms_tpdu_spam_check_set() {
			super("SET", "Set as rule operand ", "SPAM_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_spam_check_set _cli_sms_tpdu_spam_check_set = new CLI_sms_tpdu_spam_check_set();

	public class CLI_sms_tpdu_spam_check_info extends MethodDescriptor {

		public CLI_sms_tpdu_spam_check_info() {
			super("INFO", "Operand description ", "SPAM_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_spam_check_info _cli_sms_tpdu_spam_check_info = new CLI_sms_tpdu_spam_check_info();

	public class GRP_RP_SMS_TPDU_UD extends GroupDescriptor {

		public GRP_RP_SMS_TPDU_UD() {
			super("RP_SMS_TPDU_UD", "SMS Text repetition check ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_RP_SMS_TPDU_UD _grp_RP_SMS_TPDU_UD = new GRP_RP_SMS_TPDU_UD();


	public class CLI_sms_tpdu_rp_check_set extends MethodDescriptor {

		public CLI_sms_tpdu_rp_check_set() {
			super("SET", "Set as rule operand ", "RP_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_rp_check_set _cli_sms_tpdu_rp_check_set = new CLI_sms_tpdu_rp_check_set();

	public class CLI_sms_tpdu_rp_check_info extends MethodDescriptor {

		public CLI_sms_tpdu_rp_check_info() {
			super("INFO", "Operand description ", "RP_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_rp_check_info _cli_sms_tpdu_rp_check_info = new CLI_sms_tpdu_rp_check_info();

	public class GRP_QUARANTINE_SMS_TPDU_UD extends GroupDescriptor {

		public GRP_QUARANTINE_SMS_TPDU_UD() {
			super("QUARANTINE_SMS_TPDU_UD", "SMS Text quarantine check ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_QUARANTINE_SMS_TPDU_UD _grp_QUARANTINE_SMS_TPDU_UD = new GRP_QUARANTINE_SMS_TPDU_UD();


	public class CLI_sms_tpdu_q_check_set extends MethodDescriptor {

		public CLI_sms_tpdu_q_check_set() {
			super("SET", "Set as rule operand ", "QUARANTINE_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_q_check_set _cli_sms_tpdu_q_check_set = new CLI_sms_tpdu_q_check_set();

	public class CLI_sms_tpdu_q_check_info extends MethodDescriptor {

		public CLI_sms_tpdu_q_check_info() {
			super("INFO", "Operand description ", "QUARANTINE_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_q_check_info _cli_sms_tpdu_q_check_info = new CLI_sms_tpdu_q_check_info();

	public class GRP_MD5_SMS_TPDU_UD extends GroupDescriptor {

		public GRP_MD5_SMS_TPDU_UD() {
			super("MD5_SMS_TPDU_UD", "SMS Text MD5 check ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MD5_SMS_TPDU_UD _grp_MD5_SMS_TPDU_UD = new GRP_MD5_SMS_TPDU_UD();


	public class CLI_sms_tpdu_md5_check_set extends MethodDescriptor {

		public CLI_sms_tpdu_md5_check_set() {
			super("SET", "Set as rule operand ", "MD5_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_md5_check_set _cli_sms_tpdu_md5_check_set = new CLI_sms_tpdu_md5_check_set();

	public class CLI_sms_tpdu_md5_check_info extends MethodDescriptor {

		public CLI_sms_tpdu_md5_check_info() {
			super("INFO", "Operand description ", "MD5_SMS_TPDU_UD,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_md5_check_info _cli_sms_tpdu_md5_check_info = new CLI_sms_tpdu_md5_check_info();

	public class GRP_SMS_TPDU_DCS extends GroupDescriptor {

		public GRP_SMS_TPDU_DCS() {
			super("SMS_TPDU_DCS", "SMS message encoding ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_TPDU_DCS _grp_SMS_TPDU_DCS = new GRP_SMS_TPDU_DCS();


	public class CLI_sms_tpdu_dcs_set extends MethodDescriptor {

		public CLI_sms_tpdu_dcs_set() {
			super("SET", "Set as rule operand ", "SMS_TPDU_DCS,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dcs_set _cli_sms_tpdu_dcs_set = new CLI_sms_tpdu_dcs_set();

	public class CLI_sms_tpdu_dcs_info extends MethodDescriptor {

		public CLI_sms_tpdu_dcs_info() {
			super("INFO", "Operand description ", "SMS_TPDU_DCS,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_dcs_info _cli_sms_tpdu_dcs_info = new CLI_sms_tpdu_dcs_info();

	public class GRP_SMS_MSG_TYPE extends GroupDescriptor {

		public GRP_SMS_MSG_TYPE() {
			super("SMS_MSG_TYPE", "SMS message type ", "SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMS_MSG_TYPE _grp_SMS_MSG_TYPE = new GRP_SMS_MSG_TYPE();


	public class CLI_sms_tpdu_msg_type_set extends MethodDescriptor {

		public CLI_sms_tpdu_msg_type_set() {
			super("SET", "Set as rule operand ", "SMS_MSG_TYPE,SMS_TPDU,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_msg_type_set _cli_sms_tpdu_msg_type_set = new CLI_sms_tpdu_msg_type_set();

	public class CLI_sms_tpdu_msg_type_info extends MethodDescriptor {

		public CLI_sms_tpdu_msg_type_info() {
			super("INFO", "Operand description ", "SMS_MSG_TYPE,SMS_TPDU,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sms_tpdu_msg_type_info _cli_sms_tpdu_msg_type_info = new CLI_sms_tpdu_msg_type_info();

	public class GRP_M3UA extends GroupDescriptor {

		public GRP_M3UA() {
			super("M3UA", "M3UA related operands ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_M3UA _grp_M3UA = new GRP_M3UA();


	public class GRP_M3UA_DPC extends GroupDescriptor {

		public GRP_M3UA_DPC() {
			super("M3UA_DPC", "Destination point code ", "M3UA,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_M3UA_DPC _grp_M3UA_DPC = new GRP_M3UA_DPC();


	public class CLI_m3ua_dpc_set extends MethodDescriptor {

		public CLI_m3ua_dpc_set() {
			super("SET", "Set as rule operand ", "M3UA_DPC,M3UA,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_m3ua_dpc_set _cli_m3ua_dpc_set = new CLI_m3ua_dpc_set();

	public class CLI_m3ua_dpc_info extends MethodDescriptor {

		public CLI_m3ua_dpc_info() {
			super("INFO", "Operand description ", "M3UA_DPC,M3UA,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_m3ua_dpc_info _cli_m3ua_dpc_info = new CLI_m3ua_dpc_info();

	public class GRP_M3UA_OPC extends GroupDescriptor {

		public GRP_M3UA_OPC() {
			super("M3UA_OPC", "Originating point code ", "M3UA,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_M3UA_OPC _grp_M3UA_OPC = new GRP_M3UA_OPC();


	public class CLI_m3ua_opc_set extends MethodDescriptor {

		public CLI_m3ua_opc_set() {
			super("SET", "Set as rule operand ", "M3UA_OPC,M3UA,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_m3ua_opc_set _cli_m3ua_opc_set = new CLI_m3ua_opc_set();

	public class CLI_m3ua_opc_info extends MethodDescriptor {

		public CLI_m3ua_opc_info() {
			super("INFO", "Operand description ", "M3UA_OPC,M3UA,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_m3ua_opc_info _cli_m3ua_opc_info = new CLI_m3ua_opc_info();

	public class GRP_MAP extends GroupDescriptor {

		public GRP_MAP() {
			super("MAP", "MAP related operands ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP _grp_MAP = new GRP_MAP();


	public class GRP_MAP_LIST extends GroupDescriptor {

		public GRP_MAP_LIST() {
			super("MAP_LIST", "Prefined lists ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_LIST _grp_MAP_LIST = new GRP_MAP_LIST();


	public class GRP_MAP_SCOA_WL extends GroupDescriptor {

		public GRP_MAP_SCOA_WL() {
			super("MAP_SCOA_WL", "Service Centre Originating Address White list ", "MAP_LIST,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_SCOA_WL _grp_MAP_SCOA_WL = new GRP_MAP_SCOA_WL();


	public class CLI_map_list_scoa_wl_set extends MethodDescriptor {

		public CLI_map_list_scoa_wl_set() {
			super("SET", "Set operand value ", "MAP_SCOA_WL,MAP_LIST,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_list_scoa_wl_set _cli_map_list_scoa_wl_set = new CLI_map_list_scoa_wl_set();

	public class GRP_MAP_SCDA_WL extends GroupDescriptor {

		public GRP_MAP_SCDA_WL() {
			super("MAP_SCDA_WL", "Service Centre Destination Address White list ", "MAP_LIST,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_SCDA_WL _grp_MAP_SCDA_WL = new GRP_MAP_SCDA_WL();


	public class CLI_map_list_scda_wl_set extends MethodDescriptor {

		public CLI_map_list_scda_wl_set() {
			super("SET", "Set operand value ", "MAP_SCDA_WL,MAP_LIST,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_list_scda_wl_set _cli_map_list_scda_wl_set = new CLI_map_list_scda_wl_set();

	public class GRP_MAP_SCOA extends GroupDescriptor {

		public GRP_MAP_SCOA() {
			super("MAP_SCOA", "Service centre originating address ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_SCOA _grp_MAP_SCOA = new GRP_MAP_SCOA();


	public class CLI_map_scoa_set extends MethodDescriptor {

		public CLI_map_scoa_set() {
			super("SET", "Set as rule operand ", "MAP_SCOA,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_scoa_set _cli_map_scoa_set = new CLI_map_scoa_set();

	public class CLI_map_scoa_regex extends MethodDescriptor {

		public CLI_map_scoa_regex() {
			super("REGEX", "Set as regex rule operand ", "MAP_SCOA,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_scoa_regex _cli_map_scoa_regex = new CLI_map_scoa_regex();

	public class CLI_map_scoa_info extends MethodDescriptor {

		public CLI_map_scoa_info() {
			super("INFO", "Operand description ", "MAP_SCOA,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_scoa_info _cli_map_scoa_info = new CLI_map_scoa_info();

	public class GRP_MAP_SCDA extends GroupDescriptor {

		public GRP_MAP_SCDA() {
			super("MAP_SCDA", "Service centre destination address ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_SCDA _grp_MAP_SCDA = new GRP_MAP_SCDA();


	public class CLI_map_scda_set extends MethodDescriptor {

		public CLI_map_scda_set() {
			super("SET", "Set as rule operand ", "MAP_SCDA,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_scda_set _cli_map_scda_set = new CLI_map_scda_set();

	public class CLI_map_scda_regex extends MethodDescriptor {

		public CLI_map_scda_regex() {
			super("REGEX", "Set as regex rule operand ", "MAP_SCDA,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_scda_regex _cli_map_scda_regex = new CLI_map_scda_regex();

	public class CLI_map_scda_info extends MethodDescriptor {

		public CLI_map_scda_info() {
			super("INFO", "Operand description ", "MAP_SCDA,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_scda_info _cli_map_scda_info = new CLI_map_scda_info();

	public class GRP_MAP_IMSI extends GroupDescriptor {

		public GRP_MAP_IMSI() {
			super("MAP_IMSI", "SMS IMSI ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_IMSI _grp_MAP_IMSI = new GRP_MAP_IMSI();


	public class CLI_map_imsi_set extends MethodDescriptor {

		public CLI_map_imsi_set() {
			super("SET", "Set as rule operand ", "MAP_IMSI,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_imsi_set _cli_map_imsi_set = new CLI_map_imsi_set();

	public class CLI_map_imsi_regex extends MethodDescriptor {

		public CLI_map_imsi_regex() {
			super("REGEX", "Set as regex rule operand ", "MAP_IMSI,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_imsi_regex _cli_map_imsi_regex = new CLI_map_imsi_regex();

	public class CLI_map_imsi_info extends MethodDescriptor {

		public CLI_map_imsi_info() {
			super("INFO", "Operand description ", "MAP_IMSI,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_imsi_info _cli_map_imsi_info = new CLI_map_imsi_info();

	public class GRP_MAP_MSISDN extends GroupDescriptor {

		public GRP_MAP_MSISDN() {
			super("MAP_MSISDN", "SMS MSISDN ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_MAP_MSISDN _grp_MAP_MSISDN = new GRP_MAP_MSISDN();


	public class CLI_map_msisdn_set extends MethodDescriptor {

		public CLI_map_msisdn_set() {
			super("SET", "Set as rule operand ", "MAP_MSISDN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_msisdn_set _cli_map_msisdn_set = new CLI_map_msisdn_set();

	public class CLI_map_msisdn_regex extends MethodDescriptor {

		public CLI_map_msisdn_regex() {
			super("REGEX", "Set as regex rule operand ", "MAP_MSISDN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_msisdn_regex _cli_map_msisdn_regex = new CLI_map_msisdn_regex();

	public class CLI_map_msisdn_info extends MethodDescriptor {

		public CLI_map_msisdn_info() {
			super("INFO", "Operand description ", "MAP_MSISDN,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_map_msisdn_info _cli_map_msisdn_info = new CLI_map_msisdn_info();

	public class GRP_HLR_IMSI extends GroupDescriptor {

		public GRP_HLR_IMSI() {
			super("HLR_IMSI", "HLR IMSI ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_IMSI _grp_HLR_IMSI = new GRP_HLR_IMSI();


	public class CLI_hlr_imsi_set extends MethodDescriptor {

		public CLI_hlr_imsi_set() {
			super("SET", "Set as rule operand ", "HLR_IMSI,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_imsi_set _cli_hlr_imsi_set = new CLI_hlr_imsi_set();

	public class CLI_hlr_imsi_regex extends MethodDescriptor {

		public CLI_hlr_imsi_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_IMSI,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_imsi_regex _cli_hlr_imsi_regex = new CLI_hlr_imsi_regex();

	public class CLI_hlr_imsi_info extends MethodDescriptor {

		public CLI_hlr_imsi_info() {
			super("INFO", "Operand description ", "HLR_IMSI,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_imsi_info _cli_hlr_imsi_info = new CLI_hlr_imsi_info();

	public class GRP_HLR_MSISDN extends GroupDescriptor {

		public GRP_HLR_MSISDN() {
			super("HLR_MSISDN", "HLR MSISDN ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_MSISDN _grp_HLR_MSISDN = new GRP_HLR_MSISDN();


	public class CLI_hlr_msisdn_set extends MethodDescriptor {

		public CLI_hlr_msisdn_set() {
			super("SET", "Set as rule operand ", "HLR_MSISDN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_msisdn_set _cli_hlr_msisdn_set = new CLI_hlr_msisdn_set();

	public class CLI_hlr_msisdn_regex extends MethodDescriptor {

		public CLI_hlr_msisdn_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_MSISDN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_msisdn_regex _cli_hlr_msisdn_regex = new CLI_hlr_msisdn_regex();

	public class CLI_hlr_msisdn_info extends MethodDescriptor {

		public CLI_hlr_msisdn_info() {
			super("INFO", "Operand description ", "HLR_MSISDN,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_msisdn_info _cli_hlr_msisdn_info = new CLI_hlr_msisdn_info();

	public class GRP_HLR_NNN extends GroupDescriptor {

		public GRP_HLR_NNN() {
			super("HLR_NNN", "HLR Network node number ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_NNN _grp_HLR_NNN = new GRP_HLR_NNN();


	public class CLI_hlr_nnn_set extends MethodDescriptor {

		public CLI_hlr_nnn_set() {
			super("SET", "Set as rule operand ", "HLR_NNN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_nnn_set _cli_hlr_nnn_set = new CLI_hlr_nnn_set();

	public class CLI_hlr_nnn_regex extends MethodDescriptor {

		public CLI_hlr_nnn_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_NNN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_nnn_regex _cli_hlr_nnn_regex = new CLI_hlr_nnn_regex();

	public class CLI_hlr_nnn_info extends MethodDescriptor {

		public CLI_hlr_nnn_info() {
			super("INFO", "Operand description ", "HLR_NNN,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_nnn_info _cli_hlr_nnn_info = new CLI_hlr_nnn_info();

	public class GRP_HLR_ANNN extends GroupDescriptor {

		public GRP_HLR_ANNN() {
			super("HLR_ANNN", "HLR Additional network node number ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_ANNN _grp_HLR_ANNN = new GRP_HLR_ANNN();


	public class CLI_hlr_annn_set extends MethodDescriptor {

		public CLI_hlr_annn_set() {
			super("SET", "Set as rule operand ", "HLR_ANNN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_annn_set _cli_hlr_annn_set = new CLI_hlr_annn_set();

	public class CLI_hlr_annn_regex extends MethodDescriptor {

		public CLI_hlr_annn_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_ANNN,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_annn_regex _cli_hlr_annn_regex = new CLI_hlr_annn_regex();

	public class CLI_hlr_annn_info extends MethodDescriptor {

		public CLI_hlr_annn_info() {
			super("INFO", "Operand description ", "HLR_ANNN,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_annn_info _cli_hlr_annn_info = new CLI_hlr_annn_info();

	public class GRP_HLR_SCA extends GroupDescriptor {

		public GRP_HLR_SCA() {
			super("HLR_SCA", "HLR Service centre address ", "MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_SCA _grp_HLR_SCA = new GRP_HLR_SCA();


	public class CLI_hlr_sca_set extends MethodDescriptor {

		public CLI_hlr_sca_set() {
			super("SET", "Set as rule operand ", "HLR_SCA,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_sca_set _cli_hlr_sca_set = new CLI_hlr_sca_set();

	public class CLI_hlr_sca_regex extends MethodDescriptor {

		public CLI_hlr_sca_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_SCA,MAP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_sca_regex _cli_hlr_sca_regex = new CLI_hlr_sca_regex();

	public class CLI_hlr_sca_info extends MethodDescriptor {

		public CLI_hlr_sca_info() {
			super("INFO", "Operand description ", "HLR_SCA,MAP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_sca_info _cli_hlr_sca_info = new CLI_hlr_sca_info();

	public class GRP_HLR_RESULT extends GroupDescriptor {

		public GRP_HLR_RESULT() {
			super("HLR_RESULT", "HLR RESULT related operands ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_RESULT _grp_HLR_RESULT = new GRP_HLR_RESULT();


	public class GRP_HLR_RESULT_IMSI extends GroupDescriptor {

		public GRP_HLR_RESULT_IMSI() {
			super("HLR_RESULT_IMSI", "HLR RESULT IMSI ", "HLR_RESULT,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_RESULT_IMSI _grp_HLR_RESULT_IMSI = new GRP_HLR_RESULT_IMSI();


	public class CLI_hlr_result_imsi_set extends MethodDescriptor {

		public CLI_hlr_result_imsi_set() {
			super("SET", "Set as rule operand ", "HLR_RESULT_IMSI,HLR_RESULT,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_imsi_set _cli_hlr_result_imsi_set = new CLI_hlr_result_imsi_set();

	public class CLI_hlr_result_imsi_regex extends MethodDescriptor {

		public CLI_hlr_result_imsi_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_RESULT_IMSI,HLR_RESULT,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_imsi_regex _cli_hlr_result_imsi_regex = new CLI_hlr_result_imsi_regex();

	public class CLI_hlr_result_imsi_info extends MethodDescriptor {

		public CLI_hlr_result_imsi_info() {
			super("INFO", "Operand description ", "HLR_RESULT_IMSI,HLR_RESULT,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_imsi_info _cli_hlr_result_imsi_info = new CLI_hlr_result_imsi_info();

	public class GRP_HLR_RESULT_NNN extends GroupDescriptor {

		public GRP_HLR_RESULT_NNN() {
			super("HLR_RESULT_NNN", "HLR RESULT Network node number ", "HLR_RESULT,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_RESULT_NNN _grp_HLR_RESULT_NNN = new GRP_HLR_RESULT_NNN();


	public class CLI_hlr_result_nnn_set extends MethodDescriptor {

		public CLI_hlr_result_nnn_set() {
			super("SET", "Set as rule operand ", "HLR_RESULT_NNN,HLR_RESULT,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_nnn_set _cli_hlr_result_nnn_set = new CLI_hlr_result_nnn_set();

	public class CLI_hlr_result_nnn_regex extends MethodDescriptor {

		public CLI_hlr_result_nnn_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_RESULT_NNN,HLR_RESULT,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_nnn_regex _cli_hlr_result_nnn_regex = new CLI_hlr_result_nnn_regex();

	public class CLI_hlr_result_nnn_info extends MethodDescriptor {

		public CLI_hlr_result_nnn_info() {
			super("INFO", "Operand description ", "HLR_RESULT_NNN,HLR_RESULT,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_nnn_info _cli_hlr_result_nnn_info = new CLI_hlr_result_nnn_info();

	public class GRP_HLR_RESULT_ANNN extends GroupDescriptor {

		public GRP_HLR_RESULT_ANNN() {
			super("HLR_RESULT_ANNN", "HLR RESULT Additional network node number ", "HLR_RESULT,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_HLR_RESULT_ANNN _grp_HLR_RESULT_ANNN = new GRP_HLR_RESULT_ANNN();


	public class CLI_hlr_result_annn_set extends MethodDescriptor {

		public CLI_hlr_result_annn_set() {
			super("SET", "Set as rule operand ", "HLR_RESULT_ANNN,HLR_RESULT,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_annn_set _cli_hlr_result_annn_set = new CLI_hlr_result_annn_set();

	public class CLI_hlr_result_annn_regex extends MethodDescriptor {

		public CLI_hlr_result_annn_regex() {
			super("REGEX", "Set as regex rule operand ", "HLR_RESULT_ANNN,HLR_RESULT,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_annn_regex _cli_hlr_result_annn_regex = new CLI_hlr_result_annn_regex();

	public class CLI_hlr_result_annn_info extends MethodDescriptor {

		public CLI_hlr_result_annn_info() {
			super("INFO", "Operand description ", "HLR_RESULT_ANNN,HLR_RESULT,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_hlr_result_annn_info _cli_hlr_result_annn_info = new CLI_hlr_result_annn_info();

	public class GRP_SCCP extends GroupDescriptor {

		public GRP_SCCP() {
			super("SCCP", "SCCP related operands ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP _grp_SCCP = new GRP_SCCP();


	public class GRP_SCCP_LIST extends GroupDescriptor {

		public GRP_SCCP_LIST() {
			super("SCCP_LIST", "Prefined lists ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_LIST _grp_SCCP_LIST = new GRP_SCCP_LIST();


	public class GRP_SCCP_GT_CALLED_WL extends GroupDescriptor {

		public GRP_SCCP_GT_CALLED_WL() {
			super("SCCP_GT_CALLED_WL", "GT Called White list ", "SCCP_LIST,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLED_WL _grp_SCCP_GT_CALLED_WL = new GRP_SCCP_GT_CALLED_WL();


	public class CLI_sccp_list_gt_called_wl_set extends MethodDescriptor {

		public CLI_sccp_list_gt_called_wl_set() {
			super("SET", "Set operand value ", "SCCP_GT_CALLED_WL,SCCP_LIST,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_list_gt_called_wl_set _cli_sccp_list_gt_called_wl_set = new CLI_sccp_list_gt_called_wl_set();

	public class GRP_SCCP_GT_CALLING_WL extends GroupDescriptor {

		public GRP_SCCP_GT_CALLING_WL() {
			super("SCCP_GT_CALLING_WL", "GT Calling White list ", "SCCP_LIST,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLING_WL _grp_SCCP_GT_CALLING_WL = new GRP_SCCP_GT_CALLING_WL();


	public class CLI_sccp_list_gt_calling_wl_set extends MethodDescriptor {

		public CLI_sccp_list_gt_calling_wl_set() {
			super("SET", "Set operand value ", "SCCP_GT_CALLING_WL,SCCP_LIST,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_list_gt_calling_wl_set _cli_sccp_list_gt_calling_wl_set = new CLI_sccp_list_gt_calling_wl_set();

	public class GRP_NP extends GroupDescriptor {

		public GRP_NP() {
			super("NP", "Numbering plan ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP _grp_NP = new GRP_NP();


	public class GRP_NP_UNKNOWN extends GroupDescriptor {

		public GRP_NP_UNKNOWN() {
			super("NP_UNKNOWN", "Unknown ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_UNKNOWN _grp_NP_UNKNOWN = new GRP_NP_UNKNOWN();


	public class CLI_sccp_np_unknown_set extends MethodDescriptor {

		public CLI_sccp_np_unknown_set() {
			super("SET", "Set operand value ", "NP_UNKNOWN,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_unknown_set _cli_sccp_np_unknown_set = new CLI_sccp_np_unknown_set();

	public class GRP_NP_ISDN_TELEPHONE extends GroupDescriptor {

		public GRP_NP_ISDN_TELEPHONE() {
			super("NP_ISDN_TELEPHONE", "ISDN Telephone ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_ISDN_TELEPHONE _grp_NP_ISDN_TELEPHONE = new GRP_NP_ISDN_TELEPHONE();


	public class CLI_sccp_np_isdn_telephone_set extends MethodDescriptor {

		public CLI_sccp_np_isdn_telephone_set() {
			super("SET", "Set operand value ", "NP_ISDN_TELEPHONE,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_isdn_telephone_set _cli_sccp_np_isdn_telephone_set = new CLI_sccp_np_isdn_telephone_set();

	public class GRP_NP_GENERIC extends GroupDescriptor {

		public GRP_NP_GENERIC() {
			super("NP_GENERIC", "Generic ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_GENERIC _grp_NP_GENERIC = new GRP_NP_GENERIC();


	public class CLI_sccp_np_generic_set extends MethodDescriptor {

		public CLI_sccp_np_generic_set() {
			super("SET", "Set operand value ", "NP_GENERIC,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_generic_set _cli_sccp_np_generic_set = new CLI_sccp_np_generic_set();

	public class GRP_NP_DATA_X121 extends GroupDescriptor {

		public GRP_NP_DATA_X121() {
			super("NP_DATA_X121", "DATA X121 ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_DATA_X121 _grp_NP_DATA_X121 = new GRP_NP_DATA_X121();


	public class CLI_sccp_np_data_x121_set extends MethodDescriptor {

		public CLI_sccp_np_data_x121_set() {
			super("SET", "Set operand value ", "NP_DATA_X121,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_data_x121_set _cli_sccp_np_data_x121_set = new CLI_sccp_np_data_x121_set();

	public class GRP_NP_TELEX extends GroupDescriptor {

		public GRP_NP_TELEX() {
			super("NP_TELEX", "Telex ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_TELEX _grp_NP_TELEX = new GRP_NP_TELEX();


	public class CLI_sccp_np_telex_set extends MethodDescriptor {

		public CLI_sccp_np_telex_set() {
			super("SET", "Set operand value ", "NP_TELEX,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_telex_set _cli_sccp_np_telex_set = new CLI_sccp_np_telex_set();

	public class GRP_NP_MARITIME extends GroupDescriptor {

		public GRP_NP_MARITIME() {
			super("NP_MARITIME", "Maritime ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_MARITIME _grp_NP_MARITIME = new GRP_NP_MARITIME();


	public class CLI_sccp_np_maritime_set extends MethodDescriptor {

		public CLI_sccp_np_maritime_set() {
			super("SET", "Set operand value ", "NP_MARITIME,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_maritime_set _cli_sccp_np_maritime_set = new CLI_sccp_np_maritime_set();

	public class GRP_NP_LAND_MOBILE extends GroupDescriptor {

		public GRP_NP_LAND_MOBILE() {
			super("NP_LAND_MOBILE", "Land mobile ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_LAND_MOBILE _grp_NP_LAND_MOBILE = new GRP_NP_LAND_MOBILE();


	public class CLI_sccp_np_land_mobile_set extends MethodDescriptor {

		public CLI_sccp_np_land_mobile_set() {
			super("SET", "Set operand value ", "NP_LAND_MOBILE,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_land_mobile_set _cli_sccp_np_land_mobile_set = new CLI_sccp_np_land_mobile_set();

	public class GRP_NP_ISDN_MOBILE extends GroupDescriptor {

		public GRP_NP_ISDN_MOBILE() {
			super("NP_ISDN_MOBILE", "ISDN mobile ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_ISDN_MOBILE _grp_NP_ISDN_MOBILE = new GRP_NP_ISDN_MOBILE();


	public class CLI_sccp_np_isdn_mobile_set extends MethodDescriptor {

		public CLI_sccp_np_isdn_mobile_set() {
			super("SET", "Set operand value ", "NP_ISDN_MOBILE,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_isdn_mobile_set _cli_sccp_np_isdn_mobile_set = new CLI_sccp_np_isdn_mobile_set();

	public class GRP_NP_PRIVATE extends GroupDescriptor {

		public GRP_NP_PRIVATE() {
			super("NP_PRIVATE", "Private ", "NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NP_PRIVATE _grp_NP_PRIVATE = new GRP_NP_PRIVATE();


	public class CLI_sccp_np_private_set extends MethodDescriptor {

		public CLI_sccp_np_private_set() {
			super("SET", "Set operand value ", "NP_PRIVATE,NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_np_private_set _cli_sccp_np_private_set = new CLI_sccp_np_private_set();

	public class GRP_NAI extends GroupDescriptor {

		public GRP_NAI() {
			super("NAI", "Nature of address ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NAI _grp_NAI = new GRP_NAI();


	public class GRP_NAI_UNKNOWN extends GroupDescriptor {

		public GRP_NAI_UNKNOWN() {
			super("NAI_UNKNOWN", "Unknown ", "NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NAI_UNKNOWN _grp_NAI_UNKNOWN = new GRP_NAI_UNKNOWN();


	public class CLI_sccp_nai_unknown_set extends MethodDescriptor {

		public CLI_sccp_nai_unknown_set() {
			super("SET", "Set operand value ", "NAI_UNKNOWN,NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_nai_unknown_set _cli_sccp_nai_unknown_set = new CLI_sccp_nai_unknown_set();

	public class GRP_NAI_SUBSCRIBER_NUMBER extends GroupDescriptor {

		public GRP_NAI_SUBSCRIBER_NUMBER() {
			super("NAI_SUBSCRIBER_NUMBER", "Subscriber number ", "NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NAI_SUBSCRIBER_NUMBER _grp_NAI_SUBSCRIBER_NUMBER = new GRP_NAI_SUBSCRIBER_NUMBER();


	public class CLI_sccp_nai_subscriber_number_set extends MethodDescriptor {

		public CLI_sccp_nai_subscriber_number_set() {
			super("SET", "Set operand value ", "NAI_SUBSCRIBER_NUMBER,NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_nai_subscriber_number_set _cli_sccp_nai_subscriber_number_set = new CLI_sccp_nai_subscriber_number_set();

	public class GRP_NAI_RESERVED_FOR_NATIONAL_USE extends GroupDescriptor {

		public GRP_NAI_RESERVED_FOR_NATIONAL_USE() {
			super("NAI_RESERVED_FOR_NATIONAL_USE", "Reserved for national use ", "NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NAI_RESERVED_FOR_NATIONAL_USE _grp_NAI_RESERVED_FOR_NATIONAL_USE = new GRP_NAI_RESERVED_FOR_NATIONAL_USE();


	public class CLI_sccp_nai_national_use_set extends MethodDescriptor {

		public CLI_sccp_nai_national_use_set() {
			super("SET", "Set operand value ", "NAI_RESERVED_FOR_NATIONAL_USE,NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_nai_national_use_set _cli_sccp_nai_national_use_set = new CLI_sccp_nai_national_use_set();

	public class GRP_NAI_NATIONAL_SIGNIFICANT_NUMBER extends GroupDescriptor {

		public GRP_NAI_NATIONAL_SIGNIFICANT_NUMBER() {
			super("NAI_NATIONAL_SIGNIFICANT_NUMBER", "National significant number ", "NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NAI_NATIONAL_SIGNIFICANT_NUMBER _grp_NAI_NATIONAL_SIGNIFICANT_NUMBER = new GRP_NAI_NATIONAL_SIGNIFICANT_NUMBER();


	public class CLI_sccp_nai_national_significant_set extends MethodDescriptor {

		public CLI_sccp_nai_national_significant_set() {
			super("SET", "Set operand value ", "NAI_NATIONAL_SIGNIFICANT_NUMBER,NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_nai_national_significant_set _cli_sccp_nai_national_significant_set = new CLI_sccp_nai_national_significant_set();

	public class GRP_NAI_INTERNATIONAL extends GroupDescriptor {

		public GRP_NAI_INTERNATIONAL() {
			super("NAI_INTERNATIONAL", "International ", "NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NAI_INTERNATIONAL _grp_NAI_INTERNATIONAL = new GRP_NAI_INTERNATIONAL();


	public class CLI_sccp_nai_international_set extends MethodDescriptor {

		public CLI_sccp_nai_international_set() {
			super("SET", "Set operand value ", "NAI_INTERNATIONAL,NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_nai_international_set _cli_sccp_nai_international_set = new CLI_sccp_nai_international_set();

	public class GRP_GTI extends GroupDescriptor {

		public GRP_GTI() {
			super("GTI", "Global title indicator ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GTI _grp_GTI = new GRP_GTI();


	public class GRP_GTI_NONE extends GroupDescriptor {

		public GRP_GTI_NONE() {
			super("GTI_NONE", "No Global title present ", "GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GTI_NONE _grp_GTI_NONE = new GRP_GTI_NONE();


	public class CLI_sccp_gti_none_set extends MethodDescriptor {

		public CLI_sccp_gti_none_set() {
			super("SET", "Set operand value ", "GTI_NONE,GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gti_none_set _cli_sccp_gti_none_set = new CLI_sccp_gti_none_set();

	public class GRP_GTI_NAI extends GroupDescriptor {

		public GRP_GTI_NAI() {
			super("GTI_NAI", "Nature of address only ", "GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GTI_NAI _grp_GTI_NAI = new GRP_GTI_NAI();


	public class CLI_sccp_gti_nai_set extends MethodDescriptor {

		public CLI_sccp_gti_nai_set() {
			super("SET", "Set operand value ", "GTI_NAI,GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gti_nai_set _cli_sccp_gti_nai_set = new CLI_sccp_gti_nai_set();

	public class GRP_GTI_TT extends GroupDescriptor {

		public GRP_GTI_TT() {
			super("GTI_TT", "Translation type only ", "GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GTI_TT _grp_GTI_TT = new GRP_GTI_TT();


	public class CLI_sccp_gti_tt_set extends MethodDescriptor {

		public CLI_sccp_gti_tt_set() {
			super("SET", "Set operand value ", "GTI_TT,GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gti_tt_set _cli_sccp_gti_tt_set = new CLI_sccp_gti_tt_set();

	public class GRP_GTI_TTNPE extends GroupDescriptor {

		public GRP_GTI_TTNPE() {
			super("GTI_TTNPE", "Translation type and numbering plan ", "GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GTI_TTNPE _grp_GTI_TTNPE = new GRP_GTI_TTNPE();


	public class CLI_sccp_gti_ttnpe_set extends MethodDescriptor {

		public CLI_sccp_gti_ttnpe_set() {
			super("SET", "Set operand value ", "GTI_TTNPE,GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gti_ttnpe_set _cli_sccp_gti_ttnpe_set = new CLI_sccp_gti_ttnpe_set();

	public class GRP_GTI_TTNPENOA extends GroupDescriptor {

		public GRP_GTI_TTNPENOA() {
			super("GTI_TTNPENOA", "All included ", "GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_GTI_TTNPENOA _grp_GTI_TTNPENOA = new GRP_GTI_TTNPENOA();


	public class CLI_sccp_gti_ttnpenoa_set extends MethodDescriptor {

		public CLI_sccp_gti_ttnpenoa_set() {
			super("SET", "Set operand value ", "GTI_TTNPENOA,GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gti_ttnpenoa_set _cli_sccp_gti_ttnpenoa_set = new CLI_sccp_gti_ttnpenoa_set();

	public class GRP_SCCP_GT_CALLED_ADDRESS extends GroupDescriptor {

		public GRP_SCCP_GT_CALLED_ADDRESS() {
			super("SCCP_GT_CALLED_ADDRESS", "GT Called address ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLED_ADDRESS _grp_SCCP_GT_CALLED_ADDRESS = new GRP_SCCP_GT_CALLED_ADDRESS();


	public class CLI_sccp_gt_called_address_set extends MethodDescriptor {

		public CLI_sccp_gt_called_address_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLED_ADDRESS,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_address_set _cli_sccp_gt_called_address_set = new CLI_sccp_gt_called_address_set();

	public class CLI_sccp_gt_called_address_regex extends MethodDescriptor {

		public CLI_sccp_gt_called_address_regex() {
			super("REGEX", "Set as regex rule operand ", "SCCP_GT_CALLED_ADDRESS,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_address_regex _cli_sccp_gt_called_address_regex = new CLI_sccp_gt_called_address_regex();

	public class CLI_sccp_gt_called_address_info extends MethodDescriptor {

		public CLI_sccp_gt_called_address_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLED_ADDRESS,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_address_info _cli_sccp_gt_called_address_info = new CLI_sccp_gt_called_address_info();

	public class GRP_SCCP_GT_CALLING_ADDRESS extends GroupDescriptor {

		public GRP_SCCP_GT_CALLING_ADDRESS() {
			super("SCCP_GT_CALLING_ADDRESS", "GT Calling address ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLING_ADDRESS _grp_SCCP_GT_CALLING_ADDRESS = new GRP_SCCP_GT_CALLING_ADDRESS();


	public class CLI_sccp_gt_calling_address_set extends MethodDescriptor {

		public CLI_sccp_gt_calling_address_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLING_ADDRESS,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_address_set _cli_sccp_gt_calling_address_set = new CLI_sccp_gt_calling_address_set();

	public class CLI_sccp_gt_calling_address_regex extends MethodDescriptor {

		public CLI_sccp_gt_calling_address_regex() {
			super("REGEX", "Set as regex rule operand ", "SCCP_GT_CALLING_ADDRESS,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_address_regex _cli_sccp_gt_calling_address_regex = new CLI_sccp_gt_calling_address_regex();

	public class CLI_sccp_gt_calling_address_info extends MethodDescriptor {

		public CLI_sccp_gt_calling_address_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLING_ADDRESS,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_address_info _cli_sccp_gt_calling_address_info = new CLI_sccp_gt_calling_address_info();

	public class GRP_SCCP_GT_CALLED_TT extends GroupDescriptor {

		public GRP_SCCP_GT_CALLED_TT() {
			super("SCCP_GT_CALLED_TT", "GT Called translation type ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLED_TT _grp_SCCP_GT_CALLED_TT = new GRP_SCCP_GT_CALLED_TT();


	public class CLI_sccp_gt_called_tt_set extends MethodDescriptor {

		public CLI_sccp_gt_called_tt_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLED_TT,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_tt_set _cli_sccp_gt_called_tt_set = new CLI_sccp_gt_called_tt_set();

	public class CLI_sccp_gt_called_tt_regex extends MethodDescriptor {

		public CLI_sccp_gt_called_tt_regex() {
			super("REGEX", "Set as regex rule operand ", "SCCP_GT_CALLED_TT,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_tt_regex _cli_sccp_gt_called_tt_regex = new CLI_sccp_gt_called_tt_regex();

	public class CLI_sccp_gt_called_tt_info extends MethodDescriptor {

		public CLI_sccp_gt_called_tt_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLED_TT,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_tt_info _cli_sccp_gt_called_tt_info = new CLI_sccp_gt_called_tt_info();

	public class GRP_SCCP_GT_CALLING_TT extends GroupDescriptor {

		public GRP_SCCP_GT_CALLING_TT() {
			super("SCCP_GT_CALLING_TT", "GT Calling translation type ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLING_TT _grp_SCCP_GT_CALLING_TT = new GRP_SCCP_GT_CALLING_TT();


	public class CLI_sccp_gt_calling_tt_set extends MethodDescriptor {

		public CLI_sccp_gt_calling_tt_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLING_TT,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_tt_set _cli_sccp_gt_calling_tt_set = new CLI_sccp_gt_calling_tt_set();

	public class CLI_sccp_gt_calling_tt_regex extends MethodDescriptor {

		public CLI_sccp_gt_calling_tt_regex() {
			super("REGEX", "Set as regex rule operand ", "SCCP_GT_CALLING_TT,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_tt_regex _cli_sccp_gt_calling_tt_regex = new CLI_sccp_gt_calling_tt_regex();

	public class CLI_sccp_gt_calling_tt_info extends MethodDescriptor {

		public CLI_sccp_gt_calling_tt_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLING_TT,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_tt_info _cli_sccp_gt_calling_tt_info = new CLI_sccp_gt_calling_tt_info();

	public class GRP_SCCP_GT_CALLED_NAI extends GroupDescriptor {

		public GRP_SCCP_GT_CALLED_NAI() {
			super("SCCP_GT_CALLED_NAI", "GT Called nature of address ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLED_NAI _grp_SCCP_GT_CALLED_NAI = new GRP_SCCP_GT_CALLED_NAI();


	public class CLI_sccp_gt_called_nai_set extends MethodDescriptor {

		public CLI_sccp_gt_called_nai_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLED_NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_nai_set _cli_sccp_gt_called_nai_set = new CLI_sccp_gt_called_nai_set();

	public class CLI_sccp_gt_called_nai_info extends MethodDescriptor {

		public CLI_sccp_gt_called_nai_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLED_NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_nai_info _cli_sccp_gt_called_nai_info = new CLI_sccp_gt_called_nai_info();

	public class GRP_SCCP_GT_CALLING_NAI extends GroupDescriptor {

		public GRP_SCCP_GT_CALLING_NAI() {
			super("SCCP_GT_CALLING_NAI", "GT Calling nature of address ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLING_NAI _grp_SCCP_GT_CALLING_NAI = new GRP_SCCP_GT_CALLING_NAI();


	public class CLI_sccp_gt_calling_nai_set extends MethodDescriptor {

		public CLI_sccp_gt_calling_nai_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLING_NAI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_nai_set _cli_sccp_gt_calling_nai_set = new CLI_sccp_gt_calling_nai_set();

	public class CLI_sccp_gt_calling_nai_info extends MethodDescriptor {

		public CLI_sccp_gt_calling_nai_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLING_NAI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_nai_info _cli_sccp_gt_calling_nai_info = new CLI_sccp_gt_calling_nai_info();

	public class GRP_SCCP_GT_CALLED_NP extends GroupDescriptor {

		public GRP_SCCP_GT_CALLED_NP() {
			super("SCCP_GT_CALLED_NP", "GT Called numbering plan ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLED_NP _grp_SCCP_GT_CALLED_NP = new GRP_SCCP_GT_CALLED_NP();


	public class CLI_sccp_gt_called_np_set extends MethodDescriptor {

		public CLI_sccp_gt_called_np_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLED_NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_np_set _cli_sccp_gt_called_np_set = new CLI_sccp_gt_called_np_set();

	public class CLI_sccp_gt_called_np_info extends MethodDescriptor {

		public CLI_sccp_gt_called_np_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLED_NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_np_info _cli_sccp_gt_called_np_info = new CLI_sccp_gt_called_np_info();

	public class GRP_SCCP_GT_CALLING_NP extends GroupDescriptor {

		public GRP_SCCP_GT_CALLING_NP() {
			super("SCCP_GT_CALLING_NP", "GT Calling numbering plan ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLING_NP _grp_SCCP_GT_CALLING_NP = new GRP_SCCP_GT_CALLING_NP();


	public class CLI_sccp_gt_calling_np_set extends MethodDescriptor {

		public CLI_sccp_gt_calling_np_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLING_NP,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_np_set _cli_sccp_gt_calling_np_set = new CLI_sccp_gt_calling_np_set();

	public class CLI_sccp_gt_calling_np_info extends MethodDescriptor {

		public CLI_sccp_gt_calling_np_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLING_NP,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_np_info _cli_sccp_gt_calling_np_info = new CLI_sccp_gt_calling_np_info();

	public class GRP_SCCP_GT_CALLED_GTI extends GroupDescriptor {

		public GRP_SCCP_GT_CALLED_GTI() {
			super("SCCP_GT_CALLED_GTI", "GT Called indicator type ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLED_GTI _grp_SCCP_GT_CALLED_GTI = new GRP_SCCP_GT_CALLED_GTI();


	public class CLI_sccp_gt_called_gti_set extends MethodDescriptor {

		public CLI_sccp_gt_called_gti_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLED_GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_gti_set _cli_sccp_gt_called_gti_set = new CLI_sccp_gt_called_gti_set();

	public class CLI_sccp_gt_called_gti_info extends MethodDescriptor {

		public CLI_sccp_gt_called_gti_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLED_GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_called_gti_info _cli_sccp_gt_called_gti_info = new CLI_sccp_gt_called_gti_info();

	public class GRP_SCCP_GT_CALLING_GTI extends GroupDescriptor {

		public GRP_SCCP_GT_CALLING_GTI() {
			super("SCCP_GT_CALLING_GTI", "GT Calling indicator type ", "SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SCCP_GT_CALLING_GTI _grp_SCCP_GT_CALLING_GTI = new GRP_SCCP_GT_CALLING_GTI();


	public class CLI_sccp_gt_calling_gti_set extends MethodDescriptor {

		public CLI_sccp_gt_calling_gti_set() {
			super("SET", "Set as rule operand ", "SCCP_GT_CALLING_GTI,SCCP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_gti_set _cli_sccp_gt_calling_gti_set = new CLI_sccp_gt_calling_gti_set();

	public class CLI_sccp_gt_calling_gti_info extends MethodDescriptor {

		public CLI_sccp_gt_calling_gti_info() {
			super("INFO", "Operand description ", "SCCP_GT_CALLING_GTI,SCCP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_sccp_gt_calling_gti_info _cli_sccp_gt_calling_gti_info = new CLI_sccp_gt_calling_gti_info();

	public class GRP_SMPP extends GroupDescriptor {

		public GRP_SMPP() {
			super("SMPP", "SMPP related operands ", "OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP _grp_SMPP = new GRP_SMPP();


	public class GRP_RD_SMSCDR extends GroupDescriptor {

		public GRP_RD_SMSCDR() {
			super("RD_SMSCDR", "Registered delivery SMSC Delivery Receipt ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_RD_SMSCDR _grp_RD_SMSCDR = new GRP_RD_SMSCDR();


	public class GRP_SMPP_RD_SMSCDR_NO extends GroupDescriptor {

		public GRP_SMPP_RD_SMSCDR_NO() {
			super("SMPP_RD_SMSCDR_NO", "No receipt ", "RD_SMSCDR,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMSCDR_NO _grp_SMPP_RD_SMSCDR_NO = new GRP_SMPP_RD_SMSCDR_NO();


	public class CLI_smpp_rd_smscdr_no_set extends MethodDescriptor {

		public CLI_smpp_rd_smscdr_no_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMSCDR_NO,RD_SMSCDR,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smscdr_no_set _cli_smpp_rd_smscdr_no_set = new CLI_smpp_rd_smscdr_no_set();

	public class GRP_SMPP_RD_SMSCDR_SUCCESS_FAILURE extends GroupDescriptor {

		public GRP_SMPP_RD_SMSCDR_SUCCESS_FAILURE() {
			super("SMPP_RD_SMSCDR_SUCCESS_FAILURE", "Success and Failure ", "RD_SMSCDR,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMSCDR_SUCCESS_FAILURE _grp_SMPP_RD_SMSCDR_SUCCESS_FAILURE = new GRP_SMPP_RD_SMSCDR_SUCCESS_FAILURE();


	public class CLI_smpp_rd_smscdr_sf_set extends MethodDescriptor {

		public CLI_smpp_rd_smscdr_sf_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMSCDR_SUCCESS_FAILURE,RD_SMSCDR,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smscdr_sf_set _cli_smpp_rd_smscdr_sf_set = new CLI_smpp_rd_smscdr_sf_set();

	public class GRP_SMPP_RD_SMSCDR_FAILURE extends GroupDescriptor {

		public GRP_SMPP_RD_SMSCDR_FAILURE() {
			super("SMPP_RD_SMSCDR_FAILURE", "Failure only ", "RD_SMSCDR,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMSCDR_FAILURE _grp_SMPP_RD_SMSCDR_FAILURE = new GRP_SMPP_RD_SMSCDR_FAILURE();


	public class CLI_smpp_rd_smscdr_f_set extends MethodDescriptor {

		public CLI_smpp_rd_smscdr_f_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMSCDR_FAILURE,RD_SMSCDR,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smscdr_f_set _cli_smpp_rd_smscdr_f_set = new CLI_smpp_rd_smscdr_f_set();

	public class GRP_RD_SMEOA extends GroupDescriptor {

		public GRP_RD_SMEOA() {
			super("RD_SMEOA", "Registered delivery SME Originated Acknowledgement ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_RD_SMEOA _grp_RD_SMEOA = new GRP_RD_SMEOA();


	public class GRP_SMPP_RD_SMEOA_NO extends GroupDescriptor {

		public GRP_SMPP_RD_SMEOA_NO() {
			super("SMPP_RD_SMEOA_NO", "No receipt ", "RD_SMEOA,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMEOA_NO _grp_SMPP_RD_SMEOA_NO = new GRP_SMPP_RD_SMEOA_NO();


	public class CLI_smpp_rd_smeoa_no_set extends MethodDescriptor {

		public CLI_smpp_rd_smeoa_no_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMEOA_NO,RD_SMEOA,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smeoa_no_set _cli_smpp_rd_smeoa_no_set = new CLI_smpp_rd_smeoa_no_set();

	public class GRP_SMPP_RD_SMEOA_ACK extends GroupDescriptor {

		public GRP_SMPP_RD_SMEOA_ACK() {
			super("SMPP_RD_SMEOA_ACK", "Acknowledgement requested ", "RD_SMEOA,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMEOA_ACK _grp_SMPP_RD_SMEOA_ACK = new GRP_SMPP_RD_SMEOA_ACK();


	public class CLI_smpp_rd_smeoa_ack_set extends MethodDescriptor {

		public CLI_smpp_rd_smeoa_ack_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMEOA_ACK,RD_SMEOA,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smeoa_ack_set _cli_smpp_rd_smeoa_ack_set = new CLI_smpp_rd_smeoa_ack_set();

	public class GRP_SMPP_RD_SMEOA_MANUAL_USER_ACK extends GroupDescriptor {

		public GRP_SMPP_RD_SMEOA_MANUAL_USER_ACK() {
			super("SMPP_RD_SMEOA_MANUAL_USER_ACK", "Manual or User acknowledgment requested ", "RD_SMEOA,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMEOA_MANUAL_USER_ACK _grp_SMPP_RD_SMEOA_MANUAL_USER_ACK = new GRP_SMPP_RD_SMEOA_MANUAL_USER_ACK();


	public class CLI_smpp_rd_smeoa_manual_user_set extends MethodDescriptor {

		public CLI_smpp_rd_smeoa_manual_user_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMEOA_MANUAL_USER_ACK,RD_SMEOA,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smeoa_manual_user_set _cli_smpp_rd_smeoa_manual_user_set = new CLI_smpp_rd_smeoa_manual_user_set();

	public class GRP_SMPP_RD_SMEOA_BOTH extends GroupDescriptor {

		public GRP_SMPP_RD_SMEOA_BOTH() {
			super("SMPP_RD_SMEOA_BOTH", "Both types requested ", "RD_SMEOA,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMEOA_BOTH _grp_SMPP_RD_SMEOA_BOTH = new GRP_SMPP_RD_SMEOA_BOTH();


	public class CLI_smpp_rd_smeoa_both_set extends MethodDescriptor {

		public CLI_smpp_rd_smeoa_both_set() {
			super("SET", "Set operand value ", "SMPP_RD_SMEOA_BOTH,RD_SMEOA,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smeoa_both_set _cli_smpp_rd_smeoa_both_set = new CLI_smpp_rd_smeoa_both_set();

	public class GRP_RD_IN extends GroupDescriptor {

		public GRP_RD_IN() {
			super("RD_IN", "Registered delivery Intermediate Notification ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_RD_IN _grp_RD_IN = new GRP_RD_IN();


	public class GRP_NO extends GroupDescriptor {

		public GRP_NO() {
			super("NO", "No ", "RD_IN,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_NO _grp_NO = new GRP_NO();


	public class CLI_smpp_rd_in_no_set extends MethodDescriptor {

		public CLI_smpp_rd_in_no_set() {
			super("SET", "Set operand value ", "NO,RD_IN,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_in_no_set _cli_smpp_rd_in_no_set = new CLI_smpp_rd_in_no_set();

	public class GRP_YES extends GroupDescriptor {

		public GRP_YES() {
			super("YES", "Yes ", "RD_IN,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_YES _grp_YES = new GRP_YES();


	public class CLI_smpp_rd_in_yes_set extends MethodDescriptor {

		public CLI_smpp_rd_in_yes_set() {
			super("SET", "Set operand value ", "YES,RD_IN,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_in_yes_set _cli_smpp_rd_in_yes_set = new CLI_smpp_rd_in_yes_set();

	public class GRP_DATA_CODING extends GroupDescriptor {

		public GRP_DATA_CODING() {
			super("DATA_CODING", "Data coding ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_DATA_CODING _grp_DATA_CODING = new GRP_DATA_CODING();


	public class GRP_SMPP_DC_DEFAULT extends GroupDescriptor {

		public GRP_SMPP_DC_DEFAULT() {
			super("SMPP_DC_DEFAULT", "Default ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_DEFAULT _grp_SMPP_DC_DEFAULT = new GRP_SMPP_DC_DEFAULT();


	public class CLI_smpp_dc_default_set extends MethodDescriptor {

		public CLI_smpp_dc_default_set() {
			super("SET", "Set operand value ", "SMPP_DC_DEFAULT,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_default_set _cli_smpp_dc_default_set = new CLI_smpp_dc_default_set();

	public class GRP_SMPP_DC_IA5_ASCII extends GroupDescriptor {

		public GRP_SMPP_DC_IA5_ASCII() {
			super("SMPP_DC_IA5_ASCII", "IA5 ASCII ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_IA5_ASCII _grp_SMPP_DC_IA5_ASCII = new GRP_SMPP_DC_IA5_ASCII();


	public class CLI_smpp_dc_ia5_ascii_set extends MethodDescriptor {

		public CLI_smpp_dc_ia5_ascii_set() {
			super("SET", "Set operand value ", "SMPP_DC_IA5_ASCII,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_ia5_ascii_set _cli_smpp_dc_ia5_ascii_set = new CLI_smpp_dc_ia5_ascii_set();

	public class GRP_SMPP_DC_8BIT_BINARY_1 extends GroupDescriptor {

		public GRP_SMPP_DC_8BIT_BINARY_1() {
			super("SMPP_DC_8BIT_BINARY_1", "Binary type 1 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_8BIT_BINARY_1 _grp_SMPP_DC_8BIT_BINARY_1 = new GRP_SMPP_DC_8BIT_BINARY_1();


	public class CLI_smpp_dc_8bit_1_set extends MethodDescriptor {

		public CLI_smpp_dc_8bit_1_set() {
			super("SET", "Set operand value ", "SMPP_DC_8BIT_BINARY_1,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_8bit_1_set _cli_smpp_dc_8bit_1_set = new CLI_smpp_dc_8bit_1_set();

	public class GRP_SMPP_DC_ISO_8859_1 extends GroupDescriptor {

		public GRP_SMPP_DC_ISO_8859_1() {
			super("SMPP_DC_ISO_8859_1", "ISO 8859 1 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_ISO_8859_1 _grp_SMPP_DC_ISO_8859_1 = new GRP_SMPP_DC_ISO_8859_1();


	public class CLI_smpp_dc_iso_8859_1_set extends MethodDescriptor {

		public CLI_smpp_dc_iso_8859_1_set() {
			super("SET", "Set operand value ", "SMPP_DC_ISO_8859_1,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_iso_8859_1_set _cli_smpp_dc_iso_8859_1_set = new CLI_smpp_dc_iso_8859_1_set();

	public class GRP_SMPP_DC_8BIT_BINARY_2 extends GroupDescriptor {

		public GRP_SMPP_DC_8BIT_BINARY_2() {
			super("SMPP_DC_8BIT_BINARY_2", "Binary type 2 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_8BIT_BINARY_2 _grp_SMPP_DC_8BIT_BINARY_2 = new GRP_SMPP_DC_8BIT_BINARY_2();


	public class CLI_smpp_dc_8bit_2_set extends MethodDescriptor {

		public CLI_smpp_dc_8bit_2_set() {
			super("SET", "Set operand value ", "SMPP_DC_8BIT_BINARY_2,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_8bit_2_set _cli_smpp_dc_8bit_2_set = new CLI_smpp_dc_8bit_2_set();

	public class GRP_SMPP_DC_JIS extends GroupDescriptor {

		public GRP_SMPP_DC_JIS() {
			super("SMPP_DC_JIS", "JIS ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_JIS _grp_SMPP_DC_JIS = new GRP_SMPP_DC_JIS();


	public class CLI_smpp_dc_jis_set extends MethodDescriptor {

		public CLI_smpp_dc_jis_set() {
			super("SET", "Set operand value ", "SMPP_DC_JIS,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_jis_set _cli_smpp_dc_jis_set = new CLI_smpp_dc_jis_set();

	public class GRP_SMPP_DC_ISO_8859_5 extends GroupDescriptor {

		public GRP_SMPP_DC_ISO_8859_5() {
			super("SMPP_DC_ISO_8859_5", "ISO 8859 5 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_ISO_8859_5 _grp_SMPP_DC_ISO_8859_5 = new GRP_SMPP_DC_ISO_8859_5();


	public class CLI_smpp_dc_iso_8859_5_set extends MethodDescriptor {

		public CLI_smpp_dc_iso_8859_5_set() {
			super("SET", "Set operand value ", "SMPP_DC_ISO_8859_5,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_iso_8859_5_set _cli_smpp_dc_iso_8859_5_set = new CLI_smpp_dc_iso_8859_5_set();

	public class GRP_SMPP_DC_ISO_8859_8 extends GroupDescriptor {

		public GRP_SMPP_DC_ISO_8859_8() {
			super("SMPP_DC_ISO_8859_8", "ISO 8859 9 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_ISO_8859_8 _grp_SMPP_DC_ISO_8859_8 = new GRP_SMPP_DC_ISO_8859_8();


	public class CLI_smpp_dc_iso_8859_8_set extends MethodDescriptor {

		public CLI_smpp_dc_iso_8859_8_set() {
			super("SET", "Set operand value ", "SMPP_DC_ISO_8859_8,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_iso_8859_8_set _cli_smpp_dc_iso_8859_8_set = new CLI_smpp_dc_iso_8859_8_set();

	public class GRP_SMPP_DC_UCS2 extends GroupDescriptor {

		public GRP_SMPP_DC_UCS2() {
			super("SMPP_DC_UCS2", "UCS2 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_UCS2 _grp_SMPP_DC_UCS2 = new GRP_SMPP_DC_UCS2();


	public class CLI_smpp_dc_ucs2_set extends MethodDescriptor {

		public CLI_smpp_dc_ucs2_set() {
			super("SET", "Set operand value ", "SMPP_DC_UCS2,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_ucs2_set _cli_smpp_dc_ucs2_set = new CLI_smpp_dc_ucs2_set();

	public class GRP_SMPP_DC_PICTOGRAM extends GroupDescriptor {

		public GRP_SMPP_DC_PICTOGRAM() {
			super("SMPP_DC_PICTOGRAM", "Pictogram ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_PICTOGRAM _grp_SMPP_DC_PICTOGRAM = new GRP_SMPP_DC_PICTOGRAM();


	public class CLI_smpp_dc_pictogram_set extends MethodDescriptor {

		public CLI_smpp_dc_pictogram_set() {
			super("SET", "Set operand value ", "SMPP_DC_PICTOGRAM,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_pictogram_set _cli_smpp_dc_pictogram_set = new CLI_smpp_dc_pictogram_set();

	public class GRP_SMPP_DC_ISO_2011_JP extends GroupDescriptor {

		public GRP_SMPP_DC_ISO_2011_JP() {
			super("SMPP_DC_ISO_2011_JP", "ISO 2011 JP ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_ISO_2011_JP _grp_SMPP_DC_ISO_2011_JP = new GRP_SMPP_DC_ISO_2011_JP();


	public class CLI_smpp_dc_iso_2011_jp_set extends MethodDescriptor {

		public CLI_smpp_dc_iso_2011_jp_set() {
			super("SET", "Set operand value ", "SMPP_DC_ISO_2011_JP,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_iso_2011_jp_set _cli_smpp_dc_iso_2011_jp_set = new CLI_smpp_dc_iso_2011_jp_set();

	public class GRP_SMPP_DC_EXTENDED_KANJI extends GroupDescriptor {

		public GRP_SMPP_DC_EXTENDED_KANJI() {
			super("SMPP_DC_EXTENDED_KANJI", "Extended kanji ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_EXTENDED_KANJI _grp_SMPP_DC_EXTENDED_KANJI = new GRP_SMPP_DC_EXTENDED_KANJI();


	public class CLI_smpp_dc_ext_kanji_set extends MethodDescriptor {

		public CLI_smpp_dc_ext_kanji_set() {
			super("SET", "Set operand value ", "SMPP_DC_EXTENDED_KANJI,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_ext_kanji_set _cli_smpp_dc_ext_kanji_set = new CLI_smpp_dc_ext_kanji_set();

	public class GRP_SMPP_DC_KS_C_5601 extends GroupDescriptor {

		public GRP_SMPP_DC_KS_C_5601() {
			super("SMPP_DC_KS_C_5601", "KS C 5601 ", "DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DC_KS_C_5601 _grp_SMPP_DC_KS_C_5601 = new GRP_SMPP_DC_KS_C_5601();


	public class CLI_smpp_dc_ks_c_5601_set extends MethodDescriptor {

		public CLI_smpp_dc_ks_c_5601_set() {
			super("SET", "Set operand value ", "SMPP_DC_KS_C_5601,DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_ks_c_5601_set _cli_smpp_dc_ks_c_5601_set = new CLI_smpp_dc_ks_c_5601_set();

	public class GRP_SMPP_TON extends GroupDescriptor {

		public GRP_SMPP_TON() {
			super("SMPP_TON", "Type of number ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON _grp_SMPP_TON = new GRP_SMPP_TON();


	public class GRP_SMPP_TON_UNKNOWN extends GroupDescriptor {

		public GRP_SMPP_TON_UNKNOWN() {
			super("SMPP_TON_UNKNOWN", "Unknown ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_UNKNOWN _grp_SMPP_TON_UNKNOWN = new GRP_SMPP_TON_UNKNOWN();


	public class CLI_smpp_ton_unknown_set extends MethodDescriptor {

		public CLI_smpp_ton_unknown_set() {
			super("SET", "Set operand value ", "SMPP_TON_UNKNOWN,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_unknown_set _cli_smpp_ton_unknown_set = new CLI_smpp_ton_unknown_set();

	public class GRP_SMPP_TON_INTERNATIONAL extends GroupDescriptor {

		public GRP_SMPP_TON_INTERNATIONAL() {
			super("SMPP_TON_INTERNATIONAL", "International ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_INTERNATIONAL _grp_SMPP_TON_INTERNATIONAL = new GRP_SMPP_TON_INTERNATIONAL();


	public class CLI_smpp_ton_international_set extends MethodDescriptor {

		public CLI_smpp_ton_international_set() {
			super("SET", "Set operand value ", "SMPP_TON_INTERNATIONAL,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_international_set _cli_smpp_ton_international_set = new CLI_smpp_ton_international_set();

	public class GRP_SMPP_TON_NATIONAL extends GroupDescriptor {

		public GRP_SMPP_TON_NATIONAL() {
			super("SMPP_TON_NATIONAL", "National ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_NATIONAL _grp_SMPP_TON_NATIONAL = new GRP_SMPP_TON_NATIONAL();


	public class CLI_smpp_ton_national_set extends MethodDescriptor {

		public CLI_smpp_ton_national_set() {
			super("SET", "Set operand value ", "SMPP_TON_NATIONAL,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_national_set _cli_smpp_ton_national_set = new CLI_smpp_ton_national_set();

	public class GRP_SMPP_TON_NETWORK_SPECIFIC extends GroupDescriptor {

		public GRP_SMPP_TON_NETWORK_SPECIFIC() {
			super("SMPP_TON_NETWORK_SPECIFIC", "Network specific ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_NETWORK_SPECIFIC _grp_SMPP_TON_NETWORK_SPECIFIC = new GRP_SMPP_TON_NETWORK_SPECIFIC();


	public class CLI_smpp_ton_network_specific_set extends MethodDescriptor {

		public CLI_smpp_ton_network_specific_set() {
			super("SET", "Set operand value ", "SMPP_TON_NETWORK_SPECIFIC,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_network_specific_set _cli_smpp_ton_network_specific_set = new CLI_smpp_ton_network_specific_set();

	public class GRP_SMPP_TON_SUBSCRIBER_NUMBER extends GroupDescriptor {

		public GRP_SMPP_TON_SUBSCRIBER_NUMBER() {
			super("SMPP_TON_SUBSCRIBER_NUMBER", "Subscriber number ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_SUBSCRIBER_NUMBER _grp_SMPP_TON_SUBSCRIBER_NUMBER = new GRP_SMPP_TON_SUBSCRIBER_NUMBER();


	public class CLI_smpp_ton_subscriber_number_set extends MethodDescriptor {

		public CLI_smpp_ton_subscriber_number_set() {
			super("SET", "Set operand value ", "SMPP_TON_SUBSCRIBER_NUMBER,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_subscriber_number_set _cli_smpp_ton_subscriber_number_set = new CLI_smpp_ton_subscriber_number_set();

	public class GRP_SMPP_TON_ALPHANUMERIC extends GroupDescriptor {

		public GRP_SMPP_TON_ALPHANUMERIC() {
			super("SMPP_TON_ALPHANUMERIC", "Alphanumeric ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_ALPHANUMERIC _grp_SMPP_TON_ALPHANUMERIC = new GRP_SMPP_TON_ALPHANUMERIC();


	public class CLI_smpp_ton_alphanumeric_set extends MethodDescriptor {

		public CLI_smpp_ton_alphanumeric_set() {
			super("SET", "Set operand value ", "SMPP_TON_ALPHANUMERIC,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_alphanumeric_set _cli_smpp_ton_alphanumeric_set = new CLI_smpp_ton_alphanumeric_set();

	public class GRP_SMPP_TON_ABBREVIATED extends GroupDescriptor {

		public GRP_SMPP_TON_ABBREVIATED() {
			super("SMPP_TON_ABBREVIATED", "Abbreviated ", "SMPP_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TON_ABBREVIATED _grp_SMPP_TON_ABBREVIATED = new GRP_SMPP_TON_ABBREVIATED();


	public class CLI_smpp_ton_abbreviated_set extends MethodDescriptor {

		public CLI_smpp_ton_abbreviated_set() {
			super("SET", "Set operand value ", "SMPP_TON_ABBREVIATED,SMPP_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ton_abbreviated_set _cli_smpp_ton_abbreviated_set = new CLI_smpp_ton_abbreviated_set();

	public class GRP_SMPP_NP extends GroupDescriptor {

		public GRP_SMPP_NP() {
			super("SMPP_NP", "Numbering plan ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP _grp_SMPP_NP = new GRP_SMPP_NP();


	public class GRP_SMPP_NP_UNKNOWN extends GroupDescriptor {

		public GRP_SMPP_NP_UNKNOWN() {
			super("SMPP_NP_UNKNOWN", "Unknown ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_UNKNOWN _grp_SMPP_NP_UNKNOWN = new GRP_SMPP_NP_UNKNOWN();


	public class CLI_smpp_np_unknown_set extends MethodDescriptor {

		public CLI_smpp_np_unknown_set() {
			super("SET", "Set operand value ", "SMPP_NP_UNKNOWN,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_unknown_set _cli_smpp_np_unknown_set = new CLI_smpp_np_unknown_set();

	public class GRP_SMPP_NP_ISDN_TELEPHONE extends GroupDescriptor {

		public GRP_SMPP_NP_ISDN_TELEPHONE() {
			super("SMPP_NP_ISDN_TELEPHONE", "ISDN Telephone ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_ISDN_TELEPHONE _grp_SMPP_NP_ISDN_TELEPHONE = new GRP_SMPP_NP_ISDN_TELEPHONE();


	public class CLI_smpp_np_isdn_telephone_set extends MethodDescriptor {

		public CLI_smpp_np_isdn_telephone_set() {
			super("SET", "Set operand value ", "SMPP_NP_ISDN_TELEPHONE,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_isdn_telephone_set _cli_smpp_np_isdn_telephone_set = new CLI_smpp_np_isdn_telephone_set();

	public class GRP_SMPP_NP_DATA_X121 extends GroupDescriptor {

		public GRP_SMPP_NP_DATA_X121() {
			super("SMPP_NP_DATA_X121", "DATA X121 ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_DATA_X121 _grp_SMPP_NP_DATA_X121 = new GRP_SMPP_NP_DATA_X121();


	public class CLI_smpp_np_data_x121_set extends MethodDescriptor {

		public CLI_smpp_np_data_x121_set() {
			super("SET", "Set operand value ", "SMPP_NP_DATA_X121,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_data_x121_set _cli_smpp_np_data_x121_set = new CLI_smpp_np_data_x121_set();

	public class GRP_SMPP_NP_TELEX extends GroupDescriptor {

		public GRP_SMPP_NP_TELEX() {
			super("SMPP_NP_TELEX", "Telex ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_TELEX _grp_SMPP_NP_TELEX = new GRP_SMPP_NP_TELEX();


	public class CLI_smpp_np_telex_set extends MethodDescriptor {

		public CLI_smpp_np_telex_set() {
			super("SET", "Set operand value ", "SMPP_NP_TELEX,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_telex_set _cli_smpp_np_telex_set = new CLI_smpp_np_telex_set();

	public class GRP_SMPP_NP_LAND_MOBILE extends GroupDescriptor {

		public GRP_SMPP_NP_LAND_MOBILE() {
			super("SMPP_NP_LAND_MOBILE", "Land mobile ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_LAND_MOBILE _grp_SMPP_NP_LAND_MOBILE = new GRP_SMPP_NP_LAND_MOBILE();


	public class CLI_smpp_np_land_mobile_set extends MethodDescriptor {

		public CLI_smpp_np_land_mobile_set() {
			super("SET", "Set operand value ", "SMPP_NP_LAND_MOBILE,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_land_mobile_set _cli_smpp_np_land_mobile_set = new CLI_smpp_np_land_mobile_set();

	public class GRP_SMPP_NP_NATIONAL extends GroupDescriptor {

		public GRP_SMPP_NP_NATIONAL() {
			super("SMPP_NP_NATIONAL", "National ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_NATIONAL _grp_SMPP_NP_NATIONAL = new GRP_SMPP_NP_NATIONAL();


	public class CLI_smpp_np_national_set extends MethodDescriptor {

		public CLI_smpp_np_national_set() {
			super("SET", "Set operand value ", "SMPP_NP_NATIONAL,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_national_set _cli_smpp_np_national_set = new CLI_smpp_np_national_set();

	public class GRP_SMPP_NP_PRIVATE extends GroupDescriptor {

		public GRP_SMPP_NP_PRIVATE() {
			super("SMPP_NP_PRIVATE", "Private ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_PRIVATE _grp_SMPP_NP_PRIVATE = new GRP_SMPP_NP_PRIVATE();


	public class CLI_smpp_np_private_set extends MethodDescriptor {

		public CLI_smpp_np_private_set() {
			super("SET", "Set operand value ", "SMPP_NP_PRIVATE,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_private_set _cli_smpp_np_private_set = new CLI_smpp_np_private_set();

	public class GRP_SMPP_NP_ERMES extends GroupDescriptor {

		public GRP_SMPP_NP_ERMES() {
			super("SMPP_NP_ERMES", "Ermes ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_ERMES _grp_SMPP_NP_ERMES = new GRP_SMPP_NP_ERMES();


	public class CLI_smpp_np_ermes_set extends MethodDescriptor {

		public CLI_smpp_np_ermes_set() {
			super("SET", "Set operand value ", "SMPP_NP_ERMES,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_ermes_set _cli_smpp_np_ermes_set = new CLI_smpp_np_ermes_set();

	public class GRP_SMPP_NP_INTERNET_IP extends GroupDescriptor {

		public GRP_SMPP_NP_INTERNET_IP() {
			super("SMPP_NP_INTERNET_IP", "Internet IP ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_INTERNET_IP _grp_SMPP_NP_INTERNET_IP = new GRP_SMPP_NP_INTERNET_IP();


	public class CLI_smpp_np_internet_ip_set extends MethodDescriptor {

		public CLI_smpp_np_internet_ip_set() {
			super("SET", "Set operand value ", "SMPP_NP_INTERNET_IP,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_internet_ip_set _cli_smpp_np_internet_ip_set = new CLI_smpp_np_internet_ip_set();

	public class GRP_SMPP_NP_WAP_CLIENT_ID extends GroupDescriptor {

		public GRP_SMPP_NP_WAP_CLIENT_ID() {
			super("SMPP_NP_WAP_CLIENT_ID", "WAP Client ID ", "SMPP_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_NP_WAP_CLIENT_ID _grp_SMPP_NP_WAP_CLIENT_ID = new GRP_SMPP_NP_WAP_CLIENT_ID();


	public class CLI_smpp_np_wap_client_id_set extends MethodDescriptor {

		public CLI_smpp_np_wap_client_id_set() {
			super("SET", "Set operand value ", "SMPP_NP_WAP_CLIENT_ID,SMPP_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_np_wap_client_id_set _cli_smpp_np_wap_client_id_set = new CLI_smpp_np_wap_client_id_set();

	public class GRP_ESM_MM extends GroupDescriptor {

		public GRP_ESM_MM() {
			super("ESM_MM", "ESM Message mode ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_ESM_MM _grp_ESM_MM = new GRP_ESM_MM();


	public class GRP_SMPP_ESM_MM_DEFAULT extends GroupDescriptor {

		public GRP_SMPP_ESM_MM_DEFAULT() {
			super("SMPP_ESM_MM_DEFAULT", "Default ", "ESM_MM,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MM_DEFAULT _grp_SMPP_ESM_MM_DEFAULT = new GRP_SMPP_ESM_MM_DEFAULT();


	public class CLI_smpp_esm_mm_default_set extends MethodDescriptor {

		public CLI_smpp_esm_mm_default_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MM_DEFAULT,ESM_MM,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mm_default_set _cli_smpp_esm_mm_default_set = new CLI_smpp_esm_mm_default_set();

	public class GRP_SMPP_ESM_MM_DATAGRAM extends GroupDescriptor {

		public GRP_SMPP_ESM_MM_DATAGRAM() {
			super("SMPP_ESM_MM_DATAGRAM", "Datagram ", "ESM_MM,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MM_DATAGRAM _grp_SMPP_ESM_MM_DATAGRAM = new GRP_SMPP_ESM_MM_DATAGRAM();


	public class CLI_smpp_esm_mm_datagram_set extends MethodDescriptor {

		public CLI_smpp_esm_mm_datagram_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MM_DATAGRAM,ESM_MM,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mm_datagram_set _cli_smpp_esm_mm_datagram_set = new CLI_smpp_esm_mm_datagram_set();

	public class GRP_SMPP_ESM_MM_FORWARD extends GroupDescriptor {

		public GRP_SMPP_ESM_MM_FORWARD() {
			super("SMPP_ESM_MM_FORWARD", "Forward ", "ESM_MM,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MM_FORWARD _grp_SMPP_ESM_MM_FORWARD = new GRP_SMPP_ESM_MM_FORWARD();


	public class CLI_smpp_esm_mm_forward_set extends MethodDescriptor {

		public CLI_smpp_esm_mm_forward_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MM_FORWARD,ESM_MM,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mm_forward_set _cli_smpp_esm_mm_forward_set = new CLI_smpp_esm_mm_forward_set();

	public class GRP_SMPP_ESM_MM_STORE_FORWARD extends GroupDescriptor {

		public GRP_SMPP_ESM_MM_STORE_FORWARD() {
			super("SMPP_ESM_MM_STORE_FORWARD", "Store and forward ", "ESM_MM,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MM_STORE_FORWARD _grp_SMPP_ESM_MM_STORE_FORWARD = new GRP_SMPP_ESM_MM_STORE_FORWARD();


	public class CLI_smpp_esm_mm_store_forward_set extends MethodDescriptor {

		public CLI_smpp_esm_mm_store_forward_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MM_STORE_FORWARD,ESM_MM,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mm_store_forward_set _cli_smpp_esm_mm_store_forward_set = new CLI_smpp_esm_mm_store_forward_set();

	public class GRP_ESM_MT extends GroupDescriptor {

		public GRP_ESM_MT() {
			super("ESM_MT", "ESM Message type ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_ESM_MT _grp_ESM_MT = new GRP_ESM_MT();


	public class GRP_SMPP_ESM_MT_DEFAULT extends GroupDescriptor {

		public GRP_SMPP_ESM_MT_DEFAULT() {
			super("SMPP_ESM_MT_DEFAULT", "Default ", "ESM_MT,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MT_DEFAULT _grp_SMPP_ESM_MT_DEFAULT = new GRP_SMPP_ESM_MT_DEFAULT();


	public class CLI_smpp_esm_mt_default_set extends MethodDescriptor {

		public CLI_smpp_esm_mt_default_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MT_DEFAULT,ESM_MT,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mt_default_set _cli_smpp_esm_mt_default_set = new CLI_smpp_esm_mt_default_set();

	public class GRP_SMPP_ESM_MT_DELIVERY_ACK extends GroupDescriptor {

		public GRP_SMPP_ESM_MT_DELIVERY_ACK() {
			super("SMPP_ESM_MT_DELIVERY_ACK", "Delivery acknowledgement ", "ESM_MT,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MT_DELIVERY_ACK _grp_SMPP_ESM_MT_DELIVERY_ACK = new GRP_SMPP_ESM_MT_DELIVERY_ACK();


	public class CLI_smpp_esm_mt_ack_set extends MethodDescriptor {

		public CLI_smpp_esm_mt_ack_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MT_DELIVERY_ACK,ESM_MT,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mt_ack_set _cli_smpp_esm_mt_ack_set = new CLI_smpp_esm_mt_ack_set();

	public class GRP_SMPP_ESM_MT_MANUAL_USER_ACK extends GroupDescriptor {

		public GRP_SMPP_ESM_MT_MANUAL_USER_ACK() {
			super("SMPP_ESM_MT_MANUAL_USER_ACK", "Manual or user acknowledgement ", "ESM_MT,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MT_MANUAL_USER_ACK _grp_SMPP_ESM_MT_MANUAL_USER_ACK = new GRP_SMPP_ESM_MT_MANUAL_USER_ACK();


	public class CLI_smpp_esm_mt_manual_set extends MethodDescriptor {

		public CLI_smpp_esm_mt_manual_set() {
			super("SET", "Set operand value ", "SMPP_ESM_MT_MANUAL_USER_ACK,ESM_MT,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mt_manual_set _cli_smpp_esm_mt_manual_set = new CLI_smpp_esm_mt_manual_set();

	public class GRP_EMS_GF extends GroupDescriptor {

		public GRP_EMS_GF() {
			super("EMS_GF", "ESM GSM Network specific features ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_EMS_GF _grp_EMS_GF = new GRP_EMS_GF();


	public class GRP_SMPP_ESM_GF_NO extends GroupDescriptor {

		public GRP_SMPP_ESM_GF_NO() {
			super("SMPP_ESM_GF_NO", "No features ", "EMS_GF,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_GF_NO _grp_SMPP_ESM_GF_NO = new GRP_SMPP_ESM_GF_NO();


	public class CLI_smpp_esm_gf_no_set extends MethodDescriptor {

		public CLI_smpp_esm_gf_no_set() {
			super("SET", "Set operand value ", "SMPP_ESM_GF_NO,EMS_GF,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_gf_no_set _cli_smpp_esm_gf_no_set = new CLI_smpp_esm_gf_no_set();

	public class GRP_SMPP_ESM_GF_UDHI_INDICATOR extends GroupDescriptor {

		public GRP_SMPP_ESM_GF_UDHI_INDICATOR() {
			super("SMPP_ESM_GF_UDHI_INDICATOR", "UDHI Indicator ", "EMS_GF,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_GF_UDHI_INDICATOR _grp_SMPP_ESM_GF_UDHI_INDICATOR = new GRP_SMPP_ESM_GF_UDHI_INDICATOR();


	public class CLI_smpp_esm_gf_udhi_set extends MethodDescriptor {

		public CLI_smpp_esm_gf_udhi_set() {
			super("SET", "Set operand value ", "SMPP_ESM_GF_UDHI_INDICATOR,EMS_GF,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_gf_udhi_set _cli_smpp_esm_gf_udhi_set = new CLI_smpp_esm_gf_udhi_set();

	public class GRP_SMPP_ESM_GF_SET_REPLY_PATH extends GroupDescriptor {

		public GRP_SMPP_ESM_GF_SET_REPLY_PATH() {
			super("SMPP_ESM_GF_SET_REPLY_PATH", "Set reply path ", "EMS_GF,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_GF_SET_REPLY_PATH _grp_SMPP_ESM_GF_SET_REPLY_PATH = new GRP_SMPP_ESM_GF_SET_REPLY_PATH();


	public class CLI_smpp_esm_gf_reply_path_set extends MethodDescriptor {

		public CLI_smpp_esm_gf_reply_path_set() {
			super("SET", "Set operand value ", "SMPP_ESM_GF_SET_REPLY_PATH,EMS_GF,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_gf_reply_path_set _cli_smpp_esm_gf_reply_path_set = new CLI_smpp_esm_gf_reply_path_set();

	public class GRP_SMPP_ESM_GF_SET_BOTH extends GroupDescriptor {

		public GRP_SMPP_ESM_GF_SET_BOTH() {
			super("SMPP_ESM_GF_SET_BOTH", "Both set ", "EMS_GF,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_GF_SET_BOTH _grp_SMPP_ESM_GF_SET_BOTH = new GRP_SMPP_ESM_GF_SET_BOTH();


	public class CLI_smpp_esm_gf_both_set extends MethodDescriptor {

		public CLI_smpp_esm_gf_both_set() {
			super("SET", "Set operand value ", "SMPP_ESM_GF_SET_BOTH,EMS_GF,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_gf_both_set _cli_smpp_esm_gf_both_set = new CLI_smpp_esm_gf_both_set();

	public class GRP_SMPP_IP_SOURCE extends GroupDescriptor {

		public GRP_SMPP_IP_SOURCE() {
			super("SMPP_IP_SOURCE", "IP Layer source address ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_IP_SOURCE _grp_SMPP_IP_SOURCE = new GRP_SMPP_IP_SOURCE();


	public class CLI_smpp_ip_source_set extends MethodDescriptor {

		public CLI_smpp_ip_source_set() {
			super("SET", "Set as rule operand ", "SMPP_IP_SOURCE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ip_source_set _cli_smpp_ip_source_set = new CLI_smpp_ip_source_set();

	public class CLI_smpp_ip_source_regex extends MethodDescriptor {

		public CLI_smpp_ip_source_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_IP_SOURCE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ip_source_regex _cli_smpp_ip_source_regex = new CLI_smpp_ip_source_regex();

	public class CLI_smpp_ip_source_info extends MethodDescriptor {

		public CLI_smpp_ip_source_info() {
			super("INFO", "Operand description ", "SMPP_IP_SOURCE,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ip_source_info _cli_smpp_ip_source_info = new CLI_smpp_ip_source_info();

	public class GRP_SMPP_IP_DESTINATION extends GroupDescriptor {

		public GRP_SMPP_IP_DESTINATION() {
			super("SMPP_IP_DESTINATION", "IP Layer destination address ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_IP_DESTINATION _grp_SMPP_IP_DESTINATION = new GRP_SMPP_IP_DESTINATION();


	public class CLI_smpp_ip_destination_set extends MethodDescriptor {

		public CLI_smpp_ip_destination_set() {
			super("SET", "Set as rule operand ", "SMPP_IP_DESTINATION,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ip_destination_set _cli_smpp_ip_destination_set = new CLI_smpp_ip_destination_set();

	public class CLI_smpp_ip_destination_regex extends MethodDescriptor {

		public CLI_smpp_ip_destination_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_IP_DESTINATION,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ip_destination_regex _cli_smpp_ip_destination_regex = new CLI_smpp_ip_destination_regex();

	public class CLI_smpp_ip_destination_info extends MethodDescriptor {

		public CLI_smpp_ip_destination_info() {
			super("INFO", "Operand description ", "SMPP_IP_DESTINATION,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ip_destination_info _cli_smpp_ip_destination_info = new CLI_smpp_ip_destination_info();

	public class GRP_SMPP_TCP_SOURCE extends GroupDescriptor {

		public GRP_SMPP_TCP_SOURCE() {
			super("SMPP_TCP_SOURCE", "TCP Layer source address ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TCP_SOURCE _grp_SMPP_TCP_SOURCE = new GRP_SMPP_TCP_SOURCE();


	public class CLI_smpp_tcp_source_set extends MethodDescriptor {

		public CLI_smpp_tcp_source_set() {
			super("SET", "Set as rule operand ", "SMPP_TCP_SOURCE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_tcp_source_set _cli_smpp_tcp_source_set = new CLI_smpp_tcp_source_set();

	public class CLI_smpp_tcp_source_regex extends MethodDescriptor {

		public CLI_smpp_tcp_source_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_TCP_SOURCE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_tcp_source_regex _cli_smpp_tcp_source_regex = new CLI_smpp_tcp_source_regex();

	public class CLI_smpp_tcp_source_info extends MethodDescriptor {

		public CLI_smpp_tcp_source_info() {
			super("INFO", "Operand description ", "SMPP_TCP_SOURCE,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_tcp_source_info _cli_smpp_tcp_source_info = new CLI_smpp_tcp_source_info();

	public class GRP_SMPP_TCP_DESTINATION extends GroupDescriptor {

		public GRP_SMPP_TCP_DESTINATION() {
			super("SMPP_TCP_DESTINATION", "TCP Layer destination address ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_TCP_DESTINATION _grp_SMPP_TCP_DESTINATION = new GRP_SMPP_TCP_DESTINATION();


	public class CLI_smpp_tcp_destination_set extends MethodDescriptor {

		public CLI_smpp_tcp_destination_set() {
			super("SET", "Set as rule operand ", "SMPP_TCP_DESTINATION,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_tcp_destination_set _cli_smpp_tcp_destination_set = new CLI_smpp_tcp_destination_set();

	public class CLI_smpp_tcp_destination_regex extends MethodDescriptor {

		public CLI_smpp_tcp_destination_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_TCP_DESTINATION,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_tcp_destination_regex _cli_smpp_tcp_destination_regex = new CLI_smpp_tcp_destination_regex();

	public class CLI_smpp_tcp_destination_info extends MethodDescriptor {

		public CLI_smpp_tcp_destination_info() {
			super("INFO", "Operand description ", "SMPP_TCP_DESTINATION,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_tcp_destination_info _cli_smpp_tcp_destination_info = new CLI_smpp_tcp_destination_info();

	public class GRP_SMPP_SYSTEM_ID extends GroupDescriptor {

		public GRP_SMPP_SYSTEM_ID() {
			super("SMPP_SYSTEM_ID", "EMSE or SMSC Identified ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_SYSTEM_ID _grp_SMPP_SYSTEM_ID = new GRP_SMPP_SYSTEM_ID();


	public class CLI_smpp_system_id_set extends MethodDescriptor {

		public CLI_smpp_system_id_set() {
			super("SET", "Set as rule operand ", "SMPP_SYSTEM_ID,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_system_id_set _cli_smpp_system_id_set = new CLI_smpp_system_id_set();

	public class CLI_smpp_system_id_regex extends MethodDescriptor {

		public CLI_smpp_system_id_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_SYSTEM_ID,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_system_id_regex _cli_smpp_system_id_regex = new CLI_smpp_system_id_regex();

	public class CLI_smpp_system_id_info extends MethodDescriptor {

		public CLI_smpp_system_id_info() {
			super("INFO", "Operand description ", "SMPP_SYSTEM_ID,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_system_id_info _cli_smpp_system_id_info = new CLI_smpp_system_id_info();

	public class GRP_SMPP_PASSWORD extends GroupDescriptor {

		public GRP_SMPP_PASSWORD() {
			super("SMPP_PASSWORD", "Bind password ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_PASSWORD _grp_SMPP_PASSWORD = new GRP_SMPP_PASSWORD();


	public class CLI_smpp_password_set extends MethodDescriptor {

		public CLI_smpp_password_set() {
			super("SET", "Set as rule operand ", "SMPP_PASSWORD,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_password_set _cli_smpp_password_set = new CLI_smpp_password_set();

	public class CLI_smpp_password_regex extends MethodDescriptor {

		public CLI_smpp_password_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_PASSWORD,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_password_regex _cli_smpp_password_regex = new CLI_smpp_password_regex();

	public class CLI_smpp_password_info extends MethodDescriptor {

		public CLI_smpp_password_info() {
			super("INFO", "Operand description ", "SMPP_PASSWORD,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_password_info _cli_smpp_password_info = new CLI_smpp_password_info();

	public class GRP_SMPP_SERVICE_TYPE extends GroupDescriptor {

		public GRP_SMPP_SERVICE_TYPE() {
			super("SMPP_SERVICE_TYPE", "ESME Type ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_SERVICE_TYPE _grp_SMPP_SERVICE_TYPE = new GRP_SMPP_SERVICE_TYPE();


	public class CLI_smpp_service_type_set extends MethodDescriptor {

		public CLI_smpp_service_type_set() {
			super("SET", "Set as rule operand ", "SMPP_SERVICE_TYPE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_service_type_set _cli_smpp_service_type_set = new CLI_smpp_service_type_set();

	public class CLI_smpp_service_type_regex extends MethodDescriptor {

		public CLI_smpp_service_type_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_SERVICE_TYPE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_service_type_regex _cli_smpp_service_type_regex = new CLI_smpp_service_type_regex();

	public class CLI_smpp_service_type_info extends MethodDescriptor {

		public CLI_smpp_service_type_info() {
			super("INFO", "Operand description ", "SMPP_SERVICE_TYPE,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_service_type_info _cli_smpp_service_type_info = new CLI_smpp_service_type_info();

	public class GRP_SMPP_ORIGINATOR_TON extends GroupDescriptor {

		public GRP_SMPP_ORIGINATOR_TON() {
			super("SMPP_ORIGINATOR_TON", "Originator type of number ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ORIGINATOR_TON _grp_SMPP_ORIGINATOR_TON = new GRP_SMPP_ORIGINATOR_TON();


	public class CLI_smpp_originator_ton_set extends MethodDescriptor {

		public CLI_smpp_originator_ton_set() {
			super("SET", "Set as rule operand ", "SMPP_ORIGINATOR_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_originator_ton_set _cli_smpp_originator_ton_set = new CLI_smpp_originator_ton_set();

	public class CLI_smpp_originator_ton_info extends MethodDescriptor {

		public CLI_smpp_originator_ton_info() {
			super("INFO", "Operand description ", "SMPP_ORIGINATOR_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_originator_ton_info _cli_smpp_originator_ton_info = new CLI_smpp_originator_ton_info();

	public class GRP_SMPP_ORIGINATOR_NP extends GroupDescriptor {

		public GRP_SMPP_ORIGINATOR_NP() {
			super("SMPP_ORIGINATOR_NP", "Originator numbering plan ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ORIGINATOR_NP _grp_SMPP_ORIGINATOR_NP = new GRP_SMPP_ORIGINATOR_NP();


	public class CLI_smpp_originator_np_set extends MethodDescriptor {

		public CLI_smpp_originator_np_set() {
			super("SET", "Set as rule operand ", "SMPP_ORIGINATOR_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_originator_np_set _cli_smpp_originator_np_set = new CLI_smpp_originator_np_set();

	public class CLI_smpp_originator_np_info extends MethodDescriptor {

		public CLI_smpp_originator_np_info() {
			super("INFO", "Operand description ", "SMPP_ORIGINATOR_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_originator_np_info _cli_smpp_originator_np_info = new CLI_smpp_originator_np_info();

	public class GRP_SMPP_ORIGINATOR_ADDRESS extends GroupDescriptor {

		public GRP_SMPP_ORIGINATOR_ADDRESS() {
			super("SMPP_ORIGINATOR_ADDRESS", "Originator address ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ORIGINATOR_ADDRESS _grp_SMPP_ORIGINATOR_ADDRESS = new GRP_SMPP_ORIGINATOR_ADDRESS();


	public class CLI_smpp_ogirinator_address_set extends MethodDescriptor {

		public CLI_smpp_ogirinator_address_set() {
			super("SET", "Set as rule operand ", "SMPP_ORIGINATOR_ADDRESS,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_ogirinator_address_set _cli_smpp_ogirinator_address_set = new CLI_smpp_ogirinator_address_set();

	public class CLI_smpp_originator_address_regex extends MethodDescriptor {

		public CLI_smpp_originator_address_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_ORIGINATOR_ADDRESS,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_originator_address_regex _cli_smpp_originator_address_regex = new CLI_smpp_originator_address_regex();

	public class CLI_smpp_originator_address_info extends MethodDescriptor {

		public CLI_smpp_originator_address_info() {
			super("INFO", "Operand description ", "SMPP_ORIGINATOR_ADDRESS,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_originator_address_info _cli_smpp_originator_address_info = new CLI_smpp_originator_address_info();

	public class GRP_SMPP_RECIPIENT_TON extends GroupDescriptor {

		public GRP_SMPP_RECIPIENT_TON() {
			super("SMPP_RECIPIENT_TON", "Recipient type of number ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RECIPIENT_TON _grp_SMPP_RECIPIENT_TON = new GRP_SMPP_RECIPIENT_TON();


	public class CLI_smpp_recipient_ton_set extends MethodDescriptor {

		public CLI_smpp_recipient_ton_set() {
			super("SET", "Set as rule operand ", "SMPP_RECIPIENT_TON,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_ton_set _cli_smpp_recipient_ton_set = new CLI_smpp_recipient_ton_set();

	public class CLI_smpp_recipient_ton_info extends MethodDescriptor {

		public CLI_smpp_recipient_ton_info() {
			super("INFO", "Operand description ", "SMPP_RECIPIENT_TON,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_ton_info _cli_smpp_recipient_ton_info = new CLI_smpp_recipient_ton_info();

	public class GRP_SMPP_RECIPIENT_NP extends GroupDescriptor {

		public GRP_SMPP_RECIPIENT_NP() {
			super("SMPP_RECIPIENT_NP", "Recipient numbering plan ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RECIPIENT_NP _grp_SMPP_RECIPIENT_NP = new GRP_SMPP_RECIPIENT_NP();


	public class CLI_smpp_recipient_np_set extends MethodDescriptor {

		public CLI_smpp_recipient_np_set() {
			super("SET", "Set as rule operand ", "SMPP_RECIPIENT_NP,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_np_set _cli_smpp_recipient_np_set = new CLI_smpp_recipient_np_set();

	public class CLI_smpp_recipient_np_info extends MethodDescriptor {

		public CLI_smpp_recipient_np_info() {
			super("INFO", "Operand description ", "SMPP_RECIPIENT_NP,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_np_info _cli_smpp_recipient_np_info = new CLI_smpp_recipient_np_info();

	public class GRP_SMPP_RECIPIENT_ADDRESS extends GroupDescriptor {

		public GRP_SMPP_RECIPIENT_ADDRESS() {
			super("SMPP_RECIPIENT_ADDRESS", "Recipient address ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RECIPIENT_ADDRESS _grp_SMPP_RECIPIENT_ADDRESS = new GRP_SMPP_RECIPIENT_ADDRESS();


	public class CLI_smpp_recipient_address_set extends MethodDescriptor {

		public CLI_smpp_recipient_address_set() {
			super("SET", "Set as rule operand ", "SMPP_RECIPIENT_ADDRESS,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_address_set _cli_smpp_recipient_address_set = new CLI_smpp_recipient_address_set();

	public class CLI_smpp_recipient_address_regex extends MethodDescriptor {

		public CLI_smpp_recipient_address_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_RECIPIENT_ADDRESS,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_address_regex _cli_smpp_recipient_address_regex = new CLI_smpp_recipient_address_regex();

	public class CLI_smpp_recipient_address_info extends MethodDescriptor {

		public CLI_smpp_recipient_address_info() {
			super("INFO", "Operand description ", "SMPP_RECIPIENT_ADDRESS,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_recipient_address_info _cli_smpp_recipient_address_info = new CLI_smpp_recipient_address_info();

	public class GRP_SMPP_ESM_MESSAGE_MODE extends GroupDescriptor {

		public GRP_SMPP_ESM_MESSAGE_MODE() {
			super("SMPP_ESM_MESSAGE_MODE", "ESM Message mode ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MESSAGE_MODE _grp_SMPP_ESM_MESSAGE_MODE = new GRP_SMPP_ESM_MESSAGE_MODE();


	public class CLI_smpp_esm_mm_set extends MethodDescriptor {

		public CLI_smpp_esm_mm_set() {
			super("SET", "Set as rule operand ", "SMPP_ESM_MESSAGE_MODE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mm_set _cli_smpp_esm_mm_set = new CLI_smpp_esm_mm_set();

	public class CLI_smpp_esm_mm_info extends MethodDescriptor {

		public CLI_smpp_esm_mm_info() {
			super("INFO", "Operand description ", "SMPP_ESM_MESSAGE_MODE,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mm_info _cli_smpp_esm_mm_info = new CLI_smpp_esm_mm_info();

	public class GRP_SMPP_ESM_MESSAGE_TYPE extends GroupDescriptor {

		public GRP_SMPP_ESM_MESSAGE_TYPE() {
			super("SMPP_ESM_MESSAGE_TYPE", "ESM Message type ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_MESSAGE_TYPE _grp_SMPP_ESM_MESSAGE_TYPE = new GRP_SMPP_ESM_MESSAGE_TYPE();


	public class CLI_smpp_esm_mt_set extends MethodDescriptor {

		public CLI_smpp_esm_mt_set() {
			super("SET", "Set as rule operand ", "SMPP_ESM_MESSAGE_TYPE,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mt_set _cli_smpp_esm_mt_set = new CLI_smpp_esm_mt_set();

	public class CLI_smpp_esm_mt_info extends MethodDescriptor {

		public CLI_smpp_esm_mt_info() {
			super("INFO", "Operand description ", "SMPP_ESM_MESSAGE_TYPE,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_mt_info _cli_smpp_esm_mt_info = new CLI_smpp_esm_mt_info();

	public class GRP_SMPP_ESM_GSM_FEATURES extends GroupDescriptor {

		public GRP_SMPP_ESM_GSM_FEATURES() {
			super("SMPP_ESM_GSM_FEATURES", "ESM GSM Features ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_ESM_GSM_FEATURES _grp_SMPP_ESM_GSM_FEATURES = new GRP_SMPP_ESM_GSM_FEATURES();


	public class CLI_smpp_esm_gf_set extends MethodDescriptor {

		public CLI_smpp_esm_gf_set() {
			super("SET", "Set as rule operand ", "SMPP_ESM_GSM_FEATURES,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_gf_set _cli_smpp_esm_gf_set = new CLI_smpp_esm_gf_set();

	public class CLI_smpp_esm_gt_info extends MethodDescriptor {

		public CLI_smpp_esm_gt_info() {
			super("INFO", "Operand description ", "SMPP_ESM_GSM_FEATURES,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_esm_gt_info _cli_smpp_esm_gt_info = new CLI_smpp_esm_gt_info();

	public class GRP_SMPP_PROTOCOL_ID extends GroupDescriptor {

		public GRP_SMPP_PROTOCOL_ID() {
			super("SMPP_PROTOCOL_ID", "Protocol identifier ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_PROTOCOL_ID _grp_SMPP_PROTOCOL_ID = new GRP_SMPP_PROTOCOL_ID();


	public class CLI_smpp_protocol_id_set extends MethodDescriptor {

		public CLI_smpp_protocol_id_set() {
			super("SET", "Set as rule operand ", "SMPP_PROTOCOL_ID,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_protocol_id_set _cli_smpp_protocol_id_set = new CLI_smpp_protocol_id_set();

	public class CLI_smpp_protocol_id_regex extends MethodDescriptor {

		public CLI_smpp_protocol_id_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_PROTOCOL_ID,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_protocol_id_regex _cli_smpp_protocol_id_regex = new CLI_smpp_protocol_id_regex();

	public class CLI_smpp_protocol_id_info extends MethodDescriptor {

		public CLI_smpp_protocol_id_info() {
			super("INFO", "Operand description ", "SMPP_PROTOCOL_ID,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_protocol_id_info _cli_smpp_protocol_id_info = new CLI_smpp_protocol_id_info();

	public class GRP_SMPP_PRIORITY_FLAG extends GroupDescriptor {

		public GRP_SMPP_PRIORITY_FLAG() {
			super("SMPP_PRIORITY_FLAG", "Priority flag ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_PRIORITY_FLAG _grp_SMPP_PRIORITY_FLAG = new GRP_SMPP_PRIORITY_FLAG();


	public class CLI_smpp_priotity_flag_set extends MethodDescriptor {

		public CLI_smpp_priotity_flag_set() {
			super("SET", "Set as rule operand ", "SMPP_PRIORITY_FLAG,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_priotity_flag_set _cli_smpp_priotity_flag_set = new CLI_smpp_priotity_flag_set();

	public class CLI_smpp_priority_flag_regex extends MethodDescriptor {

		public CLI_smpp_priority_flag_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_PRIORITY_FLAG,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_priority_flag_regex _cli_smpp_priority_flag_regex = new CLI_smpp_priority_flag_regex();

	public class CLI_smpp_priority_flag_info extends MethodDescriptor {

		public CLI_smpp_priority_flag_info() {
			super("INFO", "Operand description ", "SMPP_PRIORITY_FLAG,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_priority_flag_info _cli_smpp_priority_flag_info = new CLI_smpp_priority_flag_info();

	public class GRP_SMPP_DELIVERY_TIME extends GroupDescriptor {

		public GRP_SMPP_DELIVERY_TIME() {
			super("SMPP_DELIVERY_TIME", "Delivery time ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DELIVERY_TIME _grp_SMPP_DELIVERY_TIME = new GRP_SMPP_DELIVERY_TIME();


	public class CLI_smpp_delivery_time_set extends MethodDescriptor {

		public CLI_smpp_delivery_time_set() {
			super("SET", "Set as rule operand ", "SMPP_DELIVERY_TIME,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_delivery_time_set _cli_smpp_delivery_time_set = new CLI_smpp_delivery_time_set();

	public class CLI_smpp_delivery_time_regex extends MethodDescriptor {

		public CLI_smpp_delivery_time_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_DELIVERY_TIME,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_delivery_time_regex _cli_smpp_delivery_time_regex = new CLI_smpp_delivery_time_regex();

	public class CLI_smpp_delivery_time_info extends MethodDescriptor {

		public CLI_smpp_delivery_time_info() {
			super("INFO", "Operand description ", "SMPP_DELIVERY_TIME,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_delivery_time_info _cli_smpp_delivery_time_info = new CLI_smpp_delivery_time_info();

	public class GRP_SMPP_VALIDITY_PERIOD extends GroupDescriptor {

		public GRP_SMPP_VALIDITY_PERIOD() {
			super("SMPP_VALIDITY_PERIOD", "Validity period ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_VALIDITY_PERIOD _grp_SMPP_VALIDITY_PERIOD = new GRP_SMPP_VALIDITY_PERIOD();


	public class CLI_smpp_validity_period_set extends MethodDescriptor {

		public CLI_smpp_validity_period_set() {
			super("SET", "Set as rule operand ", "SMPP_VALIDITY_PERIOD,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_validity_period_set _cli_smpp_validity_period_set = new CLI_smpp_validity_period_set();

	public class CLI_smpp_validity_period_regex extends MethodDescriptor {

		public CLI_smpp_validity_period_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_VALIDITY_PERIOD,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_validity_period_regex _cli_smpp_validity_period_regex = new CLI_smpp_validity_period_regex();

	public class CLI_smpp_validity_period_info extends MethodDescriptor {

		public CLI_smpp_validity_period_info() {
			super("INFO", "Operand description ", "SMPP_VALIDITY_PERIOD,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_validity_period_info _cli_smpp_validity_period_info = new CLI_smpp_validity_period_info();

	public class GRP_SMPP_RD_SMSC_RECEIPT extends GroupDescriptor {

		public GRP_SMPP_RD_SMSC_RECEIPT() {
			super("SMPP_RD_SMSC_RECEIPT", "Registered delivery SMSC delivery receipt ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SMSC_RECEIPT _grp_SMPP_RD_SMSC_RECEIPT = new GRP_SMPP_RD_SMSC_RECEIPT();


	public class CLI_smpp_rd_smsc_r_set extends MethodDescriptor {

		public CLI_smpp_rd_smsc_r_set() {
			super("SET", "Set as rule operand ", "SMPP_RD_SMSC_RECEIPT,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smsc_r_set _cli_smpp_rd_smsc_r_set = new CLI_smpp_rd_smsc_r_set();

	public class CLI_smpp_rd_smsc_r_info extends MethodDescriptor {

		public CLI_smpp_rd_smsc_r_info() {
			super("INFO", "Operand description ", "SMPP_RD_SMSC_RECEIPT,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_smsc_r_info _cli_smpp_rd_smsc_r_info = new CLI_smpp_rd_smsc_r_info();

	public class GRP_SMPP_RD_SME_ACK extends GroupDescriptor {

		public GRP_SMPP_RD_SME_ACK() {
			super("SMPP_RD_SME_ACK", "Registered delivery SME originated acknowledgment ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_SME_ACK _grp_SMPP_RD_SME_ACK = new GRP_SMPP_RD_SME_ACK();


	public class CLI_smpp_rd_sme_a_set extends MethodDescriptor {

		public CLI_smpp_rd_sme_a_set() {
			super("SET", "Set as rule operand ", "SMPP_RD_SME_ACK,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_sme_a_set _cli_smpp_rd_sme_a_set = new CLI_smpp_rd_sme_a_set();

	public class CLI_smpp_rd_sme_a_info extends MethodDescriptor {

		public CLI_smpp_rd_sme_a_info() {
			super("INFO", "Operand description ", "SMPP_RD_SME_ACK,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_sme_a_info _cli_smpp_rd_sme_a_info = new CLI_smpp_rd_sme_a_info();

	public class GRP_SMPP_RD_INTERMEDIATE_NOTIFICATION extends GroupDescriptor {

		public GRP_SMPP_RD_INTERMEDIATE_NOTIFICATION() {
			super("SMPP_RD_INTERMEDIATE_NOTIFICATION", "Registered delivery intermediate notification ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_RD_INTERMEDIATE_NOTIFICATION _grp_SMPP_RD_INTERMEDIATE_NOTIFICATION = new GRP_SMPP_RD_INTERMEDIATE_NOTIFICATION();


	public class CLI_smpp_rd_in_set extends MethodDescriptor {

		public CLI_smpp_rd_in_set() {
			super("SET", "Set as rule operand ", "SMPP_RD_INTERMEDIATE_NOTIFICATION,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_in_set _cli_smpp_rd_in_set = new CLI_smpp_rd_in_set();

	public class CLI_smpp_rd_in_info extends MethodDescriptor {

		public CLI_smpp_rd_in_info() {
			super("INFO", "Operand description ", "SMPP_RD_INTERMEDIATE_NOTIFICATION,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_rd_in_info _cli_smpp_rd_in_info = new CLI_smpp_rd_in_info();

	public class GRP_SMPP_REPLACE_IF_PRESENT extends GroupDescriptor {

		public GRP_SMPP_REPLACE_IF_PRESENT() {
			super("SMPP_REPLACE_IF_PRESENT", "Replace if present flag ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_REPLACE_IF_PRESENT _grp_SMPP_REPLACE_IF_PRESENT = new GRP_SMPP_REPLACE_IF_PRESENT();


	public class CLI_smpp_replace_ifp_set extends MethodDescriptor {

		public CLI_smpp_replace_ifp_set() {
			super("SET", "Set as rule operand ", "SMPP_REPLACE_IF_PRESENT,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_replace_ifp_set _cli_smpp_replace_ifp_set = new CLI_smpp_replace_ifp_set();

	public class CLI_smpp_replace_ifp_info extends MethodDescriptor {

		public CLI_smpp_replace_ifp_info() {
			super("INFO", "Operand description ", "SMPP_REPLACE_IF_PRESENT,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_replace_ifp_info _cli_smpp_replace_ifp_info = new CLI_smpp_replace_ifp_info();

	public class GRP_SMPP_DATA_CODING extends GroupDescriptor {

		public GRP_SMPP_DATA_CODING() {
			super("SMPP_DATA_CODING", "Message encoding ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_DATA_CODING _grp_SMPP_DATA_CODING = new GRP_SMPP_DATA_CODING();


	public class CLI_smpp_dc_set extends MethodDescriptor {

		public CLI_smpp_dc_set() {
			super("SET", "Set as rule operand ", "SMPP_DATA_CODING,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_set _cli_smpp_dc_set = new CLI_smpp_dc_set();

	public class CLI_smpp_dc_info extends MethodDescriptor {

		public CLI_smpp_dc_info() {
			super("INFO", "Operand description ", "SMPP_DATA_CODING,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_dc_info _cli_smpp_dc_info = new CLI_smpp_dc_info();

	public class GRP_SMPP_SM_DEFAULT_MSG_ID extends GroupDescriptor {

		public GRP_SMPP_SM_DEFAULT_MSG_ID() {
			super("SMPP_SM_DEFAULT_MSG_ID", "SMSC Predefined message ID ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_SM_DEFAULT_MSG_ID _grp_SMPP_SM_DEFAULT_MSG_ID = new GRP_SMPP_SM_DEFAULT_MSG_ID();


	public class CLI_smpp_sm_def_msg_id_set extends MethodDescriptor {

		public CLI_smpp_sm_def_msg_id_set() {
			super("SET", "Set as rule operand ", "SMPP_SM_DEFAULT_MSG_ID,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_def_msg_id_set _cli_smpp_sm_def_msg_id_set = new CLI_smpp_sm_def_msg_id_set();

	public class CLI_smpp_sm_def_msg_id_info extends MethodDescriptor {

		public CLI_smpp_sm_def_msg_id_info() {
			super("INFO", "Operand description ", "SMPP_SM_DEFAULT_MSG_ID,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_def_msg_id_info _cli_smpp_sm_def_msg_id_info = new CLI_smpp_sm_def_msg_id_info();

	public class GRP_SMPP_SM_LENGTH extends GroupDescriptor {

		public GRP_SMPP_SM_LENGTH() {
			super("SMPP_SM_LENGTH", "Message length ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_SM_LENGTH _grp_SMPP_SM_LENGTH = new GRP_SMPP_SM_LENGTH();


	public class CLI_smpp_sm_length_set extends MethodDescriptor {

		public CLI_smpp_sm_length_set() {
			super("SET", "Set as rule operand ", "SMPP_SM_LENGTH,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_length_set _cli_smpp_sm_length_set = new CLI_smpp_sm_length_set();

	public class CLI_smpp_sm_length_regex extends MethodDescriptor {

		public CLI_smpp_sm_length_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_SM_LENGTH,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_length_regex _cli_smpp_sm_length_regex = new CLI_smpp_sm_length_regex();

	public class CLI_smpp_sm_length_info extends MethodDescriptor {

		public CLI_smpp_sm_length_info() {
			super("INFO", "Operand description ", "SMPP_SM_LENGTH,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_length_info _cli_smpp_sm_length_info = new CLI_smpp_sm_length_info();

	public class GRP_SMPP_SM extends GroupDescriptor {

		public GRP_SMPP_SM() {
			super("SMPP_SM", "Message data ", "SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_SMPP_SM _grp_SMPP_SM = new GRP_SMPP_SM();


	public class CLI_smpp_sm_set extends MethodDescriptor {

		public CLI_smpp_sm_set() {
			super("SET", "Set as rule operand ", "SMPP_SM,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_set _cli_smpp_sm_set = new CLI_smpp_sm_set();

	public class CLI_smpp_sm_regex extends MethodDescriptor {

		public CLI_smpp_sm_regex() {
			super("REGEX", "Set as regex rule operand ", "SMPP_SM,SMPP,OPERAND,RULE", "block_type:rule_id:operand_pos:regex");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_regex _cli_smpp_sm_regex = new CLI_smpp_sm_regex();

	public class CLI_smpp_sm_info extends MethodDescriptor {

		public CLI_smpp_sm_info() {
			super("INFO", "Operand description ", "SMPP_SM,SMPP,OPERAND,RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public CLI_smpp_sm_info _cli_smpp_sm_info = new CLI_smpp_sm_info();

	public class GRP_OPERATOR extends GroupDescriptor {

		public GRP_OPERATOR() {
			super("OPERATOR", "Operator management ", "RULE", "");
		}

		public String execute(Object[] params) {
			// TODO
			return null;
		}

	}
	public GRP_OPERATOR _grp_OPERATOR = new GRP_OPERATOR();


	public class CLI_operator_set extends MethodDescriptor {

		public CLI_operator_set() {
			super("SET", "Set rule operator ", "OPERATOR,RULE", "block_type:rule_id:operator");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				CLIConnection conn = (CLIConnection)params[3];
				
				if(pd1.value != null && pd2.value != null && pd3.value != null){
					FilterType ft = null;
					if(pd1.value.equalsIgnoreCase("MO")) ft = FilterType.MO;
					else if(pd1.value.equalsIgnoreCase("MT")) ft = FilterType.MT;
					else if(pd1.value.equalsIgnoreCase("HLT")) ft = FilterType.HLR;

					if(ft != null){
						FilterDescriptor fd = null;
						for(int i = 0; i<conn.current_filter.size(); i++){
							if(conn.current_filter.get(i).filterType == ft){
								fd = conn.current_filter.get(i);
								break;
							}
						}
						if(fd != null){
							
							try{
								Integer.parseInt(pd2.value);
							}catch(Exception e){
								return "[RED]Invalid rule ID = [[GREEN]" + pd2.value + "[RED]]!";
							}
							
							if(Integer.parseInt(pd2.value) < fd.rule_mod_lst.size()){
								if(fd.rule_mod_lst.get(Integer.parseInt(pd2.value)).type == FilterDescriptorType.RULE){
									RuleDescriptor rd = (RuleDescriptor)fd.rule_mod_lst.get(Integer.parseInt(pd2.value));
									if(		pd3.value.equalsIgnoreCase("==") || 
											pd3.value.equalsIgnoreCase("!=") || 
											pd3.value.equalsIgnoreCase("<") ||
											pd3.value.equalsIgnoreCase(">") || 
											pd3.value.equalsIgnoreCase("<=") || 
											pd3.value.equalsIgnoreCase(">=") ||
											pd3.value.equalsIgnoreCase("IN") ||
											pd3.value.equalsIgnoreCase("NIN")){
										rd.evalOperator = pd3.value;
										return "[WHITE]Adding operator [[GREEN]" + pd3.value + "[WHITE]] to rule ID = [[GREEN]" + pd2.value + "[WHITE]], block = [[GREEN]" +
												pd1.value + "[WHITE]]!";
										
									}else return "[RED]Unknown operator [[GREEN]" + pd3.value + "[RED]]!"; 
									
									
								}else return "[RED]Unknown rule with ID = [[GREEN]" + pd2.value + "[RED]]!";
							}else return "[RED]Unknown rule with ID = [[GREEN]" + pd2.value + "[RED]]!";
							
							
							
						}else return "[RED]Block type [[GREEN]" + pd1.value + "[RED]] does not exist!";
						
						
					}else return "[RED]Unknown block type [[GREEN]" + pd1.value + "[RED]]!";
					
					
				}else return "[RED]Incomplete syntax!";
			}else return "[RED]Incomplete syntax!";
		}
	}
	public CLI_operator_set _cli_operator_set = new CLI_operator_set();


    
	public CLIManager(){
		super();
		// methods
		method_lst.add(_cli_filter_show);
		method_lst.add(_cli_block_add);
		method_lst.add(_cli_block_remove);
		method_lst.add(_cli_rule_set);
		method_lst.add(_cli_rule_add);
		method_lst.add(_cli_rule_remove);
		method_lst.add(_cli_action_set_continue);
		method_lst.add(_cli_action_set_continue_q);
		method_lst.add(_cli_action_set_allow);
		method_lst.add(_cli_action_set_allow_q);
		method_lst.add(_cli_action_set_allow_u);
		method_lst.add(_cli_action_set_allow_u_q);
		method_lst.add(_cli_action_set_deny);
		method_lst.add(_cli_action_set_deny_q);
		method_lst.add(_cli_action_set_goto);
		method_lst.add(_cli_general_operand_set);
		method_lst.add(_cli_general_operand_regex);
		method_lst.add(_cli_general_operand_info);
		method_lst.add(_cli_sms_tpdu_ton_unknown_set);
		method_lst.add(_cli_sms_tpdu_ton_international_set);
		method_lst.add(_cli_sms_tpdu_ton_national_set);
		method_lst.add(_cli_sms_tpdu_ton_network_specific_set);
		method_lst.add(_cli_sms_tpdu_ton_subscriber_number_set);
		method_lst.add(_cli_sms_tpdu_ton_alphanumeric_set);
		method_lst.add(_cli_sms_tpdu_ton_abbreviated_set);
		method_lst.add(_cli_sms_tpdu_dcs_default_set);
		method_lst.add(_cli_sms_tpdu_dcs_8bit_set);
		method_lst.add(_cli_sms_tpdu_dcs_16bit_set);
		method_lst.add(_cli_sms_tpdu_msg_type_single_set);
		method_lst.add(_cli_sms_tpdu_msg_type_concatenated_set);
		method_lst.add(_cli_sms_tpdu_originating_set);
		method_lst.add(_cli_sms_tpdu_originating_regex);
		method_lst.add(_cli_sms_tpdu_originating_info);
		method_lst.add(_cli_sms_tpdu_originating_enc_set);
		method_lst.add(_cli_sms_tpdu_originating_enc_info);
		method_lst.add(_cli_sms_tpdu_destination_set);
		method_lst.add(_cli_sms_tpdu_destination_regex);
		method_lst.add(_cli_sms_tpdu_destination_info);
		method_lst.add(_cli_sms_tpdu_destination_enc_set);
		method_lst.add(_cli_sms_tpdu_destination_enc_info);
		method_lst.add(_cli_sms_tpdu_ud_set);
		method_lst.add(_cli_sms_tpdu_ud_regex);
		method_lst.add(_cli_sms_tpdu_ud_info);
		method_lst.add(_cli_sms_tpdu_dict_set);
		method_lst.add(_cli_sms_tpdu_dict_info);
		method_lst.add(_cli_sms_tpdu_spam_check_set);
		method_lst.add(_cli_sms_tpdu_spam_check_info);
		method_lst.add(_cli_sms_tpdu_rp_check_set);
		method_lst.add(_cli_sms_tpdu_rp_check_info);
		method_lst.add(_cli_sms_tpdu_q_check_set);
		method_lst.add(_cli_sms_tpdu_q_check_info);
		method_lst.add(_cli_sms_tpdu_md5_check_set);
		method_lst.add(_cli_sms_tpdu_md5_check_info);
		method_lst.add(_cli_sms_tpdu_dcs_set);
		method_lst.add(_cli_sms_tpdu_dcs_info);
		method_lst.add(_cli_sms_tpdu_msg_type_set);
		method_lst.add(_cli_sms_tpdu_msg_type_info);
		method_lst.add(_cli_m3ua_dpc_set);
		method_lst.add(_cli_m3ua_dpc_info);
		method_lst.add(_cli_m3ua_opc_set);
		method_lst.add(_cli_m3ua_opc_info);
		method_lst.add(_cli_map_list_scoa_wl_set);
		method_lst.add(_cli_map_list_scda_wl_set);
		method_lst.add(_cli_map_scoa_set);
		method_lst.add(_cli_map_scoa_regex);
		method_lst.add(_cli_map_scoa_info);
		method_lst.add(_cli_map_scda_set);
		method_lst.add(_cli_map_scda_regex);
		method_lst.add(_cli_map_scda_info);
		method_lst.add(_cli_map_imsi_set);
		method_lst.add(_cli_map_imsi_regex);
		method_lst.add(_cli_map_imsi_info);
		method_lst.add(_cli_map_msisdn_set);
		method_lst.add(_cli_map_msisdn_regex);
		method_lst.add(_cli_map_msisdn_info);
		method_lst.add(_cli_hlr_imsi_set);
		method_lst.add(_cli_hlr_imsi_regex);
		method_lst.add(_cli_hlr_imsi_info);
		method_lst.add(_cli_hlr_msisdn_set);
		method_lst.add(_cli_hlr_msisdn_regex);
		method_lst.add(_cli_hlr_msisdn_info);
		method_lst.add(_cli_hlr_nnn_set);
		method_lst.add(_cli_hlr_nnn_regex);
		method_lst.add(_cli_hlr_nnn_info);
		method_lst.add(_cli_hlr_annn_set);
		method_lst.add(_cli_hlr_annn_regex);
		method_lst.add(_cli_hlr_annn_info);
		method_lst.add(_cli_hlr_sca_set);
		method_lst.add(_cli_hlr_sca_regex);
		method_lst.add(_cli_hlr_sca_info);
		method_lst.add(_cli_hlr_result_imsi_set);
		method_lst.add(_cli_hlr_result_imsi_regex);
		method_lst.add(_cli_hlr_result_imsi_info);
		method_lst.add(_cli_hlr_result_nnn_set);
		method_lst.add(_cli_hlr_result_nnn_regex);
		method_lst.add(_cli_hlr_result_nnn_info);
		method_lst.add(_cli_hlr_result_annn_set);
		method_lst.add(_cli_hlr_result_annn_regex);
		method_lst.add(_cli_hlr_result_annn_info);
		method_lst.add(_cli_sccp_list_gt_called_wl_set);
		method_lst.add(_cli_sccp_list_gt_calling_wl_set);
		method_lst.add(_cli_sccp_np_unknown_set);
		method_lst.add(_cli_sccp_np_isdn_telephone_set);
		method_lst.add(_cli_sccp_np_generic_set);
		method_lst.add(_cli_sccp_np_data_x121_set);
		method_lst.add(_cli_sccp_np_telex_set);
		method_lst.add(_cli_sccp_np_maritime_set);
		method_lst.add(_cli_sccp_np_land_mobile_set);
		method_lst.add(_cli_sccp_np_isdn_mobile_set);
		method_lst.add(_cli_sccp_np_private_set);
		method_lst.add(_cli_sccp_nai_unknown_set);
		method_lst.add(_cli_sccp_nai_subscriber_number_set);
		method_lst.add(_cli_sccp_nai_national_use_set);
		method_lst.add(_cli_sccp_nai_national_significant_set);
		method_lst.add(_cli_sccp_nai_international_set);
		method_lst.add(_cli_sccp_gti_none_set);
		method_lst.add(_cli_sccp_gti_nai_set);
		method_lst.add(_cli_sccp_gti_tt_set);
		method_lst.add(_cli_sccp_gti_ttnpe_set);
		method_lst.add(_cli_sccp_gti_ttnpenoa_set);
		method_lst.add(_cli_sccp_gt_called_address_set);
		method_lst.add(_cli_sccp_gt_called_address_regex);
		method_lst.add(_cli_sccp_gt_called_address_info);
		method_lst.add(_cli_sccp_gt_calling_address_set);
		method_lst.add(_cli_sccp_gt_calling_address_regex);
		method_lst.add(_cli_sccp_gt_calling_address_info);
		method_lst.add(_cli_sccp_gt_called_tt_set);
		method_lst.add(_cli_sccp_gt_called_tt_regex);
		method_lst.add(_cli_sccp_gt_called_tt_info);
		method_lst.add(_cli_sccp_gt_calling_tt_set);
		method_lst.add(_cli_sccp_gt_calling_tt_regex);
		method_lst.add(_cli_sccp_gt_calling_tt_info);
		method_lst.add(_cli_sccp_gt_called_nai_set);
		method_lst.add(_cli_sccp_gt_called_nai_info);
		method_lst.add(_cli_sccp_gt_calling_nai_set);
		method_lst.add(_cli_sccp_gt_calling_nai_info);
		method_lst.add(_cli_sccp_gt_called_np_set);
		method_lst.add(_cli_sccp_gt_called_np_info);
		method_lst.add(_cli_sccp_gt_calling_np_set);
		method_lst.add(_cli_sccp_gt_calling_np_info);
		method_lst.add(_cli_sccp_gt_called_gti_set);
		method_lst.add(_cli_sccp_gt_called_gti_info);
		method_lst.add(_cli_sccp_gt_calling_gti_set);
		method_lst.add(_cli_sccp_gt_calling_gti_info);
		method_lst.add(_cli_smpp_rd_smscdr_no_set);
		method_lst.add(_cli_smpp_rd_smscdr_sf_set);
		method_lst.add(_cli_smpp_rd_smscdr_f_set);
		method_lst.add(_cli_smpp_rd_smeoa_no_set);
		method_lst.add(_cli_smpp_rd_smeoa_ack_set);
		method_lst.add(_cli_smpp_rd_smeoa_manual_user_set);
		method_lst.add(_cli_smpp_rd_smeoa_both_set);
		method_lst.add(_cli_smpp_rd_in_no_set);
		method_lst.add(_cli_smpp_rd_in_yes_set);
		method_lst.add(_cli_smpp_dc_default_set);
		method_lst.add(_cli_smpp_dc_ia5_ascii_set);
		method_lst.add(_cli_smpp_dc_8bit_1_set);
		method_lst.add(_cli_smpp_dc_iso_8859_1_set);
		method_lst.add(_cli_smpp_dc_8bit_2_set);
		method_lst.add(_cli_smpp_dc_jis_set);
		method_lst.add(_cli_smpp_dc_iso_8859_5_set);
		method_lst.add(_cli_smpp_dc_iso_8859_8_set);
		method_lst.add(_cli_smpp_dc_ucs2_set);
		method_lst.add(_cli_smpp_dc_pictogram_set);
		method_lst.add(_cli_smpp_dc_iso_2011_jp_set);
		method_lst.add(_cli_smpp_dc_ext_kanji_set);
		method_lst.add(_cli_smpp_dc_ks_c_5601_set);
		method_lst.add(_cli_smpp_ton_unknown_set);
		method_lst.add(_cli_smpp_ton_international_set);
		method_lst.add(_cli_smpp_ton_national_set);
		method_lst.add(_cli_smpp_ton_network_specific_set);
		method_lst.add(_cli_smpp_ton_subscriber_number_set);
		method_lst.add(_cli_smpp_ton_alphanumeric_set);
		method_lst.add(_cli_smpp_ton_abbreviated_set);
		method_lst.add(_cli_smpp_np_unknown_set);
		method_lst.add(_cli_smpp_np_isdn_telephone_set);
		method_lst.add(_cli_smpp_np_data_x121_set);
		method_lst.add(_cli_smpp_np_telex_set);
		method_lst.add(_cli_smpp_np_land_mobile_set);
		method_lst.add(_cli_smpp_np_national_set);
		method_lst.add(_cli_smpp_np_private_set);
		method_lst.add(_cli_smpp_np_ermes_set);
		method_lst.add(_cli_smpp_np_internet_ip_set);
		method_lst.add(_cli_smpp_np_wap_client_id_set);
		method_lst.add(_cli_smpp_esm_mm_default_set);
		method_lst.add(_cli_smpp_esm_mm_datagram_set);
		method_lst.add(_cli_smpp_esm_mm_forward_set);
		method_lst.add(_cli_smpp_esm_mm_store_forward_set);
		method_lst.add(_cli_smpp_esm_mt_default_set);
		method_lst.add(_cli_smpp_esm_mt_ack_set);
		method_lst.add(_cli_smpp_esm_mt_manual_set);
		method_lst.add(_cli_smpp_esm_gf_no_set);
		method_lst.add(_cli_smpp_esm_gf_udhi_set);
		method_lst.add(_cli_smpp_esm_gf_reply_path_set);
		method_lst.add(_cli_smpp_esm_gf_both_set);
		method_lst.add(_cli_smpp_ip_source_set);
		method_lst.add(_cli_smpp_ip_source_regex);
		method_lst.add(_cli_smpp_ip_source_info);
		method_lst.add(_cli_smpp_ip_destination_set);
		method_lst.add(_cli_smpp_ip_destination_regex);
		method_lst.add(_cli_smpp_ip_destination_info);
		method_lst.add(_cli_smpp_tcp_source_set);
		method_lst.add(_cli_smpp_tcp_source_regex);
		method_lst.add(_cli_smpp_tcp_source_info);
		method_lst.add(_cli_smpp_tcp_destination_set);
		method_lst.add(_cli_smpp_tcp_destination_regex);
		method_lst.add(_cli_smpp_tcp_destination_info);
		method_lst.add(_cli_smpp_system_id_set);
		method_lst.add(_cli_smpp_system_id_regex);
		method_lst.add(_cli_smpp_system_id_info);
		method_lst.add(_cli_smpp_password_set);
		method_lst.add(_cli_smpp_password_regex);
		method_lst.add(_cli_smpp_password_info);
		method_lst.add(_cli_smpp_service_type_set);
		method_lst.add(_cli_smpp_service_type_regex);
		method_lst.add(_cli_smpp_service_type_info);
		method_lst.add(_cli_smpp_originator_ton_set);
		method_lst.add(_cli_smpp_originator_ton_info);
		method_lst.add(_cli_smpp_originator_np_set);
		method_lst.add(_cli_smpp_originator_np_info);
		method_lst.add(_cli_smpp_ogirinator_address_set);
		method_lst.add(_cli_smpp_originator_address_regex);
		method_lst.add(_cli_smpp_originator_address_info);
		method_lst.add(_cli_smpp_recipient_ton_set);
		method_lst.add(_cli_smpp_recipient_ton_info);
		method_lst.add(_cli_smpp_recipient_np_set);
		method_lst.add(_cli_smpp_recipient_np_info);
		method_lst.add(_cli_smpp_recipient_address_set);
		method_lst.add(_cli_smpp_recipient_address_regex);
		method_lst.add(_cli_smpp_recipient_address_info);
		method_lst.add(_cli_smpp_esm_mm_set);
		method_lst.add(_cli_smpp_esm_mm_info);
		method_lst.add(_cli_smpp_esm_mt_set);
		method_lst.add(_cli_smpp_esm_mt_info);
		method_lst.add(_cli_smpp_esm_gf_set);
		method_lst.add(_cli_smpp_esm_gt_info);
		method_lst.add(_cli_smpp_protocol_id_set);
		method_lst.add(_cli_smpp_protocol_id_regex);
		method_lst.add(_cli_smpp_protocol_id_info);
		method_lst.add(_cli_smpp_priotity_flag_set);
		method_lst.add(_cli_smpp_priority_flag_regex);
		method_lst.add(_cli_smpp_priority_flag_info);
		method_lst.add(_cli_smpp_delivery_time_set);
		method_lst.add(_cli_smpp_delivery_time_regex);
		method_lst.add(_cli_smpp_delivery_time_info);
		method_lst.add(_cli_smpp_validity_period_set);
		method_lst.add(_cli_smpp_validity_period_regex);
		method_lst.add(_cli_smpp_validity_period_info);
		method_lst.add(_cli_smpp_rd_smsc_r_set);
		method_lst.add(_cli_smpp_rd_smsc_r_info);
		method_lst.add(_cli_smpp_rd_sme_a_set);
		method_lst.add(_cli_smpp_rd_sme_a_info);
		method_lst.add(_cli_smpp_rd_in_set);
		method_lst.add(_cli_smpp_rd_in_info);
		method_lst.add(_cli_smpp_replace_ifp_set);
		method_lst.add(_cli_smpp_replace_ifp_info);
		method_lst.add(_cli_smpp_dc_set);
		method_lst.add(_cli_smpp_dc_info);
		method_lst.add(_cli_smpp_sm_def_msg_id_set);
		method_lst.add(_cli_smpp_sm_def_msg_id_info);
		method_lst.add(_cli_smpp_sm_length_set);
		method_lst.add(_cli_smpp_sm_length_regex);
		method_lst.add(_cli_smpp_sm_length_info);
		method_lst.add(_cli_smpp_sm_set);
		method_lst.add(_cli_smpp_sm_regex);
		method_lst.add(_cli_smpp_sm_info);
		method_lst.add(_cli_operator_set);

		// groups
		group_lst.add(_grp_FILTER);
		group_lst.add(_grp_BLOCK);
		group_lst.add(_grp_SET);
		group_lst.add(_grp_ACTION);
		group_lst.add(_grp_GENERAL);
		group_lst.add(_grp_TON_UNKNOWN);
		group_lst.add(_grp_TON_INTERNATIONAL);
		group_lst.add(_grp_TON_NATIONAL);
		group_lst.add(_grp_TON_NETWORK_SPECIFIC);
		group_lst.add(_grp_TON_SUBSCRIBER_NUMBER);
		group_lst.add(_grp_TON_ALPHANUMERIC);
		group_lst.add(_grp_TON_ABBREVIATED);
		group_lst.add(_grp_TON);
		group_lst.add(_grp_DCS_DEFAULT);
		group_lst.add(_grp_DCS_8BIT);
		group_lst.add(_grp_DCS_UCS2);
		group_lst.add(_grp_DCS);
		group_lst.add(_grp_SINGLE);
		group_lst.add(_grp_CONCATENATED);
		group_lst.add(_grp_MSG_TYPE);
		group_lst.add(_grp_SMS_TPDU_ORIGINATING);
		group_lst.add(_grp_SMS_TPDU_ORIGINATING_ENC);
		group_lst.add(_grp_SMS_TPDU_DESTINATION);
		group_lst.add(_grp_SMS_TPDU_DESTINATION_ENC);
		group_lst.add(_grp_SMS_TPDU_UD);
		group_lst.add(_grp_DICT_SMS_TPDU_UD);
		group_lst.add(_grp_SPAM_SMS_TPDU_UD);
		group_lst.add(_grp_RP_SMS_TPDU_UD);
		group_lst.add(_grp_QUARANTINE_SMS_TPDU_UD);
		group_lst.add(_grp_MD5_SMS_TPDU_UD);
		group_lst.add(_grp_SMS_TPDU_DCS);
		group_lst.add(_grp_SMS_MSG_TYPE);
		group_lst.add(_grp_SMS_TPDU);
		group_lst.add(_grp_M3UA_DPC);
		group_lst.add(_grp_M3UA_OPC);
		group_lst.add(_grp_M3UA);
		group_lst.add(_grp_MAP_SCOA_WL);
		group_lst.add(_grp_MAP_SCDA_WL);
		group_lst.add(_grp_MAP_LIST);
		group_lst.add(_grp_MAP_SCOA);
		group_lst.add(_grp_MAP_SCDA);
		group_lst.add(_grp_MAP_IMSI);
		group_lst.add(_grp_MAP_MSISDN);
		group_lst.add(_grp_HLR_IMSI);
		group_lst.add(_grp_HLR_MSISDN);
		group_lst.add(_grp_HLR_NNN);
		group_lst.add(_grp_HLR_ANNN);
		group_lst.add(_grp_HLR_SCA);
		group_lst.add(_grp_MAP);
		group_lst.add(_grp_HLR_RESULT_IMSI);
		group_lst.add(_grp_HLR_RESULT_NNN);
		group_lst.add(_grp_HLR_RESULT_ANNN);
		group_lst.add(_grp_HLR_RESULT);
		group_lst.add(_grp_SCCP_GT_CALLED_WL);
		group_lst.add(_grp_SCCP_GT_CALLING_WL);
		group_lst.add(_grp_SCCP_LIST);
		group_lst.add(_grp_NP_UNKNOWN);
		group_lst.add(_grp_NP_ISDN_TELEPHONE);
		group_lst.add(_grp_NP_GENERIC);
		group_lst.add(_grp_NP_DATA_X121);
		group_lst.add(_grp_NP_TELEX);
		group_lst.add(_grp_NP_MARITIME);
		group_lst.add(_grp_NP_LAND_MOBILE);
		group_lst.add(_grp_NP_ISDN_MOBILE);
		group_lst.add(_grp_NP_PRIVATE);
		group_lst.add(_grp_NP);
		group_lst.add(_grp_NAI_UNKNOWN);
		group_lst.add(_grp_NAI_SUBSCRIBER_NUMBER);
		group_lst.add(_grp_NAI_RESERVED_FOR_NATIONAL_USE);
		group_lst.add(_grp_NAI_NATIONAL_SIGNIFICANT_NUMBER);
		group_lst.add(_grp_NAI_INTERNATIONAL);
		group_lst.add(_grp_NAI);
		group_lst.add(_grp_GTI_NONE);
		group_lst.add(_grp_GTI_NAI);
		group_lst.add(_grp_GTI_TT);
		group_lst.add(_grp_GTI_TTNPE);
		group_lst.add(_grp_GTI_TTNPENOA);
		group_lst.add(_grp_GTI);
		group_lst.add(_grp_SCCP_GT_CALLED_ADDRESS);
		group_lst.add(_grp_SCCP_GT_CALLING_ADDRESS);
		group_lst.add(_grp_SCCP_GT_CALLED_TT);
		group_lst.add(_grp_SCCP_GT_CALLING_TT);
		group_lst.add(_grp_SCCP_GT_CALLED_NAI);
		group_lst.add(_grp_SCCP_GT_CALLING_NAI);
		group_lst.add(_grp_SCCP_GT_CALLED_NP);
		group_lst.add(_grp_SCCP_GT_CALLING_NP);
		group_lst.add(_grp_SCCP_GT_CALLED_GTI);
		group_lst.add(_grp_SCCP_GT_CALLING_GTI);
		group_lst.add(_grp_SCCP);
		group_lst.add(_grp_SMPP_RD_SMSCDR_NO);
		group_lst.add(_grp_SMPP_RD_SMSCDR_SUCCESS_FAILURE);
		group_lst.add(_grp_SMPP_RD_SMSCDR_FAILURE);
		group_lst.add(_grp_RD_SMSCDR);
		group_lst.add(_grp_SMPP_RD_SMEOA_NO);
		group_lst.add(_grp_SMPP_RD_SMEOA_ACK);
		group_lst.add(_grp_SMPP_RD_SMEOA_MANUAL_USER_ACK);
		group_lst.add(_grp_SMPP_RD_SMEOA_BOTH);
		group_lst.add(_grp_RD_SMEOA);
		group_lst.add(_grp_NO);
		group_lst.add(_grp_YES);
		group_lst.add(_grp_RD_IN);
		group_lst.add(_grp_SMPP_DC_DEFAULT);
		group_lst.add(_grp_SMPP_DC_IA5_ASCII);
		group_lst.add(_grp_SMPP_DC_8BIT_BINARY_1);
		group_lst.add(_grp_SMPP_DC_ISO_8859_1);
		group_lst.add(_grp_SMPP_DC_8BIT_BINARY_2);
		group_lst.add(_grp_SMPP_DC_JIS);
		group_lst.add(_grp_SMPP_DC_ISO_8859_5);
		group_lst.add(_grp_SMPP_DC_ISO_8859_8);
		group_lst.add(_grp_SMPP_DC_UCS2);
		group_lst.add(_grp_SMPP_DC_PICTOGRAM);
		group_lst.add(_grp_SMPP_DC_ISO_2011_JP);
		group_lst.add(_grp_SMPP_DC_EXTENDED_KANJI);
		group_lst.add(_grp_SMPP_DC_KS_C_5601);
		group_lst.add(_grp_DATA_CODING);
		group_lst.add(_grp_SMPP_TON_UNKNOWN);
		group_lst.add(_grp_SMPP_TON_INTERNATIONAL);
		group_lst.add(_grp_SMPP_TON_NATIONAL);
		group_lst.add(_grp_SMPP_TON_NETWORK_SPECIFIC);
		group_lst.add(_grp_SMPP_TON_SUBSCRIBER_NUMBER);
		group_lst.add(_grp_SMPP_TON_ALPHANUMERIC);
		group_lst.add(_grp_SMPP_TON_ABBREVIATED);
		group_lst.add(_grp_SMPP_TON);
		group_lst.add(_grp_SMPP_NP_UNKNOWN);
		group_lst.add(_grp_SMPP_NP_ISDN_TELEPHONE);
		group_lst.add(_grp_SMPP_NP_DATA_X121);
		group_lst.add(_grp_SMPP_NP_TELEX);
		group_lst.add(_grp_SMPP_NP_LAND_MOBILE);
		group_lst.add(_grp_SMPP_NP_NATIONAL);
		group_lst.add(_grp_SMPP_NP_PRIVATE);
		group_lst.add(_grp_SMPP_NP_ERMES);
		group_lst.add(_grp_SMPP_NP_INTERNET_IP);
		group_lst.add(_grp_SMPP_NP_WAP_CLIENT_ID);
		group_lst.add(_grp_SMPP_NP);
		group_lst.add(_grp_SMPP_ESM_MM_DEFAULT);
		group_lst.add(_grp_SMPP_ESM_MM_DATAGRAM);
		group_lst.add(_grp_SMPP_ESM_MM_FORWARD);
		group_lst.add(_grp_SMPP_ESM_MM_STORE_FORWARD);
		group_lst.add(_grp_ESM_MM);
		group_lst.add(_grp_SMPP_ESM_MT_DEFAULT);
		group_lst.add(_grp_SMPP_ESM_MT_DELIVERY_ACK);
		group_lst.add(_grp_SMPP_ESM_MT_MANUAL_USER_ACK);
		group_lst.add(_grp_ESM_MT);
		group_lst.add(_grp_SMPP_ESM_GF_NO);
		group_lst.add(_grp_SMPP_ESM_GF_UDHI_INDICATOR);
		group_lst.add(_grp_SMPP_ESM_GF_SET_REPLY_PATH);
		group_lst.add(_grp_SMPP_ESM_GF_SET_BOTH);
		group_lst.add(_grp_EMS_GF);
		group_lst.add(_grp_SMPP_IP_SOURCE);
		group_lst.add(_grp_SMPP_IP_DESTINATION);
		group_lst.add(_grp_SMPP_TCP_SOURCE);
		group_lst.add(_grp_SMPP_TCP_DESTINATION);
		group_lst.add(_grp_SMPP_SYSTEM_ID);
		group_lst.add(_grp_SMPP_PASSWORD);
		group_lst.add(_grp_SMPP_SERVICE_TYPE);
		group_lst.add(_grp_SMPP_ORIGINATOR_TON);
		group_lst.add(_grp_SMPP_ORIGINATOR_NP);
		group_lst.add(_grp_SMPP_ORIGINATOR_ADDRESS);
		group_lst.add(_grp_SMPP_RECIPIENT_TON);
		group_lst.add(_grp_SMPP_RECIPIENT_NP);
		group_lst.add(_grp_SMPP_RECIPIENT_ADDRESS);
		group_lst.add(_grp_SMPP_ESM_MESSAGE_MODE);
		group_lst.add(_grp_SMPP_ESM_MESSAGE_TYPE);
		group_lst.add(_grp_SMPP_ESM_GSM_FEATURES);
		group_lst.add(_grp_SMPP_PROTOCOL_ID);
		group_lst.add(_grp_SMPP_PRIORITY_FLAG);
		group_lst.add(_grp_SMPP_DELIVERY_TIME);
		group_lst.add(_grp_SMPP_VALIDITY_PERIOD);
		group_lst.add(_grp_SMPP_RD_SMSC_RECEIPT);
		group_lst.add(_grp_SMPP_RD_SME_ACK);
		group_lst.add(_grp_SMPP_RD_INTERMEDIATE_NOTIFICATION);
		group_lst.add(_grp_SMPP_REPLACE_IF_PRESENT);
		group_lst.add(_grp_SMPP_DATA_CODING);
		group_lst.add(_grp_SMPP_SM_DEFAULT_MSG_ID);
		group_lst.add(_grp_SMPP_SM_LENGTH);
		group_lst.add(_grp_SMPP_SM);
		group_lst.add(_grp_SMPP);
		group_lst.add(_grp_OPERAND);
		group_lst.add(_grp_OPERATOR);
		group_lst.add(_grp_RULE);

	}
}
