package net.m3ua.parameters.apc;

public abstract class Apc {
	public ApcType type;
	public int mask;
	
	public abstract void init(byte[] data);
}
