package net.gpu2;

public class ConnectionDescriptor {
	public boolean available = true;
	public boolean busy = false;
	public ConnectionType type;
	public NodeDescriptor node;
	public int sctp_id = -1;
	
	public ConnectionDescriptor(ConnectionType _type){
		type = _type;
	}
	
}
