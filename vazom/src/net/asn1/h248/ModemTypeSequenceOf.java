package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModemTypeSequenceOf extends SEQUENCE{

	public ModemType new_child(){
		return new ModemType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ModemType getChild(int index){
		return (ModemType)of_children.get(index);
	}
	public ModemTypeSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
