package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class GSMMAPLocalErrorcode extends INTEGER{

	public static final int _systemFailure = 34;
	public static final int _dataMissing = 35;
	public static final int _unexpectedDataValue = 36;
	public static final int _facilityNotSupported = 21;
	public static final int _incompatibleTerminal = 28;
	public static final int _resourceLimitation = 51;
	public static final int _unknownSubscriber = 1;
	public static final int _numberChanged = 44;
	public static final int _unknownMSC = 3;
	public static final int _unidentifiedSubscriber = 5;
	public static final int _unknownEquipment = 7;
	public static final int _roamingNotAllowed = 8;
	public static final int _illegalSubscriber = 9;
	public static final int _illegalEquipment = 12;
	public static final int _bearerServiceNotProvisioned = 10;
	public static final int _teleserviceNotProvisioned = 11;
	public static final int _noHandoverNumberAvailable = 25;
	public static final int _subsequentHandoverFailure = 26;
	public static final int _targetCellOutsideGroupCallArea = 42;
	public static final int _tracingBufferFull = 40;
	public static final int _noRoamingNumberAvailable = 39;
	public static final int _absentSubscriber = 27;
	public static final int _busySubscriber = 45;
	public static final int _noSubscriberReply = 46;
	public static final int _callBarred = 13;
	public static final int _forwardingViolation = 14;
	public static final int _forwardingFailed = 47;
	public static final int _cug_Reject = 15;
	public static final int _or_NotAllowed = 48;
	public static final int _ati_NotAllowed = 49;
	public static final int _atsi_NotAllowed = 60;
	public static final int _atm_NotAllowed = 61;
	public static final int _informationNotAvailabl = 62;
	public static final int _illegalSS_Operation = 16;
	public static final int _ss_ErrorStatus = 17;
	public static final int _ss_NotAvailable = 18;
	public static final int _ss_SubscriptionViolatio = 19;
	public static final int _ss_Incompatibility = 20;
	public static final int _unknownAlphabe = 71;
	public static final int _ussd_Busy = 72;
	public static final int _pw_RegistrationFailur = 37;
	public static final int _negativePW_Check = 38;
	public static final int _numberOfPW_AttemptsViolation = 43;
	public static final int _shortTermDenial = 29;
	public static final int _longTermDenial = 30;
	public static final int _subscriberBusyForMT_SMS = 31;
	public static final int _sm_DeliveryFailure = 32;
	public static final int _messageWaitingListFull = 33;
	public static final int _absentSubscriberSM = 6;
	public static final int _noGroupCallNumberAvailable = 50;
	public static final int _unauthorizedRequestingNetwork = 52;
	public static final int _unauthorizedLCSClient = 53;
	public static final int _positionMethodFailure = 54;
	public static final int _unknownOrUnreachableLCSClient = 58;
	public static final int _mm_EventNotSupported = 59;
	public static final int _secureTransportError = 4;
	public GSMMAPLocalErrorcode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
