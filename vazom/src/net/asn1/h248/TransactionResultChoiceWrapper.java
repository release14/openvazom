package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionResultChoiceWrapper extends SEQUENCE{

	public TransactionResultChoice get_transactionResult(){
		return (TransactionResultChoice)elements.get(0).data;
	}
	public TransactionResultChoice new_transactionResult(){
		return new TransactionResultChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_transactionResult(); }});
	}
	public TransactionResultChoiceWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
