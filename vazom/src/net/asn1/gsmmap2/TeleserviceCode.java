package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TeleserviceCode extends OCTET_STRING{

	public static final String allTeleservices = "'00000000'B";
	public static final String allSpeechTransmissionServices = "'00010000'B";
	public static final String telephony = "'00010001'B";
	public static final String emergencyCalls = "'00010010'B";
	public static final String allShortMessageServices = "'00100000'B";
	public static final String shortMessageMT_PP = "'00100001'B";
	public static final String shortMessageMO_PP = "'00100010'B";
	public static final String allFacsimileTransmissionServices = "'01100000'B";
	public static final String facsimileGroup3AndAlterSpeech = "'01100001'B";
	public static final String automaticFacsimileGroup3 = "'01100010'B";
	public static final String facsimileGroup4 = "'01100011'B";
	public static final String allDataTeleservices = "'01110000'B";
	public static final String allTeleservices_ExeptSMS = "'10000000'B";
	public static final String allVoiceGroupCallServices = "'10010000'B";
	public static final String voiceGroupCall = "'10010001'B";
	public static final String voiceBroadcastCall = "'10010010'B";
	public static final String allPLMN_specificTS = "'11010000'B";
	public static final String plmn_specificTS_1 = "'11010001'B";
	public static final String plmn_specificTS_2 = "'11010010'B";
	public static final String plmn_specificTS_3 = "'11010011'B";
	public static final String plmn_specificTS_4 = "'11010100'B";
	public static final String plmn_specificTS_5 = "'11010101'B";
	public static final String plmn_specificTS_6 = "'11010110'B";
	public static final String plmn_specificTS_7 = "'11010111'B";
	public static final String plmn_specificTS_8 = "'11011000'B";
	public static final String plmn_specificTS_9 = "'11011001'B";
	public static final String plmn_specificTS_A = "'11011010'B";
	public static final String plmn_specificTS_B = "'11011011'B";
	public static final String plmn_specificTS_C = "'11011100'B";
	public static final String plmn_specificTS_D = "'11011101'B";
	public static final String plmn_specificTS_E = "'11011110'B";
	public static final String plmn_specificTS_F = "'11011111'B";
	public TeleserviceCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
