package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DestTransactionID extends OCTET_STRING{

	public DestTransactionID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 9;
		asn_class = ASNTagClass.APPLICATION;
	}
}
