package net.isup.parameters.gd;

import java.util.HashMap;


public enum EncodingSchemeType {
	BCD_EVEN(0x00),
	BCD_ODD(0x20),
	IA5_CHARACTER(0x40);
	
	private int id;
	private static final HashMap<Integer, EncodingSchemeType> lookup = new HashMap<Integer, EncodingSchemeType>();
	static{
		for(EncodingSchemeType td :EncodingSchemeType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static EncodingSchemeType get(int id){ return lookup.get(id); }
	private EncodingSchemeType(int _id){ id = _id; }		
	
}
