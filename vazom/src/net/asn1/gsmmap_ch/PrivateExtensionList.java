package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PrivateExtensionList extends SEQUENCE{

	public PrivateExtension new_child(){
		return new PrivateExtension();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public PrivateExtension getChild(int index){
		return (PrivateExtension)of_children.get(index);
	}
	public PrivateExtensionList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
