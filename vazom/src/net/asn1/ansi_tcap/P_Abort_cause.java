package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class P_Abort_cause extends INTEGER{

	public static final int _unrecognizedPackageType = 1;
	public static final int _incorrectTransactionPortion = 2;
	public static final int _badlyStructuredTransactionPortion = 3;
	public static final int _unassignedRespondingTransactionID = 4;
	public static final int _permissionToReleaseProblem = 5;
	public static final int _resourceUnavailable = 6;
	public static final int _unrecognizedDialoguePortionID = 7;
	public static final int _badlyStructuredDialoguePortion = 8;
	public static final int _missingDialoguePortion = 9;
	public static final int _inconsistentDialoguePortion = 10;
	public P_Abort_cause(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 23;
		asn_class = ASNTagClass.PRIVATE;
	}
}
