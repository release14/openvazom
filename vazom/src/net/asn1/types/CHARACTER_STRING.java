package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class CHARACTER_STRING extends ASNType {

	public CHARACTER_STRING(){
		super();
		universalTag = ASNTag.CHARACTER_STRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
