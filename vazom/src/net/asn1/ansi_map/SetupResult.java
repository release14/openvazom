package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SetupResult extends OCTET_STRING{

	public SetupResult(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
