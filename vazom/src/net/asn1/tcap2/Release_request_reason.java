package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Release_request_reason extends INTEGER{

	public static final int _normal = 0;
	public static final int _urgent = 1;
	public static final int _user_defined = 30;
	public Release_request_reason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
