package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ForwardingReason extends ENUMERATED{

	public static final int _notReachable = 0;
	public static final int _busy = 1;
	public static final int _noReply = 2;
	public ForwardingReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
