package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendRoutingInfoArg extends SEQUENCE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public CUG_CheckInfo get_cug_CheckInfo(){
		return (CUG_CheckInfo)elements.get(1).data;
	}
	public CUG_CheckInfo new_cug_CheckInfo(){
		return new CUG_CheckInfo();
	}

	public NumberOfForwarding get_numberOfForwarding(){
		return (NumberOfForwarding)elements.get(2).data;
	}
	public NumberOfForwarding new_numberOfForwarding(){
		return new NumberOfForwarding();
	}

	public InterrogationType get_interrogationType(){
		return (InterrogationType)elements.get(3).data;
	}
	public InterrogationType new_interrogationType(){
		return new InterrogationType();
	}

	public NULL get_or_Interrogation(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_or_Interrogation(){
		return new NULL();
	}

	public OR_Phase get_or_Capability(){
		return (OR_Phase)elements.get(5).data;
	}
	public OR_Phase new_or_Capability(){
		return new OR_Phase();
	}

	public ISDN_AddressString get_gmsc_OrGsmSCF_Address(){
		return (ISDN_AddressString)elements.get(6).data;
	}
	public ISDN_AddressString new_gmsc_OrGsmSCF_Address(){
		return new ISDN_AddressString();
	}

	public CallReferenceNumber get_callReferenceNumber(){
		return (CallReferenceNumber)elements.get(7).data;
	}
	public CallReferenceNumber new_callReferenceNumber(){
		return new CallReferenceNumber();
	}

	public ForwardingReason get_forwardingReason(){
		return (ForwardingReason)elements.get(8).data;
	}
	public ForwardingReason new_forwardingReason(){
		return new ForwardingReason();
	}

	public Ext_BasicServiceCode get_basicServiceGroup(){
		return (Ext_BasicServiceCode)elements.get(9).data;
	}
	public Ext_BasicServiceCode new_basicServiceGroup(){
		return new Ext_BasicServiceCode();
	}

	public ExternalSignalInfo get_networkSignalInfo(){
		return (ExternalSignalInfo)elements.get(10).data;
	}
	public ExternalSignalInfo new_networkSignalInfo(){
		return new ExternalSignalInfo();
	}

	public CamelInfo get_camelInfo(){
		return (CamelInfo)elements.get(11).data;
	}
	public CamelInfo new_camelInfo(){
		return new CamelInfo();
	}

	public SuppressionOfAnnouncement get_suppressionOfAnnouncement(){
		return (SuppressionOfAnnouncement)elements.get(12).data;
	}
	public SuppressionOfAnnouncement new_suppressionOfAnnouncement(){
		return new SuppressionOfAnnouncement();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(13).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public AlertingPattern get_alertingPattern(){
		return (AlertingPattern)elements.get(14).data;
	}
	public AlertingPattern new_alertingPattern(){
		return new AlertingPattern();
	}

	public NULL get_ccbs_Call(){
		return (NULL)elements.get(15).data;
	}
	public NULL new_ccbs_Call(){
		return new NULL();
	}

	public SupportedCCBS_Phase get_supportedCCBS_Phase(){
		return (SupportedCCBS_Phase)elements.get(16).data;
	}
	public SupportedCCBS_Phase new_supportedCCBS_Phase(){
		return new SupportedCCBS_Phase();
	}

	public Ext_ExternalSignalInfo get_additionalSignalInfo(){
		return (Ext_ExternalSignalInfo)elements.get(17).data;
	}
	public Ext_ExternalSignalInfo new_additionalSignalInfo(){
		return new Ext_ExternalSignalInfo();
	}

	public IST_SupportIndicator get_istSupportIndicator(){
		return (IST_SupportIndicator)elements.get(18).data;
	}
	public IST_SupportIndicator new_istSupportIndicator(){
		return new IST_SupportIndicator();
	}

	public NULL get_pre_pagingSupported(){
		return (NULL)elements.get(19).data;
	}
	public NULL new_pre_pagingSupported(){
		return new NULL();
	}

	public CallDiversionTreatmentIndicator get_callDiversionTreatmentIndicator(){
		return (CallDiversionTreatmentIndicator)elements.get(20).data;
	}
	public CallDiversionTreatmentIndicator new_callDiversionTreatmentIndicator(){
		return new CallDiversionTreatmentIndicator();
	}

	public NULL get_longFTN_Supported(){
		return (NULL)elements.get(21).data;
	}
	public NULL new_longFTN_Supported(){
		return new NULL();
	}

	public NULL get_suppress_VT_CSI(){
		return (NULL)elements.get(22).data;
	}
	public NULL new_suppress_VT_CSI(){
		return new NULL();
	}

	public NULL get_suppressIncomingCallBarring(){
		return (NULL)elements.get(23).data;
	}
	public NULL new_suppressIncomingCallBarring(){
		return new NULL();
	}

	public NULL get_gsmSCF_InitiatedCall(){
		return (NULL)elements.get(24).data;
	}
	public NULL new_gsmSCF_InitiatedCall(){
		return new NULL();
	}

	public Ext_BasicServiceCode get_basicServiceGroup2(){
		return (Ext_BasicServiceCode)elements.get(25).data;
	}
	public Ext_BasicServiceCode new_basicServiceGroup2(){
		return new Ext_BasicServiceCode();
	}

	public ExternalSignalInfo get_networkSignalInfo2(){
		return (ExternalSignalInfo)elements.get(26).data;
	}
	public ExternalSignalInfo new_networkSignalInfo2(){
		return new ExternalSignalInfo();
	}

	public SuppressMTSS get_suppressMTSS(){
		return (SuppressMTSS)elements.get(27).data;
	}
	public SuppressMTSS new_suppressMTSS(){
		return new SuppressMTSS();
	}

	public NULL get_mtRoamingRetrySupported(){
		return (NULL)elements.get(28).data;
	}
	public NULL new_mtRoamingRetrySupported(){
		return new NULL();
	}

	public EMLPP_Priority get_callPriority(){
		return (EMLPP_Priority)elements.get(29).data;
	}
	public EMLPP_Priority new_callPriority(){
		return new EMLPP_Priority();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cug_CheckInfo(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_numberOfForwarding(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interrogationType(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_or_Interrogation(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_or_Capability(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_gmsc_OrGsmSCF_Address(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callReferenceNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_forwardingReason(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_basicServiceGroup(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkSignalInfo(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_camelInfo(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_suppressionOfAnnouncement(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_alertingPattern(); }});
		elements.add(new ElementDescriptor(15, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ccbs_Call(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_supportedCCBS_Phase(); }});
		elements.add(new ElementDescriptor(17, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additionalSignalInfo(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_istSupportIndicator(); }});
		elements.add(new ElementDescriptor(19, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_pre_pagingSupported(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_callDiversionTreatmentIndicator(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_longFTN_Supported(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_suppress_VT_CSI(); }});
		elements.add(new ElementDescriptor(23, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_suppressIncomingCallBarring(); }});
		elements.add(new ElementDescriptor(24, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_gsmSCF_InitiatedCall(); }});
		elements.add(new ElementDescriptor(25, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_basicServiceGroup2(); }});
		elements.add(new ElementDescriptor(26, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_networkSignalInfo2(); }});
		elements.add(new ElementDescriptor(27, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_suppressMTSS(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_mtRoamingRetrySupported(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_callPriority(); }});
	}
	public SendRoutingInfoArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
