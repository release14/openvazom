package net.isup.parameters;

import net.isup.ParameterType;

public class AccessTransport extends ParameterBase {
	public byte[] q931_value;
	public AccessTransport(){
		type = ParameterType.ACCESS_TRANSPORT;
	}

	public void init(byte[] data) {
		if(data != null){
			q931_value = data;
		}
	}

}
