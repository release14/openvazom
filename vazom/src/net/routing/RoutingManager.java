package net.routing;

import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import net.config.SGNConfigData;
import net.m3ua_conn.M3UAConnManager;
import net.m3ua_conn.M3UAPayload;
import net.smpp_conn.SMPPConnection;
import net.smpp_conn.SMPPPayload;
import net.vstp.VSTP_SGF_CorrelationPacket;

import org.apache.log4j.Logger;

public class RoutingManager {
	public static Logger logger=Logger.getLogger(RoutingManager.class);
	private static ConcurrentHashMap<String, RoutingConnection> map;

	
	public static void add(String id, RoutingConnection conn){
		if(!exists(id)) map.put(id, conn);
		else logger.warn("RoutingManager: connection [" + id + "] already exists!");
	}
	public static void remove(String id){
		map.remove(id);
	}
	public static boolean exists(String id){
		return map.get(id) != null;
	}
	public static RoutingConnectionType getType(String id){
		if(map.get(id) != null) return map.get(id).type;
		return null;
	}
	
	public static RoutingConnectionPayload createPayload(RoutingConnectionType type){
		switch(type){
			case M3UA: return new M3UAPayload();
			case MTP3: return null; //TODO
			case SMPP: return new SMPPPayload();
		}
		return null;
	}
	
	
	public static RoutingConnection getDefault(RoutingConnectionType type, VSTP_SGF_CorrelationPacket extra_param){
		RoutingConnection res = null;
		switch(type){
			case M3UA: res = M3UAConnManager.getConnection(M3UAConnManager.default_conn); break;
			case MTP3: 
				// TODO
				break;
			case SMPP:
				if(extra_param != null){
					if(extra_param.smpp_packet != null){
						res = map.get(extra_param.smpp_packet.client_ip + ":" + extra_param.smpp_packet.client_port);
					}
				}
				break;
		}
		return res;
	}
	
	public static RoutingConnection get(String id){
		return map.get(id);
		
		/*
		// if using SMPP connection, create new TCP connection
		if(id.equals(SGNConfigData.smpp_end_point_name)){
			try{
				Socket socket = new Socket(SGNConfigData.smpp_end_point_address, SGNConfigData.smpp_end_point_port);
				SMPPConnection smpp_conn = new SMPPConnection(socket, false);
				//smpp_conn.start();
				smpp_conn.initSMPP();
				return smpp_conn;
				
			}catch(Exception e){
				logger.warn("RoutingConnection Manager: Cannot create SMPP end point connection to: [" + SGNConfigData.smpp_end_point_address + ":" + SGNConfigData.smpp_end_point_port + "]!");
				return null;
			}
		// else return already active M3UA/MTP3
		}else{
			return map.get(id);
			
		}
		*/
	}

	
	public static void send_fail_check(RoutingConnectionPayload rpl, String conn_lst){
		RoutingConnection conn = null;
		if(rpl != null){
			String[] tmp = conn_lst.split(":");
			rpl.connections = new ArrayList<String>();
			for(int i = 0; i<tmp.length; i++) rpl.connections.add(tmp[i]);
			for(int i = 0; i<rpl.connections.size(); i++){
				conn = get(rpl.connections.get(i));
				if(conn != null){
					if(conn.active){
						conn.out_offer(rpl);
						return;
					}
				}else{
					logger.warn("Routing Connection [" + rpl.connections.get(i) + "] does not exist!");
					
				}
				
			}
			logger.warn("No active routing connections available!");

		}
	}
	
	
	
	public static void route(RoutingConnectionPayload rpl, String conn_id){
		RoutingConnection conn = map.get(conn_id);
		if(conn != null){
			logger.debug("RoutingManager: routing to connection [" + conn_id + "], type [" + conn.type + "]!");
			// send ti conn out queue
			conn.out_offer(rpl);
			
		}else logger.warn("RoutingManager: connection [" + conn_id + "] does not exist!");
	}
	public static void init(){
		logger.info("Starting Routing Manager...");
		map = new ConcurrentHashMap<String, RoutingConnection>();
		
	
	}
}
