package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TargetCellIDList extends SEQUENCE{

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(0).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public TargetCellID get_targetCellID1(){
		return (TargetCellID)elements.get(1).data;
	}
	public TargetCellID new_targetCellID1(){
		return new TargetCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetCellID1(); }});
	}
	public TargetCellIDList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
