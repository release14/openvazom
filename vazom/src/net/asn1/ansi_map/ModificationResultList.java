package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModificationResultList extends SEQUENCE{

	public ModificationResult new_child(){
		return new ModificationResult();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ModificationResult getChild(int index){
		return (ModificationResult)of_children.get(index);
	}
	public ModificationResultList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
