package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ONoAnswer extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSID get_msid(){
		return (MSID)elements.get(3).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(4).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public TriggerType get_triggerType(){
		return (TriggerType)elements.get(5).data;
	}
	public TriggerType new_triggerType(){
		return new TriggerType();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(6).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(7).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(8).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(9).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(10).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(11).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(12).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public Digits get_digits(){
		return (Digits)elements.get(13).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(14).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(15).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(16).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(17).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(18).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingPartyName get_redirectingPartyName(){
		return (RedirectingPartyName)elements.get(19).data;
	}
	public RedirectingPartyName new_redirectingPartyName(){
		return new RedirectingPartyName();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(20).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(21).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(279, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_triggerType(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(245, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingPartyName(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
	}
	public ONoAnswer(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
