package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ObservedEvent extends SEQUENCE{

	public EventName get_eventName(){
		return (EventName)elements.get(0).data;
	}
	public EventName new_eventName(){
		return new EventName();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(1).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public EventParameterSequenceOf get_eventParList(){
		return (EventParameterSequenceOf)elements.get(2).data;
	}
	public EventParameterSequenceOf new_eventParList(){
		return new EventParameterSequenceOf();
	}

	public TimeNotation get_timeNotation(){
		return (TimeNotation)elements.get(3).data;
	}
	public TimeNotation new_timeNotation(){
		return new TimeNotation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventParList(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeNotation(); }});
	}
	public ObservedEvent(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
