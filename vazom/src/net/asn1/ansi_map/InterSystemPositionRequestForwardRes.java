package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemPositionRequestForwardRes extends SET{

	public MSCID get_mscid(){
		return (MSCID)elements.get(0).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public PositionResult get_positionResult(){
		return (PositionResult)elements.get(1).data;
	}
	public PositionResult new_positionResult(){
		return new PositionResult();
	}

	public LCSBillingID get_lcsBillingID(){
		return (LCSBillingID)elements.get(2).data;
	}
	public LCSBillingID new_lcsBillingID(){
		return new LCSBillingID();
	}

	public PositionInformation get_positionInformation(){
		return (PositionInformation)elements.get(3).data;
	}
	public PositionInformation new_positionInformation(){
		return new PositionInformation();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(4).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(338, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_positionResult(); }});
		elements.add(new ElementDescriptor(367, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcsBillingID(); }});
		elements.add(new ElementDescriptor(336, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_positionInformation(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
	}
	public InterSystemPositionRequestForwardRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
