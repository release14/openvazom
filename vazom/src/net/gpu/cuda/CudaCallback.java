package net.gpu.cuda;


public abstract class CudaCallback{
	
	public abstract void execute(byte[] data);

}
