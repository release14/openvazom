package net.spcap;

import java.util.HashMap;

public enum EtherType {
	IP(0x0800),
	VLAN(0x8100);
	
	
	
	private int id;
	private static final HashMap<Integer, EtherType> lookup = new HashMap<Integer, EtherType>();
	static{
		for(EtherType td : EtherType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static EtherType get(int id){ return lookup.get(id); }
	private EtherType(int _id){ id = _id; }	
}
