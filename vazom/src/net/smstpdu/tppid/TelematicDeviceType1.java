package net.smstpdu.tppid;

import java.util.HashMap;

public enum TelematicDeviceType1 {
	IMPLICIT_SPECIFIC_TO_THIS_SC(0x00),
	TELEX(0x01),
	GROUP_3_TELEFAX(0x02),
	GROUP_4_TELEFAX(0x03),
	VOICE_TELEPHONE(0x04),
	ERMES(0x05),
	NATIONAL_PAGING_SYSTEM(0x06),
	VIDEOTEX(0x07),
	TELETEX_UNSPECIFIED(0x08),
	TELETEX_PSPDN(0x09),
	TELETEX_CSPDN(0x0A),
	TELETEX_PSTN(0x0B),
	TELETEX_ISDN(0x0C),
	UCI(0x0D),
	RESERVED1(0x0E),
	RESERVED2(0x0F),
	MSG_HANDLING_FACILITY(0x10),
	X_400_BASED_MSG_HANDLING_SYSTEM(0x11),
	INTERNET_ELECTRONIC_MAIL(0x12),
	RESERVED3(0x13),
	RESERVED4(0x14),
	RESERVED5(0x15),
	RESERVED6(0x16),
	RESERVED7(0x17),
	SC_SPECIFIC_1(0x18),
	SC_SPECIFIC_2(0x19),
	SC_SPECIFIC_3(0x1A),
	SC_SPECIFIC_4(0x1B),
	SC_SPECIFIC_5(0x1C),
	SC_SPECIFIC_6(0x1D),
	SC_SPECIFIC_7(0x1E),
	GSM_UMTS_MOBILE_STATION(0x1F);
	
	
	// BITS 4 to 0 = 000XXXXX
	private int id;
	private static final HashMap<Integer, TelematicDeviceType1> lookup = new HashMap<Integer, TelematicDeviceType1>();
	static{
		for(TelematicDeviceType1 td : TelematicDeviceType1.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static TelematicDeviceType1 get(int id){ return lookup.get(id); }
	private TelematicDeviceType1(int _id){ id = _id; }
	
	
	
}
