package net.isup.parameters.bci;

import java.util.HashMap;



public enum ChargeIndicatorType {
	NO_INDICATION(0x00),
	NO_CHARGE(0x01),
	CHARGE(0x02);
	
	
	private int id;
	private static final HashMap<Integer, ChargeIndicatorType> lookup = new HashMap<Integer, ChargeIndicatorType>();
	static{
		for(ChargeIndicatorType td : ChargeIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ChargeIndicatorType get(int id){ return lookup.get(id); }
	private ChargeIndicatorType(int _id){ id = _id; }		
}
