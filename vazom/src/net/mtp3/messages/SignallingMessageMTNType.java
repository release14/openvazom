package net.mtp3.messages;

import java.util.HashMap;

public enum SignallingMessageMTNType {
	SLTM(SignallingMessageMTNGroupType.TEST_MESSAGE.getId(), 0x01),
	SLTA(SignallingMessageMTNGroupType.TEST_MESSAGE.getId(), 0x02);
	
	private int id;
	private int class_id;
	private static final HashMap<String, SignallingMessageMTNType> lookup = new HashMap<String, SignallingMessageMTNType>();
	static{
		for(SignallingMessageMTNType td : SignallingMessageMTNType.values()){
			lookup.put(td.class_id + ":" + td.id, td);
		}
	}
	public int getId(){ return id; }
	public int getClassId(){ return class_id; }
	public static SignallingMessageMTNType get(int id, int _class_id){ return lookup.get(_class_id + ":" + id); }
	private SignallingMessageMTNType(int _class_id, int _id){ id = _id; class_id = _class_id; }	
}
