package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class HandoffReason extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _unspecified =  1 ;
	public static final int _weak_Signal =  2 ;
	public static final int _off_loading =  3 ;
	public static final int _anticipatory =  4 ;
	public HandoffReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
