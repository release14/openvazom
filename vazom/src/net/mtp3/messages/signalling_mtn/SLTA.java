package net.mtp3.messages.signalling_mtn;

import java.util.ArrayList;
import java.util.Arrays;

import net.mtp3.messages.SignallingMessageMTNGroupType;
import net.mtp3.messages.SignallingMessageMTNType;
import net.utils.Utils;

public class SLTA extends SignallingMessageBaseMTN {
	int testLength;
	byte[] testPattern;

	
	public SLTA(){
		super();
		signallingGroupType = SignallingMessageMTNGroupType.TEST_MESSAGE;
		signallingType = SignallingMessageMTNType.SLTA;
	}
	
	public byte[] encode(){
		ArrayList<Byte> buff = new ArrayList<Byte>();
		byte[] res = null;
		int tmp = (signallingType.getId() << 4) + signallingGroupType.getId();
		buff.add((byte)tmp);
		buff.add((byte)(testLength << 4));
		if(testPattern != null) for(int i = 0; i<testPattern.length; i++) buff.add(testPattern[i]);
		res = Utils.list2array(buff);
		return res;
	
	}
	
	public void init(byte[] data) {
		super.init(data);
		testLength = (data[byte_pos++] & 0xf0) >> 4;
		testPattern = Arrays.copyOfRange(data, byte_pos, byte_pos + testLength);
	}
}
