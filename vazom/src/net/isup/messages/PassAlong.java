package net.isup.messages;

import net.isup.MessageType;

public class PassAlong extends MessageBase {
	public byte[] message_data;
	public PassAlong(){
		super();
		type = MessageType.PASS_ALONG;
	}
	
	public void init(byte[] data) {
		message_data = data;
	}

}
