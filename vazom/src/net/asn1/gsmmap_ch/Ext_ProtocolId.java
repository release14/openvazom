package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Ext_ProtocolId extends ENUMERATED{

	public static final int _ets_300356 = 1;
	public Ext_ProtocolId(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
