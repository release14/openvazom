package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PropertyGroup extends SEQUENCE{

	public PropertyParm new_child(){
		return new PropertyParm();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public PropertyParm getChild(int index){
		return (PropertyParm)of_children.get(index);
	}
	public PropertyGroup(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
