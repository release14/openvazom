package net.isup.parameters;

import net.isup.ParameterType;

public abstract class ParameterBase {
	public ParameterType type;
	public int length;
	public byte[] value;
	public int byte_position = 0;
	
	
	public static ParameterBase createParameter(ParameterType param_type){
		if(param_type != null){
			switch(param_type){
				case ACCESS_DELIVERY_INFORMATION: return new AccessDeliveryInformation();
				case ACCESS_TRANSPORT: return new AccessTransport();
				case APPLICATION_TRANSPORT_PARAMETER: return new ApplicationTransportParameter();
				case AUTOMATIC_CONGESTION_LEVEL: return new AutomaticCongestionLevel();
				case BACKWARD_CALL_INDICATORS: return new BackwardCallIndicators();
				case BACKWARD_GVNS: return new BackwardGVNS();
				case CALL_DIVERSION_INFORMATION: return new CallDiversionInformation();
				case CALL_DIVERSION_TREATMENT_INDICATORS: return new CallDiversionTreatmentIndicators();
				case CALL_HISTORY_INFORMATION: return new CallHistoryInformation();
				case CALL_OFFERING_TREATMENT_INDICATORS: return new CallOfferingTreatmentIndicators();
				case CALL_REFERENCE: return new CallReference();
				case CALL_TRANSFER_NUMBER: return new CallTransferNumber();
				case CALL_TRANSFER_REFERENCE: return new CallTransferReference();
				case CALLED_DIRECTORY_NUMBER: return new CalledDirectoryNumber();
				case CALLED_IN_NUMBER: return new CalledINNumber();
				case CALLED_PARTY_NUMBER: return new CalledPartyNumber();
				case CALLING_GEODETIC_LOCATION: return new CallingGeodeticLocation();
				case CALLING_PARTY_CATEGORY: return new CallingPartyCategory();
				case CALLING_PARTY_NUMBER: return new CallingPartyNumber();
				case CAUSE_INDICATORS: return new CauseIndicators();
				case CCNR_POSSIBLE_INDICATOR: return new CCNRPossibleIndicator();
				case CCSS: return new CCSS();
				case CHARGED_PARTY_IDENTIFICATION: return new ChargedPartyIdentification();
				case CIRCUIT_ASSIGNMENT_MAP: return new CircuitAssignmentMap();
				case CIRCUIT_GROUP_SUPERVISION: return new CircuitGroupSupervision();
				case CIRCUIT_STATE_INDICATOR: return new CircuitStateIndicator();
				case CLOSED_USER_GROUP_INTERLOCK_CODE: return new ClosedUserGroupInterlock();
				case COLLECT_CALL_REQUEST: return new CollectCallRequest();
				case CONFERENCE_TREATMENT_INDICATORS: return new ConferenceTreatmentIndicators();
				case CONNECTED_NUMBER: return new ConnectedNumber();
				case CONNECTION_REQUEST: return new ConnectionRequest();
				case CONTINUITY_INDICATORS: return new ContinuityIndicators();
				case CORRELATION_ID: return new CorrelationId();
				case DISPLAY_INFORMATION: return new DisplayInformation();
				case ECHO_CONTROL_INFORMATION: return new EchoControlInformation();
				case EVENT_INFORMATION: return new EventInformation();
				case FACILITY_INDICATOR: return new FacilityIndicator();
				case FORWARD_CALL_INDICATORS: return new ForwardCallIndicators();
				case FORWARD_GVNS: return new ForwardGVNS();
				case GENERIC_DIGITS: return new GenericDigits();
				case GENERIC_NOTIFICATION: return new GenericNotificationIndicator();
				case GENERIC_NUMBER: return new GenericNumber();
				case HOP_COUNTER: return new HopCounter();
				case HTR_INFORMATION: return new HTRInformation();
				case INFORMATION_INDICATORS: return new InformationIndicators();
				case INFORMATION_REQUEST_INDICATORS: return new InformationRequestIndicators();
				case LOCATION_NUMBER: return new LocationNumber();
				case LOOP_PREVENTION_INDICATORS: return new LoopPreventionIndicators();
				case MCID_REQUEST_INDICATOR: return new MCIDRequestIndicators();
				case MCID_RESPONSE_INDICATOR: return new MCIDResponseIndicators();
				case MESSAGE_COMPATIBILITY_INFORMATION: return new MessageCompatibilityInformation();
				case MLPP_PRECEDENCE: return new MLPPPrecedence();
				case NATURE_OF_CONNECTION_INDICATORS: return new NatureOfConnIndicators();
				case NETWORK_MANAGEMENT_CONTROLS: return new NetworkManagementControls();
				case NETWORK_ROUTING_NUMBER: return new NetworkRoutingNumber();
				case NETWORK_SPECIFIC_FACILITIES: return new NetworkSpecificFacility();
				case NUMBER_PORTABILITY_FORWARD_INFORMATION: return new NumberPortabilityForwardInformation();
				case OPTIONAL_BACKWARD_CALL_INDICATORS: return new OptionalBackwardCallIndicators();
				case OPTIONAL_FORWARD_CALL_INDICATORS: return new OptionalForwardCallIndicators();
				case ORIGINAL_CALLED_IN_NUMBER: return new OriginalCalledINNumber();
				case ORIGINAL_CALLED_NUMBER: return new OriginalCalledNumber();
				case ORIGINATION_ISC_POINT_CODE: return new OriginationISCPointCode();
				case PARAMETER_COMPATIBILITY_INFORMATION: return new ParameterCompatibilityInformation();
				case PIVOT_CAPABILITY: return new PivotCapability();
				case PIVOT_COUNTER: return new PivotCounter();
				case PIVOT_ROUTING_BACKWARD_INFORMATION: return new PivotRoutingBackwardInformation();
				case PIVOT_ROUTING_FORWARD_INFORMATION: return new PivotRoutingForwardInformation();
				case PIVOT_ROUTING_INDICATORS: return new PivotRoutingIndicators();
				case PIVOT_STATUS: return new PivotStatus();
				case PROPAGATION_DELAY_COUNTER: return new PropagationDelayCounter();
				case QUERY_ON_RELEASE_CAPABILITY: return new QueryOnReleaseCapability();
				case RANGE_AND_STATUS: return new RangeAndStatus();
				case REDIRECT_BACKWARD_INFORMATION: return new RedirectBackwardInformation();
				case REDIRECT_CAPABILITY: return new RedirectCapability();
				case REDIRECT_COUNTER: return new RedirectCounter();
				case REDIRECT_FORWARD_INFORMATION: return new RedirectForwardInformation();
				case REDIRECT_STATUS: return new RedirectStatus();
				case REDIRECTING_NUMBER: return new RedirectingNumber();
				case REDIRECTION_INFORMATION: return new RedirectionInformation();
				case REDIRECTION_NUMBER: return new RedirectionNumber();
				case REDIRECTION_NUMBER_RESTRICTION: return new RedirectionNumberRestriction();
				case REMOTE_OPERATIONS: return new RemoteOperations();
				case SCF_ID: return new SCFId();
				case SERVICE_ACTIVATION: return new ServiceActivation();
				case SIGNALING_POINT_CODE: return new SignallingPointCode();
				case SUBSEQUENT_NUMBER: return new SubsequentNumber();
				case SUSPEND_RESUME_INDICATORS: return new SuspendResumeIndicators();
				case TRANSIT_NETWORK_SELECTION: return new TransitNetworkSelection();
				case TRANSMISSION_MEDIUM_REQUIREMENT: return new TransmissionMediumRequirement();
				case TRANSMISSION_MEDIUM_REQUIREMENT_PRIME: return new TransmissionMediumRequirementPrime();
				case TRANSMISSION_MEDIUM_USED: return new TransmissionMediumUsed();
				case UID_ACTION_INDICATORS: return new UIDActionIndicators();
				case UID_CAPABILITY_INDICATORS: return new UIDCapabilityIndicators();
				case USER_SERVICE_INFORMATION: return new UserServiceInformation();
				case USER_SERVICE_INFORMATION_PRIME: return new UserServiceInformationPrime();
				case USER_TELESERVICE_INFORMATION: return new UserTeleserviceInformation();
				case USER_TO_USER_INDICATORS: return new UserToUserIndicators();
				case USER_TO_USER_INFORMATION: return new UserToUserInformation();
				
				
				
				
			}
		}
		return null;
	}
	
	
	public void init(byte[] data){
		value = data;
	}	
	
	
}
