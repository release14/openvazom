package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SigParameter extends SEQUENCE{

	public SigParameterName get_sigParameterName(){
		return (SigParameterName)elements.get(0).data;
	}
	public SigParameterName new_sigParameterName(){
		return new SigParameterName();
	}

	public SigParamValues get_value(){
		return (SigParamValues)elements.get(1).data;
	}
	public SigParamValues new_value(){
		return new SigParamValues();
	}

	public SigParameterExtraInfoChoiceWrapper get_extraInfo(){
		return (SigParameterExtraInfoChoiceWrapper)elements.get(2).data;
	}
	public SigParameterExtraInfoChoiceWrapper new_extraInfo(){
		return new SigParameterExtraInfoChoiceWrapper();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sigParameterName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_value(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extraInfo(); }});
	}
	public SigParameter(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
