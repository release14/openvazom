package net.test;

import java.util.UUID;

import net.config.FNConfigData;
import net.ds.DataSourceType;
import net.gpu2.GPUDataItemType;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;

public class gpuntester {
	
	public static boolean connectNode(int sctp_id){
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		if(sctp_id > -1){
			// negotiate VSTP connect
			res = SSctp.send(sctp_id, VSTP.generate_init(LocationType.GPUN, FNConfigData.fn_id).getBytes(), 0);
			if(res > -1){
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						if(VSTP.check_accept(new String(sd.payload))){
							vstp_ready = true;
						}
					}
				}
				if(!vstp_ready){
					SSctp.shutdownClient(sctp_id);
					return false;
					
				}else return true;
			}else{
				SSctp.shutdownClient(sctp_id);
				return false;
			}
		}else{
			SSctp.shutdownClient(sctp_id);
			return false;
		}
	}
	public static void main(String[] args) {
		if(args.length < 5){
			System.out.println("Missing parameters!");
			System.out.println("Usage:");
			System.out.println("  arg 0 = PDN ADDRESS");
			System.out.println("  arg 1 = PDN PORT");
			System.out.println("  arg 2 = UPDATE BATCH SIZE");
			System.out.println("  arg 3 = LD BATCH SIZE");
			System.out.println("  arg 4 = LD PAUSE IN MILISEC");
			System.exit(0);
		}
		int sctp_id = SSctp.initClient(args[0], Integer.parseInt(args[1]), 16, null, 0);
		String reply = null;
		String test_pattern = "1234567890-=qwertyuiop[]asdfghjkl;xx<zxcvbnm,./QWERTYUIOP[]ASDFGHJKL;xx<ZXCVBNM,./";
		MessageDescriptor md = null;
		MessageDescriptor md2 = null;
		UUID uuid = UUID.randomUUID();
		SSCTPDescriptor sd = null;
		int sctp_res;
		System.out.println("           PDN IP = " + args[0]);
		System.out.println("         PDN PORT = " + args[1]);
		System.out.println("UPDATE BATCH SIZE = " + args[2]);
		System.out.println("    LD BATCH SIZE = " + args[3]);
		if(sctp_id > 0){
			connectNode(sctp_id);
			

			//System.out.println(new String(md.encode()));
			
			// send
			System.out.println("Sending updates...");
			for(int i = 0; i<Integer.parseInt(args[2]); i++){
				uuid = UUID.randomUUID();
				md = new MessageDescriptor();
				md.header.destination = LocationType.GPUN;
				md.header.destination_id = "fn1";
				md.header.ds = DataSourceType.NA;
				md.header.msg_id = uuid.toString();
				md.header.msg_type = MessageType.SGC;
				md.header.source = LocationType.FN;
				md.header.source_id = "tester";
				md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.U.toString());
				md.values.put(VSTPDataItemType.GPU_PARAM.getId(), 0 + ":" + Utils.bytes2hexstr(test_pattern.getBytes(), ""));
				sctp_res = SSctp.send(sctp_id, md.encode(), 0);
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						md2 = VSTP.decode(sd.payload);
						if(md2.header.source == LocationType.GPUN && md2.header.msg_type == MessageType.SGC){
							if(md2.header.msg_id.equals(uuid.toString())){
								System.out.println("Update num. " + i + " status = OK");
							}
						}
					}
				}				
			}

			
			System.out.println("Pausing for 2 seconds...");
			
			try{ Thread.sleep(2000); }catch(Exception e){}
			System.out.println("Running kernel...");
			for(int i = 0; i<Integer.parseInt(args[3]); i++){
				uuid = UUID.randomUUID();
				md = new MessageDescriptor();
				md.header.destination = LocationType.GPUN;
				md.header.destination_id = "fn1";
				md.header.ds = DataSourceType.NA;
				md.header.msg_id = uuid.toString();
				md.header.msg_type = MessageType.SGC;
				md.header.source = LocationType.FN;
				md.header.source_id = "tester";
				md.values.put(VSTPDataItemType.GPU_CMD.getId(), GPUDataItemType.LD.toString());
				md.values.put(VSTPDataItemType.GPU_PARAM.getId(), 0 + ":" + Utils.bytes2hexstr(test_pattern.getBytes(), ""));

				//System.out.println(new String(md.encode()));
				
				// send
				sctp_res = SSctp.send(sctp_id, md.encode(), 0);
				// receive
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						md2 = VSTP.decode(sd.payload);
						if(md2.header.source == LocationType.GPUN && md2.header.msg_type == MessageType.SGC){
							System.out.println("Kernel VSTP id = [" + md2.header.msg_id + "], result = [" + md2.values.get(VSTPDataItemType.GPU_RESULT.getId()) + "]");
							//System.out.println("===================");
							//System.out.println(new String(md2.encode()));
						}
					}
				}				
				try{ Thread.sleep(Integer.parseInt(args[4])); }catch(Exception e){}
				
				
			}

			
			
		}

		
		
	}

}
