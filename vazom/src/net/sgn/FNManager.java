package net.sgn;

import java.util.concurrent.ConcurrentHashMap;


import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.vstp.LocationType;
import net.vstp.VSTP;

import org.apache.log4j.Logger;

public class FNManager {
	public static Logger logger=Logger.getLogger(FNManager.class);
	public static ConcurrentHashMap<Integer, FNDescriptor> fn_map;
	public static boolean fn_connected = false;

	
	public static void connectNode(int id){
		FNDescriptor fnd = fn_map.get(id);
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		String pl_str;
		String tmp;
		if(fnd != null){
			logger.info("Negotiating VSTP Connection, client sctp id = [" + id + "]!");
			if(fnd.sctp_id > -1){
				// negotiate VSTP connect
				sd = SSctp.receive(fnd.sctp_id);
				if(sd != null){
					if(sd.payload != null){
						pl_str = new String(sd.payload);
						if(VSTP.check_init(pl_str)){
							// node id
							tmp = VSTP.get_init_node_id(pl_str);
							fnd.id = tmp;
							res = SSctp.send(fnd.sctp_id, VSTP.generate_ack(LocationType.FN, tmp).getBytes(), 0);
							if(res > -1){
								logger.info("VSTP Connection initialized, sctp id = [" + fnd.sctp_id + "], node id = [" + fnd.id + "]!");
								vstp_ready = true;
								fn_connected = true;
								fnd.start();
							}
						}
					}
				}
				if(!vstp_ready){
					logger.warn("Error while negotiating VSTP Connection, removing FN Connection, sctp id = [" + id + "]!");
					removeNode(id);
					
				}
				
				
			}else logger.warn("Error while initializing SCTP Connection, client id = [" + id + "]!");

		}
	}	
	
	public static void addNode(int id, FNDescriptor fnd){
		fn_map.put(id, fnd);
		logger.info("Adding F node, sctp id = [" + id + "]!");
		connectNode(id);
	}
	public static FNDescriptor getNode(int sctp_id){
		return fn_map.get(sctp_id);
	}
	public static FNDescriptor getNode(String id){
		for(Integer i : fn_map.keySet()) if(fn_map.get(i).id.equals(id)) return fn_map.get(i);
		return null;
	}
	public static void removeNode(int id){
		fn_map.remove(id);
		if(fn_map.size() == 0) fn_connected = false;
	}	
	
	public static void init(){
		fn_map = new ConcurrentHashMap<Integer, FNDescriptor>();
		logger.info("Starting F Node Manager...");

	}
}
