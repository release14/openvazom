package net.ber;

public class BERInvalidLength extends Exception {
	long length;
	
	public BERInvalidLength(long _length){
		super("Invalid BER length: " + _length);
		length = _length;
	}
	
	
}
