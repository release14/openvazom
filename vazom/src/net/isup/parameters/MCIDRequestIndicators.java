package net.isup.parameters;

import net.isup.ParameterType;

public class MCIDRequestIndicators extends ParameterBase {

	public MCIDRequestIndicators(){
		type = ParameterType.MCID_REQUEST_INDICATOR;
	}
}
