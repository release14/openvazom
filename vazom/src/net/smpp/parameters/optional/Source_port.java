package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Source_port extends OptionalParameterBase {

	public Source_port(){
		type = OptionalParameterType.SOURCE_PORT;
	}
	public int value;
	public void init(byte[] data) {
		value = ((data[0] << 8) & 0xff) + (data[1] & 0xff);

	}

}
