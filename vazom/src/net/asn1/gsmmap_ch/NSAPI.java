package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NSAPI extends INTEGER{

	public NSAPI(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
