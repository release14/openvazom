package net.hplmnr;

import java.util.ArrayList;

import net.asn1.gsmmap2.MO_ForwardSM_Arg;
import net.asn1.gsmmap2.MT_ForwardSM_Arg;
import net.asn1.gsmmap2.ReturnError;
import net.asn1.gsmmap2.RoutingInfoForSM_Arg;
import net.asn1.gsmmap2.RoutingInfoForSM_Res;
import net.asn1.tcap2.TCMessage;
import net.m3ua.M3UAPacket;
import net.mtp3.MTP3Packet;
import net.routing.RoutingConnectionType;
import net.sccp.messages.UDT_UnitData;
import net.sctp.SSCTPDescriptor;
import net.smstpdu.AddressString;

public class HPLMNRPacket {
	public RoutingConnectionType routingType;
	public int ssctp_sid;
	public byte[] tcap_sid;
	public byte[] tcap_did;
	public String gsm_c;
	public byte[] invoke_id;
	//public boolean hasDialogue;
	//public byte[] app_ctx_oid;
	
	public HPLMNRReplyType replyType;
	public String calledPartyGT;
	public String callingPartyGT;
	public AddressString msisdn;
	public AddressString smsc;
	public String imsi;
	public AddressString networkNodeNumber;
	public AddressString additonalNumber;
	
	// MT specific part
	//public BerTag mt_root;
	//public Parameter mt_invokeParameter;
	public TCMessage mt_tcm;
	public TCMessage mo_tcm;
	public TCMessage sri_tcm;
	public ReturnError sri_gsmmap_err;
	public MT_ForwardSM_Arg mt_arg;
	public MO_ForwardSM_Arg mo_arg;
	public RoutingInfoForSM_Res sri_res;
	public RoutingInfoForSM_Arg sri_arg;
	public byte[] tcap_packet;
	
	public M3UAPacket m3ua_packet;
	public MTP3Packet mtp3_packet;
	//public DATA m3ua_data;
	public UDT_UnitData sccp_udt;
	
	public ArrayList<Object> extra_params;
	
	
}
