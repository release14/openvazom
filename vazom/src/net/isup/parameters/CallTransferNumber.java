package net.isup.parameters;

import net.isup.ParameterType;

public class CallTransferNumber extends ParameterBase {
	public CallTransferNumber(){
		type = ParameterType.CALL_TRANSFER_NUMBER;
	}
}
