package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StreamDescriptorSequenceOf extends SEQUENCE{

	public StreamDescriptor new_child(){
		return new StreamDescriptor();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public StreamDescriptor getChild(int index){
		return (StreamDescriptor)of_children.get(index);
	}
	public StreamDescriptorSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
