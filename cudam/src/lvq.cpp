#include <stdio.h> 
#include <string.h> 
#include "lvq.h" 

// constuctor
LVQ::LVQ(int _max_q_size, int _max_i_size){
	printf("%s\n", "Constructor...");
	max_q_size = _max_q_size;
	max_i_size = _max_i_size;
	current_size = 0;
	init();

}
// destructor
LVQ::~LVQ(){
        delete[] queue;
	printf("%s\n", "Destructor..");
}


void LVQ::init(){
	// each element is defined by one byte for length (max 255) and item data
	queue = new char[max_q_size*(max_i_size+1)];
	pthread_mutex_init(&q_lock, NULL);
	printf("%s\n", "Init...");
}

void LVQ::push(char *data, int length){
	if(length <= max_i_size){
		// lock mutex
		pthread_mutex_lock(&q_lock);
		// pop first, move other
		if(current_size == max_q_size){
			memcpy(queue, queue+(max_i_size+1), (max_q_size-1)*(max_i_size+1));
			current_size--;
		}

		queue[current_size*(max_i_size+1)] = length;
		memcpy(queue+current_size*(max_i_size+1)+1, data, length);
		current_size++;
		printf("push, list size: %d, item size: %d\n", current_size, length);
		// unlock mutex
		pthread_mutex_unlock(&q_lock);

	}
}


// puts data in buffer, returns length of buffer
int LVQ::pop(char *buffer){
	if(current_size > 0){
		// lock mutex
		pthread_mutex_lock(&q_lock);
		int l = queue[0] & 0xff;
		// get first item
		memcpy(buffer, queue+1, l);
		// move other items
		memcpy(queue, queue+(max_i_size+1), (current_size-1)*(max_i_size+1));	
		current_size--;
		printf("pop, list size: %d, item size: %d\n", current_size, l);
		// unlock mutex
		pthread_mutex_unlock(&q_lock);
		return l;

	}
}

// create a snapshot of current queue
int LVQ::snapshot(char *buffer){
	if(current_size > 0){
		// lock mutex
		pthread_mutex_lock(&q_lock);
		// copy memory
		memcpy(buffer, queue, (max_q_size)*(max_i_size+1));
		// unlock mutex
		pthread_mutex_unlock(&q_lock);
		// return current queue size
		return current_size;
	}
	
}

