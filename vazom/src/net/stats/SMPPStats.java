package net.stats;

public class SMPPStats {
	public long CLIENT_CONNECTION_COUNT;
	public long END_POINT_CONNECTION_COUNT;
	public long SUBMIT_SM_COUNT;
	public long SUBMIT_MULTI_COUNT;
	public long DELIVER_SM_COUNT;

}
