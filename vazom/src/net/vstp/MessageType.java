package net.vstp;

public enum MessageType {
	// Voice
	ISUP_IAM, //InitialAddress
	ISUP_ACM, //AddressComplete
	ISUP_REL, //Release
	ISUP_RLC, //ReleaseComplete
	ISUP_ANM, //Answer
	
	// Other
	DB_REQUEST,
	DB_REPLY,
	
	// SMS
	SMS_MO,
	SMS_MT,
	SMPP_MO,
	SMPP_MT,
	SMPP_RESP,
	TCAP_ABORT,
	MAP_RETURN_ERROR,
	MAP_RETURN_RESULT,
	HLR,
	HLRREQ,
	HLRRPL,
	SGC, // Inter node communication
	CLI; // Command Interface
}
