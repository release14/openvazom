package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SeizureType extends ENUMERATED{

	public static final int _unspecified =  0 ;
	public static final int _loop_back =  1 ;
	public SeizureType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
