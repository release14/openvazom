package net.stats;

public class VSTPStats {
	// SG
	public long VSTP_SGF_TIMEOUT;
	public long VSTP_CRL_ADD;
	public long VSTP_CRL_REMOVE;
	public long VSTP_SGC_CONNECTION_LOST;
	public long VSTP_F_CONNECTION_LOST;
	public long VSTP_NO_F_NODES;
	// F
	public long VSTP_SG_CONNECTION_LOST;
	public long VSTP_NO_SG_NODES;
}
