package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Ext_BasicServiceCode extends CHOICE{

	public Ext_BearerServiceCode get_ext_BearerService(){
		return (Ext_BearerServiceCode)elements.get(0).data;
	}
	public Ext_BearerServiceCode new_ext_BearerService(){
		return new Ext_BearerServiceCode();
	}

	public Ext_TeleserviceCode get_ext_Teleservice(){
		return (Ext_TeleserviceCode)elements.get(1).data;
	}
	public Ext_TeleserviceCode new_ext_Teleservice(){
		return new Ext_TeleserviceCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ext_BearerService(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ext_Teleservice(); }});
	}
	public Ext_BasicServiceCode(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
