package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SecondRequestedActions extends SEQUENCE{

	public BOOLEAN get_keepActive(){
		return (BOOLEAN)elements.get(0).data;
	}
	public BOOLEAN new_keepActive(){
		return new BOOLEAN();
	}

	public EventDMWrapper get_eventDM(){
		return (EventDMWrapper)elements.get(1).data;
	}
	public EventDMWrapper new_eventDM(){
		return new EventDMWrapper();
	}

	public SignalsDescriptor get_signalsDescriptor(){
		return (SignalsDescriptor)elements.get(2).data;
	}
	public SignalsDescriptor new_signalsDescriptor(){
		return new SignalsDescriptor();
	}

	public NotifyBehaviourWrapper get_notifyBehaviour(){
		return (NotifyBehaviourWrapper)elements.get(3).data;
	}
	public NotifyBehaviourWrapper new_notifyBehaviour(){
		return new NotifyBehaviourWrapper();
	}

	public NULL get_resetEventsDescriptor(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_resetEventsDescriptor(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_keepActive(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_eventDM(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalsDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_notifyBehaviour(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_resetEventsDescriptor(); }});
	}
	public SecondRequestedActions(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
