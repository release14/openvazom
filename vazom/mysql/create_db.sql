SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- -----------------------------------------------------
-- Table `direction`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `direction` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `type` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `mode`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mode` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `filter_action`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `filter_action` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SmsEncoding`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SmsEncoding` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TypeOfNumber`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TypeOfNumber` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pdu_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `pdu_type` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sms_status`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sms_status` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sms`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sms` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `direction_id` INT NOT NULL ,
  `type_id` INT NOT NULL ,
  `mode_id` INT NOT NULL ,
  `pdu_id` INT NULL ,
  `filter_action_id` INT NULL ,
  `sms_status_id` INT NULL ,
  `filter_total_score` INT NULL ,
  `filter_exit_point` VARCHAR(45) NULL ,
  `ip_source` VARCHAR(45) NULL ,
  `ip_destination` VARCHAR(45) NULL ,
  `tcp_source` INT NULL ,
  `tcp_destination` INT NULL ,
  `system_id` VARCHAR(45) NULL ,
  `gt_called` VARCHAR(150) NULL ,
  `gt_calling` VARCHAR(150) NULL ,
  `scda` VARCHAR(150) NULL ,
  `scoa` VARCHAR(150) NULL ,
  `imsi` VARCHAR(150) NULL ,
  `msisdn` VARCHAR(150) NULL ,
  `sms_destination` VARCHAR(150) NULL ,
  `sms_originating` VARCHAR(150) NULL ,
  `sms_text` TEXT NULL ,
  `timestamp` TIMESTAMP NULL ,
  `sms_text_enc_id` INT NULL ,
  `sms_destination_enc_id` INT NULL ,
  `sms_originating_enc_id` INT NULL ,
  `m3ua_data_dpc` INT NULL ,
  `m3ua_data_opc` INT NULL ,
  `tcap_sid` BIGINT NULL ,
  `tcap_did` BIGINT NULL ,
  `sms_conc_partnum` INT NULL ,
  `sms_conc_parts` INT NULL ,
  `sms_conc_msgid` INT NULL ,
  `error_code` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_sms_type` (`type_id` ASC) ,
  INDEX `fk_sms_direction1` (`direction_id` ASC) ,
  INDEX `fk_sms_mode1` (`mode_id` ASC) ,
  INDEX `fk_sms_filter_action1` (`filter_action_id` ASC) ,
  INDEX `fk_sms_SmsEncoding1` (`sms_text_enc_id` ASC) ,
  INDEX `fk_sms_TypeOfNumber1` (`sms_destination_enc_id` ASC) ,
  INDEX `fk_sms_TypeOfNumber2` (`sms_originating_enc_id` ASC) ,
  INDEX `fk_sms_PDUType1` (`pdu_id` ASC) ,
  INDEX `fk_sms_sms_status1` (`sms_status_id` ASC) ,
  CONSTRAINT `fk_sms_type`
    FOREIGN KEY (`type_id` )
    REFERENCES `type` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_direction1`
    FOREIGN KEY (`direction_id` )
    REFERENCES `direction` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_mode1`
    FOREIGN KEY (`mode_id` )
    REFERENCES `mode` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_filter_action1`
    FOREIGN KEY (`filter_action_id` )
    REFERENCES `filter_action` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_SmsEncoding1`
    FOREIGN KEY (`sms_text_enc_id` )
    REFERENCES `SmsEncoding` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_TypeOfNumber1`
    FOREIGN KEY (`sms_destination_enc_id` )
    REFERENCES `TypeOfNumber` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_TypeOfNumber2`
    FOREIGN KEY (`sms_originating_enc_id` )
    REFERENCES `TypeOfNumber` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_PDUType1`
    FOREIGN KEY (`pdu_id` )
    REFERENCES `pdu_type` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sms_sms_status1`
    FOREIGN KEY (`sms_status_id` )
    REFERENCES `sms_status` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `sri_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sri_type` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sri`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sri` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `sri_type_id` INT NOT NULL ,
  `m3ua_data_opc` INT NULL ,
  `m3ua_data_dpc` INT NULL ,
  `gt_called` VARCHAR(150) NULL ,
  `gt_calling` VARCHAR(150) NULL ,
  `tcap_sid` BIGINT NULL ,
  `tcap_did` BIGINT NULL ,
  `app_ctx_oid` VARCHAR(300) NULL ,
  `imsi` VARCHAR(20) NULL ,
  `msisdn` VARCHAR(50) NULL ,
  `msisdn_nai` INT NULL ,
  `nnn` VARCHAR(50) NULL ,
  `annn` VARCHAR(50) NULL ,
  `sca` VARCHAR(50) NULL ,
  `timestamp` TIMESTAMP NULL ,
  `error_code` INT NULL ,
  `no_reply` TINYINT(1)  NULL ,
  `tcap_dialogue_error_code` INT NULL ,
  `tcap_abort_error_code` INT NULL ,
  `gsmc` VARCHAR(150) NULL ,
  `gsmc_nai` INT NULL ,
  `vmsc` VARCHAR(150) NULL ,
  `vmsc_nai` INT NULL ,
  `roaming_number` VARCHAR(150) NULL ,
  `roaming_number_nai` INT NULL ,
  `forwarded_to_number` VARCHAR(150) NULL ,
  `forwarded_to_number_nai` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_sri_sri_type1` (`sri_type_id` ASC) ,
  CONSTRAINT `fk_sri_sri_type1`
    FOREIGN KEY (`sri_type_id` )
    REFERENCES `sri_type` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `logging`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `logging` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `action` VARCHAR(300) NOT NULL ,
  `details` TEXT NULL ,
  `user` VARCHAR(32) NOT NULL ,
  `timestamp` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(32) NOT NULL ,
  `password` VARCHAR(32) NOT NULL ,
  `valid_to` TIMESTAMP NULL ,
  `timestamp` TIMESTAMP NULL ,
  `email` VARCHAR(100) NULL ,
  `home_phone` VARCHAR(45) NULL ,
  `office_phone` VARCHAR(45) NULL ,
  `cell_phone` VARCHAR(45) NULL ,
  `address` VARCHAR(300) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GSMMAPLocalErrorcode`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `GSMMAPLocalErrorcode` (
  `code` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`code`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TCAPDialogueError`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TCAPDialogueError` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NULL ,
  PRIMARY KEY (`code`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TCAPP_AbortCause`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `TCAPP_AbortCause` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NULL ,
  PRIMARY KEY (`code`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `filter_simulation`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `filter_simulation` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `smsfs_data` TEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `filter_simulation_data`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `filter_simulation_data` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `sms_id` INT NOT NULL ,
  `filter_simulation_id` INT NOT NULL ,
  `filter_action_id` INT NOT NULL ,
  `filter_total_score` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_filter_simulation_filter_action1` (`filter_action_id` ASC) ,
  INDEX `fk_filter_simulation_data_filter_simulation1` (`filter_simulation_id` ASC) ,
  INDEX `fk_filter_simulation_data_sms1` (`sms_id` ASC) ,
  CONSTRAINT `fk_filter_simulation_filter_action1`
    FOREIGN KEY (`filter_action_id` )
    REFERENCES `filter_action` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_filter_simulation_data_filter_simulation1`
    FOREIGN KEY (`filter_simulation_id` )
    REFERENCES `filter_simulation` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_filter_simulation_data_sms1`
    FOREIGN KEY (`sms_id` )
    REFERENCES `sms` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `quarantine`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `quarantine` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `sms_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_quarantine_sms1` (`sms_id` ASC) ,
  CONSTRAINT `fk_quarantine_sms1`
    FOREIGN KEY (`sms_id` )
    REFERENCES `sms` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `role`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `role` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users_role`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `users_role` (
  `users_id` INT NOT NULL ,
  `role_id` INT NOT NULL ,
  PRIMARY KEY (`users_id`, `role_id`) ,
  INDEX `fk_users_has_role_role1` (`role_id` ASC) ,
  CONSTRAINT `fk_users_has_role_users1`
    FOREIGN KEY (`users_id` )
    REFERENCES `users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_role_role1`
    FOREIGN KEY (`role_id` )
    REFERENCES `role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsfs_list`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `smsfs_list` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(300) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `smsfs_list_item`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `smsfs_list_item` (
  `smsfs_list_id` INT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  INDEX `fk_smsfs_list_item_smsfs_list1` (`smsfs_list_id` ASC) ,
  PRIMARY KEY (`smsfs_list_id`, `name`) ,
  CONSTRAINT `fk_smsfs_list_item_smsfs_list1`
    FOREIGN KEY (`smsfs_list_id` )
    REFERENCES `smsfs_list` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `voice`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `voice` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `dpc` INT NULL ,
  `opc` INT NULL ,
  `cic` INT NULL ,
  `called_party` VARCHAR(45) NULL ,
  `called_party_nai` INT NULL ,
  `calling_party` VARCHAR(45) NULL ,
  `calling_party_nai` INT NULL ,
  `ringing_duration` BIGINT NULL ,
  `call_duration` BIGINT NULL ,
  `release_cause_code` INT NULL ,
  `release_cause_class_code` INT NULL ,
  `timestamp` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `release_class`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `release_class` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`code`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `release_cause`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `release_cause` (
  `code` INT NOT NULL ,
  `release_class_code` INT NOT NULL ,
  `name` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`code`, `release_class_code`) ,
  INDEX `fk_release_cause_release_class1` (`release_class_code` ASC) ,
  CONSTRAINT `fk_release_cause_release_class1`
    FOREIGN KEY (`release_class_code` )
    REFERENCES `release_class` (`code` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `nai`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `nai` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`code`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `SMPPErrorCode`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `SMPPErrorCode` (
  `code` INT NOT NULL ,
  `name` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`code`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `ss7_smpp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ss7_smpp` (
  `error_code_smpp` INT NOT NULL ,
  `error_code_ss7` INT NOT NULL ,
  PRIMARY KEY (`error_code_smpp`, `error_code_ss7`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `smpp_users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `smpp_users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `smpp_system_id` VARCHAR(45) NOT NULL ,
  `smpp_password` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `direction`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `direction` (`id`, `name`) VALUES (1, 'MO');
INSERT INTO `direction` (`id`, `name`) VALUES (2, 'MT');

COMMIT;

-- -----------------------------------------------------
-- Data for table `type`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `type` (`id`, `name`) VALUES (1, 'SINGLE');
INSERT INTO `type` (`id`, `name`) VALUES (2, 'CONCATENATED');
INSERT INTO `type` (`id`, `name`) VALUES (3, 'UNKNOWN');

COMMIT;

-- -----------------------------------------------------
-- Data for table `mode`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `mode` (`id`, `name`) VALUES (1, 'INTRUSIVE');
INSERT INTO `mode` (`id`, `name`) VALUES (2, 'NON INTRUSIVE');

COMMIT;

-- -----------------------------------------------------
-- Data for table `filter_action`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `filter_action` (`id`, `name`) VALUES (1, 'ACCEPTED');
INSERT INTO `filter_action` (`id`, `name`) VALUES (2, 'REJECTED');

COMMIT;

-- -----------------------------------------------------
-- Data for table `SmsEncoding`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (0, 'DEFAULT');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (4, '8bit');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (8, 'UCS2');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (12, 'RESERVED');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1000, 'DEFAULT');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1001, 'IA5_ASCII');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1002, '_8BIT_BINARY_1');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1003, 'ISO_8859_1');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1004, '_8BIT_BINARY_2');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1005, 'JIS');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1006, 'ISO_8859_5');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1007, 'ISO_8859_8');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1008, 'UCS2');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1009, 'PICTOGRAM');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1010, 'ISO_2011_JP');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1013, 'EXTENDED_KANJI');
INSERT INTO `SmsEncoding` (`id`, `name`) VALUES (1014, 'KS_C_5601');

COMMIT;

-- -----------------------------------------------------
-- Data for table `TypeOfNumber`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (0, 'UNKNOWN');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (16, 'INTERNATIONAL');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (32, 'NATIONAL');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (48, 'NETWORK SPECIFIC');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (64, 'SUBSCRIBER NUMBER');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (80, 'ALPHANUMERIC');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (96, 'ABBREVIATED');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (112, 'RESERVED');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1000, 'UNKNOWN');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1001, 'INTERNATIONAL');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1002, 'NATIONAL');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1003, 'NETWORK_SPECIFIC');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1004, 'SUBSCRIBER_NUMBER');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1005, 'ALPHANUMERIC');
INSERT INTO `TypeOfNumber` (`id`, `name`) VALUES (1006, 'ABBREVIATED');

COMMIT;

-- -----------------------------------------------------
-- Data for table `pdu_type`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `pdu_type` (`id`, `name`) VALUES (1, 'SMS-TPDU');
INSERT INTO `pdu_type` (`id`, `name`) VALUES (2, 'SMPP-PDU');

COMMIT;

-- -----------------------------------------------------
-- Data for table `sms_status`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `sms_status` (`id`, `name`) VALUES (1, 'OK');
INSERT INTO `sms_status` (`id`, `name`) VALUES (2, 'ERROR_TCAP_ABORT');
INSERT INTO `sms_status` (`id`, `name`) VALUES (3, 'ERROR_TCAP_DIALOGUE');
INSERT INTO `sms_status` (`id`, `name`) VALUES (4, 'ERROR_MAP');
INSERT INTO `sms_status` (`id`, `name`) VALUES (5, 'SMPP_ERROR');

COMMIT;

-- -----------------------------------------------------
-- Data for table `sri_type`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `sri_type` (`id`, `name`) VALUES (1, 'SM - Short message');
INSERT INTO `sri_type` (`id`, `name`) VALUES (2, 'CH - Call handling');

COMMIT;

-- -----------------------------------------------------
-- Data for table `users`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `users` (`id`, `username`, `password`, `valid_to`, `timestamp`, `email`, `home_phone`, `office_phone`, `cell_phone`, `address`) VALUES (1, 'dfranusic', 'c5b3b0e1ec42c72c913edc97ec6c9c7f', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `GSMMAPLocalErrorcode`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (34, 'systemFailure');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (35, 'dataMissing');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (36, 'unexpectedDataValue');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (21, 'facilityNotSupported');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (28, 'incompatibleTerminal');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (51, 'resourceLimitation');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (1, 'unknownSubscriber');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (44, 'numberChanged');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (3, 'unknownMSC');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (5, 'unidentifiedSubscriber');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (7, 'unknownEquipment');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (8, 'roamingNotAllowed');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (9, 'illegalSubscriber');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (12, 'illegalEquipment');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (10, 'bearerServiceNotProvisioned');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (11, 'teleserviceNotProvisioned');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (25, 'noHandoverNumberAvailable');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (26, 'subsequentHandoverFailure');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (42, 'targetCellOutsideGroupCallArea');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (40, 'tracingBufferFull');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (39, 'noRoamingNumberAvailable');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (27, 'absentSubscriber');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (45, 'busySubscriber');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (46, 'noSubscriberReply');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (13, 'callBarred');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (14, 'forwardingViolation');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (47, 'forwardingFailed');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (15, 'cug-Reject');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (48, 'or-NotAllowed');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (49, 'ati-NotAllowed');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (60, 'atsi-NotAllowed');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (61, 'atm-NotAllowed');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (62, 'informationNotAvailabl');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (16, 'illegalSS-Operation');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (17, 'ss-ErrorStatus');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (18, 'ss-NotAvailable');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (19, 'ss-SubscriptionViolatio');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (20, 'ss-Incompatibility');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (71, 'unknownAlphabe');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (72, 'ussd-Busy');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (37, 'pw-RegistrationFailur');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (38, 'negativePW-Check');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (43, 'numberOfPW-AttemptsViolation');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (29, 'shortTermDenial');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (30, 'longTermDenial');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (31, 'subscriberBusyForMT-SMS');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (32, 'sm-DeliveryFailure');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (33, 'messageWaitingListFull');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (6, 'absentSubscriberSM');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (50, 'noGroupCallNumberAvailable');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (52, 'unauthorizedRequestingNetwork');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (53, 'unauthorizedLCSClient');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (54, 'positionMethodFailure');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (58, 'unknownOrUnreachableLCSClient');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (59, 'mm-EventNotSupported');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (4, 'secureTransportError');
INSERT INTO `GSMMAPLocalErrorcode` (`code`, `name`) VALUES (9999, 'NOT INCLUDED');

COMMIT;

-- -----------------------------------------------------
-- Data for table `TCAPDialogueError`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `TCAPDialogueError` (`code`, `name`) VALUES (0, 'null');
INSERT INTO `TCAPDialogueError` (`code`, `name`) VALUES (1, 'no-reason-given');
INSERT INTO `TCAPDialogueError` (`code`, `name`) VALUES (2, 'application-context-name-not-supported/no-common-dialogue-portion');

COMMIT;

-- -----------------------------------------------------
-- Data for table `TCAPP_AbortCause`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `TCAPP_AbortCause` (`code`, `name`) VALUES (0, 'unrecognizedMessageType');
INSERT INTO `TCAPP_AbortCause` (`code`, `name`) VALUES (1, 'unrecognizedTransactionID');
INSERT INTO `TCAPP_AbortCause` (`code`, `name`) VALUES (2, 'badlyFormattedTransactionPortion');
INSERT INTO `TCAPP_AbortCause` (`code`, `name`) VALUES (3, 'incorrectTransactionPortion');
INSERT INTO `TCAPP_AbortCause` (`code`, `name`) VALUES (4, 'resourceLimitation');
INSERT INTO `TCAPP_AbortCause` (`code`, `name`) VALUES (9999, 'NOT INCLUDED');

COMMIT;

-- -----------------------------------------------------
-- Data for table `role`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `role` (`id`, `name`) VALUES (1, 'Administrator');
INSERT INTO `role` (`id`, `name`) VALUES (2, 'User');

COMMIT;

-- -----------------------------------------------------
-- Data for table `smsfs_list`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `smsfs_list` (`id`, `name`, `description`) VALUES (1, 'LD_SPAM', NULL);
INSERT INTO `smsfs_list` (`id`, `name`, `description`) VALUES (2, 'MD5_QUARANTINE', NULL);
INSERT INTO `smsfs_list` (`id`, `name`, `description`) VALUES (3, 'MD5_NORMAL', NULL);
INSERT INTO `smsfs_list` (`id`, `name`, `description`) VALUES (4, 'DICTIONARY', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `release_class`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `release_class` (`code`, `name`) VALUES (0, 'NORMAL_EVENT_1');
INSERT INTO `release_class` (`code`, `name`) VALUES (16, 'NORMAL_EVENT_2');
INSERT INTO `release_class` (`code`, `name`) VALUES (32, 'RESOURCE_UNAVAILBLE');
INSERT INTO `release_class` (`code`, `name`) VALUES (48, 'SERVICE_OR_OPTION_UNAVAILABLE');
INSERT INTO `release_class` (`code`, `name`) VALUES (64, 'SERVICE_OR_OPTION_NOT_IMPLEMENTED');
INSERT INTO `release_class` (`code`, `name`) VALUES (80, 'INVALID_MESSAGE');
INSERT INTO `release_class` (`code`, `name`) VALUES (96, 'PROTOCOL_ERROR');
INSERT INTO `release_class` (`code`, `name`) VALUES (112, 'INTERWORKING');

COMMIT;

-- -----------------------------------------------------
-- Data for table `release_cause`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (1, 0, 'UNALLOCATED_NUMBER');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 0, 'NO_ROUTE_TO_SPECIFIED_TRANSIT_NETWORK');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (3, 0, 'NO_ROUTE_TO_DESTINATION');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (4, 0, 'SEND_SPECIAL_INFORMATION_TONE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (5, 0, 'MISDIALLED_TRUNK_PREFIX');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (6, 0, 'CHANNEL_UNACCEPTABLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (7, 0, 'CALL_AWARDED_BEING_DELIVERED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (8, 0, 'PREEMPTION');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (9, 0, 'PREEMPTION_CIRCUIT_RESERVER');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (0, 16, 'NORMAL_CALL_CLEARING');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (1, 16, 'USER_BUSY');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 16, 'NO_USER_RESPONDING');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (3, 16, 'NO_ANSWER');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (4, 16, 'SUBSCRIBER_ABSENT');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (5, 16, 'CALL_REJECTED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (6, 16, 'NUMBER_CHANGED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (7, 16, 'REDIRECTION');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (9, 16, 'EXCHANGE_ROUTING_ERROR');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (10, 16, 'NON_SELECTED_USER_CLEARIG');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (11, 16, 'DESTINATION_OUT_OF_ORDER');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (12, 16, 'INVALID_NUMBER_FORMAT');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (13, 16, 'FACILITY_REJECTED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (14, 16, 'RESPONSE_TO_STATUS_ENQUIRY');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (15, 16, 'NORMAL_UNSPECIFIED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 32, 'CIRCUIT_OR_CHANNEL_UNAVAILABLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (6, 32, 'NETWORK_OUT_OF_ORDER');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (7, 32, 'PERMANENT_FRAME_MODE_CONNECTION_OUT_OF_SERVICE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (8, 32, 'PERMANENT_FRAME_MODE_CONNECTION_OPERATIONAL');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (9, 32, 'TEMPORARY_FAILURE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (10, 32, 'SWITCHING_EQUIPMENT_CONGESTION');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (11, 32, 'ACCESS_INFORMATION_DISCARDED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (12, 32, 'REQUESTED_CIRCUIT_OR_CHANNEL_UNAVAILABLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (14, 32, 'PRECEDENCE_CALL_BLOCKED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (15, 32, 'RESOURCE_UNAVAILABLE_UNSPECIFIED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (1, 48, 'QUALITY_OF_SERVICE_UNAVAILABLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 48, 'EWQUESTED_FACILITY_NOT_SUBSCRIBED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (5, 48, 'OUTGOING_CALLS_BARRED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (7, 48, 'INCOMING_CALLS_BARRED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (9, 48, 'BEARER_CAPABILITY_NOT_AUTHORIZED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (10, 48, 'BEARER_CAPABILITY_UNAVAILABLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (14, 48, 'INCONSISTENCY_IN_DESIGNATED_OUTGOING_ACCESS_INFORMATION_AND_SUBSCRIBER_CLASS');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (15, 48, 'SERVICE_OR_OPTION_UNAVAILABLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (1, 64, 'BEARER_CAPABILITY_NOT_IMPLEMENTED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 64, 'CHANNEL_TYPE_NOT_IMPLEMENTED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (5, 64, 'REQUESTED_FACILITY_NOT_IMPLEMENTED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (6, 64, 'RESTRICTED_DIGITAL_INFORMATION_BEARED_CAPABILITY');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (15, 64, 'SERVICE_OR_OPTION_NOT_IMPLEMENTED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (1, 80, 'INVALID_CALL_REFERENCE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 80, 'CHANNEl_DOES_NOT_EXIST');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (3, 80, 'CALL_IDENTITY_DOES_NOT_EXIST');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (4, 80, 'CALL_IDENTITY_IN_USE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (5, 80, 'NO_CALL_SUSPENDED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (6, 80, 'CALL_CLEARED_FOR_REQUESTED_CALL_IDENTITY');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (7, 80, 'USER_NOT_MEMBER_OF_CUG');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (8, 80, 'IMCOMPATIBLE_DESTINATION');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (10, 80, 'NON_EXISTENT_CUG');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (11, 80, 'INVALID_TRANSIT_NETWORK_SELECTION');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (15, 80, 'INVALID_MESSAGE_UNSPECIFIED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (0, 96, 'MANDATORY_ELEMENT_MISSING');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (1, 96, 'MESSAGE_TYPE_NON_EXISTENT');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (2, 96, 'MESSAGE_NOT_COMPATIBLE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (3, 96, 'PARAMETER_NON_EXISTENT');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (4, 96, 'INVALID_INFORMATION_ELEMENT_CONTENTS');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (5, 96, 'MESSAGE_NOT_COMATIBLE_WITH_CALL_STATE');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (6, 96, 'RECOVERY_ON_TIMER_EXPIRY');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (7, 96, 'PARAMETER_NON_EXISTENT_PASSED_ON');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (14, 96, 'MESSAGE_WITH_UNRECOGNIZED_PARAMETER_DISCARDED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (15, 96, 'PROTOCOL_ERROR_UNSPECIFIED');
INSERT INTO `release_cause` (`code`, `release_class_code`, `name`) VALUES (0, 112, 'INTERWORKING_UNSPECIFED');

COMMIT;

-- -----------------------------------------------------
-- Data for table `nai`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `nai` (`code`, `name`) VALUES (1, 'SUBSCRIBER_NUMBER');
INSERT INTO `nai` (`code`, `name`) VALUES (2, 'UNKNOWN');
INSERT INTO `nai` (`code`, `name`) VALUES (3, 'NATIONAL');
INSERT INTO `nai` (`code`, `name`) VALUES (4, 'INTERNATIONAL');
INSERT INTO `nai` (`code`, `name`) VALUES (5, 'NETWORK_SPECIFIC');
INSERT INTO `nai` (`code`, `name`) VALUES (6, 'NETWORK_ROUTING_NUMBER_IN_NATIONAL_NUMBER_FORMAT');
INSERT INTO `nai` (`code`, `name`) VALUES (7, 'NETWORK_ROUTING_NUMBER_IN_NETWORK_SPECIFIC_NUMBER_FORMAT');
INSERT INTO `nai` (`code`, `name`) VALUES (8, 'NETWORK_ROUTING_NUMBER_CONCATENATED_WITH_CALLED_DIRECTORY_NUMBER');
INSERT INTO `nai` (`code`, `name`) VALUES (0, 'UNKNOWN');
INSERT INTO `nai` (`code`, `name`) VALUES (16, 'INTERNATIONAL');
INSERT INTO `nai` (`code`, `name`) VALUES (32, 'NATIONAL');
INSERT INTO `nai` (`code`, `name`) VALUES (48, 'NETWORK_SPECIFIC');
INSERT INTO `nai` (`code`, `name`) VALUES (64, 'SUBSCRIBER_NUMBER');
INSERT INTO `nai` (`code`, `name`) VALUES (80, 'ALPHANUMERIC');
INSERT INTO `nai` (`code`, `name`) VALUES (96, 'ABBREVIATED');
INSERT INTO `nai` (`code`, `name`) VALUES (112, 'RESERVED');

COMMIT;

-- -----------------------------------------------------
-- Data for table `SMPPErrorCode`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (0, 'No Error');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (1, 'Message Length is invalid');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (2, 'Command Length is invalid');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (3, 'Invalid Command ID');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (4, 'Incorrect BIND Status for given command');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (5, 'ESME Already in Bound State');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (6, 'Invalid Priority Flag');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (7, 'Invalid Registered Delivery Flag');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (8, 'System Error');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (10, 'Invalid Source Address');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (11, 'Invalid Dest Addr');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (12, 'Message ID is invalid');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (13, 'Bind Failed');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (14, 'Invalid Password');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (15, 'Invalid System ID');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (17, 'Cancel SM Failed');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (19, 'Replace SM Failed');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (20, 'Message Queue Full');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (21, 'Invalid Service Type');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (51, 'Invalid number of destinations');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (52, 'Invalid Distribution List name');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (64, 'Destination flag is invalid (submit_multi)');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (66, 'Invalid ‘submit with replace’ request');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (67, 'Invalid esm_class field data');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (68, 'Cannot Submit to Distribution List');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (69, 'submit_sm or submit_multi failed');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (72, 'Invalid Source address TON');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (73, 'Invalid Source address NPI');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (74, 'Invalid Destination address TON');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (75, 'Invalid Destination address NPI');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (83, 'Invalid system_type field');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (84, 'Invalid replace_if_present flag');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (85, 'Invalid number of messages');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (88, 'Throttling error (ESME has exceeded allowed message limits)');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (97, 'Invalid Scheduled Delivery Time');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (98, 'Invalid message validity period (Expiry time)');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (99, 'Predefined Message Invalid or Not Found');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (100, 'ESME Receiver Temporary App Error Code');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (101, 'ESME Receiver Permanent App Error Code');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (102, 'ESME Receiver Reject Message Error Code');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (103, 'query_sm request failed');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (192, 'Error in the optional part of the PDU Body');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (193, 'Optional Parameter not allowed');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (194, 'Invalid Parameter Length');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (195, 'Expected Optional Parameter missing');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (196, 'Invalid Optional Parameter Value');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (254, 'Delivery Failure');
INSERT INTO `SMPPErrorCode` (`code`, `name`) VALUES (255, 'Unknown Error');

COMMIT;
