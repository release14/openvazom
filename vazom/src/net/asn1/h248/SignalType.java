package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SignalType extends ENUMERATED{

	public static final int _brief = 0;
	public static final int _onOff = 1;
	public static final int _timeOut = 2;
	public SignalType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
