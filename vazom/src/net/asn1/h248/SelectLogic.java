package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SelectLogic extends CHOICE{

	public NULL get_andAUDITSelect(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_andAUDITSelect(){
		return new NULL();
	}

	public NULL get_orAUDITSelect(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_orAUDITSelect(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_andAUDITSelect(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_orAUDITSelect(); }});
	}
	public SelectLogic(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
