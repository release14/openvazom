package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Dialogue_service_userInteger extends INTEGER{

	public static final int _null = 0;
	public static final int _no_reason_given = 1;
	public static final int _application_context_name_not_supported = 2;
	public Dialogue_service_userInteger(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
