package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SecurityParmIndex extends OCTET_STRING{

	public SecurityParmIndex(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
