package net.sctp;

import java.util.ArrayList;


public class SSCTPDescriptor {
	public int sid;
	public byte[] payload;
	public ArrayList<Object> extra_params;
	
	
	public SSCTPDescriptor(int _sid, byte[] _payload){
		sid = _sid;
		payload = _payload;
		
	}
}
