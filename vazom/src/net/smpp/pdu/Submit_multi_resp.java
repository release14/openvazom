package net.smpp.pdu;

import java.util.ArrayList;

import net.smpp.ErrorCodeType;
import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.utils.Utils;

public class Submit_multi_resp extends PDUBase {
	public class UnsuccessSme{
		public TypeOfNumber dest_addr_ton;
		public NumberingPlan dest_addr_npi;
		public String destination_addr;
		public ErrorCodeType error_status_code;
		
	}
	public String message_id;
	public int no_unsuccess;
	public UnsuccessSme[] unsuccess_sme_lst;
	
	public Submit_multi_resp(){
		type = PDUType.SUBMIT_MULTI_RESP;
	}

	public byte[] encode(){
		//command_length += system_id.length() + 1 + (sc_interface_version != null ? sc_interface_version.length : 0);
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		Utils.bytesToLst(buff, Utils.Cstr_encode(message_id));
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		
		return Utils.list2array(buff);

	}	
	
	public void init(byte[] data) {
		UnsuccessSme us = null;
		// System ID
		message_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += message_id.length() + 1;
		// no_unsuccess
		no_unsuccess = data[byte_pos++] & 0xff;
		//unsuccess list
		if(no_unsuccess > 0){
			unsuccess_sme_lst = new UnsuccessSme[no_unsuccess];
			for(int i = 0; i<no_unsuccess; i++){
				us = new UnsuccessSme();
				us.dest_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
				us.dest_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
				us.destination_addr = Utils.Cstr_decode(data, byte_pos);
				byte_pos += us.destination_addr.length() + 1;
				us.error_status_code = ErrorCodeType.get(data[byte_pos++] & 0xff);
			}
			
		}
	}

}
