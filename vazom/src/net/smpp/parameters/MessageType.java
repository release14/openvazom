package net.smpp.parameters;

import java.util.HashMap;

// bits 5-2
// 00XXXX00
public enum MessageType {
	DEFAULT(0x00),
	DELIVERY_ACK(0x08),
	MANUAL_USER_ACK(0x10);

	private int id;
	private static final HashMap<Integer, MessageType> lookup = new HashMap<Integer, MessageType>();
	static{
		for(MessageType td : MessageType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static MessageType get(int id){ return lookup.get(id); }
	private MessageType(int _id){ id = _id; }
}
