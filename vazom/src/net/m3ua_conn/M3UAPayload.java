package net.m3ua_conn;

import net.m3ua.M3UAPacket;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionType;
import net.sctp.SSCTPDescriptor;
import net.vstp.VSTP_SGF_CorrelationPacket;

public class M3UAPayload extends RoutingConnectionPayload{
	public M3UAPacket m3ua_packet;
	public SSCTPDescriptor sd;
	
	
	public M3UAPayload(){
		type = RoutingConnectionType.M3UA;
	}


	public void setParamsFromVSTP_SGF(VSTP_SGF_CorrelationPacket vstp_sgf) {
		sd = vstp_sgf.sd;
		m3ua_packet = vstp_sgf.sri_packet.m3ua_packet;
		callback_required = vstp_sgf.callback_required;
		extra_params = vstp_sgf.extra_params;
		callback = vstp_sgf.callback;
		if(sd != null && m3ua_packet != null) sd.payload = m3ua_packet.encode();

	}


	public void setParams(Object[] params) {
		try{
			sd = (SSCTPDescriptor)params[0];
			m3ua_packet = (M3UAPacket)params[1];
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
