package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StatusReportArg extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public EventReportData get_eventReportData(){
		return (EventReportData)elements.get(1).data;
	}
	public EventReportData new_eventReportData(){
		return new EventReportData();
	}

	public CallReportData get_callReportdata(){
		return (CallReportData)elements.get(2).data;
	}
	public CallReportData new_callReportdata(){
		return new CallReportData();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_eventReportData(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callReportdata(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public StatusReportArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
