package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TrunkStatus extends ENUMERATED{

	public static final int _idle =  0 ;
	public static final int _blocked =  1 ;
	public TrunkStatus(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
