package net.smstpdu.tpdu.udh;

import java.util.ArrayList;
import java.util.Arrays;

import net.utils.Utils;

public class UDH {
	public ArrayList<InformationElement> informationElements;
	public int length;
	
	public UDH(){
		informationElements = new ArrayList<InformationElement>();
		
		
		
	}
	public IE_ConcatenatedReference findIERef(){
		IE_ConcatenatedReference ref = null;
		for(int i = 0; i<informationElements.size(); i++) 
			if(informationElements.get(i).type == InformationElementType.CONCATENATED_16bit || informationElements.get(i).type == InformationElementType.CONCATENATED_8bit){
				return (IE_ConcatenatedReference)informationElements.get(i);
			
		}
		return ref;
	
	}

	public int findIE(InformationElementType type){
		for(int i = 0; i<informationElements.size(); i++) if(informationElements.get(i).type == type){
			return i;
			
		}
		return -1;
		
	}
	public byte[] encode(){
		ArrayList<Byte> buff = new ArrayList<Byte>();
		buff.add((byte)0); // length, calculated later
		for(int i = 0; i<informationElements.size(); i++) Utils.bytesToLst(buff, informationElements.get(i).encode());
		buff.set(0, (byte)(buff.size() - 1));
		return Utils.list2array(buff);
		
	}
	public void init(byte[] data){
		int i = 0;
		int l = 0;
		InformationElement iec;
		while(i < data.length){
			//System.out.println(data[i]);
			//System.out.println("i: " + i);
			if(InformationElementType.get(data[i] & 0xFF) != null){
				switch(InformationElementType.get(data[i] & 0xFF)){
					case CONCATENATED_8bit:
						iec = new IE_Concatenated_8bit();
						iec.length = data[i+1] & 0xFF;
						if(iec.length > 0) iec.init(Arrays.copyOfRange(data, i+2, i+2 + iec.length));
						informationElements.add(iec);
						break;
					case CONCATENATED_16bit:
						iec = new IE_Concatenated_16bit();
						iec.length = data[i+1] & 0xFF;
						if(iec.length > 0) iec.init(Arrays.copyOfRange(data, i+2, i+2 + iec.length));
						informationElements.add(iec);
						
			
				}
			}
			i += 2 + (data[i+1] & 0xFF);
			
			
		}
		
	}
}
