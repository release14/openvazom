package net.smstpdu.tppid;

import java.util.HashMap;


public enum TPPID_Format {
	DEFAULT1(0x00),
	DEFAULT2(0x40),
	RESERVED(0x80),
	SPECIFIC(0xC0);
	
	
	// BITS 7 and 6 = XX000000
	private int id;
	private static final HashMap<Integer, TPPID_Format> lookup = new HashMap<Integer, TPPID_Format>();
	static{
		for(TPPID_Format td : TPPID_Format.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static TPPID_Format get(int id){ return lookup.get(id); }
	private TPPID_Format(int _id){ id = _id; }

}
