package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class EventBufferControl extends ENUMERATED{

	public static final int _off = 0;
	public static final int _lockStep = 1;
	public EventBufferControl(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
