package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Signal extends SEQUENCE{

	public SignalName get_signalName(){
		return (SignalName)elements.get(0).data;
	}
	public SignalName new_signalName(){
		return new SignalName();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(1).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public SignalType get_sigType(){
		return (SignalType)elements.get(2).data;
	}
	public SignalType new_sigType(){
		return new SignalType();
	}

	public INTEGER get_duration(){
		return (INTEGER)elements.get(3).data;
	}
	public INTEGER new_duration(){
		return new INTEGER();
	}

	public NotifyCompletion get_notifyCompletion(){
		return (NotifyCompletion)elements.get(4).data;
	}
	public NotifyCompletion new_notifyCompletion(){
		return new NotifyCompletion();
	}

	public BOOLEAN get_keepActive(){
		return (BOOLEAN)elements.get(5).data;
	}
	public BOOLEAN new_keepActive(){
		return new BOOLEAN();
	}

	public SigParameterSequenceOf get_sigParList(){
		return (SigParameterSequenceOf)elements.get(6).data;
	}
	public SigParameterSequenceOf new_sigParList(){
		return new SigParameterSequenceOf();
	}

	public SignalDirection get_direction(){
		return (SignalDirection)elements.get(7).data;
	}
	public SignalDirection new_direction(){
		return new SignalDirection();
	}

	public RequestID get_requestID(){
		return (RequestID)elements.get(8).data;
	}
	public RequestID new_requestID(){
		return new RequestID();
	}

	public INTEGER get_intersigDelay(){
		return (INTEGER)elements.get(9).data;
	}
	public INTEGER new_intersigDelay(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sigType(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_duration(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_notifyCompletion(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_keepActive(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sigParList(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_direction(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_requestID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_intersigDelay(); }});
	}
	public Signal(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
