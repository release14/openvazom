package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PSID_RSIDList extends SEQUENCE{

	public PSID_RSIDInformation get_pSID_RSIDInformation(){
		return (PSID_RSIDInformation)elements.get(0).data;
	}
	public PSID_RSIDInformation new_pSID_RSIDInformation(){
		return new PSID_RSIDInformation();
	}

	public PSID_RSIDInformation get_pSID_RSIDInformation1(){
		return (PSID_RSIDInformation)elements.get(1).data;
	}
	public PSID_RSIDInformation new_pSID_RSIDInformation1(){
		return new PSID_RSIDInformation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(202, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_pSID_RSIDInformation(); }});
		elements.add(new ElementDescriptor(202, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pSID_RSIDInformation1(); }});
	}
	public PSID_RSIDList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
