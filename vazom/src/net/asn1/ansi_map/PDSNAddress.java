package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PDSNAddress extends OCTET_STRING{

	public PDSNAddress(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
