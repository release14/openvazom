package net.sctp;

import java.util.Arrays;

import net.sctp.chunk.ChunkBase;
import net.sctp.chunk.DATA;


public class SCTP {

	public static SCTPPacket decode(byte[] data){
		int byte_pos = 0;
		int m = 0;
		SCTPPacket res = new SCTPPacket();
		ChunkBase chunk = null;
		ChunkType ct = null;
		res.sourcePort = (data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF) & 0xFFFF;
		res.destinationPort = (data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF) & 0xFFFF;
		res.verificationTag = Arrays.copyOfRange(data, byte_pos, byte_pos + 4);
		byte_pos += 4;
		res.checksum = Arrays.copyOfRange(data, byte_pos, byte_pos + 4);
		byte_pos += 4;
		
		// chunks
		while(byte_pos < data.length){
			//System.out.println(data[byte_pos] & 0xff);
			ct = ChunkType.get(data[byte_pos] & 0xFF);
			if(ct != null){
				switch(ct){
					case DATA: chunk = new DATA(); break;
					// TODO 
					// other chunk types
					default: chunk = new ChunkBase(); break;
					
				}	
		
			}else chunk = new ChunkBase();
			//System.out.println(chunk.type);
			//System.out.println(chunk.getLength(data, byte_pos));
			//System.out.println(ChunkType.get(data[byte_pos] & 0xFF));
			chunk.decode(Arrays.copyOfRange(data, byte_pos, chunk.getLength(data, byte_pos) + byte_pos));
			// minimum chunk length is 4( no data), should be more then 4 if any 
			// data is present
			//if(chunk.length >= 4){
			res.chunks.add(chunk);
			byte_pos += chunk.length;

			// chunk has to be a multiple of 4, if not, zero padding is added
			m = chunk.length % 4;
			byte_pos += (m > 0 ? 4 - m : 0);
					
			
			
		}
		
		return res;
	
	}	
}
