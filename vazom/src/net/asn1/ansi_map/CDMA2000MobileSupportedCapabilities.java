package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMA2000MobileSupportedCapabilities extends OCTET_STRING{

	public CDMA2000MobileSupportedCapabilities(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
