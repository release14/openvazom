package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceDataAccessElement extends SEQUENCE{

	public DataAccessElementList get_dataAccessElementList(){
		return (DataAccessElementList)elements.get(0).data;
	}
	public DataAccessElementList new_dataAccessElementList(){
		return new DataAccessElementList();
	}

	public ServiceID get_serviceID(){
		return (ServiceID)elements.get(1).data;
	}
	public ServiceID new_serviceID(){
		return new ServiceID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(250, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataAccessElementList(); }});
		elements.add(new ElementDescriptor(246, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceID(); }});
	}
	public ServiceDataAccessElement(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
