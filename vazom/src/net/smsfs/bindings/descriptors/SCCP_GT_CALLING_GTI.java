package net.smsfs.bindings.descriptors;

import net.smsfs.SmsfsPacket;
import net.smsfs.bindings.BindingDescriptor;

public class SCCP_GT_CALLING_GTI extends BindingDescriptor {
	public SCCP_GT_CALLING_GTI(){
		super(22);
	}
	public SCCP_GT_CALLING_GTI(int index){
		super(index);
	}
	public void modify(SmsfsPacket fp, String newValue) {
		// Not available
	}

}
