package net.isup.parameters;

import net.isup.ParameterType;

public class TransmissionMediumUsed extends ParameterBase {
	public TransmissionMediumUsed(){
		type = ParameterType.TRANSMISSION_MEDIUM_USED;
	}
}
