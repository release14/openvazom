package net.isup.parameters;

import net.isup.ParameterType;

public class CallOfferingTreatmentIndicators extends ParameterBase {
	public CallOfferingTreatmentIndicators(){
		type = ParameterType.CALL_OFFERING_TREATMENT_INDICATORS;
	}
}
