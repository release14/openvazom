package net.sccp.parameters;

public class LongData extends ParamBase {
	public byte[] value;
	
	public LongData(){
		super();
		type = ParamType.LONG_DATA;
	}
	
	public void init(byte[] data) {
		value = data;
	}

}
