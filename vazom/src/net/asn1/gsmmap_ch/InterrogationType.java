package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class InterrogationType extends ENUMERATED{

	public static final int _basicCall = 0;
	public static final int _forwarding = 1;
	public InterrogationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
