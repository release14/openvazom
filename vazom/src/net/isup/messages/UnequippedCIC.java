package net.isup.messages;

import net.isup.MessageType;

public class UnequippedCIC extends MessageBase {
	public UnequippedCIC(){
		super();
		type = MessageType.UNEQUIPPED_CIC;
	}

	public void init(byte[] data) {
		// no data
	}

}
