package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Reason extends CHOICE{

	public P_AbortCause get_p_abortCause(){
		return (P_AbortCause)elements.get(0).data;
	}
	public P_AbortCause new_p_abortCause(){
		return new P_AbortCause();
	}

	public DialoguePortion get_u_abortCause(){
		return (DialoguePortion)elements.get(1).data;
	}
	public DialoguePortion new_u_abortCause(){
		return new DialoguePortion();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(10, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_p_abortCause(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_u_abortCause(); }});
	}
	public Reason(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
