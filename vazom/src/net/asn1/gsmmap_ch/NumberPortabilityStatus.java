package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NumberPortabilityStatus extends ENUMERATED{

	public static final int _notKnownToBePorted = 0;
	public static final int _ownNumberPortedOut = 1;
	public static final int _foreignNumberPortedToForeignNetwork = 2;
	public static final int _ownNumberNotPortedOut = 4;
	public static final int _foreignNumberPortedIn = 5;
	public NumberPortabilityStatus(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
