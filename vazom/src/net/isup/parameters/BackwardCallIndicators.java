package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.bci.CalledPartyCategoryIndicatorType;
import net.isup.parameters.bci.CalledPartyStatusIndicatorType;
import net.isup.parameters.bci.ChargeIndicatorType;
import net.isup.parameters.bci.EndToEndMethodIndicatorType;
import net.isup.parameters.bci.SCCPMethodIndicatorType;

public class BackwardCallIndicators extends ParameterBase {
	public ChargeIndicatorType charge_indicator;
	public CalledPartyStatusIndicatorType called_party_status_indicator;
	public CalledPartyCategoryIndicatorType called_party_category_indicator;
	public EndToEndMethodIndicatorType end_to_end_method_indicator;
	public boolean interworking_indicator;
	public boolean end_to_end_information_indicator;
	public boolean isdn_user_part_indicator;
	public boolean holding_indicator;
	public boolean isdn_access_indicator;
	public boolean echo_control_device_indicator;
	public SCCPMethodIndicatorType sccp_method_indicator;
	
	public BackwardCallIndicators(){
		type = ParameterType.BACKWARD_CALL_INDICATORS;
	}

	public void init(byte[] data) {
		
		if(data != null){
			charge_indicator = ChargeIndicatorType.get(data[byte_position] & 0x03);
			called_party_status_indicator = CalledPartyStatusIndicatorType.get(data[byte_position] & 0x0c);
			called_party_category_indicator = CalledPartyCategoryIndicatorType.get(data[byte_position] & 0x30);
			end_to_end_method_indicator = EndToEndMethodIndicatorType.get(data[byte_position] & 0xc0);
			byte_position++;
			interworking_indicator = (data[byte_position] & 0x01) == 0x01;
			end_to_end_information_indicator = (data[byte_position] & 0x02) == 0x02;
			isdn_user_part_indicator = (data[byte_position] & 0x04) == 0x04;
			holding_indicator = (data[byte_position] & 0x08) == 0x08;
			isdn_access_indicator = (data[byte_position] & 0x10) == 0x10;
			echo_control_device_indicator = (data[byte_position] & 0x20) == 0x20;
			sccp_method_indicator = SCCPMethodIndicatorType.get(data[byte_position] & 0xc0);
		}
	}

}
