package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NotifyReply extends SEQUENCE{

	public TerminationIDList get_terminationID(){
		return (TerminationIDList)elements.get(0).data;
	}
	public TerminationIDList new_terminationID(){
		return new TerminationIDList();
	}

	public ErrorDescriptor get_errorDescriptor(){
		return (ErrorDescriptor)elements.get(1).data;
	}
	public ErrorDescriptor new_errorDescriptor(){
		return new ErrorDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_errorDescriptor(); }});
	}
	public NotifyReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
