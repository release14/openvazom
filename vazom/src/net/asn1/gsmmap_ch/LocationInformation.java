package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocationInformation extends SEQUENCE{

	public AgeOfLocationInformation get_ageOfLocationInformation(){
		return (AgeOfLocationInformation)elements.get(0).data;
	}
	public AgeOfLocationInformation new_ageOfLocationInformation(){
		return new AgeOfLocationInformation();
	}

	public GeographicalInformation get_geographicalInformation(){
		return (GeographicalInformation)elements.get(1).data;
	}
	public GeographicalInformation new_geographicalInformation(){
		return new GeographicalInformation();
	}

	public ISDN_AddressString get_vlr_number(){
		return (ISDN_AddressString)elements.get(2).data;
	}
	public ISDN_AddressString new_vlr_number(){
		return new ISDN_AddressString();
	}

	public LocationNumber get_locationNumber(){
		return (LocationNumber)elements.get(3).data;
	}
	public LocationNumber new_locationNumber(){
		return new LocationNumber();
	}

	public CellGlobalIdOrServiceAreaIdOrLAI get_cellGlobalIdOrServiceAreaIdOrLAI(){
		return (CellGlobalIdOrServiceAreaIdOrLAI)elements.get(4).data;
	}
	public CellGlobalIdOrServiceAreaIdOrLAI new_cellGlobalIdOrServiceAreaIdOrLAI(){
		return new CellGlobalIdOrServiceAreaIdOrLAI();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(5).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public LSAIdentity get_selectedLSA_Id(){
		return (LSAIdentity)elements.get(6).data;
	}
	public LSAIdentity new_selectedLSA_Id(){
		return new LSAIdentity();
	}

	public ISDN_AddressString get_msc_Number(){
		return (ISDN_AddressString)elements.get(7).data;
	}
	public ISDN_AddressString new_msc_Number(){
		return new ISDN_AddressString();
	}

	public GeodeticInformation get_geodeticInformation(){
		return (GeodeticInformation)elements.get(8).data;
	}
	public GeodeticInformation new_geodeticInformation(){
		return new GeodeticInformation();
	}

	public NULL get_currentLocationRetrieved(){
		return (NULL)elements.get(9).data;
	}
	public NULL new_currentLocationRetrieved(){
		return new NULL();
	}

	public NULL get_sai_Present(){
		return (NULL)elements.get(10).data;
	}
	public NULL new_sai_Present(){
		return new NULL();
	}

	public LocationInformationEPS get_locationInformationEPS(){
		return (LocationInformationEPS)elements.get(11).data;
	}
	public LocationInformationEPS new_locationInformationEPS(){
		return new LocationInformationEPS();
	}

	public UserCSGInformation get_userCSGInformation(){
		return (UserCSGInformation)elements.get(12).data;
	}
	public UserCSGInformation new_userCSGInformation(){
		return new UserCSGInformation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_ageOfLocationInformation(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_geographicalInformation(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_vlr_number(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationNumber(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cellGlobalIdOrServiceAreaIdOrLAI(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_selectedLSA_Id(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_msc_Number(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_geodeticInformation(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_currentLocationRetrieved(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_sai_Present(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_locationInformationEPS(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_userCSGInformation(); }});
	}
	public LocationInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
