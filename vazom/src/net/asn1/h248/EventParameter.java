package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventParameter extends SEQUENCE{

	public EventParameterName get_eventParameterName(){
		return (EventParameterName)elements.get(0).data;
	}
	public EventParameterName new_eventParameterName(){
		return new EventParameterName();
	}

	public EventParamValues get_eventParamValue(){
		return (EventParamValues)elements.get(1).data;
	}
	public EventParamValues new_eventParamValue(){
		return new EventParamValues();
	}

	public EventParameterExtraInfoChoiceWrapper get_extraInfo(){
		return (EventParameterExtraInfoChoiceWrapper)elements.get(2).data;
	}
	public EventParameterExtraInfoChoiceWrapper new_extraInfo(){
		return new EventParameterExtraInfoChoiceWrapper();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventParameterName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventParamValue(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extraInfo(); }});
	}
	public EventParameter(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
