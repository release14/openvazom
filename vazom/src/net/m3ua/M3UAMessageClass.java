package net.m3ua;

import java.util.HashMap;


public enum M3UAMessageClass {
	MGMT(0x00),
	TRANSFER(0x01),
	SSNM(0x02),
	ASPSM(0x03),
	ASPTM(0x04),
	RKM(0x09);
	
	
	private int id;
	private static final HashMap<Integer, M3UAMessageClass> lookup = new HashMap<Integer, M3UAMessageClass>();
	static{
		for(M3UAMessageClass td : M3UAMessageClass.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static M3UAMessageClass get(int id){ return lookup.get(id); }
	private M3UAMessageClass(int _id){ id = _id; }	
}
