package net.stats;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Calendar;

import net.config.FNConfigData;
import net.utils.Utils;

import org.apache.log4j.Logger;

public class StatsMonitor {
	private static class StatsMon_r implements Runnable{
		StatsMonitorRecord sr = null;
		StatsMonitorRecord tmp_record;
		Calendar tmp_c = null;
		String line = null;
		int line_counter = 0;
		public void run() {
			logger.info("STARTING!");
			// initial value
			tmp_record = new StatsMonitorRecord();
			tmp_record.mo = StatsManager.DECODER_STATS.MO_COUNT;
			tmp_record.mt = StatsManager.DECODER_STATS.MT_COUNT;
			tmp_record.sms_deliver = StatsManager.DECODER_STATS.SMS_DELIVER_COUNT;
			tmp_record.sms_submit = StatsManager.DECODER_STATS.SMS_SUBMIT_COUNT;
			while(!stopping){
				// new recotd
				sr = new StatsMonitorRecord();
				sr.mo = StatsManager.DECODER_STATS.MO_COUNT - tmp_record.mo;
				sr.mt = StatsManager.DECODER_STATS.MT_COUNT - tmp_record.mt;
				sr.sms_deliver = StatsManager.DECODER_STATS.SMS_DELIVER_COUNT - tmp_record.sms_deliver;
				sr.sms_submit = StatsManager.DECODER_STATS.SMS_SUBMIT_COUNT - tmp_record.sms_submit;
				tmp_c = Calendar.getInstance();
				// new base value
				tmp_record.mo = StatsManager.DECODER_STATS.MO_COUNT;
				tmp_record.mt = StatsManager.DECODER_STATS.MT_COUNT;
				tmp_record.sms_deliver = StatsManager.DECODER_STATS.SMS_DELIVER_COUNT;
				tmp_record.sms_submit = StatsManager.DECODER_STATS.SMS_SUBMIT_COUNT;
				// prepare line
				line = Utils.date2str(tmp_c) + ":" + String.format("%02d", tmp_c.get(Calendar.HOUR_OF_DAY)) + "-" +
					String.format("%02d", tmp_c.get(Calendar.MINUTE)) + "-" +
					String.format("%02d", tmp_c.get(Calendar.SECOND)) + ":" +
					sr.mo + "-" + sr.mt + "-" + sr.sms_submit + "-" + sr.sms_deliver;
				// write line
				out.println(line);
				out.flush();
				line_counter++;
				// if stats monitor buffer is full
				if(line_counter >= FNConfigData.stats_monitor_buffer){
					try{
						line = in.readLine();
						logger.info("Resetting StatsMonitor statistics, purging values for [ " + line.substring(0, 10) + " ]!");
						out.close();
						in.close();
						// purge oldest date
						purge(line.substring(0, 10));
						init_file();
						
					}catch(Exception e){
						e.printStackTrace();
					}
					init_file();
					line_counter = 0;
				}
				try{ Thread.sleep(1000); }catch(Exception e){e.printStackTrace();}
			}			
			logger.info("ENDING!");
		}
		
		
	}
	private static Logger logger=Logger.getLogger(StatsMonitor.class);
	private static StatsMon_r stats_mon_r;
	private static Thread stats_mon_t;
	private static boolean stopping;
	private static File stats_file = null;
	private static PrintWriter out = null;
	private static BufferedReader in = null;
		

	private static void purge(String s){
		File f = new File("tmp_stats.dat");
		File f2 = new File("stats.dat");
		String l = null;
		try{
			PrintWriter n_out = new PrintWriter(f);
			BufferedReader o_in = new BufferedReader(new InputStreamReader(new FileInputStream(f2)));
			while((l = o_in.readLine()) != null){
				if(!l.substring(0, 10).equals(s)){
					n_out.println(l);
					n_out.flush();
				}
			}
			n_out.close();
			o_in.close();
			f2.delete();
			f.renameTo(new File("stats.dat"));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	private static void init_file(){
		stats_file = new File("stats.dat");
		try{
			out = new PrintWriter(stats_file);
			in = new BufferedReader(new InputStreamReader(new FileInputStream(stats_file)));
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public static void init(){
		logger.info("Starting Stats Monitor module...");
		logger.info("Stats Monitor buffer set to [ " + FNConfigData.stats_monitor_buffer / 86400 + " days ]!");
		init_file();
		
		stats_mon_r = new StatsMon_r();
		stats_mon_t = new Thread(stats_mon_r, "STATS_MONITOR");
		stats_mon_t.start();
		
	}
		
}
