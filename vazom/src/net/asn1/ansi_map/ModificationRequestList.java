package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModificationRequestList extends SEQUENCE{

	public ModificationRequestType new_child(){
		return new ModificationRequestType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ModificationRequestType getChild(int index){
		return (ModificationRequestType)of_children.get(index);
	}
	public ModificationRequestList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
