package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARE_apdu extends SEQUENCE{

	public BIT_STRING get_protocol_version(){
		return (BIT_STRING)elements.get(0).data;
	}
	public BIT_STRING new_protocol_version(){
		return new BIT_STRING();
	}

	public OBJECT_IDENTIFIER get_application_context_name(){
		return (OBJECT_IDENTIFIER)elements.get(1).data;
	}
	public OBJECT_IDENTIFIER new_application_context_name(){
		return new OBJECT_IDENTIFIER();
	}

	public Associate_result get_result(){
		return (Associate_result)elements.get(2).data;
	}
	public Associate_result new_result(){
		return new Associate_result();
	}

	public Associate_source_diagnostic get_result_source_diagnostic(){
		return (Associate_source_diagnostic)elements.get(3).data;
	}
	public Associate_source_diagnostic new_result_source_diagnostic(){
		return new Associate_source_diagnostic();
	}

	public User_information get_user_information(){
		return (User_information)elements.get(4).data;
	}
	public User_information new_user_information(){
		return new User_information();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_protocol_version(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_application_context_name(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_result(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_result_source_diagnostic(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_user_information(); }});
	}
	public AARE_apdu(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 1;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.APPLICATION;
	}
}
