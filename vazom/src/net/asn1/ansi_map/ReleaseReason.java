package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReleaseReason extends ENUMERATED{

	public static final int _unspecified = 0;
	public static final int _callOverClearForward = 1;
	public static final int _callOverClearBackward = 2;
	public static final int _handoffSuccessful = 3;
	public static final int _handoffAbort_call_over = 4;
	public static final int _handoffAbort_not_received = 5;
	public static final int _abnormalMobileTermination = 6;
	public static final int _abnormalSwitchTermination = 7;
	public static final int _specialFeatureRelease = 8;
	public static final int _sessionOverClearForward = 9;
	public static final int _sessionOverClearBackward = 10;
	public static final int _clearAllServicesForward = 11;
	public static final int _clearAllServicesBackward = 12;
	public static final int _anchor_MSC_was_removed_from_the_packet_data_session = 13;
	public static final int _keep_MS_on_traffic_channel = 14;
	public ReleaseReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
