package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ModulusValue extends OCTET_STRING{

	public ModulusValue(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
