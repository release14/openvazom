package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallDirection extends OCTET_STRING{

	public CallDirection(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
