package net.isup.messages;

import java.util.ArrayList;
import java.util.Arrays;

import net.isup.MessageType;
import net.isup.ParameterType;
import net.isup.Pointer;
import net.isup.parameters.ParameterBase;


public abstract class MessageBase {
	public MessageType type;
	protected boolean opt_done = false;
	protected int byte_pos = 0;
	protected Pointer[] pointers;
	public ArrayList<ParameterBase> parameters;
	

	
	public MessageBase(){
		parameters = new ArrayList<ParameterBase>();
	}
	public abstract void init(byte[] _data);
	
	protected byte[] getPointerData(byte[] data, int pointer_index){
		return Arrays.copyOfRange(data, pointers[pointer_index].position + 1, pointers[pointer_index].position + 1 + (data[pointers[pointer_index].position] & 0xFF));
		
	}
	protected void processOptional(byte[] data, int pointer_index){
		ParameterType pt = null;
		ParameterBase param = null;
		int l;
		// optional
		// Optional part
		// if equals 0(zero) no optional parameters present
		if(pointers[pointer_index].value != 0x00){
			// position = first optional parameter
			byte_pos = pointers[pointer_index].position;
			while(!opt_done){
				pt = ParameterType.get(data[byte_pos++] & 0xFF);
				if(pt != null){
					// End of optional parameters has no length, only one byte which is 0x00
					if(pt != ParameterType.END_OF_OPTIONAL_PARAMETERS) l = data[byte_pos++] & 0xFF;
					else l = 0;
					switch(pt){
						case END_OF_OPTIONAL_PARAMETERS:
							opt_done = true;
							break;
						default:
							param = ParameterBase.createParameter(pt);
							break;
					}	
					if(param != null){
						param.init(Arrays.copyOfRange(data, byte_pos, l + byte_pos));
						parameters.add(param);
						param = null;
					}					
				}else l = data[byte_pos++] & 0xFF;
				byte_pos += l;
				
			}			
		}			
	}
	
	public ParameterBase getParameter(ParameterType param_type){
		for(int i = 0; i<parameters.size(); i++) if(parameters.get(i).type == param_type) return parameters.get(i);
		return null;
	}
	
}
