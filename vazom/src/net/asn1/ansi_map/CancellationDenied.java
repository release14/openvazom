package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CancellationDenied extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _multipleAccess =  1 ;
	public static final int _busy =  2 ;
	public CancellationDenied(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
