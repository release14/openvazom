package net.cli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import net.utils.Utils;


public class AuthManager {
	public static Logger logger=Logger.getLogger(AuthManager.class);
	public static ConcurrentHashMap<String, AuthUser> auth_map;
	private static String db_file;
	
	public static synchronized void remove(String username) {
		auth_map.remove(username);
		save();
	}
	
	public static synchronized void set(String username, String password, int group) {
		AuthUser au = new AuthUser();
		au.username = username;
		au.password = Utils.bytes2hexstr(Utils.md5(password), "");
		au.group = group;
		auth_map.put(username, au);
		save();
	}
	
	public static String grpId2Name(int grp_id){
		switch(grp_id){
			case 0: return "admin";
			case 1: return "user";
		}
		return "Unknown!";
		
	}
	
	private static void load(){
		File f = new File(db_file);
		String line;
		String[] tmp;
		AuthUser au = null;
		try{
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			while((line = in.readLine()) != null){
				tmp = line.split(":");
				au = new AuthUser();
				au.username = tmp[0];
				au.password = tmp[2];
				au.group = Integer.parseInt(tmp[1]);
				auth_map.put(au.username, au);
			}		
			in.close();
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("CLI Auth DB [" + db_file + "] does not exist!");
		}
		
		
	}
	private static void save(){
		try{
			PrintWriter pw = new PrintWriter(new File(db_file));
			for(String s : auth_map.keySet()){
				pw.println(s + ":" + auth_map.get(s).group + ":" + auth_map.get(s).password);
			}
			pw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public static boolean userExists(String username){
		return auth_map.get(username) != null;
	}
	
	public static int checkGroup(String username){
		AuthUser au = auth_map.get(username);
		if(au != null){
			return au.group;
		}
		
		return -1;
	
	}	
	
	public static boolean check(String username, String password){
		AuthUser au = auth_map.get(username);
		if(au != null){
			return password.equals(au.password);
		}
		
		return false;
		
		
	}
	
	
	
	
	public static void init(String db){
		logger.info("Starting CLI Auth Manager...");
		db_file = db;
		auth_map = new ConcurrentHashMap<String, AuthUser>();
		load();
	}
}
