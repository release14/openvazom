package net.sccp.parameters.refusal_cause;

import java.util.HashMap;


public enum RefusalCauseType {
	END_USER_ORIGINATED(0x00),
	END_USER_CONGESTION(0x01),
	END_USER_FAILURE(0x02),
	SCCP_USER_ORIGINATED(0x03),
	DESTINATION_ADDRESS_UNKNOWN(0x04),
	DESTINATION_INACESSIBLE(0x05),
	QOS_NOT_AVAILABLE_NON_TRANASIENT(0x06),
	QOS_NOT_AVAILABLE_TRANSIENT(0x07),
	ACCESS_FAILURE(0x08),
	ACCESS_CONGESTION(0x09),
	SUBSYSTEM_FAILURE(0x0A),
	SUBSYSTEM_CONGESTION(0x0B),
	EXPIRATION_OF_CONN_ESTABLISHMENT_TIMER(0x0C),
	INCOMPATIBLE_USER_DATA(0x0D),
	RESERVED(0x0E),
	UNQUALIFIED(0x0F),
	HOP_COUNTER_VIOLATION(0x10),
	SCCP_FAILURE(0x11),
	NO_TRANSLATION_FOR_ADDR_OF_SUCH_NATURE(0x12),
	UNEQUIPPED_USER(0x13);
	
	private int id;
	private static final HashMap<Integer, RefusalCauseType> lookup = new HashMap<Integer, RefusalCauseType>();
	static{
		for(RefusalCauseType td : RefusalCauseType.values()){
			lookup.put(td.getId(), td);
		}
	}
	private int getId(){ return id; }
	public static RefusalCauseType get(int id){ return lookup.get(id); }
	private RefusalCauseType(int _id){ id = _id; }
	
}
