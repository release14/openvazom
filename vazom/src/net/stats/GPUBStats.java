package net.stats;

public class GPUBStats {
	public long LD_REQUEST_COUNT;
	public long LD_REPLY_OK_COUNT;
	public long LD_REPLY_ERROR_COUNT;
	public long LD_MAX_REPLY_TIME;
	public long LD_LAST_REPLY_TIME;
	public long LD_CONNECTION_DOWN_COUNT;
	public long LD_NODE_BUSY_COUNT;
	
	public long UPDATE_REQUEST_COUNT;
	public long UPDATE_REPLY_OK_COUNT;
	public long UPDATE_REPLY_ERROR_COUNT;
	public long UPDATE_MAX_REPLY_TIME;
	public long UPDATE_LAST_REPLY_TIME;
	public long UPDATE_CONNECTION_DOWN_COUNT;
	
	public long MD5_REQUEST_COUNT;
	public long MD5_REPLY_OK_COUNT;
	public long MD5_REPLY_ERROR_COUNT;
	public long MD5_MAX_REPLY_TIME;
	public long MD5_LAST_REPLY_TIME;
	public long MD5_CONNECTION_DOWN_COUNT;
	public long MD5_NODE_BUSY_COUNT;
	
	public long MD5_UPDATE_REQUEST_COUNT;
	public long MD5_UPDATE_REPLY_OK_COUNT;
	public long MD5_UPDATE_REPLY_ERROR_COUNT;
	public long MD5_UPDATE_MAX_REPLY_TIME;
	public long MD5_UPDATE_LAST_REPLY_TIME;
	public long MD5_UPDATE_CONNECTION_DOWN_COUNT;
	
	public long LST_REMOVE_COUNT;
	public long LST_REMOVE_ERR_COUNT;

	public void md5_update_setMaxTS(long ts){
		if(ts > MD5_UPDATE_MAX_REPLY_TIME) MD5_UPDATE_MAX_REPLY_TIME = ts;
	}
	public void md5_setMaxTS(long ts){
		if(ts > MD5_MAX_REPLY_TIME) MD5_MAX_REPLY_TIME = ts;
	}
	
	
	public void update_setMaxTS(long ts){
		if(ts > UPDATE_MAX_REPLY_TIME) UPDATE_MAX_REPLY_TIME = ts;
	}
	public void ld_setMaxTS(long ts){
		if(ts > LD_MAX_REPLY_TIME) LD_MAX_REPLY_TIME = ts;
	}
}
