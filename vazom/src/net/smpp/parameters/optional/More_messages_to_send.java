package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class More_messages_to_send extends OptionalParameterBase {
	public More_messages_to_send(){
		type = OptionalParameterType.MORE_MESSAGES_TO_SEND;
	}
	public int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
