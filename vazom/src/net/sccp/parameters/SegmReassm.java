package net.sccp.parameters;

public class SegmReassm extends ParamBase {
	public boolean moreDataIndication;
	
	public SegmReassm(){
		super();
		type = ParamType.SEGMENTING_REASSEMBLING;
	}
	
	public void init(byte[] data) {
		moreDataIndication = (data[0] & 0x01) == 0x01;
	}

}
