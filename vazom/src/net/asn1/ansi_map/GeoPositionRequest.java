package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class GeoPositionRequest extends SET{

	public PositionRequestType get_positionRequestType(){
		return (PositionRequestType)elements.get(0).data;
	}
	public PositionRequestType new_positionRequestType(){
		return new PositionRequestType();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(1).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(2).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(3).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LCSBillingID get_lcsBillingID(){
		return (LCSBillingID)elements.get(4).data;
	}
	public LCSBillingID new_lcsBillingID(){
		return new LCSBillingID();
	}

	public LCS_Client_ID get_lcs_Client_ID(){
		return (LCS_Client_ID)elements.get(5).data;
	}
	public LCS_Client_ID new_lcs_Client_ID(){
		return new LCS_Client_ID();
	}

	public MEID get_meid(){
		return (MEID)elements.get(6).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(7).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MobilePositionCapability get_mobilePositionCapability(){
		return (MobilePositionCapability)elements.get(8).data;
	}
	public MobilePositionCapability new_mobilePositionCapability(){
		return new MobilePositionCapability();
	}

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(9).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public DTXIndication get_dtxIndication(){
		return (DTXIndication)elements.get(10).data;
	}
	public DTXIndication new_dtxIndication(){
		return new DTXIndication();
	}

	public ReceivedSignalQuality get_receivedSignalQuality(){
		return (ReceivedSignalQuality)elements.get(11).data;
	}
	public ReceivedSignalQuality new_receivedSignalQuality(){
		return new ReceivedSignalQuality();
	}

	public CDMAChannelData get_cdmaChannelData(){
		return (CDMAChannelData)elements.get(12).data;
	}
	public CDMAChannelData new_cdmaChannelData(){
		return new CDMAChannelData();
	}

	public CDMACodeChannel get_cdmaCodeChannel(){
		return (CDMACodeChannel)elements.get(13).data;
	}
	public CDMACodeChannel new_cdmaCodeChannel(){
		return new CDMACodeChannel();
	}

	public CDMAMobileCapabilities get_cdmaMobileCapabilities(){
		return (CDMAMobileCapabilities)elements.get(14).data;
	}
	public CDMAMobileCapabilities new_cdmaMobileCapabilities(){
		return new CDMAMobileCapabilities();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(15).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServingOneWayDelay2 get_cdmaServingOneWayDelay2(){
		return (CDMAServingOneWayDelay2)elements.get(16).data;
	}
	public CDMAServingOneWayDelay2 new_cdmaServingOneWayDelay2(){
		return new CDMAServingOneWayDelay2();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(17).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public CDMATargetMAHOList get_cdmaTargetMAHOList(){
		return (CDMATargetMAHOList)elements.get(18).data;
	}
	public CDMATargetMAHOList new_cdmaTargetMAHOList(){
		return new CDMATargetMAHOList();
	}

	public CDMAPSMMList get_cdmaPSMMList(){
		return (CDMAPSMMList)elements.get(19).data;
	}
	public CDMAPSMMList new_cdmaPSMMList(){
		return new CDMAPSMMList();
	}

	public NAMPSChannelData get_nampsChannelData(){
		return (NAMPSChannelData)elements.get(20).data;
	}
	public NAMPSChannelData new_nampsChannelData(){
		return new NAMPSChannelData();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(21).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TargetMeasurementList get_targetMeasurementList(){
		return (TargetMeasurementList)elements.get(22).data;
	}
	public TargetMeasurementList new_targetMeasurementList(){
		return new TargetMeasurementList();
	}

	public TDMA_MAHO_CELLID get_tdma_MAHO_CELLID(){
		return (TDMA_MAHO_CELLID)elements.get(23).data;
	}
	public TDMA_MAHO_CELLID new_tdma_MAHO_CELLID(){
		return new TDMA_MAHO_CELLID();
	}

	public TDMA_MAHO_CHANNEL get_tdma_MAHO_CHANNEL(){
		return (TDMA_MAHO_CHANNEL)elements.get(24).data;
	}
	public TDMA_MAHO_CHANNEL new_tdma_MAHO_CHANNEL(){
		return new TDMA_MAHO_CHANNEL();
	}

	public TDMA_TimeAlignment get_tdma_TimeAlignment(){
		return (TDMA_TimeAlignment)elements.get(25).data;
	}
	public TDMA_TimeAlignment new_tdma_TimeAlignment(){
		return new TDMA_TimeAlignment();
	}

	public TDMAVoiceMode get_tdmaVoiceMode(){
		return (TDMAVoiceMode)elements.get(26).data;
	}
	public TDMAVoiceMode new_tdmaVoiceMode(){
		return new TDMAVoiceMode();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(27).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(28).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public NetworkTMSI get_networkTMSI(){
		return (NetworkTMSI)elements.get(29).data;
	}
	public NetworkTMSI new_networkTMSI(){
		return new NetworkTMSI();
	}

	public PQOS_HorizontalPosition get_pqos_HorizontalPosition(){
		return (PQOS_HorizontalPosition)elements.get(30).data;
	}
	public PQOS_HorizontalPosition new_pqos_HorizontalPosition(){
		return new PQOS_HorizontalPosition();
	}

	public PQOS_HorizontalVelocity get_pqos_HorizontalVelocity(){
		return (PQOS_HorizontalVelocity)elements.get(31).data;
	}
	public PQOS_HorizontalVelocity new_pqos_HorizontalVelocity(){
		return new PQOS_HorizontalVelocity();
	}

	public PQOS_MaximumPositionAge get_pqos_MaximumPositionAge(){
		return (PQOS_MaximumPositionAge)elements.get(32).data;
	}
	public PQOS_MaximumPositionAge new_pqos_MaximumPositionAge(){
		return new PQOS_MaximumPositionAge();
	}

	public PQOS_PositionPriority get_pqos_PositionPriority(){
		return (PQOS_PositionPriority)elements.get(33).data;
	}
	public PQOS_PositionPriority new_pqos_PositionPriority(){
		return new PQOS_PositionPriority();
	}

	public PQOS_ResponseTime get_pqos_ResponseTime(){
		return (PQOS_ResponseTime)elements.get(34).data;
	}
	public PQOS_ResponseTime new_pqos_ResponseTime(){
		return new PQOS_ResponseTime();
	}

	public PQOS_VerticalPosition get_pqos_VerticalPosition(){
		return (PQOS_VerticalPosition)elements.get(35).data;
	}
	public PQOS_VerticalPosition new_pqos_VerticalPosition(){
		return new PQOS_VerticalPosition();
	}

	public PQOS_VerticalVelocity get_pqos_VerticalVelocity(){
		return (PQOS_VerticalVelocity)elements.get(36).data;
	}
	public PQOS_VerticalVelocity new_pqos_VerticalVelocity(){
		return new PQOS_VerticalVelocity();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(37).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public Teleservice_Priority get_teleservice_Priority(){
		return (Teleservice_Priority)elements.get(38).data;
	}
	public Teleservice_Priority new_teleservice_Priority(){
		return new Teleservice_Priority();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(337, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_positionRequestType(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(367, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcsBillingID(); }});
		elements.add(new ElementDescriptor(358, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcs_Client_ID(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(335, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobilePositionCapability(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(329, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dtxIndication(); }});
		elements.add(new ElementDescriptor(72, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_receivedSignalQuality(); }});
		elements.add(new ElementDescriptor(63, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaChannelData(); }});
		elements.add(new ElementDescriptor(68, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaCodeChannel(); }});
		elements.add(new ElementDescriptor(330, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileCapabilities(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(347, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServingOneWayDelay2(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(136, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMAHOList(); }});
		elements.add(new ElementDescriptor(346, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPSMMList(); }});
		elements.add(new ElementDescriptor(76, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsChannelData(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(157, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetMeasurementList(); }});
		elements.add(new ElementDescriptor(359, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_MAHO_CELLID(); }});
		elements.add(new ElementDescriptor(360, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_MAHO_CHANNEL(); }});
		elements.add(new ElementDescriptor(362, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_TimeAlignment(); }});
		elements.add(new ElementDescriptor(223, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaVoiceMode(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(233, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkTMSI(); }});
		elements.add(new ElementDescriptor(372, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_HorizontalPosition(); }});
		elements.add(new ElementDescriptor(373, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_HorizontalVelocity(); }});
		elements.add(new ElementDescriptor(374, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_MaximumPositionAge(); }});
		elements.add(new ElementDescriptor(375, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_PositionPriority(); }});
		elements.add(new ElementDescriptor(376, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_ResponseTime(); }});
		elements.add(new ElementDescriptor(377, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_VerticalPosition(); }});
		elements.add(new ElementDescriptor(378, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pqos_VerticalVelocity(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(290, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_teleservice_Priority(); }});
	}
	public GeoPositionRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
