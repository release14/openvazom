package net.sccp;

import java.util.HashMap;

public enum NatureOfAddress {
	UNKNOWN(0x00),
	SUBSCRIBER_NUMBER(0x01),
	RESERVED_FOR_NATIONAL_USE(0x02),
	NATIONAL_SIGNIFICANT_NUMBER(0x03),
	INTERNATIONAL(0x04);
	
	
	// bits 6..0
	private int id;
	private static final HashMap<Integer, NatureOfAddress> lookup = new HashMap<Integer, NatureOfAddress>();
	static{
		for(NatureOfAddress td : NatureOfAddress.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static NatureOfAddress get(int id){ return lookup.get(id); }
	private NatureOfAddress(int _id){ id = _id; }

}
