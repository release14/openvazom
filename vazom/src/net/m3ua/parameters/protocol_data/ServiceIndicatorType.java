package net.m3ua.parameters.protocol_data;

import java.util.HashMap;



public enum ServiceIndicatorType {
	SNMP(0x00),
	SNTMM(0x01),
	SNTMSM(0x02),
	SCCP(0x03),
	TUP(0x04),
	ISUP(0x05),
	DUP_CALL_AND_CIRCUIT(0x06),
	DUP_REG_AND_CANC(0x07),
	MTP_TESTING(0x08),
	BISUP(0x09),
	SISUP(0x0a),
	GCP(0x0e);
	
	private int id;
	
	private static final HashMap<Integer, ServiceIndicatorType> lookup = new HashMap<Integer, ServiceIndicatorType>();
	static{
		for(ServiceIndicatorType td : ServiceIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ServiceIndicatorType get(int id){ return lookup.get(id); }
	private ServiceIndicatorType(int _id){ id = _id; }	
}
