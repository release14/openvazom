package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Subaddress extends OCTET_STRING{

	public Subaddress(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
