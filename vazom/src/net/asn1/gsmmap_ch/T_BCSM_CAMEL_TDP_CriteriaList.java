package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class T_BCSM_CAMEL_TDP_CriteriaList extends SEQUENCE{

	public T_BCSM_CAMEL_TDP_Criteria new_child(){
		return new T_BCSM_CAMEL_TDP_Criteria();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public T_BCSM_CAMEL_TDP_Criteria getChild(int index){
		return (T_BCSM_CAMEL_TDP_Criteria)of_children.get(index);
	}
	public T_BCSM_CAMEL_TDP_CriteriaList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
