package net.sccp;

import java.util.HashMap;

public enum SubsystemNumber {
	SSN_UNKNOWN(0x00),
	SCCP_MANAGEMENT(0x01),
	ITU_T_RESERVED(0x02),
	ISDN_USER_PART(0x03),
	OMAP(0x04),
	MAP(0x05),
	HLR(0x06),
	VLR(0x07),
	MSC(0x08),
	EIC(0x09),
	AUC(0x0A),
	ISDN_SUPPLEMENTARY(0x0B),
	RESERVED_FOR_INTERNATIONAL_USE(0x0C),
	BROADBAND_ISDN_EDGE_TO_EDGE(0x0D),
	TC_TEST_RESPONDER(0x0E);
	
	
	
	// 1 byte
	private int id;
	private static final HashMap<Integer, SubsystemNumber> lookup = new HashMap<Integer, SubsystemNumber>();
	static{
		for(SubsystemNumber td : SubsystemNumber.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static SubsystemNumber get(int id){ return lookup.get(id); }
	private SubsystemNumber(int _id){ id = _id; }
	
}
