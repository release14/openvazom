package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventBufferDescriptor extends SEQUENCE{

	public EventSpec new_child(){
		return new EventSpec();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public EventSpec getChild(int index){
		return (EventSpec)of_children.get(index);
	}
	public EventBufferDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
