package net.m3ua;

import java.util.ArrayList;

import net.m3ua.messages.M3UAMessage;

public class M3UAPacket {

	public int version;
	public M3UAMessageClass messageClass;
	public M3UAMessageType messageType;
	public int messageLength;
	public M3UAMessage message;
	
	public byte[] encode(){
		byte[] res = null;
		int total_length = 8;
		ArrayList<Byte> buff = new ArrayList<Byte>();

		// common header
		buff.add((byte)version);
		buff.add((byte)0x00);
		buff.add((byte)messageClass.getId());
		buff.add((byte)messageType.getId());

		// length values, calculated later
		buff.add((byte)0x00);
		buff.add((byte)0x00);
		buff.add((byte)0x00);
		buff.add((byte)0x00);		
		
		// m3ua parameters, encode and return total length(data + padding)
		total_length += message.encode(buff);
		
		
		// set total message length
		buff.set(4, (byte)(total_length >> 24));
		buff.set(5, (byte)(total_length >> 16));
		buff.set(6, (byte)(total_length >> 8));
		buff.set(7, (byte)(total_length & 0xFF));
		
		//System.out.println(total_length);
		
		// prepare result
		res = new byte[buff.size()];
		for(int i = 0; i<buff.size(); i++) res[i] = buff.get(i);
		return res;
		
	}
	
	
}
