package net.sgc;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.asn1.tcap2.TCMessage;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
import net.ds.DataSourceType;
import net.hplmnr.HPLMNRPacket;
import net.m3ua.M3UA;
import net.m3ua.messages.DATA;
import net.routing.RoutingConnectionType;
import net.sccp.SCCP;
import net.sccp.messages.UDT_UnitData;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.sgn.FNDescriptor;
import net.sgn.FNManager;
import net.smpp_conn.SMPPPacket;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;
import net.vstp.VSTP_SGF_CorrelationPacket;
import net.vstp.VSTP_SGF_CorrelationManager;


import org.apache.log4j.Logger;

public class SGCManager {
	public static Logger logger=Logger.getLogger(SGCManager.class);
	private static ConcurrentLinkedQueue<MessageDescriptor> out_queue;
	private static ConcurrentLinkedQueue<MessageDescriptor> in_queue;
	public static ConcurrentHashMap<String, SGC_SGDescriptor> sgc_client_map;
	public static ConcurrentHashMap<Integer, SGC_SGDescriptor> sgc_host_map;
	private static SGCReconnect sgcr_r;
	private static Listener listener;
	private static SGCWorker sgc_worker_r;
	private static Thread sgc_worker_t;
	private static Thread listener_t;
	private static Thread sgcr_t;
	private static boolean stopping;
	private static int server_sctp_id = -1;

	
	private static class SGCWorker implements Runnable{
		MessageDescriptor md = null;
		String tmp = null;
		VSTP_SGF_CorrelationPacket vsd = null;
		BerTranscoderv2 ber = new BerTranscoderv2();
		int sid;
		byte[] data;
		FNDescriptor fnd = null;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				md = in_queue.poll();
				if(md != null){
					// check for SGC message
					if(md.header.msg_type == MessageType.SGC){
						// add correlation
						if(md.values.get(VSTPDataItemType.VSTP_CRL_ACTION.getId()).equals("1")){
							tmp = md.values.get(VSTPDataItemType.VSTP_CRL_DATA.getId());
							if(tmp != null){
								// check if correlation exist
								if(!VSTP_SGF_CorrelationManager.exists(md.header.msg_id)){
									vsd = new VSTP_SGF_CorrelationPacket();
									data = Utils.strhex2bytes(tmp);
									// decode data
									switch(md.header.ds){
										case SCTP:
											sid = Integer.parseInt(md.values.get(VSTPDataItemType.VSTP_CRL_SCTP_SID.getId()));
											vsd.sri_packet = new HPLMNRPacket();
											vsd.type = RoutingConnectionType.M3UA;
											vsd.sri_packet.m3ua_packet = M3UA.decode(data);
											vsd.sri_packet.sccp_udt = (UDT_UnitData)SCCP.decode(((DATA)vsd.sri_packet.m3ua_packet.message).protocolData.userProtocolData);
											switch(md.header.msg_type){
												case HLR: vsd.sri_packet.sri_tcm = (TCMessage)ber.decode(new TCMessage(), vsd.sri_packet.sccp_udt.data); break;
												case SMS_MO: vsd.sri_packet.mo_tcm = (TCMessage)ber.decode(new TCMessage(), vsd.sri_packet.sccp_udt.data); break;
												case SMS_MT: vsd.sri_packet.mt_tcm = (TCMessage)ber.decode(new TCMessage(), vsd.sri_packet.sccp_udt.data); break;
											}
											vsd.sd = new SSCTPDescriptor(sid, data);
											break;
										case E1:
											vsd.type = RoutingConnectionType.MTP3;
											// TODO
											break;
										case SMPP:
											vsd.type = RoutingConnectionType.SMPP;
											vsd.smpp_packet = new SMPPPacket();
											vsd.smpp_packet_data = data;
											// TODO
											break;
									}
									// add to map
									VSTP_SGF_CorrelationManager.add(md.header.msg_id, vsd);
									// stats
									StatsManager.VSTP_STATS.VSTP_CRL_ADD++;
									
								}else{
									//logger.warn("VSTP-SGF-Add: Correlation exists: [" + md.header.msg_id + "]!");
								}
							}
						// remove correlation
						}else if(md.values.get(VSTPDataItemType.VSTP_CRL_ACTION.getId()).equals("0")){
							if(VSTP_SGF_CorrelationManager.exists(md.header.msg_id)){
								// remove from map
								VSTP_SGF_CorrelationManager.remove(md.header.msg_id);
							}else{
								//logger.warn("VSTP-SGF-Remove: Correlation does not exist: [" + md.header.msg_id + "]!");
								
							}
						// smsfs action
						}else if(md.values.get(VSTPDataItemType.VSTP_CRL_ACTION.getId()).equals("2")){
							// distibute to all connected F nodes
							for(Integer i : FNManager.fn_map.keySet()){
								fnd = FNManager.getNode(i);
								// update all other F nodes
								if(!fnd.id.equals(md.header.source_id)) fnd.private_out_queue.offer(md);
							}
						}					
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
			}			
			logger.info("Ending...");
		}
	}
	public static void distribute_vstp_crl_remove(String msg_id, DataSourceType ds){
		MessageDescriptor md = new MessageDescriptor();
		SGC_SGDescriptor sgcd = null;

		// header
		md.header.destination = LocationType.SGN;
		md.header.ds = ds;
		md.header.msg_id = msg_id;
		md.header.msg_type = MessageType.SGC;
		md.header.source = LocationType.SGN;

		// body
		md.values.put(VSTPDataItemType.VSTP_CRL_ACTION.getId(), "0");

		// send to out queue (client map)
		for(String s : sgc_client_map.keySet()){
			sgcd = sgc_client_map.get(s);
			sgcd.private_out_queue.offer(md);
		}
		/*
		// send to out queue (host map)
		for(Integer s : sgc_host_map.keySet()){
			sgcd = sgc_host_map.get(s);
			sgcd.private_out_queue.offer(md);
		}
		*/
	
	}
	
	public static void distribute_vstp_crl_add(String msg_id, VSTP_SGF_CorrelationPacket vsd){
		MessageDescriptor md = new MessageDescriptor();
		SGC_SGDescriptor sgcd = null;

		// header
		md.header.destination = LocationType.SGN;
		switch(vsd.type){
			case M3UA: 
				md.header.ds = DataSourceType.SCTP; 
				md.values.put(VSTPDataItemType.VSTP_CRL_DATA.getId(), Utils.bytes2hexstr(vsd.sri_packet.m3ua_packet.encode(), ""));
				md.values.put(VSTPDataItemType.VSTP_CRL_SCTP_SID.getId(), Integer.toString(vsd.sd.sid));
				break;
			case MTP3: 
				md.header.ds = DataSourceType.E1;
				// TODO
				break;
			case SMPP: 
				md.header.ds = DataSourceType.SMPP;
				md.values.put(VSTPDataItemType.VSTP_CRL_DATA.getId(), Utils.bytes2hexstr(vsd.smpp_packet_data, ""));
				break;
		}
		md.header.msg_id = msg_id;
		md.header.msg_type = MessageType.SGC;
		md.header.source = LocationType.SGN;

		// body
		md.values.put(VSTPDataItemType.VSTP_CRL_ACTION.getId(), "1");
		
		// send to out queue (client map)
		for(String s : sgc_client_map.keySet()){
			sgcd = sgc_client_map.get(s);
			sgcd.private_out_queue.offer(md);
		}
		/*
		// send to out queue (host map)
		for(Integer s : sgc_host_map.keySet()){
			sgcd = sgc_host_map.get(s);
			sgcd.private_out_queue.offer(md);
		}
		*/
	}
	
	private static class Listener implements Runnable{
		int c_sctp_id = -1;
		SGC_SGDescriptor sgcd = null;
		public void run() {
			logger.info("Starting...");
			server_sctp_id = SSctp.initServer(SGNConfigData.sgc_bind, SGNConfigData.sgc_port);
			if(server_sctp_id > -1){
				logger.info("SGC Listener started, sctp server id = [" + server_sctp_id + "]!");
				while(!stopping){
					c_sctp_id = SSctp.clientAccept(server_sctp_id);
					if(c_sctp_id > -1){
						sgcd = new SGC_SGDescriptor(c_sctp_id);
						addNode_host(c_sctp_id, sgcd);
					}

				}	
				
			}else logger.error("Error while starting SCTP server connection for SGC Listener!");
			logger.info("Ending...");
		}
	}
	

	
	
	
	private static class SGCReconnect implements Runnable{
		public void run() {
			SGC_SGDescriptor sgcd = null;
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.sgc_reconnect_interval); }catch(Exception e){ e.printStackTrace(); }
				for(String s: sgc_client_map.keySet()){
					sgcd = sgc_client_map.get(s);
					if(!sgcd.active){
						logger.info("Reactivating SGC Client Connection to SG node [" + sgcd.id + "], address = [" + sgcd.address + "], port = [" + sgcd.port + "]!");
						connectNode_client(sgcd.id);
					}
				}
			}
		}
	}
	public static void deactivateNode_host(Integer id){
		sgc_host_map .get(id).stop();
		sgc_host_map.get(id).active = false;
	}

	
	

	public static void connectNode_host(int id){
		SGC_SGDescriptor sgcd = sgc_host_map.get(id);
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		String pl_str;
		String tmp;
		if(sgcd != null){
			logger.info("Negotiating VSTP Host Connection, client sctp id = [" + id + "]!");
			if(sgcd.sctp_id > -1){
				// negotiate VSTP connect
				sd = SSctp.receive(sgcd.sctp_id);
				if(sd != null){
					if(sd.payload != null){
						pl_str = new String(sd.payload);
						if(VSTP.check_init(pl_str)){
							// node id
							tmp = VSTP.get_init_node_id(pl_str);
							sgcd.id = tmp;
							res = SSctp.send(sgcd.sctp_id, VSTP.generate_ack(LocationType.SGN, tmp).getBytes(), 0);
							if(res > -1){
								logger.info("VSTP Connection initialized, sctp id = [" + sgcd.sctp_id + "], node id = [" + sgcd.id + "]!");
								vstp_ready = true;
								sgcd.start();
							}
						}
					}
				}
				if(!vstp_ready){
					logger.warn("Error while negotiating VSTP Connection, removing FN Connection, sctp id = [" + id + "]!");
					removeNode_host(id);
					
				}
				
				
			}else logger.warn("Error while initializing SCTP Connection, client id = [" + id + "]!");

		}
	}	
		
	
	public static void connectNode_client(String id){
		SGC_SGDescriptor sgcd = sgc_client_map.get(id);
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		if(sgcd != null){
			logger.info("Initializing SCTP Connection, client id = [" + id + "]!");
			SSctp.shutdownClient(sgcd.sctp_id);
			try{ Thread.sleep(1000); }catch(Exception e){}
			sgcd.sctp_id = SSctp.initClient(sgcd.address, sgcd.port, 16, null, 0);
			if(sgcd.sctp_id > -1){
				logger.info("SCTP Connection, client id = [" + sgcd.sctp_id + "], ready!");
				logger.info("Negotiating VSTP Client Connection, client id = [" + sgcd.sctp_id + "]!");
				// negotiate VSTP connect
				res = SSctp.send(sgcd.sctp_id, VSTP.generate_init(LocationType.SGN, SGNConfigData.sgn_id).getBytes(), 0);
				if(res > -1){
					sd = SSctp.receive(sgcd.sctp_id);
					if(sd != null){
						if(sd.payload != null){
							if(VSTP.check_accept(new String(sd.payload))){
								logger.info("VSTP Client Connection initialized, id = [" + id + "]!");
								vstp_ready = true;
								// start
								sgcd.start();
							}
						}
					}
					if(!vstp_ready){
						logger.warn("Error while negotiating VSTP Client Connection, removing SGC Connection [" + id + "]!");
						removeNode_client(id);
						
					}
				}else{
					logger.warn("Error while negotiating VSTP Client Connection, removing SGC Connection [" + id + "]!");
					removeNode_client(id);
					
				}
				
				
			}else logger.warn("Error while initializing SCTP Connection, client id = [" + id + "]!");

		}

	
	}
	
	public static void start_SGC_monitor(){
		if(SGNConfigData.sgc_reconnect_status){
			sgcr_r = new SGCReconnect();
			sgcr_t = new Thread(sgcr_r, "SGC_RECONNECT_MONITOR");
			sgcr_t.start();
			
		}
	}
	
	public static void addNode_host(Integer id, SGC_SGDescriptor sgcd){
		sgc_host_map.put(id, sgcd);
		logger.info("Adding SGC Host Connection to SG node, sctp id = [" + id + "]");
		connectNode_host(id);

	}
	
	public static void removeNode_host(Integer id){
		sgc_host_map.remove(id);

		
	}
	
	
	public static void addNode_client(String id, SGC_SGDescriptor sgcd){
		sgc_client_map.put(id, sgcd);
		logger.info("Adding SGC Client Connection to SG node [" + id + "], address = [" + sgcd.address + "], port = [" + sgcd.port + "]!");
		connectNode_client(id);

	}
	
	public static void removeNode_client(String id){
		sgc_client_map.remove(id);
	}
	public static void deactivateNode_client(String id){
		sgc_client_map .get(id).stop();
		sgc_client_map.get(id).active = false;
	}
	
	public static MessageDescriptor out_poll(){
		return out_queue.poll();
	}
	public static void out_offer(MessageDescriptor  md){
		if(out_queue.size() < SGNConfigData.sgn_global_max_queue_size) out_queue.offer(md);
		else{
			StatsManager.SYSTEM_STATS.SGC_OUT_QUEUE_MAX++;
			//logger.warn("SGCManager.out_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
		}
	}
	public static void in_offer(MessageDescriptor  md){
		if(in_queue.size() < SGNConfigData.sgn_global_max_queue_size) in_queue.offer(md);
		else{
			StatsManager.SYSTEM_STATS.SGC_IN_QUEUE_MAX++;
			//logger.warn("SGCManager.in_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
		}
	}
	public static MessageDescriptor in_poll(){
		return in_queue.poll();
	}

	
	public static void init(){
		logger.info("Starting SGC Manager...");
		sgc_client_map = new ConcurrentHashMap<String, SGC_SGDescriptor>();
		sgc_host_map = new ConcurrentHashMap<Integer, SGC_SGDescriptor>();
		out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		in_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		logger.info("Starting SGC Connection Monitor...");
		start_SGC_monitor();

		// connection listener thread
		listener = new Listener();
		listener_t = new Thread(listener, "SGC_LISTENER");
		listener_t.start();
		
		// SGC worker
		sgc_worker_r = new SGCWorker();
		sgc_worker_t = new Thread(sgc_worker_r, "SGC_WORKER");
		sgc_worker_t.start();
		



	}

}
