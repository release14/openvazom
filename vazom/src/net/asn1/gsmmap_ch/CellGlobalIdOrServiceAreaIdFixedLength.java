package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CellGlobalIdOrServiceAreaIdFixedLength extends OCTET_STRING{

	public CellGlobalIdOrServiceAreaIdFixedLength(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
