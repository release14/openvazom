package net.isup.parameters.ei;

import java.util.HashMap;

public enum EventPresentationRestrictedIndicatorType {
	NO_INDICATION(0x00),
	PRESENTATION_RESTRICTED(0x80);
	
	
	private int id;
	private static final HashMap<Integer, EventPresentationRestrictedIndicatorType> lookup = new HashMap<Integer, EventPresentationRestrictedIndicatorType>();
	static{
		for(EventPresentationRestrictedIndicatorType td :EventPresentationRestrictedIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static EventPresentationRestrictedIndicatorType get(int id){ return lookup.get(id); }
	private EventPresentationRestrictedIndicatorType(int _id){ id = _id; }		
}
