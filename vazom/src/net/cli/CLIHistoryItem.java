package net.cli;

import java.util.ArrayList;

public class CLIHistoryItem {
	public String cmd;
	public String parent;
	public ArrayList<ParamDescriptor> params;
	public boolean incomplete;
	public DescriptorBase descriptor;
}
