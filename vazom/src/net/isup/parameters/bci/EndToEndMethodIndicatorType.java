package net.isup.parameters.bci;

import java.util.HashMap;

public enum EndToEndMethodIndicatorType {
	NO_METHOD_AVAILABLE(0x00),
	PASS_ALONG(0x40),
	SCCP(0x80),
	PASS_ALONG_AND_SCCP(0x80);
	
	private int id;
	private static final HashMap<Integer, EndToEndMethodIndicatorType> lookup = new HashMap<Integer, EndToEndMethodIndicatorType>();
	static{
		for(EndToEndMethodIndicatorType td : EndToEndMethodIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static EndToEndMethodIndicatorType get(int id){ return lookup.get(id); }
	private EndToEndMethodIndicatorType(int _id){ id = _id; }
}
