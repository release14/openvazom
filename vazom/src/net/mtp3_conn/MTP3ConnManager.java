package net.mtp3_conn;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class MTP3ConnManager {
	public static Logger logger=Logger.getLogger(MTP3ConnManager.class);
	public static ConcurrentHashMap<String, MTP3Connection> conn_map;

	public static void init(){
		logger.info("Starting E1 Manager...");
		conn_map = new ConcurrentHashMap<String, MTP3Connection>();

	}
}
