package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MegacoMessage extends SEQUENCE{

	public AuthenticationHeader get_authHeader(){
		return (AuthenticationHeader)elements.get(0).data;
	}
	public AuthenticationHeader new_authHeader(){
		return new AuthenticationHeader();
	}

	public Message get_mess(){
		return (Message)elements.get(1).data;
	}
	public Message new_mess(){
		return new Message();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authHeader(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mess(); }});
	}
	public MegacoMessage(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
