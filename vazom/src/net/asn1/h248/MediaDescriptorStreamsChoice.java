package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MediaDescriptorStreamsChoice extends CHOICE{

	public StreamParms get_oneStream(){
		return (StreamParms)elements.get(0).data;
	}
	public StreamParms new_oneStream(){
		return new StreamParms();
	}

	public StreamDescriptorSequenceOf get_multiStream(){
		return (StreamDescriptorSequenceOf)elements.get(1).data;
	}
	public StreamDescriptorSequenceOf new_multiStream(){
		return new StreamDescriptorSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_oneStream(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_multiStream(); }});
	}
	public MediaDescriptorStreamsChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
