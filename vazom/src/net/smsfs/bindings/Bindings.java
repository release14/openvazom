package net.smsfs.bindings;

import java.util.HashMap;

import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.NumberingPlan;
import net.smpp.parameters.Data_codingType;
import net.smpp.parameters.GSMNetworkSpecific;
import net.smpp.parameters.IntermediateNotificationType;
import net.smpp.parameters.MessageMode;
import net.smpp.parameters.MessageType;
import net.smpp.parameters.SMEOrigAckType;
import net.smpp.parameters.SMSCDeliveryReceiptType;
import net.smsfs.SmsfsManager;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLED_ADDRESS;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLING_ADDRESS;
import net.smsfs.bindings.descriptors.HLR_ANNN;
import net.smsfs.bindings.descriptors.HLR_IMSI;
import net.smsfs.bindings.descriptors.HLR_MSISDN;
import net.smsfs.bindings.descriptors.HLR_NNN;
import net.smsfs.bindings.descriptors.HLR_SCA;
import net.smsfs.bindings.descriptors.M3UA_DPC;
import net.smsfs.bindings.descriptors.M3UA_OPC;
import net.smsfs.bindings.descriptors.MAP_IMSI;
import net.smsfs.bindings.descriptors.MAP_MSISDN;
import net.smsfs.bindings.descriptors.MAP_SCDA;
import net.smsfs.bindings.descriptors.MAP_SCOA;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLING_GTI;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLING_NAI;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLING_NP;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLING_TT;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLED_GTI;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLED_NAI;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLED_NP;
import net.smsfs.bindings.descriptors.SCCP_GT_CALLED_TT;
import net.smsfs.bindings.descriptors.SMS_MSG_TYPE;
import net.smsfs.bindings.descriptors.SMS_TPDU_DCS;
import net.smsfs.bindings.descriptors.SMS_TPDU_DESTINATION;
import net.smsfs.bindings.descriptors.SMS_TPDU_DESTINATION_ENC;
import net.smsfs.bindings.descriptors.SMS_TPDU_ORIGINATING;
import net.smsfs.bindings.descriptors.SMS_TPDU_ORIGINATING_ENC;
import net.smsfs.bindings.descriptors.SMS_TPDU_UD;
import net.smsfs.bindings.descriptors.smpp.SMPP_DATA_CODING;
import net.smsfs.bindings.descriptors.smpp.SMPP_DELIVERY_TIME;
import net.smsfs.bindings.descriptors.smpp.SMPP_ESM_GSM_FEATURES;
import net.smsfs.bindings.descriptors.smpp.SMPP_ESM_MESSAGE_MODE;
import net.smsfs.bindings.descriptors.smpp.SMPP_ESM_MESSAGE_TYPE;
import net.smsfs.bindings.descriptors.smpp.SMPP_IP_DESTINATION;
import net.smsfs.bindings.descriptors.smpp.SMPP_IP_SOURCE;
import net.smsfs.bindings.descriptors.smpp.SMPP_ORIGINATOR_ADDRESS;
import net.smsfs.bindings.descriptors.smpp.SMPP_ORIGINATOR_NP;
import net.smsfs.bindings.descriptors.smpp.SMPP_ORIGINATOR_TON;
import net.smsfs.bindings.descriptors.smpp.SMPP_PASSWORD;
import net.smsfs.bindings.descriptors.smpp.SMPP_PRIORITY_FLAG;
import net.smsfs.bindings.descriptors.smpp.SMPP_PROTOCOL_ID;
import net.smsfs.bindings.descriptors.smpp.SMPP_RD_INTERMEDIATE_NOTIFICATION;
import net.smsfs.bindings.descriptors.smpp.SMPP_RD_SME_ACK;
import net.smsfs.bindings.descriptors.smpp.SMPP_RD_SMSC_RECEIPT;
import net.smsfs.bindings.descriptors.smpp.SMPP_RECIPIENT_ADDRESS;
import net.smsfs.bindings.descriptors.smpp.SMPP_RECIPIENT_NP;
import net.smsfs.bindings.descriptors.smpp.SMPP_RECIPIENT_TON;
import net.smsfs.bindings.descriptors.smpp.SMPP_REPLACE_IF_PRESENT;
import net.smsfs.bindings.descriptors.smpp.SMPP_SERVICE_TYPE;
import net.smsfs.bindings.descriptors.smpp.SMPP_SM;
import net.smsfs.bindings.descriptors.smpp.SMPP_SM_DEFAULT_MSG_ID;
import net.smsfs.bindings.descriptors.smpp.SMPP_SM_LENGTH;
import net.smsfs.bindings.descriptors.smpp.SMPP_SYSTEM_ID;
import net.smsfs.bindings.descriptors.smpp.SMPP_TCP_DESTINATION;
import net.smsfs.bindings.descriptors.smpp.SMPP_TCP_SOURCE;
import net.smsfs.bindings.descriptors.smpp.SMPP_VALIDITY_PERIOD;
import net.smstpdu.SmsType;
import net.smstpdu.TypeOfNumber;
import net.smstpdu.tpdcs.general.Alphabet;


public class Bindings {

	private static HashMap<String, BindingDescriptor> map;
	private static HashMap<String, BindingDescriptor> smpp_map;
	private static HashMap<String, BindingDescriptor> hlr_map;
	private static HashMap<String, Integer> hlr_result_map;
	private static HashMap<String, Object> common_map;
	
	public static BindingDescriptor get_smpp(String id){
		return smpp_map.get(id);
	}

	
	public static BindingDescriptor get(String id){
		return map.get(id);
	}
	public static BindingDescriptor get_hlr(String id){
		return hlr_map.get(id);
	}
	public static Integer get_hlr_result(String id){
		return hlr_result_map.get(id);
	}
	public static Object getCommon(String id){
		return common_map.get(id);
	}
	private static void init_common(){
		// Arrays
		//common_map.put("SCCP.GT_CALLED.WL", SmsfsManager.sccp_gt_called_wl);
		//common_map.put("SCCP.GT_CALLING.WL", SmsfsManager.sccp_gt_calling_wl);
		//common_map.put("MAP.SCOA.WL", SmsfsManager.map_scoa_wl);
		//common_map.put("MAP.SCDA.WL", SmsfsManager.map_scda_wl);
		
		// Type Of Number
		common_map.put("TON.UNKNOWN", Integer.toString(TypeOfNumber.UNKNOWN.getId()));
		common_map.put("TON.INTERNATIONAL", Integer.toString(TypeOfNumber.INTERNATIONAL.getId()));
		common_map.put("TON.NATIONAL", Integer.toString(TypeOfNumber.NATIONAL.getId()));
		common_map.put("TON.NETWORK_SPECIFIC", Integer.toString(TypeOfNumber.NETWORK_SPECIFIC.getId()));
		common_map.put("TON.SUBSCRIBER_NUMBER", Integer.toString(TypeOfNumber.SUBSCRIBER_NUMBER.getId()));
		common_map.put("TON.ALPHANUMERIC", Integer.toString(TypeOfNumber.ALPHANUMERIC.getId()));
		common_map.put("TON.ABBREVIATED", Integer.toString(TypeOfNumber.ABBREVIATED.getId()));		
		
		// DCS
		common_map.put("DCS.DEFAULT", Integer.toString(Alphabet.DEFAULT.getId()));
		common_map.put("DCS.8BIT", Integer.toString(Alphabet._8BIT.getId()));
		common_map.put("DCS.UCS2", Integer.toString(Alphabet.UCS2.getId()));
		
		// Message type
		common_map.put("MSG_TYPE.SINGLE", Integer.toString(SmsType.SINGLE.getId()));
		common_map.put("MSG_TYPE.CONCATENATED", Integer.toString(SmsType.CONCATENATED.getId()));
		
		// NP Types
		common_map.put("NP.UNKNOWN", Integer.toString(NumberingPlan.UNKNOWN.getId()));
		common_map.put("NP.ISDN_TELEPHONE", Integer.toString(NumberingPlan.ISDN_TELEPHONE.getId()));
		common_map.put("NP.GENERIC", Integer.toString(NumberingPlan.GENERIC.getId()));
		common_map.put("NP.DATA_X121", Integer.toString(NumberingPlan.DATA_X121.getId()));
		common_map.put("NP.TELEX", Integer.toString(NumberingPlan.TELEX.getId())); 
		common_map.put("NP.MARITIME", Integer.toString(NumberingPlan.MARITIME.getId()));
		common_map.put("NP.LAND_MOBILE", Integer.toString(NumberingPlan.LAND_MOBILE.getId()));
		common_map.put("NP.ISDN_MOBILE", Integer.toString(NumberingPlan.ISDN_MOBILE.getId()));
		common_map.put("NP.PRIVATE", Integer.toString(NumberingPlan.PRIVATE.getId()));
		
		// NAI types
		common_map.put("NAI.UNKNOWN", Integer.toString(NatureOfAddress.UNKNOWN.getId()));
		common_map.put("NAI.SUBSCRIBER_NUMBER", Integer.toString(NatureOfAddress.SUBSCRIBER_NUMBER.getId()));
		common_map.put("NAI.RESERVED_FOR_NATIONAL_USE", Integer.toString(NatureOfAddress.RESERVED_FOR_NATIONAL_USE.getId()));
		common_map.put("NAI.NAI_NATIONAL_SIGNIFICANT_NUMBER", Integer.toString(NatureOfAddress.NATIONAL_SIGNIFICANT_NUMBER.getId()));
		common_map.put("NAI.NAI_INTERNATIONAL", Integer.toString(NatureOfAddress.INTERNATIONAL.getId()));
		
		// GTI types
		common_map.put("GTI.NONE", Integer.toString(GlobalTitleIndicator.NO_TITLE.getId()));
		common_map.put("GTI.NAI_ONLY", Integer.toString(GlobalTitleIndicator.NATURE_OF_ADDRESS_INDICATOR_ONLY.getId()));
		common_map.put("GTI.TT_ONLY", Integer.toString(GlobalTitleIndicator.TRANSLATION_TYPE_ONLY.getId()));
		common_map.put("GTI.TTNPE", Integer.toString(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING.getId()));
		common_map.put("GTI.TTNPENOA", Integer.toString(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS.getId()));
		
		// SMPP
		// SMPP ESM Message Mode
		common_map.put("SMPP.ESM.MM.DEFAULT", Integer.toString(MessageMode.DEFAULT_SMSC_MODE.getId()));
		common_map.put("SMPP.ESM.MM.DATAGRAM", Integer.toString(MessageMode.DATAGRAM_MODE.getId()));
		common_map.put("SMPP.ESM.MM.FORWARD", Integer.toString(MessageMode.FORWARD_MODE.getId()));
		common_map.put("SMPP.ESM.MM.STORE_FORWARD", Integer.toString(MessageMode.STORE_FORWARD_MODE.getId()));
		// SMPP ESM Message Type
		common_map.put("SMPP.ESM.MT.DEFAULT", Integer.toString(MessageType.DEFAULT.getId()));
		common_map.put("SMPP.ESM.MT.DELIVERY_ACK", Integer.toString(MessageType.DELIVERY_ACK.getId()));
		common_map.put("SMPP.ESM.MT.MANUAL_USER_ACK", Integer.toString(MessageType.MANUAL_USER_ACK.getId()));
		// SMPP ESM GSM Features
		common_map.put("SMPP.ESM.GF.NO", Integer.toString(GSMNetworkSpecific.NO_SPECIFIC_FEATURES.getId()));
		common_map.put("SMPP.ESM.GF.UDHI_INDICATOR", Integer.toString(GSMNetworkSpecific.UDHI_INDICATOR.getId()));
		common_map.put("SMPP.ESM.GF.SET_REPLY_PATH", Integer.toString(GSMNetworkSpecific.SET_REPLY_PATH.getId()));
		common_map.put("SMPP.ESM.GF.SET_BOTH", Integer.toString(GSMNetworkSpecific.SET_UDHI_AND_REPLY_PATH.getId()));
		// SMPP Type Of Number
		common_map.put("SMPP.TON.UNKNOWN", Integer.toString(net.smpp.TypeOfNumber.UNKNOWN.getId()));
		common_map.put("SMPP.TON.INTERNATIONAL", Integer.toString(net.smpp.TypeOfNumber.INTERNATIONAL.getId()));
		common_map.put("SMPP.TON.NATIONAL", Integer.toString(net.smpp.TypeOfNumber.NATIONAL.getId()));
		common_map.put("SMPP.TON.NETWORK_SPECIFIC", Integer.toString(net.smpp.TypeOfNumber.NETWORK_SPECIFIC.getId()));
		common_map.put("SMPP.TON.SUBSCRIBER_NUMBER", Integer.toString(net.smpp.TypeOfNumber.SUBSCRIBER_NUMBER.getId()));
		common_map.put("SMPP.TON.ALPHANUMERIC", Integer.toString(net.smpp.TypeOfNumber.ALPHANUMERIC.getId()));
		common_map.put("SMPP.TON.ABBREVIATED", Integer.toString(net.smpp.TypeOfNumber.ABBREVIATED.getId()));
		// SMPP Numbering Plan
		common_map.put("SMPP.NP.UNKNOWN", Integer.toString(net.smpp.NumberingPlan.UNKNOWN.getId()));
		common_map.put("SMPP.NP.ISDN_TELEPHONE", Integer.toString(net.smpp.NumberingPlan.ISDN_TELEPHONE.getId()));
		common_map.put("SMPP.NP.DATA_X121", Integer.toString(net.smpp.NumberingPlan.DATA_X121.getId()));
		common_map.put("SMPP.NP.TELEX", Integer.toString(net.smpp.NumberingPlan.TELEX.getId()));
		common_map.put("SMPP.NP.LAND_MOBILE", Integer.toString(net.smpp.NumberingPlan.LAND_MOBILE.getId()));
		common_map.put("SMPP.NP.NATIONAL", Integer.toString(net.smpp.NumberingPlan.NATIONAL.getId()));
		common_map.put("SMPP.NP.PRIVATE", Integer.toString(net.smpp.NumberingPlan.PRIVATE.getId()));
		common_map.put("SMPP.NP.ERMES", Integer.toString(net.smpp.NumberingPlan.ERMES.getId()));
		common_map.put("SMPP.NP.INTERNET_IP",Integer.toString( net.smpp.NumberingPlan.INTERNET_IP.getId()));
		common_map.put("SMPP.NP.WAP_CLIENT_ID", Integer.toString(net.smpp.NumberingPlan.WAP_CLIENT_ID.getId()));
		// SMPP Registered delivery SMSC Delivery Receipt
		common_map.put("SMPP.RD.SMSCDR.NO", Integer.toString(SMSCDeliveryReceiptType.NO_SMSC_DELIVERY.getId()));
		common_map.put("SMPP.RD.SMSCDR.SUCCESS_FAILURE", Integer.toString(SMSCDeliveryReceiptType.SUCCESS_FAILURE.getId()));
		common_map.put("SMPP.RD.SMSCDR.FAILURE", Integer.toString(SMSCDeliveryReceiptType.FAILURE.getId()));
		// SMPP Registered delivery SME Orig Ack
		common_map.put("SMPP.RD.SMEOA.NO", Integer.toString(SMEOrigAckType.NO_SME_ACK.getId()));
		common_map.put("SMPP.RD.SMEOA.ACK", Integer.toString(SMEOrigAckType.SME_ACK.getId()));
		common_map.put("SMPP.RD.SMEOA.MANUAL_USER_ACK", Integer.toString(SMEOrigAckType.SME_MANUAL_USER_ACK.getId()));
		common_map.put("SMPP.RD.SMEOA.BOTH", Integer.toString(SMEOrigAckType.SME_BOTH.getId()));
		// SMPP Registered delivery Intermediate Notification
		common_map.put("SMPP.RD.IN.NO", Integer.toString(IntermediateNotificationType.NO.getId()));
		common_map.put("SMPP.RD.IN.YES", Integer.toString(IntermediateNotificationType.YES.getId()));
		// SMPP Data Coding
		common_map.put("SMPP.DC.DEFAULT", Integer.toString(Data_codingType.DEFAULT.getId()));
		common_map.put("SMPP.DC.IA5_ASCII", Integer.toString(Data_codingType.IA5_ASCII.getId()));
		common_map.put("SMPP.DC.8BIT_BINARY_1", Integer.toString(Data_codingType._8BIT_BINARY_1.getId()));
		common_map.put("SMPP.DC.ISO_8859_1", Integer.toString(Data_codingType.ISO_8859_1.getId()));
		common_map.put("SMPP.DC.8BIT_BINARY_2", Integer.toString(Data_codingType._8BIT_BINARY_2.getId()));
		common_map.put("SMPP.DC.JIS", Integer.toString(Data_codingType.JIS.getId()));
		common_map.put("SMPP.DC.ISO_8859_5", Integer.toString(Data_codingType.ISO_8859_5.getId()));
		common_map.put("SMPP.DC.ISO_8859_8", Integer.toString(Data_codingType.ISO_8859_8.getId()));
		common_map.put("SMPP.DC.UCS2", Integer.toString(Data_codingType.UCS2.getId()));
		common_map.put("SMPP.DC.PICTOGRAM", Integer.toString(Data_codingType.PICTOGRAM.getId()));
		common_map.put("SMPP.DC.ISO_2011_JP", Integer.toString(Data_codingType.ISO_2011_JP.getId()));
		common_map.put("SMPP.DC.EXTENDED_KANJI", Integer.toString(Data_codingType.EXTENDED_KANJI.getId()));
		common_map.put("SMPP.DC.KS_C_5601", Integer.toString(Data_codingType.KS_C_5601.getId()));
		
		
		
		
		
	}
	private static void init_hlr_result(){
		hlr_result_map.put("HLR.RESULT.IMSI", 0);
		hlr_result_map.put("HLR.RESULT.NNN", 1);
		hlr_result_map.put("HLR.RESULT.ANNN", 2);
		
		
	}
	private static void init_hlr(){
		// HLR
		hlr_map.put("M3UA.DPC", new M3UA_DPC(0));
		hlr_map.put("M3UA.OPC", new M3UA_OPC(1));
		hlr_map.put("SCCP.GT_CALLED.ADDRESS", new SCCP_GT_CALLED_ADDRESS(2));
		hlr_map.put("SCCP.GT_CALLING.ADDRESS", new SCCP_GT_CALLING_ADDRESS(3));
		hlr_map.put("SCCP.GT_CALLED.TT", new SCCP_GT_CALLED_TT(4));
		hlr_map.put("SCCP.GT_CALLING.TT", new SCCP_GT_CALLING_TT(5));
		hlr_map.put("SCCP.GT_CALLED.NAI", new SCCP_GT_CALLED_NAI(6));
		hlr_map.put("SCCP.GT_CALLING.NAI", new SCCP_GT_CALLING_NAI(7));
		hlr_map.put("SCCP.GT_CALLED.NP", new SCCP_GT_CALLED_NP(8));
		hlr_map.put("SCCP.GT_CALLING.NP", new SCCP_GT_CALLING_NP(9));
		hlr_map.put("SCCP.GT_CALLED.GTI", new SCCP_GT_CALLED_GTI(10));
		hlr_map.put("SCCP.GT_CALLING.GTI", new SCCP_GT_CALLING_GTI(11));
		hlr_map.put("HLR.IMSI", new HLR_IMSI());
		hlr_map.put("HLR.MSISDN", new HLR_MSISDN());
		hlr_map.put("HLR.NNN", new HLR_NNN());
		hlr_map.put("HLR.ANNN", new HLR_ANNN());
		hlr_map.put("HLR.SCA", new HLR_SCA());
		
	
	}
	private static void init_smpp(){
		// SMPP
		smpp_map.put("SMPP.IP.SOURCE", new SMPP_IP_SOURCE());
		smpp_map.put("SMPP.IP.DESTINATION", new SMPP_IP_DESTINATION());
		smpp_map.put("SMPP.TCP.SOURCE", new SMPP_TCP_SOURCE());
		smpp_map.put("SMPP.TCP.DESTINATION", new SMPP_TCP_DESTINATION());
		smpp_map.put("SMPP.SYSTEM_ID", new SMPP_SYSTEM_ID());
		smpp_map.put("SMPP.PASSWORD", new SMPP_PASSWORD());
		smpp_map.put("SMPP.SERVICE.TYPE", new SMPP_SERVICE_TYPE());
		smpp_map.put("SMPP.ORIGINATOR.TON", new SMPP_ORIGINATOR_TON());
		smpp_map.put("SMPP.ORIGINATOR.NP", new SMPP_ORIGINATOR_NP());
		smpp_map.put("SMPP.ORIGINATOR.ADDRESS", new SMPP_ORIGINATOR_ADDRESS());
		smpp_map.put("SMPP.RECIPIENT.TON", new SMPP_RECIPIENT_TON());
		smpp_map.put("SMPP.RECIPIENT.NP", new SMPP_RECIPIENT_NP());
		smpp_map.put("SMPP.RECIPIENT.ADDRESS", new SMPP_RECIPIENT_ADDRESS());
		smpp_map.put("SMPP.ESM.MESSAGE.MODE", new SMPP_ESM_MESSAGE_MODE());
		smpp_map.put("SMPP.ESM.MESSAGE.TYPE", new SMPP_ESM_MESSAGE_TYPE());
		smpp_map.put("SMPP.ESM.GSM_FEATURES", new SMPP_ESM_GSM_FEATURES());
		smpp_map.put("SMPP.PROTOCOL_ID", new SMPP_PROTOCOL_ID());
		smpp_map.put("SMPP.PRIORITY.FLAG", new SMPP_PRIORITY_FLAG());
		smpp_map.put("SMPP.DELIVERY_TIME", new SMPP_DELIVERY_TIME());
		smpp_map.put("SMPP.VALIDITY_PERIOD", new SMPP_VALIDITY_PERIOD());
		smpp_map.put("SMPP.RD.SMSC_RECEIPT", new SMPP_RD_SMSC_RECEIPT());
		smpp_map.put("SMPP.RD.SME_ACK", new SMPP_RD_SME_ACK());
		smpp_map.put("SMPP.RD.INTERMEDIATE_NOTIFICATION", new SMPP_RD_INTERMEDIATE_NOTIFICATION());
		smpp_map.put("SMPP.REPLACE_IF_PRESENT", new SMPP_REPLACE_IF_PRESENT());
		smpp_map.put("SMPP.DATA_CODING", new SMPP_DATA_CODING());
		smpp_map.put("SMPP.SM_DEFAULT_MSG_ID", new SMPP_SM_DEFAULT_MSG_ID());
		smpp_map.put("SMPP.SM_LENGTH", new SMPP_SM_LENGTH());
		smpp_map.put("SMPP.SM", new SMPP_SM());
		smpp_map.put("SMS.MSG_TYPE", new SMS_MSG_TYPE(28));
	}
	
	public static void init(){
		map = new HashMap<String, BindingDescriptor>();
		hlr_map = new HashMap<String, BindingDescriptor>();
		hlr_result_map = new HashMap<String, Integer>();
		common_map = new HashMap<String, Object>();
		smpp_map = new HashMap<String, BindingDescriptor>();
		init_common();
		
		map.put("SCCP.GT_CALLED.ADDRESS", new SCCP_GT_CALLED_ADDRESS());
		map.put("SCCP.GT_CALLING.ADDRESS", new SCCP_GT_CALLING_ADDRESS());
		map.put("MAP.SCOA", new MAP_SCOA());
		map.put("MAP.SCDA", new MAP_SCDA());
		map.put("MAP.IMSI", new MAP_IMSI());
		map.put("MAP.MSISDN", new MAP_MSISDN());
		map.put("M3UA.DPC", new M3UA_DPC());
		map.put("M3UA.OPC", new M3UA_OPC());
		map.put("SMS_TPDU.ORIGINATING", new SMS_TPDU_ORIGINATING());
		map.put("SMS_TPDU.ORIGINATING.ENC", new SMS_TPDU_ORIGINATING_ENC());
		map.put("SMS_TPDU.DESTINATION", new SMS_TPDU_DESTINATION());
		map.put("SMS_TPDU.DESTINATION.ENC", new SMS_TPDU_DESTINATION_ENC());
		map.put("SMS_TPDU.UD", new SMS_TPDU_UD());
		map.put("SMS_TPDU.DCS", new SMS_TPDU_DCS());
		map.put("SMS.MSG_TYPE", new SMS_MSG_TYPE());

		// extra SCCP
		map.put("SCCP.GT_CALLED.TT", new SCCP_GT_CALLED_TT());
		map.put("SCCP.GT_CALLING.TT", new SCCP_GT_CALLING_TT());
		map.put("SCCP.GT_CALLED.NAI", new SCCP_GT_CALLED_NAI());
		map.put("SCCP.GT_CALLING.NAI", new SCCP_GT_CALLING_NAI());
		map.put("SCCP.GT_CALLED.NP", new SCCP_GT_CALLED_NP());
		map.put("SCCP.GT_CALLING.NP", new SCCP_GT_CALLING_NP());
		map.put("SCCP.GT_CALLED.GTI", new SCCP_GT_CALLED_GTI());
		map.put("SCCP.GT_CALLING.GTI", new SCCP_GT_CALLING_GTI());
		
		// init HLR
		init_hlr();
		
		// init HLR result
		init_hlr_result();
		
		// init SMPP
		init_smpp();

		
		
		
	}
	

}
