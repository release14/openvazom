package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SRFDirectiveRes extends SET{

	public Digits get_digits(){
		return (Digits)elements.get(0).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public ScriptResult get_scriptResult(){
		return (ScriptResult)elements.get(1).data;
	}
	public ScriptResult new_scriptResult(){
		return new ScriptResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(269, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_scriptResult(); }});
	}
	public SRFDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
