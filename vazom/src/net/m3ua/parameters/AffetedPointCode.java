package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;


public class AffetedPointCode extends M3UAParameter {
	
	public AffetedPointCode(){
		type = M3UAParameterType.AFFECTED_POINT_CODE;
	}
	public void init(byte[] data) {
		value = data;
		
	}
	public void setAffetedPointCode(int val){
		value = new byte[3];
		value[0] = (byte)(val >> 16);
		value[1] = (byte)(val >> 8);
		value[2] = (byte)(val & 0xFF);
		
	}


}
