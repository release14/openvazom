package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TimeNotation extends SEQUENCE{

	public IA5STRING get_date(){
		return (IA5STRING)elements.get(0).data;
	}
	public IA5STRING new_date(){
		return new IA5STRING();
	}

	public IA5STRING get_time(){
		return (IA5STRING)elements.get(1).data;
	}
	public IA5STRING new_time(){
		return new IA5STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_date(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_time(); }});
	}
	public TimeNotation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
