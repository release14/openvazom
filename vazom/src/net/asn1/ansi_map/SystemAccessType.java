package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SystemAccessType extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _unspecified = 1;
	public static final int _flash_request = 2;
	public static final int _autonomous_registration = 3;
	public static final int _call_origination = 4;
	public static final int _page_response = 5;
	public static final int _no_access = 6;
	public static final int _power_down_registration = 7;
	public static final int _sms_page_response = 8;
	public static final int _otasp = 9;
	public SystemAccessType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
