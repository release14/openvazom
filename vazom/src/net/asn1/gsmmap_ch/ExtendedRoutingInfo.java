package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ExtendedRoutingInfo extends CHOICE{

	public RoutingInfo get_routingInfo(){
		return (RoutingInfo)elements.get(0).data;
	}
	public RoutingInfo new_routingInfo(){
		return new RoutingInfo();
	}

	public CamelRoutingInfo get_camelRoutingInfo(){
		return (CamelRoutingInfo)elements.get(1).data;
	}
	public CamelRoutingInfo new_camelRoutingInfo(){
		return new CamelRoutingInfo();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_routingInfo(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_camelRoutingInfo(); }});
	}
	public ExtendedRoutingInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
