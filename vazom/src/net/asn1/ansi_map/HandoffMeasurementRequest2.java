package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffMeasurementRequest2 extends SET{

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(0).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public CDMACallMode get_cdmaCallMode(){
		return (CDMACallMode)elements.get(1).data;
	}
	public CDMACallMode new_cdmaCallMode(){
		return new CDMACallMode();
	}

	public CDMAChannelData get_cdmaChannelData(){
		return (CDMAChannelData)elements.get(2).data;
	}
	public CDMAChannelData new_cdmaChannelData(){
		return new CDMAChannelData();
	}

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(3).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public CDMAServingOneWayDelay get_cdmaServingOneWayDelay(){
		return (CDMAServingOneWayDelay)elements.get(4).data;
	}
	public CDMAServingOneWayDelay new_cdmaServingOneWayDelay(){
		return new CDMAServingOneWayDelay();
	}

	public CDMAStationClassMark get_cdmaStationClassMark(){
		return (CDMAStationClassMark)elements.get(5).data;
	}
	public CDMAStationClassMark new_cdmaStationClassMark(){
		return new CDMAStationClassMark();
	}

	public CDMAStationClassMark2 get_cdmaStationClassMark2(){
		return (CDMAStationClassMark2)elements.get(6).data;
	}
	public CDMAStationClassMark2 new_cdmaStationClassMark2(){
		return new CDMAStationClassMark2();
	}

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(7).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public MSLocation get_msLocation(){
		return (MSLocation)elements.get(8).data;
	}
	public MSLocation new_msLocation(){
		return new MSLocation();
	}

	public NAMPSCallMode get_nampsCallMode(){
		return (NAMPSCallMode)elements.get(9).data;
	}
	public NAMPSCallMode new_nampsCallMode(){
		return new NAMPSCallMode();
	}

	public NAMPSChannelData get_nampsChannelData(){
		return (NAMPSChannelData)elements.get(10).data;
	}
	public NAMPSChannelData new_nampsChannelData(){
		return new NAMPSChannelData();
	}

	public StationClassMark get_stationClassMark(){
		return (StationClassMark)elements.get(11).data;
	}
	public StationClassMark new_stationClassMark(){
		return new StationClassMark();
	}

	public TargetCellIDList get_targetCellIDList(){
		return (TargetCellIDList)elements.get(12).data;
	}
	public TargetCellIDList new_targetCellIDList(){
		return new TargetCellIDList();
	}

	public TDMABandwidth get_tdmaBandwidth(){
		return (TDMABandwidth)elements.get(13).data;
	}
	public TDMABandwidth new_tdmaBandwidth(){
		return new TDMABandwidth();
	}

	public TDMACallMode get_tdmaCallMode(){
		return (TDMACallMode)elements.get(14).data;
	}
	public TDMACallMode new_tdmaCallMode(){
		return new TDMACallMode();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(15).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(16).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TDMATerminalCapability get_tdmaTerminalCapability(){
		return (TDMATerminalCapability)elements.get(17).data;
	}
	public TDMATerminalCapability new_tdmaTerminalCapability(){
		return new TDMATerminalCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(62, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaCallMode(); }});
		elements.add(new ElementDescriptor(63, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaChannelData(); }});
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(60, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServingOneWayDelay(); }});
		elements.add(new ElementDescriptor(59, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark(); }});
		elements.add(new ElementDescriptor(177, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark2(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(70, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msLocation(); }});
		elements.add(new ElementDescriptor(165, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsCallMode(); }});
		elements.add(new ElementDescriptor(74, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsChannelData(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_stationClassMark(); }});
		elements.add(new ElementDescriptor(207, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetCellIDList(); }});
		elements.add(new ElementDescriptor(220, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBandwidth(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaCallMode(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(179, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaTerminalCapability(); }});
	}
	public HandoffMeasurementRequest2(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
