package net.isup.parameters;

import net.isup.ParameterType;

public class AccessDeliveryInformation extends ParameterBase {
	boolean setup_message_generated;
	
	public AccessDeliveryInformation(){
		type = ParameterType.ACCESS_DELIVERY_INFORMATION;
	}
	
	public void init(byte[] data) {
		if(data != null){
			setup_message_generated = (data[0] & 0x01) == 0x01;
		}
	}

}
