// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/dfranusic/cliwalker.g 2011-04-30 20:02:25

package net.cli;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.antlr.stringtemplate.*;
import org.antlr.stringtemplate.language.*;
import java.util.HashMap;
public class cliwalker extends TreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "L_PAREN", "R_PAREN", "R_SQ_B", "L_SQ_B", "L_CR_B", "R_CR_B", "ANNT", "EQUAL", "ASSIGN", "OR", "NEQUAL", "COLON", "STMTSEP", "PERCENT", "HEX_P", "LT", "GT", "LTE", "GTE", "PLUS", "MINUS", "AND", "SQUOTE", "REGEX_BLOCK", "ASTERISK", "COMMA", "CLI_FINAL", "CLI_COMPLEX", "CLI_METHOD", "CLI_DEPTH", "CLI_PARENT", "CLI_PARAMS", "CLI_PARAM", "CLI_DESC", "WORD", "WS", "SL_COMMENT"
    };
    public static final int NEQUAL=14;
    public static final int LT=19;
    public static final int PERCENT=17;
    public static final int CLI_METHOD=32;
    public static final int CLI_PARENT=34;
    public static final int HEX_P=18;
    public static final int REGEX_BLOCK=27;
    public static final int GTE=22;
    public static final int CLI_PARAM=36;
    public static final int L_CR_B=8;
    public static final int SQUOTE=26;
    public static final int MINUS=24;
    public static final int AND=25;
    public static final int EOF=-1;
    public static final int R_CR_B=9;
    public static final int LTE=21;
    public static final int ANNT=10;
    public static final int R_PAREN=5;
    public static final int ASTERISK=28;
    public static final int CLI_PARAMS=35;
    public static final int COLON=15;
    public static final int WORD=38;
    public static final int WS=39;
    public static final int L_SQ_B=7;
    public static final int COMMA=29;
    public static final int R_SQ_B=6;
    public static final int EQUAL=11;
    public static final int OR=13;
    public static final int L_PAREN=4;
    public static final int SL_COMMENT=40;
    public static final int ASSIGN=12;
    public static final int CLI_FINAL=30;
    public static final int GT=20;
    public static final int PLUS=23;
    public static final int CLI_DESC=37;
    public static final int STMTSEP=16;
    public static final int CLI_DEPTH=33;
    public static final int CLI_COMPLEX=31;

    // delegates
    // delegators


        public cliwalker(TreeNodeStream input) {
            this(input, new RecognizerSharedState());
        }
        public cliwalker(TreeNodeStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected StringTemplateGroup templateLib =
      new StringTemplateGroup("cliwalkerTemplates", AngleBracketTemplateLexer.class);

    public void setTemplateLib(StringTemplateGroup templateLib) {
      this.templateLib = templateLib;
    }
    public StringTemplateGroup getTemplateLib() {
      return templateLib;
    }
    /** allows convenient multi-value initialization:
     *  "new STAttrMap().put(...).put(...)"
     */
    public static class STAttrMap extends HashMap {
      public STAttrMap put(String attrName, Object value) {
        super.put(attrName, value);
        return this;
      }
      public STAttrMap put(String attrName, int value) {
        super.put(attrName, new Integer(value));
        return this;
      }
    }

    public String[] getTokenNames() { return cliwalker.tokenNames; }
    public String getGrammarFileName() { return "/home/dfranusic/cliwalker.g"; }


      String inst = ""; 
      String groups = "";


    public static class input_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "input"
    // /home/dfranusic/cliwalker.g:26:1: input : (c+= cliitem )* -> class(methods=$cinstances=instgroups=groups);
    public final cliwalker.input_return input() throws RecognitionException {
        cliwalker.input_return retval = new cliwalker.input_return();
        retval.start = input.LT(1);

        List list_c=null;
        RuleReturnScope c = null;
        try {
            // /home/dfranusic/cliwalker.g:27:3: ( (c+= cliitem )* -> class(methods=$cinstances=instgroups=groups))
            // /home/dfranusic/cliwalker.g:27:5: (c+= cliitem )*
            {
            // /home/dfranusic/cliwalker.g:27:5: (c+= cliitem )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=CLI_FINAL && LA1_0<=CLI_COMPLEX)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/dfranusic/cliwalker.g:27:6: c+= cliitem
            	    {
            	    pushFollow(FOLLOW_cliitem_in_input64);
            	    c=cliitem();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if (list_c==null) list_c=new ArrayList();
            	    list_c.add(c.getTemplate());


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);



            // TEMPLATE REWRITE
            if ( state.backtracking==0 ) {
              // 27:19: -> class(methods=$cinstances=instgroups=groups)
              {
                  retval.st = templateLib.getInstanceOf("class",
                new STAttrMap().put("methods", list_c).put("instances", inst).put("groups", groups));
              }

            }
            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "input"

    public static class finalitem_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "finalitem"
    // /home/dfranusic/cliwalker.g:30:1: finalitem : ( ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.text) | ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (c+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.textmethod_params=$c));
    public final cliwalker.finalitem_return finalitem() throws RecognitionException {
        cliwalker.finalitem_return retval = new cliwalker.finalitem_return();
        retval.start = input.LT(1);

        CommonTree a=null;
        CommonTree b=null;
        CommonTree p=null;
        CommonTree dsc=null;
        CommonTree c=null;
        List list_c=null;

        try {
            // /home/dfranusic/cliwalker.g:31:3: ( ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.text) | ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (c+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.textmethod_params=$c))
            int alt3=2;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // /home/dfranusic/cliwalker.g:31:5: ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) )
                    {
                    match(input,CLI_FINAL,FOLLOW_CLI_FINAL_in_finalitem99); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    a=(CommonTree)match(input,WORD,FOLLOW_WORD_in_finalitem103); if (state.failed) return retval;
                    b=(CommonTree)match(input,WORD,FOLLOW_WORD_in_finalitem107); if (state.failed) return retval;
                    match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_finalitem110); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    p=(CommonTree)match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_finalitem114); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    match(input,CLI_DESC,FOLLOW_CLI_DESC_in_finalitem118); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    dsc=(CommonTree)match(input,CLI_DESC,FOLLOW_CLI_DESC_in_finalitem122); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                       inst += "method_lst.add(_cli_" + (b!=null?b.getText():null) + ");\n"; 
                    }


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 32:3: -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.text)
                      {
                          retval.st = templateLib.getInstanceOf("method_class",
                        new STAttrMap().put("name", (b!=null?b.getText():null)).put("method_name", (a!=null?a.getText():null)).put("method_desc", (dsc!=null?dsc.getText():null)).put("method_parent", (p!=null?p.getText():null)));
                      }

                    }
                    }
                    break;
                case 2 :
                    // /home/dfranusic/cliwalker.g:33:5: ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (c+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) )
                    {
                    match(input,CLI_FINAL,FOLLOW_CLI_FINAL_in_finalitem159); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    a=(CommonTree)match(input,WORD,FOLLOW_WORD_in_finalitem163); if (state.failed) return retval;
                    b=(CommonTree)match(input,WORD,FOLLOW_WORD_in_finalitem167); if (state.failed) return retval;
                    match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_finalitem170); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    p=(CommonTree)match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_finalitem174); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    match(input,CLI_PARAMS,FOLLOW_CLI_PARAMS_in_finalitem178); if (state.failed) return retval;

                    if ( input.LA(1)==Token.DOWN ) {
                        match(input, Token.DOWN, null); if (state.failed) return retval;
                        // /home/dfranusic/cliwalker.g:33:72: (c+= WORD )*
                        loop2:
                        do {
                            int alt2=2;
                            int LA2_0 = input.LA(1);

                            if ( (LA2_0==WORD) ) {
                                alt2=1;
                            }


                            switch (alt2) {
                        	case 1 :
                        	    // /home/dfranusic/cliwalker.g:0:0: c+= WORD
                        	    {
                        	    c=(CommonTree)match(input,WORD,FOLLOW_WORD_in_finalitem182); if (state.failed) return retval;
                        	    if (list_c==null) list_c=new ArrayList();
                        	    list_c.add(c);


                        	    }
                        	    break;

                        	default :
                        	    break loop2;
                            }
                        } while (true);


                        match(input, Token.UP, null); if (state.failed) return retval;
                    }
                    match(input,CLI_DESC,FOLLOW_CLI_DESC_in_finalitem187); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    dsc=(CommonTree)match(input,CLI_DESC,FOLLOW_CLI_DESC_in_finalitem191); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                       inst += "method_lst.add(_cli_" + (b!=null?b.getText():null) + ");\n"; 
                    }


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 34:3: -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.textmethod_params=$c)
                      {
                          retval.st = templateLib.getInstanceOf("method_class",
                        new STAttrMap().put("name", (b!=null?b.getText():null)).put("method_name", (a!=null?a.getText():null)).put("method_desc", (dsc!=null?dsc.getText():null)).put("method_parent", (p!=null?p.getText():null)).put("method_params", list_c));
                      }

                    }
                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "finalitem"

    public static class complexitem_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "complexitem"
    // /home/dfranusic/cliwalker.g:37:1: complexitem : ( ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_block_complex(methods=$cgrp_class=$a.textgrp_desc=$dsc.textgrp_parent=$p.text) | ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (prms+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_block_complex(methods=$cgrp_class=$a.textgrp_desc=$dsc.textgrp_parent=$p.textgrp_params=$prms));
    public final cliwalker.complexitem_return complexitem() throws RecognitionException {
        cliwalker.complexitem_return retval = new cliwalker.complexitem_return();
        retval.start = input.LT(1);

        CommonTree a=null;
        CommonTree p=null;
        CommonTree dsc=null;
        CommonTree prms=null;
        List list_prms=null;
        List list_c=null;
        RuleReturnScope c = null;
        try {
            // /home/dfranusic/cliwalker.g:38:3: ( ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_block_complex(methods=$cgrp_class=$a.textgrp_desc=$dsc.textgrp_parent=$p.text) | ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (prms+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_block_complex(methods=$cgrp_class=$a.textgrp_desc=$dsc.textgrp_parent=$p.textgrp_params=$prms))
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==CLI_COMPLEX) ) {
                int LA7_1 = input.LA(2);

                if ( (synpred5_cliwalker()) ) {
                    alt7=1;
                }
                else if ( (true) ) {
                    alt7=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // /home/dfranusic/cliwalker.g:38:5: ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) )
                    {
                    match(input,CLI_COMPLEX,FOLLOW_CLI_COMPLEX_in_complexitem241); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    a=(CommonTree)match(input,WORD,FOLLOW_WORD_in_complexitem246); if (state.failed) return retval;

                    if ( input.LA(1)==Token.DOWN ) {
                        match(input, Token.DOWN, null); if (state.failed) return retval;
                        // /home/dfranusic/cliwalker.g:38:29: (c+= cliitem )*
                        loop4:
                        do {
                            int alt4=2;
                            int LA4_0 = input.LA(1);

                            if ( ((LA4_0>=CLI_FINAL && LA4_0<=CLI_COMPLEX)) ) {
                                alt4=1;
                            }


                            switch (alt4) {
                        	case 1 :
                        	    // /home/dfranusic/cliwalker.g:0:0: c+= cliitem
                        	    {
                        	    pushFollow(FOLLOW_cliitem_in_complexitem250);
                        	    c=cliitem();

                        	    state._fsp--;
                        	    if (state.failed) return retval;
                        	    if (list_c==null) list_c=new ArrayList();
                        	    list_c.add(c.getTemplate());


                        	    }
                        	    break;

                        	default :
                        	    break loop4;
                            }
                        } while (true);


                        match(input, Token.UP, null); if (state.failed) return retval;
                    }
                    match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_complexitem255); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    p=(CommonTree)match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_complexitem259); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    match(input,CLI_DESC,FOLLOW_CLI_DESC_in_complexitem263); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    dsc=(CommonTree)match(input,CLI_DESC,FOLLOW_CLI_DESC_in_complexitem267); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      groups += "group_lst.add(_grp_" + (a!=null?a.getText():null) + ");\n"; 
                    }


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 39:3: -> method_block_complex(methods=$cgrp_class=$a.textgrp_desc=$dsc.textgrp_parent=$p.text)
                      {
                          retval.st = templateLib.getInstanceOf("method_block_complex",
                        new STAttrMap().put("methods", list_c).put("grp_class", (a!=null?a.getText():null)).put("grp_desc", (dsc!=null?dsc.getText():null)).put("grp_parent", (p!=null?p.getText():null)));
                      }

                    }
                    }
                    break;
                case 2 :
                    // /home/dfranusic/cliwalker.g:40:5: ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (prms+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) )
                    {
                    match(input,CLI_COMPLEX,FOLLOW_CLI_COMPLEX_in_complexitem304); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    a=(CommonTree)match(input,WORD,FOLLOW_WORD_in_complexitem309); if (state.failed) return retval;

                    if ( input.LA(1)==Token.DOWN ) {
                        match(input, Token.DOWN, null); if (state.failed) return retval;
                        // /home/dfranusic/cliwalker.g:40:29: (c+= cliitem )*
                        loop5:
                        do {
                            int alt5=2;
                            int LA5_0 = input.LA(1);

                            if ( ((LA5_0>=CLI_FINAL && LA5_0<=CLI_COMPLEX)) ) {
                                alt5=1;
                            }


                            switch (alt5) {
                        	case 1 :
                        	    // /home/dfranusic/cliwalker.g:0:0: c+= cliitem
                        	    {
                        	    pushFollow(FOLLOW_cliitem_in_complexitem313);
                        	    c=cliitem();

                        	    state._fsp--;
                        	    if (state.failed) return retval;
                        	    if (list_c==null) list_c=new ArrayList();
                        	    list_c.add(c.getTemplate());


                        	    }
                        	    break;

                        	default :
                        	    break loop5;
                            }
                        } while (true);


                        match(input, Token.UP, null); if (state.failed) return retval;
                    }
                    match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_complexitem318); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    p=(CommonTree)match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_complexitem322); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    match(input,CLI_PARAMS,FOLLOW_CLI_PARAMS_in_complexitem326); if (state.failed) return retval;

                    if ( input.LA(1)==Token.DOWN ) {
                        match(input, Token.DOWN, null); if (state.failed) return retval;
                        // /home/dfranusic/cliwalker.g:40:85: (prms+= WORD )*
                        loop6:
                        do {
                            int alt6=2;
                            int LA6_0 = input.LA(1);

                            if ( (LA6_0==WORD) ) {
                                alt6=1;
                            }


                            switch (alt6) {
                        	case 1 :
                        	    // /home/dfranusic/cliwalker.g:0:0: prms+= WORD
                        	    {
                        	    prms=(CommonTree)match(input,WORD,FOLLOW_WORD_in_complexitem330); if (state.failed) return retval;
                        	    if (list_prms==null) list_prms=new ArrayList();
                        	    list_prms.add(prms);


                        	    }
                        	    break;

                        	default :
                        	    break loop6;
                            }
                        } while (true);


                        match(input, Token.UP, null); if (state.failed) return retval;
                    }
                    match(input,CLI_DESC,FOLLOW_CLI_DESC_in_complexitem335); if (state.failed) return retval;

                    match(input, Token.DOWN, null); if (state.failed) return retval;
                    dsc=(CommonTree)match(input,CLI_DESC,FOLLOW_CLI_DESC_in_complexitem339); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;

                    match(input, Token.UP, null); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                      groups += "group_lst.add(_grp_" + (a!=null?a.getText():null) + ");\n"; 
                    }


                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 41:3: -> method_block_complex(methods=$cgrp_class=$a.textgrp_desc=$dsc.textgrp_parent=$p.textgrp_params=$prms)
                      {
                          retval.st = templateLib.getInstanceOf("method_block_complex",
                        new STAttrMap().put("methods", list_c).put("grp_class", (a!=null?a.getText():null)).put("grp_desc", (dsc!=null?dsc.getText():null)).put("grp_parent", (p!=null?p.getText():null)).put("grp_params", list_prms));
                      }

                    }
                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "complexitem"

    public static class cliitem_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start "cliitem"
    // /home/dfranusic/cliwalker.g:44:1: cliitem : ( (a+= finalitem ) -> method_block(methods=$a) | (b+= complexitem ) -> method_block(methods=$b));
    public final cliwalker.cliitem_return cliitem() throws RecognitionException {
        cliwalker.cliitem_return retval = new cliwalker.cliitem_return();
        retval.start = input.LT(1);

        List list_a=null;
        List list_b=null;
        RuleReturnScope a = null;
        RuleReturnScope b = null;
        try {
            // /home/dfranusic/cliwalker.g:45:3: ( (a+= finalitem ) -> method_block(methods=$a) | (b+= complexitem ) -> method_block(methods=$b))
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==CLI_FINAL) ) {
                alt8=1;
            }
            else if ( (LA8_0==CLI_COMPLEX) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // /home/dfranusic/cliwalker.g:45:5: (a+= finalitem )
                    {
                    // /home/dfranusic/cliwalker.g:45:5: (a+= finalitem )
                    // /home/dfranusic/cliwalker.g:45:6: a+= finalitem
                    {
                    pushFollow(FOLLOW_finalitem_in_cliitem390);
                    a=finalitem();

                    state._fsp--;
                    if (state.failed) return retval;
                    if (list_a==null) list_a=new ArrayList();
                    list_a.add(a.getTemplate());


                    }



                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 45:20: -> method_block(methods=$a)
                      {
                          retval.st = templateLib.getInstanceOf("method_block",
                        new STAttrMap().put("methods", list_a));
                      }

                    }
                    }
                    break;
                case 2 :
                    // /home/dfranusic/cliwalker.g:46:5: (b+= complexitem )
                    {
                    // /home/dfranusic/cliwalker.g:46:5: (b+= complexitem )
                    // /home/dfranusic/cliwalker.g:46:6: b+= complexitem
                    {
                    pushFollow(FOLLOW_complexitem_in_cliitem409);
                    b=complexitem();

                    state._fsp--;
                    if (state.failed) return retval;
                    if (list_b==null) list_b=new ArrayList();
                    list_b.add(b.getTemplate());


                    }



                    // TEMPLATE REWRITE
                    if ( state.backtracking==0 ) {
                      // 46:22: -> method_block(methods=$b)
                      {
                          retval.st = templateLib.getInstanceOf("method_block",
                        new STAttrMap().put("methods", list_b));
                      }

                    }
                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "cliitem"

    // $ANTLR start synpred5_cliwalker
    public final void synpred5_cliwalker_fragment() throws RecognitionException {   
        CommonTree a=null;
        CommonTree p=null;
        CommonTree dsc=null;
        List list_c=null;
        RuleReturnScope c = null;
        // /home/dfranusic/cliwalker.g:38:5: ( ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) ) )
        // /home/dfranusic/cliwalker.g:38:5: ^( CLI_COMPLEX ^(a= WORD (c+= cliitem )* ) ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) )
        {
        match(input,CLI_COMPLEX,FOLLOW_CLI_COMPLEX_in_synpred5_cliwalker241); if (state.failed) return ;

        match(input, Token.DOWN, null); if (state.failed) return ;
        a=(CommonTree)match(input,WORD,FOLLOW_WORD_in_synpred5_cliwalker246); if (state.failed) return ;

        if ( input.LA(1)==Token.DOWN ) {
            match(input, Token.DOWN, null); if (state.failed) return ;
            // /home/dfranusic/cliwalker.g:38:29: (c+= cliitem )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=CLI_FINAL && LA9_0<=CLI_COMPLEX)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // /home/dfranusic/cliwalker.g:0:0: c+= cliitem
            	    {
            	    pushFollow(FOLLOW_cliitem_in_synpred5_cliwalker250);
            	    c=cliitem();

            	    state._fsp--;
            	    if (state.failed) return ;
            	    if (list_c==null) list_c=new ArrayList();
            	    list_c.add(c);


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            match(input, Token.UP, null); if (state.failed) return ;
        }
        match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_synpred5_cliwalker255); if (state.failed) return ;

        match(input, Token.DOWN, null); if (state.failed) return ;
        p=(CommonTree)match(input,CLI_PARENT,FOLLOW_CLI_PARENT_in_synpred5_cliwalker259); if (state.failed) return ;

        match(input, Token.UP, null); if (state.failed) return ;
        match(input,CLI_DESC,FOLLOW_CLI_DESC_in_synpred5_cliwalker263); if (state.failed) return ;

        match(input, Token.DOWN, null); if (state.failed) return ;
        dsc=(CommonTree)match(input,CLI_DESC,FOLLOW_CLI_DESC_in_synpred5_cliwalker267); if (state.failed) return ;

        match(input, Token.UP, null); if (state.failed) return ;

        match(input, Token.UP, null); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred5_cliwalker

    // Delegated rules

    public final boolean synpred5_cliwalker() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_cliwalker_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA3 dfa3 = new DFA3(this);
    static final String DFA3_eotS =
        "\13\uffff";
    static final String DFA3_eofS =
        "\13\uffff";
    static final String DFA3_minS =
        "\1\36\1\2\2\46\1\42\1\2\1\42\1\3\1\43\2\uffff";
    static final String DFA3_maxS =
        "\1\36\1\2\2\46\1\42\1\2\1\42\1\3\1\45\2\uffff";
    static final String DFA3_acceptS =
        "\11\uffff\1\1\1\2";
    static final String DFA3_specialS =
        "\13\uffff}>";
    static final String[] DFA3_transitionS = {
            "\1\1",
            "\1\2",
            "\1\3",
            "\1\4",
            "\1\5",
            "\1\6",
            "\1\7",
            "\1\10",
            "\1\12\1\uffff\1\11",
            "",
            ""
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "30:1: finalitem : ( ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.text) | ^( CLI_FINAL a= WORD b= WORD ^( CLI_PARENT p= CLI_PARENT ) ^( CLI_PARAMS (c+= WORD )* ) ^( CLI_DESC dsc= CLI_DESC ) ) -> method_class(name=$b.textmethod_name=$a.textmethod_desc=$dsc.textmethod_parent=$p.textmethod_params=$c));";
        }
    }
 

    public static final BitSet FOLLOW_cliitem_in_input64 = new BitSet(new long[]{0x00000000C0000002L});
    public static final BitSet FOLLOW_CLI_FINAL_in_finalitem99 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_finalitem103 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_WORD_in_finalitem107 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_CLI_PARENT_in_finalitem110 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_PARENT_in_finalitem114 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_DESC_in_finalitem118 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_DESC_in_finalitem122 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_FINAL_in_finalitem159 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_finalitem163 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_WORD_in_finalitem167 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_CLI_PARENT_in_finalitem170 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_PARENT_in_finalitem174 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_PARAMS_in_finalitem178 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_finalitem182 = new BitSet(new long[]{0x0000004000000008L});
    public static final BitSet FOLLOW_CLI_DESC_in_finalitem187 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_DESC_in_finalitem191 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_COMPLEX_in_complexitem241 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_complexitem246 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_cliitem_in_complexitem250 = new BitSet(new long[]{0x00000000C0000008L});
    public static final BitSet FOLLOW_CLI_PARENT_in_complexitem255 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_PARENT_in_complexitem259 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_DESC_in_complexitem263 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_DESC_in_complexitem267 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_COMPLEX_in_complexitem304 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_complexitem309 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_cliitem_in_complexitem313 = new BitSet(new long[]{0x00000000C0000008L});
    public static final BitSet FOLLOW_CLI_PARENT_in_complexitem318 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_PARENT_in_complexitem322 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_PARAMS_in_complexitem326 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_complexitem330 = new BitSet(new long[]{0x0000004000000008L});
    public static final BitSet FOLLOW_CLI_DESC_in_complexitem335 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_DESC_in_complexitem339 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_finalitem_in_cliitem390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_complexitem_in_cliitem409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CLI_COMPLEX_in_synpred5_cliwalker241 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_WORD_in_synpred5_cliwalker246 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_cliitem_in_synpred5_cliwalker250 = new BitSet(new long[]{0x00000000C0000008L});
    public static final BitSet FOLLOW_CLI_PARENT_in_synpred5_cliwalker255 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_PARENT_in_synpred5_cliwalker259 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CLI_DESC_in_synpred5_cliwalker263 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_CLI_DESC_in_synpred5_cliwalker267 = new BitSet(new long[]{0x0000000000000008L});

}