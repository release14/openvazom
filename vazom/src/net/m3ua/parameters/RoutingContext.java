package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class RoutingContext extends M3UAParameter {
	
	public RoutingContext(){
		type = M3UAParameterType.ROUTING_CONTEXT;
	}
	public void setRoutingContext(int val){
		value = new byte[4];
		value[0] = (byte)(val >> 24);
		value[1] = (byte)(val >> 16);
		value[2] = (byte)(val >> 8);
		value[3] = (byte)(val & 0xFF);
		
	}
	public void init(byte[] data) {
		value = data;
		//value = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + (data[3] & 0xFF);

	}



}
