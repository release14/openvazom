/*
 CLI AST WALKER v1.0 
*/

tree grammar cliwalker;


options{
  tokenVocab=cli;
  ASTLabelType=CommonTree;
  output=template;
  backtrack=true;
}


@header{
package net.cli;
}

@members{
  String inst = ""; 
  String groups = "";
}
input
  : (c+=cliitem)* -> class(methods={$c}, instances={inst}, groups={groups})
  ;

finalitem
  : ^(CLI_FINAL a=WORD b=WORD ^(CLI_PARENT p=CLI_PARENT) ^(CLI_DESC dsc=CLI_DESC)){ inst += "method_lst.add(_cli_" + $b.text + ");\n"; } 
  -> method_class(name={$b.text}, method_name={$a.text}, method_desc={$dsc.text}, method_parent={$p.text})
  | ^(CLI_FINAL a=WORD b=WORD ^(CLI_PARENT p=CLI_PARENT) ^(CLI_PARAMS c+=WORD*) ^(CLI_DESC dsc=CLI_DESC)) { inst += "method_lst.add(_cli_" + $b.text + ");\n"; } 
  -> method_class(name={$b.text}, method_name={$a.text}, method_desc={$dsc.text}, method_parent={$p.text}, method_params={$c})
  ;

complexitem
  : ^(CLI_COMPLEX ^(a=WORD c+=cliitem*) ^(CLI_PARENT p=CLI_PARENT) ^(CLI_DESC dsc=CLI_DESC)) {groups += "group_lst.add(_grp_" + $a.text + ");\n"; }
  -> method_block_complex(methods={$c}, grp_class={$a}, grp_desc={$dsc}, grp_parent={$p.text})
  | ^(CLI_COMPLEX ^(a=WORD c+=cliitem*) ^(CLI_PARENT p=CLI_PARENT) ^(CLI_PARAMS prms+=WORD*) ^(CLI_DESC dsc=CLI_DESC)) {groups += "group_lst.add(_grp_" + $a.text + ");\n"; }
  -> method_block_complex(methods={$c}, grp_class={$a}, grp_desc={$dsc}, grp_parent={$p.text}, grp_params={$prms})
  ;

cliitem
  : (a+=finalitem) -> method_block(methods={$a})
  | (b+=complexitem) -> method_block(methods={$b})
  ;
