package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RemoteUserFreeArg extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public ExternalSignalInfo get_callInfo(){
		return (ExternalSignalInfo)elements.get(1).data;
	}
	public ExternalSignalInfo new_callInfo(){
		return new ExternalSignalInfo();
	}

	public CCBS_Feature get_ccbs_Feature(){
		return (CCBS_Feature)elements.get(2).data;
	}
	public CCBS_Feature new_ccbs_Feature(){
		return new CCBS_Feature();
	}

	public ISDN_AddressString get_translatedB_Number(){
		return (ISDN_AddressString)elements.get(3).data;
	}
	public ISDN_AddressString new_translatedB_Number(){
		return new ISDN_AddressString();
	}

	public NULL get_replaceB_Number(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_replaceB_Number(){
		return new NULL();
	}

	public AlertingPattern get_alertingPattern(){
		return (AlertingPattern)elements.get(5).data;
	}
	public AlertingPattern new_alertingPattern(){
		return new AlertingPattern();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(6).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_callInfo(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ccbs_Feature(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_translatedB_Number(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_replaceB_Number(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertingPattern(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public RemoteUserFreeArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
