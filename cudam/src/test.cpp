#include <stdio.h>
#include <string.h>
#include <time.h>
#include <cuda_runtime_api.h>
#include "ld-kernel.h"
using namespace std;


int main(){
	char L2[] =     "11LL STUFF FOR FREE ONLY 99,99SELL STUFF FOR FREE ONLY 99,99SELL STUFF FOR FREE ONLY 99,99SELL STUFF FOR FREE ONLY 99,99SELL STUFF FOR FREE ";
	int strLen = strlen(L2);
	int *pstrLen = &strLen;
	char *sms_data = new char[102400*161];
	int *result = new int[102400];
        cudaEvent_t start, stop;
        float elapsed;
	printf("%s\n", "Initializing...");
	printf("%s\n", "Loading messages...");
	
	FILE *pfile;
        char line[161];
        int l = 0;
	int sc = 0;
	char fc;
        pfile = fopen("msg1.tmp", "r");    
	while((fc = fgetc(pfile)) != EOF){
		if(fc != 10) line[sc++] = fc;	
		else{
			for(int i = sc; i>0; i--) line[i] = line[i-1];
			line[0] = sc;
			memcpy(sms_data+l*161, line, sc+1);
			sc = 0;
			l++;
		}
		
	}

	printf("[%d] messages loaded...\n", l);
        fclose(pfile);
	
	int *result_int = new int[102400];

	char *device_lst;
	int *device_lst_size;
	int *device_max_i_size;
	char *device_str;
	int *device_str_len;
	int *device_result;
	int *device_total_ld;
	int mr1, mr2, mr3, mr4, mr5, mr6;
        size_t *free_mem = new size_t;
        size_t *total_mem = new size_t;
        cudaMemGetInfo(free_mem, total_mem);
        printf("Cuda Memory info[Before allocation]: Free[%dMb], Total=[%dMb]\n", *free_mem/1024/1024, *total_mem/1024/1024);
	printf("%s\n", "1. Allocating memory on CUDA device!");
	// device mem init
	mr2 = cudaMalloc((void**)&device_result, sizeof(int)*102400);
	mr3 = cudaMalloc((void**)&device_lst, sizeof(char)*102400*161);
	cudaMalloc((void**)&device_lst_size, sizeof(int));
	cudaMalloc((void**)&device_max_i_size, sizeof(int));
	mr4 = cudaMalloc((void**)&device_str, sizeof(char)*strlen(L2));
	mr5 = cudaMalloc((void**)&device_str_len, sizeof(int));
	mr6 = cudaMalloc((void**)&device_total_ld, sizeof(int));
	if(/*mr1 != 0 ||*/ mr2 != 0 || mr3 != 0 || mr4 != 0 || mr5 != 0 || mr6 != 0){
		printf("%s\n", "Memory allocation error!");
		printf("mr1[%dMb]=%d, mr2=%d, mr3=%d, mr4=%d, mr5=%d, mr6=%d\n", (sizeof(unsigned char)*102400*161*161)/1024/1024, mr1, mr2, mr3, mr4, mr5, mr6);
		return 1;
	}
	
        cudaMemGetInfo(free_mem, total_mem);
        printf("Cuda Memory info[After allocation]: Free=[%dMb], Total=[%dMb]\n", *free_mem/1024/1024, *total_mem/1024/1024);

	printf("%s\n", "2. Copying memory from Host to CUDA device!");
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, 0);

	cudaMemcpy(device_str, L2, sizeof(char)*strlen(L2),  cudaMemcpyHostToDevice);	
	cudaMemcpy(device_str_len, pstrLen, sizeof(int),  cudaMemcpyHostToDevice);	
	
	cudaMemcpy(device_lst, sms_data, sizeof(char)*102400*161, cudaMemcpyHostToDevice);
	int lst_size = 102400;
	int max_i_size = 160;
	cudaMemcpy(device_lst_size, &lst_size, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(device_max_i_size, &max_i_size, sizeof(int), cudaMemcpyHostToDevice);

	cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsed, start, stop);
        cudaEventDestroy(start);
        cudaEventDestroy(stop);
	printf("Elapsed=[%fmsec]\n", elapsed);
        dim3 threads(512, 1);
        dim3 blocks(200, 1);

	printf("3. Executing kernel [%d x %d x %d = %d] on CUDA device!\n", blocks.x, blocks.y, threads.x, blocks.x*blocks.y*threads.x);


	cudaThreadSynchronize();
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	LD_kernel(device_lst, device_lst_size, device_max_i_size, device_str, device_str_len, device_result, blocks, threads);
	cudaThreadSynchronize();
	cudaMemcpy(result, device_result, sizeof(int)*102400,  cudaMemcpyDeviceToHost);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsed, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);
	

	printf("Elapsed=[%fmsec]\n", elapsed);
	// print result
	printf("%s\n", "4. Copying memory from CUDA device to Host!");
	printf("TOTAL=%d\n", result[0]);
	// free memory
	delete[] sms_data;
	delete[] result;
	// free device memory
	cudaFree(device_result);
	cudaFree(device_lst);
	cudaFree(device_str);
	cudaFree(device_str_len);
	cudaFree(device_total_ld);
        cudaMemGetInfo(free_mem, total_mem);
        printf("Cuda Memory info[After deallocation]: Free=[%dMb], Total=[%dMb]\n", *free_mem/1024/1024, *total_mem/1024/1024);
	delete free_mem;
        delete total_mem;

	int err = cudaGetLastError();
	printf("LAST ERROR=%d\n", err);
	return 0;


}
