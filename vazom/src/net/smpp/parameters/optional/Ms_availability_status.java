package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Ms_availability_status extends OptionalParameterBase {
	public enum ValueType{
		AVAILABLE(0x00),
		DENIED(0x01),
		UNAVAILABLE(0x02);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public ValueType value;
	public Ms_availability_status(){
		type = OptionalParameterType.MS_AVAILABILITY_STATUS;
	}

	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
