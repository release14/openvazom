package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DigitMapDescriptor extends SEQUENCE{

	public DigitMapName get_digitMapName(){
		return (DigitMapName)elements.get(0).data;
	}
	public DigitMapName new_digitMapName(){
		return new DigitMapName();
	}

	public DigitMapValue get_digitMapValue(){
		return (DigitMapValue)elements.get(1).data;
	}
	public DigitMapValue new_digitMapValue(){
		return new DigitMapValue();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digitMapName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digitMapValue(); }});
	}
	public DigitMapDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
