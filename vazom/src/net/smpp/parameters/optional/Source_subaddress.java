package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;
import net.utils.Utils;

public class Source_subaddress extends OptionalParameterBase {
	public enum ValueType{
		RESERVED_1(0x01),
		RESERVED_2(0x02),
		NSAP_EVEN(0x80),
		NSAP_ODD(0x88);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	
	public Source_subaddress(){
		type = OptionalParameterType.SOURCE_SUBADDRESS;
	}
	public ValueType subaddressType;
	public String subaddress;
	
	public void init(byte[] data) {
		subaddressType = ValueType.get(data[0] & 0xff);
		subaddress = Utils.Cstr_decode(data, 1);
	}

}
