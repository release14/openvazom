package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceChangeResParm extends SEQUENCE{

	public MIdWrapper get_serviceChangeMgcId(){
		return (MIdWrapper)elements.get(0).data;
	}
	public MIdWrapper new_serviceChangeMgcId(){
		return new MIdWrapper();
	}

	public ServiceChangeAddressWrapper get_serviceChangeAddress(){
		return (ServiceChangeAddressWrapper)elements.get(1).data;
	}
	public ServiceChangeAddressWrapper new_serviceChangeAddress(){
		return new ServiceChangeAddressWrapper();
	}

	public INTEGER get_serviceChangeVersion(){
		return (INTEGER)elements.get(2).data;
	}
	public INTEGER new_serviceChangeVersion(){
		return new INTEGER();
	}

	public ServiceChangeProfile get_serviceChangeProfile(){
		return (ServiceChangeProfile)elements.get(3).data;
	}
	public ServiceChangeProfile new_serviceChangeProfile(){
		return new ServiceChangeProfile();
	}

	public TimeNotation get_timestamp(){
		return (TimeNotation)elements.get(4).data;
	}
	public TimeNotation new_timestamp(){
		return new TimeNotation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeMgcId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeAddress(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeVersion(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeProfile(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timestamp(); }});
	}
	public ServiceChangeResParm(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
