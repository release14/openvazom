package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAuditParameter extends CHOICE{

	public IndAudMediaDescriptor get_indaudmediaDescriptor(){
		return (IndAudMediaDescriptor)elements.get(0).data;
	}
	public IndAudMediaDescriptor new_indaudmediaDescriptor(){
		return new IndAudMediaDescriptor();
	}

	public IndAudEventsDescriptor get_indaudeventsDescriptor(){
		return (IndAudEventsDescriptor)elements.get(1).data;
	}
	public IndAudEventsDescriptor new_indaudeventsDescriptor(){
		return new IndAudEventsDescriptor();
	}

	public IndAudEventBufferDescriptor get_indaudeventBufferDescriptor(){
		return (IndAudEventBufferDescriptor)elements.get(2).data;
	}
	public IndAudEventBufferDescriptor new_indaudeventBufferDescriptor(){
		return new IndAudEventBufferDescriptor();
	}

	public IndAudSignalsDescriptorWrapper get_indaudsignalsDescriptor(){
		return (IndAudSignalsDescriptorWrapper)elements.get(3).data;
	}
	public IndAudSignalsDescriptorWrapper new_indaudsignalsDescriptor(){
		return new IndAudSignalsDescriptorWrapper();
	}

	public IndAudDigitMapDescriptor get_indauddigitMapDescriptor(){
		return (IndAudDigitMapDescriptor)elements.get(4).data;
	}
	public IndAudDigitMapDescriptor new_indauddigitMapDescriptor(){
		return new IndAudDigitMapDescriptor();
	}

	public IndAudStatisticsDescriptor get_indaudstatisticsDescriptor(){
		return (IndAudStatisticsDescriptor)elements.get(5).data;
	}
	public IndAudStatisticsDescriptor new_indaudstatisticsDescriptor(){
		return new IndAudStatisticsDescriptor();
	}

	public IndAudPackagesDescriptor get_indaudpackagesDescriptor(){
		return (IndAudPackagesDescriptor)elements.get(6).data;
	}
	public IndAudPackagesDescriptor new_indaudpackagesDescriptor(){
		return new IndAudPackagesDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indaudmediaDescriptor(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indaudeventsDescriptor(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indaudeventBufferDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indaudsignalsDescriptor(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indauddigitMapDescriptor(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indaudstatisticsDescriptor(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_indaudpackagesDescriptor(); }});
	}
	public IndAuditParameter(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
