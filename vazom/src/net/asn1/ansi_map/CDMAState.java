package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMAState extends OCTET_STRING{

	public CDMAState(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
