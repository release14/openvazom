package net.ds.smpp;

import net.smpp.ErrorCodeType;
import net.smpp.PDUType;
import net.smpp.pdu.PDUBase;

public class SMPPSequenceDescriptor {
	public long sequence;
	public PDUType pdu_type;
	public PDUBase pdu;
	public ErrorCodeType response_code;
}
