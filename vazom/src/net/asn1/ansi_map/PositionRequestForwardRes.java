package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PositionRequestForwardRes extends SET{

	public MSCID get_mscid(){
		return (MSCID)elements.get(0).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSStatus get_mSStatus(){
		return (MSStatus)elements.get(1).data;
	}
	public MSStatus new_mSStatus(){
		return new MSStatus();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(2).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(3).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(313, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSStatus(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
	}
	public PositionRequestForwardRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
