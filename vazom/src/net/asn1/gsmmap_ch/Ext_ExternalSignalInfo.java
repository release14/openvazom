package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Ext_ExternalSignalInfo extends SEQUENCE{

	public Ext_ProtocolId get_ext_ProtocolId(){
		return (Ext_ProtocolId)elements.get(0).data;
	}
	public Ext_ProtocolId new_ext_ProtocolId(){
		return new Ext_ProtocolId();
	}

	public SignalInfo get_signalInfo(){
		return (SignalInfo)elements.get(1).data;
	}
	public SignalInfo new_signalInfo(){
		return new SignalInfo();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_ext_ProtocolId(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_signalInfo(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public Ext_ExternalSignalInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
