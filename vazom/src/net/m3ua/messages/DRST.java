package net.m3ua.messages;

import java.util.ArrayList;
import java.util.Arrays;

import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.AffetedPointCode;
import net.m3ua.parameters.InfoString;
import net.m3ua.parameters.M3UAParameter;
import net.m3ua.parameters.NetworkAppearance;
import net.m3ua.parameters.RoutingContext;
import net.m3ua.parameters.UserCause;

public class DRST extends M3UAMessage {
	public NetworkAppearance networkAppearance;
	public RoutingContext routingContext;
	public AffetedPointCode affectedPointCode;
	public InfoString infoString;
	
	public DRST(){
		type = M3UAMessageType.DRST;
		
	}
	public void init(byte[] data) {
		int tag;
		int l;
		int m;
		M3UAParameterType pt;
		while(byte_pos < data.length){
			tag = (data[byte_pos] << 8) + (data[byte_pos + 1] & 0xFF);
			byte_pos += 2;
			// length(l) = two bytes for tag + two bytes for length + length of actual data
			l = (data[byte_pos] << 8) + (data[byte_pos + 1] & 0xFF);
			byte_pos += 2;
			
			pt = M3UAParameterType.get(tag);
			switch(pt){
				case NETWORK_APPEARANCE:
					networkAppearance = new NetworkAppearance();
					networkAppearance.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				case ROUTING_CONTEXT:
					routingContext = new RoutingContext();
					routingContext.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				case AFFECTED_POINT_CODE:
					affectedPointCode = new AffetedPointCode();
					affectedPointCode.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
				case INFO_STRING:
					infoString = new InfoString();
					infoString.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l - 4));
					break;
			}

			// parameter has to be a multiple of 4, if not, zero padding is added
			m = l % 4;
			byte_pos += l + (m > 0 ? 4 - m : 0) - 4;
			
		}

		
	}

	@Override
	public int encode(ArrayList<Byte> buff) {
		// TODO Auto-generated method stub
		return 0;
	}

}
