package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionPDU extends SEQUENCE{

	public TransactionID get_identifier(){
		return (TransactionID)elements.get(0).data;
	}
	public TransactionID new_identifier(){
		return new TransactionID();
	}

	public DialoguePortion get_dialoguePortion(){
		return (DialoguePortion)elements.get(1).data;
	}
	public DialoguePortion new_dialoguePortion(){
		return new DialoguePortion();
	}

	public ComponentSequence get_componentPortion(){
		return (ComponentSequence)elements.get(2).data;
	}
	public ComponentSequence new_componentPortion(){
		return new ComponentSequence();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(7, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_identifier(); }});
		elements.add(new ElementDescriptor(25, ASNTagClass.PRIVATE, true, false){public void set(){ data = new_dialoguePortion(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.PRIVATE, true, false){public void set(){ data = new_componentPortion(); }});
	}
	public TransactionPDU(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
