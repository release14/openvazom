package net.fn;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.config.FNConfigData;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.stats.StatsManager;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.VSTP;

import org.apache.log4j.Logger;

public class SGDescriptor {
	public static Logger logger=Logger.getLogger(SGDescriptor.class);
	public String id;
	public String address;
	public int port;
	private boolean stopping;
	public int sctp_id = -1;
	public ConcurrentLinkedQueue<MessageDescriptor> private_out_queue;
	private Reader reader_r;
	private Writer writer_r;
	private Thread reader_t;
	private Thread writer_t;
	public boolean active;
	//private ConcurrentLinkedQueue<MessageDescriptor> in_queue;
	//private ConcurrentLinkedQueue<MessageDescriptor> out_queue;

	private class Reader implements Runnable{
		public void run() {
			SSCTPDescriptor sd = null;
			MessageDescriptor md = null;
			logger.info("Starting...");
			while(!stopping){
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						//System.out.println("---------VSTP PACKET START----------");
						//System.out.println(new String(sd.payload));
						//System.out.println("---------VSTP PACKET END----------");
						
						md = VSTP.decode(sd.payload);
						if(md != null) FNode.in_offer(md);
					}
				}else{
					stopping = true;
					logger.info("Deactivating SG Connection [" + id + "]!");
					SGNManager.deactivateNode(id);
					// stats
					StatsManager.VSTP_STATS.VSTP_SG_CONNECTION_LOST++;

				}
			}
			logger.info("Ending...");
			
		}
	}	
	private class Writer implements Runnable{
		public void run() {
			MessageDescriptor md = null;
			MessageDescriptor md_p = null;
			int res;
			logger.info("Starting...");
			while(!stopping){
				md = FNode.out_poll();
				if(md != null){
					md.header.source = LocationType.FN;
					md.header.source_id = FNConfigData.fn_id;
					md.header.destination = LocationType.SGN;
					md.header.destination_id = id;
					res = SSctp.send(sctp_id, md.encode(), 0);
					if(res == -1){
						// send back to queue
						FNode.out_offer(md);
						// remove connection
						stopping = true;
						logger.info("Deactivating SG Connection [" + id + "]!");
						SGNManager.deactivateNode(id);
						// stats
						StatsManager.VSTP_STATS.VSTP_SG_CONNECTION_LOST++;

					}
					
				}
				// private queue
				md_p = private_out_queue.poll();
				if(md_p != null){
					// set destination id
					md_p.header.destination_id = id;
					md_p.header.source_id = FNConfigData.fn_id;
					// encode and send
					SSctp.send(sctp_id, md_p.encode(), 0);

				}

				if(md == null && md_p == null){
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
				
					
			}
			logger.info("Ending...");
			
		}
	}	
	public void stop(){
		stopping = true;
	}
	public void start(){
		active = true;
		stopping = false;
		logger.info("Starting SGN Connection id = [" + id + "]!");
		reader_r = new Reader();
		reader_t = new Thread(reader_r, "SG_READER_" + id);
		writer_r = new Writer();
		writer_t = new Thread(writer_r, "SG_WRITER_" + id);
		reader_t.start();
		writer_t.start();
		
		
	}
	public SGDescriptor(String _id, String _address, int _port){
		//in_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		//out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		private_out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		id = _id;
		address = _address;
		port = _port;
	}
	
}
