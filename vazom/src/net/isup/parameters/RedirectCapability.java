package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectCapability extends ParameterBase {
	public RedirectCapability(){
		type = ParameterType.REDIRECT_CAPABILITY;
	}
}
