package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DeregistrationType extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _deregister_for_an_unspecified_reason =  1 ;
	public static final int _deregister_for_an_administrative_reason =  2 ;
	public static final int _deregister_due_to_MS_power_down =  3 ;
	public DeregistrationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
