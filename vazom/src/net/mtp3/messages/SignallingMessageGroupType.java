package net.mtp3.messages;

import java.util.HashMap;

public enum SignallingMessageGroupType {
	CHM(0x01),// Changeover and changeback messages
	ECM(0x02),// Emergency changeover message
	FCM(0x03),// Transfer-controlled and signalling route set congestion messages
	TFM(0x04),// Transfer-prohibited-allowed-restricted messages
	RSM(0x05),// Signalling-route-set-test messages
	MIM(0x06),// Management inhibit messages
	TRM(0x07),// Traffic restart allowed message
	DLM(0x08),// Signalling-data-link-connection messages
	UFC(0x0a);// User part flow control messages
	
	private int id;
	private static final HashMap<Integer, SignallingMessageGroupType> lookup = new HashMap<Integer, SignallingMessageGroupType>();
	static{
		for(SignallingMessageGroupType td : SignallingMessageGroupType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static SignallingMessageGroupType get(int id){ return lookup.get(id); }
	private SignallingMessageGroupType(int _id){ id = _id; }	
}
