package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemPage2Res extends SET{

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(0).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public AuthenticationResponseBaseStation get_authenticationResponseBaseStation(){
		return (AuthenticationResponseBaseStation)elements.get(1).data;
	}
	public AuthenticationResponseBaseStation new_authenticationResponseBaseStation(){
		return new AuthenticationResponseBaseStation();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(2).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(3).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public RANDC get_randc(){
		return (RANDC)elements.get(4).data;
	}
	public RANDC new_randc(){
		return new RANDC();
	}

	public RandomVariableBaseStation get_randomVariableBaseStation(){
		return (RandomVariableBaseStation)elements.get(5).data;
	}
	public RandomVariableBaseStation new_randomVariableBaseStation(){
		return new RandomVariableBaseStation();
	}

	public SystemAccessType get_systemAccessType(){
		return (SystemAccessType)elements.get(6).data;
	}
	public SystemAccessType new_systemAccessType(){
		return new SystemAccessType();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(7).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public TDMADataMode get_tdmaDataMode(){
		return (TDMADataMode)elements.get(8).data;
	}
	public TDMADataMode new_tdmaDataMode(){
		return new TDMADataMode();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(9).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(10).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(36, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_authenticationResponseBaseStation(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randc(); }});
		elements.add(new ElementDescriptor(41, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_randomVariableBaseStation(); }});
		elements.add(new ElementDescriptor(34, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemAccessType(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemCapabilities(); }});
		elements.add(new ElementDescriptor(222, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaDataMode(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
	}
	public InterSystemPage2Res(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
