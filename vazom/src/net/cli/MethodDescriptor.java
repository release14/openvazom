package net.cli;

public abstract class MethodDescriptor extends DescriptorBase {

	public MethodDescriptor(String name, String description, String parent, String params) {
		super(name, description, parent, params);
		type = MethodType.FINAL;
	}

	
}
