package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ProvideRoamingNumberArg extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public ISDN_AddressString get_msc_Number(){
		return (ISDN_AddressString)elements.get(1).data;
	}
	public ISDN_AddressString new_msc_Number(){
		return new ISDN_AddressString();
	}

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(2).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public LMSI get_lmsi(){
		return (LMSI)elements.get(3).data;
	}
	public LMSI new_lmsi(){
		return new LMSI();
	}

	public ExternalSignalInfo get_gsm_BearerCapability(){
		return (ExternalSignalInfo)elements.get(4).data;
	}
	public ExternalSignalInfo new_gsm_BearerCapability(){
		return new ExternalSignalInfo();
	}

	public ExternalSignalInfo get_networkSignalInfo(){
		return (ExternalSignalInfo)elements.get(5).data;
	}
	public ExternalSignalInfo new_networkSignalInfo(){
		return new ExternalSignalInfo();
	}

	public SuppressionOfAnnouncement get_suppressionOfAnnouncement(){
		return (SuppressionOfAnnouncement)elements.get(6).data;
	}
	public SuppressionOfAnnouncement new_suppressionOfAnnouncement(){
		return new SuppressionOfAnnouncement();
	}

	public ISDN_AddressString get_gmsc_Address(){
		return (ISDN_AddressString)elements.get(7).data;
	}
	public ISDN_AddressString new_gmsc_Address(){
		return new ISDN_AddressString();
	}

	public CallReferenceNumber get_callReferenceNumber(){
		return (CallReferenceNumber)elements.get(8).data;
	}
	public CallReferenceNumber new_callReferenceNumber(){
		return new CallReferenceNumber();
	}

	public NULL get_or_Interrogation(){
		return (NULL)elements.get(9).data;
	}
	public NULL new_or_Interrogation(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(10).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public AlertingPattern get_alertingPattern(){
		return (AlertingPattern)elements.get(11).data;
	}
	public AlertingPattern new_alertingPattern(){
		return new AlertingPattern();
	}

	public NULL get_ccbs_Call(){
		return (NULL)elements.get(12).data;
	}
	public NULL new_ccbs_Call(){
		return new NULL();
	}

	public SupportedCamelPhases get_supportedCamelPhasesInInterrogatingNode(){
		return (SupportedCamelPhases)elements.get(13).data;
	}
	public SupportedCamelPhases new_supportedCamelPhasesInInterrogatingNode(){
		return new SupportedCamelPhases();
	}

	public Ext_ExternalSignalInfo get_additionalSignalInfo(){
		return (Ext_ExternalSignalInfo)elements.get(14).data;
	}
	public Ext_ExternalSignalInfo new_additionalSignalInfo(){
		return new Ext_ExternalSignalInfo();
	}

	public NULL get_orNotSupportedInGMSC(){
		return (NULL)elements.get(15).data;
	}
	public NULL new_orNotSupportedInGMSC(){
		return new NULL();
	}

	public NULL get_pre_pagingSupported(){
		return (NULL)elements.get(16).data;
	}
	public NULL new_pre_pagingSupported(){
		return new NULL();
	}

	public NULL get_longFTN_Supported(){
		return (NULL)elements.get(17).data;
	}
	public NULL new_longFTN_Supported(){
		return new NULL();
	}

	public NULL get_suppress_VT_CSI(){
		return (NULL)elements.get(18).data;
	}
	public NULL new_suppress_VT_CSI(){
		return new NULL();
	}

	public OfferedCamel4CSIs get_offeredCamel4CSIsInInterrogatingNode(){
		return (OfferedCamel4CSIs)elements.get(19).data;
	}
	public OfferedCamel4CSIs new_offeredCamel4CSIsInInterrogatingNode(){
		return new OfferedCamel4CSIs();
	}

	public NULL get_mtRoamingRetrySupported(){
		return (NULL)elements.get(20).data;
	}
	public NULL new_mtRoamingRetrySupported(){
		return new NULL();
	}

	public PagingArea get_pagingArea(){
		return (PagingArea)elements.get(21).data;
	}
	public PagingArea new_pagingArea(){
		return new PagingArea();
	}

	public EMLPP_Priority get_callPriority(){
		return (EMLPP_Priority)elements.get(22).data;
	}
	public EMLPP_Priority new_callPriority(){
		return new EMLPP_Priority();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_msc_Number(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lmsi(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_gsm_BearerCapability(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkSignalInfo(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_suppressionOfAnnouncement(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_gmsc_Address(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callReferenceNumber(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_or_Interrogation(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_alertingPattern(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ccbs_Call(); }});
		elements.add(new ElementDescriptor(15, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_supportedCamelPhasesInInterrogatingNode(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additionalSignalInfo(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_orNotSupportedInGMSC(); }});
		elements.add(new ElementDescriptor(17, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_pre_pagingSupported(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_longFTN_Supported(); }});
		elements.add(new ElementDescriptor(19, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_suppress_VT_CSI(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_offeredCamel4CSIsInInterrogatingNode(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_mtRoamingRetrySupported(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_pagingArea(); }});
		elements.add(new ElementDescriptor(23, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_callPriority(); }});
	}
	public ProvideRoamingNumberArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
