package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CallReportData extends SEQUENCE{

	public MonitoringMode get_monitoringMode(){
		return (MonitoringMode)elements.get(0).data;
	}
	public MonitoringMode new_monitoringMode(){
		return new MonitoringMode();
	}

	public CallOutcome get_callOutcome(){
		return (CallOutcome)elements.get(1).data;
	}
	public CallOutcome new_callOutcome(){
		return new CallOutcome();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_monitoringMode(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callOutcome(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public CallReportData(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
