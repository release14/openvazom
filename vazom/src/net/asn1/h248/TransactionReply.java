package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionReply extends SEQUENCE{

	public TransactionId get_transactionId(){
		return (TransactionId)elements.get(0).data;
	}
	public TransactionId new_transactionId(){
		return new TransactionId();
	}

	public NULL get_immAckRequired(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_immAckRequired(){
		return new NULL();
	}

	public TransactionResultChoiceWrapper get_transactionResult(){
		return (TransactionResultChoiceWrapper)elements.get(2).data;
	}
	public TransactionResultChoiceWrapper new_transactionResult(){
		return new TransactionResultChoiceWrapper();
	}

	public SegmentNumber get_segmentNumber(){
		return (SegmentNumber)elements.get(3).data;
	}
	public SegmentNumber new_segmentNumber(){
		return new SegmentNumber();
	}

	public NULL get_segmentationComplete(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_segmentationComplete(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_immAckRequired(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionResult(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_segmentNumber(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_segmentationComplete(); }});
	}
	public TransactionReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
