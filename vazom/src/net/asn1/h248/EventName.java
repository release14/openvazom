package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class EventName extends OCTET_STRING{

	public static final String __ = "PkgdName";
	public EventName(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
