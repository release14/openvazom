package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendRoutingInfoResV2 extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public RoutingInfo get_routingInfo(){
		return (RoutingInfo)elements.get(1).data;
	}
	public RoutingInfo new_routingInfo(){
		return new RoutingInfo();
	}

	public CUG_CheckInfo get_cug_CheckInfo(){
		return (CUG_CheckInfo)elements.get(2).data;
	}
	public CUG_CheckInfo new_cug_CheckInfo(){
		return new CUG_CheckInfo();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_routingInfo(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_cug_CheckInfo(); }});
	}
	public SendRoutingInfoResV2(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
