package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class CorrelationId extends M3UAParameter {
	public int correlationId;
	public CorrelationId(){
		type = M3UAParameterType.CORRELATION_ID;
	}
	
	public void init(byte[] data) {
		value = data;
		correlationId = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + (data[3] & 0xFF);

	}

}
