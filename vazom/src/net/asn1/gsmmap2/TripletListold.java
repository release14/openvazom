package net.asn1.gsmmap2;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TripletListold extends SEQUENCE{

	public AuthenticationTriplet_v2 new_child(){
		return new AuthenticationTriplet_v2();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public AuthenticationTriplet_v2 getChild(int index){
		return (AuthenticationTriplet_v2)of_children.get(index);
	}
	public TripletListold(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
