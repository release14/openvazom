package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PrivateExtension extends SEQUENCE{

	public OBJECT_IDENTIFIER get_extId(){
		return (OBJECT_IDENTIFIER)elements.get(0).data;
	}
	public OBJECT_IDENTIFIER new_extId(){
		return new OBJECT_IDENTIFIER();
	}

	public ANY get_extType(){
		return (ANY)elements.get(1).data;
	}
	public ANY new_extType(){
		return new ANY();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_extId(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extType(); }});
	}
	public PrivateExtension(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
