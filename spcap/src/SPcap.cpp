#include "SPcap.h"
#include <string.h>
using namespace std;

// static list of active streams
map<int, stream_descriptor*> SPcap::stream_lst;
char SPcap::errbuf[PCAP_ERRBUF_SIZE];
int SPcap::NEXT_STREAM_ID;
pthread_mutex_t SPcap::stram_lst_mt;

SPcap::SPcap() {
}


void SPcap::init(){
	//SPcap::MAX_Q_LENGTH = l;
	SPcap::NEXT_STREAM_ID = 1;
	// init mutex
	pthread_mutex_init(&SPcap::stram_lst_mt, NULL);

}
SPcap::~SPcap() {
	// TODO Auto-generated destructor stub
}
void SPcap::addToStreamList(stream_descriptor *sd){
	//TODO
	// should use mutex
	pthread_mutex_lock(&SPcap::stram_lst_mt);

	stream_lst[sd->id] = sd;
	pthread_mutex_unlock(&SPcap::stram_lst_mt);
	//stream_lst.push_back(sd);


}
#ifdef __PFRING__
stream_descriptor *SPcap::initCapture(string device, int snaplen, int max_q_size){
	stream_descriptor *sd = NULL;
	pfring *handle = pfring_open(const_cast<char*>(device.c_str()), 1, snaplen, 1);
	if(handle == NULL){
		cout << "pfring_open: error while loading!" << endl;
	}else{
		int err = pfring_enable_ring(handle);
        if(err == 0){
			sd = new stream_descriptor;
			// set values
			sd->handle = handle;
			sd->id = SPcap::NEXT_STREAM_ID++;
			sd->MAX_Q_SIZE = max_q_size;
			sd->MAX_Q_LIMIT_REACHED = 0;
			sd->q = new deque<packet_descriptor*>;
			sd->tmp_q = new deque<packet_descriptor*>;
			// init mutex
			pthread_mutex_init(&sd->q_lock, NULL);
			//add to list
			addToStreamList(sd);
        }else cout << "pfring_enable_ring: error while enabling ring, erro number = " << err << endl;

	}
	return sd;
}
#else

stream_descriptor *SPcap::initCapture(string device, int snaplen, int max_q_size){
	stream_descriptor *sd = NULL;

	pcap_t *handle = pcap_open_live(device.c_str(), snaplen, 1, 10000, errbuf);
	if(handle == NULL){
		cout << errbuf << endl;
	}else{
		sd = new stream_descriptor;
		// set values
		sd->handle = handle;
		sd->id = SPcap::NEXT_STREAM_ID++;
		sd->MAX_Q_SIZE = max_q_size;
		sd->MAX_Q_LIMIT_REACHED = 0;
		sd->q = new deque<packet_descriptor*>;
		sd->tmp_q = new deque<packet_descriptor*>;
		// init mutex
		pthread_mutex_init(&sd->q_lock, NULL);
		//add to list
		addToStreamList(sd);


	}
	return sd;

}
#endif

stream_descriptor* SPcap::getStream(int id){
	stream_descriptor *sd = NULL;
	pthread_mutex_lock(&SPcap::stram_lst_mt);
	sd = stream_lst[id];
	pthread_mutex_unlock(&SPcap::stram_lst_mt);
	return sd;
}

#ifdef __PFRING__
void SPcap::stopCapture(int id){
	stream_descriptor *sd = getStream(id);
	if(sd){
		pfring_breakloop(sd->handle);
	}

}
#else
void SPcap::stopCapture(int id){
	stream_descriptor *sd = getStream(id);
	if(sd){
		pcap_breakloop(sd->handle);
	}

}
#endif
void SPcap::startCapture(int id){
	//cout << "startCapture " << (int)id << endl;
	stream_descriptor *sd = getStream(id);
	//cout << "startCapture " << (int)sd->id << endl;
	if(sd){
		//cout << "startCapture" << endl;
		// init thread
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        sched_param sp;
        sp.__sched_priority = 99;
        pthread_attr_setschedparam(&attr, &sp);

		pthread_create(&sd->thread, &attr, capture_thread, (void *)sd);
		// pthread name
		char *pthread_name = new char[30];
		sprintf(pthread_name, "spcap_pthread_%i", id);
		pthread_setname_np(sd->thread, pthread_name);
		//pthread_join(sd->thread, NULL);
		//pcap_loop(sd->handle, -1, packet_received, &id);
	}else{
		cout << "Stream ID [ " << id << " ] not found!" << endl;
	}
}
packet_descriptor* SPcap::popPacket(int id){
	stream_descriptor *sd = getStream(id);
	packet_descriptor *pd = NULL;

	if(sd != NULL){
		pthread_mutex_lock(&sd->q_lock);
		//pthread_spin_lock(&sd->s_lock);
		if(!sd->q->empty()){
			pd = sd->q->front();
			sd->q->pop_front();
			// !!! free memory when done width popped packet!!
		}
		pthread_mutex_unlock(&sd->q_lock);
		//pthread_spin_unlock(&sd->s_lock);
	}
	return pd;

}
#ifdef __PFRING__
pfring_stat* SPcap::getStats(int id){
	pfring_stat *ps = NULL;
	stream_descriptor *sd = SPcap::getStream(id);
	if(sd != NULL){
		ps = new pfring_stat;
		pfring_stats(sd->handle, ps);
		//cout << "Stats: recv=" << ps->ps_recv << ", drop=" << ps->ps_drop << ", ifdrop=" << ps->ps_ifdrop <<  endl;

	}
	return ps;


}
#else
pcap_stat* SPcap::getStats(int id){
	pcap_stat *ps = NULL;
	stream_descriptor *sd = SPcap::getStream(id);
	if(sd != NULL){
		ps = new pcap_stat;
		pcap_stats(sd->handle, ps);
		//cout << "Stats: recv=" << ps->ps_recv << ", drop=" << ps->ps_drop << ", ifdrop=" << ps->ps_ifdrop <<  endl;

	}
	return ps;

}
#endif

#ifdef __PFRING__
void* SPcap::capture_thread(void *args){
	stream_descriptor *sd = (stream_descriptor *)args;
	packet_descriptor *pd;
	// start loop
	pfring_loop(sd->handle, packet_received, (u_char *)sd, 1);

	// free queue element pointers
	pthread_mutex_lock(&sd->q_lock);
	while(!sd->q->empty()){
		pd = sd->q->front();
		sd->q->pop_front();
		delete[] pd->packet;
		pd->packet = NULL;
		delete pd;
		pd = NULL;
	}
	// free temp q
	while(!sd->tmp_q->empty()){
		pd = sd->tmp_q->front();
		sd->tmp_q->pop_front();
		delete[] pd->packet;
		pd->packet = NULL;
		delete pd;
		pd = NULL;
	}

	// free queue memory
	delete sd->q;
	delete sd->tmp_q;
	// free stream descriptor memory
	pthread_mutex_unlock(&sd->q_lock);

	pfring_close(sd->handle);
	// remove from list
	pthread_mutex_lock(&SPcap::stram_lst_mt);
	stream_lst.erase(sd->id);
	delete sd;
	pthread_mutex_unlock(&SPcap::stram_lst_mt);

	return NULL;
}
#else
void* SPcap::capture_thread(void *args){

	stream_descriptor *sd = (stream_descriptor *)args;
	packet_descriptor *pd;
	// start loop
	pcap_loop(sd->handle, -1, packet_received, (u_char *)sd);
	//cout << "capture stopped" << endl;

	// free queue element pointers
	pthread_mutex_lock(&sd->q_lock);
	while(!sd->q->empty()){
		pd = sd->q->front();
		sd->q->pop_front();
		delete[] pd->packet;
		pd->packet = NULL;
		delete pd;
		pd = NULL;
	}
	// free temp q
	while(!sd->tmp_q->empty()){
		pd = sd->tmp_q->front();
		sd->tmp_q->pop_front();
		delete[] pd->packet;
		pd->packet = NULL;
		delete pd;
		pd = NULL;
	}

	// free queue memory
	delete sd->q;
	delete sd->tmp_q;
	// free stream descriptor memory
	pthread_mutex_unlock(&sd->q_lock);

	pcap_close(sd->handle);
	// remove from list
	pthread_mutex_lock(&SPcap::stram_lst_mt);
	stream_lst.erase(sd->id);
	delete sd;
	pthread_mutex_unlock(&SPcap::stram_lst_mt);

	return NULL;
}
#endif

#ifdef __PFRING__
void SPcap::packet_received(const struct pfring_pkthdr *hdr, const u_char *packet, const u_char *id){
	stream_descriptor *sd = (stream_descriptor *)id;
	packet_descriptor *packet_c;
	int mutex_status;

	if(sd){
		// init packet descriptor
		packet_c = new packet_descriptor;
		packet_c->packet = new u_char[hdr->caplen];
		packet_c->caplen = hdr->caplen;
		// copy packet data
		memcpy(packet_c->packet, packet, hdr->caplen);


		mutex_status = pthread_mutex_trylock(&sd->q_lock);

		// if mutex locked
		if(mutex_status == 0){

			// process temp queue
			while(!sd->tmp_q->empty()){
				sd->q->push_back(sd->tmp_q->front());
				sd->tmp_q->pop_front();
			}

			if(sd->q->size() < sd->MAX_Q_SIZE){
				// add to queue
				sd->q->push_back(packet_c);
			}else{
				sd->MAX_Q_LIMIT_REACHED++;
				//cout << "[ " << sd->id << " ] SPcap::packet_received - Q FULL! [ " << sd->MAX_Q_SIZE << " ]" << endl;
				delete[] packet_c->packet;
				packet_c->packet = NULL;
				delete packet_c;
				packet_c = NULL;

			}
			pthread_mutex_unlock(&sd->q_lock);

		}else{
			if(sd->tmp_q->size() < sd->MAX_Q_SIZE){
				sd->tmp_q->push_back(packet_c);
			}else{
				//cout << "[ " << sd->id << " ] SPcap::packet_received - TMP_Q FULL! [ " << sd->MAX_Q_SIZE << " ]" << endl;
				delete[] packet_c->packet;
				packet_c->packet = NULL;
				delete packet_c;
				packet_c = NULL;
			}

		}
	}
}
#else
void SPcap::packet_received(u_char *id, const struct pcap_pkthdr *hdr, const u_char *packet){

	stream_descriptor *sd = (stream_descriptor *)id;
	packet_descriptor *packet_c;
	int mutex_status;

	if(sd){
		// init packet descriptor
		packet_c = new packet_descriptor;
		packet_c->packet = new u_char[hdr->caplen];
		packet_c->caplen = hdr->caplen;
		// copy packet data
		memcpy(packet_c->packet, packet, hdr->caplen);


		mutex_status = pthread_mutex_trylock(&sd->q_lock);

		// if mutex locked
		if(mutex_status == 0){

			// process temp queue
			while(!sd->tmp_q->empty()){
				sd->q->push_back(sd->tmp_q->front());
				sd->tmp_q->pop_front();
			}

			if(sd->q->size() < sd->MAX_Q_SIZE){
				// add to queue
				sd->q->push_back(packet_c);
			}else{
				sd->MAX_Q_LIMIT_REACHED++;
				//cout << "[ " << sd->id << " ] SPcap::packet_received - Q FULL! [ " << sd->MAX_Q_SIZE << " ]" << endl;
				delete[] packet_c->packet;
				packet_c->packet = NULL;
				delete packet_c;
				packet_c = NULL;

			}
			pthread_mutex_unlock(&sd->q_lock);

		}else{
			if(sd->tmp_q->size() < sd->MAX_Q_SIZE){
				sd->tmp_q->push_back(packet_c);
			}else{
				//cout << "[ " << sd->id << " ] SPcap::packet_received - TMP_Q FULL! [ " << sd->MAX_Q_SIZE << " ]" << endl;
				delete[] packet_c->packet;
				packet_c->packet = NULL;
				delete packet_c;
				packet_c = NULL;
			}

		}

	}



}
#endif


#ifdef __PFRING__
void SPcap::setBPF(pfring *handle, string filter){
	// TODO
}
#else
void SPcap::setBPF(pcap_t *handle, string filter){
	struct bpf_program bpf;
	bpf_u_int32 netmask = 0;
	if(pcap_compile(handle, &bpf, filter.c_str(), 0, netmask) == -1){
		cout << "BPF Filter error: [ " << filter << " ]" << endl;
	}else{
		if(pcap_setfilter(handle, &bpf) == -1){
			cout << "Cannot set filter: [ " << filter << " ]" << endl;
		}
	}
}
#endif

#ifdef __PFRING__
int SPcap::getDevices(string *buffer){
	// TODO
}
#else
int SPcap::getDevices(string *buffer){
	pcap_if_t *alldevs, *d;
	int i = 0;
	if(pcap_findalldevs(&alldevs, errbuf) != -1){
		for(d = alldevs; d; d = d->next){
			buffer[i] = d->name;
			i++;
		}
	}
	delete alldevs;
	delete d;

	return i;
}
#endif
