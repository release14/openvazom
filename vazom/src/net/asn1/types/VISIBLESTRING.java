package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class VISIBLESTRING extends ASNType {
	public VISIBLESTRING(){
		super();
		universalTag = ASNTag.VISIBLESTRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
