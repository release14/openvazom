package net.mtp3;

import java.util.Arrays;

import net.mtp3.messages.normal.SCCPDataMessage;
import net.mtp3.messages.signalling.SignallingMessageBase;
import net.mtp3.messages.signalling_mtn.SignallingMessageBaseMTN;

public class MTP3 {

	public static MTP3Packet decode(byte[] data){
		MTP3Packet res = new MTP3Packet();
		res.init(data);
		switch(res.serviceInformation.serviceIndicator){
			case SCCP: 
				res.message = new SCCPDataMessage();
				res.message.init(Arrays.copyOfRange(data, res.byte_pos, data.length));
				break;
			case SIGNALLING_NETWORK_MANAGEMENT_MESSAGES:
				res.message = SignallingMessageBase.decode(Arrays.copyOfRange(data, res.byte_pos, data.length));
				break;
			case SIGNALLING_NETWORK_TESTING_AND_MAINTENANCE_MESSAGES:
				res.message = SignallingMessageBaseMTN.decode(Arrays.copyOfRange(data, res.byte_pos, data.length));
				break;
		}
		return res;
	}
}
