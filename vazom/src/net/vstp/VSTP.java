package net.vstp;

import net.ds.DataSourceType;
import org.apache.log4j.Logger;

public class VSTP {
	public static Logger logger=Logger.getLogger(VSTP.class);

	public static String generate_init(LocationType client_type, String client_id){
		return "VSTP:R14:1.0:" + client_type + ":" + client_id + ":INIT" + "\n" + "END" + "\n";
	}
	public static String generate_ack(LocationType client_type, String client_id){
		return "VSTP:R14:1.0:" + client_type + ":" + client_id + ":ACK" + "\n" + "END" + "\n";
	}
	public static boolean check_accept(String data){
		try{
			String[] tmp_lst = data.split(":");
			// VSTP:R14:1.0:FN:node_id:ACK
			if(tmp_lst.length == 6){
				if(tmp_lst[0].equals("VSTP")){
					if(tmp_lst[1].equals("R14")){
						if(tmp_lst[5].startsWith("ACK")){
							return true;
							//return "VSTP:R14:" + tmp_lst[2] + ":" + tmp_lst[3] + ":ACK";
						}
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
		
	}
	public static String get_init_node_id(String data){
		try{
			String[] tmp_lst = data.split(":");
			return tmp_lst[4];
		}catch(Exception e){
			//e.printStackTrace();
		}		
		return null;
	}
	public static boolean check_init(String data){
		try{
			String[] tmp_lst = data.split(":");
			// VSTP:R14:1.0:FN:NODE_ID:INIT
			if(tmp_lst.length == 6){
				if(tmp_lst[0].equals("VSTP")){
					if(tmp_lst[1].equals("R14")){
						if(tmp_lst[5].startsWith("INIT")){
							///return "VSTP:R14:" + tmp_lst[2] + ":" + tmp_lst[3] + ":INIT";
							return true;
						}
					}
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
		
	}
	public static MessageDescriptor decode(byte[] data){
		MessageDescriptor res = null;
		String data_str = null;
		String[] tmp_lst = null;
		String[] tmp_line_lst = null;
		tmp_line_lst = null;
		String tmp_line = null;
		if(data != null){
			try{
				res = new MessageDescriptor();
				data_str = new String(data);
				tmp_lst = data_str.split("\n");
				tmp_line = tmp_lst[0];
				tmp_line_lst = tmp_line.split(":");
				// header
				res.header = new HeaderDescriptor();
				res.header.source = LocationType.valueOf(tmp_line_lst[0]);
				res.header.source_id = tmp_line_lst[1];
				res.header.ds = DataSourceType.valueOf(tmp_line_lst[2]);
				res.header.destination = LocationType.valueOf(tmp_line_lst[3]);
				res.header.destination_id = tmp_line_lst[4];
				res.header.msg_type = MessageType.valueOf(tmp_line_lst[5]);
				res.header.msg_id = tmp_line_lst[6];
				// values
				for(int i = 1; i<tmp_lst.length; i++){
					// skip END
					if(!tmp_lst[i].equals("END")){
						tmp_line_lst = tmp_lst[i].split("=");
						res.values.put(tmp_line_lst[0].trim(), (tmp_line_lst.length > 1 ? tmp_line_lst[1].trim() : null));
						// extract filter result if found
						if(tmp_line_lst[0].trim().equalsIgnoreCase("FILTER.RESULT")){
							if(tmp_line_lst[1].trim().equalsIgnoreCase("1")) res.filter_result = FilterResultType.ALLOW;
							else res.filter_result = FilterResultType.DENY;
						}
						
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				logger.warn("Invalid VSTP packet...");
			}
			
		}
		return res;
		
	}
}
