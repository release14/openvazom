package net.sccp.parameters;

public class Data extends ParamBase {
	public byte[] value;
	public Data(){
		super();
		type = ParamType.DATA;
	}
	
	public void init(byte[] data) {
		value = data;
	}

}
