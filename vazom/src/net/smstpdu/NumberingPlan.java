package net.smstpdu;

import java.util.HashMap;

public enum NumberingPlan {
	UNKNOWN(0x00),
	ISDN_TELEPHONE(0x01),
	DATA_X121(0x03),
	TELEX(0x04),
	LAND_MOBILE(0x06),
	NATIONAL(0x08),
	PRIVATE(0x09),
	ERMES(0x0A),
	RESERVED(0x0F);
	
	
	// BITS 3..0 = 0000XXXX
	private int id;
	private static final HashMap<Integer, NumberingPlan> lookup = new HashMap<Integer, NumberingPlan>();
	static{
		for(NumberingPlan td : NumberingPlan.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static NumberingPlan get(int id){ return lookup.get(id); }
	private NumberingPlan(int _id){ id = _id; }
	
}
