package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ContextIDinListSequenceOf extends SEQUENCE{

	public ContextIDinList new_child(){
		return new ContextIDinList();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ContextIDinList getChild(int index){
		return (ContextIDinList)of_children.get(index);
	}
	public ContextIDinListSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
