package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class O_BcsmTriggerDetectionPoint extends ENUMERATED{

	public static final int _collectedInfo = 2;
	public static final int _routeSelectFailure = 4;
	public O_BcsmTriggerDetectionPoint(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
