package net.m3ua.parameters.status;

import java.util.HashMap;

public enum StatusType {
	AS_STATE_CHANGE(0x01),
	OTHER(0x02);
	
	private int id;
	private static final HashMap<Integer, StatusType> lookup = new HashMap<Integer, StatusType>();
	static{
		for(StatusType td : StatusType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static StatusType get(int id){ return lookup.get(id); }
	private StatusType(int _id){ id = _id; }		
}
