package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceDataAccessElementType extends ServiceDataAccessElement{

	public ServiceDataAccessElementType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 270;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
