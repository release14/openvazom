package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SelectLogicWrapper extends SEQUENCE{

	public SelectLogic get_selectLogic(){
		return (SelectLogic)elements.get(0).data;
	}
	public SelectLogic new_selectLogic(){
		return new SelectLogic();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_selectLogic(); }});
	}
	public SelectLogicWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
