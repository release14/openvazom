package net.m3ua.parameters.user_cause;

import java.util.HashMap;

public enum CauseType {
	UNKNOWN(0x00),
	UNEQUIPPED_REMOTE_USER(0x01),
	INACCESSIBLE_REMOTE_USER(0x02);
	
	
	private int id;
	private static final HashMap<Integer, CauseType> lookup = new HashMap<Integer, CauseType>();
	static{
		for(CauseType td : CauseType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static CauseType get(int id){ return lookup.get(id); }
	private CauseType(int _id){ id = _id; }	
}
