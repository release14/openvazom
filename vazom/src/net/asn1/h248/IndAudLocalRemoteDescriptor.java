package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudLocalRemoteDescriptor extends SEQUENCE{

	public INTEGER get_propGroupID(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_propGroupID(){
		return new INTEGER();
	}

	public IndAudPropertyGroup get_propGrps(){
		return (IndAudPropertyGroup)elements.get(1).data;
	}
	public IndAudPropertyGroup new_propGrps(){
		return new IndAudPropertyGroup();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_propGroupID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_propGrps(); }});
	}
	public IndAudLocalRemoteDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
