package net.sctp;

public class SSctp {
	static{ System.loadLibrary("ssctp"); }

	public static native int initClient(String address, int port, int streamSount, String local_ip, int local_port);
	public static native void shutdownClient(int socket_id);
	
	public static native int initServer(String local_address, int local_port);
	public static native int clientAccept(int server_socket_id);
	public static native void shutdownServer(int server_socket_id);
	
	
	public static native int send(int socket_id, byte[] data, int sid);
	public static native SSCTPDescriptor receive(int socket_id);

	
	public static native int sendM3ua(int socket_id, byte[] data, int sid);
	public static native SSCTPDescriptor receiveM3ua(int socket_id);
	
	public static Timeout runTimeout(int timeout, int sctp_id){
		Timeout t = new Timeout(timeout, sctp_id);
		new Thread(t).start();
		return t;
		
	}
	
	public static void stopTimeout(Timeout t){
		t.stop();
		
	}
	
	
	public static class Timeout implements Runnable{
		int timeout;
		int sctp_id;
		boolean done = false;
		public Timeout(int _timeout, int _sctp_id){
			timeout = _timeout;
			sctp_id = _sctp_id;
		}
		public void stop(){
			done = true;
		}
		public void run() {
			try{ Thread.sleep(timeout); }catch(Exception e){}
			if(!done){
				shutdownClient(sctp_id);
			}
			
		}
		
	}
	
}
