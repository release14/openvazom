package net.isup.parameters;

import net.isup.ParameterType;

public class LoopPreventionIndicators extends ParameterBase {
	public LoopPreventionIndicators(){
		type = ParameterType.LOOP_PREVENTION_INDICATORS;
	}
}
