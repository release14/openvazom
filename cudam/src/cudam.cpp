// CUDA manager
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cudam.h>
#include <ld-kernel.h>
#include <md5-kernel.h>
#include <search-kernel.h>
using namespace std;

// static
int CUDAM::max_rep_lst_size;
int CUDAM::max_spam_lst_size;
int CUDAM::max_md5_quarantine_lst_size;
int CUDAM::max_md5_normal_lst_size;
int CUDAM::max_q_size;
int CUDAM::max_i_size;
int CUDAM::max_i_variation;
pthread_mutex_t CUDAM::gpu_map_lock;
bool CUDAM::stopping;
int CUDAM::device_count;
map<int, GPUDescriptor*> CUDAM::gpu_map;
map<int, GridDescriptor*> CUDAM::grid_map;
map<int, GPUDescriptor*>::iterator CUDAM::last_used_device_it = gpu_map.end();
//pthread_mutex_t CUDAM::gpu_map_lock;

int CUDAM::get_device_lst_item(int device, int lst_id, int item_id, char *result, int *result_size){
	GPUDescriptor *gd = NULL;
	bool lst_found = false;
	int *current_host_size;
	char *current_device_lst;
	int current_max_i_size;
	int extra = 0;
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
	if(it != gpu_map.end()){
		gd = it->second;
		switch(lst_id){
			case LD_REPETITION:
                        	current_host_size = &gd->rep_current_size;
                       	 	current_device_lst = gd->rep_device_data_lst;
				current_max_i_size = max_i_size;
				extra = 1;
                       	 	lst_found = true;
				if(item_id < *current_host_size) cudaMemcpy(result_size, current_device_lst + item_id * (current_max_i_size + extra), 1, cudaMemcpyDeviceToHost);
				break;
			case LD_SPAM:
                                current_host_size = &gd->spam_current_size;
                                current_device_lst = gd->spam_device_data_lst;
				current_max_i_size = max_i_size;
				extra = 1;
                                lst_found = true;
				if(item_id < *current_host_size) cudaMemcpy(result_size, current_device_lst + item_id * (current_max_i_size + extra), 1, cudaMemcpyDeviceToHost);
				break;
			case MD5_QUARANTINE_LST:
                                current_host_size = &gd->md5_quarantine_current_size;
                                current_device_lst = gd->md5_quarantine_device_data_lst;
				current_max_i_size = 16;// md5 hash size
				*result_size = 16;
                                lst_found = true;
				break;
                        case MD5_NORMAL_LST:
                                current_host_size = &gd->md5_normal_current_size;
                                current_device_lst = gd->md5_normal_device_data_lst;
                                current_max_i_size = 16;// md5 hash size
				*result_size = 16;
                                lst_found = true;
                                break;
		}
		// check if list exists
		if(lst_found){
			if(item_id < *current_host_size){
				cudaMemcpy(result, current_device_lst + item_id * (current_max_i_size + extra) + extra, *result_size, cudaMemcpyDeviceToHost);
				//printf("CUDAM::get_device_lst_item result: %s\n", result);
				//printf("CUDAM::get_device_lst_item result size: %d\n", *result_size);
				return 0;		
				
			}else cout << "CUDAM::get_device_lst_item: item id [" << item_id << "] is out of range!" << endl;
			
		}else cout << "CUDAM::get_device_lst_item: unknown list type: [" << lst_id << "]!" << endl;
		
	}else cout << "CUDAM::get_device_lst_item: Device [" << device << "] does not exist!" << endl;
	return -1;

}



void CUDAM::shutdown(){
	stopping = true;
}
void CUDAM::shutdownComplete(){
	GridDescriptor *gd = NULL;
	pthread_mutex_lock(&gpu_map_lock);
	if(gpu_map.size() == 0){
		map<int, GridDescriptor*>::iterator it;   
		for(it = grid_map.begin(); it != grid_map.end(); it++){
			gd = it->second;
			delete gd;
		} 
		grid_map.clear();
		cout << "CUDAM::shutdownComplete" << endl;
	}
	pthread_mutex_unlock(&gpu_map_lock);
}
void CUDAM::doShutdown(int device){
	GPUDescriptor *gd = NULL;
	LDPacket *ldp = NULL;
	DataPacket *dp = NULL;
	MD5Packet *md5p = NULL;
	stopping = true;
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
	if(it != gpu_map.end()){
		gd = it->second;
		cout << "Shutting down GPU: " << gd->id << endl;
		// remove from list
		gpu_map.erase(gd->id);
		// clear ld queue, deallocate memory
		// for each element
		while(!gd->ld_queue->empty()){
			ldp = gd->ld_queue->front();
			gd->ld_queue->pop_front();
			if(ldp->data != NULL) delete[] ldp->data;
			if(ldp->result != NULL) delete[] ldp->result;
			delete ldp;
		}
		// deallocate ld queue
		delete gd->ld_queue;
		// clear update queue, deallocate memory
		// for each element
		while(!gd->update_queue->empty()){
			dp = gd->update_queue->front();
			gd->update_queue->pop_front();
			if(dp->data != NULL) delete[] dp->data;
			delete dp;
			
		}
		// deallocate update queue
		delete gd->update_queue;
		
		// clear md5 queue, deallocate memory
                while(!gd->md5_queue->empty()){
                        md5p = gd->md5_queue->front();
                        gd->md5_queue->pop_front();
                        if(md5p->hash != NULL) delete[] md5p->hash;
                        delete md5p;
                }
		// deallocate md5 queue
		delete gd->md5_queue;


		// deallocate GPU descriptor
		delete gd;
	}

}
void CUDAM::init_device(int device){
	GPUDescriptor *gd = new GPUDescriptor;
	gd->id = device;
	gd->rep_current_size = 0;
	gd->spam_current_size = 0;
	gd->md5_quarantine_current_size = 0;
	gd->md5_normal_current_size = 0;
	gd->rep_current_pos = 0;
	gd->spam_current_pos = 0;
	gd->md5_quarantine_current_pos = 0;
	gd->md5_normal_current_pos = 0;
	gd->execution_count = 0;
	gd->last_execution_time = 0;
	gd->max_execution_time = 0;
	gd->ld_queue = new deque<LDPacket*>;
	gd->update_queue = new deque<DataPacket*>;
	gd->md5_queue = new deque<MD5Packet*>;

	// add to map
	gpu_map[gd->id] = gd;
	// init pthread
	// scheduling parameters
        pthread_attr_init(&gd->pth_attr);
        pthread_attr_setinheritsched(&gd->pth_attr, PTHREAD_EXPLICIT_SCHED);
        pthread_attr_setschedpolicy(&gd->pth_attr, SCHED_FIFO);
        sched_param sp;
        sp.__sched_priority = 99;
        pthread_attr_setschedparam(&gd->pth_attr, &sp);
	// ld_queue and update_queue mutex init
	pthread_mutex_init(&gd->ld_lock, NULL);
	pthread_mutex_init(&gd->update_lock, NULL);
	pthread_mutex_init(&gd->md5_lock, NULL);
	pthread_mutex_init(&gd->stats_lock, NULL);
	// pthread method init
	pthread_create(&gd->pth, &gd->pth_attr, gpu_ld_thread, (void *)gd);
	// pthread name
	char *pthread_name = new char[30];
	sprintf(pthread_name, "cudam_pthread_%i", gd->id);
	pthread_setname_np(gd->pth, pthread_name);
	//pthread_join(gd->pth, NULL);
	//
	cout << "CUDAM::init_device: " << device << endl;

}
void CUDAM::initAllDevices(){
	// get CUDA devices
	cout << "CUDAM::initAllDevices..." << endl;
	for(int i = 0; i<device_count; i++) init_device(i);
}
GridDescriptor* CUDAM::getGrid(int lst_id){
        GridDescriptor *gd = NULL;
        map<int, GridDescriptor*>::iterator it = grid_map.find(lst_id);
        if(it != grid_map.end()) return it->second;
	return NULL;

	
}
void CUDAM::initGrid(int lst_id, int blocks_w, int blocks_h, int threads){
        GridDescriptor *gd = NULL;
        map<int, GridDescriptor*>::iterator it = grid_map.find(lst_id);
        if(it == grid_map.end()){
		// init descriptor
		gd = new GridDescriptor;
		gd->blocks.x = blocks_w;
		gd->blocks.y = blocks_h;
		gd->threads.x = threads;
		// add to map
		grid_map[lst_id] = gd;	
		cout 	<< "CUDAM::initGrid lst id [" 
			<< lst_id 
			<< "]: " 
			<< gd->blocks.x 
			<< " x " 
			<< gd->blocks.y 
			<< " x " 
			<< gd->threads.x 
			<< "  = " 
			<< gd->blocks.x*gd->blocks.y*gd->threads.x 
			<< " threads..." 
			<< endl;
	}else cout << "CUDAM::initGrid: lst id [" << lst_id << "] grid configuration is already set!" << endl;
}

void CUDAM::init(	int _max_rep_lst_size, 
			int _max_spam_lst_size, 
			int _max_md5_quarantine_lst_size, 
			int _max_md5_normal_lst_size, 
			int _max_i_size, 
			int _max_i_variation,
			int _max_q_size){ 
		
        max_rep_lst_size = _max_rep_lst_size;
        max_spam_lst_size = _max_spam_lst_size;
	max_md5_quarantine_lst_size = _max_md5_quarantine_lst_size;
	max_md5_normal_lst_size = _max_md5_normal_lst_size;
	max_q_size = _max_q_size;
        max_i_size = _max_i_size;
        max_i_variation = _max_i_variation;
        device_count = getDeviceCount();
	pthread_mutex_init(&gpu_map_lock, NULL);
	//pthread_mutex_init(&gpu_map_lock, NULL);
        cout << "Device count: " << device_count << endl;



}

// gpu pthread routine
void *CUDAM::gpu_ld_thread(void *args){
	GPUDescriptor *gd = (GPUDescriptor *)args;
	LDPacket *ldp = NULL;
	DataPacket *dp = NULL;
	MD5Packet *md5p = NULL;
	bool locked = true;
	bool lst_found = false;
	int *current_device_size;
	int current_host_size;
	char *current_device_lst;
	char *current_device_result;
	bool update_q_has_data = false;	
	bool ld_q_has_data = false;	
	bool md5_q_has_data = false;	
	int last_error;
	GridDescriptor *grid = NULL;
	// pthread sleep settings
	timespec st;
	timespec rmtp;
	st.tv_sec = 0;
	st.tv_nsec = 1000000;

	// select GPU
	int err = cudaSetDevice(gd->id);
	// dummy just to force context creation
	cudaFree(0);
	cout << "Starting GPU pthread, id: " << gd->id << ", err: " << err << endl;
	// allocate memory on cuda device
	cudaMalloc((void**)&gd->device_max_i_size, sizeof(int));
	cudaMalloc((void**)&gd->device_max_i_variation, sizeof(int));
	cudaMalloc((void**)&gd->rep_device_data_lst, max_rep_lst_size*(max_i_size+1));
	cudaMalloc((void**)&gd->spam_device_data_lst, max_spam_lst_size*(max_i_size+1));
	cudaMalloc((void**)&gd->md5_quarantine_device_data_lst, max_md5_quarantine_lst_size*16);
	cudaMalloc((void**)&gd->md5_normal_device_data_lst, max_md5_normal_lst_size*16);
	cudaMalloc((void**)&gd->rep_device_current_size, sizeof(int));
	cudaMalloc((void**)&gd->spam_device_current_size, sizeof(int));
	cudaMalloc((void**)&gd->md5_quarantine_device_current_size, sizeof(int));
	cudaMalloc((void**)&gd->md5_normal_device_current_size, sizeof(int));
	cudaMalloc((void**)&gd->device_rep_result, max_rep_lst_size*sizeof(char));
	cudaMalloc((void**)&gd->device_spam_result, max_spam_lst_size*sizeof(char));
	cudaMalloc((void**)&gd->device_md5_q_result, max_md5_quarantine_lst_size*sizeof(char));
	cudaMalloc((void**)&gd->device_md5_result, max_md5_normal_lst_size*sizeof(char));
	cudaMalloc((void**)&gd->device_current_msg, max_i_size*sizeof(char));
	cudaMalloc((void**)&gd->device_current_msg_length, sizeof(int));
	cudaMalloc((void**)&gd->device_current_md5, 16);
	// needle search
	cudaMalloc((void**)&gd->device_needle, max_i_size*sizeof(char));
	cudaMalloc((void**)&gd->device_needle_size, sizeof(int));
	cudaMalloc((void**)&gd->device_max_needle_size, sizeof(int));
	cudaMalloc((void**)&gd->device_needle_length_size, sizeof(int));
	// init list sizes to 0
	// set max item size on device
	cudaMemcpy(gd->device_max_i_size, &max_i_size, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gd->device_max_i_variation, &max_i_variation, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gd->rep_device_current_size, &gd->rep_current_size, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gd->spam_device_current_size, &gd->spam_current_size, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gd->md5_quarantine_device_current_size, &gd->md5_quarantine_current_size, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(gd->md5_normal_device_current_size, &gd->md5_normal_current_size, sizeof(int), cudaMemcpyHostToDevice);

	// loop
	while(!stopping){	
		update_q_has_data = false;
		ld_q_has_data = false;
		// check for new DataPacket
		// ------------------------
		// mutex lock
		pthread_mutex_lock(&gd->update_lock);
		locked = true;
		// check queue
		if(!gd->update_queue->empty()){
			update_q_has_data = true;
			// pop from queue
			dp = gd->update_queue->front();
			gd->update_queue->pop_front();	
			// unlock mutex, set flag
			pthread_mutex_unlock(&gd->update_lock);
			locked = false;
			// check action
			// ACT_NEW
			if(dp->action == ACT_NEW){
				// push to cuda device
				push_update_device(gd->id, dp);
			// ACT_DELETE
			}else if(dp->action == ACT_DELETE){
				remove_from_lst(gd->id, dp);
				//removeFromLst(gd->id, dp->lst_type, dp->action_param);
			// ACT_RESET
			}else if(dp->action == ACT_RESET){
				reset(gd->id, dp->lst_type);
			}
			// free DataPacket memory
			if(dp->data != NULL) delete[] dp->data;
			delete dp;
			dp = NULL;
			
		}else update_q_has_data = false;
		// mutex unlock
		if(locked) pthread_mutex_unlock(&gd->update_lock); 
		// ----------------------



		// check for new MD5Packet
		// mutex lock
		pthread_mutex_lock(&gd->md5_lock);
		locked = true;
		// check queue
		if(!gd->md5_queue->empty()){
			md5_q_has_data = true;
			// pop from queue
			md5p = gd->md5_queue->front();
			gd->md5_queue->pop_front();
			// unlock mutex, set flag	
			pthread_mutex_unlock(&gd->md5_lock);
			locked = false;
			//if(md5p->action == ACT_DELETE){
				// delete item
				//removeFromLst(gd->id, md5p->lst_type, md5p->action_param);
			/*}else*/ 
			if(md5p->action == ACT_COMPARE){
				switch(md5p->lst_type){
					case MD5_QUARANTINE_LST:
                                     		current_device_size = gd->md5_quarantine_device_current_size;
		                                current_host_size = gd->md5_quarantine_current_size;
                                        	current_device_lst = gd->md5_quarantine_device_data_lst;
						current_device_result = gd->device_md5_q_result;
						lst_found = true;
						break;
					case MD5_NORMAL_LST:
                                                current_device_size = gd->md5_normal_device_current_size;
                                                current_host_size = gd->md5_normal_current_size;
                                                current_device_lst = gd->md5_normal_device_data_lst;
						current_device_result = gd->device_md5_result;
						lst_found = true;
						break;
				}
				if(lst_found){
					// compare
					push_md5_compare_device(gd->id, md5p);
                       	        	// init timers
                                	cudaEventCreate(&gd->timer_start);
	                                cudaEventCreate(&gd->timer_stop);
          		                cudaEventRecord(gd->timer_start, 0);
					// grid configuration
					grid = getGrid(md5p->lst_type);
					if(grid != NULL){
						// run kernel
						MD5_KERNEL(	current_device_lst, 
								current_device_size, 
								gd->device_current_md5, 
								current_device_result, 
								grid->blocks, 
								grid->threads);

						// wait for threads to finish
						cudaThreadSynchronize();
						// check for errors
						last_error = cudaGetLastError();
						if(last_error == 0){
							// copy result from device to host
							cudaMemcpy(md5p->result, current_device_result, current_host_size, cudaMemcpyDeviceToHost);
							// set result size
							md5p->result_size = current_host_size;
						}else{
        	                                	md5p->result_size = 1;
                	                        	md5p->result[0] = -100;
						}
					}else{
						md5p->result_size = 1;
                                                md5p->result[0] = -120;
						cout << "Grid configuration for lst id [" << md5p->lst_type << "] not found!" << endl;	
					}
                                	// process timers
                                	cudaEventRecord(gd->timer_stop, 0);
                                	cudaEventSynchronize(gd->timer_stop);
                                	// counter update
                                	// lock stats mutex
                                	pthread_mutex_lock(&gd->stats_lock);
                                	gd->execution_count++;
                                	cudaEventElapsedTime(&gd->last_execution_time, gd->timer_start, gd->timer_stop);
                                	if(gd->last_execution_time > gd->max_execution_time) gd->max_execution_time = gd->last_execution_time;
                                	// unlock stats mutex
                                	pthread_mutex_unlock(&gd->stats_lock);
                                	// destory timers
                                	cudaEventDestroy(gd->timer_start);
                                	cudaEventDestroy(gd->timer_stop);
					/*	
					for(int i = 0; i<md5p->result_size; i++)
                                	cout    << i
                                        	<< "MD5 Result: "
                                        	<< (int)md5p->result[i]
                                        	<<  "%"
                                        	<< ", elapsed(msec): "
                                        	<< gd->last_execution_time
                                        	<< ", max elapsed(msec): "
                                        	<< gd-> max_execution_time
                                        	<< ", lst size: "
                                        	<< current_host_size
                                        	//<< ", q size: "
                                        	//<< gd->ld_queue->size()
                                        	<< endl;
					*/
						// reset
						lst_found = false;
                                // unknown list
                                }else{
                                        cout << "CUDAM::gpu_md5_thread [" << gd->id << "]: unknown list type: [" << md5p->lst_type << "]!" << endl;
                                        // return error
                                        md5p->result_size = 1;
                                        md5p->result[0] = -110;
                                }
			}else{
				cout << "CUDAM::gpu_md5_thread [" << gd->id << "]: unknown action [" << md5p->action << "]!" << endl;
			}
                        // check for callback
                        if(md5p->callback_args != NULL && md5p->callback_method != NULL) md5p->callback_method(md5p->callback_args);

                        if(md5p->hash != NULL) delete[] md5p->hash;
			if(md5p->result != NULL) delete[] md5p->result;
                        delete md5p;
                        md5p = NULL;

		
		}else md5_q_has_data = false;
		// mutex unlock
		if(locked) pthread_mutex_unlock(&gd->md5_lock);
	



		// check for new LDPacket
		// ----------------------
		// mutex lock
		pthread_mutex_lock(&gd->ld_lock);
		locked = true;
		// check queue
		if(!gd->ld_queue->empty()){
			ld_q_has_data = true;
			ldp = gd->ld_queue->front();
            		gd->ld_queue->pop_front();
			// unlock mutex, set flag
			pthread_mutex_unlock(&gd->ld_lock);
			locked = false;
			// push to cuda device
			push_ld_device(gd->id, ldp);
			// execute cuda kernel
			// check which list to use
			switch(ldp->lst_type){
				case LD_REPETITION: 
					current_device_size = gd->rep_device_current_size;
					current_host_size = gd->rep_current_size;
					current_device_lst = gd->rep_device_data_lst;
					current_device_result = gd->device_rep_result;
					lst_found = true;
					break;
				case LD_SPAM: 
					current_device_size = gd->spam_device_current_size;
					current_host_size = gd->spam_current_size;
					current_device_lst = gd->spam_device_data_lst;
					current_device_result = gd->device_spam_result;
					lst_found = true;
					break;
			}
			// if list type is recognized
			if(lst_found){
				// init timers
			    	cudaEventCreate(&gd->timer_start);
				cudaEventCreate(&gd->timer_stop);
				cudaEventRecord(gd->timer_start, 0);
                                // grid configuration
                                grid = getGrid(ldp->lst_type);
                                if(grid != NULL){
					// run kernel
					LD_kernel(	current_device_lst,
							current_device_size,
							gd->device_max_i_size,
							gd->device_max_i_variation,
							gd->device_current_msg,
							gd->device_current_msg_length,
							current_device_result,
							grid->blocks,
							grid->threads);
					// wait for kernel to finish
					cudaThreadSynchronize();
					// check for last error
					last_error = cudaGetLastError();
					// if not error encountered
					if(last_error == 0){
						// copy memory from device to host
						cudaMemcpy(ldp->result, current_device_result, current_host_size*sizeof(char), cudaMemcpyDeviceToHost);
						// set result size
						ldp->result_size = current_host_size;
					}else{
						// error encountered
						ldp->result_size = 1;
						ldp->result[0] = -100;
					}
				}else{
			 		ldp->result_size = 1;
                                        ldp->result[0] = -120;
					cout << "Grid configuration for lst id [" << ldp->lst_type << "] not found!" << endl;
				}
			    	// process timers
				cudaEventRecord(gd->timer_stop, 0);
				cudaEventSynchronize(gd->timer_stop);
				// counter update
				// lock stats mutex
				pthread_mutex_lock(&gd->stats_lock);
				gd->execution_count++;
				cudaEventElapsedTime(&gd->last_execution_time, gd->timer_start, gd->timer_stop);
				if(gd->last_execution_time > gd->max_execution_time) gd->max_execution_time = gd->last_execution_time;
				// unlock stats mutex
				pthread_mutex_unlock(&gd->stats_lock);
				// destory timers
				cudaEventDestroy(gd->timer_start);
			    	cudaEventDestroy(gd->timer_stop);
				//if(gd->execution_count % 100 == 0)
				for(int i = 0; i<current_host_size; i++) 
				/*	
				cout 	<< i
					<< "LD Result: "
					<< (int)ldp->result[i]
					<<  "%"
					<< ", elapsed(msec): "
					<< gd->last_execution_time
					<< ", max elapsed(msec): "
					<< gd-> max_execution_time
					<< ", lst size: "
					<< current_host_size
					//<< ", q size: "
					//<< gd->ld_queue->size()
					<< endl;
				*/
					// reset lst_found
					lst_found = false;
			// unknown list
			}else{
				cout << "CUDAM::gpu_ld_thread [" << gd->id << "]: unknown list type: [" << ldp->lst_type << "]!" << endl;
				// return error
				ldp->result_size = 1;
                		ldp->result[0] = -110;

			}

			// check for callback
			if(ldp->callback_args != NULL && ldp->callback_method != NULL) ldp->callback_method(ldp->callback_args);
			// free memory
			if(ldp->data != NULL) delete[] ldp->data;
			if(ldp->result != NULL) delete[] ldp->result;
			delete ldp;
			ldp = NULL;



        	}else ld_q_has_data = false;
		// mutex unlock
		if(locked) pthread_mutex_unlock(&gd->ld_lock);
		// ---------------------
		
		// if both LD and UPDATE queues are emtpy,
		// put pthread to sleep
		if(!ld_q_has_data && !update_q_has_data && !md5_q_has_data){
			//cout << "Sleeping..." <<  gd->rep_current_size << ":" << gd->spam_current_size << endl;
			nanosleep(&st, &rmtp);
		}
	}
	// deallocate cuda memory
	cout << "Freeing memory on CUDA device: " << gd->id << endl;
	cudaFree(gd->device_max_i_size);
	cudaFree(gd->device_max_i_variation);
	cudaFree(gd->rep_device_data_lst);
	cudaFree(gd->spam_device_data_lst);
	cudaFree(gd->md5_quarantine_device_data_lst);
	cudaFree(gd->md5_normal_device_data_lst);
	cudaFree(gd->rep_device_current_size);
	cudaFree(gd->spam_device_current_size);
	cudaFree(gd->md5_quarantine_device_current_size);
	cudaFree(gd->md5_normal_device_current_size);
	cudaFree(gd->device_rep_result);
	cudaFree(gd->device_spam_result);
	cudaFree(gd->device_md5_q_result);
	cudaFree(gd->device_md5_result);
	cudaFree(gd->device_current_msg);
	cudaFree(gd->device_current_msg_length);
	cudaFree(gd->device_current_md5);
	cudaFree(gd->device_needle);
	cudaFree(gd->device_needle_size);
	cudaFree(gd->device_max_needle_size);
	cudaFree(gd->device_needle_length_size);
	// deallocate host memory
	doShutdown(gd->id);
	shutdownComplete();
}

int CUDAM::getDeviceCount(){
	int res;
	cudaGetDeviceCount(&res); 
	return res;

}

int CUDAM::getDeviceInfo(int dev, cudaDeviceProp *res){
	return cudaGetDeviceProperties(res, dev);

}

int CUDAM::getActiveDevice(){
	int res;
	cudaGetDevice(&res);
	return res;
}


void CUDAM::reset(int device, int lst_id){
        int *current_device_size;
        int *current_host_size;
        int *current_pos;
        GPUDescriptor *gd = NULL;
        map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);

        if(it != gpu_map.end()){
                gd = it->second;
                // get lst type 
                switch(lst_id){
                        // MD5 quarantine list
                        case MD5_QUARANTINE_LST:
                                gd->md5_quarantine_current_size = 0;
                                gd->md5_quarantine_current_pos = 0;
				cudaMemcpy(gd->md5_quarantine_device_current_size, &gd->md5_quarantine_current_size, sizeof(int), cudaMemcpyHostToDevice);
				break;
                        // MD5 general/normal list
                        case MD5_NORMAL_LST:
				gd->md5_normal_current_size = 0;
				gd->md5_normal_current_pos = 0;
				cudaMemcpy(gd->md5_normal_device_current_size, &gd->md5_normal_current_size, sizeof(int), cudaMemcpyHostToDevice);
				break;
                        // Repetition list
                        case LD_REPETITION:
				gd->rep_current_size = 0;
				gd->rep_current_pos = 0;
				cudaMemcpy(gd->rep_device_current_size, &gd->rep_current_size, sizeof(int), cudaMemcpyHostToDevice);
				break;
                        // Known spam list
                        case LD_SPAM:
				gd->spam_current_size = 0;
				gd->spam_current_pos = 0;
				cudaMemcpy(gd->spam_device_current_size, &gd->spam_current_size, sizeof(int), cudaMemcpyHostToDevice);
				break;
			// ALL
			case ALL_LISTS:
                                gd->md5_quarantine_current_size = 0;
                                gd->md5_quarantine_current_pos = 0;
                                gd->md5_normal_current_size = 0;
                                gd->md5_normal_current_pos = 0;
                                gd->rep_current_size = 0;
                                gd->rep_current_pos = 0;
                                gd->spam_current_size = 0;
                                gd->spam_current_pos = 0;
				cudaMemcpy(gd->md5_quarantine_device_current_size, &gd->md5_quarantine_current_size, sizeof(int), cudaMemcpyHostToDevice);					
				cudaMemcpy(gd->md5_normal_device_current_size, &gd->md5_normal_current_size, sizeof(int), cudaMemcpyHostToDevice);
				cudaMemcpy(gd->rep_device_current_size, &gd->rep_current_size, sizeof(int), cudaMemcpyHostToDevice);
				cudaMemcpy(gd->spam_device_current_size, &gd->spam_current_size, sizeof(int), cudaMemcpyHostToDevice);
				break;
			default: cout << "CUDAM::reset: unknown list type: [" << lst_id << "]!" << endl; break;
		}

	}


}

void CUDAM::push_ld_host(int device, LDPacket *ldp){
	GPUDescriptor *gd = NULL;
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
	if(it != gpu_map.end()){
		gd = it->second;
		if(ldp->length <= max_i_size){
			//cout << "CUDAM::push_ld_host: " << gd->id << endl;
			// lock mutex
			pthread_mutex_lock(&gd->ld_lock);
			if(gd->ld_queue->size() < max_q_size){
				gd->ld_queue->push_back(ldp);
			}else{
				cout << "CUDAM::push_ld_host: QUEUE FULL -> " << max_q_size << endl;
				if(ldp->data != NULL) delete[] ldp->data;
				if(ldp->result != NULL) delete[] ldp->result;
				delete ldp;

			}
			// unlock mutex
			pthread_mutex_unlock(&gd->ld_lock);
		}else cout << "CUDAM::push_ld_host: mesage length [" << ldp->length << "] exceeds max length [" << max_i_size << "]" << endl;
	}else cout << "CUDAM::push_ld_host: unknown device [" << device << "]!" << endl;
}


void CUDAM::push_ld_device(int device, LDPacket *ldp){
	GPUDescriptor *gd = gpu_map[device];
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
	if(it != gpu_map.end()){
		gd = it->second;
		//cout << "CUDAM::push_ld_device: " << gd->id << endl;
		cudaMemcpy(gd->device_current_msg, ldp->data, ldp->length, cudaMemcpyHostToDevice);	
		cudaMemcpy(gd->device_current_msg_length, &ldp->length, sizeof(int), cudaMemcpyHostToDevice);	

	}else cout << "CUDAM::push_ld_device: unknown device [" << device << "]!" << endl;
}

void CUDAM::push_update_host(int device, DataPacket *dp){
	GPUDescriptor *gd = NULL;
	bool valid = false;
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
	if(it != gpu_map.end()){
		gd = it->second;
		// check if packet is valid
		// LD lists
		switch(dp->action){
			case ACT_NEW:
				switch(dp->lst_type){
					case LD_SPAM:
					case LD_REPETITION:
						valid = dp->length <= max_i_size;
						break;
					case MD5_QUARANTINE_LST:
					case MD5_NORMAL_LST:
						valid = dp->length == 16;
						break;
				}
				break;
			case ACT_DELETE: valid = true; break;
			case ACT_RESET: valid = true; break;
		}
		// procede if valid	
		if(valid){
			//cout << "CUDAM::push_update_host: " << gd->id << endl;
			// mutex lock
			pthread_mutex_lock(&gd->update_lock);
			if(gd->update_queue->size() < max_q_size){
				gd->update_queue->push_back(dp);	
			}else{ 
				cout << "CUDAM::push_update_host: QUEUE FULL -> " << max_q_size << endl;
				if(dp->data != NULL) delete[] dp->data;
				delete dp;

			}
			// mutex unlock
			pthread_mutex_unlock(&gd->update_lock);
		}else cout 	<< "CUDAM::push_update_host: DataPacket for lst id [" 
				<< dp->lst_type 
				<< "], action [" 
				<< dp->action 
				<< "], length [" 
				<< dp->length 
				<< "] is invalid" 
				<< "]" 
				<< endl;
	}else cout << "CUDAM::push_update_host: unknown device [" << device << "]!" << endl;
}


void CUDAM::push_update_device(int device, DataPacket *dp){
	GPUDescriptor *gd = NULL;
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
	if(it != gpu_map.end()){
		gd = it->second;
		//cout << "CUDAM::push_update_device: " << gd->id << endl;
		// if item length allowed
		switch(dp->lst_type){
			// MD5 quarantine list
			case MD5_QUARANTINE_LST:
				if(dp->length == 16){
			                // check position
			                if(gd->md5_quarantine_current_pos >= max_md5_quarantine_lst_size) gd->md5_quarantine_current_pos = 0;
			                // copy to device
                			cudaMemcpy(gd->md5_quarantine_device_data_lst + gd->md5_quarantine_current_pos*16, dp->data, 16, cudaMemcpyHostToDevice);
        		        	// inc position
		                	gd->md5_quarantine_current_pos++;
        	       			if(gd->md5_quarantine_current_size < max_md5_quarantine_lst_size){
			                 	gd->md5_quarantine_current_size++;
		        	                cudaMemcpy(gd->md5_quarantine_device_current_size, &gd->md5_quarantine_current_size, sizeof(int), cudaMemcpyHostToDevice);
	                		}
				}else cout << "CUDAM::push_update_device: MD5 QUARANTINE item max size exceeded [" << dp->length << "]!" << endl;
				break;
			// MD5 general/normal list
                        case MD5_NORMAL_LST:
                                if(dp->length == 16){
                                        // check position
                                        if(gd->md5_normal_current_pos >= max_md5_normal_lst_size) gd->md5_normal_current_pos = 0;
                                        // copy to device
                                        cudaMemcpy(gd->md5_normal_device_data_lst + gd->md5_normal_current_pos*16, dp->data, 16, cudaMemcpyHostToDevice);
                                        // inc position
                                        gd->md5_normal_current_pos++;
                                        if(gd->md5_normal_current_size < max_md5_normal_lst_size){
                                                gd->md5_normal_current_size++;
                                                cudaMemcpy(gd->md5_normal_device_current_size, &gd->md5_normal_current_size, sizeof(int), cudaMemcpyHostToDevice);
                                        }
                                }else cout << "CUDAM::push_update_device: MD5 NORMAL item max size exceeded [" << dp->length << "]!" << endl;
                                break;

			// Repetition list
			case LD_REPETITION:
				// queue full on device, move items to the left
				/*
				if(gd->rep_current_size >= max_data_lst_size){
					cudaMemcpy(	gd->rep_device_data_lst, 
							gd->rep_device_data_lst+(max_i_size+1), 
							(max_data_lst_size-1)*(max_i_size+1), 
							cudaMemcpyDeviceToDevice);
					gd->rep_current_size--;	
				}
				*/
				if(gd->rep_current_pos >= max_rep_lst_size) gd->rep_current_pos = 0;
				// length
				cudaMemcpy(gd->rep_device_data_lst+gd->rep_current_pos*(max_i_size+1), &dp->length, 1, cudaMemcpyHostToDevice);
				// data part
				cudaMemcpy(gd->rep_device_data_lst+gd->rep_current_pos*(max_i_size+1)+1, dp->data, dp->length, cudaMemcpyHostToDevice);
				// current size on host and device
				//gd->rep_current_size++;
				//cudaMemcpy(gd->rep_device_current_size, &gd->rep_current_size, sizeof(int), cudaMemcpyHostToDevice);
				//printf("[LD_REPETITION] push, gpu: %d, list size: %d, item size: %d\n", gd->id, gd->rep_current_size, dp->length);
				gd->rep_current_pos++;
				if(gd->rep_current_size < max_rep_lst_size){
					gd->rep_current_size++;
					cudaMemcpy(gd->rep_device_current_size, &gd->rep_current_size, sizeof(int), cudaMemcpyHostToDevice);
				}
				break;
			// Known spam list
			case LD_SPAM:
				// queue full on device, move items to the left
				/*
				if(gd->spam_current_size >= max_data_lst_size){
					cudaMemcpy(	gd->spam_device_data_lst, 
							gd->spam_device_data_lst+(max_i_size+1), 
							(max_data_lst_size-1)*(max_i_size+1), 
							cudaMemcpyDeviceToDevice);
						gd->spam_current_size--;	
				}
				*/
				if(gd->spam_current_pos >= max_spam_lst_size) gd->spam_current_pos = 0;
				// length
				cudaMemcpy(gd->spam_device_data_lst+gd->spam_current_pos*(max_i_size+1), &dp->length, 1, cudaMemcpyHostToDevice);
				// data part
				cudaMemcpy(gd->spam_device_data_lst+gd->spam_current_pos*(max_i_size+1)+1, dp->data, dp->length, cudaMemcpyHostToDevice);
				//gd->spam_current_size++;
				//cudaMemcpy(gd->spam_device_current_size, &gd->spam_current_size, sizeof(int), cudaMemcpyHostToDevice);
				//printf("[LD_SPAM] push, gpu: %d, list size: %d, item size: %d\n", gd->id, gd->spam_current_size, dp->length);
				gd->spam_current_pos++;
				if(gd->spam_current_size < max_spam_lst_size){
					gd->spam_current_size++;
					cudaMemcpy(gd->spam_device_current_size, &gd->spam_current_size, sizeof(int), cudaMemcpyHostToDevice);
				}
				break;
			default: cout << "CUDAM::push_update_device: unknown list [" << dp->lst_type << "]!" << endl; break;
		}

	}else cout << "CUDAM::push_update_device: unknown device [" << device << "]!" << endl; 
}

void CUDAM::push_md5_host(int device, MD5Packet *md5p){
        GPUDescriptor *gd = NULL;
        map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);

        if(it != gpu_map.end()){
                gd = it->second;
		pthread_mutex_lock(&gd->md5_lock);
		if(gd->md5_queue->size() < max_q_size){
			gd->md5_queue->push_back(md5p);
	
		}else{
			cout << "CUDAM::push_md5_host: QUEUE FULL -> " << max_q_size << endl;
			if(md5p->hash != NULL) delete[] md5p->hash;
			delete md5p;
		}
	
		pthread_mutex_unlock(&gd->md5_lock);
	

	}
}
void CUDAM::push_md5_compare_device(int device, MD5Packet *md5p){
        GPUDescriptor *gd = gpu_map[device];
        map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
        if(it != gpu_map.end()){
                gd = it->second;
		if(md5p->action == ACT_COMPARE && md5p->hash != NULL){
			cudaMemcpy(gd->device_current_md5, md5p->hash, 16, cudaMemcpyHostToDevice);
		}else cout << "CUDAM::push_md5_compare_device: invalid MD5 packet!" << endl;

        }else cout << "CUDAM::push_md5_compare_device: unknown device [" << device << "]!" << endl;
}

/*
void CUDAM::push_md5_device(int device, MD5Packet *md5p){
        GPUDescriptor *gd = gpu_map[device];
        map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);
        if(it != gpu_map.end()){
                gd = it->second;
		// check position
		if(gd->md5_quarantine_current_pos >= max_md5_quarantine_lst_size) gd->md5_quarantine_current_pos = 0;
		// copy to device
		cudaMemcpy(gd->md5_quarantine_device_data_lst + gd->md5_quarantine_current_pos*16, md5p->hash, 16, cudaMemcpyHostToDevice);
		// inc position
                gd->md5_quarantine_current_pos++;
                if(gd->md5_quarantine_current_size < max_md5_quarantine_lst_size){
                	gd->md5_quarantine_current_size++;
                	cudaMemcpy(gd->md5_quarantine_device_current_size, &gd->md5_quarantine_current_size, sizeof(int), cudaMemcpyHostToDevice);
                }
	
	}else cout << "CUDAM::push_md5_device: unknown device [" << device << "]!" << endl;
}
*/


void CUDAM::remove_from_lst(int device, DataPacket *dp){
	bool lst_found = false;
	int *current_device_size;
	int *current_host_size;
	char *current_device_lst;
	char *current_device_result;
	char *tmp_result;
	int current_max_i_size;
	int needle_length_size;
        GPUDescriptor *gd = NULL;
	GridDescriptor *grid = NULL;
	int tmp_err;
        map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);

	if(dp == NULL){
		cout << "CUDAM::remove_from_lst: DataPacket is NULL!" << endl; 
		return;
	}

	if(dp->data == NULL){
		cout << "CUDAM::remove_from_lst: DataPacket->data is NULL!" << endl; 
		return;
	}

	if(it != gpu_map.end()){
                gd = it->second;
		// get lst type	
		switch(dp->lst_type){
			case LD_REPETITION:
				current_device_size = gd->rep_device_current_size;
                        	current_host_size = &gd->rep_current_size;
                       	 	current_device_lst = gd->rep_device_data_lst;
				current_max_i_size = max_i_size;
				current_device_result = gd->device_rep_result;
				needle_length_size = 1; // has one byte length
                       	 	lst_found = true;
				break;
			case LD_SPAM:
                                current_device_size = gd->spam_device_current_size;
                                current_host_size = &gd->spam_current_size;
                                current_device_lst = gd->spam_device_data_lst;
				current_max_i_size = max_i_size;
				current_device_result = gd->device_spam_result;
				needle_length_size = 1; // has one byte length
                                lst_found = true;
				break;
			case MD5_QUARANTINE_LST:
                                current_device_size = gd->md5_quarantine_device_current_size;
                                current_host_size = &gd->md5_quarantine_current_size;
                                current_device_lst = gd->md5_quarantine_device_data_lst;
				current_max_i_size = 16;// md5 hash size
				current_device_result = gd->device_md5_q_result;
				needle_length_size = 0;
                                lst_found = true;
				break;
                        case MD5_NORMAL_LST:
                                current_device_size = gd->md5_normal_device_current_size;
                                current_host_size = &gd->md5_normal_current_size;
                                current_device_lst = gd->md5_normal_device_data_lst;
                                current_max_i_size = 16;// md5 hash size
				current_device_result = gd->device_md5_result;
				needle_length_size = 0;
                                lst_found = true;
                                break;

		}
		// check if list exists
		if(lst_found){
			// allocate host result memory
			tmp_result = new char[*current_host_size];
			// copy to device memory
			cudaMemcpy(gd->device_needle, dp->data, dp->length, cudaMemcpyHostToDevice);	
			cudaMemcpy(gd->device_needle_size, &dp->length, sizeof(int), cudaMemcpyHostToDevice);	
			cudaMemcpy(gd->device_max_needle_size, &current_max_i_size, sizeof(int), cudaMemcpyHostToDevice);	
			cudaMemcpy(gd->device_needle_length_size, &needle_length_size, sizeof(int), cudaMemcpyHostToDevice);	
			
			// grid configuration
			grid = getGrid(dp->lst_type);
              	       	
			// init timers
                       	cudaEventCreate(&gd->timer_start);
                        cudaEventCreate(&gd->timer_stop);
       		        cudaEventRecord(gd->timer_start, 0);
			
			if(grid != NULL){
				// run kernel
				SEARCH_KERNEL(	current_device_lst,
						current_device_size,
						gd->device_needle,
						gd->device_needle_size,
						gd->device_max_needle_size,
						gd->device_needle_length_size,
						current_device_result,		
						grid->blocks,
						grid->threads
						);
				// wait for kernel to finish
				cudaThreadSynchronize();
				// check for last error
				tmp_err = cudaGetLastError();
				// if not error encountered
				if(tmp_err == 0){
					// copy memory from device to host
					cudaMemcpy(tmp_result, current_device_result, *current_host_size*sizeof(char), cudaMemcpyDeviceToHost);
					for(int i = 0; i<*current_host_size; i++){
						// match found
						if(tmp_result[i] == 1){
							// zero fill
							cudaMemset(current_device_lst+i*(current_max_i_size + needle_length_size), 0, current_max_i_size + needle_length_size);

						}
					}
					
				}
				
						
			}
                      	// process timers
                       	cudaEventRecord(gd->timer_stop, 0);
                       	cudaEventSynchronize(gd->timer_stop);
                       	// counter update
                       	// lock stats mutex
                       	pthread_mutex_lock(&gd->stats_lock);
                       	gd->execution_count++;
                       	cudaEventElapsedTime(&gd->last_execution_time, gd->timer_start, gd->timer_stop);
                       	if(gd->last_execution_time > gd->max_execution_time) gd->max_execution_time = gd->last_execution_time;
                       	// unlock stats mutex
                       	pthread_mutex_unlock(&gd->stats_lock);
                      	// destory timers
                    	cudaEventDestroy(gd->timer_start);
                       	cudaEventDestroy(gd->timer_stop);
			// cleanup
			delete[] tmp_result;
				
			
		}else cout << "CUDAM::remove_from_lst: unknown list type: [" << dp->lst_type << "]!" << endl;
		
	}else cout << "CUDAM::remove_from_lst: Device [" << device << "] does not exist!" << endl;
}

/*
void CUDAM::removeFromLst(int device, int lst_id, int item_id){
	bool lst_found = false;
	int *current_device_size;
	int *current_host_size;
	char *current_device_lst;
	int current_max_i_size;
	int *current_pos;
        GPUDescriptor *gd = NULL;
        map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);

	if(it != gpu_map.end()){
                gd = it->second;
		// get lst type	
		switch(lst_id){
			case LD_REPETITION:
				current_device_size = gd->rep_device_current_size;
                        	current_host_size = &gd->rep_current_size;
                       	 	current_device_lst = gd->rep_device_data_lst;
				current_max_i_size = max_i_size + 1; //+1 for item length
				current_pos = &gd->rep_current_pos;
                       	 	lst_found = true;
				break;
			case LD_SPAM:
                                current_device_size = gd->spam_device_current_size;
                                current_host_size = &gd->spam_current_size;
                                current_device_lst = gd->spam_device_data_lst;
				current_max_i_size = max_i_size + 1; //+1 for item length
				current_pos = &gd->spam_current_pos;
                                lst_found = true;
				break;
			case MD5_QUARANTINE_LST:
                                current_device_size = gd->md5_quarantine_device_current_size;
                                current_host_size = &gd->md5_quarantine_current_size;
                                current_device_lst = gd->md5_quarantine_device_data_lst;
				current_max_i_size = 16;// md5 hash size
				current_pos = &gd->md5_quarantine_current_pos;
                                lst_found = true;
				break;
                        case MD5_NORMAL_LST:
                                current_device_size = gd->md5_normal_device_current_size;
                                current_host_size = &gd->md5_normal_current_size;
                                current_device_lst = gd->md5_normal_device_data_lst;
                                current_max_i_size = 16;// md5 hash size
                                current_pos = &gd->md5_normal_current_pos;
                                lst_found = true;
                                break;

		}
		// check if list exists
		if(lst_found){
			if(item_id < *current_host_size){
				(*current_host_size)--;
				// shift all items to the left
				if(item_id < *current_host_size){
					cudaMemcpy(	current_device_lst+item_id*current_max_i_size, 
							current_device_lst+(item_id+1)*current_max_i_size, 
							current_max_i_size*(*current_host_size - item_id), 
							cudaMemcpyDeviceToDevice);
				}
				cudaMemcpy(current_device_size, current_host_size, sizeof(int), cudaMemcpyHostToDevice);
				// update position
				*current_pos = *current_host_size;
				//cout << "Current pos: " << *current_pos << endl;
			}else cout << "CUDAM::removeFromLst: item id [" << item_id << "] is out of range!" << endl;
			
		}else cout << "CUDAM::removeFromLst: unknown list type: [" << lst_id << "]!" << endl;
		
	}else cout << "CUDAM::removeFromLst: Device [" << device << "] does not exist!" << endl;
}
*/

// device round robin
int CUDAM::getFreeDevice(){
	int device = -1;
	pthread_mutex_lock(&gpu_map_lock);
	// if at the end, move to first one
	if(last_used_device_it == gpu_map.end()) last_used_device_it = gpu_map.begin();
	device = last_used_device_it->second->id;
	last_used_device_it++;
	pthread_mutex_unlock(&gpu_map_lock);
	return device;
}

void CUDAM::getKernelStats(int device, KernelStats *stats){
	GPUDescriptor *gd = NULL;
	map<int, GPUDescriptor*>::iterator it = gpu_map.find(device);

	if(it != gpu_map.end()){
		gd = it->second;
		pthread_mutex_lock(&gd->stats_lock);
		stats->last_execution_time = gd->last_execution_time;
		stats->max_execution_time = gd->max_execution_time;
		stats->execution_count = gd->execution_count;	
		pthread_mutex_unlock(&gd->stats_lock);	
		// queue sizes
		// update queue
		pthread_mutex_lock(&gd->update_lock);
		stats->update_q_size = gd->update_queue->size();
		pthread_mutex_unlock(&gd->update_lock);
		// ld queue
		pthread_mutex_lock(&gd->ld_lock);
		stats->ld_q_size = gd->ld_queue->size();
		pthread_mutex_unlock(&gd->ld_lock);
		// md5 queue
		pthread_mutex_lock(&gd->md5_lock);
		stats->md5_q_size = gd->md5_queue->size();
		pthread_mutex_unlock(&gd->md5_lock);
	}else cout << "CUDAM::getKernelStats: Device [" << device << "] does not exist!" << endl;

}
