package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SegmentReply extends SEQUENCE{

	public TransactionId get_transactionId(){
		return (TransactionId)elements.get(0).data;
	}
	public TransactionId new_transactionId(){
		return new TransactionId();
	}

	public SegmentNumber get_segmentNumber(){
		return (SegmentNumber)elements.get(1).data;
	}
	public SegmentNumber new_segmentNumber(){
		return new SegmentNumber();
	}

	public NULL get_segmentationComplete(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_segmentationComplete(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_segmentNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_segmentationComplete(); }});
	}
	public SegmentReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
