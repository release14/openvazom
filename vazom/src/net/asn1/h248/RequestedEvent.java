package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RequestedEvent extends SEQUENCE{

	public EventName get_eventName(){
		return (EventName)elements.get(0).data;
	}
	public EventName new_eventName(){
		return new EventName();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(1).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public RequestedActions get_eventAction(){
		return (RequestedActions)elements.get(2).data;
	}
	public RequestedActions new_eventAction(){
		return new RequestedActions();
	}

	public EventParameterSequenceOf get_evParList(){
		return (EventParameterSequenceOf)elements.get(3).data;
	}
	public EventParameterSequenceOf new_evParList(){
		return new EventParameterSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_eventAction(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_evParList(); }});
	}
	public RequestedEvent(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
