package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MobileStationMSID extends CHOICE{

	public MobileStationMIN get_mobileStationMIN(){
		return (MobileStationMIN)elements.get(0).data;
	}
	public MobileStationMIN new_mobileStationMIN(){
		return new MobileStationMIN();
	}

	public MobileStationIMSI get_mobileStationIMSI(){
		return (MobileStationIMSI)elements.get(1).data;
	}
	public MobileStationIMSI new_mobileStationIMSI(){
		return new MobileStationIMSI();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(184, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileStationMIN(); }});
		elements.add(new ElementDescriptor(286, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileStationIMSI(); }});
	}
	public MobileStationMSID(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
