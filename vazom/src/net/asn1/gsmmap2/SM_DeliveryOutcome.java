package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SM_DeliveryOutcome extends ENUMERATED{

	public static final int _memoryCapacityExceeded = 0;
	public static final int _absentSubscriber = 1;
	public static final int _successfulTransfer = 2;
	public SM_DeliveryOutcome(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
