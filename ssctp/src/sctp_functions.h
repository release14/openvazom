/* 
 * File:   sctp_functions.h
 * Author: jdjurici
 *
 * Created on 2010. studeni 28, 15:47
 */

#ifndef _SCTP_FUNCTIONS_H
#define	_SCTP_FUNCTIONS_H

int init_sctp_server(unsigned long, int);
int shutdown_sctp_server(int);
int get_client(int);
int init_sctp_client(unsigned long, int, int);
int init_sctp_client_bind(unsigned long, unsigned long, int, int, int);
int send_sctp(int , const void *, size_t *, unsigned long *, int );
int rcv_sctp(int , const void *, int *, struct sctp_sndrcvinfo *);
void shutdown_sctp_client(int *);

#endif	/* _SCTP_FUNCTIONS_H */

