package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CommandReply extends CHOICE{

	public AmmsReply get_addReply(){
		return (AmmsReply)elements.get(0).data;
	}
	public AmmsReply new_addReply(){
		return new AmmsReply();
	}

	public AmmsReply get_moveReply(){
		return (AmmsReply)elements.get(1).data;
	}
	public AmmsReply new_moveReply(){
		return new AmmsReply();
	}

	public AmmsReply get_modReply(){
		return (AmmsReply)elements.get(2).data;
	}
	public AmmsReply new_modReply(){
		return new AmmsReply();
	}

	public AmmsReply get_subtractReply(){
		return (AmmsReply)elements.get(3).data;
	}
	public AmmsReply new_subtractReply(){
		return new AmmsReply();
	}

	public AuditReplyWrapper get_auditCapReply(){
		return (AuditReplyWrapper)elements.get(4).data;
	}
	public AuditReplyWrapper new_auditCapReply(){
		return new AuditReplyWrapper();
	}

	public AuditReplyWrapper get_auditValueReply(){
		return (AuditReplyWrapper)elements.get(5).data;
	}
	public AuditReplyWrapper new_auditValueReply(){
		return new AuditReplyWrapper();
	}

	public NotifyReply get_notifyReply(){
		return (NotifyReply)elements.get(6).data;
	}
	public NotifyReply new_notifyReply(){
		return new NotifyReply();
	}

	public ServiceChangeReply get_serviceChangeReply(){
		return (ServiceChangeReply)elements.get(7).data;
	}
	public ServiceChangeReply new_serviceChangeReply(){
		return new ServiceChangeReply();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_addReply(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_moveReply(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_modReply(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_subtractReply(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditCapReply(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditValueReply(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_notifyReply(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceChangeReply(); }});
	}
	public CommandReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
