package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.ErrorCodeType;
import net.smpp.OptionalParameterType;

public class Network_error_code extends OptionalParameterBase {
	public enum NetworkType{
		ANSI_136(0x01),
		IS_95(0x02),
		GSM(0x03),
		RESERVED(0x04);
		private int id;
		private static final HashMap<Integer, NetworkType> lookup = new HashMap<Integer, NetworkType>();
		static{
			for(NetworkType td : NetworkType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static NetworkType get(int id){ return lookup.get(id); }
		private NetworkType(int _id){ id = _id; }			
	}
	public Network_error_code(){
		type = OptionalParameterType.NETWORK_ERROR_CODE;
	}
	public NetworkType networkType;
	public ErrorCodeType errorCode;
	public void init(byte[] data) {
		networkType = NetworkType.get(data[0] & 0xff);
		errorCode = ErrorCodeType.get(((data[1] & 0xff) << 8) + (data[2] & 0xff));
	}

}
