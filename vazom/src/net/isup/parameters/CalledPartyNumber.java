package net.isup.parameters;

import java.util.Arrays;

import net.isup.ParameterType;
import net.isup.parameters.cldpn.NatureOfAddressIndicatorType;
import net.isup.parameters.cldpn.NumberingPlanIndicatorType;

public class CalledPartyNumber extends ParameterBase {
	public boolean odd_number_of_address_signals;
	public NatureOfAddressIndicatorType nature_of_address_indicator;
	public boolean routing_to_internal_network_nummber_not_allowed;
	public NumberingPlanIndicatorType numbering_plan_indicator;
	public byte[] address_signal;
	
	public CalledPartyNumber(){
		type = ParameterType.CALLED_PARTY_NUMBER;
	}

	public void init(byte[] data) {
		if(data != null){
			nature_of_address_indicator = NatureOfAddressIndicatorType.get(data[byte_position] & 0x7f);
			odd_number_of_address_signals = (data[byte_position] & 0x80) == 0x80;
			byte_position++;
			routing_to_internal_network_nummber_not_allowed = (data[byte_position] & 0x80) == 0x80;
			numbering_plan_indicator = NumberingPlanIndicatorType.get(data[byte_position] & 0x70);
			byte_position++;
			address_signal = Arrays.copyOfRange(data, byte_position, data.length);
		}
	}

}
