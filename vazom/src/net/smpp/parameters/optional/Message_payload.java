package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;
import net.utils.Utils;

public class Message_payload extends OptionalParameterBase {

	public Message_payload(){
		type = OptionalParameterType.MESSAGE_PAYLOAD;
	}
	public String value;
	public void init(byte[] data) {
		value = Utils.Cstr_decode(data, 0);
	}

}
