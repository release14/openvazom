package net.ds.smpp;

import java.util.concurrent.ConcurrentHashMap;

import net.config.SGNConfigData;
import net.smpp.ErrorCodeType;
import net.smpp.PDUType;
import net.smpp.pdu.PDUBase;

import org.apache.log4j.Logger;

public class SMPPCorrelationManager {
	public static Logger logger=Logger.getLogger(SMPPCorrelationManager.class);
	private static ConcurrentHashMap<String, SMPPCorrelationPacket> map;
	public static boolean stopping;
	private static Timeout timeout_r;
	private static Thread timeout_t;
	
	private static class Timeout implements Runnable{
		public void run() {
			long l;
			SMPPCorrelationPacket smppp;
			logger.info("Starting...");
			l = System.currentTimeMillis();
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.smpp_check_inerval); }catch(Exception e){ e.printStackTrace(); }
				for(String s : map.keySet()){
					smppp = map.get(s);
					if(l - smppp.ts > SGNConfigData.smpp_timeout){
						logger.warn("Removing SMPP Correlation packet [" + s + "], timeout reached!");
						smppp.sequence_map.clear();
						map.remove(s);
					}
					
				}

			}
			logger.info("Ending...");
		}
		
	}
	
	public static void init(){
		logger.info("Starting SMPP Correlation Manager...");
		map = new ConcurrentHashMap<String, SMPPCorrelationPacket>();
		logger.info("Starting SMPP Correlation Monitor...");
		
		timeout_r = new Timeout();
		timeout_t = new Thread(timeout_r, "SMPP_TIMEOUT");
		timeout_t.start();
		
	}

	
	
	public static void newSequence(String ip_source, String ip_destination, int tcp_source, int tcp_destination, long sequence, PDUType pdu_type, ErrorCodeType sequence_status){
		SMPPCorrelationPacket res = get(ip_source, ip_destination, tcp_source, tcp_destination);
		if(res != null){
			SMPPSequenceDescriptor sd = new SMPPSequenceDescriptor();
			sd.pdu_type = pdu_type;
			sd.response_code = sequence_status;
			sd.sequence = sequence;
			res.sequence_map.put(sequence, sd);
			res.ts = System.currentTimeMillis();
		}
	}	
	public static void newSequence(SMPPCorrelationPacket smppp, long sequence, PDUType pdu_type, ErrorCodeType sequence_status, PDUBase pdu){
		if(smppp != null){
			SMPPCorrelationPacket res = get(smppp.ip_source, smppp.ip_destination, smppp.tcp_source, smppp.tcp_destination);
			if(res != null){
				SMPPSequenceDescriptor sd = new SMPPSequenceDescriptor();
				sd.pdu_type = pdu_type;
				sd.pdu = pdu;
				sd.response_code = sequence_status;
				sd.sequence = sequence;
				res.sequence_map.put(sequence, sd);
				res.ts = System.currentTimeMillis();
			}
			
		}
	}	

	public static SMPPCorrelationPacket get(String ip_source, String ip_destination, int tcp_source, int tcp_destination){
		SMPPCorrelationPacket res = null;
		res = map.get(ip_source + ":" + ip_destination + ":" + tcp_source + ":" + tcp_destination);
		if(res == null) res = map.get(ip_destination + ":" + ip_source+ ":" + tcp_destination + ":" + tcp_source);
		return res;
	}

	
	public static void add(SMPPCorrelationPacket p){
		p.ts = System.currentTimeMillis();
		map.put(p.ip_source + ":" + p.ip_destination + ":" + p.tcp_source + ":" + p.tcp_destination, p);
	}
	
	public static void remove(SMPPCorrelationPacket smppp){
		if(smppp != null){
			SMPPCorrelationPacket res = map.remove(smppp.ip_source + ":" + smppp.ip_destination + ":" + smppp.tcp_source + ":" + smppp.tcp_destination);
			if(res == null) map.remove(smppp.ip_destination + ":" + smppp.ip_source + ":" + smppp.tcp_destination + ":" + smppp.tcp_source);
			
		}
	}
	public static void remove(String ip_source, String ip_destination, int tcp_source, int tcp_destination){
		map.remove(ip_source + ":" + ip_destination + ":" + tcp_source + ":" + tcp_destination);
	}
	
}
