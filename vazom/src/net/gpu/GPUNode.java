package net.gpu;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import net.config.ConfigManagerV2;
import net.config.GPUNConfigData;
import net.gpu.cuda.CudaCallback;
import net.gpu.cuda.CudaManager;
import net.gpu.cuda.KernelStats;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class GPUNode {
	private static Logger logger;
	public class Listener_r implements Runnable{
		private Socket connection;
		private ClientConnection con;
		public void run() {
			while(!stopping){
				try{
					connection = socket.accept();
					con = new ClientConnection(connection, connection_lst);
					connection_lst.put(connection.getInetAddress().toString() + ":" + connection.getPort(), con);
					logger.info("NEW CLIENT CONNECTION: [ " + connection.getInetAddress().toString() + ":" + connection.getPort() + " ]!");
					
				}catch(Exception e){
					logger.error("ERROR WHILE ACCEPTING NEW CONNECTION TO CMDI!");
				}
				
			}
			
		}
		
	}

	private static ConcurrentHashMap<String, ClientConnection> connection_lst;
	public ServerSocket socket;
	public BufferedReader in;
	public PrintWriter out;
	public boolean stopping;
	public Listener_r listener_r;
	public Thread listener_t;
	public static Vector<String> text_lst;
	//public static ConcurrentHashMap<Integer, NodeWorker> worker_map;
	
	public static KernelStats getStats(int gpu_id){
		return CudaManager.getStats(gpu_id);
	}	
	public static void remove(int lst_id, int item_id){
		// push to all devices
		for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
			if(GPUNConfigData.debug) logger.debug("CUDA Remove, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "], item id: [" + item_id + "]");
			CudaManager.enqueueRemove(GPUNConfigData.cuda_devices[i], lst_id, item_id);
		
		}
	}
	public static void update(int lst_id, byte[] data){
		// push to all devices
		if(lst_id == CudaManager.MD5_QUARANTINE_LST || lst_id == CudaManager.MD5_NORMAL_LST){
			for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
				if(GPUNConfigData.debug) logger.debug("CUDA MD5 Update, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
				CudaManager.pushToMD5UpdateQueue(GPUNConfigData.cuda_devices[i], lst_id, data);
			}			
			
		}else{
			for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
				if(GPUNConfigData.debug) logger.debug("CUDA Update, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
				CudaManager.pushToUPDATEQueue(GPUNConfigData.cuda_devices[i], lst_id, data);
			}			
		}
	}
	public static void check(int lst_id, byte[] data, CudaCallback callback){
		int dev = CudaManager.getFreeDevice();
		if(lst_id == CudaManager.MD5_QUARANTINE_LST || lst_id == CudaManager.MD5_NORMAL_LST){
			if(GPUNConfigData.debug) logger.debug("CUDA MD5, GPU: [" + dev + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
			CudaManager.pushToMD5Queue(dev, lst_id, data, callback);
		}else{
			if(GPUNConfigData.debug) logger.debug("CUDA LD, GPU: [" + dev + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
			CudaManager.pushToLDQueue(dev, lst_id, data, callback);
		}
	}
	
	public GPUNode(){
		String tmp;
		String[] tmp_lst;
		int tmp_id;
		ConfigManagerV2.init("conf/gpun.properties");
		connection_lst = new ConcurrentHashMap<String, ClientConnection>();
		//worker_map = new ConcurrentHashMap<Integer, NodeWorker>();
		text_lst = new Vector<String>();
		try{
			// get config data
			GPUNConfigData.id = ConfigManagerV2.get("id");
			GPUNConfigData.cuda_max_repetition_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.repetition.size"));
			GPUNConfigData.cuda_max_spam_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.spam.size"));
			GPUNConfigData.cuda_max_quarantine_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.quarantine.size"));
			GPUNConfigData.cuda_max_md5_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.md5.size"));
			GPUNConfigData.cuda_max_i_size = Integer.parseInt(ConfigManagerV2.get("cuda.max_i_size"));
			GPUNConfigData.cuda_max_q_size = Integer.parseInt(ConfigManagerV2.get("cuda.max_q_size"));
			// grid configurations
			// Repetition grid
			GPUNConfigData.repetition_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.repetition.grid").split(":");
			GPUNConfigData.repetition_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.repetition_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.repetition_grid.threads = Integer.parseInt(tmp_lst[2]);
			// Spam grid
			GPUNConfigData.spam_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.spam.grid").split(":");
			GPUNConfigData.spam_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.spam_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.spam_grid.threads = Integer.parseInt(tmp_lst[2]);
			// Quarantine grid
			GPUNConfigData.quarantine_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.quarantine.grid").split(":");
			GPUNConfigData.quarantine_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.quarantine_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.quarantine_grid.threads = Integer.parseInt(tmp_lst[2]);
			// MD5 grid
			GPUNConfigData.md5_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.md5.grid").split(":");
			GPUNConfigData.md5_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.md5_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.md5_grid.threads = Integer.parseInt(tmp_lst[2]);
			
			// other
			GPUNConfigData.port = Integer.parseInt(ConfigManagerV2.get("port"));
			GPUNConfigData.ld_threshold = Integer.parseInt(ConfigManagerV2.get("ld.threshold"));
			//REPConfigData.ld_quarantine_threshold = Integer.parseInt(ConfigManagerV2.get("ld.quarantine.threshold"));
			GPUNConfigData.debug = (ConfigManagerV2.get("debug").equalsIgnoreCase("ON") ? true : false);
			// cuda devices
			tmp = ConfigManagerV2.get("cuda.devices");
			tmp_lst = tmp.split(":");
			GPUNConfigData.cuda_devices = new int[tmp_lst.length];
			for(int i = 0; i<tmp_lst.length; i++){
				tmp_id = Integer.parseInt(tmp_lst[i]);
				GPUNConfigData.cuda_devices[i] = tmp_id;
			}
			// init socket
			socket = new ServerSocket(GPUNConfigData.port);
			// init thread
			listener_r = new Listener_r();
			listener_t = new Thread(listener_r, "RN_LISTENER");
			listener_t.start();
			// CUDA
			logger.info("Initializing CUDA...");
			logger.info("Max repetition list size: [" + GPUNConfigData.cuda_max_repetition_size + "]!");
			logger.info("Max known spam list size: [" + GPUNConfigData.cuda_max_spam_size + "]!");
			logger.info("Max Quarantine list size: [" + GPUNConfigData.cuda_max_quarantine_size + "]!");
			logger.info("Max MD5 list size: [" + GPUNConfigData.cuda_max_md5_size + "]!");
			logger.info("Max list item size: [" + GPUNConfigData.cuda_max_i_size + "]!");
			logger.info("Max device queue size: [" + GPUNConfigData.cuda_max_q_size + "]!");
			logger.info("LD threshold: [" + GPUNConfigData.ld_threshold + "]!");
			//logger.info("LD quarantine threshold: [" + REPConfigData.ld_quarantine_threshold + "]!");
			logger.info("REPETITION Grid configuration: [" + GPUNConfigData.repetition_grid.blocks_w + " x " + GPUNConfigData.repetition_grid.blocks_h + " x " + GPUNConfigData.repetition_grid.threads + "]!");
			logger.info("SPAM Grid configuration: [" + GPUNConfigData.spam_grid.blocks_w + " x " + GPUNConfigData.spam_grid.blocks_h + " x " + GPUNConfigData.spam_grid.threads + "]!");
			logger.info("QUARANTINE Grid configuration: [" + GPUNConfigData.quarantine_grid.blocks_w + " x " + GPUNConfigData.quarantine_grid.blocks_h + " x " + GPUNConfigData.quarantine_grid.threads + "]!");
			logger.info("MD5 Grid configuration: [" + GPUNConfigData.md5_grid.blocks_w + " x " + GPUNConfigData.md5_grid.blocks_h + " x " + GPUNConfigData.md5_grid.threads + "]!");
			/*
			if(REPConfigData.ld_quarantine_threshold >= REPConfigData.ld_threshold){
				logger.error("LD quarantine threshold >= LD threshold!");
				System.exit(0);
			}
			*/
			// init CUDAM
			CudaManager.init(GPUNConfigData.cuda_max_repetition_size,
							GPUNConfigData.cuda_max_spam_size,
							GPUNConfigData.cuda_max_quarantine_size,
							GPUNConfigData.cuda_max_md5_size,
							GPUNConfigData.cuda_max_i_size, 
							3,
							GPUNConfigData.cuda_max_q_size); 
			// init CUDAM grids
			CudaManager.initGrid(CudaManager.LD_REPETITION, GPUNConfigData.repetition_grid.blocks_w, GPUNConfigData.repetition_grid.blocks_h, GPUNConfigData.repetition_grid.threads);
			CudaManager.initGrid(CudaManager.LD_SPAM, GPUNConfigData.spam_grid.blocks_w, GPUNConfigData.spam_grid.blocks_h, GPUNConfigData.spam_grid.threads);
			CudaManager.initGrid(CudaManager.MD5_QUARANTINE_LST, GPUNConfigData.quarantine_grid.blocks_w, GPUNConfigData.quarantine_grid.blocks_h, GPUNConfigData.quarantine_grid.threads);
			CudaManager.initGrid(CudaManager.MD5_NORMAL_LST, GPUNConfigData.md5_grid.blocks_w, GPUNConfigData.md5_grid.blocks_h, GPUNConfigData.md5_grid.threads);
			
			logger.info("Initializing GPUs...");
			for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
				logger.info("Initializing GPU:[" + GPUNConfigData.cuda_devices[i] + "]...");
				CudaManager.initDevice(GPUNConfigData.cuda_devices[i]);
			}
			logger.info("CUDA ready...");
			logger.info("GPU Node ready!");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error while starting GPU Node!");
		}
		
	}
	
	
	
	public static void main(String[] args) {
		PropertyConfigurator.configure(GPUNode.class.getResource("/log4j.gpun.properties"));
		logger = Logger.getLogger(GPUNode.class);
		new GPUNode();
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				CudaManager.shutdown();
				try{ Thread.sleep(5000); }catch(Exception e){e.printStackTrace();}
			
				
			}
			
		});
		
		
	}
}
