package net.isup.messages;

import net.isup.MessageType;

public class ContinuityCheckRequest extends MessageBase {
	public ContinuityCheckRequest(){
		super();
		type = MessageType.CONTINUITY_CHECK_REQUEST;
	}

	public void init(byte[] data) {
		// no data
	}

}
