package net.isup.parameters;

import net.isup.ParameterType;

public class TransmissionMediumRequirementPrime extends ParameterBase {
	public TransmissionMediumRequirementPrime(){
		type = ParameterType.TRANSMISSION_MEDIUM_REQUIREMENT_PRIME;
	}
}
