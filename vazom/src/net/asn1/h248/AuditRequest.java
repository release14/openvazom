package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuditRequest extends SEQUENCE{

	public TerminationID get_terminationID(){
		return (TerminationID)elements.get(0).data;
	}
	public TerminationID new_terminationID(){
		return new TerminationID();
	}

	public AuditDescriptor get_auditDescriptor(){
		return (AuditDescriptor)elements.get(1).data;
	}
	public AuditDescriptor new_auditDescriptor(){
		return new AuditDescriptor();
	}

	public TerminationIDList get_terminationIDList(){
		return (TerminationIDList)elements.get(2).data;
	}
	public TerminationIDList new_terminationIDList(){
		return new TerminationIDList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_terminationIDList(); }});
	}
	public AuditRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
