package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PQOS_ResponseTime extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _no_Delay = 1;
	public static final int _low_Delay = 2;
	public static final int _delay_Tolerant = 3;
	public PQOS_ResponseTime(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
