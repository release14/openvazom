package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class SEQUENCE extends ASNType {
	public SEQUENCE(){
		super();
		universalTag = ASNTag.SEQUENCE;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Constructed;
	}
}
