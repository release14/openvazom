package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class GmscCamelSubscriptionInfo extends SEQUENCE{

	public T_CSI get_t_CSI(){
		return (T_CSI)elements.get(0).data;
	}
	public T_CSI new_t_CSI(){
		return new T_CSI();
	}

	public O_CSI get_o_CSI(){
		return (O_CSI)elements.get(1).data;
	}
	public O_CSI new_o_CSI(){
		return new O_CSI();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public O_BcsmCamelTDPCriteriaList get_o_BcsmCamelTDP_CriteriaList(){
		return (O_BcsmCamelTDPCriteriaList)elements.get(3).data;
	}
	public O_BcsmCamelTDPCriteriaList new_o_BcsmCamelTDP_CriteriaList(){
		return new O_BcsmCamelTDPCriteriaList();
	}

	public T_BCSM_CAMEL_TDP_CriteriaList get_t_BCSM_CAMEL_TDP_CriteriaList(){
		return (T_BCSM_CAMEL_TDP_CriteriaList)elements.get(4).data;
	}
	public T_BCSM_CAMEL_TDP_CriteriaList new_t_BCSM_CAMEL_TDP_CriteriaList(){
		return new T_BCSM_CAMEL_TDP_CriteriaList();
	}

	public D_CSI get_d_csi(){
		return (D_CSI)elements.get(5).data;
	}
	public D_CSI new_d_csi(){
		return new D_CSI();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_t_CSI(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_o_CSI(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_o_BcsmCamelTDP_CriteriaList(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_t_BCSM_CAMEL_TDP_CriteriaList(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_d_csi(); }});
	}
	public GmscCamelSubscriptionInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
