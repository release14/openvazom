package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Ext_QoS_Subscribed extends OCTET_STRING{

	public Ext_QoS_Subscribed(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
