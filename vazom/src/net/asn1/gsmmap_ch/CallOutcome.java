package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallOutcome extends ENUMERATED{

	public static final int _success = 0;
	public static final int _failure = 1;
	public static final int _busy = 2;
	public CallOutcome(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
