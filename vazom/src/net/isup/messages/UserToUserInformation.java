package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;

public class UserToUserInformation extends MessageBase {
	public UserToUserInformation user_to_user_information;
	public UserToUserInformation(){
		super();
		type = MessageType.USER_TO_USER_INFORMATION;
		pointers = new Pointer[2];
		
	}

	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			pointers[1] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			user_to_user_information = new UserToUserInformation();
			user_to_user_information.init(getPointerData(data, 0));
			
			processOptional(data, 1);
			
		}
	}

}
