package net.smpp.pdu;

import net.smpp.PDUType;

public class Cancel_sm_resp extends PDUBase {

	public Cancel_sm_resp(){
		type = PDUType.CANCEL_SM_RESP;
	}

	public void init(byte[] data) {
		// no data

	}

}
