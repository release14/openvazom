package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class OTASPRequest extends SET{

	public ActionCode get_actionCode(){
		return (ActionCode)elements.get(0).data;
	}
	public ActionCode new_actionCode(){
		return new ActionCode();
	}

	public AKeyProtocolVersion get_aKeyProtocolVersion(){
		return (AKeyProtocolVersion)elements.get(1).data;
	}
	public AKeyProtocolVersion new_aKeyProtocolVersion(){
		return new AKeyProtocolVersion();
	}

	public AuthenticationData get_authenticationData(){
		return (AuthenticationData)elements.get(2).data;
	}
	public AuthenticationData new_authenticationData(){
		return new AuthenticationData();
	}

	public AuthenticationResponse get_authenticationResponse(){
		return (AuthenticationResponse)elements.get(3).data;
	}
	public AuthenticationResponse new_authenticationResponse(){
		return new AuthenticationResponse();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(4).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(5).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(6).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MobileStationMSID get_mobileStationMSID(){
		return (MobileStationMSID)elements.get(7).data;
	}
	public MobileStationMSID new_mobileStationMSID(){
		return new MobileStationMSID();
	}

	public MobileStationPartialKey get_mobileStationPartialKey(){
		return (MobileStationPartialKey)elements.get(8).data;
	}
	public MobileStationPartialKey new_mobileStationPartialKey(){
		return new MobileStationPartialKey();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(9).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public NewlyAssignedMSID get_newlyAssignedMSID(){
		return (NewlyAssignedMSID)elements.get(10).data;
	}
	public NewlyAssignedMSID new_newlyAssignedMSID(){
		return new NewlyAssignedMSID();
	}

	public RandomVariable get_randomVariable(){
		return (RandomVariable)elements.get(11).data;
	}
	public RandomVariable new_randomVariable(){
		return new RandomVariable();
	}

	public RandomVariableBaseStation get_randomVariableBaseStation(){
		return (RandomVariableBaseStation)elements.get(12).data;
	}
	public RandomVariableBaseStation new_randomVariableBaseStation(){
		return new RandomVariableBaseStation();
	}

	public ServiceIndicator get_serviceIndicator(){
		return (ServiceIndicator)elements.get(13).data;
	}
	public ServiceIndicator new_serviceIndicator(){
		return new ServiceIndicator();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(14).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(15).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public MEID get_meid(){
		return (MEID)elements.get(16).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(128, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_actionCode(); }});
		elements.add(new ElementDescriptor(181, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_aKeyProtocolVersion(); }});
		elements.add(new ElementDescriptor(161, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationData(); }});
		elements.add(new ElementDescriptor(35, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponse(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_mobileStationMSID(); }});
		elements.add(new ElementDescriptor(185, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileStationPartialKey(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_newlyAssignedMSID(); }});
		elements.add(new ElementDescriptor(40, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariable(); }});
		elements.add(new ElementDescriptor(41, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_randomVariableBaseStation(); }});
		elements.add(new ElementDescriptor(193, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceIndicator(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemCapabilities(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public OTASPRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
