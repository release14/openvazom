package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;

public class Answer extends MessageBase {
	public Answer(){
		super();
		type = MessageType.ANSWER;
		pointers = new Pointer[1];
	}

	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			// optional
			processOptional(data, 0);
			
		}
	}

}
