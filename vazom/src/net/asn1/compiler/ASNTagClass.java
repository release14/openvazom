package net.asn1.compiler;

import java.util.HashMap;

public enum ASNTagClass {
	UNIVERSAL(0x00),
	APPLICATION(0x40),
	CONTEXT_SPECIFIC(0x80),
	PRIVATE(0xC0);
	/*
	public synchronized static ASNTagClass fromId(int id){
		ASNTagClass res = null;
		switch(id){
			case 0x00: res = UNIVERSAL; break;
			case 0x40: res = APPLICATION; break;
			case 0x80: res = CONTEXT_SPECIFIC; break;
			case 0xC0: res = PRIVATE; break;
		}
		return res;
	}
*/
	private int id;
	private static final HashMap<Integer, ASNTagClass> lookup = new HashMap<Integer, ASNTagClass>();
	static{
		for(ASNTagClass td : ASNTagClass.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static ASNTagClass get(int id){ return lookup.get(id); }
	private ASNTagClass(int _id){ id = _id; }


}
