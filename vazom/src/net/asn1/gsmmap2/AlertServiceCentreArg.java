package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AlertServiceCentreArg extends SEQUENCE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public AddressString get_serviceCentreAddress(){
		return (AddressString)elements.get(1).data;
	}
	public AddressString new_serviceCentreAddress(){
		return new AddressString();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_serviceCentreAddress(); }});
	}
	public AlertServiceCentreArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
