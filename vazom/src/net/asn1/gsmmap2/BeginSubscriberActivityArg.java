package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class BeginSubscriberActivityArg extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public ISDN_AddressString get_originatingEntityNumber(){
		return (ISDN_AddressString)elements.get(1).data;
	}
	public ISDN_AddressString new_originatingEntityNumber(){
		return new ISDN_AddressString();
	}

	public AddressString get_msisdn(){
		return (AddressString)elements.get(2).data;
	}
	public AddressString new_msisdn(){
		return new AddressString();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_originatingEntityNumber(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.PRIVATE, true, false){public void set(){ data = new_msisdn(); }});
	}
	public BeginSubscriberActivityArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
