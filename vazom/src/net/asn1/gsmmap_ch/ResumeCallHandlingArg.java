package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ResumeCallHandlingArg extends SEQUENCE{

	public CallReferenceNumber get_callReferenceNumber(){
		return (CallReferenceNumber)elements.get(0).data;
	}
	public CallReferenceNumber new_callReferenceNumber(){
		return new CallReferenceNumber();
	}

	public Ext_BasicServiceCode get_basicServiceGroup(){
		return (Ext_BasicServiceCode)elements.get(1).data;
	}
	public Ext_BasicServiceCode new_basicServiceGroup(){
		return new Ext_BasicServiceCode();
	}

	public ForwardingData get_forwardingData(){
		return (ForwardingData)elements.get(2).data;
	}
	public ForwardingData new_forwardingData(){
		return new ForwardingData();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(3).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public CUG_CheckInfo get_cug_CheckInfo(){
		return (CUG_CheckInfo)elements.get(4).data;
	}
	public CUG_CheckInfo new_cug_CheckInfo(){
		return new CUG_CheckInfo();
	}

	public O_CSI get_o_CSI(){
		return (O_CSI)elements.get(5).data;
	}
	public O_CSI new_o_CSI(){
		return new O_CSI();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(6).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NULL get_ccbs_Possible(){
		return (NULL)elements.get(7).data;
	}
	public NULL new_ccbs_Possible(){
		return new NULL();
	}

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(8).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public UU_Data get_uu_Data(){
		return (UU_Data)elements.get(9).data;
	}
	public UU_Data new_uu_Data(){
		return new UU_Data();
	}

	public NULL get_allInformationSent(){
		return (NULL)elements.get(10).data;
	}
	public NULL new_allInformationSent(){
		return new NULL();
	}

	public D_CSI get_d_csi(){
		return (D_CSI)elements.get(11).data;
	}
	public D_CSI new_d_csi(){
		return new D_CSI();
	}

	public O_BcsmCamelTDPCriteriaList get_o_BcsmCamelTDPCriteriaList(){
		return (O_BcsmCamelTDPCriteriaList)elements.get(12).data;
	}
	public O_BcsmCamelTDPCriteriaList new_o_BcsmCamelTDPCriteriaList(){
		return new O_BcsmCamelTDPCriteriaList();
	}

	public Ext_BasicServiceCode get_basicServiceGroup2(){
		return (Ext_BasicServiceCode)elements.get(13).data;
	}
	public Ext_BasicServiceCode new_basicServiceGroup2(){
		return new Ext_BasicServiceCode();
	}

	public NULL get_mtRoamingRetry(){
		return (NULL)elements.get(14).data;
	}
	public NULL new_mtRoamingRetry(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callReferenceNumber(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_basicServiceGroup(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_forwardingData(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cug_CheckInfo(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_o_CSI(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ccbs_Possible(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uu_Data(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_allInformationSent(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_d_csi(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_o_BcsmCamelTDPCriteriaList(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_basicServiceGroup2(); }});
		elements.add(new ElementDescriptor(15, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_mtRoamingRetry(); }});
	}
	public ResumeCallHandlingArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
