package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARE_apdu_Associate_result extends SEQUENCE{

	public Associate_result get_result(){
		return (Associate_result)elements.get(0).data;
	}
	public Associate_result new_result(){
		return new Associate_result();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_result(); }});
	}
	public AARE_apdu_Associate_result(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 2;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
