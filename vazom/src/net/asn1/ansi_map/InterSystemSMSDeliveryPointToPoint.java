package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemSMSDeliveryPointToPoint extends SET{

	public SMS_BearerData get_sms_BearerData(){
		return (SMS_BearerData)elements.get(0).data;
	}
	public SMS_BearerData new_sms_BearerData(){
		return new SMS_BearerData();
	}

	public SMS_TeleserviceIdentifier get_sms_TeleserviceIdentifier(){
		return (SMS_TeleserviceIdentifier)elements.get(1).data;
	}
	public SMS_TeleserviceIdentifier new_sms_TeleserviceIdentifier(){
		return new SMS_TeleserviceIdentifier();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(2).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(3).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(4).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public SMS_MessageCount get_sms_MessageCount(){
		return (SMS_MessageCount)elements.get(5).data;
	}
	public SMS_MessageCount new_sms_MessageCount(){
		return new SMS_MessageCount();
	}

	public SMS_OriginalOriginatingAddress get_sms_OriginalOriginatingAddress(){
		return (SMS_OriginalOriginatingAddress)elements.get(6).data;
	}
	public SMS_OriginalOriginatingAddress new_sms_OriginalOriginatingAddress(){
		return new SMS_OriginalOriginatingAddress();
	}

	public SMS_OriginalOriginatingSubaddress get_sms_OriginalOriginatingSubaddress(){
		return (SMS_OriginalOriginatingSubaddress)elements.get(7).data;
	}
	public SMS_OriginalOriginatingSubaddress new_sms_OriginalOriginatingSubaddress(){
		return new SMS_OriginalOriginatingSubaddress();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(105, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sms_BearerData(); }});
		elements.add(new ElementDescriptor(116, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sms_TeleserviceIdentifier(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(108, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_MessageCount(); }});
		elements.add(new ElementDescriptor(112, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalOriginatingAddress(); }});
		elements.add(new ElementDescriptor(113, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_OriginalOriginatingSubaddress(); }});
	}
	public InterSystemSMSDeliveryPointToPoint(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
