package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class BulkDisconnection extends SET{

	public MSCID get_mscid(){
		return (MSCID)elements.get(0).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public TimeDateOffset get_timeDateOffset(){
		return (TimeDateOffset)elements.get(1).data;
	}
	public TimeDateOffset new_timeDateOffset(){
		return new TimeDateOffset();
	}

	public TimeOfDay get_timeOfDay(){
		return (TimeOfDay)elements.get(2).data;
	}
	public TimeOfDay new_timeOfDay(){
		return new TimeOfDay();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(3).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(4).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(275, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeDateOffset(); }});
		elements.add(new ElementDescriptor(309, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeOfDay(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
	}
	public BulkDisconnection(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
