package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PagingArea extends SEQUENCE{

	public LocationArea new_child(){
		return new LocationArea();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public LocationArea getChild(int index){
		return (LocationArea)of_children.get(index);
	}
	public PagingArea(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
