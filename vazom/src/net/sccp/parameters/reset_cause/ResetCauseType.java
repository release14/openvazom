package net.sccp.parameters.reset_cause;

import java.util.HashMap;


public enum ResetCauseType {
	END_USER_ORIGINATED(0x00),
	SCCP_USER_ORIGINATED(0x01),
	OUT_OF_ORDER_PS(0x02),
	OUT_OF_ORDER_PR(0x03),
	RPE_MESSAGE_OUT_OF_WINDOW(0x04),
	RPE_INCORRECT_PS(0x05),
	RPE_GENERAL(0x06),
	REMOTE_END_USER_OPERATIONAL(0x07),
	NETWORK_OPERATIONAL(0x08),
	ACCESS_OPERATIONAL(0x09),
	NETWORK_CONGESTION(0x0A),
	RESERVED(0x0B),
	UNQUALIFIED(0x0C);
	
	// 1 byte
	private int id;
	private static final HashMap<Integer, ResetCauseType> lookup = new HashMap<Integer, ResetCauseType>();
	static{
		for(ResetCauseType td : ResetCauseType.values()){
			lookup.put(td.getId(), td);
		}
	}
	private int getId(){ return id; }
	public static ResetCauseType get(int id){ return lookup.get(id); }
	private ResetCauseType(int _id){ id = _id; }
	
}
