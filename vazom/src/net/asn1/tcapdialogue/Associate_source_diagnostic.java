package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Associate_source_diagnostic extends CHOICE{

	public Associate_source_diagnostic_dialogue_service_user get_dialogue_service_user(){
		return (Associate_source_diagnostic_dialogue_service_user)elements.get(0).data;
	}
	public Associate_source_diagnostic_dialogue_service_user new_dialogue_service_user(){
		return new Associate_source_diagnostic_dialogue_service_user();
	}

	public Dialogue_service_providerInteger get_dialogue_service_provider(){
		return (Dialogue_service_providerInteger)elements.get(1).data;
	}
	public Dialogue_service_providerInteger new_dialogue_service_provider(){
		return new Dialogue_service_providerInteger();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dialogue_service_user(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_dialogue_service_provider(); }});
	}
	public Associate_source_diagnostic(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
