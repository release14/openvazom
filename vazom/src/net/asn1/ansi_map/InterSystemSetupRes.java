package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemSetupRes extends SET{

	public CDMAConnectionReferenceList get_cdmaConnectionReferenceList(){
		return (CDMAConnectionReferenceList)elements.get(0).data;
	}
	public CDMAConnectionReferenceList new_cdmaConnectionReferenceList(){
		return new CDMAConnectionReferenceList();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(1).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public ISLPInformation get_ilspInformation(){
		return (ISLPInformation)elements.get(2).data;
	}
	public ISLPInformation new_ilspInformation(){
		return new ISLPInformation();
	}

	public SetupResult get_setupResult(){
		return (SetupResult)elements.get(3).data;
	}
	public SetupResult new_setupResult(){
		return new SetupResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(212, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceList(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(217, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ilspInformation(); }});
		elements.add(new ElementDescriptor(151, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_setupResult(); }});
	}
	public InterSystemSetupRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
