package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MessageBodyType extends SEQUENCE{

	public MessageBodyChoice get_messageBody(){
		return (MessageBodyChoice)elements.get(0).data;
	}
	public MessageBodyChoice new_messageBody(){
		return new MessageBodyChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_messageBody(); }});
	}
	public MessageBodyType(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
