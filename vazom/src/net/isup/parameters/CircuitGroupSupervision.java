package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.cgs.CircuitGroupSupervisionType;

public class CircuitGroupSupervision extends ParameterBase {
	public CircuitGroupSupervisionType circuit_group_indicator;
	public CircuitGroupSupervision(){
		type = ParameterType.CIRCUIT_GROUP_SUPERVISION;
	}

	public void init(byte[] data) {
		if(data != null){
			circuit_group_indicator = CircuitGroupSupervisionType.get(data[byte_position] & 0x03);
		}
	}

}
