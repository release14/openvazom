package net.sccp.parameters.global_title;

import java.util.HashMap;

public enum EncodingScheme {
	UNKNOWN(0x00),
	BCD_ODD(0x01),
	BCD_EVEN(0x02),
	NATIONAL_SPECIFIC(0x03);
	
	// BITS 0..3 0000XXXX
	private int id;
	private static final HashMap<Integer, EncodingScheme> lookup = new HashMap<Integer, EncodingScheme>();
	static{
		for(EncodingScheme td : EncodingScheme.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static EncodingScheme get(int id){ return lookup.get(id); }
	private EncodingScheme(int _id){ id = _id; }
	
}
