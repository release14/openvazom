package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffMeasurementRequest2Res extends SET{

	public CDMATargetMeasurementList get_cdmaTargetMeasurementList(){
		return (CDMATargetMeasurementList)elements.get(0).data;
	}
	public CDMATargetMeasurementList new_cdmaTargetMeasurementList(){
		return new CDMATargetMeasurementList();
	}

	public TargetMeasurementList get_targetMeasurementList(){
		return (TargetMeasurementList)elements.get(1).data;
	}
	public TargetMeasurementList new_targetMeasurementList(){
		return new TargetMeasurementList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(134, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMeasurementList(); }});
		elements.add(new ElementDescriptor(157, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetMeasurementList(); }});
	}
	public HandoffMeasurementRequest2Res(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
