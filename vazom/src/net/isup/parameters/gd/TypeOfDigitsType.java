package net.isup.parameters.gd;

import java.util.HashMap;

public enum TypeOfDigitsType {
	ACCOUNT_CODE(0x00),
	AUTHORISATION_CODE(0x01),
	PRIVATE_NETWORKING_TRAVELLING_CLASS_MASK(0x02),
	BUSINESS_COMMUNICATION_GROUP_IDENTITY(0x03);
	
	
	private int id;
	private static final HashMap<Integer, TypeOfDigitsType> lookup = new HashMap<Integer, TypeOfDigitsType>();
	static{
		for(TypeOfDigitsType td :TypeOfDigitsType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static TypeOfDigitsType get(int id){ return lookup.get(id); }
	private TypeOfDigitsType(int _id){ id = _id; }	
}
