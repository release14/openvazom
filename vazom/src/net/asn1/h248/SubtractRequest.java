package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SubtractRequest extends SEQUENCE{

	public TerminationIDList get_terminationID(){
		return (TerminationIDList)elements.get(0).data;
	}
	public TerminationIDList new_terminationID(){
		return new TerminationIDList();
	}

	public AuditDescriptor get_auditDescriptor(){
		return (AuditDescriptor)elements.get(1).data;
	}
	public AuditDescriptor new_auditDescriptor(){
		return new AuditDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_auditDescriptor(); }});
	}
	public SubtractRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
