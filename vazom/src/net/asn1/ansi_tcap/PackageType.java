package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PackageType extends CHOICE{

	public UniTransactionPDU get_unidirectional(){
		return (UniTransactionPDU)elements.get(0).data;
	}
	public UniTransactionPDU new_unidirectional(){
		return new UniTransactionPDU();
	}

	public TransactionPDU get_queryWithPerm(){
		return (TransactionPDU)elements.get(1).data;
	}
	public TransactionPDU new_queryWithPerm(){
		return new TransactionPDU();
	}

	public TransactionPDU get_queryWithoutPerm(){
		return (TransactionPDU)elements.get(2).data;
	}
	public TransactionPDU new_queryWithoutPerm(){
		return new TransactionPDU();
	}

	public TransactionPDU get_response(){
		return (TransactionPDU)elements.get(3).data;
	}
	public TransactionPDU new_response(){
		return new TransactionPDU();
	}

	public TransactionPDU get_conversationWithPerm(){
		return (TransactionPDU)elements.get(4).data;
	}
	public TransactionPDU new_conversationWithPerm(){
		return new TransactionPDU();
	}

	public TransactionPDU get_conversationWithoutPerm(){
		return (TransactionPDU)elements.get(5).data;
	}
	public TransactionPDU new_conversationWithoutPerm(){
		return new TransactionPDU();
	}

	public Abort get_abort(){
		return (Abort)elements.get(6).data;
	}
	public Abort new_abort(){
		return new Abort();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_unidirectional(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_queryWithPerm(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_queryWithoutPerm(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_response(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_conversationWithPerm(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_conversationWithoutPerm(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_abort(); }});
	}
	public PackageType(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
