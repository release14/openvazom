package net.db;

import net.ds.DataSourceType;

public abstract class DBRecordBase {
	public DBRecordType recordType;
	public long timestamp;
	public boolean no_reply = false;
	public DataSourceType dataSource;
	public Long tcap_sid;
	public Long tcap_did;
	public String app_ctx_oid;
	public Integer gsm_map_error_code;
	public Integer tcap_dialogue_error_code;
	public Integer tcap_abort_error_code;
	public Integer smpp_error_code;
	public Integer filter_action_id;
	public Integer filter_total_score;
	public String filter_exit_point;
	// default = OK
	public int sms_status = 1;

	public DBRecordBase(){
		
	}

}
