package net.smpp.parameters.optional;

import net.smpp.NumberingPlan;
import net.smpp.OptionalParameterType;
import net.smpp.TypeOfNumber;
import net.utils.Utils;

public class Callback_num extends OptionalParameterBase {

	public Callback_num(){
		type = OptionalParameterType.CALLBACK_NUM;
	}
	public boolean dtmf;
	public TypeOfNumber ton;
	public NumberingPlan npi;
	public String digits;
	public void init(byte[] data) {
		dtmf = data[0] == 0x01;
		ton = TypeOfNumber.get(data[1] & 0xff);
		npi = NumberingPlan.get(data[2] & 0xff);
		digits = Utils.Cstr_decode(data, 3);
	}

}
