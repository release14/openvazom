package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PropertyParm extends SEQUENCE{

	public PkgdName get_propertyName(){
		return (PkgdName)elements.get(0).data;
	}
	public PkgdName new_propertyName(){
		return new PkgdName();
	}

	public PropertyIDSequenceOf get_value(){
		return (PropertyIDSequenceOf)elements.get(1).data;
	}
	public PropertyIDSequenceOf new_value(){
		return new PropertyIDSequenceOf();
	}

	public PropertyParmExtraInfoChoiceWrapper get_extraInfo(){
		return (PropertyParmExtraInfoChoiceWrapper)elements.get(2).data;
	}
	public PropertyParmExtraInfoChoiceWrapper new_extraInfo(){
		return new PropertyParmExtraInfoChoiceWrapper();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_propertyName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_value(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extraInfo(); }});
	}
	public PropertyParm(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
