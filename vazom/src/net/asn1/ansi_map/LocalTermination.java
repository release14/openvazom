package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocalTermination extends SEQUENCE{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public TerminationTreatment get_terminationTreatment(){
		return (TerminationTreatment)elements.get(1).data;
	}
	public TerminationTreatment new_terminationTreatment(){
		return new TerminationTreatment();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(2).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(3).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(4).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(5).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(6).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(7).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(8).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(9).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(10).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(11).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public VoiceMailboxPIN get_voiceMailboxPIN(){
		return (VoiceMailboxPIN)elements.get(12).data;
	}
	public VoiceMailboxPIN new_voiceMailboxPIN(){
		return new VoiceMailboxPIN();
	}

	public VoiceMailboxNumber get_voiceMailboxNumber(){
		return (VoiceMailboxNumber)elements.get(13).data;
	}
	public VoiceMailboxNumber new_voiceMailboxNumber(){
		return new VoiceMailboxNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(121, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationTreatment(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(159, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voiceMailboxPIN(); }});
		elements.add(new ElementDescriptor(160, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voiceMailboxNumber(); }});
	}
	public LocalTermination(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
