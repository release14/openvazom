package net.isup.parameters;

import net.isup.ParameterType;

public class AutomaticCongestionLevel extends ParameterBase {
	public boolean congestion_level_1_exceeded;
	public boolean congestion_level_2_exceeded;
	
	public AutomaticCongestionLevel(){
		type = ParameterType.AUTOMATIC_CONGESTION_LEVEL;
	}

	public void init(byte[] data) {
		if(data != null){
			congestion_level_1_exceeded = data[0] == 0x01;
			congestion_level_2_exceeded = data[0] == 0x02;
			
		}
	}

}
