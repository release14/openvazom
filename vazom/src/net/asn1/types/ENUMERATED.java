package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class ENUMERATED extends ASNType {

	public ENUMERATED(){
		super();
		universalTag = ASNTag.ENUMERATED;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Primitive;
		
	}
}
