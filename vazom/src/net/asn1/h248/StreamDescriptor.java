package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StreamDescriptor extends SEQUENCE{

	public StreamID get_streamID(){
		return (StreamID)elements.get(0).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public StreamParms get_streamParms(){
		return (StreamParms)elements.get(1).data;
	}
	public StreamParms new_streamParms(){
		return new StreamParms();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_streamParms(); }});
	}
	public StreamDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
