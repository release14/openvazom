package net.stats;

public class RefilterStats {
	public long POSITION;
	public long MIN_ID;
	public long MAX_ID;
	public String MIN_TS;
	public String MAX_TS;
	public long START_TIME;
	public long ELAPSED_TIME;
	
	
	public synchronized void incPos(){
		POSITION++;
	}
	
}
