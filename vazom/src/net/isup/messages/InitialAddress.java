package net.isup.messages;

import java.util.Arrays;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.CalledPartyNumber;
import net.isup.parameters.CallingPartyCategory;
import net.isup.parameters.ForwardCallIndicators;
import net.isup.parameters.NatureOfConnIndicators;
import net.isup.parameters.TransmissionMediumRequirement;

public class InitialAddress extends MessageBase {
	public NatureOfConnIndicators nature_of_connection_indicators;
	public ForwardCallIndicators forward_call_indicators;
	public CallingPartyCategory calling_party_category;
	public TransmissionMediumRequirement transmission_medium_requirement;
	public CalledPartyNumber called_party_number;
	
	public InitialAddress(){
		super();
		type = MessageType.INITIAL_ADDRESS;
		pointers = new Pointer[2];
	}

	public void init(byte[] data) {
		if(data != null){
			// mandatory
			nature_of_connection_indicators = new NatureOfConnIndicators();
			nature_of_connection_indicators.init(new byte[]{data[byte_pos++]});
			
			forward_call_indicators = new ForwardCallIndicators();
			forward_call_indicators.init(Arrays.copyOfRange(data, byte_pos, byte_pos + 2));
			byte_pos += 2;
			
			calling_party_category = new CallingPartyCategory();
			calling_party_category.init(new byte[]{data[byte_pos++]});
			
			transmission_medium_requirement = new TransmissionMediumRequirement();
			transmission_medium_requirement.init(new byte[]{data[byte_pos++]});
			
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			pointers[1] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			// variable mandatory
			called_party_number = new CalledPartyNumber();
			called_party_number.init(getPointerData(data, 0));
			
			// optional
			processOptional(data, 1);
			
		}
	}

}
