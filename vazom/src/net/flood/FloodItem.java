package net.flood;

import java.util.ArrayList;

public class FloodItem {
	public String item;
	public long minute;
	public long hour;
	public long day;
	
	
	public ArrayList<Long> tmp_minute;
	public ArrayList<Long> tmp_hour;
	
	public FloodItem(){
		tmp_hour = new ArrayList<Long>();
		tmp_minute = new ArrayList<Long>();
	}
	
}
