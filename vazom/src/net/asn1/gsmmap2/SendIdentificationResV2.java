package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendIdentificationResV2 extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public TripletListold get_tripletList(){
		return (TripletListold)elements.get(1).data;
	}
	public TripletListold new_tripletList(){
		return new TripletListold();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_tripletList(); }});
	}
	public SendIdentificationResV2(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
