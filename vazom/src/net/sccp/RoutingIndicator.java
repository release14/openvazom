package net.sccp;

import java.util.HashMap;

public enum RoutingIndicator {
	ROUTE_ON_SSN(0x40),
	ROUTE_ON_GT(0x00);
	
	// bit 7
	private int id;
	private static final HashMap<Integer, RoutingIndicator> lookup = new HashMap<Integer, RoutingIndicator>();
	static{
		for(RoutingIndicator td : RoutingIndicator.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static RoutingIndicator get(int id){ return lookup.get(id); }
	private RoutingIndicator(int _id){ id = _id; }
	
}
