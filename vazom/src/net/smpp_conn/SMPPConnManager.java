package net.smpp_conn;

import java.net.Socket;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import net.config.SGNConfigData;
import net.ds.DataSourceType;
import net.logging.LoggingManager;
import net.routing.RoutingConnectionDirectionType;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionType;
import net.routing.RoutingConnectionWorkerMethod;
import net.routing.RoutingManager;
import net.sgc.SGCManager;
import net.sgn.SGNode;
import net.smpp.ErrorCodeType;
import net.smpp.OptionalParameterType;
import net.smpp.SMPP_PDU;
import net.smpp.TimeFormat;
import net.smpp.parameters.optional.OptionalParameterBase;
import net.smpp.parameters.optional.Sar_msg_ref_num;
import net.smpp.parameters.optional.Sar_segment_seqnum;
import net.smpp.parameters.optional.Sar_total_segments;
import net.smpp.pdu.Bind_receiver;
import net.smpp.pdu.Bind_receiver_resp;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Bind_transceiver_resp;
import net.smpp.pdu.Bind_transmitter;
import net.smpp.pdu.Bind_transmitter_resp;
import net.smpp.pdu.Deliver_sm;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_multi;
import net.smpp.pdu.Submit_sm;
import net.smpp.pdu.Submit_sm_resp;
import net.smpp.pdu.Unbind;
import net.smpp.pdu.Unbind_resp;
import net.smpp.pdu.Submit_multi.DestinationAddress;
import net.smstpdu.SmsType;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.HeaderDescriptor;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;
import net.vstp.VSTP_SGF_CorrelationManager;
import net.vstp.VSTP_SGF_CorrelationPacket;

import org.apache.log4j.Logger;

public class SMPPConnManager {
	public static Logger logger=Logger.getLogger(SMPPConnManager.class);
	public static ConcurrentHashMap<String, ProxyCorrelation> proxy_map;
	public static ConcurrentHashMap<String, SMPPConnection> conn_map;

	public static ConcurrentHashMap<String, String> smpp_user_map;
	private static ConcurrentHashMap<String, MessageDescriptor> smpp_user_correlation_map;
	
	public static boolean stopping;

	private static SMPPMonitor smpp_mon_r;
	private static Thread smpp_mon_t;
	
	private static Smpp_user_sync smpp_user_sync_r;
	private static Thread smpp_user_sync_t;

	private static class Smpp_user_sync implements Runnable{
		MessageDescriptor md = null;
		UUID uid = null;

		public void run() {
			LoggingManager.info(logger, "Starting...");
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.smpp_users_sync_interval); }catch(Exception e){ e.printStackTrace();	}
				// clear previouse correlations
				smpp_user_correlation_map.clear();
				
				uid = UUID.randomUUID();
				md = new MessageDescriptor();
				md.header.destination = LocationType.FN;
				md.header.ds = DataSourceType.NA;
				md.header.msg_id = uid.toString();
				md.header.msg_type = MessageType.DB_REQUEST;
				md.header.source = LocationType.SGN;
				md.callback = new SMPP_USER_SYNC_Callback(md.header.msg_id);
				// body
				md.values.put(VSTPDataItemType.DB_REQUEST_TYPE.getId(), VSTPDataItemType.DB_SMPP_USER_SYNC.getId());
				
				// save correlation
				smpp_user_correlation_map.put(md.header.msg_id, md);
				
				// send
				SGNode.out_offer(md);
				
				
			}
			LoggingManager.info(logger, "Ending...");
		}
	
	}	
	
	private static class SMPPMonitor implements Runnable{
		public void run() {
			SMPPConnection conn = null;
			LoggingManager.debug(SGNConfigData.smpp_debug, logger, "Starting...");
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.smpp_check_inerval ); }catch(Exception e){ e.printStackTrace(); }
				for(String s: conn_map.keySet()){
					conn = conn_map.get(s);
					if(!conn.active && !conn.down){
						LoggingManager.debug(SGNConfigData.smpp_debug, logger, "Reactivating SMPP Connection [" + conn.id + "], address = [" + conn.ip + "], port = [" + conn.port + "]!");
						reconnect(conn.id);
					}
				}
			}
			LoggingManager.debug(SGNConfigData.smpp_debug, logger, "Ending...");
			
		}
	}
	
	// used for proxying data from end point connection to client connection
	private static class SMPPProxyConverter extends RoutingConnectionWorkerMethod{
		SMPPClientConnection client_conn;
		
		public SMPPProxyConverter(SMPPConnection conn, SMPPClientConnection _client_conn){
			super(conn);
			client_conn = _client_conn;
		}
		public void execute(Object o) {
			client_conn.out_offer((byte[])o);
		}
	}
	
	
	public static void remove_smpp_user(String user){
		if(user != null) smpp_user_map.remove(user);
	}
	public static void add_smpp_user(String user, String pwd){
		if(user != null && pwd != null) smpp_user_map.put(user, pwd);
	}
	public static ErrorCodeType auth_smpp_user(String user, String pwd){
		if(user != null && pwd != null){
			String auth_user_pwd = smpp_user_map.get(user);
			if(auth_user_pwd != null){
				if(auth_user_pwd.equals(pwd)) return ErrorCodeType.ESME_ROK; else return ErrorCodeType.ESME_RINVPASWD;
			}else return ErrorCodeType.ESME_RINVSYSID;
			
		}
		return ErrorCodeType.ESME_RBINDFAIL;
	}
	
	public static void start_SMPP_monitor(){
		smpp_mon_r = new SMPPMonitor();
		smpp_mon_t = new Thread(smpp_mon_r, "SMPP_MONITOR");
		smpp_mon_t.start();
	}
	
	public static boolean connectionExists(String conn_id){
		return (conn_map.get(conn_id) != null);
	}
	
	public static void addConnection(	String id, 
										String ip, 
										int port, 
										String system_id, 
										String password, 
										int el_interval,
										RoutingConnectionDirectionType direction){
		SMPPConnection conn = null;
		if(!connectionExists(id)){
			switch(direction){
				case CLIENT:
					conn = new SMPPConnection(id, ip, port, system_id, password, el_interval);
					conn.direction = direction;
					if(conn.initSocket()){
						conn.initSMPP();
						
					}
					break;
				case SERVER:
					conn = new SMPPConnection(id, ip, port, null, null, 0, direction);
					conn.start();
					conn.init_server(ip, port);
					break;
			}
			
			if(conn != null) conn_map.put(id, conn);
			
		}else{
			LoggingManager.debug(SGNConfigData.smpp_debug, logger, "Connection [" + id + "] already exists!");

		}
	}
	
	public static SMPPConnection getConnection(String id){
		return conn_map.get(id);
	}
	
	public static void removeConnection(String conn_id){
		conn_map.remove(conn_id);
	}
	
	public static SMPPConnection shutConnection(String conn_id){
		SMPPConnection conn = conn_map.get(conn_id);
		if(conn != null){
			conn.down = true;
			conn.stop();
			return conn;
		}
		return null;
	}
	
	public static SMPPConnection noshutConnection(String conn_id){
		SMPPConnection conn = conn_map.get(conn_id);
		if(conn != null){
			conn.down = false;
			reconnect(conn_id);
			return conn;
		}
		return null;
	}	
	
	public static void reconnect(String conn_id){
		SMPPConnection conn = getConnection(conn_id);
		if(conn != null){
			if(conn.initSocket()){
				conn.initSMPP();
			}
		}
	
	}

	public static void processFilter_sms(SMPPPacket smpp){
		MessageDescriptor md = null;
		VSTP_SGF_CorrelationPacket vsp = null;
		Submit_sm submit_sm = null;
		Submit_multi submit_multi = null;
		Deliver_sm deliver_sm = null;
		OptionalParameterBase opp = null;
		
		if(smpp.smpp_pdu != null){
			try{
				md = new MessageDescriptor();
				// VSTP
				// header
				md.header = new HeaderDescriptor();
				md.header.destination = LocationType.FN;
				// DS
				md.header.ds = DataSourceType.SMPP;
				md.header.msg_id = smpp.client_ip + "." + smpp.client_port + "." + smpp.smpp_pdu.sequence_number;
				md.header.source = LocationType.SGN;
				// vstp-sri correlation
				vsp = new VSTP_SGF_CorrelationPacket();
				vsp.smpp_packet = smpp;
				vsp.smpp_packet_data = smpp.smpp_data;
				vsp.type = RoutingConnectionType.SMPP;
				VSTP_SGF_CorrelationManager.add(md.header.msg_id, vsp);
				// distribute
				SGCManager.distribute_vstp_crl_add(md.header.msg_id, vsp);
				// stats
				StatsManager.VSTP_STATS.VSTP_CRL_ADD++;

				// VSTP body
				md.values.put(VSTPDataItemType.IP_SOURCE.getId(), smpp.client_ip);
				md.values.put(VSTPDataItemType.IP_DESTINATION.getId(), smpp.host_ip);
				md.values.put(VSTPDataItemType.TCP_SOURCE.getId(), Integer.toString(smpp.client_port));
				md.values.put(VSTPDataItemType.TCP_DESTINATION.getId(), Integer.toString(smpp.host_port));
				md.values.put(VSTPDataItemType.SMPP_SYSTEM_ID.getId(), smpp.system_id);
				md.values.put(VSTPDataItemType.SMPP_PASSWORD.getId(), smpp.password);
				
				
				
				
				switch(smpp.smpp_pdu.type){
					case SUBMIT_MULTI:
						md.header.msg_type = MessageType.SMPP_MO;
						submit_multi = (Submit_multi)smpp.smpp_pdu;
						md.values.put(VSTPDataItemType.SMPP_SERVICE_TYPE.getId(), submit_multi.service_type.toString());
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_TON.getId(), Integer.toString(submit_multi.source_addr_ton.getId()));
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_NP.getId(), Integer.toString(submit_multi.source_addr_npi.getId()));
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_ADDRESS.getId(), submit_multi.source_addr);
						
						DestinationAddress da = null;
						//String tmp_ton = null;
						//String tmp_np = null;
						String tmp_addr = null;
						for(int i = 0; i<submit_multi.dest_address_lst.length; i++){
							da = submit_multi.dest_address_lst[i];
							/*
							if(da.type == DestFlagType.SME_ADDRESS){
								tmp_ton += da.dest_addr_ton.getId() + ":";
								tmp_np += da.dest_addr_npi.getId() + ":";
							}
							*/
							tmp_addr += da.destination_addr + ":";
						}
						//if(tmp_ton != null) tmp_ton = tmp_ton.substring(0, tmp_ton.length() - 1);
						//if(tmp_np != null) tmp_np = tmp_np.substring(0, tmp_np.length() - 1);
						if(tmp_addr != null) tmp_addr = tmp_addr.substring(0, tmp_addr.length() - 1);
						
						
						//md.values.put(VSTPDataItemType.SMPP_RECIPIENT_TON.getId(), tmp_ton);
						//md.values.put(VSTPDataItemType.SMPP_RECIPIENT_NP.getId(), tmp_np);
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_ADDRESS.getId(), tmp_addr);
						
						md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_MODE.getId(), Integer.toString(submit_multi.message_mode.getId()));
						md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_TYPE.getId(), Integer.toString(submit_multi.message_type.getId()));
						md.values.put(VSTPDataItemType.SMPP_ESM_GSM_FEATURES.getId(), Integer.toString(submit_multi.ns_features.getId()));
						md.values.put(VSTPDataItemType.SMPP_PROTOCOL_ID.getId(), Integer.toString(submit_multi.protocol_id));
						md.values.put(VSTPDataItemType.SMPP_PRIORITY_FLAG.getId(), Integer.toString(submit_multi.priority_flag));
						md.values.put(VSTPDataItemType.SMPP_DELIVERY_TIME.getId(), Long.toString(TimeFormat.toTimestamp(submit_multi.schedule_delivery_time).ts));
						md.values.put(VSTPDataItemType.SMPP_VALIDITY_PERIOD.getId(), Long.toString(TimeFormat.toTimestamp(submit_multi.validity_period).ts));
						md.values.put(VSTPDataItemType.SMPP_RD_SMSC_RECEIPT.getId(), Integer.toString(submit_multi.rd_smsc_delivery_receipt.getId()));
						md.values.put(VSTPDataItemType.SMPP_RD_SME_ACK.getId(), Integer.toString(submit_multi.rd_sme_orig_ack.getId()));
						md.values.put(VSTPDataItemType.SMPP_RD_INTERMEDIATE_NOTIFICATION.getId(), Integer.toString(submit_multi.rd_intermediate_notification.getId()));
						md.values.put(VSTPDataItemType.SMPP_REPLACE_IF_PRESENT.getId(), Integer.toString(submit_multi.replace_if_present_flag));
						md.values.put(VSTPDataItemType.SMPP_DATA_CODING.getId(), Integer.toString(submit_multi.data_coding.getId()));
						md.values.put(VSTPDataItemType.SMPP_SM_DEFAULT_MSG_ID.getId(), Integer.toString(submit_multi.sm_default_msg_id));
						md.values.put(VSTPDataItemType.SMPP_SM_LENGTH.getId(), Integer.toString(submit_multi.sm_length));
						md.values.put(VSTPDataItemType.SMPP_SM.getId(), Utils.bytes2hexstr(submit_multi.short_message, ""));
						// UDH
						opp = submit_multi.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						// CONCATENATED
						if(opp != null){
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
							// msg ref
							md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(((Sar_msg_ref_num)opp).value));
							// msg part number
							opp = submit_multi.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
							if(opp != null) md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(((Sar_segment_seqnum)opp).value));
							// msg total parts
							opp = submit_multi.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
							if(opp != null) md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(((Sar_total_segments)opp).value));
						// SINGLE
						}else{
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
						}
						
						break;
						
						
					case SUBMT_SM:
						md.header.msg_type = MessageType.SMPP_MO;
						submit_sm = (Submit_sm)smpp.smpp_pdu;
						md.values.put(VSTPDataItemType.SMPP_SERVICE_TYPE.getId(), submit_sm.service_type.toString());
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_TON.getId(), Integer.toString(submit_sm.source_addr_ton.getId()));
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_NP.getId(), Integer.toString(submit_sm.source_addr_npi.getId()));
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_ADDRESS.getId(), submit_sm.source_addr);
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_TON.getId(), Integer.toString(submit_sm.dest_addr_ton.getId()));
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_NP.getId(), Integer.toString(submit_sm.dest_addr_npi.getId()));
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_ADDRESS.getId(), submit_sm.destination_addr);
						md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_MODE.getId(), Integer.toString(submit_sm.message_mode.getId()));
						md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_TYPE.getId(), Integer.toString(submit_sm.message_type.getId()));
						md.values.put(VSTPDataItemType.SMPP_ESM_GSM_FEATURES.getId(), Integer.toString(submit_sm.ns_features.getId()));
						md.values.put(VSTPDataItemType.SMPP_PROTOCOL_ID.getId(), Integer.toString(submit_sm.protocol_id));
						md.values.put(VSTPDataItemType.SMPP_PRIORITY_FLAG.getId(), Integer.toString(submit_sm.priority_flag));
						md.values.put(VSTPDataItemType.SMPP_DELIVERY_TIME.getId(), Long.toString(TimeFormat.toTimestamp(submit_sm.schedule_delivery_time).ts));
						md.values.put(VSTPDataItemType.SMPP_VALIDITY_PERIOD.getId(), Long.toString(TimeFormat.toTimestamp(submit_sm.validity_period).ts));
						md.values.put(VSTPDataItemType.SMPP_RD_SMSC_RECEIPT.getId(), Integer.toString(submit_sm.rd_smsc_delivery_receipt.getId()));
						md.values.put(VSTPDataItemType.SMPP_RD_SME_ACK.getId(), Integer.toString(submit_sm.rd_sme_orig_ack.getId()));
						md.values.put(VSTPDataItemType.SMPP_RD_INTERMEDIATE_NOTIFICATION.getId(), Integer.toString(submit_sm.rd_intermediate_notification.getId()));
						md.values.put(VSTPDataItemType.SMPP_REPLACE_IF_PRESENT.getId(), Integer.toString(submit_sm.replace_if_present_flag));
						md.values.put(VSTPDataItemType.SMPP_DATA_CODING.getId(), Integer.toString(submit_sm.data_coding.getId()));
						md.values.put(VSTPDataItemType.SMPP_SM_DEFAULT_MSG_ID.getId(), Integer.toString(submit_sm.sm_default_msg_id));
						md.values.put(VSTPDataItemType.SMPP_SM_LENGTH.getId(), Integer.toString(submit_sm.sm_length));
						md.values.put(VSTPDataItemType.SMPP_SM.getId(), Utils.bytes2hexstr(submit_sm.short_message, ""));
						
						
						// UDH
						opp = submit_sm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						// CONCATENATED
						if(opp != null){
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
							// msg ref
							md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(((Sar_msg_ref_num)opp).value));
							// msg part number
							opp = submit_sm.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
							if(opp != null) md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(((Sar_segment_seqnum)opp).value));
							// msg total parts
							opp = submit_sm.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
							if(opp != null) md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(((Sar_total_segments)opp).value));
						// SINGLE
						}else{
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
						}
						break;
						
					case DELIVER_SM:
						md.header.msg_type = MessageType.SMPP_MT;
						deliver_sm = (Deliver_sm)smpp.smpp_pdu;
						md.values.put(VSTPDataItemType.SMPP_SERVICE_TYPE.getId(), deliver_sm.service_type.toString());
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_TON.getId(), Integer.toString(deliver_sm.source_addr_ton.getId()));
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_NP.getId(), Integer.toString(deliver_sm.source_addr_npi.getId()));
						md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_ADDRESS.getId(), deliver_sm.source_addr);
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_TON.getId(), Integer.toString(deliver_sm.dest_addr_ton.getId()));
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_NP.getId(), Integer.toString(deliver_sm.dest_addr_npi.getId()));
						md.values.put(VSTPDataItemType.SMPP_RECIPIENT_ADDRESS.getId(), deliver_sm.destination_addr);
						md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_MODE.getId(), Integer.toString(deliver_sm.message_mode.getId()));
						md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_TYPE.getId(), Integer.toString(deliver_sm.message_type.getId()));
						md.values.put(VSTPDataItemType.SMPP_ESM_GSM_FEATURES.getId(), Integer.toString(deliver_sm.ns_features.getId()));
						md.values.put(VSTPDataItemType.SMPP_PROTOCOL_ID.getId(), Integer.toString(deliver_sm.protocol_id));
						md.values.put(VSTPDataItemType.SMPP_PRIORITY_FLAG.getId(), Integer.toString(deliver_sm.priority_flag));
						md.values.put(VSTPDataItemType.SMPP_DELIVERY_TIME.getId(), Long.toString(TimeFormat.toTimestamp(deliver_sm.schedule_delivery_time).ts));
						md.values.put(VSTPDataItemType.SMPP_VALIDITY_PERIOD.getId(), Long.toString(TimeFormat.toTimestamp(deliver_sm.validity_period).ts));
						md.values.put(VSTPDataItemType.SMPP_RD_SMSC_RECEIPT.getId(), Integer.toString(deliver_sm.rd_smsc_delivery_receipt.getId()));
						md.values.put(VSTPDataItemType.SMPP_RD_SME_ACK.getId(), Integer.toString(deliver_sm.rd_sme_orig_ack.getId()));
						md.values.put(VSTPDataItemType.SMPP_RD_INTERMEDIATE_NOTIFICATION.getId(), Integer.toString(deliver_sm.rd_intermediate_notification.getId()));
						md.values.put(VSTPDataItemType.SMPP_REPLACE_IF_PRESENT.getId(), Integer.toString(deliver_sm.replace_if_present_flag));
						md.values.put(VSTPDataItemType.SMPP_DATA_CODING.getId(), Integer.toString(deliver_sm.data_coding.getId()));
						md.values.put(VSTPDataItemType.SMPP_SM_DEFAULT_MSG_ID.getId(), Integer.toString(deliver_sm.sm_default_msg_id));
						md.values.put(VSTPDataItemType.SMPP_SM_LENGTH.getId(), Integer.toString(deliver_sm.sm_length));
						md.values.put(VSTPDataItemType.SMPP_SM.getId(), Utils.bytes2hexstr(deliver_sm.short_message, ""));
						// UDH
						opp = deliver_sm.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
						// CONCATENATED
						if(opp != null){
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
							// msg ref
							md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(((Sar_msg_ref_num)opp).value));
							// msg part number
							opp = deliver_sm.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
							if(opp != null) md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(((Sar_segment_seqnum)opp).value));
							// msg total parts
							opp = deliver_sm.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
							if(opp != null) md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(((Sar_total_segments)opp).value));
						// SINGLE
						}else{
							md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
						}						
						break;
				}
				
				//System.out.println(new String(md.encode()));
				// send to queue
				SGNode.out_offer(md);
				//logger.debug("VSTP id = [" + md.header.msg_id + "], type = [" + md.header.msg_type + "] sent to filter node!");
				//System.out.println("---------- " + md.header.msg_id + " -------------");
				//System.out.println(new String(md.encode()));
				//System.out.println("--------------------------------");

			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	}	
	
	
	public static byte[] process(byte[] data, SMPPPacket smppp, SMPPClientConnection client_conn){
		byte[] res = null;
		PDUBase[] pdu_lst = null;
		PDUBase pdu = null;
		Bind_transceiver bind_transceiver = null;
		Bind_transceiver_resp bind_transceiver_resp = null;
		
		Bind_transmitter bind_transmitter = null;
		Bind_transmitter_resp bind_transmitter_resp = null;
		
		Bind_receiver bind_receiver = null;
		Bind_receiver_resp bind_receiver_resp = null;
		
		Submit_sm submit_sm = null;
		Submit_sm_resp submit_sm_resp = null;
		
		Unbind unbind = null;
		Unbind_resp unbind_resp = null;
		
		RoutingConnectionPayload rpl = null;
		ProxyCorrelation pc = null;
		
		boolean ok_to_send = true;
		
		if(data != null){
			pdu_lst = SMPP_PDU.decode(data);
			for(int i = 0; i<pdu_lst.length; i++){
				pdu = pdu_lst[i];
				switch(pdu.type){
					/*
					case UNBIND:
						unbind = (Unbind)pdu;
						unbind_resp = new Unbind_resp();
						unbind_resp.sequence_number = unbind.sequence_number;
						unbind_resp.command_status = ErrorCodeType.ESME_ROK;
						res = unbind_resp.encode();
						break;
					*/
					case BIND_TRANSCEIVER:
						
						bind_transceiver = (Bind_transceiver)pdu;
						/*
						bind_transceiver_resp = new Bind_transceiver_resp();
						bind_transceiver_resp.command_status = ErrorCodeType.ESME_ROK;
						bind_transceiver_resp.sequence_number = bind_transceiver.sequence_number;
						bind_transceiver_resp.system_id = bind_transceiver.system_id;
						*/
						client_conn.smpp_system_id = bind_transceiver.system_id;
						client_conn.smpp_password = bind_transceiver.password;
						
						//res = bind_transceiver_resp.encode();
						break;
					case BIND_TRANSMITTER:
						bind_transmitter = (Bind_transmitter)pdu;
						/*
						bind_transmitter_resp = new Bind_transmitter_resp();
						bind_transmitter_resp.command_status = ErrorCodeType.ESME_ROK;
						bind_transmitter_resp.sequence_number = bind_transmitter.sequence_number;
						bind_transmitter_resp.system_id = bind_transmitter.system_id;
						*/
						client_conn.smpp_system_id = bind_transmitter.system_id;
						client_conn.smpp_password = bind_transmitter.password;
						
						//res = bind_transmitter_resp.encode();
						break;
					
					case SUBMIT_MULTI:
						// filter
						smppp.smpp_pdu = pdu;
						smppp.smpp_data = data;
						processFilter_sms(smppp);
						ok_to_send = false;
						// stats
						StatsManager.SMPP_STATS.SUBMIT_MULTI_COUNT++;
						break;
					case DELIVER_SM:
						// filter
						smppp.smpp_pdu = pdu;
						smppp.smpp_data = data;
						processFilter_sms(smppp);
						ok_to_send = false;
						// stats
						StatsManager.SMPP_STATS.DELIVER_SM_COUNT++;
						break;
					case SUBMT_SM:
						/*
						submit_sm = (Submit_sm)pdu;
						submit_sm_resp = new Submit_sm_resp();
						submit_sm_resp.command_status = ErrorCodeType.ESME_ROK;
						submit_sm_resp.sequence_number = submit_sm.sequence_number;
						submit_sm_resp.message_id = "KITA";
						res = submit_sm_resp.encode();
						*/
						// filter
						smppp.smpp_pdu = pdu;
						smppp.smpp_data = data;
						processFilter_sms(smppp);
						ok_to_send = false;
						// stats
						StatsManager.SMPP_STATS.SUBMIT_SM_COUNT++;
						break;
				}
				if(ok_to_send){
					pc = SMPPConnManager.correlation_get(client_conn.con_ip + ":" + client_conn.con_port);
					if(pc != null){
						rpl = RoutingManager.createPayload(RoutingConnectionType.SMPP);
						rpl.setParams(new Object[]{pdu, data});
						// send to out queue
						pc.end_point_conn.out_offer(rpl);

					}						
					
				}
			}
		}
		
		return res;
		
	}
	public static void correlation_remove(SMPPClientConnection client_conn){
		conn_map.remove(client_conn.con_ip + ":" + client_conn.con_port);
		
		
	}
	
	public static ProxyCorrelation correlation_get(String id){
		return proxy_map.get(id);
	
	}
	
	public static ProxyCorrelation correlation_add(SMPPClientConnection client_conn){
		try{
			// get end point connection params
			SMPPConnection sconn = getConnection(SGNConfigData.smpp_proxy_end_point);
			if(sconn != null){
				Socket socket = new Socket(sconn.ip, sconn.port);
				ProxyCorrelation proxy_conn = new ProxyCorrelation();
				// end point connection
				SMPPConnection conn = new SMPPConnection(socket, true);
				// create converter - proxying packets from end point to client
				SMPPProxyConverter converter = new SMPPProxyConverter(conn, client_conn);
				// attach
				conn.attachWorkerMethod(converter);
				// start
				conn.start();
				// stats
				StatsManager.SMPP_STATS.END_POINT_CONNECTION_COUNT++;
				// add routing connetion (end point connection)
				RoutingManager.add(client_conn.con_ip + ":" + client_conn.con_port, conn);
				// set connetion params
				proxy_conn.client_conn = client_conn;
				proxy_conn.end_point_conn = conn;
				// save (end point - client) correlation to map
				proxy_map.put(client_conn.con_ip + ":" + client_conn.con_port, proxy_conn);
				
				return proxy_conn;
				
			}else{
				LoggingManager.error(logger, "SMPP Proxy end point connection [" + SGNConfigData.smpp_proxy_end_point + "] not found!");
			}
		}catch(Exception e){
			LoggingManager.error(logger, "SMPP Connection Manager: Cannot create SMPP end point connection!");
		}
		return null;
	}
	public static void init_smpp_user_sync_thread(){
		smpp_user_sync_r = new Smpp_user_sync();
		smpp_user_sync_t = new Thread(smpp_user_sync_r, "SMPP_USER_SYNC");
		smpp_user_sync_t.start();
	
	}	
	public static void remove_smpp_user_correlation(String id){
		smpp_user_correlation_map.remove(id);
	}
	public static void add_smpp_user_correlation(MessageDescriptor md){
		smpp_user_correlation_map.put(md.header.msg_id, md);
	}
	public static void process_smpp_user_sync_reply(MessageDescriptor md){
		MessageDescriptor md_cor = smpp_user_correlation_map.get(md.header.msg_id);
		if(md_cor != null){
			if(md.callback != null){
				((SMPP_USER_SYNC_Callback)md.callback).set_smpp_user_sync_reply(md);
				md.callback.run();
			}
		}
		
	}
	
	
	public static void init(){
		LoggingManager.info(logger, "Starting SMPP Connection Manager...");
		proxy_map = new ConcurrentHashMap<String, ProxyCorrelation>();
		conn_map = new ConcurrentHashMap<String, SMPPConnection>();
		smpp_user_map = new ConcurrentHashMap<String, String>();
		smpp_user_correlation_map = new ConcurrentHashMap<String, MessageDescriptor>();
		LoggingManager.info(logger, "Starting SMPP Connection Monitor...");
		start_SMPP_monitor();
		
		
	}
}
