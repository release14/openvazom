package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Ussd_service_op extends OptionalParameterBase {
	public enum ValueType{
		PSSD_INDICATION(0x00),
		PSSR_INDICATION(1),
		USSR_REQUEST(2),
		USSN_REQUEST(3),
		PSSD_RESPONSE(16),
		PSSR_RESPONSE(17),
		USSR_CONFIRM(18),
		USSN_CONFIRM(19);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public ValueType value;
	public Ussd_service_op(){
		type = OptionalParameterType.USSD_SERVICE_OP;
	}

	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
