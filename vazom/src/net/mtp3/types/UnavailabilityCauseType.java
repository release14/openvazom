package net.mtp3.types;

import java.util.HashMap;

public enum UnavailabilityCauseType {
	UNKNOWN(0x00),
	UNEQUIPPED_REMOTE_USER(0x01),
	INACCESSIBLE_REMOTE_USER(0x02);
	
	private int id;
	private static final HashMap<Integer, UnavailabilityCauseType> lookup = new HashMap<Integer, UnavailabilityCauseType>();
	static{
		for(UnavailabilityCauseType td : UnavailabilityCauseType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static UnavailabilityCauseType get(int id){ return lookup.get(id); }
	private UnavailabilityCauseType(int _id){ id = _id; }	
}
