package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationHeader extends SEQUENCE{

	public SecurityParmIndex get_secParmIndex(){
		return (SecurityParmIndex)elements.get(0).data;
	}
	public SecurityParmIndex new_secParmIndex(){
		return new SecurityParmIndex();
	}

	public SequenceNum get_seqNum(){
		return (SequenceNum)elements.get(1).data;
	}
	public SequenceNum new_seqNum(){
		return new SequenceNum();
	}

	public AuthData get_ad(){
		return (AuthData)elements.get(2).data;
	}
	public AuthData new_ad(){
		return new AuthData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_secParmIndex(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_seqNum(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ad(); }});
	}
	public AuthenticationHeader(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
