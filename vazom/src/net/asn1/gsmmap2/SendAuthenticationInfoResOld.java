package net.asn1.gsmmap2;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendAuthenticationInfoResOld extends SEQUENCE{

	public SendAuthenticationInfoResOldSeq new_child(){
		return new SendAuthenticationInfoResOldSeq();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SendAuthenticationInfoResOldSeq getChild(int index){
		return (SendAuthenticationInfoResOldSeq)of_children.get(index);
	}
	public SendAuthenticationInfoResOld(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
