package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendRoutingInfoRes extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public ExtendedRoutingInfo get_extendedRoutingInfo(){
		return (ExtendedRoutingInfo)elements.get(1).data;
	}
	public ExtendedRoutingInfo new_extendedRoutingInfo(){
		return new ExtendedRoutingInfo();
	}

	public CUG_CheckInfo get_cug_CheckInfo(){
		return (CUG_CheckInfo)elements.get(2).data;
	}
	public CUG_CheckInfo new_cug_CheckInfo(){
		return new CUG_CheckInfo();
	}

	public NULL get_cugSubscriptionFlag(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_cugSubscriptionFlag(){
		return new NULL();
	}

	public SubscriberInfo get_subscriberInfo(){
		return (SubscriberInfo)elements.get(4).data;
	}
	public SubscriberInfo new_subscriberInfo(){
		return new SubscriberInfo();
	}

	public SS_List get_ss_List(){
		return (SS_List)elements.get(5).data;
	}
	public SS_List new_ss_List(){
		return new SS_List();
	}

	public Ext_BasicServiceCode get_basicService(){
		return (Ext_BasicServiceCode)elements.get(6).data;
	}
	public Ext_BasicServiceCode new_basicService(){
		return new Ext_BasicServiceCode();
	}

	public NULL get_forwardingInterrogationRequired(){
		return (NULL)elements.get(7).data;
	}
	public NULL new_forwardingInterrogationRequired(){
		return new NULL();
	}

	public ISDN_AddressString get_vmsc_Address(){
		return (ISDN_AddressString)elements.get(8).data;
	}
	public ISDN_AddressString new_vmsc_Address(){
		return new ISDN_AddressString();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(9).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NAEA_PreferredCI get_naea_PreferredCI(){
		return (NAEA_PreferredCI)elements.get(10).data;
	}
	public NAEA_PreferredCI new_naea_PreferredCI(){
		return new NAEA_PreferredCI();
	}

	public CCBS_Indicators get_ccbs_Indicators(){
		return (CCBS_Indicators)elements.get(11).data;
	}
	public CCBS_Indicators new_ccbs_Indicators(){
		return new CCBS_Indicators();
	}

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(12).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public NumberPortabilityStatus get_numberPortabilityStatus(){
		return (NumberPortabilityStatus)elements.get(13).data;
	}
	public NumberPortabilityStatus new_numberPortabilityStatus(){
		return new NumberPortabilityStatus();
	}

	public IST_AlertTimerValue get_istAlertTimer(){
		return (IST_AlertTimerValue)elements.get(14).data;
	}
	public IST_AlertTimerValue new_istAlertTimer(){
		return new IST_AlertTimerValue();
	}

	public SupportedCamelPhases get_supportedCamelPhasesInVMSC(){
		return (SupportedCamelPhases)elements.get(15).data;
	}
	public SupportedCamelPhases new_supportedCamelPhasesInVMSC(){
		return new SupportedCamelPhases();
	}

	public OfferedCamel4CSIs get_offeredCamel4CSIsInVMSC(){
		return (OfferedCamel4CSIs)elements.get(16).data;
	}
	public OfferedCamel4CSIs new_offeredCamel4CSIsInVMSC(){
		return new OfferedCamel4CSIs();
	}

	public RoutingInfo get_routingInfo2(){
		return (RoutingInfo)elements.get(17).data;
	}
	public RoutingInfo new_routingInfo2(){
		return new RoutingInfo();
	}

	public SS_List get_ss_List2(){
		return (SS_List)elements.get(18).data;
	}
	public SS_List new_ss_List2(){
		return new SS_List();
	}

	public Ext_BasicServiceCode get_basicService2(){
		return (Ext_BasicServiceCode)elements.get(19).data;
	}
	public Ext_BasicServiceCode new_basicService2(){
		return new Ext_BasicServiceCode();
	}

	public AllowedServices get_allowedServices(){
		return (AllowedServices)elements.get(20).data;
	}
	public AllowedServices new_allowedServices(){
		return new AllowedServices();
	}

	public UnavailabilityCause get_unavailabilityCause(){
		return (UnavailabilityCause)elements.get(21).data;
	}
	public UnavailabilityCause new_unavailabilityCause(){
		return new UnavailabilityCause();
	}

	public NULL get_releaseResourcesSupported(){
		return (NULL)elements.get(22).data;
	}
	public NULL new_releaseResourcesSupported(){
		return new NULL();
	}

	public ExternalSignalInfo get_gsm_BearerCapability(){
		return (ExternalSignalInfo)elements.get(23).data;
	}
	public ExternalSignalInfo new_gsm_BearerCapability(){
		return new ExternalSignalInfo();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extendedRoutingInfo(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cug_CheckInfo(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cugSubscriptionFlag(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_subscriberInfo(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ss_List(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_basicService(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_forwardingInterrogationRequired(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_vmsc_Address(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_naea_PreferredCI(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ccbs_Indicators(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_numberPortabilityStatus(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_istAlertTimer(); }});
		elements.add(new ElementDescriptor(15, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_supportedCamelPhasesInVMSC(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_offeredCamel4CSIsInVMSC(); }});
		elements.add(new ElementDescriptor(17, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_routingInfo2(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ss_List2(); }});
		elements.add(new ElementDescriptor(19, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_basicService2(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_allowedServices(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_unavailabilityCause(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_releaseResourcesSupported(); }});
		elements.add(new ElementDescriptor(23, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_gsm_BearerCapability(); }});
	}
	public SendRoutingInfoRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
