package net.m3ua.parameters;

import java.util.Arrays;

import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;

public class ProtocolData extends M3UAParameter {
	public ServiceIndicatorType serviceIndicator;
	public int networkIndicator;
	public int messagePriority;
	public int destinationPointCode;
	public int originatingPointCode;
	public int signallingLinkSelectionCode;
	public byte[] userProtocolData;
	
	public ProtocolData(){
		type = M3UAParameterType.PROTOCOL_DATA;
	}
	public void setOPC(int value){ originatingPointCode = value; }
	public void setDPC(int value){ destinationPointCode = value; }
	//public void setSI(int value){ serviceIndicator = ServiceIndicatorType.get(value); }
	public void setSI(ServiceIndicatorType value){ serviceIndicator = value; }
	public void setNI(int value){ networkIndicator = value; }
	public void setMP(int value){ messagePriority = value; }
	public void setSLS(int value){ signallingLinkSelectionCode = value; }
	public void setUserProtocolData(byte[] udata){ userProtocolData = udata; }
	
	public byte[] encode(){
		int pos = 0;
		byte[] res = new byte[userProtocolData.length + 16];
		res[pos++] = (byte)(type.getId() >> 8);
		res[pos++] = (byte)(type.getId() & 0xFF);
		res[pos++] = (byte)((userProtocolData.length + 16) >> 8);
		res[pos++] = (byte)(userProtocolData.length + 16);
		// OPC
		res[pos++] = (byte)(originatingPointCode >> 24);
		res[pos++] = (byte)(originatingPointCode >> 16);
		res[pos++] = (byte)(originatingPointCode >> 8);
		res[pos++] = (byte)(originatingPointCode);
		// DPC
		res[pos++] = (byte)(destinationPointCode >> 24);
		res[pos++] = (byte)(destinationPointCode >> 16);
		res[pos++] = (byte)(destinationPointCode >> 8);
		res[pos++] = (byte)(destinationPointCode);
		// SI
		res[pos++] = (byte)(serviceIndicator.getId());
		// NI
		res[pos++] = (byte)(networkIndicator);
		// MP
		res[pos++] = (byte)(messagePriority);
		// SLS
		res[pos++] = (byte)(signallingLinkSelectionCode);
		if(userProtocolData != null) for(int i = 0; i<userProtocolData.length; i++) res[pos++] = userProtocolData[i];
		
		//System.out.println("AAAAAAAAAAAAA: " + res.length);
		return res;
	}	
	public void init(byte[] data) {
		value = data;
		originatingPointCode = (data[byte_pos] << 24) + (data[byte_pos + 1] << 16) + (data[byte_pos + 2] << 8) + (data[byte_pos + 3] & 0xFF);
		byte_pos += 4;
		destinationPointCode = (data[byte_pos] << 24) + (data[byte_pos + 1] << 16) + (data[byte_pos + 2] << 8) + (data[byte_pos + 3] & 0xFF);
		byte_pos += 4;
		
		serviceIndicator = ServiceIndicatorType.get(data[byte_pos++] & 0xFF);
		networkIndicator = data[byte_pos++] & 0xFF;
		messagePriority = data[byte_pos++] & 0xFF;
		signallingLinkSelectionCode = data[byte_pos++] & 0xFF;
		
		userProtocolData = Arrays.copyOfRange(data, byte_pos, data.length);
		
		
		
	}



}
