package net.smsfs.list.lists;

import net.smsfs.list.ListBase;
import net.smsfs.list.ListId;
import net.smsfs.list.ListType;

public class DictionaryList extends ListBase {

	public DictionaryList(int max_size) {
		super(false, max_size);
		list_type = ListType.SMSFS_LIST;
		list_type_id = ListId.DICTIONARY.toString();
	}

}
