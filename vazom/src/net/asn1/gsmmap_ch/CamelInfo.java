package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CamelInfo extends SEQUENCE{

	public SupportedCamelPhases get_supportedCamelPhases(){
		return (SupportedCamelPhases)elements.get(0).data;
	}
	public SupportedCamelPhases new_supportedCamelPhases(){
		return new SupportedCamelPhases();
	}

	public NULL get_suppress_T_CSI(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_suppress_T_CSI(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public OfferedCamel4CSIs get_offeredCamel4CSIs(){
		return (OfferedCamel4CSIs)elements.get(3).data;
	}
	public OfferedCamel4CSIs new_offeredCamel4CSIs(){
		return new OfferedCamel4CSIs();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_supportedCamelPhases(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_suppress_T_CSI(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_offeredCamel4CSIs(); }});
	}
	public CamelInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
