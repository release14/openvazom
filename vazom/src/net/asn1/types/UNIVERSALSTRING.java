package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class UNIVERSALSTRING extends ASNType {
	public UNIVERSALSTRING(){
		super();
		universalTag = ASNTag.UNIVERSALSTRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
