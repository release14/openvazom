package net.isup;

import java.util.Arrays;

import net.isup.messages.AddressComplete;
import net.isup.messages.Answer;
import net.isup.messages.ApplicationTransport;
import net.isup.messages.Blocking;
import net.isup.messages.BlockingAck;
import net.isup.messages.CallProgress;
import net.isup.messages.CircuitGroupBlocking;
import net.isup.messages.CircuitGroupBlockingAck;
import net.isup.messages.CircuitGroupQuery;
import net.isup.messages.CircuitGroupQueryResponse;
import net.isup.messages.CircuitGroupReset;
import net.isup.messages.CircuitGroupResetAck;
import net.isup.messages.CircuitGroupUnblocking;
import net.isup.messages.CircuitGroupUnblockingAck;
import net.isup.messages.Confusion;
import net.isup.messages.Connect;
import net.isup.messages.Continuity;
import net.isup.messages.ContinuityCheckRequest;
import net.isup.messages.Facility;
import net.isup.messages.FacilityAccepted;
import net.isup.messages.FacilityReject;
import net.isup.messages.FacilityRequest;
import net.isup.messages.ForwardTransfer;
import net.isup.messages.IdentificationRequest;
import net.isup.messages.IdentificationResponse;
import net.isup.messages.Information;
import net.isup.messages.InformationRequest;
import net.isup.messages.InitialAddress;
import net.isup.messages.LoopBackAck;
import net.isup.messages.LoopPrevention;
import net.isup.messages.NetworkResourceManagement;
import net.isup.messages.Overload;
import net.isup.messages.PassAlong;
import net.isup.messages.PreReleaseInformation;
import net.isup.messages.Release;
import net.isup.messages.ReleaseComplete;
import net.isup.messages.ResetCircuit;
import net.isup.messages.Resume;
import net.isup.messages.Segmentation;
import net.isup.messages.SubsequentAddress;
import net.isup.messages.SubsequentDirectoryNumber;
import net.isup.messages.Suspend;
import net.isup.messages.Unblocking;
import net.isup.messages.UnblockingAck;
import net.isup.messages.UnequippedCIC;
import net.isup.messages.UserPartTest;
import net.isup.messages.UserToUserInformation;
import net.isup.parameters.ChargedPartyIdentification;
import net.mtp3.messages.signalling.UserPartUnavailable;



public class ISUP {
	public static ISUPPacket decode(byte[] data){
		ISUPPacket res = new ISUPPacket();
		MessageType msg_type = null; 
		res.cic = ((data[0] & 0xff) << 8) + (data[1] & 0xff);
		msg_type = MessageType.get(data[2] & 0xff);
		if(msg_type != null){
			switch(msg_type){
				case ADDRESS_COMPLETE: res.message = new AddressComplete(); break;
				case INITIAL_ADDRESS: res.message = new InitialAddress(); break;
				case ANSWER: res.message = new Answer(); break;
				case APPLICATION_TRANSPORT: res.message = new ApplicationTransport(); break;
				case BLOCKING: res.message = new Blocking(); break;
				case BLOCKING_ACKNOWLEDGEMENT: res.message = new BlockingAck(); break;
				case CALL_PROGRESS: res.message = new CallProgress(); break;
				case CIRCUIT_GROUP_BLOCKING: res.message = new CircuitGroupBlocking(); break;
				case CIRCUIT_GROUP_BLOCKING_ACKNOWLEDGEMENT: res.message = new CircuitGroupBlockingAck(); break;
				case CIRCUIT_GROUP_QUERY: res.message = new CircuitGroupQuery(); break;
				case CIRCUIT_GROUP_QUERY_RESPONSE: res.message = new CircuitGroupQueryResponse(); break;
				case CIRCUIT_GROUP_RESET: res.message = new CircuitGroupReset(); break;
				case CIRCUIT_GROUP_RESET_ACKNOWLEDGEMENT: res.message = new CircuitGroupResetAck(); break;
				case CIRCUIT_GROUP_UNBLOCKING: res.message = new CircuitGroupUnblocking(); break;
				case CIRCUIT_GROUP_UNBLOCKING_ACKNOWLEDGEMENT: res.message = new CircuitGroupUnblockingAck(); break;
				case CONFUSION: res.message = new Confusion(); break;
				case CONNECT: res.message = new Connect(); break;
				case CONTINUITY: res.message = new Continuity(); break;
				case CONTINUITY_CHECK_REQUEST: res.message = new ContinuityCheckRequest(); break;
				case FACILITY: res.message = new Facility(); break;
				case FACILITY_ACCEPTED: res.message = new FacilityAccepted(); break;
				case FACILITY_REJECT: res.message = new FacilityReject(); break;
				case FACILITY_REQUEST: res.message = new FacilityRequest(); break;
				case FORWARD_TRANSFER: res.message = new ForwardTransfer(); break;
				case IDENTIFICATION_REQUEST: res.message = new IdentificationRequest(); break;
				case IDENTIFICATION_RESPONSE: res.message = new IdentificationResponse(); break;
				case INFORMATION: res.message = new Information(); break;
				case INFORMATION_REQUEST: res.message = new InformationRequest(); break;
				case LOOP_BACK_ACKNOWLEDGEMENT: res.message = new LoopBackAck(); break;
				case LOOP_PREVENTION: res.message = new LoopPrevention(); break;
				case NETWORK_RESOURCE_MANAGEMENT: res.message = new NetworkResourceManagement(); break;
				case OVERLOAD: res.message = new Overload(); break;
				case PASS_ALONG: res.message = new PassAlong(); break;
				case PRE_RELEASE_INFORMATION: res.message = new PreReleaseInformation(); break;
				case RELEASE: res.message = new Release(); break;
				case RELEASE_COMPLETE: res.message = new ReleaseComplete(); break;
				case RESET_CIRCUIT: res.message = new ResetCircuit(); break;
				case RESUME: res.message = new Resume(); break;
				case SEGMENTATION: res.message = new Segmentation(); break;
				case SUBSEQUENT_ADDRESS: res.message = new SubsequentAddress(); break;
				case SUBSEQUENT_DIRECTORY_NUMBER: res.message = new SubsequentDirectoryNumber(); break;
				case SUSPEND: res.message = new Suspend(); break;
				case UNBLOCKING: res.message = new Unblocking(); break;
				case UNBLOCKING_ACKNOWLEDGEMENT: res.message = new UnblockingAck(); break;
				case UNEQUIPPED_CIC: res.message = new UnequippedCIC(); break;
				case USER_PART_TEST: res.message = new UserPartTest(); break;
				case USER_TO_USER_INFORMATION: res.message = new UserToUserInformation();break;
		
			}
			
			if(res.message != null){
				res.message.init(Arrays.copyOfRange(data, 3, data.length));
			}
		}
		return res;
	}
}
