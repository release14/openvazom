package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudSignalsDescriptorWrapper extends SEQUENCE{

	public IndAudSignalsDescriptor get_indAudSignalsDescriptor(){
		return (IndAudSignalsDescriptor)elements.get(0).data;
	}
	public IndAudSignalsDescriptor new_indAudSignalsDescriptor(){
		return new IndAudSignalsDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_indAudSignalsDescriptor(); }});
	}
	public IndAudSignalsDescriptorWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
