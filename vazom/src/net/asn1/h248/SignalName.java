package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SignalName extends OCTET_STRING{

	public SignalName(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
