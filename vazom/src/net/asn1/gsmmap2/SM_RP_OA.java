package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SM_RP_OA extends CHOICE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public AddressString get_serviceCentreAddressOA(){
		return (AddressString)elements.get(1).data;
	}
	public AddressString new_serviceCentreAddressOA(){
		return new AddressString();
	}

	public NULL get_noSM_RP_OA(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_noSM_RP_OA(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceCentreAddressOA(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_noSM_RP_OA(); }});
	}
	public SM_RP_OA(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
