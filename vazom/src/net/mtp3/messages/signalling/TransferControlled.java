package net.mtp3.messages.signalling;

import java.util.Arrays;

import net.utils.Utils;

public class TransferControlled extends SignallingMessageBase {
	public int destination;
	public TransferControlled(){
		super();
	}
	
	public void init(byte[] data) {
		super.init(data);
		boolean[] tmp_bits = Utils.bytes2bits(Utils.reverse(Arrays.copyOfRange(data, byte_pos, byte_pos + 1)));;
		destination = Utils.bits2int(tmp_bits, 0, 14);
	}
}
