package net.config;

import java.lang.reflect.Field;
import java.util.ArrayList;

import net.db.DBType;


public class FNConfigData {
	
	public static String[] search(String ptrn){
		String[] res = null;
		ArrayList<String> tmp = new ArrayList<String>();
		Field[] flst = FNConfigData.class.getFields();
		for(int i = 0; i<flst.length; i++) if(flst[i].getName().toUpperCase().contains(ptrn.toUpperCase())){
			tmp.add(flst[i].getName().replaceAll("_", "."));
		}
		res = new String[tmp.size()];
		for(int i = 0; i<tmp.size(); i++) res[i] = tmp.get(i);
		return res;
	}
	
	public static void set(String item_name, String item_value){
		item_name = item_name.replaceAll("\\.", "_");
		try{
			Field fld = FNConfigData.class.getField(item_name);
			String fld_type = fld.getType().getSimpleName();

			if(fld_type.equalsIgnoreCase("String")){
				fld.set(FNConfigData.class, item_value);
				
			}else if(fld_type.equalsIgnoreCase("int")){
				fld.set(FNConfigData.class, Integer.parseInt(item_value));
				
			}else if(fld_type.equalsIgnoreCase("boolean")){
				if(item_value.equalsIgnoreCase("true")){
					fld.set(FNConfigData.class, true);
				}else{
					fld.set(FNConfigData.class, false);
					
				}
				
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
	}
	public static Object get(String item_name){
		item_name = item_name.replaceAll("\\.", "_");
		try{
			Field fld = FNConfigData.class.getField(item_name);
			return fld.get(FNConfigData.class);
		}catch(Exception e){
			//e.printStackTrace();
		}
		return null;
	}
	// flood
	public static boolean flood_status;
	public static int flood_max_list_size;
	
	// vstp cmdi
	public static int vstp_cmdi_port;
	
	// hlr
	public static int hlr_timeout_check_interval;
	public static int hlr_timeout;

	
	// node
	public static String fn_id;
	public static boolean fgn_debug;
	public static int fn_global_max_queue_size;
	public static int sg_workers;
	public static int sg_reconnect_interval;
	public static boolean sg_reconnect_status;
	public static boolean smsfs_status;
	public static boolean smsfs_sms_non_intrusive_status;
	public static boolean smsfs_sms_intrusive_status;

	public static boolean smsfs_hlr_non_intrusive_status;
	public static boolean smsfs_hlr_intrusive_status;

	public static boolean smsfs_isup_non_intrusive_status;
	public static boolean smsfs_isup_intrusive_status;
	
	public static boolean dr_status;
	public static boolean dr_intrusive_sms_status;
	public static boolean dr_non_intrusive_sms_status;
	public static boolean dr_intrusive_hlr_status;
	public static boolean dr_non_intrusive_hlr_status;
	public static boolean dr_intrusive_isup_status;
	public static boolean dr_non_intrusive_isup_status;

	public static int max_total_score;
	
	public static int db_packet_size;
	public static int db_packet_sri_size;
	public static int db_packet_isup_size;
	public static DBType db_type;
	public static String db_host;
	public static int db_port;
	public static String db_name;
	public static String db_username;
	public static String db_password;
	public static int db_connection_pool_size;
	
	//public static String db_sim_host;
	//public static int db_sim_port;
	//public static String db_sim_name;
	//public static String db_sim_username;
	//public static String db_sim_password;
	
	
	public static String smsfs_filter;
	public static int smsfs_spam_max;
	public static int smsfs_quarantine_max;
	public static int smsfs_md5_max;
	public static int smsfs_goto_loop_max;

	public static boolean dr_sri_errors_status;
	public static int dr_errors_check_interval;
	public static int dr_errors_timeout;
	public static int dr_isup_timeout;

	public static boolean dr_sms_errors_status;
	public static int dr_sms_errors_check_interval;
	public static int dr_sms_errors_timeout;
	
	
	
	public static int smsfs_lists_sync_interval;
	public static int smsfs_lists_max;
	// Refilter
	public static int refilter_workers;
	public static int refilter_batch_size;
	// gpub
	public static boolean gpub_status;
	public static boolean gpub_debug;
	public static int gpub_ld_min_length;
	public static int gpub_ld_max_length;
	
	public static int cmdi_port;
	public static int ds_port;
	public static int ds_threads;
	public static int smsfs_workers;
	public static int dr_workers;
	// STATS
	public static boolean stats_monitor_status;
	public static int stats_monitor_buffer;
	
}
