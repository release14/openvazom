package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Credit;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.ReceiveSeqNum;

public class AK_DataAcknowledgement extends MessageBase {
	public Dlr destinationLocalReference;
	public ReceiveSeqNum receiveSequenceNumber;
	public Credit credit;
	
	public AK_DataAcknowledgement(){
		super();
		type = MessageType.AK_DATA_ACKNOWLEDGEMENT;
	}
	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		receiveSequenceNumber = new ReceiveSeqNum();
		receiveSequenceNumber.init(new byte[]{_data[byte_pos++]});
		
		// Fixed mandatory
		credit = new Credit();
		credit.init(new byte[]{_data[byte_pos++]});
	}

}
