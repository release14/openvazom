package net.isup.parameters.csi;

import java.util.HashMap;

public enum MaintenanceBlockingStateType {
	TRANSIENT(0x00),
	UNEQUIPPED(0x03);
	
	
	private int id;
	private static final HashMap<Integer, MaintenanceBlockingStateType> lookup = new HashMap<Integer, MaintenanceBlockingStateType>();
	static{
		for(MaintenanceBlockingStateType td :MaintenanceBlockingStateType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static MaintenanceBlockingStateType get(int id){ return lookup.get(id); }
	private MaintenanceBlockingStateType(int _id){ id = _id; }		
}
