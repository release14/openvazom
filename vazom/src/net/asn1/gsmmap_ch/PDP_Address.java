package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PDP_Address extends OCTET_STRING{

	public PDP_Address(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
