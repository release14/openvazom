package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.Slr;

public class RLC_ReleaseComplete extends MessageBase {
	public Dlr destinationLocalReference;
	public Slr sourceLocalReference;
	
	public RLC_ReleaseComplete(){
		super();
		type = MessageType.RLC_RELEASE_COMPLETE;
	}
	
	public void init(byte[] _data) {
		byte_pos++;

		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		sourceLocalReference = new Slr();
		sourceLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
	}

}
