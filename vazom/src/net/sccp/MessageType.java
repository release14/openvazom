package net.sccp;

import java.util.HashMap;

public enum MessageType {
	CR_CONNECTION_REQUEST(0x00),
	CC_CONNECTION_CONFIRM(0x02),
	CREF_CONNECTION_REFUSED(0x03),
	RLSD_RELEASED(0x04),
	RLC_RELEASE_COMPLETE(0x05),
	DT1_DATA_FORM_1(0x06),
	DT2_DATA_FORM_2(0x07),
	AK_DATA_ACKNOWLEDGEMENT(0x08),
	UDT_UNITDATA(0x09),
	UDTS_UNITDATA_SERVICE(0x0A),
	ED_EXPEDITED_DATA(0x0B),
	EA_EXPEDITED_DATA_ACKNOWLEDGMENT(0x0C),
	RSR_RESET_REQUEST(0x0D),
	RSC_RESET_CONFIRM(0x0E),
	ERR_PROTOCOL_DATA_UNIT_ERROR(0x0F),
	IT_INACTIVITY_TEST(0x10),
	XUDT_EXTENDED_UNITDATA(0x11),
	XUDTS_EXTENDED_UNIDATA_SERVICE(0x12),
	LUDT_LONG_UNIDATA(0x13),
	LUDTS_LONG_UNIDATA_SERVICE(0x14);
	
	private int id;
	private static final HashMap<Integer, MessageType> lookup = new HashMap<Integer, MessageType>();
	static{
		for(MessageType td : MessageType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static MessageType get(int id){ return lookup.get(id); }
	private MessageType(int _id){ id = _id; }
	
}
