package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class O_BcsmCamelTDPDataList extends SEQUENCE{

	public O_BcsmCamelTDPData new_child(){
		return new O_BcsmCamelTDPData();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public O_BcsmCamelTDPData getChild(int index){
		return (O_BcsmCamelTDPData)of_children.get(index);
	}
	public O_BcsmCamelTDPDataList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
