package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SignalDirection extends ENUMERATED{

	public static final int _internal = 0;
	public static final int _external = 1;
	public static final int _both = 2;
	public SignalDirection(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
