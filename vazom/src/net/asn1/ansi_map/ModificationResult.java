package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModificationResult extends CHOICE{

	public DataResult get_dataResult(){
		return (DataResult)elements.get(0).data;
	}
	public DataResult new_dataResult(){
		return new DataResult();
	}

	public ServiceDataResultList get_serviceDataResultList(){
		return (ServiceDataResultList)elements.get(1).data;
	}
	public ServiceDataResultList new_serviceDataResultList(){
		return new ServiceDataResultList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(253, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataResult(); }});
		elements.add(new ElementDescriptor(273, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceDataResultList(); }});
	}
	public ModificationResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
