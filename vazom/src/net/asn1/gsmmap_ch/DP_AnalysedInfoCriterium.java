package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DP_AnalysedInfoCriterium extends SEQUENCE{

	public ISDN_AddressString get_dialledNumber(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_dialledNumber(){
		return new ISDN_AddressString();
	}

	public ServiceKey get_serviceKey(){
		return (ServiceKey)elements.get(1).data;
	}
	public ServiceKey new_serviceKey(){
		return new ServiceKey();
	}

	public ISDN_AddressString get_gsmSCF_Address(){
		return (ISDN_AddressString)elements.get(2).data;
	}
	public ISDN_AddressString new_gsmSCF_Address(){
		return new ISDN_AddressString();
	}

	public DefaultCallHandling get_defaultCallHandling(){
		return (DefaultCallHandling)elements.get(3).data;
	}
	public DefaultCallHandling new_defaultCallHandling(){
		return new DefaultCallHandling();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(4).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_dialledNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_serviceKey(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_gsmSCF_Address(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_defaultCallHandling(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public DP_AnalysedInfoCriterium(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
