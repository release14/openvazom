package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudStreamDescriptorSequenceOf extends SEQUENCE{

	public IndAudStreamDescriptor new_child(){
		return new IndAudStreamDescriptor();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public IndAudStreamDescriptor getChild(int index){
		return (IndAudStreamDescriptor)of_children.get(index);
	}
	public IndAudStreamDescriptorSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
