package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SupportedCamelPhases extends BIT_STRING{

	public static final int _phase1 = 0;
	public static final int _phase2 = 1;
	public static final int _phase3 = 2;
	public static final int _phase4 = 3;
	public SupportedCamelPhases(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
