package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class BasicServiceCriteria extends SEQUENCE{

	public Ext_BasicServiceCode new_child(){
		return new Ext_BasicServiceCode();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public Ext_BasicServiceCode getChild(int index){
		return (Ext_BasicServiceCode)of_children.get(index);
	}
	public BasicServiceCriteria(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
