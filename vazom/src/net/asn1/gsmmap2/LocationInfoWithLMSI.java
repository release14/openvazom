package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocationInfoWithLMSI extends SEQUENCE{

	public ISDN_AddressString get_networkNode_Number(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_networkNode_Number(){
		return new ISDN_AddressString();
	}

	public LMSI get_lmsi(){
		return (LMSI)elements.get(1).data;
	}
	public LMSI new_lmsi(){
		return new LMSI();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NULL get_gprsNodeIndicator(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_gprsNodeIndicator(){
		return new NULL();
	}

	public Additional_Number get_additional_Number(){
		return (Additional_Number)elements.get(4).data;
	}
	public Additional_Number new_additional_Number(){
		return new Additional_Number();
	}

	// HACK START
	//public INTEGER get_HACK(){
	//	return (INTEGER)elements.get(5).data;
	//}
	//public INTEGER new_HACK(){
	//	return new INTEGER();
	//}
	// HACK END
	
	
	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_networkNode_Number(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_lmsi(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_gprsNodeIndicator(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additional_Number(); }});
		// HACK START
		//elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_HACK(); }});
		// HACK END
		
	}
	public LocationInfoWithLMSI(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
