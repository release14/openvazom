package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMARedirectRecord extends SEQUENCE{

	public CDMABandClass get_cdmaBandClass(){
		return (CDMABandClass)elements.get(0).data;
	}
	public CDMABandClass new_cdmaBandClass(){
		return new CDMABandClass();
	}

	public CDMAChannelNumberList get_cdmaChannelNumberList(){
		return (CDMAChannelNumberList)elements.get(1).data;
	}
	public CDMAChannelNumberList new_cdmaChannelNumberList(){
		return new CDMAChannelNumberList();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public CDMANetworkIdentification get_cdmaNetworkIdentification(){
		return (CDMANetworkIdentification)elements.get(3).data;
	}
	public CDMANetworkIdentification new_cdmaNetworkIdentification(){
		return new CDMANetworkIdentification();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(170, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaBandClass(); }});
		elements.add(new ElementDescriptor(227, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaChannelNumberList(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(232, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaNetworkIdentification(); }});
	}
	public CDMARedirectRecord(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
