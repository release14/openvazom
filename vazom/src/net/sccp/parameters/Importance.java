package net.sccp.parameters;

public class Importance extends ParamBase {
	public int value;
	
	public Importance(){
		super();
		type = ParamType.IMPORTANCE;
	}
	
	
	public void init(byte[] data) {
		value = data[0] & 0xFF;
	}

}
