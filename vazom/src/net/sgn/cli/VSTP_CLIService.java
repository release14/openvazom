package net.sgn.cli;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;

import net.cli.CLIHistoryItem;
import net.config.SGNConfigData;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class VSTP_CLIService {
	public static Logger logger=Logger.getLogger(VSTP_CLIService.class);
	public static boolean stopping;
	private static Reader reader_r;
	private static Thread reader_t;
	private static DatagramSocket socket;
	
	private static class Reader implements Runnable{
		public void run() {
			byte[] buff = new byte[8192];
			byte[] tmp_buff;
			MessageDescriptor vstp_md = null;
			DatagramPacket dp = null;
			String cmd;
			String reply = null;
			InetAddress addr;
			CLIManager cli = new CLIManager();
			ArrayList<CLIHistoryItem> his_lst = null;
			CLIHistoryItem his = null;
			Object[] cli_params;
			logger.info("Starting...");
			while(!stopping){
				try{
					dp = new DatagramPacket(buff, buff.length);
					socket.receive(dp);
					vstp_md = VSTP.decode(Arrays.copyOfRange(buff, 0, dp.getLength()));
					if(vstp_md != null){
						if(vstp_md.header.msg_type == MessageType.CLI){
							if(vstp_md.values.get(VSTPDataItemType.VSTP_CLI_CMD.getId()) != null){
								cmd = vstp_md.values.get(VSTPDataItemType.VSTP_CLI_CMD.getId());
								his_lst = cli.processLine(cmd);
								if(his_lst.size() > 0){
									his = his_lst.get(his_lst.size() - 1);
									//System.out.println(his.cmd);
									if(his.descriptor != null) {
										if(his.params != null){
											cli_params = new Object[his.params.size()];
											for(int i = 0; i<cli_params.length; i++) cli_params[i] = his.params.get(i);
											reply = his.descriptor.execute(cli_params);
										}else reply = his.descriptor.execute(null);
										// clear ansi colors
										reply = Utils.ansi_clear(reply);
										// reply
										vstp_md.header.source = LocationType.SGN;
										vstp_md.header.source_id = SGNConfigData.sgn_id;
										vstp_md.values.clear();
										vstp_md.values.put(VSTPDataItemType.VSTP_CLI_RESULT.getId(), Utils.bytes2hexstr(reply.getBytes(), ""));
										 
										tmp_buff = vstp_md.encode();
										dp = new DatagramPacket(tmp_buff, tmp_buff.length, dp.getAddress(), dp.getPort());
										socket.send(dp);
										
									}
								}

							}else logger.warn("Unknown VSTP CLI command!");
							
						}else logger.warn("Invalid VSTP packet type!");
					}
					
				}catch(Exception e){
					logger.warn("VSTP_CLIService: Invalid VSTP packet!");
					e.printStackTrace();
				}
				
			}
			logger.info("Ending...");
			
		}
		
	}
	
	
	public static void init(int port){
		logger.info("Starting VSTP CLI Manager...");
		try{
			socket = new DatagramSocket(port);
			reader_r = new Reader();
			reader_t = new Thread(reader_r, "VSTP_CLI_READER");
			reader_t.start();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
}
