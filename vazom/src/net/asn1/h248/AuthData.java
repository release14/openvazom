package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AuthData extends OCTET_STRING{

	public AuthData(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
