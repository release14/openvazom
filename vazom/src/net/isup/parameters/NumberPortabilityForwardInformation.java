package net.isup.parameters;

import net.isup.ParameterType;

public class NumberPortabilityForwardInformation extends ParameterBase {
	public NumberPortabilityForwardInformation(){
		type = ParameterType.NUMBER_PORTABILITY_FORWARD_INFORMATION;
	}
}
