package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReportingState extends ENUMERATED{

	public static final int _stopMonitoring = 0;
	public static final int _startMonitoring = 1;
	public ReportingState(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
