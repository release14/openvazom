package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReleaseCause extends ENUMERATED{

	public static final int _unspecified = 0;
	public static final int _calling_Party = 1;
	public static final int _called_Party = 2;
	public static final int _commanded_Disconnect = 3;
	public ReleaseCause(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
