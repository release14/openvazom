package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FeatureRequestRes extends SET{

	public FeatureResult get_featureResult(){
		return (FeatureResult)elements.get(0).data;
	}
	public FeatureResult new_featureResult(){
		return new FeatureResult();
	}

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(1).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public ActionCode get_actionCode(){
		return (ActionCode)elements.get(2).data;
	}
	public ActionCode new_actionCode(){
		return new ActionCode();
	}

	public AnnouncementList get_announcementList(){
		return (AnnouncementList)elements.get(3).data;
	}
	public AnnouncementList new_announcementList(){
		return new AnnouncementList();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(4).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(5).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(6).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(7).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public ConferenceCallingIndicator get_conferenceCallingIndicator(){
		return (ConferenceCallingIndicator)elements.get(8).data;
	}
	public ConferenceCallingIndicator new_conferenceCallingIndicator(){
		return new ConferenceCallingIndicator();
	}

	public Digits get_digits(){
		return (Digits)elements.get(9).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public Digits get_digits_Destination(){
		return (Digits)elements.get(10).data;
	}
	public Digits new_digits_Destination(){
		return new Digits();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(11).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DisplayText2 get_displayText2(){
		return (DisplayText2)elements.get(12).data;
	}
	public DisplayText2 new_displayText2(){
		return new DisplayText2();
	}

	public DMH_AccountCodeDigits get_dmh_AccountCodeDigits(){
		return (DMH_AccountCodeDigits)elements.get(13).data;
	}
	public DMH_AccountCodeDigits new_dmh_AccountCodeDigits(){
		return new DMH_AccountCodeDigits();
	}

	public DMH_AlternateBillingDigits get_dmh_AlternateBillingDigits(){
		return (DMH_AlternateBillingDigits)elements.get(14).data;
	}
	public DMH_AlternateBillingDigits new_dmh_AlternateBillingDigits(){
		return new DMH_AlternateBillingDigits();
	}

	public DMH_BillingDigits get_dmh_BillingDigits(){
		return (DMH_BillingDigits)elements.get(15).data;
	}
	public DMH_BillingDigits new_dmh_BillingDigits(){
		return new DMH_BillingDigits();
	}

	public DMH_RedirectionIndicator get_dmh_RedirectionIndicator(){
		return (DMH_RedirectionIndicator)elements.get(16).data;
	}
	public DMH_RedirectionIndicator new_dmh_RedirectionIndicator(){
		return new DMH_RedirectionIndicator();
	}

	public GroupInformation get_groupInformation(){
		return (GroupInformation)elements.get(17).data;
	}
	public GroupInformation new_groupInformation(){
		return new GroupInformation();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(18).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public NoAnswerTime get_noAnswerTime(){
		return (NoAnswerTime)elements.get(19).data;
	}
	public NoAnswerTime new_noAnswerTime(){
		return new NoAnswerTime();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(20).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PACAIndicator get_pACAIndicator(){
		return (PACAIndicator)elements.get(21).data;
	}
	public PACAIndicator new_pACAIndicator(){
		return new PACAIndicator();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(22).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(23).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(24).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingNumberString get_redirectingNumberString(){
		return (RedirectingNumberString)elements.get(25).data;
	}
	public RedirectingNumberString new_redirectingNumberString(){
		return new RedirectingNumberString();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(26).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public ResumePIC get_resumePIC(){
		return (ResumePIC)elements.get(27).data;
	}
	public ResumePIC new_resumePIC(){
		return new ResumePIC();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(28).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public TerminationList get_terminationList(){
		return (TerminationList)elements.get(29).data;
	}
	public TerminationList new_terminationList(){
		return new TerminationList();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(30).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public TriggerAddressList get_triggerAddressList(){
		return (TriggerAddressList)elements.get(31).data;
	}
	public TriggerAddressList new_triggerAddressList(){
		return new TriggerAddressList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_featureResult(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(128, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_actionCode(); }});
		elements.add(new ElementDescriptor(130, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_announcementList(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(137, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_conferenceCallingIndicator(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits_Destination(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(299, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText2(); }});
		elements.add(new ElementDescriptor(140, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AccountCodeDigits(); }});
		elements.add(new ElementDescriptor(141, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AlternateBillingDigits(); }});
		elements.add(new ElementDescriptor(142, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_BillingDigits(); }});
		elements.add(new ElementDescriptor(88, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_RedirectionIndicator(); }});
		elements.add(new ElementDescriptor(163, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_groupInformation(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(96, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_noAnswerTime(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(146, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pACAIndicator(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(101, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberString(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(266, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_resumePIC(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(120, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationList(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(276, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerAddressList(); }});
	}
	public FeatureRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
