package net.isup.parameters.cause;

import java.util.HashMap;


public enum CodingStandardType {

	ITU(0x00),
	ISO_IEC(0x20),
	NATIONAL(0x40),
	LOCATION_SPECIFIC(0x60);
	
	
	private int id;
	private static final HashMap<Integer, CodingStandardType> lookup = new HashMap<Integer, CodingStandardType>();
	static{
		for(CodingStandardType td :CodingStandardType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CodingStandardType get(int id){ return lookup.get(id); }
	private CodingStandardType(int _id){ id = _id; }		
}
