package net.isup.parameters;

import net.isup.ParameterType;

public class CallingGeodeticLocation extends ParameterBase {
	public CallingGeodeticLocation(){
		type = ParameterType.CALLING_GEODETIC_LOCATION;
	}
}
