package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InformationDirectiveRes extends SET{

	public AlertResult get_alertResult(){
		return (AlertResult)elements.get(0).data;
	}
	public AlertResult new_alertResult(){
		return new AlertResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(129, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertResult(); }});
	}
	public InformationDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
