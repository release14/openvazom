package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SubscriberInfo extends SEQUENCE{

	public LocationInformation get_locationInformation(){
		return (LocationInformation)elements.get(0).data;
	}
	public LocationInformation new_locationInformation(){
		return new LocationInformation();
	}

	public SubscriberState get_subscriberState(){
		return (SubscriberState)elements.get(1).data;
	}
	public SubscriberState new_subscriberState(){
		return new SubscriberState();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public LocationInformationGPRS get_locationInformationGPRS(){
		return (LocationInformationGPRS)elements.get(3).data;
	}
	public LocationInformationGPRS new_locationInformationGPRS(){
		return new LocationInformationGPRS();
	}

	public PS_SubscriberState get_ps_SubscriberState(){
		return (PS_SubscriberState)elements.get(4).data;
	}
	public PS_SubscriberState new_ps_SubscriberState(){
		return new PS_SubscriberState();
	}

	public IMEI get_imei(){
		return (IMEI)elements.get(5).data;
	}
	public IMEI new_imei(){
		return new IMEI();
	}

	public MS_Classmark2 get_ms_Classmark2(){
		return (MS_Classmark2)elements.get(6).data;
	}
	public MS_Classmark2 new_ms_Classmark2(){
		return new MS_Classmark2();
	}

	public GPRSMSClass get_gprs_MS_Class(){
		return (GPRSMSClass)elements.get(7).data;
	}
	public GPRSMSClass new_gprs_MS_Class(){
		return new GPRSMSClass();
	}

	public MNPInfoRes get_mnpInfoRes(){
		return (MNPInfoRes)elements.get(8).data;
	}
	public MNPInfoRes new_mnpInfoRes(){
		return new MNPInfoRes();
	}

	public IMS_VoiceOverPS_SessionsInd get_imsVoiceOverPS_SessionsIndication(){
		return (IMS_VoiceOverPS_SessionsInd)elements.get(9).data;
	}
	public IMS_VoiceOverPS_SessionsInd new_imsVoiceOverPS_SessionsIndication(){
		return new IMS_VoiceOverPS_SessionsInd();
	}

	public Time get_lastUE_ActivityTime(){
		return (Time)elements.get(10).data;
	}
	public Time new_lastUE_ActivityTime(){
		return new Time();
	}

	public Used_RAT_Type get_lastRAT_Type(){
		return (Used_RAT_Type)elements.get(11).data;
	}
	public Used_RAT_Type new_lastRAT_Type(){
		return new Used_RAT_Type();
	}

	public PS_SubscriberState get_eps_SubscriberState(){
		return (PS_SubscriberState)elements.get(12).data;
	}
	public PS_SubscriberState new_eps_SubscriberState(){
		return new PS_SubscriberState();
	}

	public LocationInformationEPS get_locationInformationEPS(){
		return (LocationInformationEPS)elements.get(13).data;
	}
	public LocationInformationEPS new_locationInformationEPS(){
		return new LocationInformationEPS();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationInformation(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_subscriberState(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_locationInformationGPRS(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ps_SubscriberState(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_imei(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ms_Classmark2(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_gprs_MS_Class(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_mnpInfoRes(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_imsVoiceOverPS_SessionsIndication(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_lastUE_ActivityTime(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_lastRAT_Type(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_eps_SubscriberState(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_locationInformationEPS(); }});
	}
	public SubscriberInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
