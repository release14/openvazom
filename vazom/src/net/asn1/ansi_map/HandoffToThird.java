package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffToThird extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(2).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public InterSwitchCount get_interSwitchCount(){
		return (InterSwitchCount)elements.get(3).data;
	}
	public InterSwitchCount new_interSwitchCount(){
		return new InterSwitchCount();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(4).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(5).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(6).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public BaseStationManufacturerCode get_baseStationManufacturerCode(){
		return (BaseStationManufacturerCode)elements.get(7).data;
	}
	public BaseStationManufacturerCode new_baseStationManufacturerCode(){
		return new BaseStationManufacturerCode();
	}

	public StationClassMark get_stationClassMark(){
		return (StationClassMark)elements.get(8).data;
	}
	public StationClassMark new_stationClassMark(){
		return new StationClassMark();
	}

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(9).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public CDMABandClassList get_cdmaBandClassList(){
		return (CDMABandClassList)elements.get(10).data;
	}
	public CDMABandClassList new_cdmaBandClassList(){
		return new CDMABandClassList();
	}

	public CDMACallMode get_cdmaCallMode(){
		return (CDMACallMode)elements.get(11).data;
	}
	public CDMACallMode new_cdmaCallMode(){
		return new CDMACallMode();
	}

	public CDMAChannelData get_cdmaChannelData(){
		return (CDMAChannelData)elements.get(12).data;
	}
	public CDMAChannelData new_cdmaChannelData(){
		return new CDMAChannelData();
	}

	public CDMAMobileProtocolRevision get_cdmaMobileProtocolRevision(){
		return (CDMAMobileProtocolRevision)elements.get(13).data;
	}
	public CDMAMobileProtocolRevision new_cdmaMobileProtocolRevision(){
		return new CDMAMobileProtocolRevision();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(14).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(15).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(16).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public CDMAServingOneWayDelay get_cdmaServingOneWayDelay(){
		return (CDMAServingOneWayDelay)elements.get(17).data;
	}
	public CDMAServingOneWayDelay new_cdmaServingOneWayDelay(){
		return new CDMAServingOneWayDelay();
	}

	public CDMAStationClassMark get_cdmaStationClassMark(){
		return (CDMAStationClassMark)elements.get(18).data;
	}
	public CDMAStationClassMark new_cdmaStationClassMark(){
		return new CDMAStationClassMark();
	}

	public CDMAStationClassMark2 get_cdmaStationClassMark2(){
		return (CDMAStationClassMark2)elements.get(19).data;
	}
	public CDMAStationClassMark2 new_cdmaStationClassMark2(){
		return new CDMAStationClassMark2();
	}

	public CDMATargetMAHOList get_cdmaTargetMAHOList(){
		return (CDMATargetMAHOList)elements.get(20).data;
	}
	public CDMATargetMAHOList new_cdmaTargetMAHOList(){
		return new CDMATargetMAHOList();
	}

	public CDMATargetMeasurementList get_cdmaTargetMeasurementList(){
		return (CDMATargetMeasurementList)elements.get(21).data;
	}
	public CDMATargetMeasurementList new_cdmaTargetMeasurementList(){
		return new CDMATargetMeasurementList();
	}

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(22).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(23).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public HandoffReason get_handoffReason(){
		return (HandoffReason)elements.get(24).data;
	}
	public HandoffReason new_handoffReason(){
		return new HandoffReason();
	}

	public HandoffState get_handoffState(){
		return (HandoffState)elements.get(25).data;
	}
	public HandoffState new_handoffState(){
		return new HandoffState();
	}

	public MSLocation get_msLocation(){
		return (MSLocation)elements.get(26).data;
	}
	public MSLocation new_msLocation(){
		return new MSLocation();
	}

	public NAMPSCallMode get_nampsCallMode(){
		return (NAMPSCallMode)elements.get(27).data;
	}
	public NAMPSCallMode new_nampsCallMode(){
		return new NAMPSCallMode();
	}

	public NAMPSChannelData get_nampsChannelData(){
		return (NAMPSChannelData)elements.get(28).data;
	}
	public NAMPSChannelData new_nampsChannelData(){
		return new NAMPSChannelData();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(29).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public TDMABurstIndicator get_tdmaBurstIndicator(){
		return (TDMABurstIndicator)elements.get(30).data;
	}
	public TDMABurstIndicator new_tdmaBurstIndicator(){
		return new TDMABurstIndicator();
	}

	public TDMACallMode get_tdmaCallMode(){
		return (TDMACallMode)elements.get(31).data;
	}
	public TDMACallMode new_tdmaCallMode(){
		return new TDMACallMode();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(32).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(33).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TDMATerminalCapability get_tdmaTerminalCapability(){
		return (TDMATerminalCapability)elements.get(34).data;
	}
	public TDMATerminalCapability new_tdmaTerminalCapability(){
		return new TDMATerminalCapability();
	}

	public TDMAVoiceCoder get_tdmaVoiceCoder(){
		return (TDMAVoiceCoder)elements.get(35).data;
	}
	public TDMAVoiceCoder new_tdmaVoiceCoder(){
		return new TDMAVoiceCoder();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(36).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(37).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interSwitchCount(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(197, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_baseStationManufacturerCode(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_stationClassMark(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(172, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaBandClassList(); }});
		elements.add(new ElementDescriptor(62, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaCallMode(); }});
		elements.add(new ElementDescriptor(63, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaChannelData(); }});
		elements.add(new ElementDescriptor(66, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileProtocolRevision(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(60, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServingOneWayDelay(); }});
		elements.add(new ElementDescriptor(59, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark(); }});
		elements.add(new ElementDescriptor(177, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark2(); }});
		elements.add(new ElementDescriptor(136, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMAHOList(); }});
		elements.add(new ElementDescriptor(134, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMeasurementList(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_handoffReason(); }});
		elements.add(new ElementDescriptor(164, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_handoffState(); }});
		elements.add(new ElementDescriptor(70, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msLocation(); }});
		elements.add(new ElementDescriptor(165, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsCallMode(); }});
		elements.add(new ElementDescriptor(74, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsChannelData(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(31, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBurstIndicator(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaCallMode(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(179, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaTerminalCapability(); }});
		elements.add(new ElementDescriptor(180, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaVoiceCoder(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
	}
	public HandoffToThird(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
