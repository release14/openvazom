package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectStatus extends ParameterBase {
	public RedirectStatus(){
		type = ParameterType.REDIRECT_STATUS;
	}
}
