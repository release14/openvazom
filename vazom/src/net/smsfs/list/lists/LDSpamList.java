package net.smsfs.list.lists;

import net.config.FNConfigData;
import net.gpu.cuda.CudaManager;
import net.gpu2.GPUBalancer;
import net.smsfs.list.ListBase;
import net.smsfs.list.ListId;
import net.smsfs.list.ListType;

public class LDSpamList extends ListBase {

	public LDSpamList(int max_size) {
		super(true, max_size);
		list_type = ListType.SMSFS_LIST;
		list_type_id = ListId.LD_SPAM.toString();

	}
	
	public void on_add(byte[] data){
		if(data != null){
			if(FNConfigData.gpub_status){
				GPUBalancer.update(CudaManager.LD_SPAM, new String(data));
			}
			
		}
		
	}
	public void on_delete(byte[] data){
		if(data != null){
			if(FNConfigData.gpub_status){
				GPUBalancer.remove(CudaManager.LD_SPAM, data);
			}
			
		}
	}

}
