package net.isup.parameters.cgs;

import java.util.HashMap;


public enum CircuitGroupSupervisionType {
	MAINTENANCE(0x00),
	HARDWARE_FAILURE(0x01);
	
	
	private int id;
	private static final HashMap<Integer, CircuitGroupSupervisionType> lookup = new HashMap<Integer, CircuitGroupSupervisionType>();
	static{
		for(CircuitGroupSupervisionType td :CircuitGroupSupervisionType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CircuitGroupSupervisionType get(int id){ return lookup.get(id); }
	private CircuitGroupSupervisionType(int _id){ id = _id; }		
}
