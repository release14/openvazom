package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class User_message_reference extends OptionalParameterBase {

	public User_message_reference(){
		type = OptionalParameterType.USER_MESSAGE_REFERENCE;
	}
	public int value;
	
	public void init(byte[] data) {
		value = ((data[0] << 8) & 0xff) + (data[1] & 0xff);

	}

}
