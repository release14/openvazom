package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NotReachableReason extends ENUMERATED{

	public static final int _msPurged = 0;
	public static final int _imsiDetached = 1;
	public static final int _restrictedArea = 2;
	public static final int _notRegistered = 3;
	public NotReachableReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
