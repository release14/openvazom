package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TDMA_MAHO_CELLID extends OCTET_STRING{

	public TDMA_MAHO_CELLID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
