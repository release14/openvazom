package net.gpu;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionDescriptor {
	public boolean available = true;
	public boolean busy = false;
	public Socket socket;
	public BufferedReader in;
	public PrintWriter out;
	public ConnectionType type;
	public NodeDescriptor node;

	public ConnectionDescriptor(ConnectionType _type){
		type = _type;
	}
	
}
