package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InvokeData extends SEQUENCE{

	public HandoffMeasurementRequest get_handoffMeasurementRequest(){
		return (HandoffMeasurementRequest)elements.get(0).data;
	}
	public HandoffMeasurementRequest new_handoffMeasurementRequest(){
		return new HandoffMeasurementRequest();
	}

	public FacilitiesDirective get_facilitiesDirective(){
		return (FacilitiesDirective)elements.get(1).data;
	}
	public FacilitiesDirective new_facilitiesDirective(){
		return new FacilitiesDirective();
	}

	public HandoffBack get_handoffBack(){
		return (HandoffBack)elements.get(2).data;
	}
	public HandoffBack new_handoffBack(){
		return new HandoffBack();
	}

	public FacilitiesRelease get_facilitiesRelease(){
		return (FacilitiesRelease)elements.get(3).data;
	}
	public FacilitiesRelease new_facilitiesRelease(){
		return new FacilitiesRelease();
	}

	public QualificationRequest get_qualificationRequest(){
		return (QualificationRequest)elements.get(4).data;
	}
	public QualificationRequest new_qualificationRequest(){
		return new QualificationRequest();
	}

	public QualificationDirective get_qualificationDirective(){
		return (QualificationDirective)elements.get(5).data;
	}
	public QualificationDirective new_qualificationDirective(){
		return new QualificationDirective();
	}

	public Blocking get_blocking(){
		return (Blocking)elements.get(6).data;
	}
	public Blocking new_blocking(){
		return new Blocking();
	}

	public Unblocking get_unblocking(){
		return (Unblocking)elements.get(7).data;
	}
	public Unblocking new_unblocking(){
		return new Unblocking();
	}

	public ResetCircuit get_resetCircuit(){
		return (ResetCircuit)elements.get(8).data;
	}
	public ResetCircuit new_resetCircuit(){
		return new ResetCircuit();
	}

	public TrunkTest get_trunkTest(){
		return (TrunkTest)elements.get(9).data;
	}
	public TrunkTest new_trunkTest(){
		return new TrunkTest();
	}

	public TrunkTestDisconnect get_trunkTestDisconnect(){
		return (TrunkTestDisconnect)elements.get(10).data;
	}
	public TrunkTestDisconnect new_trunkTestDisconnect(){
		return new TrunkTestDisconnect();
	}

	public RegistrationNotification get_registrationNotification(){
		return (RegistrationNotification)elements.get(11).data;
	}
	public RegistrationNotification new_registrationNotification(){
		return new RegistrationNotification();
	}

	public RegistrationCancellation get_registrationCancellation(){
		return (RegistrationCancellation)elements.get(12).data;
	}
	public RegistrationCancellation new_registrationCancellation(){
		return new RegistrationCancellation();
	}

	public LocationRequest get_locationRequest(){
		return (LocationRequest)elements.get(13).data;
	}
	public LocationRequest new_locationRequest(){
		return new LocationRequest();
	}

	public RoutingRequest get_routingRequest(){
		return (RoutingRequest)elements.get(14).data;
	}
	public RoutingRequest new_routingRequest(){
		return new RoutingRequest();
	}

	public FeatureRequest get_featureRequest(){
		return (FeatureRequest)elements.get(15).data;
	}
	public FeatureRequest new_featureRequest(){
		return new FeatureRequest();
	}

	public UnreliableRoamerDataDirective get_unreliableRoamerDataDirective(){
		return (UnreliableRoamerDataDirective)elements.get(16).data;
	}
	public UnreliableRoamerDataDirective new_unreliableRoamerDataDirective(){
		return new UnreliableRoamerDataDirective();
	}

	public MSInactive get_mSInactive(){
		return (MSInactive)elements.get(17).data;
	}
	public MSInactive new_mSInactive(){
		return new MSInactive();
	}

	public TransferToNumberRequest get_transferToNumberRequest(){
		return (TransferToNumberRequest)elements.get(18).data;
	}
	public TransferToNumberRequest new_transferToNumberRequest(){
		return new TransferToNumberRequest();
	}

	public RedirectionRequest get_redirectionRequest(){
		return (RedirectionRequest)elements.get(19).data;
	}
	public RedirectionRequest new_redirectionRequest(){
		return new RedirectionRequest();
	}

	public HandoffToThird get_handoffToThird(){
		return (HandoffToThird)elements.get(20).data;
	}
	public HandoffToThird new_handoffToThird(){
		return new HandoffToThird();
	}

	public FlashRequest get_flashRequest(){
		return (FlashRequest)elements.get(21).data;
	}
	public FlashRequest new_flashRequest(){
		return new FlashRequest();
	}

	public AuthenticationDirective get_authenticationDirective(){
		return (AuthenticationDirective)elements.get(22).data;
	}
	public AuthenticationDirective new_authenticationDirective(){
		return new AuthenticationDirective();
	}

	public AuthenticationRequest get_authenticationRequest(){
		return (AuthenticationRequest)elements.get(23).data;
	}
	public AuthenticationRequest new_authenticationRequest(){
		return new AuthenticationRequest();
	}

	public BaseStationChallenge get_baseStationChallenge(){
		return (BaseStationChallenge)elements.get(24).data;
	}
	public BaseStationChallenge new_baseStationChallenge(){
		return new BaseStationChallenge();
	}

	public AuthenticationFailureReport get_authenticationFailureReport(){
		return (AuthenticationFailureReport)elements.get(25).data;
	}
	public AuthenticationFailureReport new_authenticationFailureReport(){
		return new AuthenticationFailureReport();
	}

	public CountRequest get_countRequest(){
		return (CountRequest)elements.get(26).data;
	}
	public CountRequest new_countRequest(){
		return new CountRequest();
	}

	public InterSystemPage get_interSystemPage(){
		return (InterSystemPage)elements.get(27).data;
	}
	public InterSystemPage new_interSystemPage(){
		return new InterSystemPage();
	}

	public UnsolicitedResponse get_unsolicitedResponse(){
		return (UnsolicitedResponse)elements.get(28).data;
	}
	public UnsolicitedResponse new_unsolicitedResponse(){
		return new UnsolicitedResponse();
	}

	public BulkDeregistration get_bulkDeregistration(){
		return (BulkDeregistration)elements.get(29).data;
	}
	public BulkDeregistration new_bulkDeregistration(){
		return new BulkDeregistration();
	}

	public HandoffMeasurementRequest2 get_handoffMeasurementRequest2(){
		return (HandoffMeasurementRequest2)elements.get(30).data;
	}
	public HandoffMeasurementRequest2 new_handoffMeasurementRequest2(){
		return new HandoffMeasurementRequest2();
	}

	public FacilitiesDirective2 get_facilitiesDirective2(){
		return (FacilitiesDirective2)elements.get(31).data;
	}
	public FacilitiesDirective2 new_facilitiesDirective2(){
		return new FacilitiesDirective2();
	}

	public HandoffBack2 get_handoffBack2(){
		return (HandoffBack2)elements.get(32).data;
	}
	public HandoffBack2 new_handoffBack2(){
		return new HandoffBack2();
	}

	public HandoffToThird2 get_handoffToThird2(){
		return (HandoffToThird2)elements.get(33).data;
	}
	public HandoffToThird2 new_handoffToThird2(){
		return new HandoffToThird2();
	}

	public AuthenticationDirectiveForward get_authenticationDirectiveForward(){
		return (AuthenticationDirectiveForward)elements.get(34).data;
	}
	public AuthenticationDirectiveForward new_authenticationDirectiveForward(){
		return new AuthenticationDirectiveForward();
	}

	public AuthenticationStatusReport get_authenticationStatusReport(){
		return (AuthenticationStatusReport)elements.get(35).data;
	}
	public AuthenticationStatusReport new_authenticationStatusReport(){
		return new AuthenticationStatusReport();
	}

	public InformationDirective get_informationDirective(){
		return (InformationDirective)elements.get(36).data;
	}
	public InformationDirective new_informationDirective(){
		return new InformationDirective();
	}

	public InformationForward get_informationForward(){
		return (InformationForward)elements.get(37).data;
	}
	public InformationForward new_informationForward(){
		return new InformationForward();
	}

	public InterSystemAnswer get_interSystemAnswer(){
		return (InterSystemAnswer)elements.get(38).data;
	}
	public InterSystemAnswer new_interSystemAnswer(){
		return new InterSystemAnswer();
	}

	public InterSystemPage2 get_interSystemPage2(){
		return (InterSystemPage2)elements.get(39).data;
	}
	public InterSystemPage2 new_interSystemPage2(){
		return new InterSystemPage2();
	}

	public InterSystemSetup get_interSystemSetup(){
		return (InterSystemSetup)elements.get(40).data;
	}
	public InterSystemSetup new_interSystemSetup(){
		return new InterSystemSetup();
	}

	public OriginationRequest get_originationRequest(){
		return (OriginationRequest)elements.get(41).data;
	}
	public OriginationRequest new_originationRequest(){
		return new OriginationRequest();
	}

	public RandomVariableRequest get_randomVariableRequest(){
		return (RandomVariableRequest)elements.get(42).data;
	}
	public RandomVariableRequest new_randomVariableRequest(){
		return new RandomVariableRequest();
	}

	public RedirectionDirective get_redirectionDirective(){
		return (RedirectionDirective)elements.get(43).data;
	}
	public RedirectionDirective new_redirectionDirective(){
		return new RedirectionDirective();
	}

	public RemoteUserInteractionDirective get_remoteUserInteractionDirective(){
		return (RemoteUserInteractionDirective)elements.get(44).data;
	}
	public RemoteUserInteractionDirective new_remoteUserInteractionDirective(){
		return new RemoteUserInteractionDirective();
	}

	public SMSDeliveryBackward get_sMSDeliveryBackward(){
		return (SMSDeliveryBackward)elements.get(45).data;
	}
	public SMSDeliveryBackward new_sMSDeliveryBackward(){
		return new SMSDeliveryBackward();
	}

	public SMSDeliveryForward get_sMSDeliveryForward(){
		return (SMSDeliveryForward)elements.get(46).data;
	}
	public SMSDeliveryForward new_sMSDeliveryForward(){
		return new SMSDeliveryForward();
	}

	public SMSDeliveryPointToPoint get_sMSDeliveryPointToPoint(){
		return (SMSDeliveryPointToPoint)elements.get(47).data;
	}
	public SMSDeliveryPointToPoint new_sMSDeliveryPointToPoint(){
		return new SMSDeliveryPointToPoint();
	}

	public SMSNotification get_sMSNotification(){
		return (SMSNotification)elements.get(48).data;
	}
	public SMSNotification new_sMSNotification(){
		return new SMSNotification();
	}

	public SMSRequest get_sMSRequest(){
		return (SMSRequest)elements.get(49).data;
	}
	public SMSRequest new_sMSRequest(){
		return new SMSRequest();
	}

	public OTASPRequest get_oTASPRequest(){
		return (OTASPRequest)elements.get(50).data;
	}
	public OTASPRequest new_oTASPRequest(){
		return new OTASPRequest();
	}

	public ChangeFacilities get_changeFacilities(){
		return (ChangeFacilities)elements.get(51).data;
	}
	public ChangeFacilities new_changeFacilities(){
		return new ChangeFacilities();
	}

	public ChangeService get_changeService(){
		return (ChangeService)elements.get(52).data;
	}
	public ChangeService new_changeService(){
		return new ChangeService();
	}

	public ParameterRequest get_parameterRequest(){
		return (ParameterRequest)elements.get(53).data;
	}
	public ParameterRequest new_parameterRequest(){
		return new ParameterRequest();
	}

	public TMSIDirective get_tMSIDirective(){
		return (TMSIDirective)elements.get(54).data;
	}
	public TMSIDirective new_tMSIDirective(){
		return new TMSIDirective();
	}

	public NumberPortabilityRequest get_numberPortabilityRequest(){
		return (NumberPortabilityRequest)elements.get(55).data;
	}
	public NumberPortabilityRequest new_numberPortabilityRequest(){
		return new NumberPortabilityRequest();
	}

	public ServiceRequest get_serviceRequest(){
		return (ServiceRequest)elements.get(56).data;
	}
	public ServiceRequest new_serviceRequest(){
		return new ServiceRequest();
	}

	public AnalyzedInformation get_analyzedInformation(){
		return (AnalyzedInformation)elements.get(57).data;
	}
	public AnalyzedInformation new_analyzedInformation(){
		return new AnalyzedInformation();
	}

	public ConnectionFailureReport get_connectionFailureReport(){
		return (ConnectionFailureReport)elements.get(58).data;
	}
	public ConnectionFailureReport new_connectionFailureReport(){
		return new ConnectionFailureReport();
	}

	public ConnectResource get_connectResource(){
		return (ConnectResource)elements.get(59).data;
	}
	public ConnectResource new_connectResource(){
		return new ConnectResource();
	}

	public FacilitySelectedAndAvailable get_facilitySelectedAndAvailable(){
		return (FacilitySelectedAndAvailable)elements.get(60).data;
	}
	public FacilitySelectedAndAvailable new_facilitySelectedAndAvailable(){
		return new FacilitySelectedAndAvailable();
	}

	public Modify get_modify(){
		return (Modify)elements.get(61).data;
	}
	public Modify new_modify(){
		return new Modify();
	}

	public Search get_search(){
		return (Search)elements.get(62).data;
	}
	public Search new_search(){
		return new Search();
	}

	public SeizeResource get_seizeResource(){
		return (SeizeResource)elements.get(63).data;
	}
	public SeizeResource new_seizeResource(){
		return new SeizeResource();
	}

	public SRFDirective get_sRFDirective(){
		return (SRFDirective)elements.get(64).data;
	}
	public SRFDirective new_sRFDirective(){
		return new SRFDirective();
	}

	public TBusy get_tBusy(){
		return (TBusy)elements.get(65).data;
	}
	public TBusy new_tBusy(){
		return new TBusy();
	}

	public TNoAnswer get_tNoAnswer(){
		return (TNoAnswer)elements.get(66).data;
	}
	public TNoAnswer new_tNoAnswer(){
		return new TNoAnswer();
	}

	public SMSDeliveryPointToPointAck get_smsDeliveryPointToPointAck(){
		return (SMSDeliveryPointToPointAck)elements.get(67).data;
	}
	public SMSDeliveryPointToPointAck new_smsDeliveryPointToPointAck(){
		return new SMSDeliveryPointToPointAck();
	}

	public MessageDirective get_messageDirective(){
		return (MessageDirective)elements.get(68).data;
	}
	public MessageDirective new_messageDirective(){
		return new MessageDirective();
	}

	public BulkDisconnection get_bulkDisconnection(){
		return (BulkDisconnection)elements.get(69).data;
	}
	public BulkDisconnection new_bulkDisconnection(){
		return new BulkDisconnection();
	}

	public CallControlDirective get_callControlDirective(){
		return (CallControlDirective)elements.get(70).data;
	}
	public CallControlDirective new_callControlDirective(){
		return new CallControlDirective();
	}

	public OAnswer get_oAnswer(){
		return (OAnswer)elements.get(71).data;
	}
	public OAnswer new_oAnswer(){
		return new OAnswer();
	}

	public ODisconnect get_oDisconnect(){
		return (ODisconnect)elements.get(72).data;
	}
	public ODisconnect new_oDisconnect(){
		return new ODisconnect();
	}

	public CallRecoveryReport get_callRecoveryReport(){
		return (CallRecoveryReport)elements.get(73).data;
	}
	public CallRecoveryReport new_callRecoveryReport(){
		return new CallRecoveryReport();
	}

	public TAnswer get_tAnswer(){
		return (TAnswer)elements.get(74).data;
	}
	public TAnswer new_tAnswer(){
		return new TAnswer();
	}

	public TDisconnect get_tDisconnect(){
		return (TDisconnect)elements.get(75).data;
	}
	public TDisconnect new_tDisconnect(){
		return new TDisconnect();
	}

	public UnreliableCallData get_unreliableCallData(){
		return (UnreliableCallData)elements.get(76).data;
	}
	public UnreliableCallData new_unreliableCallData(){
		return new UnreliableCallData();
	}

	public OCalledPartyBusy get_oCalledPartyBusy(){
		return (OCalledPartyBusy)elements.get(77).data;
	}
	public OCalledPartyBusy new_oCalledPartyBusy(){
		return new OCalledPartyBusy();
	}

	public ONoAnswer get_oNoAnswer(){
		return (ONoAnswer)elements.get(78).data;
	}
	public ONoAnswer new_oNoAnswer(){
		return new ONoAnswer();
	}

	public PositionRequest get_positionRequest(){
		return (PositionRequest)elements.get(79).data;
	}
	public PositionRequest new_positionRequest(){
		return new PositionRequest();
	}

	public PositionRequestForward get_positionRequestForward(){
		return (PositionRequestForward)elements.get(80).data;
	}
	public PositionRequestForward new_positionRequestForward(){
		return new PositionRequestForward();
	}

	public CallTerminationReport get_callTerminationReport(){
		return (CallTerminationReport)elements.get(81).data;
	}
	public CallTerminationReport new_callTerminationReport(){
		return new CallTerminationReport();
	}

	public GeoPositionRequest get_geoPositionRequest(){
		return (GeoPositionRequest)elements.get(82).data;
	}
	public GeoPositionRequest new_geoPositionRequest(){
		return new GeoPositionRequest();
	}

	public InterSystemPositionRequest get_interSystemPositionRequest(){
		return (InterSystemPositionRequest)elements.get(83).data;
	}
	public InterSystemPositionRequest new_interSystemPositionRequest(){
		return new InterSystemPositionRequest();
	}

	public InterSystemPositionRequestForward get_interSystemPositionRequestForward(){
		return (InterSystemPositionRequestForward)elements.get(84).data;
	}
	public InterSystemPositionRequestForward new_interSystemPositionRequestForward(){
		return new InterSystemPositionRequestForward();
	}

	public ACGDirective get_aCGDirective(){
		return (ACGDirective)elements.get(85).data;
	}
	public ACGDirective new_aCGDirective(){
		return new ACGDirective();
	}

	public RoamerDatabaseVerificationRequest get_roamerDatabaseVerificationRequest(){
		return (RoamerDatabaseVerificationRequest)elements.get(86).data;
	}
	public RoamerDatabaseVerificationRequest new_roamerDatabaseVerificationRequest(){
		return new RoamerDatabaseVerificationRequest();
	}

	public AddService get_addService(){
		return (AddService)elements.get(87).data;
	}
	public AddService new_addService(){
		return new AddService();
	}

	public DropService get_dropService(){
		return (DropService)elements.get(88).data;
	}
	public DropService new_dropService(){
		return new DropService();
	}

	public LCSParameterRequest get_lcsParameterRequest(){
		return (LCSParameterRequest)elements.get(89).data;
	}
	public LCSParameterRequest new_lcsParameterRequest(){
		return new LCSParameterRequest();
	}

	public CheckMEID get_checkMEID(){
		return (CheckMEID)elements.get(90).data;
	}
	public CheckMEID new_checkMEID(){
		return new CheckMEID();
	}

	public PositionEventNotification get_positionEventNotification(){
		return (PositionEventNotification)elements.get(91).data;
	}
	public PositionEventNotification new_positionEventNotification(){
		return new PositionEventNotification();
	}

	public StatusRequest get_statusRequest(){
		return (StatusRequest)elements.get(92).data;
	}
	public StatusRequest new_statusRequest(){
		return new StatusRequest();
	}

	public InterSystemSMSDeliveryPointToPoint get_interSystemSMSDeliveryPointToPoint(){
		return (InterSystemSMSDeliveryPointToPoint)elements.get(93).data;
	}
	public InterSystemSMSDeliveryPointToPoint new_interSystemSMSDeliveryPointToPoint(){
		return new InterSystemSMSDeliveryPointToPoint();
	}

	public QualificationRequest2 get_qualificationRequest2(){
		return (QualificationRequest2)elements.get(94).data;
	}
	public QualificationRequest2 new_qualificationRequest2(){
		return new QualificationRequest2();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffMeasurementRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitiesDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffBack(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitiesRelease(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_qualificationRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_qualificationDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_blocking(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_unblocking(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_resetCircuit(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_trunkTest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_trunkTestDisconnect(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_registrationNotification(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_registrationCancellation(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_locationRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_routingRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_featureRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_unreliableRoamerDataDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_mSInactive(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_transferToNumberRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_redirectionRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffToThird(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_flashRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_baseStationChallenge(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationFailureReport(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_countRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPage(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_unsolicitedResponse(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_bulkDeregistration(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffMeasurementRequest2(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitiesDirective2(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffBack2(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffToThird2(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationDirectiveForward(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationStatusReport(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_informationDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_informationForward(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemAnswer(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPage2(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemSetup(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_originationRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_randomVariableRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_redirectionDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_remoteUserInteractionDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSDeliveryBackward(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSDeliveryForward(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSDeliveryPointToPoint(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSNotification(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oTASPRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_changeFacilities(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_changeService(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_parameterRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tMSIDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_numberPortabilityRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_serviceRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_analyzedInformation(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_connectionFailureReport(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_connectResource(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitySelectedAndAvailable(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_modify(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_search(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_seizeResource(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sRFDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tBusy(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tNoAnswer(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_smsDeliveryPointToPointAck(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_messageDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_bulkDisconnection(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_callControlDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oAnswer(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oDisconnect(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_callRecoveryReport(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tAnswer(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tDisconnect(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_unreliableCallData(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oCalledPartyBusy(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oNoAnswer(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_positionRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_positionRequestForward(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_callTerminationReport(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_geoPositionRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPositionRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPositionRequestForward(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_aCGDirective(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_roamerDatabaseVerificationRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_addService(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_dropService(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_lcsParameterRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_checkMEID(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_positionEventNotification(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_statusRequest(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemSMSDeliveryPointToPoint(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_qualificationRequest2(); }});
	}
	public InvokeData(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
