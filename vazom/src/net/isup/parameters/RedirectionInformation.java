package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectionInformation extends ParameterBase {
	public RedirectionInformation(){
		type = ParameterType.REDIRECTION_INFORMATION;
	}
}
