package net.smpp.parameters;

import java.util.HashMap;

// bits 7 and 6
// XX000000
public enum GSMNetworkSpecific {
	NO_SPECIFIC_FEATURES(0x00),
	UDHI_INDICATOR(0x40),
	SET_REPLY_PATH(0x80),
	SET_UDHI_AND_REPLY_PATH(0xc0);

	private int id;
	private static final HashMap<Integer, GSMNetworkSpecific> lookup = new HashMap<Integer, GSMNetworkSpecific>();
	static{
		for(GSMNetworkSpecific td : GSMNetworkSpecific.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static GSMNetworkSpecific get(int id){ return lookup.get(id); }
	private GSMNetworkSpecific(int _id){ id = _id; }
}
