package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class OAnswer extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSID get_msid(){
		return (MSID)elements.get(3).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public TimeDateOffset get_timeDateOffset(){
		return (TimeDateOffset)elements.get(4).data;
	}
	public TimeDateOffset new_timeDateOffset(){
		return new TimeDateOffset();
	}

	public TimeOfDay get_timeOfDay(){
		return (TimeOfDay)elements.get(5).data;
	}
	public TimeOfDay new_timeOfDay(){
		return new TimeOfDay();
	}

	public TriggerType get_triggerType(){
		return (TriggerType)elements.get(6).data;
	}
	public TriggerType new_triggerType(){
		return new TriggerType();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(7).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(8).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public FeatureIndicator get_featureIndicator(){
		return (FeatureIndicator)elements.get(9).data;
	}
	public FeatureIndicator new_featureIndicator(){
		return new FeatureIndicator();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(10).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(11).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(12).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(275, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_timeDateOffset(); }});
		elements.add(new ElementDescriptor(309, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_timeOfDay(); }});
		elements.add(new ElementDescriptor(279, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_triggerType(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(306, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_featureIndicator(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemMyTypeCode(); }});
	}
	public OAnswer(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
