package net.smsfs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.config.FNConfigData;
import net.ds.DataSourceType;
import net.flood.FloodItem;
import net.flood.FloodManager;
import net.flood.GlobalMaxType;
import net.fn.FNode;
import net.gpu2.GPUBalancer;
import net.gpu2.NodeType;
import net.gpu.cuda.CudaManager;
import net.hlr.HLRManager;
import net.hlr.HLRPacket;
import net.smsfs.bindings.Bindings;
import net.smsfs.list.ListBase;
import net.smsfs.list.ListDescriptor;
import net.smsfs.list.ListId;
import net.smsfs.list.ListItemBase;
import net.smsfs.list.ListManagerV2;
import net.smsfs.list.ListType;
//import net.smsfs.text.MD5Manager;
//import net.smsfs.text.QuarantineManager;
//import net.smsfs.text.SpamManager;
//import net.smsfs.text.SpamDict;
import net.utils.Utils;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

public class Smsfs {
	private static Logger logger=Logger.getLogger(Smsfs.class);

	public static Tree getRules(Tree t, FilterType ftype){
		if(t != null){
			//System.out.println(t.getChild(0).toString());
			for(int i = 0; i<t.getChildCount(); i++){
				//System.out.println(t.getChild(i).getChild(0) toString());
				if(t.getChild(i).toString().equals(ftype.toString())) return t.getChild(i);
			}
			
		}
		return null;

	}
	private static boolean isRuleMethod(String item){
		if(	item.equalsIgnoreCase("FLOOD") || 
			item.equalsIgnoreCase("FLOOD.MAX") || 
			item.equalsIgnoreCase("FLOOD.GLOBAL") || 
			item.equalsIgnoreCase("FLOOD.GLOBAL.MAX") ||
			item.equalsIgnoreCase("FLOOD.ALL.MAX") ||
			item.equalsIgnoreCase("LIST")) return true;
		return false;
	}
	
	public static FilterDescriptor processFilter(Tree t, FilterType ft){
		FilterDescriptor res = null;
		RuleDescriptor rd;
		ModifierDescriptor md;
		if(t != null){
			res = new FilterDescriptor();
			res.filterType = ft;
			
			for(int i = 0; i<t.getChildCount(); i++){
				// Rule
				if(t.getChild(i).getText().equals("RULE_DEF")){
					rd = new RuleDescriptor();
					rd.points = Integer.parseInt(t.getChild(i).getChild(0).getChild(0).toString());
					rd.active = (t.getChild(i).getChild(1).getChild(0).toString().equals("ON") ? true: false);

					// Item 1
					// regex
					if(t.getChild(i).getChild(2).getChild(0).toString().equalsIgnoreCase("RULE_REGEX")){
						rd.evalItem1Regex = t.getChild(i).getChild(2).getChild(0).getChild(0).toString();
						rd.evalItem1 = t.getChild(i).getChild(2).getChild(0).getChild(1).toString();
						rd.item1_is_regex = true;
					// flood method
					}else if(isRuleMethod(t.getChild(i).getChild(2).getChild(0).toString())){
						rd.item1_is_method = true;
						rd.evalItem1 = t.getChild(i).getChild(2).getChild(0).toString();
						//t.getChild(i).getChild(2).getChild(0).getChild(0).toString();;
						//System.out.println(rd.evalItem1);
						rd.item_1_method_params = new ArrayList<String>();
						for(int j = 0; j<t.getChild(i).getChild(2).getChild(0).getChildCount(); j++){
							rd.item_1_method_params.add(t.getChild(i).getChild(2).getChild(0).getChild(j).toString());
							//System.out.println(rd.item_1_method_params.get(rd.item_1_method_params.size()-1));
						}
						
					// normal
					}else{
						rd.evalItem1 = t.getChild(i).getChild(2).getChild(0).toString();
					}
					// operator
					rd.evalOperator = t.getChild(i).getChild(2).getChild(1).toString();
					
					
					// Item 2
					// regex
					if(t.getChild(i).getChild(2).getChild(2).toString().equalsIgnoreCase("RULE_REGEX")){
						rd.evalItem2Regex = t.getChild(i).getChild(2).getChild(2).getChild(0).toString();
						rd.evalItem2 = t.getChild(i).getChild(2).getChild(2).getChild(1).toString();
						rd.item2_is_regex = true;
					
					// flood  method
					}else if(isRuleMethod(t.getChild(i).getChild(2).getChild(2).toString())){
						rd.item2_is_method = true;
						rd.evalItem2 = t.getChild(i).getChild(2).getChild(2).toString();
						//t.getChild(i).getChild(2).getChild(0).getChild(0).toString();;
						//System.out.println(rd.evalItem2);
						rd.item_2_method_params = new ArrayList<String>();
						for(int j = 0; j<t.getChild(i).getChild(2).getChild(2).getChildCount(); j++){
							rd.item_2_method_params.add(t.getChild(i).getChild(2).getChild(2).getChild(j).toString());
							//System.out.println(rd.item_2_method_params.get(rd.item_2_method_params.size()-1));
						}

						
						
					// normal
					}else{
						rd.evalItem2 = t.getChild(i).getChild(2).getChild(2).toString();
					}
					// actions
					rd.ruleEvalTrue = RuleAction.valueOf(t.getChild(i).getChild(3).getChild(0).toString());
					// action connection label
					//System.out.println(t.getChild(i).getChild(3).getChildCount());
					if(t.getChild(i).getChild(3).getChildCount() > 1){
						for(int j = 1; j<t.getChild(i).getChild(3).getChildCount(); j++){
							rd.ruleEvalTrue_connection.add(t.getChild(i).getChild(3).getChild(1).toString());
							//System.out.println(t.getChild(i).getChild(3).getChild(1).toString());
						}
						//System.out.println(rd.ruleEvalTrue_connection);
					}

					// action connection label
					rd.ruleEvalFalse = RuleAction.valueOf(t.getChild(i).getChild(4).getChild(0).toString());
					// action connection label
					if(t.getChild(i).getChild(4).getChildCount() > 1){
						for(int j = 1; j<t.getChild(i).getChild(4).getChildCount(); j++){
							rd.ruleEvalFalse_connection.add(t.getChild(i).getChild(4).getChild(1).toString());
							//System.out.println(t.getChild(i).getChild(3).getChild(1).toString());
						
						}						
						//System.out.println(rd.ruleEvalFalse_connection);
					}
					
					
					
					
					// eval False goto index
					if(rd.ruleEvalFalse == RuleAction.GOTO){
						rd.ruleEvalFalseGotoLabel = t.getChild(i).getChild(4).getChild(0).getChild(0).toString();
					}
					// eval True goto index
					if(rd.ruleEvalTrue == RuleAction.GOTO){
						rd.ruleEvalTrueGotoLabel = t.getChild(i).getChild(3).getChild(0).getChild(0).toString();
					}
					// label exists
					if(t.getChild(i).getChildCount() == 6){
						rd.label = t.getChild(i).getChild(5).getChild(0).toString();
						//System.out.println(rd.label);
						
					}
					//System.out.println(t.getChild(i).getChild(4).getChild(0).toString());
					//System.out.println(t.getChild(i).getChild(4).getChild(0).getChild(0).toString());
	
					res.rule_mod_lst.add(rd);
					
				// dummy Rule
				}else if(t.getChild(i).getText().equals("DUMMY_RULE_DEF")){
					rd = new RuleDescriptor();
					rd.isDummy = true;
					rd.active = (t.getChild(i).getChild(0).getChild(0).toString().equals("ON") ? true: false);
					rd.ruleEvalTrue = RuleAction.valueOf(t.getChild(i).getChild(1).toString());
					// eval True goto index
					if(rd.ruleEvalTrue == RuleAction.GOTO){
						rd.ruleEvalTrueGotoLabel = t.getChild(i).getChild(1).getChild(0).toString();
						//System.out.println(rd.ruleEvalTrueGotoLabel);
					}
					//System.out.println(rd.ruleEvalTrue);
					// label exists
					if(t.getChild(i).getChildCount() == 3){
						rd.label = t.getChild(i).getChild(2).getChild(0).toString();
						
					}
					res.rule_mod_lst.add(rd);
					
					
				// Modifier
				}else if(t.getChild(i).getText().equals("MODIFIER")){
					md = new ModifierDescriptor();
					md.active = (t.getChild(i).getChild(0).getChild(0).toString().equals("ON") ? true: false);
					md.name = t.getChild(i).getChild(1).toString();
					//System.out.println(md.name);
					md.newValue = t.getChild(i).getChild(1).getChild(0).toString();
					//System.out.println(md.newValue);
					// extra params
					if(t.getChild(i).getChild(1).getChild(0).getChildCount() > 0){
						md.extra_params = new HashMap<String, String>();
						for(int j = 0; j<t.getChild(i).getChild(1).getChild(0).getChildCount(); j++){
							
							//System.out.println((t.getChild(i).getChild(1).getChild(0).getChild(j).toString()));
							md.extra_params.put(t.getChild(i).getChild(1).getChild(0).getChild(j).toString(), 
												t.getChild(i).getChild(1).getChild(0).getChild(j).getChild(0).toString());
						}
					}
					// label exists
					if(t.getChild(i).getChildCount() == 3){
						md.label = t.getChild(i).getChild(2).getChild(0).toString();
						//System.out.println(md.label);
						
					}
					
					res.rule_mod_lst.add(md);
					//System.out.println(md.name);
					//System.out.println(md.newValue);
					
				}
			}			
		}
		return res;
		
	}
	private static boolean executeMD5(SmsfsPacket fp, RuleDescriptor rd, int lst_type, boolean dummy){
		boolean res = false;
		int md5 = 0;
		int max;
		try{
			if(dummy) md5 = 0; else{
				if(fp.filterMode == FilterMode.NORMAL){
					switch(lst_type){
						case CudaManager.MD5_QUARANTINE_LST: md5 = GPUBalancer.checkMD5(Utils.sms_md5(fp), lst_type); break;
						//case CudaManager.MD5_NORMAL_LST: md5 = GPUBalancer.checkMD5(Utils.md5((String)fp.bindings.get(Bindings.get("SMS_TPDU.UD").index)), lst_type); break;
						case CudaManager.MD5_NORMAL_LST: md5 = GPUBalancer.checkMD5(Utils.md5(get_message_text(fp)), lst_type); break;
					}
				}else{
					switch(lst_type){
						case CudaManager.MD5_QUARANTINE_LST: md5 = GPUBalancer.checkMD5(Utils.sms_md5(fp), lst_type, NodeType.REFILTER); break;
						//case CudaManager.MD5_NORMAL_LST: md5 = GPUBalancer.checkMD5(Utils.md5((String)fp.bindings.get(Bindings.get("SMS_TPDU.UD").index)), lst_type, NodeType.REFILTER); break;
						case CudaManager.MD5_NORMAL_LST: md5 = GPUBalancer.checkMD5(Utils.md5(get_message_text(fp)), lst_type, NodeType.REFILTER); break;
					}
				}
			}
			//max = Integer.parseInt(rd.evalItem2);
			//max = Integer.parseInt((String)Utils.isNull(getBoundItem(rd.evalItem2, fp), "0"));
			max = Utils.safeStr2int((String)Utils.isNull(getBoundItem(rd.evalItem2, fp), "0"));
			
			if(rd.evalOperator.equals("==")){
				res = md5 ==  max;
				
			}else if(rd.evalOperator.equals("!=")){
				res = md5 !=  max;

			}else if(rd.evalOperator.equals("<")){		
				res = md5 <  max;
			
			}else if(rd.evalOperator.equals(">")){
				res = md5 >  max;
				
			}else if(rd.evalOperator.equals("<=")){
				res = md5 <=  max;
				
			}else if(rd.evalOperator.equals(">=")){
				res = md5 >=  max;
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}

	private static boolean executeLD(SmsfsPacket fp, RuleDescriptor rd, int lst_id, boolean dummy){
		boolean res = false;
		int ld = 0;
		int max;
		try{
			if(dummy) ld = 0; else{
				//if(fp.filterMode == FilterMode.NORMAL) ld = GPUBalancer.checkLD(lst_id, (String)fp.bindings.get(Bindings.get("SMS_TPDU.UD").index));
				//else ld = GPUBalancer.checkLD(lst_id, (String)fp.bindings.get(Bindings.get("SMS_TPDU.UD").index), NodeType.REFILTER);
				if(fp.filterMode == FilterMode.NORMAL) ld = GPUBalancer.checkLD(lst_id, get_message_text(fp));
				else ld = GPUBalancer.checkLD(lst_id, get_message_text(fp), NodeType.REFILTER);

			}
			//max = Integer.parseInt(rd.evalItem2);
			//max = Integer.parseInt((String)Utils.isNull(getBoundItem(rd.evalItem2, fp), "0"));
			max = Utils.safeStr2int((String)Utils.isNull(getBoundItem(rd.evalItem2, fp), "0"));

			if(rd.evalOperator.equals("==")){
				res = ld ==  max;
				
			}else if(rd.evalOperator.equals("!=")){
				res = ld !=  max;

			}else if(rd.evalOperator.equals("<")){		
				res = ld <  max;
			
			}else if(rd.evalOperator.equals(">")){
				res = ld >  max;
				
			}else if(rd.evalOperator.equals("<=")){
				res = ld <=  max;
				
			}else if(rd.evalOperator.equals(">=")){
				res = ld >=  max;
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}

	private static boolean executeDict(int hits, RuleDescriptor rd, SmsfsPacket fp){
		boolean res = false;
		try{
			//int rulehits = Integer.parseInt(rd.evalItem2);
			//int rulehits = Integer.parseInt((String)Utils.isNull(getBoundItem(rd.evalItem2, fp), "0"));
			int rulehits = Utils.safeStr2int((String)Utils.isNull(getBoundItem(rd.evalItem2, fp), "0"));
			if(rd.evalOperator.equals("==")){
				res = hits ==  rulehits;
				
			}else if(rd.evalOperator.equals("!=")){
				res = hits !=  rulehits;

			}else if(rd.evalOperator.equals("<")){		
				res = hits <  rulehits;
			
			}else if(rd.evalOperator.equals(">")){
				res = hits >  rulehits;
				
			}else if(rd.evalOperator.equals("<=")){
				res = hits <=  rulehits;
				
			}else if(rd.evalOperator.equals(">=")){
				res = hits >=  rulehits;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
		
		
	}

	public static String executeRegex(String regex, String item){
		String res = "0";
		try{
			Pattern pt = Pattern.compile(regex);
			Matcher mt = pt.matcher(item);
			mt.find();
			if(mt.groupCount() > 0) res = mt.group(1);
			else{
				logger.warn("executeRegex: regex [" + regex + "] does not contain any groupig operators!");
			} 
			
		}catch(Exception e){
			//e.printStackTrace();
			logger.error("executeRegex: regex [" + regex + "] error while trying to process item [" + item + "]!");
		}
		return res;
		
	}

	private static Object getBoundItem_smpp(String item_id,  SmsfsPacket fp){
		Object res = null;
		int index;
		if(Bindings.get_smpp(item_id) != null){
			index = Bindings.get_smpp(item_id).index;
			if(index < fp.bindings.size()){
				res = fp.bindings.get(index);
				if(res != null) return res;
			}
		}else{
			// try common map
			res = Bindings.getCommon(item_id);
			if(res != null) return res;
			else return item_id;
		}
		return null;
	}
	
	
	private static Object getBoundItem_sms(String item_id,  SmsfsPacket fp){
		Object res = null;
		int index;
		if(Bindings.get(item_id) != null){
			index = Bindings.get(item_id).index;
			if(index < fp.bindings.size()){
				res = fp.bindings.get(index);
				if(res != null) return res;
			}
		}else{
			// try common map
			res = Bindings.getCommon(item_id);
			if(res != null) return res;
			else return item_id;
		}
		return null;
	}

	private static Object getBoundItem_hlr_result(String item_id,  SmsfsPacket fp){
		MessageDescriptor md = null;
		Integer index;
		HLRPacket hlrp = null;
		if(fp.extra_data.size() > 0){
			md = (MessageDescriptor)fp.extra_data.get(0);
			if((index = Bindings.get_hlr_result(item_id)) != null){
				hlrp = HLRManager.getHLRP("HLRREQ." + md.header.msg_id);
				//System.out.println("getBoundItem_hlr_result: " + md.header.msg_id + ", hlrp: " + hlrp);
				if(hlrp != null){
					switch(index){
						case 0: return hlrp.imsi;
						case 1: return hlrp.nnn;
						case 2: return hlrp.annn;
					}
				}
			}
			
			
		}
		return null;
	}
	private static Object getBoundItem_hlr(String item_id,  SmsfsPacket fp){
		Object res = null;
		int index;
		if(Bindings.get_hlr(item_id) != null){
			index = Bindings.get_hlr(item_id).index;
			if(index < fp.bindings.size()){
				res = fp.bindings.get(index);
				if(res != null) return res;
			}
		}else{
			// try common map
			res = Bindings.getCommon(item_id);
			if(res != null) return res;
			else return item_id;
		}
		return null;
	}
	
	private static Object getBoundItem(String item_id,  SmsfsPacket fp){
		if(fp.hlr_request_executed && item_id.startsWith("HLR.RESULT")){
			return getBoundItem_hlr_result(item_id, fp);
		}else{
			switch(fp.filterType){
				case HLR: return getBoundItem_hlr(item_id, fp);
				case MO:
				case MT: return getBoundItem_sms(item_id, fp);
				case SMPP_MO:
				case SMPP_MT: return getBoundItem_smpp(item_id, fp);
			}
			return null;
			/*
			if(fp.filterType == FilterType.HLR) return getBoundItem_hlr(item_id, fp);
			else return getBoundItem_sms(item_id, fp);
			*/
		}
	}

	private static Object processRuleMethod(String method, ArrayList<String> method_params, SmsfsPacket fp){
		if(method.equalsIgnoreCase("LIST")){
			ListBase lb = ListManagerV2.get_list(ListType.SMSFS_LIST, method_params.get(0).toUpperCase());
			if(lb != null){
				return lb.get_list(fp.filterMode);
			}else return null;
			//return ListManager.getList(method_params.get(0));
			
		}else if(method.equalsIgnoreCase("FLOOD")){
			FloodItem fi = FloodManager.get((String)Utils.isNull(getBoundItem(method_params.get(0), fp), "0"), fp.filterType);
			if(fi != null){
				if(method_params.get(1).equalsIgnoreCase("MINUTE")){
					return Long.toString(fi.minute);
				}else if(method_params.get(1).equalsIgnoreCase("HOUR")){
					return Long.toString(fi.hour);
				}else if(method_params.get(1).equalsIgnoreCase("DAY")){
					return Long.toString(fi.day);
				}
				
			}
		}else if(method.equalsIgnoreCase("FLOOD.MAX")){
			FloodItem fi = FloodManager.get_max((String)Utils.isNull(getBoundItem(method_params.get(0), fp), "0"), fp.filterType);
			if(fi != null){
				if(method_params.get(1).equalsIgnoreCase("MINUTE")){
					return Long.toString(fi.minute);
				}else if(method_params.get(1).equalsIgnoreCase("HOUR")){
					return Long.toString(fi.hour);
				}else if(method_params.get(1).equalsIgnoreCase("DAY")){
					return Long.toString(fi.day);
				}
			}else return Long.toString(9223372036854775806L);
		}else if(method.equalsIgnoreCase("FLOOD.GLOBAL")){
			switch(fp.filterType){
				case MO:
				case SMPP_MO:
					if(method_params.get(0).equalsIgnoreCase("MINUTE")){
						return FloodManager.get_global_current(GlobalMaxType.GLOBAL_MO_MINUTE);
					}else if(method_params.get(0).equalsIgnoreCase("HOUR")){
						return FloodManager.get_global_current(GlobalMaxType.GLOBAL_MO_HOUR);
					}else if(method_params.get(0).equalsIgnoreCase("DAY")){
						return FloodManager.get_global_current(GlobalMaxType.GLOBAL_MO_DAY);
					}
					break;
				case MT:
				case SMPP_MT:
					if(method_params.get(0).equalsIgnoreCase("MINUTE")){
						return FloodManager.get_global_current(GlobalMaxType.GLOBAL_MT_MINUTE);
					}else if(method_params.get(0).equalsIgnoreCase("HOUR")){
						return FloodManager.get_global_current(GlobalMaxType.GLOBAL_MT_HOUR);
					}else if(method_params.get(0).equalsIgnoreCase("DAY")){
						return FloodManager.get_global_current(GlobalMaxType.GLOBAL_MT_DAY);
					}
					break;
			}
		}else if(method.equalsIgnoreCase("FLOOD.GLOBAL.MAX")){
			switch(fp.filterType){
				case MO:
				case SMPP_MO:
					if(method_params.get(0).equalsIgnoreCase("MINUTE")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_MO_MINUTE);
					}else if(method_params.get(0).equalsIgnoreCase("HOUR")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_MO_HOUR);
					}else if(method_params.get(0).equalsIgnoreCase("DAY")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_MO_DAY);
					}
					break;
				case MT:
				case SMPP_MT:
					if(method_params.get(0).equalsIgnoreCase("MINUTE")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_MT_MINUTE);
					}else if(method_params.get(0).equalsIgnoreCase("HOUR")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_MT_HOUR);
					}else if(method_params.get(0).equalsIgnoreCase("DAY")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_MT_DAY);
					}
					break;
				default: return Long.toString(9223372036854775806L);
			}
			
			
		}else if(method.equalsIgnoreCase("FLOOD.ALL.MAX")){
			switch(fp.filterType){
				case MO:
				case SMPP_MO:
					if(method_params.get(0).equalsIgnoreCase("MINUTE")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_ALL_DEST_MO_MINUTE);
					}else if(method_params.get(0).equalsIgnoreCase("HOUR")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_ALL_DEST_MO_HOUR);
					}else if(method_params.get(0).equalsIgnoreCase("DAY")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_ALL_DEST_MO_DAY);
					}
					break;
				case MT:
				case SMPP_MT:
					if(method_params.get(0).equalsIgnoreCase("MINUTE")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_ALL_DEST_MT_MINUTE);
					}else if(method_params.get(0).equalsIgnoreCase("HOUR")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_ALL_DEST_MT_HOUR);
					}else if(method_params.get(0).equalsIgnoreCase("DAY")){
						return FloodManager.get_global_max(GlobalMaxType.GLOBAL_ALL_DEST_MT_DAY);
					}
					break;
				default: return Long.toString(9223372036854775806L);
			}
			
		}
		return null;
	}
	
	private static boolean executeOperator(RuleDescriptor rd, SmsfsPacket fp){
		boolean res = false;
		Object item1 = null;
		Object item2 = null;
		List<?> lst;
		ListItemBase li = null;
		boolean lst_in = false;
		boolean lst_nin = false;
		String tmp_str = null;
		
		try{
			// item 1
			if(!Utils.isInt(rd.evalItem1)){
				if(!rd.item1_is_method){
					item1 = getBoundItem(rd.evalItem1, fp);
					// check if item1 is null
					if(item1 != null){
						// regex
						if(rd.item1_is_regex) item1 = executeRegex(rd.evalItem1Regex, (String)item1);
					}else item1 = "0";
					
				}else{
					item1 = Utils.isNull(processRuleMethod(rd.evalItem1, rd.item_1_method_params, fp), "0");
				}
			}else item1 = rd.evalItem1;
			
			// item 2
			if(!Utils.isInt(rd.evalItem2)){
				if(!rd.item2_is_method){
					item2 = getBoundItem(rd.evalItem2, fp);
					// check if item2 is null
					if(item2 != null){
						// regex
						if(rd.item2_is_regex) item2 = executeRegex(rd.evalItem2Regex, (String)item2);
					}else item2 = "0";
					
				}else{
					item2 = Utils.isNull(processRuleMethod(rd.evalItem2, rd.item_2_method_params, fp), "0");
				}
				
			}else item2 = rd.evalItem2;
			
			
			// String equality
			if(rd.evalOperator.equals("==")){
				res = item1.equals(item2);
			// String not equality
			}else if(rd.evalOperator.equals("!=")){
				res = !item1.equals(item2);
			// Lists
			}else if((lst_in = rd.evalOperator.equalsIgnoreCase("IN")) || (lst_nin = rd.evalOperator.equalsIgnoreCase("NIN"))){
				if(item2 instanceof List<?>){
					// IN
					if(lst_in){
						lst = (List<?>)item2;
						for(Object lst_item : lst){
							li = (ListItemBase)lst_item;
							tmp_str = new String(li.data);
							if(tmp_str.equals(item1)){
								res = true;
								break;
							}
						}
					// NIN
					}else if(lst_nin){
						lst = (List<?>)item2;
						for(Object lst_item : lst){
							li = (ListItemBase)lst_item;
							tmp_str = new String(li.data);
							if(tmp_str.equals(item1)){
								res = true;
								break;
							}
						}
						res = !res;
					}					
				}
				
				
			}else{
				// <, >, <=, >= available only with number types
				if(Utils.isInt((String)item1) && Utils.isInt((String)item2)){
					// String converted to int
					if(rd.evalOperator.equals("<")){
						res = Long.parseLong((String)item1) < Long.parseLong((String)item2);
					// String converted to int
					}else if(rd.evalOperator.equals(">")){
						res = Long.parseLong((String)item1) > Long.parseLong((String)item2);
					// String converted to int
					}else if(rd.evalOperator.equals("<=")){
						res = Long.parseLong((String)item1) <= Long.parseLong((String)item2);
					// String converted to int
					}else if(rd.evalOperator.equals(">=")){
						res = Long.parseLong((String)item1) >= Long.parseLong((String)item2);
					}
				
				}else{
					logger.warn("SMSFS: [" + item1 + " " + rd.evalOperator  + " " + item2 + "] operand/operator combination does not support numeric operators [<, >, <=, >=]!");
				}
			}
				
				
		}catch(Exception e){
			e.printStackTrace();
		}
		return res;
		
	}

	
	private static int findLabel(FilterDescriptor fd, String label){
		RuleDescriptor rd = null;
		ModifierDescriptor md = null;
		for(int i = 0; i<fd.rule_mod_lst.size(); i++){
			if(fd.rule_mod_lst.get(i).type == FilterDescriptorType.RULE){
				rd = (RuleDescriptor)fd.rule_mod_lst.get(i);
				if(rd.label.equalsIgnoreCase(label)) return i;
			}else if(fd.rule_mod_lst.get(i).type == FilterDescriptorType.MODIFIER){
				md = (ModifierDescriptor)fd.rule_mod_lst.get(i);
				if(md.label.equalsIgnoreCase(label)) return i;
				
			}
		}
		return -1;
	}
	private static String get_message_text(SmsfsPacket fp){
		try{
			switch(fp.filterType){
				// SS7/SIGTRAN
				case MO:
				case MT: return (String)fp.bindings.get(Bindings.get("SMS_TPDU.UD").index);
				
				// SMPP
				case SMPP_MO:
				case SMPP_MT: return (String)fp.bindings.get(Bindings.get_smpp("SMPP.SM").index);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static FilterResult executeFilter(FilterDescriptor fd, SmsfsPacket fp){
		RuleDescriptor rd;
		ModifierDescriptor md;
		FilterResult res = null;
		boolean ruleres = false;
		RuleAction ra;
		int dict_hits;
		int i = 0;
		int loop_c = 0;
		String goto_lbl = "";
		int gi;
		//int tmp_id;
		//byte[] vtp_smsfs_data;
		ListBase lb = null;
		ListItemBase li = null;
		ListDescriptor tmp_lst_desc = null;
		MessageDescriptor vstp_md;
		byte[] hex_str;
		byte[] tmp_hash;
		VSTPModifier vstp_modifier = null;
		if(fd != null && fp != null){
			res = new FilterResult();
			while(i < fd.rule_mod_lst.size()){
				if(fd.rule_mod_lst.get(i).type == FilterDescriptorType.RULE){
					rd = (RuleDescriptor)fd.rule_mod_lst.get(i);
					if(rd.active){
						// set current label as exit point
						res.filter_exit_point = rd.label;
						// Dictionary matching
						if(rd.evalItem1.equalsIgnoreCase("DICT.SMS")){
							lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.DICTIONARY.toString());
							tmp_lst_desc = lb.get_list_desc(fp.filterMode);
							if(tmp_lst_desc != null){
								dict_hits = tmp_lst_desc.pattern_search(get_message_text(fp));
								ruleres = executeDict(dict_hits, rd, fp);
							}
							
							if(!ruleres){
								ra = rd.ruleEvalFalse;
								goto_lbl = rd.ruleEvalFalseGotoLabel;
								fp.totalScore += rd.points;
								res.routing_connections = rd.ruleEvalFalse_connection;
								
							}else{
								ra = rd.ruleEvalTrue;
								goto_lbl = rd.ruleEvalTrueGotoLabel;
								res.routing_connections = rd.ruleEvalTrue_connection;
							}
							switch(ra){
								case ALLOW:
									res.result = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_QUARANTINE:
									res.result = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case ALLOW_UNCONDITIONAL:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_UNCONDITIONAL_QUARANTINE:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case DENY:
									res.result = false;
									res.total_score = fp.totalScore;
									return res;
								case DENY_QUARANTINE:
									res.result = false;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case CONTINUE_QUARANTINE:
									res.quarantine = true;
									break;
									
							}
						// MD5 list
						}else if(rd.evalItem1.equalsIgnoreCase("MD5.SMS")){
							// if LD Balancer module if ON
							if(FNConfigData.gpub_status){
								ruleres = executeMD5(fp, rd, CudaManager.MD5_NORMAL_LST, false);
							// else repetitions = 0
							}else{
								ruleres = executeMD5(fp, rd,  CudaManager.MD5_NORMAL_LST, true);
								
							}
							if(!ruleres){
								ra = rd.ruleEvalFalse;
								goto_lbl = rd.ruleEvalFalseGotoLabel;
								fp.totalScore += rd.points;
								res.routing_connections = rd.ruleEvalFalse_connection;
							}else{
								ra = rd.ruleEvalTrue;
								goto_lbl = rd.ruleEvalTrueGotoLabel;
								res.routing_connections = rd.ruleEvalTrue_connection;
							}
							switch(ra){
								case ALLOW:
									res.result = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_QUARANTINE:
									res.result = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case ALLOW_UNCONDITIONAL:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_UNCONDITIONAL_QUARANTINE:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case DENY:
									res.result = false;
									res.total_score = fp.totalScore;
									return res;
								case DENY_QUARANTINE:
									res.result = false;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case CONTINUE_QUARANTINE:
									res.quarantine = true;
									break;
							}
							
						// Quarantine list
						}else if(rd.evalItem1.equalsIgnoreCase("QUARANTINE.SMS")){
							// if LD Balancer module if ON
							if(FNConfigData.gpub_status){
								ruleres = executeMD5(fp, rd, CudaManager.MD5_QUARANTINE_LST, false);
							
							// else repetitions = 0
							}else{
								ruleres = executeMD5(fp, rd,  CudaManager.MD5_QUARANTINE_LST, true);
								
							}
							if(!ruleres){
								ra = rd.ruleEvalFalse;
								goto_lbl = rd.ruleEvalFalseGotoLabel;
								fp.totalScore += rd.points;
								res.routing_connections = rd.ruleEvalFalse_connection;
							}else{
								ra = rd.ruleEvalTrue;
								goto_lbl = rd.ruleEvalTrueGotoLabel;
								res.routing_connections = rd.ruleEvalTrue_connection;
							}
							switch(ra){
								case ALLOW:
									res.result = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_QUARANTINE:
									res.result = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case ALLOW_UNCONDITIONAL:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_UNCONDITIONAL_QUARANTINE:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case DENY:
									res.result = false;
									res.total_score = fp.totalScore;
									return res;
								case DENY_QUARANTINE:
									res.result = false;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case CONTINUE_QUARANTINE:
									res.quarantine = true;
									break;
							}
							
							
						// LD (known spam)
						}else if(rd.evalItem1.equalsIgnoreCase("SPAM.SMS")){
							// if LD Balancer module if ON
							if(FNConfigData.gpub_status){
								ruleres = executeLD(fp, rd, CudaManager.LD_SPAM, false);
							
							// else repetitions = 0
							}else{
								ruleres = executeLD(fp, rd, CudaManager.LD_SPAM, true);
								
							}
							
							if(!ruleres){
								ra = rd.ruleEvalFalse;
								goto_lbl = rd.ruleEvalFalseGotoLabel;
								fp.totalScore += rd.points;
								res.routing_connections = rd.ruleEvalFalse_connection;
							}else{
								ra = rd.ruleEvalTrue;
								goto_lbl = rd.ruleEvalTrueGotoLabel;
								res.routing_connections = rd.ruleEvalTrue_connection;
							}
							switch(ra){
								case ALLOW:
									res.result = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_QUARANTINE:
									res.result = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case ALLOW_UNCONDITIONAL:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_UNCONDITIONAL_QUARANTINE:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case DENY:
									res.result = false;
									res.total_score = fp.totalScore;
									return res;
								case DENY_QUARANTINE:
									res.result = false;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case CONTINUE_QUARANTINE:
									res.quarantine = true;
									break;
							}
						// LD (repetitions)
						}else if(rd.evalItem1.equalsIgnoreCase("RP.SMS")){
							// if LD Balancer module if ON
							if(FNConfigData.gpub_status){
								ruleres = executeLD(fp, rd, CudaManager.LD_REPETITION, false);
								if(fp.filterMode == FilterMode.NORMAL) GPUBalancer.update(CudaManager.LD_REPETITION, get_message_text(fp));
								else GPUBalancer.update(CudaManager.LD_REPETITION, get_message_text(fp), NodeType.REFILTER);
							
							// else repetitions = 0
							}else{
								ruleres = executeLD(fp, rd, CudaManager.LD_REPETITION, true);
								
							}
							if(!ruleres){
								ra = rd.ruleEvalFalse;
								goto_lbl = rd.ruleEvalFalseGotoLabel;
								fp.totalScore += rd.points;
								res.routing_connections = rd.ruleEvalFalse_connection;
							}else{
								ra = rd.ruleEvalTrue;
								goto_lbl = rd.ruleEvalTrueGotoLabel;
								res.routing_connections = rd.ruleEvalTrue_connection;
							}
							switch(ra){
								case ALLOW:
									res.result = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_QUARANTINE:
									res.result = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case ALLOW_UNCONDITIONAL:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_UNCONDITIONAL_QUARANTINE:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case DENY:
									res.result = false;
									res.total_score = fp.totalScore;
									return res;
								case DENY_QUARANTINE:
									res.result = false;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case CONTINUE_QUARANTINE:
									res.quarantine = true;
									break;
							}
							
							
						// Regular logical operators
						}else{
							// check for dummy rule
							if(rd.isDummy){
								ruleres = true;
							}else{
								// rule evaluates to false, add to total score
								ruleres = executeOperator(rd, fp);
							}
							if(!ruleres){
								ra = rd.ruleEvalFalse;
								goto_lbl = rd.ruleEvalFalseGotoLabel;
								fp.totalScore += rd.points;
								res.routing_connections = rd.ruleEvalFalse_connection;
							}else{
								ra = rd.ruleEvalTrue;
								goto_lbl = rd.ruleEvalTrueGotoLabel;
								res.routing_connections = rd.ruleEvalTrue_connection;
							}
							switch(ra){
								case ALLOW:
									res.result = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_QUARANTINE:
									res.result = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case ALLOW_UNCONDITIONAL:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									return res;
								case ALLOW_UNCONDITIONAL_QUARANTINE:
									res.result = true;
									res.isUnconditional = true;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case DENY:
									res.result = false;
									res.total_score = fp.totalScore;
									return res;
								case DENY_QUARANTINE:
									res.result = false;
									res.total_score = fp.totalScore;
									res.quarantine = true;
									return res;
								case CONTINUE_QUARANTINE:
									res.quarantine = true;
									break;
							}
							
							
						}
						
						
					}
				// modifier
				}else{
					md = (ModifierDescriptor)fd.rule_mod_lst.get(i);
					if(md.active){
						// modifier method
						if(md.name.equalsIgnoreCase("MODIFIER_METHOD")){
							if(md.newValue.equalsIgnoreCase("HLR.REQUEST")){
								// remove finished
								//if(fp.last_hlr_req_id != null) HLRManager.removeMutex(fp.last_hlr_req_id);
								// prepare new
								//System.out.println("md.extra_params.get(0): " + md.extra_params.get(0));
								//System.out.println("md.extra_params.get(1): " + md.extra_params.get(1));
								String hlr_msisdn = (String)getBoundItem((String)md.extra_params.get("HLR_MSISDN"), fp);
								String hlr_sca = (String)getBoundItem((String)md.extra_params.get("HLR_SCA"), fp);
								MessageDescriptor orig_md = (MessageDescriptor)fp.extra_data.get(0);
								if(hlr_msisdn != null && hlr_sca != null){
									vstp_md = new MessageDescriptor();
									// header
									vstp_md.header.ds = DataSourceType.NA;
									vstp_md.header.msg_id = "HLRREQ." + orig_md.header.msg_id;
									vstp_md.header.msg_type = MessageType.HLRREQ;
									// body
									vstp_md.values.put(VSTPDataItemType.HLR_MSISDN.getId(), hlr_msisdn);
									vstp_md.values.put(VSTPDataItemType.HLR_SCA.getId(), hlr_sca);
									// mutex
									vstp_md.mutex = new Object();
									// add mutex
									HLRManager.addMutex(vstp_md.header.msg_id, vstp_md.mutex);
									// mark as executex
									fp.hlr_request_executed = true;
									fp.last_hlr_req_id = vstp_md.header.msg_id;
									// seng to queue
									FNode.out_offer(vstp_md);
									//System.out.println("---- HLR REQ START -----");
									//System.out.println(new String(vstp_md.encode()));
									//System.out.println("---- HLR REQ END -----");
									// wait
									//logger.info("Waiting for HLR REPLY!");
									synchronized(vstp_md.mutex){
										try{
											vstp_md.mutex.wait();

										}catch(Exception e){
											e.printStackTrace();
										}
									}
									//logger.info("Received HLR REPLY!");

								}else logger.warn("Error while generating HLR Request, msisdn = [" + hlr_msisdn + "], sca = [" + hlr_sca + "]!");
								
								
							
							// execute method
							}else if(md.newValue.equalsIgnoreCase("MD5.UPDATE_LST")){
								// if LD Balancer module if ON
								if(FNConfigData.gpub_status){
									lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_NORMAL.toString());
									tmp_hash = Utils.md5(get_message_text(fp));
									if(tmp_hash != null){
										hex_str = Utils.bytes2hexstr(tmp_hash, "").getBytes();
										li = lb.exists(hex_str, fp.filterMode);
										if(li == null){
											lb.add(hex_str, fp.filterMode, true);
											// add to cuda nodes
											if(fp.filterMode == FilterMode.NORMAL){
												GPUBalancer.update_md5(tmp_hash, CudaManager.MD5_NORMAL_LST);
											}else GPUBalancer.update_md5(tmp_hash, CudaManager.MD5_NORMAL_LST, NodeType.REFILTER);
										}
									}else logger.warn("MD5.UPDATE_LST: error while generating MD5!");
								}
							}else if(md.newValue.equalsIgnoreCase("MD5.REMOVE")){								
								// if LD Balancer module if ON
								if(FNConfigData.gpub_status){	
									lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_NORMAL.toString());
									tmp_hash = Utils.md5(get_message_text(fp));
									if(tmp_hash != null){
										hex_str = Utils.bytes2hexstr(tmp_hash, "").getBytes();
										li = lb.exists(hex_str, fp.filterMode);
										if(li != null){
											lb.remove(li.data, fp.filterMode);
											// remove from cuda nodes
											if(fp.filterMode == FilterMode.NORMAL){
												GPUBalancer.remove(CudaManager.MD5_NORMAL_LST, tmp_hash);
											}else GPUBalancer.remove(CudaManager.MD5_NORMAL_LST, tmp_hash, NodeType.REFILTER);
										}
									}else logger.warn("MD5.UPDATE_LST: error while generating MD5!");
								}
								
							}else if(md.newValue.equalsIgnoreCase("SPAM.UPDATE_LST")){
								// if LD Balancer module if ON
								if(FNConfigData.gpub_status){
									lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.LD_SPAM.toString());
									// save new spam to known spam list
									// check if spam def already exists
									if(lb.exists(get_message_text(fp).getBytes(), fp.filterMode) == null){
										lb.add(get_message_text(fp).getBytes(), fp.filterMode, true);
										// send to update queue on device
										if(fp.filterMode == FilterMode.NORMAL){
											GPUBalancer.update(CudaManager.LD_SPAM, get_message_text(fp));
										}else GPUBalancer.update(CudaManager.LD_SPAM, get_message_text(fp), NodeType.REFILTER);
									}else logger.warn("SPAM.UPDATE_LST: item already exists!");
								}							
							}else if(md.newValue.equalsIgnoreCase("SPAM.REMOVE")){
								// if LD Balancer module if ON
								if(FNConfigData.gpub_status){
									lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.LD_SPAM.toString());
									// check if spam def already exists
									li = lb.exists(get_message_text(fp).getBytes(), fp.filterMode);
									if(li != null){
										lb.remove(li.data, fp.filterMode);
										// remove from cuda nodes
										if(fp.filterMode == FilterMode.NORMAL){
											GPUBalancer.remove(CudaManager.LD_SPAM, get_message_text(fp).getBytes());
										}else GPUBalancer.remove(CudaManager.LD_SPAM, li.data, NodeType.REFILTER);
									}else logger.warn("SPAM.REMOVE: item not found!");
								}
							}else if(md.newValue.equalsIgnoreCase("QUARANTINE.UPDATE_LST")){
								// if LD Balancer module if ON
								if(FNConfigData.gpub_status){
									lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_QUARANTINE.toString());
									tmp_hash = Utils.sms_md5(fp);
									if(tmp_hash != null){
										hex_str = Utils.bytes2hexstr(tmp_hash, "").getBytes();
										li = lb.exists(hex_str, fp.filterMode);
										if(li == null){
											lb.add(hex_str, fp.filterMode, true);
											// add to cuda nodes
											if(fp.filterMode == FilterMode.NORMAL){
												GPUBalancer.update_md5(tmp_hash, CudaManager.MD5_QUARANTINE_LST);
											}else GPUBalancer.update_md5(tmp_hash, CudaManager.MD5_QUARANTINE_LST, NodeType.REFILTER);
										}
									}else logger.warn("QUARANTINE.UPDATE_LST: error while generating MD5!");
								}
							}else if(md.newValue.equalsIgnoreCase("QUARANTINE.REMOVE")){
								// if LD Balancer module if ON
								if(FNConfigData.gpub_status){	
									lb = ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.LD_SPAM.toString());
									tmp_hash = Utils.sms_md5(fp);
									if(tmp_hash != null){
										hex_str = Utils.bytes2hexstr(tmp_hash, "").getBytes();
										li = lb.exists(hex_str, fp.filterMode);
										if(li != null){
											lb.remove(li.data, fp.filterMode);
											// remove from cuda nodes
											if(fp.filterMode == FilterMode.NORMAL){
												GPUBalancer.remove(CudaManager.MD5_QUARANTINE_LST, tmp_hash);
											}else GPUBalancer.remove(CudaManager.MD5_QUARANTINE_LST, tmp_hash, NodeType.REFILTER);
										}
									}else logger.warn("QUARANTINE.REMOVE: error while generating MD5!");
								}
							// explicit filter converters
							}else if(md.newValue.equalsIgnoreCase("CONVERT.SS7")){
								vstp_modifier = new VSTPModifier();
								vstp_modifier.item = VSTPDataItemType.get(md.newValue);
								vstp_modifier.value = "";
								res.vstp_modifiers.add(vstp_modifier);
							}else if(md.newValue.equalsIgnoreCase("CONVERT.SMPP")){
								vstp_modifier = new VSTPModifier();
								vstp_modifier.item = VSTPDataItemType.get(md.newValue);
								vstp_modifier.value = "";
								res.vstp_modifiers.add(vstp_modifier);
							// NO.DR = NO DATA RETENTION for current packet
							}else if(md.newValue.equalsIgnoreCase("NO.DR")){
								res.no_dr = true;
							}
						// simple value change
						// modified on SG node
						}else{
							//getBoundItem(md.newValue, fp);
							//System.out.println("Bound item: " + md.name + ", md req. val: "  + md.newValue + ", val: " + (String)getBoundItem(md.newValue, fp));
							vstp_modifier = new VSTPModifier();
							vstp_modifier.item = VSTPDataItemType.get(md.name);
							vstp_modifier.value = (String)getBoundItem(md.newValue, fp);
							res.vstp_modifiers.add(vstp_modifier);
						}
					
					}				
				}
				// infinite loop protection
				if(loop_c > FNConfigData.smsfs_goto_loop_max){
					logger.warn("GOTO max loop count [" + FNConfigData.smsfs_goto_loop_max + "] exceeded!");
					return res;
				}

				// goto index
				if(!goto_lbl.equalsIgnoreCase("")){
					gi = findLabel(fd, goto_lbl);
					if(gi != -1){
						i = gi;
						loop_c++;
						goto_lbl = "";
					}else{
						logger.warn("GOTO label [" + goto_lbl + "] does not exist!");
						return res;
					}
				// normal increment
				}else{
					i++;
				}
				
			}
			res.total_score = fp.totalScore;
			
		}
		return res;
	}

}
