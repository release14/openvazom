package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Alert_on_message_delivery extends OptionalParameterBase {

	public Alert_on_message_delivery(){
		type = OptionalParameterType.ALERT_ON_MESSAGE_DELIVERY;
	}

	public void init(byte[] data) {
		// no value
	}

}
