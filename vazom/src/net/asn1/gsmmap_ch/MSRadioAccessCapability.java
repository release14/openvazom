package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MSRadioAccessCapability extends OCTET_STRING{

	public MSRadioAccessCapability(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
