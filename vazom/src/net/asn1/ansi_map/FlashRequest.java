package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FlashRequest extends SET{

	public Digits get_digits(){
		return (Digits)elements.get(0).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(1).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(2).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(3).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(4).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public EmergencyServicesRoutingDigits get_emergencyServicesRoutingDigits(){
		return (EmergencyServicesRoutingDigits)elements.get(5).data;
	}
	public EmergencyServicesRoutingDigits new_emergencyServicesRoutingDigits(){
		return new EmergencyServicesRoutingDigits();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(239, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_emergencyServicesRoutingDigits(); }});
	}
	public FlashRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
