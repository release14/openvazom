package net.isup.parameters;

import net.isup.ParameterType;

public class EOOP extends ParameterBase {

	public EOOP(){
		type = ParameterType.END_OF_OPTIONAL_PARAMETERS;
	}

	public void init(byte[] data) {
		// no data
	}

}
