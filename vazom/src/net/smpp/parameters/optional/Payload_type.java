package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Payload_type extends OptionalParameterBase {
	public enum ValueType{
		DEFAULT(0x00),
		WCMP(0x01);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public ValueType value;
	public Payload_type(){
		type = OptionalParameterType.PAYLOAD_TYPE;
	}

	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
